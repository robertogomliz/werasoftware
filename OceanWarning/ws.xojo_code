#tag Module
Protected Module ws
	#tag Method, Flags = &h0
		Function UpdateStatusCurrent(path as string) As Boolean
		  
		  // Reads the warnings contained in the text file given by 'path'. If the input is empty (or just one space " ") the parameters will be taken from relative path using /../OceanWarning/Current.status.
		  dim f as new FolderItem
		  dim SourceStream as TextInputStream
		  dim i, n as Integer
		  dim auxStr() as String
		  
		  if path.Len>3 then
		    f=GetFolderItem(path)
		  else //empty string
		    f=GetFolderItem(App.execDir + "../OceanWarning/CurrentProbes.status")  // Relative path ist besser
		  end
		  
		  if f = Nil then
		    log.Write(false,"Invalid pathname for current warning status!")
		  elseif not(f.Exists) then
		    log.Write(false,"Current warning status file does not exists!")
		  else
		    SourceStream=TextInputStream.Open(f)
		    if SourceStream = Nil then
		      log.Write(false,"Error opening current warning status file!")
		    else
		      n = CLong(Trim(SourceStream.ReadLine))
		      Redim LatCur(n-1)
		      Redim LonCur(n-1)
		      Redim WarnTypeCur(n-1)
		      Redim NameCur(n-1)
		      Redim ValCur(n-1)
		      
		      if n=0 then
		        statusCur=1
		      else
		        statusCur=2
		        for i=0 to (n-1)
		          auxStr=Split(SourceStream.ReadLine,Chr(9))  //Each column is separated by a tab
		          if auxStr.Ubound<2 then
		            log.Write(false,"Error reading current warning status contents in line "+ CStr(i+1) + "!")
		            SourceStream.Close
		            return false
		          else
		            NameCur(i)=trim(auxStr(0))
		            LatCur(i)=CDbl(trim(auxStr(1)))
		            LonCur(i)=CDbl(trim(auxStr(2)))
		            WarnTypeCur(i)=CLong(trim(auxStr(3)))
		            ValCur(i)=CDbl(trim(auxStr(4)))
		            if WarnTypeCur(i)>statusCur then
		              statusCur=WarnTypeCur(i)
		            end
		          end
		        next
		      end
		      SourceStream.Close
		      return false
		    end
		  end
		  
		  statusCur=0
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UpdateStatusCurrentGeneral(path as string) As Boolean
		  
		  // Reads the warnings contained in the text file given by 'path'. If the input is empty (or just one space " ") the parameters will be taken from relative path using /../OceanWarning/CurrentGeneral.status.
		  dim f as new FolderItem
		  dim SourceStream as TextInputStream
		  dim i, n as Integer
		  dim auxStr(), auxStr2 as String
		  
		  if path.Len>3 then
		    f=GetFolderItem(path)
		  else //empty string
		    f=GetFolderItem(App.execDir + "../OceanWarning/CurrentGeneral.status")  // Relative path ist besser
		  end
		  
		  if f = Nil then
		    log.Write(false,"Invalid pathname for global current warning status!")
		  elseif not(f.Exists) then
		    log.Write(false,"Global current warning status file does not exists!")
		  else
		    SourceStream=TextInputStream.Open(f)
		    if SourceStream = Nil then
		      log.Write(false,"Error opening global current warning status file!")
		    else
		      auxStr2 = Trim(SourceStream.ReadLine)
		      
		      if auxStr2=globalAlertCurTimeStamp then //we have this information already loaded... lets exit
		        SourceStream.Close
		        return false
		      end
		      
		      // If did not exit before, then we must update the variables in ws...
		      
		      globalAlertCurTimeStamp=auxStr2
		      
		      'Update yellow warnings...
		      auxStr=Split(SourceStream.ReadLine,Chr(9))
		      n=CLong(trim(auxStr(1)))
		      
		      Redim globalAlertCurYellowX(n-1)
		      Redim globalAlertCurYellowY(n-1)
		      
		      for i=0 to n-1
		        auxStr=Split(SourceStream.ReadLine,Chr(9))
		        globalAlertCurYellowX(i)=CLong(trim(auxStr(1)))
		        globalAlertCurYellowY(i)=CLong(trim(auxStr(0)))
		      next
		      
		      
		      'Update red warnings...
		      auxStr=Split(SourceStream.ReadLine,Chr(9))
		      n=CLong(trim(auxStr(1)))
		      
		      Redim globalAlertCurRedX(n-1)
		      Redim globalAlertCurRedY(n-1)
		      
		      for i=0 to n-1
		        auxStr=Split(SourceStream.ReadLine,Chr(9))
		        globalAlertCurRedX(i)=CLong(trim(auxStr(1)))
		        globalAlertCurRedY(i)=CLong(trim(auxStr(0)))
		      next
		      
		      'Update max value...
		      auxStr=Split(SourceStream.ReadLine,Chr(9))
		      globalAlertCurMax=CDbl(trim(auxStr(1)))
		      
		      
		      return false
		    end
		  end
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UpdateStatusWave(path as string) As Boolean
		  
		  // Reads the warnings contained in the text file given by 'path'. If the input is empty (or just one space " ") the parameters will be taken from relative path using /../OceanWarning/Wave.status.
		  dim f as new FolderItem
		  dim SourceStream as TextInputStream
		  dim i, n as Integer
		  dim auxStr() as String
		  
		  if path.Len>3 then
		    f=GetFolderItem(path)
		  else //empty string
		    f=GetFolderItem(App.execDir + "../OceanWarning/WaveProbes.status")  // Relative path ist besser
		  end
		  
		  if f = Nil then
		    log.Write(false,"Invalid pathname for wave warning status!")
		  elseif not(f.Exists) then
		    log.Write(false,"Wave warning status file does not exists!")
		  else
		    SourceStream=TextInputStream.Open(f)
		    if SourceStream = Nil then
		      log.Write(false,"Error opening wave warning status file!")
		    else
		      n = CLong(Trim(SourceStream.ReadLine))
		      Redim LatWav(n-1)
		      Redim LonWav(n-1)
		      Redim WarnTypeWav(n-1)
		      Redim NameWav(n-1)
		      Redim ValWav(n-1)
		      
		      if n=0 then
		        statusWav=1
		      else
		        statusWav=2
		        for i=0 to (n-1)
		          auxStr=Split(SourceStream.ReadLine,Chr(9))  //Each column is separated by a tab
		          if auxStr.Ubound<2 then
		            log.Write(false,"Error reading wave warning status contents in line "+ CStr(i+1) + "!")
		            SourceStream.Close
		            return false
		          else
		            NameWav(i)=trim(auxStr(0))
		            LatWav(i)=CDbl(trim(auxStr(1)))
		            LonWav(i)=CDbl(trim(auxStr(2)))
		            WarnTypeWav(i)=CLong(trim(auxStr(3)))
		            ValWav(i)=CDbl(trim(auxStr(4)))
		            if WarnTypeWav(i)>statusWav then
		              statusWav=WarnTypeWav(i)
		            end
		          end
		        next
		      end
		      SourceStream.Close
		      return false
		    end
		  end
		  
		  statusWav=0
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UpdateStatusWaveGeneral(path as string) As Boolean
		  
		  // Reads the warnings contained in the text file given by 'path'. If the input is empty (or just one space " ") the parameters will be taken from relative path using /../OceanWarning/WaveGeneral.status.
		  dim f as new FolderItem
		  dim SourceStream as TextInputStream
		  dim i, n as Integer
		  dim auxStr(), auxStr2 as String
		  
		  if path.Len>3 then
		    f=GetFolderItem(path)
		  else //empty string
		    f=GetFolderItem(App.execDir + "../OceanWarning/WaveGeneral.status")  // Relative path ist besser
		  end
		  
		  if f = Nil then
		    log.Write(false,"Invalid pathname for global wave warning status!")
		  elseif not(f.Exists) then
		    log.Write(false,"Global wave warning status file does not exists!")
		  else
		    SourceStream=TextInputStream.Open(f)
		    if SourceStream = Nil then
		      log.Write(false,"Error opening global wave warning status file!")
		    else
		      auxStr2 = Trim(SourceStream.ReadLine)
		      
		      if auxStr2=globalAlertWavTimeStamp then //we have this information already loaded... lets exit
		        SourceStream.Close
		        return false
		      end
		      
		      // If did not exit before, then we must update the variables in ws...
		      
		      globalAlertWavTimeStamp=auxStr2
		      
		      'Update yellow warnings...
		      auxStr=Split(SourceStream.ReadLine,Chr(9))
		      n=CLong(trim(auxStr(1)))
		      
		      Redim globalAlertWavYellowX(n-1)
		      Redim globalAlertWavYellowY(n-1)
		      
		      for i=0 to n-1
		        auxStr=Split(SourceStream.ReadLine,Chr(9))
		        globalAlertWavYellowX(i)=CLong(trim(auxStr(1)))
		        globalAlertWavYellowY(i)=CLong(trim(auxStr(0)))
		      next
		      
		      
		      'Update red warnings...
		      auxStr=Split(SourceStream.ReadLine,Chr(9))
		      n=CLong(trim(auxStr(1)))
		      
		      Redim globalAlertWavRedX(n-1)
		      Redim globalAlertWavRedY(n-1)
		      
		      for i=0 to n-1
		        auxStr=Split(SourceStream.ReadLine,Chr(9))
		        globalAlertWavRedX(i)=CLong(trim(auxStr(1)))
		        globalAlertWavRedY(i)=CLong(trim(auxStr(0)))
		      next
		      
		      'Update max value...
		      auxStr=Split(SourceStream.ReadLine,Chr(9))
		      globalAlertWavMax=CDbl(trim(auxStr(1)))
		      
		      
		      return false
		    end
		  end
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UpdateStatusWind(path as string) As Boolean
		  
		  // Reads the warnings contained in the text file given by 'path'. If the input is empty (or just one space " ") the parameters will be taken from relative path using /../OceanWarning/Wind.status.
		  dim f as new FolderItem
		  dim SourceStream as TextInputStream
		  dim i, n as Integer
		  dim auxStr() as String
		  
		  if path.Len>3 then
		    f=GetFolderItem(path)
		  else //empty string
		    f=GetFolderItem(App.execDir + "../OceanWarning/Wind.status")  // Relative path ist besser
		  end
		  
		  if f = Nil then
		    log.Write(false,"Invalid pathname for wind warning status file!")
		  elseif not(f.Exists) then
		    log.Write(false,"Wind warning status file does not exists!")
		  else
		    SourceStream=TextInputStream.Open(f)
		    if SourceStream = Nil then
		      log.Write(false,"Error opening wind warning status file!")
		    else
		      n = CLong(Trim(SourceStream.ReadLine))
		      Redim LatWin(n-1)
		      Redim LonWin(n-1)
		      Redim WarnTypeWin(n-1)
		      Redim NameWin(n-1)
		      Redim ValWin(n-1)
		      
		      if n=0 then
		        statusWin=1
		      else
		        statusWin=2
		        for i=0 to (n-1)
		          auxStr=Split(SourceStream.ReadLine,Chr(9))  //Each column is separated by a tab
		          if auxStr.Ubound<2 then
		            log.Write(false,"Error reading wind warning status contents in line "+ CStr(i+1) + "!")
		            SourceStream.Close
		            return false
		          else
		            NameWin(i)=trim(auxStr(0))
		            LatWin(i)=CDbl(trim(auxStr(1)))
		            LonWin(i)=CDbl(trim(auxStr(2)))
		            WarnTypeWin(i)=CLong(trim(auxStr(3)))
		            ValWin(i)=CDbl(trim(auxStr(4)))
		            if WarnTypeWin(i)>statusWin then
		              statusWin=WarnTypeWin(i)
		            end
		          end
		        next
		      end if
		      SourceStream.Close
		      return false
		    end
		  end
		  
		  statusWin=0
		  return true
		End Function
	#tag EndMethod


	#tag Note, Name = Updates
		
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new updates in the next line, not at the end!!! latest update info should ALWAYS be displayed first!!!
		
		2014.02.14
		Roberto Gomez
		-Added method UpdateStatusWaveGeneral for global warnings.
		-Now using status filenames as WaveProbes.status.
		
		2013.12.10
		Roberto Gomez
		-Added method UpdateStatusCurrentGeneral for global warnings.
		-Now using status filenames as CurrentProbes.status.
		
		2013.08.09
		Roberto Gomez
		-Now using folder OceanWarning instead of WarningSystem.
		-Now using status filenames as Current.status, or Wave.status instead of StatusCurrent.txt or StatusWave.txt.
		
		2013.08.05
		-Added properties name and values for Current, wave and wind. Modified the status files accordingly.
		
		2013.08.05
		-Added Methods UpdateStatusWave and UpdateStatusWind
		-Added property status for Current, wave and wind, where 0=failed to update, 1=OK, 2=Warning low, 3=Warning medium, 4=Warning high
		
		2013.07.26
		-First version created:
		-Added properties Lat, Lon and WarnType for current, wave and wind.
		-Method UpdateStatusCurrent used for updating the current warnings extracted from file /home/wera/data-combiner/WarningSystem/StatusCurrent.txt
		
		
		
		IMPORTANT!!! Insert notes of new update at the beginning, not at the end!!! Latest update info should ALWAYS be displayed first!!!
		
	#tag EndNote


	#tag Property, Flags = &h0
		globalAlertCurMax As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		globalAlertCurRedX(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		globalAlertCurRedY(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		globalAlertCurTimeStamp As string
	#tag EndProperty

	#tag Property, Flags = &h0
		globalAlertCurYellowX(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		globalAlertCurYellowY(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		globalAlertWavMax As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		globalAlertWavRedX(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		globalAlertWavRedY(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		globalAlertWavTimeStamp As String
	#tag EndProperty

	#tag Property, Flags = &h0
		globalAlertWavYellowX(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		globalAlertWavYellowY(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		LatCur() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		LatWav() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		LatWin() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		LonCur() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		LonWav() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		LonWin() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		NameCur() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		NameWav() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		NameWin() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		statusCur As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		statusWav As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		statusWin As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		ValCur() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		ValWav() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		ValWin() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		WarnTypeCur() As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		WarnTypeWav() As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		WarnTypeWin() As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="globalAlertCurMax"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="globalAlertCurTimeStamp"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="globalAlertWavMax"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="globalAlertWavTimeStamp"
			Group="Behavior"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="statusCur"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="statusWav"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="statusWin"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
