#tag Module
Protected Module wc
	#tag Method, Flags = &h0
		Function AssignParameterValue(lineOfText as string) As boolean
		  // Assigns the value of a parameter from a line of text from the configuration file
		  
		  Dim auxStringArray(-1), auxString as String
		  Dim i as Integer
		  
		  auxStringArray=split(lineOfText,"=")
		  
		  if Ubound(auxStringArray)<=0 then
		    return false
		  else
		    auxString=Trim(auxStringArray(0)) 'Isolate variable name
		    cfgVarsName_wc.Append(auxString) 'Add it to the list
		    
		    auxStringArray=split(auxStringArray(1),"//")  // Remove comment
		    auxStringArray=split(Trim(auxStringArray(0)),Chr(9))  // Isolate the parameter value by removing the tabs after the value
		    auxString=Trim(auxStringArray(0))  // The variable value should be in the first cell... we only remove spaces before and after
		    
		    Select Case cfgVarsName_wc(cfgVarsName_wc.Ubound)
		    case "curUseQualLevel"
		      curUseQualLevel=CLong(auxString)
		    case "wavUseQualLevel"
		      wavUseQualLevel=CLong(auxString)
		    case "winUseQualLevel"
		      winUseQualLevel=CLong(auxString)
		    case "recipientsLowRisk"
		      recipientsLowRisk=auxString
		    case "recipientsHighRisk"
		      recipientsHighRisk=auxString
		      
		    case "useGlobalLimitCur"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        useGlobalLimitCur=true
		      else
		        useGlobalLimitCur=false
		      end
		    case "curGlobalLow"
		      curGlobalLow=CDbl(auxString)
		    case "curGlobalHigh"
		      curGlobalHigh=CDbl(auxString)
		      
		    case "useGlobalLimitWav"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        useGlobalLimitWav=true
		      else
		        useGlobalLimitWav=false
		      end
		    case "wavGlobalLow"
		      wavGlobalLow=CDbl(auxString)
		    case "wavGlobalHigh"
		      wavGlobalHigh=CDbl(auxString)
		      
		    case "useGlobalLimitWin"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        useGlobalLimitWin=true
		      else
		        useGlobalLimitWin=false
		      end
		    case "winGlobalLow"
		      winGlobalLow=CDbl(auxString)
		    case "winGlobalHigh"
		      winGlobalHigh=CDbl(auxString)
		      
		    case "nProbes"
		      nProbes=CLong(auxString)
		      Redim probeName_p(nProbes-1)
		      Redim lat_p(nProbes-1)
		      Redim lon_p(nProbes-1)
		      Redim curEnabled_p(nProbes-1)
		      Redim curLimLow_p(nProbes-1)
		      Redim curLimHigh_p(nProbes-1)
		      Redim wavEnabled_p(nProbes-1)
		      Redim wavLimLow_p(nProbes-1)
		      Redim wavLimHigh_p(nProbes-1)
		      Redim winEnabled_P(nProbes-1)
		      Redim winLimLow_p(nProbes-1)
		      Redim winlimHigh_p(nProbes-1)
		      
		    case "averageNeighbors"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        averageNeighbors=true
		      else
		        averageNeighbors=false
		      end
		    case "probeName_p"
		      for i=0 to nProbes-1
		        if auxStringArray.Ubound<i then
		          probeName_p(i)="location"+CStr(i+1)
		        else
		          probeName_p(i)=auxStringArray(i)
		        end
		      next
		      
		    case "lat_p"
		      for i=0 to nProbes-1
		        if auxStringArray.Ubound<i then
		          lat_p(i)=0
		        else
		          lat_p(i)=CDbl(auxStringArray(i))
		        end
		      next
		      
		    case "lon_p"
		      for i=0 to nProbes-1
		        if auxStringArray.Ubound<i then
		          lon_p(i)=0
		        else
		          lon_p(i)=CDbl(auxStringArray(i))
		        end
		      next
		      
		    case "curEnabled_p"
		      for i=0 to nProbes-1
		        if auxStringArray.Ubound<i then
		          curEnabled_p(i)=false
		        else
		          if auxStringArray(i)="true" or auxStringArray(i)="True" or auxStringArray(i)="TRUE" or auxStringArray(i)=".TRUE" or auxStringArray(i)=".TRUE." or auxStringArray(i)="1" then
		            curEnabled_p(i)=true
		          else
		            curEnabled_p(i)=false
		          end
		        end
		      next
		      
		    case "curLimLow_p"
		      for i=0 to nProbes-1
		        if auxStringArray.Ubound<i then
		          curLimLow_p(i)=0
		        else
		          curLimLow_p(i)=CDbl(auxStringArray(i))
		        end
		      next
		      
		    case "curLimHigh_p"
		      for i=0 to nProbes-1
		        if auxStringArray.Ubound<i then
		          curLimHigh_p(i)=0
		        else
		          curLimHigh_p(i)=CDbl(auxStringArray(i))
		        end
		      next
		      
		    case "wavEnabled_p"
		      for i=0 to nProbes-1
		        if auxStringArray.Ubound<i then
		          wavEnabled_p(i)=false
		        else
		          if auxStringArray(i)="true" or auxStringArray(i)="True" or auxStringArray(i)="TRUE" or auxStringArray(i)=".TRUE" or auxStringArray(i)=".TRUE." or auxStringArray(i)="1" then
		            wavEnabled_p(i)=true
		          else
		            wavEnabled_p(i)=false
		          end
		        end
		      next
		      
		    case "wavLimLow_p"
		      for i=0 to nProbes-1
		        if auxStringArray.Ubound<i then
		          wavLimLow_p(i)=0
		        else
		          wavLimLow_p(i)=CDbl(auxStringArray(i))
		        end
		      next
		      
		    case "wavLimHigh_p"
		      for i=0 to nProbes-1
		        if auxStringArray.Ubound<i then
		          wavLimHigh_p(i)=0
		        else
		          wavLimHigh_p(i)=CDbl(auxStringArray(i))
		        end
		      next
		      
		    case "winEnabled_p"
		      for i=0 to nProbes-1
		        if auxStringArray.Ubound<i then
		          winEnabled_p(i)=false
		        else
		          if auxStringArray(i)="true" or auxStringArray(i)="True" or auxStringArray(i)="TRUE" or auxStringArray(i)=".TRUE" or auxStringArray(i)=".TRUE." or auxStringArray(i)="1" then
		            winEnabled_p(i)=true
		          else
		            winEnabled_p(i)=false
		          end
		        end
		      next
		      
		    case "winLimLow_p"
		      for i=0 to nProbes-1
		        if auxStringArray.Ubound<i then
		          winLimLow_p(i)=0
		        else
		          winLimLow_p(i)=CDbl(auxStringArray(i))
		        end
		      next
		      
		    case "winLimHigh_p"
		      for i=0 to nProbes-1
		        if auxStringArray.Ubound<i then
		          winLimHigh_p(i)=0
		        else
		          winLimHigh_p(i)=CDbl(auxStringArray(i))
		        end
		      next
		      
		    else
		      //Warning log... parameter not recognized!
		      log.Write(false,"WARNING... parameter in warning configuration file not recognized > " + cfgVarsName_wc(cfgVarsName_wc.Ubound))
		      cfgVarsName_wc.Remove(cfgVarsName_wc.Ubound)
		      return false
		    end
		    return true
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function InitializeConfig(path as string) As boolean
		  // Initializes all the parameters and properties used to work correctly. This is done by reading the parameters configuration file 'OceanWarningConfig.cfg'. Returns true when the Initialization is successfully made.
		  
		  
		  // Opening OceanWarningConfig.cfg file =======================================================
		  Dim f As New FolderItem
		  
		  if path.Len>3 then
		    f=GetFolderItem(path)
		  else //empty string
		    f = GetFolderItem(App.execDir + "OceanWarningConfig.cfg")
		  end
		  
		  Dim configFile As TextInputStream
		  
		  If f <> Nil Then
		    If f.Exists Then
		      // Be aware that TextInputStream.Open coud raise an exception
		      Try
		        configFile = TextInputStream.Open(f)
		        configFile.Encoding = Encodings.ASCII
		      Catch e As IOException
		        configFile.Close
		        log.Write(true,"Error accessing ocean warning configuration file.")
		        return false
		      End Try
		    Else
		      log.Write(true,"Ocean warning configuration file does not exists.")
		      return false
		    End If
		  Else
		    log.Write(true,"Ocean warning configuration file is 'Nil'. Make sure folder exists.")
		    return false
		  End If
		  
		  
		  // Reading OceanWarningConfig.cfg file =======================================================
		  
		  Dim auxString As String
		  Dim cnt, i as Integer
		  
		  linesOfTxtFile_wc=Split(configFile.ReadAll,EndOfLine)  // Split the text content into lines
		  
		  cnt=0
		  ReDim lineIdxOfVars_wc(-1)
		  ReDim cfgVarsName_wc(-1)
		  for i=0 to linesOfTxtFile_wc.Ubound
		    
		    auxString=left(Trim(linesOfTxtFile_wc(i)),1)  // Getting the first written character to see if it is only a comment or a blank line
		    
		    if auxString="/" or auxString="!" or auxString="%" or auxString="" then
		      continue
		    else
		      if wc.AssignParameterValue(linesOfTxtFile_wc(i)) then
		        lineIdxOfVars_wc.Append(i)
		        cnt=cnt+1
		      else
		        // If Assign... did not suceed then it is already logged to file as a warning
		      end
		    end
		  next
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SaveConfig(path as string) As Boolean
		  // Saves the current configuration parameters to 'OceanWarningConfig.cfg'. Returns true when successfull.
		  
		  Dim f As  FolderItem
		  Dim t as TextOutputStream
		  Dim i, count, j as Integer
		  Dim auxString, auxStringArray(-1) as String
		  
		  if path.Len>3 then
		    f=GetFolderItem(path)
		  else //empty string
		    f = GetFolderItem(App.execDir + "OceanWarningConfig.cfg")
		  end
		  
		  if f<>Nil Then
		    
		    t =t.Create(f)
		    
		    count=0
		    for i=0 to linesOfTxtFile_wc.Ubound
		      if count<=lineIdxOfVars_wc.Ubound then  'Avoid OutOfBoundsException for the last lines of file where there are no more variables, but there are still lines to write
		        if i=lineIdxOfVars_wc(count) then  'A line is being written where a variable is located
		          
		          //Build string of updated value of variable
		          Select case cfgVarsName_wc(count)
		          case "curUseQualLevel"
		            auxString=CStr(curUseQualLevel)
		          case "wavUseQualLevel"
		            auxString=CStr(wavUseQualLevel)
		          case "winUseQualLevel"
		            auxString=CStr(winUseQualLevel)
		          case "recipientsLowRisk"
		            auxString=recipientsLowRisk
		          case "recipientsHighRisk"
		            auxString=recipientsHighRisk
		            
		          case "useGlobalLimitCur"
		            if useGlobalLimitCur then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "curGlobalLow"
		            auxString=CStr(curGlobalLow)
		          case "curGlobalHigh"
		            auxString=CStr(curGlobalHigh)
		            
		          case "useGlobalLimitWav"
		            if useGlobalLimitWav then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "wavGlobalLow"
		            auxString=CStr(wavGlobalLow)
		          case "wavGlobalHigh"
		            auxString=CStr(wavGlobalHigh)
		            
		          case "useGlobalLimitWin"
		            if useGlobalLimitWin then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "winGlobalLow"
		            auxString=CStr(winGlobalLow)
		          case "winGlobalHigh"
		            auxString=CStr(winGlobalHigh)
		            
		          case "nProbes"
		            auxString=CStr(nProbes)
		          case "averageNeighbors"
		            if averageNeighbors then
		              auxString="true"
		            else
		              auxString="false"
		            end
		            
		            
		          case "probeName_p"
		            auxString=probeName_p(0)
		            for j=1 to nProbes-1
		              auxString=auxString+Chr(9)+probeName_p(j)
		            next
		            
		          case "lat_p"
		            auxString=CStr(lat_p(0))
		            for j=1 to nProbes-1
		              auxString=auxString+Chr(9)+CStr(lat_p(j))
		            next
		            
		          case "lon_p"
		            auxString=CStr(lon_p(0))
		            for j=1 to nProbes-1
		              auxString=auxString+Chr(9)+CStr(lon_p(j))
		            next
		            
		          case "curEnabled_p"
		            auxString=CStr(curEnabled_p(0))
		            for j=1 to nProbes-1
		              auxString=auxString+Chr(9)+CStr(curEnabled_p(j))
		            next
		            
		          case "curLimLow_p"
		            auxString=CStr(curLimLow_p(0))
		            for j=1 to nProbes-1
		              auxString=auxString+Chr(9)+CStr(curLimLow_p(j))
		            next
		            
		          case "curLimHigh_p"
		            auxString=CStr(curLimHigh_p(0))
		            for j=1 to nProbes-1
		              auxString=auxString+Chr(9)+CStr(curLimHigh_p(j))
		            next
		            
		          case "wavEnabled_p"
		            auxString=CStr(wavEnabled_p(0))
		            for j=1 to nProbes-1
		              auxString=auxString+Chr(9)+CStr(wavEnabled_p(j))
		            next
		            
		          case "wavLimLow_p"
		            auxString=CStr(wavLimLow_p(0))
		            for j=1 to nProbes-1
		              auxString=auxString+Chr(9)+CStr(wavLimLow_p(j))
		            next
		            
		          case "wavLimHigh_p"
		            auxString=CStr(wavLimHigh_p(0))
		            for j=1 to nProbes-1
		              auxString=auxString+Chr(9)+CStr(wavLimHigh_p(j))
		            next
		            
		          case "winEnabled_p"
		            auxString=CStr(winEnabled_p(0))
		            for j=1 to nProbes-1
		              auxString=auxString+Chr(9)+CStr(winEnabled_p(j))
		            next
		            
		          case "winLimLow_p"
		            auxString=CStr(winLimLow_p(0))
		            for j=1 to nProbes-1
		              auxString=auxString+Chr(9)+CStr(winLimLow_p(j))
		            next
		            
		          case "winLimHigh_p"
		            auxString=CStr(winLimHigh_p(0))
		            for j=1 to nProbes-1
		              auxString=auxString+Chr(9)+CStr(winLimHigh_p(j))
		            next
		            
		          else
		            //Warning log... parameter not recognized!
		            t.WriteLine(linesOfTxtFile_wc(i))
		            log.Write(false,cfgVarsName_wc(count) + " - Line " + CStr(i) + ": WarningConfig parameter not recognized.")
		            count=count + 1
		            continue
		          end
		          
		          //Write line with updated variable
		          auxStringArray=Split(linesOfTxtFile_wc(i),"=")
		          
		          if auxStringArray.Ubound<=0 then
		            //Warning log... could not find = sign in a text line where a variable is defined
		            t.WriteLine(linesOfTxtFile_wc(i))
		            log.Write(false,cfgVarsName_wc(count) + " - " + f.Name + " Line " + CStr(i) + ": Could not find = sign in a text line where a variable seems to be defined.")
		            count=count + 1
		            continue
		          else
		            t.Write(auxStringArray(0) + "= " + auxString)
		            
		            auxStringArray=Split(linesOfTxtFile_wc(i),"//")
		            if auxStringArray.Ubound<=0 then 'Adding also the comment to the line
		              t.WriteLine("")
		            else
		              if len(auxString)>=22 then
		                t.WriteLine(" //"+auxStringArray(1))
		              elseif len(auxString)>=14 then
		                t.WriteLine(Chr(9)+"//"+auxStringArray(1))
		              elseif len(auxString)>=6 then
		                t.WriteLine(Chr(9)+Chr(9)+"//"+auxStringArray(1))
		              else
		                t.WriteLine(Chr(9)+Chr(9)+Chr(9)+"//"+auxStringArray(1))
		              end if
		            end if
		          end
		          count=count+1
		          
		        else
		          t.WriteLine(linesOfTxtFile_wc(i))
		        end
		      else
		        t.WriteLine(linesOfTxtFile_wc(i))
		      end
		    next
		    
		    t.close
		    
		    return true
		  else
		    log.Write(true,"Could not save "+ f.Name + " file. Folder " + f.Parent.AbsolutePath + " may not exist.")
		    return false
		  end if
		  
		End Function
	#tag EndMethod


	#tag Note, Name = FileExample
		
		// File containing configuration parameters for WERA Warning System.
		// Use '//' to insert comments.
		
		    curUseQualified    = true            // Evaluate only good qualified measurements
		    wavUseQualified    = false            // 
		    winUseQualified    = false
		    averageNeighbors    = true            // Calculate an average of a 3x3 square before investigating
		
		// Recipients for low risk warnings
		    recipientsLowRisk    = gomez@helzel.com,roberto.gomliz@gmail.com
		
		// Recipients for high risk warnings
		    recipientsHighRisk    = gomez@helzel.com
		
		//VARIABLE nProbes SHOULD BE DECLARED BEFORE THE PARAMETERS ON EACH PROBE!
		    nProbes        = 5            // Number of probes to be analyzed.
		
		//Parameters of the probes are given as tab separated values. The first value corresponds to the first probe and so on.
		
		    // Name given the points being investigated
		    probeName_p    = location1    location2    location3    location4    location5
		
		    // Latitude coordinate of the point being investigated
		    lat_p        = 33.1    33.2    33.3    33.4    33.5
		
		    // Longitude coordinate of the point being investigated
		    lon_p        = -78.1    -78.2    -78.3    -78.4    -78.5
		
		    // Enable investigation of current measurements
		    curEnabled_p    = true    true    true    true    true
		
		    // Lower warning threshold (low risk) for measured current in m/s
		    curLimLow_p        = 2.1    2.2    2.3    2.4    2.5
		
		    // Higher warning threshold (high risk) for measured current in m/s
		    curLimHigh_p    = 3.1    3.2    3.3    3.4    3.5
		
		    // Enable investigation of current measurements
		    wavEnabled_p    = true    true    true    true    true
		
		    // Lower warning threshold (low risk) for measured wave-height in m
		    wavLimLow_p        = 1.1    1.2    1.3    1.4    1.5
		
		    // Higher warning threshold (high risk) for measured wave-height in m
		    wavLimHigh_p    = 1.6    1.7    1.8    1.9    2
		
		    // Enable investigation of current measurements
		    winEnabled_p    = true    true    true    true    true
		
		    // Lower warning threshold (low risk) for measured wind-speed in m/s
		    winLimLow_p        = 11    12    13    14    15
		
		    // Higher warning threshold (high risk) for measured wind-speed in m/s
		    winLimHigh_p    = 21    22    23    24    25
		
		//__________________________________________________________________________
		
	#tag EndNote

	#tag Note, Name = Updates
		
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new updates in the next line, not at the end!!! latest update info should ALWAYS be displayed first!!!
		
		2013.12.04
		Roberto Gomez
		-Replaced boolean parameters curUseQualified, wavUseQualified and winUseQualified by integer parameters curUseQualLevel, wavUseQualLevel and winUseQualLevel.
		-Added parameters useGlobalLimitCur, useGlobalLimitWav and useGlobalLimitWin.
		
		2013.11.08
		Roberto Gomez
		-Added parameters curGlobalLow, curGlobalHigh, wavGlobalLow, wavGlobalHigh, winGlobalLow and winGlobalHigh to set global thresholds to evaluate measurements.
		
		2013.09.12
		Roberto Gomez
		-Included an extra trim in AssignParameterValue method when doing split with tabs for parameters for each probe. Avoids having an space at the beginning of parameters for the first probe.
		
		2013.08.07
		Roberto Gomez
		-First version
		
		
		IMPORTANT!!! Insert notes of new update at the beginning, not at the end!!! Latest update info should ALWAYS be displayed first!!!
		
		
		
		
		
	#tag EndNote


	#tag Property, Flags = &h0
		averageNeighbors As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		cfgVarsName_wc(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		curEnabled_p(-1) As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		curGlobalHigh As Double = 15
	#tag EndProperty

	#tag Property, Flags = &h0
		curGlobalLow As double = 10
	#tag EndProperty

	#tag Property, Flags = &h0
		curLimHigh_p(-1) As Single
	#tag EndProperty

	#tag Property, Flags = &h0
		curLimLow_p(-1) As Single
	#tag EndProperty

	#tag Property, Flags = &h0
		curUseQualLevel As Integer = 3
	#tag EndProperty

	#tag Property, Flags = &h0
		lat_p(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		lineIdxOfVars_wc(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		linesOfTxtFile_wc(-1) As string
	#tag EndProperty

	#tag Property, Flags = &h0
		lon_p(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		nProbes As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		probeName_p(-1) As string
	#tag EndProperty

	#tag Property, Flags = &h0
		recipientsHighRisk As string
	#tag EndProperty

	#tag Property, Flags = &h0
		recipientsLowRisk As string
	#tag EndProperty

	#tag Property, Flags = &h0
		useGlobalLimitCur As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		useGlobalLimitWav As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		useGlobalLimitWin As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		wavEnabled_p(-1) As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		wavGlobalHigh As Double = 10
	#tag EndProperty

	#tag Property, Flags = &h0
		wavGlobalLow As Double = 5
	#tag EndProperty

	#tag Property, Flags = &h0
		wavLimHigh_p(-1) As Single
	#tag EndProperty

	#tag Property, Flags = &h0
		wavLimLow_p(-1) As Single
	#tag EndProperty

	#tag Property, Flags = &h0
		wavUseQualLevel As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		winEnabled_p(-1) As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		winGlobalHigh As Double = 25
	#tag EndProperty

	#tag Property, Flags = &h0
		winGlobalLow As Double = 20
	#tag EndProperty

	#tag Property, Flags = &h0
		winLimHigh_p(-1) As Single
	#tag EndProperty

	#tag Property, Flags = &h0
		winLimLow_p(-1) As Single
	#tag EndProperty

	#tag Property, Flags = &h0
		winUseQualLevel As Integer = 1
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="averageNeighbors"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="curGlobalHigh"
			Group="Behavior"
			InitialValue="15"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="curGlobalLow"
			Group="Behavior"
			InitialValue="10"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="curUseQualLevel"
			Group="Behavior"
			InitialValue="3"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nProbes"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recipientsHighRisk"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recipientsLowRisk"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="useGlobalLimitCur"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="useGlobalLimitWav"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="useGlobalLimitWin"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="wavGlobalHigh"
			Group="Behavior"
			InitialValue="10"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="wavGlobalLow"
			Group="Behavior"
			InitialValue="5"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="wavUseQualLevel"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="winGlobalHigh"
			Group="Behavior"
			InitialValue="25"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="winGlobalLow"
			Group="Behavior"
			InitialValue="20"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="winUseQualLevel"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
