#tag Class
Protected Class UpdaterThreadClass
Inherits Thread
	#tag Event
		Sub Run()
		  // Here all things should be updated
		  
		  // Update last maps  ======================================
		  
		  dim rs as RecordSet
		  
		  if cfgDV.enableCCV or cfgDV.nStations=1 then
		    if cfgDV.nStations=1 then
		      if cfgDV.homeMapTypeCur=0 then rs=db.GetLastMeasured(1,1) else rs=db.GetLastMeasured(1,2)
		    elseif cfgDV.initialMapType>0 AND cfgDV.initialMapType<10 then
		      if cfgDV.homeMapTypeCur=0 then rs=db.GetLastMeasured(cfgDV.initialMapType,1) else rs=db.GetLastMeasured(cfgDV.initialMapType,2)
		    else
		      if cfgDV.homeMapTypeCur=0 then rs=db.GetLastMeasured(0,1) else rs=db.GetLastMeasured(0,2)
		    end if
		    if rs<>Nil then
		      if not(rs.BOF) then
		        App.lastCurMapDate=rs.Field("measTimeDB").DateValue
		        if cfgDV.nStations=1 OR (cfgDV.initialMapType>0 AND cfgDV.initialMapType<10) then App.lastCurMapId=rs.Field("id_tb_crad_header").IntegerValue else App.lastCurMapId=rs.Field("id_tb_cuv_headers").IntegerValue  //In case it is a one station system and radial current map should be considered
		        App.lastCurMapQual=false
		        
		        if cfgDV.homeMapTypeCur<>0 AND cfgDV.nStations>1 then
		          dim tmpInt as integer=rs.Field("measQual").IntegerValue
		          if tmpInt>=8 then
		            tmpInt=tmpInt-8
		            if cfgDV.homeMapTypeCur=4 then App.lastCurMapQual=true
		          end
		          if tmpInt>=4 then
		            tmpInt=tmpInt-4
		            if cfgDV.homeMapTypeCur=3 then App.lastCurMapQual=true
		          end
		          if tmpInt>=2 then
		            tmpInt=tmpInt-2
		            if cfgDV.homeMapTypeCur=2 then App.lastCurMapQual=true
		          end
		          if tmpInt=1 AND cfgDV.homeMapTypeCur=1 then App.lastCurMapQual=true
		        else
		          App.lastCurMapQual=true
		        end
		        
		      end
		    end
		  end
		  if cfgDV.enableCSWH then
		    if cfgDV.homeMapTypeWav=0 then rs=db.GetLastMeasured(10,1) else rs=db.GetLastMeasured(10,2)
		    if rs<>Nil then
		      if not(rs.BOF) then
		        App.lastWavMapDate=rs.Field("measTimeDB").DateValue
		        App.lastWavMapId=rs.Field("id_tb_wuv_headers").IntegerValue
		      end
		    end
		  end
		  if cfgDV.enableWD then
		    if cfgDV.homeMapTypeWin=0 then rs=db.GetLastMeasured(20,1) else rs=db.GetLastMeasured(20,2)
		    if rs<>Nil then
		      if not(rs.BOF) then
		        App.lastWinMapDate=rs.Field("measTimeDB").DateValue
		        App.lastWinMapId=rs.Field("id_tb_wuv_headers").IntegerValue
		      end
		    end
		  end
		  if cfgDV.enableForecastCCV then
		    dim f as FolderItem
		    f=GetFolderItem(cfgDV.stdFolder + "Arret_outbox/curextrap/animation.txt")
		    dim tis as TextInputStream
		    If f <> Nil Then
		      If f.Exists Then
		        Try
		          tis = TextInputStream.Open(f)
		          tis.Encoding = Encodings.ASCII
		          App.lastFcstCurDate.SQLDateTime=std.Gurgeltime2DBstring(tis.ReadLine.Trim)
		        Catch e As IOException
		        End Try
		        tis.Close
		      End If
		    End If
		  end
		  
		  // Update latest measured stations
		  if cfgDV.nStations>1 then
		    
		    if cfgDV.enableCCV then
		      rs=db.GetLastMeasured(1,0)
		      if rs<>Nil then
		        if not(rs.BOF) then App.lastSt1CurDate=rs.Field("measTimeDB").DateValue
		      end
		      rs=db.GetLastMeasured(2,0)
		      if rs<>Nil then
		        if not(rs.BOF) then App.lastSt2CurDate=rs.Field("measTimeDB").DateValue
		      end
		    end if
		    
		    if cfgDV.enableCSWH OR cfgDV.enableWD then
		      rs=db.GetLastMeasured(11,0)
		      if rs<>Nil then
		        if not(rs.BOF) then App.lastSt1WavDate=rs.Field("measTimeDB").DateValue
		      end
		      rs=db.GetLastMeasured(12,0)
		      if rs<>Nil then
		        if not(rs.BOF) then App.lastSt2WavDate=rs.Field("measTimeDB").DateValue
		      end
		    end if
		    
		  end
		  
		  
		  //Update System Monitor Variables =====================================
		  
		  if not(sm.UpdateSystemStatus) then
		    log.Write(true,"Problem updating system status.")
		  end
		  
		  
		  //Update Ocean Alert Variables  =======================================
		  
		  if App.OceanWarningSystemEnabled then
		    if cfgDV.enableCCV then
		      if ws.UpdateStatusCurrent(" ") then
		        log.Write(true,"Problem updating alerts for currents.")
		      end
		      if ws.UpdateStatusCurrentGeneral(" ") then
		        log.Write(true,"Problem updating global alerts for currents.")
		      end
		    end
		    if cfgDV.enableCSWH then
		      if ws.UpdateStatusWave(" ") then
		        log.Write(true,"Problem updating alerts for waves.")
		      end
		      if ws.UpdateStatusWaveGeneral(" ") then
		        log.Write(true,"Problem updating global alerts for waves.")
		      end
		    end
		    if cfgDV.enableWD then
		      if ws.UpdateStatusWind(" ") then
		        log.Write(true,"Problem updating alerts for wind.")
		      end
		    end
		  end
		  
		  
		End Sub
	#tag EndEvent


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Priority"
			Visible=true
			Group="Behavior"
			InitialValue="5"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StackSize"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
