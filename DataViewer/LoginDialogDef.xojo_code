#tag WebPage
Begin WebDialog LoginDialogDef
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   220
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   0
   Left            =   324
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   LockVertical    =   False
   MinHeight       =   0
   MinWidth        =   0
   Resizable       =   False
   Style           =   "84463615"
   TabOrder        =   0
   Title           =   "#kLoginDialogDefTitle"
   Top             =   257
   Type            =   3
   VerticalCenter  =   0
   Visible         =   True
   Width           =   270
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebTextField TxtUsername
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   40
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "511391743"
      TabOrder        =   0
      Text            =   ""
      Top             =   76
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   191
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TxtPassword
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   40
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "511391743"
      TabOrder        =   1
      Text            =   ""
      Top             =   138
      Type            =   1
      VerticalCenter  =   0
      Visible         =   True
      Width           =   191
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblUsername
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   40
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1035042815"
      TabOrder        =   4
      Text            =   "#kLoginUsername"
      Top             =   51
      VerticalCenter  =   0
      Visible         =   True
      Width           =   191
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblPass
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   40
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1035042815"
      TabOrder        =   5
      Text            =   "#kLoginPassword"
      Top             =   113
      VerticalCenter  =   0
      Visible         =   True
      Width           =   107
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton OKBtn
      AutoDisable     =   False
      Caption         =   "#App.kAccept"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   150
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "434558975"
      TabOrder        =   2
      Top             =   180
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton CancelBtn
      AutoDisable     =   False
      Caption         =   "#App.kCancel"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "434558975"
      TabOrder        =   3
      Top             =   180
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblInfo
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   29
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   6
      Text            =   "#kLoginString"
      Top             =   10
      VerticalCenter  =   0
      Visible         =   True
      Width           =   230
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Close()
		  Login.Lbl.Text=replace(kRefreshToLogin(cfgDV.language),"WERA",std.kRadName)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  me.Title=kLoginDialogDefTitle(cfgDV.language)
		  CancelBtn.Caption=App.kCancel(cfgDV.language)
		  LblInfo.Text=kLoginString(cfgDV.language)
		  LblPass.Text=kLoginPassword(cfgDV.language)
		  LblUsername.Text=kLoginUsername(cfgDV.language)
		  OKBtn.Caption=App.kAccept(cfgDV.language)
		  
		  TxtUsername.SetFocus
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub DoLogin()
		  dim auxInt as Integer
		  
		  auxInt=access.usernames.IndexOf(TxtUsername.Text)
		  
		  if auxInt>=0 then
		    if StrComp( TxtPassword.Text , access.passwords(auxInt) , 1) = 0 then
		      Session.account=access.usernames(auxInt)
		      Session.ShowWelcomeMsg
		      self.Hide
		      Main.Show
		      log.Write(false,"Login succesful. Account " + access.usernames(auxInt))
		      Return
		    end
		  end
		  log.Write(false,"Login failed. Account " + TxtUsername.Text)
		  MsgBox("Login failed!")
		  
		End Sub
	#tag EndMethod


	#tag Constant, Name = kLoginDialogDefTitle, Type = String, Dynamic = True, Default = \"Restricted access.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Restricted access."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Accesso restringido."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Acesso restrito."
	#tag EndConstant

	#tag Constant, Name = kLoginPassword, Type = String, Dynamic = True, Default = \"Password", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Password"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Contrase\xC3\xB1a"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Senha"
	#tag EndConstant

	#tag Constant, Name = kLoginString, Type = String, Dynamic = True, Default = \"Restricted access. Provide ID.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Restricted access. Provide ID."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Accesso restringido. Favor de identificarse."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Acesso restrito. Fornecer ID."
	#tag EndConstant

	#tag Constant, Name = kLoginUsername, Type = String, Dynamic = True, Default = \"Username", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Username"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Nombre de usuario"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Nome de usu\xC3\xA1rio"
	#tag EndConstant

	#tag Constant, Name = kRefreshToLogin, Type = String, Dynamic = True, Default = \"Refresh to start a session on WERA DataViewer.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Refresh to start a session on WERA DataViewer."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Carga la p\xC3\xA1gina de nuevo para entrar al WERA DataViewer."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Atualizar para iniciar uma sess\xC3\xA3o em WERA DataViewer."
	#tag EndConstant


#tag EndWindowCode

#tag Events TxtUsername
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  if Details.KeyCode = 13 then
		    DoLogin
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TxtPassword
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  if Details.KeyCode = 13 then
		    DoLogin
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OKBtn
	#tag Event
		Sub Action()
		  DoLogin
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events CancelBtn
	#tag Event
		Sub Action()
		  Login.Lbl.Text=replace(kRefreshToLogin(cfgDV.language),"WERA",std.kRadName)
		  self.Hide
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Group="ID"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Minimum Size"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Minimum Size"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizable"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Behavior"
		InitialValue="Untitled"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Type"
		Visible=true
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"1 - Sheet"
			"2 - Palette"
			"3 - Modal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
