#tag WebPage
Begin WebContainer iCalendarEuro
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   200
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   310
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "CommonFont"
   TabOrder        =   0
   Top             =   70
   VerticalCenter  =   0
   Visible         =   True
   Width           =   192
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   193
      HelpTag         =   "#kSelectDate"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   True
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "954357759"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   188
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel DOW
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   16
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   0
      Left            =   13
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   "M"
      Top             =   18
      VerticalCenter  =   0
      Visible         =   True
      Width           =   26
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel DOW
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   16
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   1
      Left            =   38
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "T"
      Top             =   18
      VerticalCenter  =   0
      Visible         =   True
      Width           =   19
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel DOW
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   16
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   2
      Left            =   62
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   2
      Text            =   "W"
      Top             =   18
      VerticalCenter  =   0
      Visible         =   True
      Width           =   21
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel DOW
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   16
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   3
      Left            =   88
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   3
      Text            =   "T"
      Top             =   18
      VerticalCenter  =   0
      Visible         =   True
      Width           =   19
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel DOW
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   16
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   4
      Left            =   137
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   4
      Text            =   "S"
      Top             =   18
      VerticalCenter  =   0
      Visible         =   True
      Width           =   19
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel DOW
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   16
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   5
      Left            =   112
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   5
      Text            =   "F"
      Top             =   18
      VerticalCenter  =   0
      Visible         =   True
      Width           =   21
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel DOW
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   16
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   6
      Left            =   160
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   6
      Text            =   "S"
      Top             =   18
      VerticalCenter  =   0
      Visible         =   True
      Width           =   21
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator Separator1
      Cursor          =   0
      Enabled         =   True
      Height          =   2
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   35
      VerticalCenter  =   0
      Visible         =   True
      Width           =   170
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   0
      Left            =   154
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   7
      Text            =   "00"
      Top             =   35
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   1
      Left            =   9
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   8
      Text            =   "00"
      Top             =   54
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   2
      Left            =   32
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   9
      Text            =   "00"
      Top             =   54
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   3
      Left            =   58
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   10
      Text            =   "00"
      Top             =   54
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   4
      Left            =   82
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   11
      Text            =   "00"
      Top             =   54
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   5
      Left            =   106
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   12
      Text            =   "00"
      Top             =   54
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   6
      Left            =   131
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   13
      Text            =   "00"
      Top             =   54
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   7
      Left            =   154
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   14
      Text            =   "00"
      Top             =   53
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   8
      Left            =   9
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   15
      Text            =   "00"
      Top             =   75
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   9
      Left            =   32
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   16
      Text            =   "00"
      Top             =   75
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   10
      Left            =   58
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   17
      Text            =   "00"
      Top             =   75
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   11
      Left            =   82
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   18
      Text            =   "00"
      Top             =   75
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   12
      Left            =   106
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   19
      Text            =   "00"
      Top             =   75
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   13
      Left            =   131
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   20
      Text            =   "00"
      Top             =   75
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   14
      Left            =   154
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   21
      Text            =   "00"
      Top             =   75
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   15
      Left            =   9
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   22
      Text            =   "00"
      Top             =   96
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   16
      Left            =   32
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   23
      Text            =   "00"
      Top             =   96
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   17
      Left            =   58
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   24
      Text            =   "00"
      Top             =   96
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   18
      Left            =   82
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   25
      Text            =   "00"
      Top             =   96
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   19
      Left            =   106
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   26
      Text            =   "00"
      Top             =   96
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   20
      Left            =   131
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   27
      Text            =   "00"
      Top             =   96
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   21
      Left            =   154
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   28
      Text            =   "00"
      Top             =   96
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   22
      Left            =   9
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   29
      Text            =   "00"
      Top             =   116
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   23
      Left            =   32
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   30
      Text            =   "00"
      Top             =   116
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   24
      Left            =   58
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   31
      Text            =   "00"
      Top             =   116
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   25
      Left            =   82
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   32
      Text            =   "00"
      Top             =   116
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   26
      Left            =   106
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   33
      Text            =   "00"
      Top             =   116
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   27
      Left            =   131
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   34
      Text            =   "00"
      Top             =   116
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   28
      Left            =   154
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   35
      Text            =   "00"
      Top             =   116
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   29
      Left            =   9
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   36
      Text            =   "00"
      Top             =   135
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   30
      Left            =   31
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   37
      Text            =   "00"
      Top             =   135
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   31
      Left            =   58
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   38
      Text            =   "00"
      Top             =   135
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   32
      Left            =   82
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   39
      Text            =   "00"
      Top             =   135
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   33
      Left            =   106
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   40
      Text            =   "00"
      Top             =   135
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   34
      Left            =   131
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   41
      Text            =   "00"
      Top             =   135
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   35
      Left            =   153
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   42
      Text            =   "00"
      Top             =   135
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel iDay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   36
      Left            =   9
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   43
      Text            =   "00"
      Top             =   154
      VerticalCenter  =   0
      Visible         =   True
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lblMonth
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   "#kClickToSelectMonth"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   3
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1707350015"
      TabOrder        =   44
      Text            =   "Untitled"
      Top             =   -1
      VerticalCenter  =   0
      Visible         =   True
      Width           =   183
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebScrollBar ScrollBar1
      Cursor          =   0
      Enabled         =   True
      Height          =   11
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LineStep        =   1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Maximum         =   600
      Minimum         =   0
      PageStep        =   20
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   205
      Value           =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   184
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lblNextMonth
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   21
      HelpTag         =   "#kNextMonth"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   163
      LockBottom      =   False
      LockedInPosition=   True
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1968883711"
      TabOrder        =   45
      Text            =   ">"
      Top             =   165
      VerticalCenter  =   0
      Visible         =   True
      Width           =   23
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lblPrevMonth
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   21
      HelpTag         =   "#kPrevMonth"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1968883711"
      TabOrder        =   46
      Text            =   "<"
      Top             =   165
      VerticalCenter  =   0
      Visible         =   True
      Width           =   38
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lblPrevYear
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   21
      HelpTag         =   "#kPrevYear"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   True
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1968883711"
      TabOrder        =   47
      Text            =   "<<"
      Top             =   165
      VerticalCenter  =   0
      Visible         =   True
      Width           =   28
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lblNextYear
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   21
      HelpTag         =   "#kNextYear"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   101
      LockBottom      =   False
      LockedInPosition=   True
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1968883711"
      TabOrder        =   48
      Text            =   ">>"
      Top             =   165
      VerticalCenter  =   0
      Visible         =   True
      Width           =   28
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  
		  
		  dim i as integer
		  dim Today as new Date
		  
		  'set the month names
		  months=Array("",kJanuary(cfgDV.language),kFebruary(cfgDV.language),kMarch(cfgDV.language),kApril(cfgDV.language),kMay(cfgDV.language),kJune(cfgDV.language),kJuly(cfgDV.language),kAugust(cfgDV.language),kSeptember(cfgDV.language),kOctober(cfgDV.language),kNovember(cfgDV.language),kDecember(cfgDV.language))
		  
		  'get today's date
		  now=new date
		  
		  'show the month
		  ScrollBar1.Value=12*(now.year-1980) + now.month-1
		  
		  for i = 0 to 36
		    iDay(i).HelpTag=kSelectDate(cfgDV.language)  'Added as HelpTag
		    if iDay(i).Text = str(Today.Day) then
		      iDay(i).Style = DayPicked
		    else
		      iDay(i).Style = CenterDays
		    end if
		  next i
		  
		  //Localization of other help tags
		  lblMonth.HelpTag=kClickToSelectMonth(cfgDV.language)
		  lblNextMonth.HelpTag=kNextMonth(cfgDV.language)
		  lblNextYear.HelpTag=kNextYear(cfgDV.language)
		  lblPrevMonth.HelpTag=kPrevMonth(cfgDV.language)
		  lblPrevYear.HelpTag=kPrevYear(cfgDV.language)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub ShowMonth()
		  dim i, dayOrig as integer
		  
		  'clear day numbers
		  for i=0 to 36
		    iDay(i).Text=""
		    iDay(i).Style=CenterDays  // RG: To also activate the selection of day
		  next
		  
		  dayOrig=now.day // RG: I dont want to affect now.day value when calling this method that is why I store it first
		  
		  'set date from scrollbar value
		  now.day=1
		  now.Month=1+ScrollBar1.Value mod 12
		  now.Year=1980+ScrollBar1.Value\12
		  
		  'show month/year
		  lblMonth.Text=months(now.month) + " " + str(now.year)
		  
		  'display calendar
		  i=now.DayOfWeek-1
		  do
		    iDay(i).Text=str(now.Day)     'set day caption
		    
		    if now.Day=dayOrig then // RG: Update selected day
		      iDay(i).Style=DayPicked
		      SelectedDay=i
		    end
		    
		    now.day=now.day+1                           'next day
		    i=i+1                                                   'next text label
		  loop until now.day=1
		  
		  'we've gone on a month, so step back
		  now.month=now.month-1
		  
		  now.day=dayOrig // RG: returning value to original so that it remains unnafected
		  
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		months(12) As string
	#tag EndProperty

	#tag Property, Flags = &h0
		now As date
	#tag EndProperty

	#tag Property, Flags = &h0
		SelectedDay As Integer = 0
	#tag EndProperty


	#tag Constant, Name = kApril, Type = String, Dynamic = True, Default = \"April", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"April"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Abril"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Abril"
	#tag EndConstant

	#tag Constant, Name = kAugust, Type = String, Dynamic = True, Default = \"August", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"August"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Agosto"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Agosto"
	#tag EndConstant

	#tag Constant, Name = kClickToSelectMonth, Type = String, Dynamic = True, Default = \"Click to select month.", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Click to select month."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Clic para seleccionar mes."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Clique para selecionar o m\xC3\xAAs."
	#tag EndConstant

	#tag Constant, Name = kDecember, Type = String, Dynamic = True, Default = \"December", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"December"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Diciembre"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Dezembro"
	#tag EndConstant

	#tag Constant, Name = kFebruary, Type = String, Dynamic = True, Default = \"February", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"February"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Febrero"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Fevereiro"
	#tag EndConstant

	#tag Constant, Name = kJanuary, Type = String, Dynamic = True, Default = \"January", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"January"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Enero"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Janeiro"
	#tag EndConstant

	#tag Constant, Name = kJuly, Type = String, Dynamic = True, Default = \"July", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"July"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Julio"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Julho"
	#tag EndConstant

	#tag Constant, Name = kJune, Type = String, Dynamic = True, Default = \"June", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"June"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Junio"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Junho"
	#tag EndConstant

	#tag Constant, Name = kMarch, Type = String, Dynamic = True, Default = \"March", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"March"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Marzo"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Mar\xC3\xA7o"
	#tag EndConstant

	#tag Constant, Name = kMay, Type = String, Dynamic = True, Default = \"May", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"May"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mayo"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Maio"
	#tag EndConstant

	#tag Constant, Name = kNextMonth, Type = String, Dynamic = True, Default = \"Next Month", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Next Month"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mes siguiente"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"M\xC3\xAAs seguinte"
	#tag EndConstant

	#tag Constant, Name = kNextYear, Type = String, Dynamic = True, Default = \"Next Year", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Next Year"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"A\xC3\xB1o siguiente"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Ano seguinte"
	#tag EndConstant

	#tag Constant, Name = kNovember, Type = String, Dynamic = True, Default = \"November", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"November"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Noviembre"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Novembro"
	#tag EndConstant

	#tag Constant, Name = kOctober, Type = String, Dynamic = True, Default = \"October", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"October"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Octubre"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Outubro"
	#tag EndConstant

	#tag Constant, Name = kPrevMonth, Type = String, Dynamic = True, Default = \"Previous Month", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Previous Month"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mes anterior"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"M\xC3\xAAs anterior"
	#tag EndConstant

	#tag Constant, Name = kPrevYear, Type = String, Dynamic = True, Default = \"Previous Year", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Previous Year"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"A\xC3\xB1o anterior"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Ano anterior"
	#tag EndConstant

	#tag Constant, Name = kSelectDate, Type = String, Dynamic = True, Default = \"Select date.", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select date."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Selecciona fecha."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Selecionar data."
	#tag EndConstant

	#tag Constant, Name = kSeptember, Type = String, Dynamic = True, Default = \"September", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"September"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Septiembre"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Setembro"
	#tag EndConstant


#tag EndWindowCode

#tag Events iDay
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  'a day has been clicked
		  Dim sDay as integer
		  
		  SelectedDay = index
		  
		  if iDay(index).Text>"" then            'a valid day?
		    now.day=val(iDay(index).Text)   'set date
		    'self.hide
		  end if
		  
		  for sDay = 0 to 36
		    iDay(sDay).Style = CenterDays
		  next sDay
		  
		  iDay(SelectedDay).Style = DayPicked
		  
		  // Before, the RefreshLstAvailMeas method was at the MouseDown event of the WebCalendar object. When clicking on a day, both MouseDown event of WebCalendar, and MouseDown event of iDay() were fired.
		  // Sometimes , RefreshLstAvailMeas method inside MouseDown event of WebCalendar was executed before iDay(SelectedDay).Style = DayPicked was executed in MouseDown event of iDay().
		  // This resulted in refreshing LstAvailMeas for measurements of the previously selected day, and not for the actual day that was clicked. Doing everything here will ensure the correct order of execution and correct displaying of available measurements in DB.
		  if Main.OceanDataPage.Enabled then
		    Main.OceanDataPage.RefreshLstAvailMeas
		    Main.OceanDataPage.RefreshMap
		  else
		    main.TimeDataPage.RefreshLstAvailMeas
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lblMonth
	#tag Event
		Sub Shown()
		  Dim menu As New WebMenuItem
		  
		  menu.Append(New WebMenuItem(kJanuary(cfgDV.language)))
		  menu.Append(New WebMenuItem(kFebruary(cfgDV.language)))
		  menu.Append(New WebMenuItem(kMarch(cfgDV.language)))
		  menu.Append(New WebMenuItem(kApril(cfgDV.language)))
		  menu.Append(New WebMenuItem(kMay(cfgDV.language)))
		  menu.Append(New WebMenuItem(kJune(cfgDV.language)))
		  menu.Append(New WebMenuItem(kJuly(cfgDV.language)))
		  menu.Append(New WebMenuItem(kAugust(cfgDV.language)))
		  menu.Append(New WebMenuItem(kSeptember(cfgDV.language)))
		  menu.Append(New WebMenuItem(kOctober(cfgDV.language)))
		  menu.Append(New WebMenuItem(kNovember(cfgDV.language)))
		  menu.Append(New WebMenuItem(kDecember(cfgDV.language)))
		  
		  Me.ContextualMenu = menu
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  me.PresentContextualMenu
		End Sub
	#tag EndEvent
	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  Dim newDate as new Date
		  Dim Month as integer
		  newDate=now
		  
		  dim s as string = Item.Text
		  
		  if s=kJanuary(cfgDV.language) then
		    Month=1
		  elseif s=kFebruary(cfgDV.language) then
		    Month=2
		  elseif s=kMarch(cfgDV.language) then
		    Month=3
		  elseif s=kApril(cfgDV.language) then
		    Month=4
		  elseif s=kMay(cfgDV.language) then
		    Month=5
		  elseif s=kJune(cfgDV.language) then
		    Month=6
		  elseif s=kJuly(cfgDV.language) then
		    Month=7
		  elseif s=kAugust(cfgDV.language) then
		    Month=8
		  elseif s=kSeptember(cfgDV.language) then
		    Month=9
		  elseif s=kOctober(cfgDV.language) then
		    Month=10
		  elseif s=kNovember(cfgDV.language) then
		    Month=11
		  else
		    Month=12
		  end
		  
		  
		  ScrollBar1.Value=(newDate.Year-1980)*12 + Month - 1
		  ShowMonth
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ScrollBar1
	#tag Event
		Sub ValueChanged()
		  'change month/year
		  ShowMonth
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lblNextMonth
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ScrollBar1.Value = scrollbar1.Value + 1
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lblPrevMonth
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ScrollBar1.Value = scrollbar1.Value - 1
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lblPrevYear
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ScrollBar1.Value = scrollbar1.Value - 12
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lblNextYear
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ScrollBar1.Value = scrollbar1.Value + 12
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="SelectedDay"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
