#tag WebStyle
WebStyle StyContainer
Inherits WebStyle
	#tag WebStyleStateGroup
		text-font=Verdana,Arial,sans-serif,Helvetica
		text-size=1em
		text-color=B2B2B2FF
		misc-background=solid 444444FF
		corner-topleft=15px
		corner-bottomleft=15px
		corner-bottomright=15px
		corner-topright=15px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle StyContainer
#tag EndWebStyle

