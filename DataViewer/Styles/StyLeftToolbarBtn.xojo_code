#tag WebStyle
WebStyle StyLeftToolbarBtn
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid 444444FF
		corner-topleft=7px
		corner-bottomleft=7px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-opacity=0.5
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle StyLeftToolbarBtn
#tag EndWebStyle

