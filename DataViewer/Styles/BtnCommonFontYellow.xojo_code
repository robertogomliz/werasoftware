#tag WebStyle
WebStyle BtnCommonFontYellow
Inherits WebStyle
	#tag WebStyleStateGroup
		text-font=Verdana,Arial,Helvetica,sans-serif
		text-size=1em
		text-color=EBEBEBFF
		misc-background=gradient vertical 0 FFC000FF 1 576C82FF
		text-decoration=True false false false false
		corner-topleft=15px
		corner-bottomleft=15px
		corner-bottomright=15px
		corner-topright=15px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle BtnCommonFontYellow
#tag EndWebStyle

