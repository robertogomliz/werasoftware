#tag WebStyle
WebStyle StyLeftToolbarBtnSelected
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=gradient vertical 0 6892C4FF 0.5864662 28599BFF 0.4360902 4574ACFF 1 1A3C6AFF
		corner-topleft=7px
		corner-bottomleft=7px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-opacity=0.5
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle StyLeftToolbarBtnSelected
#tag EndWebStyle

