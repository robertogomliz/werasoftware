#tag WebStyle
WebStyle SmallFontRight
Inherits WebStyle
	#tag WebStyleStateGroup
		text-font=Verdana,sans-serif,Helvetica,Arial
		text-size=0.75em
		text-align=right
		text-color=B2B2B2FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle SmallFontRight
#tag EndWebStyle

