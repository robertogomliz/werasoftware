#tag WebStyle
WebStyle SmallFont
Inherits WebStyle
	#tag WebStyleStateGroup
		text-font=Verdana,Helvetica,Arial,sans-serif
		text-size=0.75em
		text-color=B2B2B2FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle SmallFont
#tag EndWebStyle

