#tag WebStyle
WebStyle BtnSmallFont
Inherits WebStyle
	#tag WebStyleStateGroup
		text-font=Verdana,Arial,sans-serif,Helvetica
		text-size=0.75em
		text-color=EBEBEBFF
		misc-background=gradient vertical 0 798CA5FF 1 576C82FF
		text-decoration=True false false false false
		corner-topleft=15px
		corner-bottomleft=15px
		corner-bottomright=15px
		corner-topright=15px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle BtnSmallFont
#tag EndWebStyle

