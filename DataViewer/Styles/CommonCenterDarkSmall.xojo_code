#tag WebStyle
WebStyle CommonCenterDarkSmall
Inherits WebStyle
	#tag WebStyleStateGroup
		text-font=Verdana,Arial,sans-serif,Helvetica
		text-align=center
		text-size=0.75em
		text-color=444444FF
		corner-topleft=8px
		corner-bottomleft=8px
		corner-bottomright=8px
		corner-topright=8px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle CommonCenterDarkSmall
#tag EndWebStyle

