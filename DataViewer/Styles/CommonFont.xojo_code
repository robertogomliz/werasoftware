#tag WebStyle
WebStyle CommonFont
Inherits WebStyle
	#tag WebStyleStateGroup
		text-font=Verdana,Arial,sans-serif,Helvetica
		text-size=1em
		text-color=B2B2B2FF
		misc-background=solid 444444FF
		corner-topleft=8px
		corner-bottomleft=8px
		corner-bottomright=8px
		corner-topright=8px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle CommonFont
#tag EndWebStyle

