#tag WebStyle
WebStyle BtnCommonFontDisabled
Inherits WebStyle
	#tag WebStyleStateGroup
		text-font=Verdana,Arial,sans-serif,Helvetica
		text-size=1em
		text-color=DCDCDC64
		misc-background=gradient vertical 0 3E5D7D64 1 0F2949FF
		text-decoration=False True false false false
		corner-topleft=15px
		corner-bottomleft=15px
		corner-bottomright=15px
		corner-topright=15px
		misc-opacity=0.5019608
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle BtnCommonFontDisabled
#tag EndWebStyle

