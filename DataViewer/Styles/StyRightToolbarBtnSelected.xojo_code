#tag WebStyle
WebStyle StyRightToolbarBtnSelected
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=gradient vertical 0 F7F7EEFF 1 BFB4B2FF
		text-color=444444FF
		corner-topright=7px
		corner-bottomright=7px
		text-decoration=True false false false false
		border-left=10px solid B6B6B600
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-opacity=0.5
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle StyRightToolbarBtnSelected
#tag EndWebStyle

