#tag WebStyle
WebStyle StyRightToolbarBtn
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid DCDCDCFF
		text-color=B2B2B2FF
		border-left=10px solid B6B6B600
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-opacity=0.5
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle StyRightToolbarBtn
#tag EndWebStyle

