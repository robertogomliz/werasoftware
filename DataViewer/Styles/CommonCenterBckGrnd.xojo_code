#tag WebStyle
WebStyle CommonCenterBckGrnd
Inherits WebStyle
	#tag WebStyleStateGroup
		text-font=Helvetica,Arial,sans-serif,Verdana
		text-size=1em
		misc-background=solid 005192FF
		text-align=center
		text-color=FFFFFFFF
		corner-topleft=5px
		corner-bottomleft=5px
		corner-bottomright=5px
		corner-topright=5px
		text-decoration=True false false false false
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle CommonCenterBckGrnd
#tag EndWebStyle

