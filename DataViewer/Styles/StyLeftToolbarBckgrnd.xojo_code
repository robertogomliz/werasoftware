#tag WebStyle
WebStyle StyLeftToolbarBckgrnd
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid 444444FF
		border-left=1px solid 444444FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-opacity=0.5
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle StyLeftToolbarBckgrnd
#tag EndWebStyle

