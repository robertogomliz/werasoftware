#tag WebStyle
WebStyle BtnSmallFontDisabled
Inherits WebStyle
	#tag WebStyleStateGroup
		text-font=Verdana,Arial,sans-serif,Helvetica
		text-size=0.75em
		text-color=EBEBEBFF
		misc-background=gradient vertical 0 6A7D9680 1 485D7340
		text-decoration=True false false false false
		corner-topleft=15px
		corner-bottomleft=15px
		corner-bottomright=15px
		corner-topright=15px
		misc-opacity=0.5
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle BtnSmallFontDisabled
#tag EndWebStyle

