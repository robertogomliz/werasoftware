#tag WebStyle
WebStyle BtnCommonFontRed
Inherits WebStyle
	#tag WebStyleStateGroup
		text-font=Verdana,Arial,sans-serif,Helvetica
		text-size=1em
		text-color=EBEBEBFF
		misc-background=gradient vertical 0 FF0000FF 1 576C82FF
		text-decoration=True false false false false
		corner-topleft=15px
		corner-bottomleft=15px
		corner-bottomright=15px
		corner-topright=15px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle BtnCommonFontRed
#tag EndWebStyle

