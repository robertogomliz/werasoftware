#tag Module
Protected Module access
	#tag Method, Flags = &h0
		Function LoadAccounts() As boolean
		  // Loads the usernames and passwords
		  
		  // Opening access.cfg file =======================================================
		  Dim f As New FolderItem
		  
		  f = GetFolderItem(App.execDir + "access.cfg")
		  
		  Dim configFile As TextInputStream
		  
		  If f <> Nil Then
		    If f.Exists Then
		      // Be aware that TextInputStream.Open coud raise an exception
		      Try
		        configFile = TextInputStream.Open(f)
		        configFile.Encoding = Encodings.ASCII
		      Catch e As IOException
		        configFile.Close
		        log.Write(true,"Error accessing username and passwords file.")
		        return false
		      End Try
		    Else
		      log.Write(true,"Username and passwords file does not exists.")
		      return false
		    End If
		  Else
		    log.Write(true,"Username and passwords file is 'Nil'. Make sure folder exists.")
		    return false
		  End If
		  
		  
		  // Reading access.cfg file =======================================================
		  
		  Dim auxString, auxStringArray(-1), linesOfTxtFile(-1) As String
		  Dim cnt, i as Integer
		  
		  linesOfTxtFile=Split(configFile.ReadAll,EndOfLine)  // Split the text content into lines
		  
		  cnt=0
		  ReDim passwords(-1)
		  ReDim usernames(-1)
		  for i=0 to linesOfTxtFile.Ubound
		    
		    auxString=left(Trim(linesOfTxtFile(i)),1)  // Getting the first written character to see if it is only a comment or a blank line
		    
		    if auxString="/" or auxString="!" or auxString="%" or auxString="" then
		      continue
		    else
		      auxStringArray=split(Trim(linesOfTxtFile(i)),Chr(9))
		      if auxStringArray.Ubound<>1 then
		        log.Write(false,"Invalid username-password: " + Trim(linesOfTxtFile(i)))
		        continue
		      else
		        usernames.Append(auxStringArray(0))
		        passwords.Append(auxStringArray(1))
		        continue
		      end
		    end
		  next
		  
		  return false
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		passwords(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		usernames(-1) As string
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
