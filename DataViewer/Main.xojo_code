#tag WebPage
Begin WebPage Main
   Compatibility   =   ""
   Cursor          =   2
   Enabled         =   True
   Height          =   930
   HelpTag         =   ""
   HorizontalCenter=   0
   ImplicitInstance=   True
   Index           =   0
   IsImplicitInstance=   False
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   LockVertical    =   False
   MinHeight       =   930
   MinWidth        =   1218
   Style           =   "WindowStyle"
   TabOrder        =   0
   Title           =   "WERA DataViewer"
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1218
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _ImplicitInstance=   False
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WSConfig WSConfigPage
      Cursor          =   0
      Enabled         =   True
      Height          =   823
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   194
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      selectedProbe   =   0
      Style           =   "1897781247"
      TabOrder        =   17
      Top             =   90
      VerticalCenter  =   0
      Visible         =   True
      Width           =   999
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin TimeData TimeDataPage
      cancelLoading   =   False
      Cursor          =   0
      dataTypeID      =   0
      Enabled         =   False
      flagToAvoidExceptionDueToBug=   False
      Height          =   823
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      ix              =   0
      iy              =   0
      lat             =   0.0
      Left            =   194
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      lon             =   0.0
      minutesInBetween=   0.0
      qualFilterLevel =   0
      Scope           =   0
      ScrollbarsVisible=   0
      SelectedMeas    =   ""
      Style           =   "1897781247"
      TabOrder        =   14
      Top             =   90
      usingSynthBuoy  =   0
      VerticalCenter  =   0
      Visible         =   False
      Width           =   999
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin OceanData OceanDataPage
      animDelay       =   500
      cancelLoading   =   False
      clickMarkVisible=   False
      clickMarkX      =   0
      clickMarkY      =   0
      Cursor          =   0
      dataTypeID      =   0
      Enabled         =   False
      Height          =   823
      HelpTag         =   ""
      HorizontalCenter=   0
      IdGridCurrentPict=   0
      IdHeaderCurrentPict=   0
      Index           =   -2147483648
      lat             =   0.0
      Left            =   194
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      lon             =   0.0
      qualiAcronym    =   ""
      qualLevel       =   0
      Scope           =   0
      ScrollbarsVisible=   0
      sFolder         =   ""
      sSufix          =   ""
      Style           =   "1897781247"
      TabOrder        =   8
      tmpSufix        =   ".cur_asc"
      Top             =   90
      VerticalCenter  =   0
      Visible         =   False
      Width           =   999
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin RadarData RadarDataPage
      Cursor          =   0
      Enabled         =   False
      Height          =   823
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   194
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1897781247"
      TabOrder        =   6
      Top             =   90
      VerticalCenter  =   0
      Visible         =   False
      Width           =   999
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin Home HomePage
      canvasVisible   =   False
      clickMarkVisible=   False
      clickMarkX      =   0
      clickMarkY      =   0
      currentPictureType=   0
      Cursor          =   0
      Enabled         =   True
      GACurMax        =   0.0
      GAWavMax        =   0.0
      Height          =   823
      HelpTag         =   ""
      HorizontalCenter=   0
      IdHeaderCurrentPict=   0
      Index           =   -2147483648
      indexAlertSelected=   0
      Left            =   194
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pathLoading     =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      sFolder         =   ""
      sSufix          =   ""
      statusAlertCur  =   1
      statusAlertWav  =   1
      statusAlertWin  =   1
      Style           =   "1897781247"
      TabOrder        =   5
      timerCounter    =   10
      timeStampGlobalAlertCur=   ""
      timeStampGlobalAlertWav=   ""
      Top             =   90
      VerticalCenter  =   0
      Visible         =   True
      Width           =   999
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel CopyRightNotice
      Cursor          =   2
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   52
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1137727487"
      TabOrder        =   1
      Text            =   "#kAbout"
      Top             =   897
      VerticalCenter  =   0
      Visible         =   True
      Width           =   94
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLink HelzelLink
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1821235199"
      TabOrder        =   9
      Target          =   1
      Text            =   "#std.kComName"
      Top             =   515
      URL             =   "#std.kWebPage"
      VerticalCenter  =   0
      Visible         =   True
      Width           =   188
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel RadarName
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   60
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   15
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   True
      Scope           =   0
      Style           =   "1939664895"
      TabOrder        =   12
      Text            =   "Long Bay System somewhere and third line g"
      Top             =   97
      VerticalCenter  =   0
      Visible         =   True
      Width           =   164
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView HelzelLogo
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   52
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   9
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   830935039
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   466
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   177
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTimer KeepDBAlive
      Cursor          =   0
      Enabled         =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Mode            =   2
      Period          =   300000
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Top             =   20
      VerticalCenter  =   0
      Visible         =   True
      Width           =   32
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin PageNavi PageNavi1
      Cursor          =   0
      Enabled         =   True
      Height          =   260
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   9
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "84463615"
      TabOrder        =   15
      Top             =   188
      VerticalCenter  =   0
      Visible         =   True
      Width           =   179
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView upperBar
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   89
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   545687551
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   0
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1218
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView SystemStatusPicto
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   64
      HelpTag         =   "Indicator of status of WERA system."
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   61
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   25876479
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   591
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   64
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator Separator1
      Cursor          =   0
      Enabled         =   True
      Height          =   2
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   15
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   160
      VerticalCenter  =   0
      Visible         =   True
      Width           =   164
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label1
      Cursor          =   2
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   18
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1109
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "332994559"
      TabOrder        =   16
      Text            =   "#kHelp"
      Top             =   13
      VerticalCenter  =   0
      Visible         =   True
      Width           =   74
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin Thread ParallelThread
      Height          =   32
      Index           =   -2147483648
      Left            =   20
      LockedInPosition=   False
      Priority        =   5
      Scope           =   0
      StackSize       =   0
      Style           =   "0"
      TabPanelIndex   =   0
      Top             =   20
      Width           =   32
   End
   Begin WebImageView ImageView3
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   18
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1087
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   413085695
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   14
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   19
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator Separator2
      Cursor          =   0
      Enabled         =   True
      Height          =   2
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   15
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   897
      VerticalCenter  =   0
      Visible         =   True
      Width           =   164
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel CopyRightNotice1
      Cursor          =   2
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   12
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "134051839"
      TabOrder        =   18
      Text            =   "WERA  DataViewer"
      Top             =   875
      VerticalCenter  =   0
      Visible         =   True
      Width           =   170
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel CopyRightNotice2
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   11
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   19
      Text            =   "#kSystemAdmin"
      Top             =   452
      VerticalCenter  =   0
      Visible         =   True
      Width           =   150
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView AlertPicto
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   64
      HelpTag         =   "Indicator for extreme oceanographic measurements."
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   61
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1503197183
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   696
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   64
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel SystemStatusLabel
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   16
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   41
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   20
      Text            =   "System status:"
      Top             =   576
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel AlertLabel
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   16
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   41
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   21
      Text            =   "Ocean alert:"
      Top             =   681
      VerticalCenter  =   0
      Visible         =   False
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTimer UpdateToServer
      Cursor          =   0
      Enabled         =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Mode            =   2
      Period          =   15000
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Top             =   20
      VerticalCenter  =   0
      Visible         =   True
      Width           =   32
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  RadarName.Text=cfgDV.systemName
		  CopyRightNotice.Text=kAbout(cfgDV.language)
		  CopyRightNotice1.Text=std.kRadName + " DataViewer"
		  CopyRightNotice2.Text=kSystemAdmin(cfgDV.language)
		  SystemStatusLabel.Text=kSystemStatus(cfgDV.language)
		  SystemStatusPicto.HelpTag=replace(kHelpTagStatus(cfgDV.language),"WERA",std.kRadName)
		  AlertLabel.Text=kMaritimeAlert(cfgDV.language)
		  AlertPicto.HelpTag=kHelpTagMaritimeAlert(cfgDV.language)
		  Title=cfgDV.titleInBrowser
		  Label1.Text=kHelp(cfgDV.language)
		  
		  
		  if App.OceanWarningSystemEnabled then
		    AlertLabel.Visible=true
		    AlertPicto.Visible=true
		  end
		  
		  // Changing to customized logo for system administrator
		  if cfgDV.ci_NaviBarLogo<>"default" then
		    dim FI as FolderItem
		    Dim Pict1 as WebPicture
		    Dim Pict2 as Picture
		    dim offset as integer
		    
		    FI=GetFolderItem(App.execDir + cfgDV.ci_NaviBarLogo)
		    
		    if FI.Exists then
		      Pict1=new WebPicture(FI)
		      if Pict1 = Nil then
		        log.Write(true,"Could not load customized logo! Leaving default.")
		      else
		        //Checking height of logo and move accordingly
		        Pict2=new Picture(5,5)
		        Pict2=Picture.Open(FI)
		        if Pict2=Nil then
		          offset=0
		        else
		          offset=Pict2.Height - HelzelLogo.Height
		        end
		        
		        HelzelLogo.Height=HelzelLogo.Height + offset
		        HelzelLogo.Picture=Pict1
		        if cfgDV.ci_linkText<>"default" then
		          HelzelLink.Top=HelzelLink.Top + offset
		          HelzelLink.Text=cfgDV.ci_linkText
		        end
		        if cfgDV.ci_link<>"default" then
		          HelzelLink.URL="http://"+cfgDV.ci_link    //The http:// has to be added here and not configured, since parametes file reads // as comment.
		        end
		        AlertLabel.Top=AlertLabel.Top+offset
		        AlertPicto.Top=AlertPicto.Top+offset
		        SystemStatusLabel.Top=SystemStatusLabel.Top+offset
		        SystemStatusPicto.Top=SystemStatusPicto.Top+offset
		      end
		    else
		      log.Write(true,"Could not find customized logo! Leaving default.")
		    end
		  end if
		  
		  
		  
		  
		  // Changing to customized upper bar
		  if cfgDV.ci_headerImage<>"default" then
		    dim FI as FolderItem
		    Dim Pict1 as WebPicture
		    
		    FI=GetFolderItem(App.execDir + cfgDV.ci_headerImage)
		    
		    if FI.Exists then
		      Pict1=new WebPicture(FI)
		      if Pict1 = Nil then
		        log.Write(true,"Could not load customized upper bar! Leaving default.")
		      else
		        upperBar.Picture=Pict1
		      end
		    else
		      log.Write(true,"Could not find customized upper bar! Leaving default.")
		    end
		  end if
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  'dim script as string
		  '
		  'script = "var elt = document.getElementById('" + OceanDataPage.OceanImage.ControlID + "_image');" + EndOfLine + _
		  '"elt.style.left = '0px';" + EndOfLine + _
		  '"elt.style.top = '0px';" + EndOfLine + _
		  '"elt.style.marginTop = '0px';" + EndOfLine + _
		  '"elt.style.marginLeft = '0px';" + EndOfLine + _
		  '"elt.style.width = '" + str(OceanDataPage.OceanImage.Width, "0") + "px';" + EndOfLine + _
		  '"elt.style.height = '" + str(OceanDataPage.OceanImage.Height, "0") + "px';"
		  '
		  'ExecuteJavaScript(script)
		  '
		  '
		  'script = "var elt = document.getElementById('" + HomePage.ImgHome.ControlID + "_image');" + EndOfLine + _
		  '"elt.style.left = '0px';" + EndOfLine + _
		  '"elt.style.top = '0px';" + EndOfLine + _
		  '"elt.style.marginTop = '0px';" + EndOfLine + _
		  '"elt.style.marginLeft = '0px';" + EndOfLine + _
		  '"elt.style.width = '" + str(HomePage.ImgHome.Width, "0") + "px';" + EndOfLine + _
		  '"elt.style.height = '" + str(HomePage.ImgHome.Height, "0") + "px';"
		  '
		  'ExecuteJavaScript(script)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Shown()
		  
		  'MsgBox(Session.Identifier)
		  
		  PageNavi1.HomeClicked   // Activate home page at the beginning
		  
		  UpdateToServerMethod
		  
		  if DebugBuild then
		    MsgBox("Oh... I am running in debug mode. Do not forget to update the version notes (look at App.VersionNotes)")
		  end
		  
		  if sm.warningAvail(0) or sm.warningAvail(1) or (sm.extraWarnings<>EndOfLine) then
		    RadarDataPage.StationStatus.Visible=true
		    RadarDataPage.StationStatusLabel.Visible=true
		    SystemStatusPicto.Picture=pwr_o
		    SystemStatusPicto.Cursor=2
		    SystemStatusLabel.Style=CommonCenterDarkSmall
		    SystemStatusLabel.cursor=2
		    
		  else
		    RadarDataPage.StationStatus.Visible=false
		    RadarDataPage.StationStatusLabel.Visible=false
		    SystemStatusPicto.Picture=pwr_a
		    SystemStatusPicto.Cursor=0
		    SystemStatusLabel.Style=SmallFont
		    SystemStatusLabel.cursor=0
		    
		  end if
		  
		  
		  if sm.warningAvail(0) then
		    RadarDataPage.StationsTable.Cell(0,12)="(!)"
		    RadarDataPage.StationsTable.CellStyle(0,12)=CommonCenterSmallRed
		  else
		    RadarDataPage.StationsTable.Cell(0,12)="OK"
		    RadarDataPage.StationsTable.CellStyle(0,12)=CommonCenterSmall
		  end
		  
		  if cfgDV.nStations>1 then
		    if sm.warningAvail(1) then
		      RadarDataPage.StationsTable.Cell(1,12)="(!)"
		      RadarDataPage.StationsTable.CellStyle(1,12)=CommonCenterSmallRed
		    else
		      RadarDataPage.StationsTable.Cell(1,12)="OK"
		      RadarDataPage.StationsTable.CellStyle(1,12)=CommonCenterSmall
		    end
		  end
		  
		  //Update ocean alert status indicator
		  if ws.statusCur>1 or ws.statusWav>1 or ws.statusWin>1 or ws.globalAlertCurRedX.Ubound>=0 or ws.globalAlertCurYellowX.Ubound>=0 or ws.globalAlertWavRedX.Ubound>=0 or ws.globalAlertWavYellowX.Ubound>=0 then
		    AlertPicto.Picture=warn_a
		    AlertPicto.Cursor=2
		    AlertLabel.Cursor=2
		    AlertLabel.Style=CommonCenterSmallRed
		  else
		    AlertPicto.Picture=warn_i
		    AlertPicto.Cursor=0
		    AlertLabel.Cursor=0
		    AlertLabel.Style=SmallFont
		  end
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function CreateCDL(data(,,,) as single, dates() as date, LstIdx as integer) As FolderItem
		  Dim lineString as String
		  Dim file As FolderItem
		  Dim fileStream As TextOutputStream
		  Dim i, j, k, l, nt, nx, ny, nv as Integer
		  Dim tab, quotes, auxStr as string
		  Dim auxInt as Int32
		  
		  tab=Chr(9)
		  quotes=Chr(34)
		  
		  nt=UBound(data,2) + 1
		  nx=UBound(data,4) + 1
		  ny=UBound(data,3) + 1
		  nv=UBound(data,1) + 1  //number of variables measured
		  
		  Redim varNames(-1)
		  
		  //================ Creating file ===================================================
		  
		  
		  // When multiple users try to make a NetCDF file sometimes TextOutputStream.Create gives an IOException with error number = 11
		  // It turns out that the description for error number=11 is "Try Again"... not very descriptive... but anyways we try again up to 5 times.
		  Dim success as Boolean
		  i=0
		  for i=1 to 5
		    file = GetFolderItem(App.execDir + "CDL_file" + CStr(App.sessionsIDs.IndexOf(Session.Identifier)) + ".cdl")
		    If file <> Nil Then
		      success=true
		      Try
		        fileStream = TextOutputStream.Create(file)
		      Catch e as IOException
		        success=false
		        i=i+1
		      End Try
		      
		      If success then
		        exit
		      elseif i>5 then
		        log.Write(false,"Could not create textoutputstream for CDL file.")
		        MsgBox(kPleaseTryAgain(cfgDV.language))
		        
		        'Trying to exit without downloading nothing... first check in which page we are
		        if OceanDataPage.Enabled then
		          OceanDataPage.cancelLoading=true
		        else
		          TimeDataPage.cancelLoading=true
		        end
		        return(file)
		      end
		      
		    else
		      log.Write(true,"Not able to create folderItem for CDL file. It is Nil.")
		    End If
		    
		    
		  next
		  
		  
		  fileStream.WriteLine("netcdf CDL_file.cdl {")
		  
		  fileStream.WriteLine("dimensions:")     //===============================  dimensions
		  
		  fileStream.WriteLine(tab + "time = " + Str(nt)) + " ;"  // should it be unlimited???
		  fileStream.WriteLine(tab + "lon = " + Str(nx)) + " ;"
		  fileStream.WriteLine(tab + "lat = " + Str(ny)) + " ;"
		  fileStream.WriteLine(tab + "z = 1") + " ;"
		  
		  fileStream.WriteLine("variables:")     //===============================  variables
		  
		  fileStream.WriteLine(tab + "int time(time) ;")
		  fileStream.WriteLine(tab + tab + "time:standard_name = " + quotes + "time" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "time:long_name = " + quotes + "time" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "time:units = " + quotes + "sec since 1970-1-1 00:00:00" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "time:axis = " + quotes + "T" + quotes + " ;")
		  
		  fileStream.WriteLine(tab + "float lon(lon) ;")
		  fileStream.WriteLine(tab + tab + "lon:standard_name = " + quotes + "longitude" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "lon:long_name = " + quotes + "longitude" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "lon:units = " + quotes + "degrees_east" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "lon:reference = " + quotes + "geographical coordinates" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "lon:axis = " + quotes + "X" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "lon:valid_range = -180., 180. ;")
		  
		  fileStream.WriteLine(tab + "float lat(lat) ;")
		  fileStream.WriteLine(tab + tab + "lat:standard_name = " + quotes + "latitude" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "lat:long_name = " + quotes + "latitude" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "lat:units = " + quotes + "degrees_north" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "lat:reference = " + quotes + "geographical coordinates" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "lat:axis = " + quotes + "Y" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "lat:valid_range = -90., 90. ;")
		  
		  fileStream.WriteLine(tab + "float z(z) ;")
		  fileStream.WriteLine(tab + tab + "z:standard_name = " + quotes + "height" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "z:long_name = " + quotes + "height" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "z:units = " + quotes + "meter" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "z:reference = " + quotes + "mean sea level (MSL)" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "z:positive = " + quotes + "up" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "z:axis = " + quotes + "Z" + quotes + " ;")
		  
		  // measured variables metadata
		  Select Case LstIdx
		  case 100  // Special case of creating cdf file of single point in time
		    
		    if TimeDataPage.pmSymbols.Text=TimeData.kStickPlot(cfgDV.language) then
		      
		      Declare_water_spd(fileStream)
		      Declare_water_dir(fileStream)
		      
		    else
		      auxStr=TimeDataPage.pmDataType.Text
		      if auxStr = App.kNameCCVAbs(cfgDV.language) then
		        Declare_water_spd(fileStream)
		      elseif auxStr = App.kNameCCVDir(cfgDV.language) then
		        Declare_water_dir(fileStream)
		      elseif auxStr = App.kNameCCVE(cfgDV.language) then
		        Declare_water_u(fileStream)
		      elseif auxStr = App.kNameCCVN(cfgDV.language) then
		        Declare_water_v(fileStream)
		      elseif auxStr = (App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVE(cfgDV.language))) then
		        Declare_water_u_acc(fileStream)
		      elseif auxStr = (App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVN(cfgDV.language))) then
		        Declare_water_v_acc(fileStream)
		      elseif auxStr = App.kNameCSWHVal(cfgDV.language) then
		        Declare_sig_wv_ht(fileStream)
		      elseif auxStr = App.kNameCSWHDir(cfgDV.language) then
		        Declare_sig_wv_dir(fileStream)
		      elseif auxStr = App.kNameWD(cfgDV.language) then
		        Declare_wdir(fileStream)
		      elseif auxStr = App.kNameWS(cfgDV.language) then
		        Declare_wspd(fileStream)
		      elseif auxStr = (App.kNameCSWHVal(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		        Declare_sig_wv_ht(fileStream)
		      elseif auxStr = (App.kNameCSWHDir(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		        Declare_sig_wv_dir(fileStream)
		      elseif auxStr = (App.kNameWD(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		        Declare_wdir(fileStream)
		      elseif auxStr = (App.kNameWS(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		        Declare_wspd(fileStream)
		      else
		        
		        for i=0 to cfgDV.nStations-1
		          if auxStr = (siteInfo.sname(i) + " - " + App.kNameRC(cfgDV.language)) then
		            Declare_vel(fileStream)
		          elseif auxStr = (siteInfo.sname(i) + " - " + App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameRC(cfgDV.language))) then
		            Declare_acc(fileStream)
		          end
		        next
		      end
		      
		      if TimeDataPage.cb2ndplot.Value then
		        
		        auxStr=TimeDataPage.pmDataType2.Text
		        if auxStr = App.kNameCCVAbs(cfgDV.language) then
		          Declare_water_spd(fileStream)
		        elseif auxStr = App.kNameCCVDir(cfgDV.language) then
		          Declare_water_dir(fileStream)
		        elseif auxStr = App.kNameCCVE(cfgDV.language) then
		          Declare_water_u(fileStream)
		        elseif auxStr = App.kNameCCVN(cfgDV.language) then
		          Declare_water_v(fileStream)
		        elseif auxStr = (App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVE(cfgDV.language))) then
		          Declare_water_u_acc(fileStream)
		        elseif auxStr = (App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVN(cfgDV.language))) then
		          Declare_water_v_acc(fileStream)
		        elseif auxStr = App.kNameCSWHVal(cfgDV.language) then
		          Declare_sig_wv_ht(fileStream)
		        elseif auxStr = App.kNameCSWHDir(cfgDV.language) then
		          Declare_sig_wv_dir(fileStream)
		        elseif auxStr = App.kNameWD(cfgDV.language) then
		          Declare_wdir(fileStream)
		        elseif auxStr = App.kNameWS(cfgDV.language) then
		          Declare_wspd(fileStream)
		        elseif auxStr = (App.kNameCSWHVal(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		          Declare_sig_wv_ht(fileStream)
		        elseif auxStr = (App.kNameCSWHDir(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		          Declare_sig_wv_dir(fileStream)
		        elseif auxStr = (App.kNameWD(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		          Declare_wdir(fileStream)
		        elseif auxStr = (App.kNameWS(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		          Declare_wspd(fileStream)
		        else
		          
		          for i=0 to cfgDV.nStations-1
		            if auxStr = (siteInfo.sname(i) + " - " + App.kNameRC(cfgDV.language)) then
		              Declare_vel(fileStream)
		            elseif auxStr = (siteInfo.sname(i) + " - " + App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameRC(cfgDV.language))) then
		              Declare_acc(fileStream)
		            end
		          next
		        end
		      end
		      
		    end
		    
		  case 0
		    
		    Declare_water_u(fileStream)
		    Declare_water_v(fileStream)
		    Declare_water_u_acc(fileStream)
		    Declare_water_v_acc(fileStream)
		    
		  case 10
		    
		    Declare_sig_wv_ht(fileStream)
		    Declare_sig_wv_dir(fileStream)
		    
		  case 20
		    
		    Declare_wspd(fileStream)
		    Declare_wdir(fileStream)
		    
		  case 1
		    
		    Declare_vel(fileStream)
		    Declare_acc(fileStream)
		    Declare_var(fileStream)
		    Declare_pow(fileStream)
		    
		  End select
		  
		  // global attributes
		  fileStream.WriteLine("")
		  fileStream.WriteLine("// global attributes:")
		  fileStream.WriteLine(tab + tab + ":title = " + quotes + "CDL_file" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + ":institution = " + quotes + cfgDV.nc_institution + quotes + " ;")
		  fileStream.WriteLine(tab + tab + ":institution_url = " + quotes + cfgDV.nc_institution_url + quotes + " ;")
		  fileStream.WriteLine(tab + tab + ":contact = " + quotes + cfgDV.nc_contact + quotes + " ;")
		  fileStream.WriteLine(tab + tab + ":source = " + quotes + cfgDV.nc_source + quotes + " ;")
		  fileStream.WriteLine(tab + tab + ":history = " + quotes + cfgDV.nc_history + quotes + " ;")
		  fileStream.WriteLine(tab + tab + ":references = " + quotes + cfgDV.nc_references + quotes + " ;")
		  if App.StageCode=3 then
		    fileStream.WriteLine(tab + tab + ":comment = " + quotes + "This file was generated from " + std.kRadName + " DataViewer Beta V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + "." + quotes + " ;")
		  else
		    fileStream.WriteLine(tab + tab + ":comment = " + quotes + "This file was generated from " + std.kRadName + " DataViewer V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + "." + quotes + " ;")
		  end
		  fileStream.WriteLine(tab + tab + ":Conventions = " + quotes + "CF-1.6" + quotes + " ;")
		  dim nowDate as new date
		  fileStream.WriteLine(tab + tab + ":creation_date = " + quotes +  nowDate.SQLDateTime + " GMT=" + Str(nowDate.GMTOffset) + quotes + " ;")
		  
		  fileStream.WriteLine("data:")     //===============================  data
		  
		  // time
		  fileStream.WriteLine("")
		  fileStream.WriteLine(tab + "time =")
		  lineString=""
		  for i=0 to nt-1
		    if (i mod 10)=0 and not(i=0) then
		      fileStream.WriteLine(lineString)
		      lineString=""
		    end
		    auxInt=(dates(i).Year-1970)*365*24*60*60  //contribution of every year in seconds
		    auxInt=auxInt + Floor( ( dates(i).Year - 1970 + 1) / 4 ) *24*60*60   //contribution of extra day on leap years in seconds (the rule should be more complicated, but we should not worry until 2100)
		    auxInt=auxInt + (dates(i).DayOfYear-1)*24*60*60  //contribution of days of current year in seconds
		    auxInt=auxInt + (dates(i).Hour*60*60) + (dates(i).Minute*60) + dates(i).Second  //contribution of seconds of current day
		    lineString=lineString + Str(auxInt) + ", "
		  next
		  fileStream.WriteLine( left(lineString,len(lineString)-2) + " ;")
		  
		  // lon
		  fileStream.WriteLine("")
		  fileStream.WriteLine(tab + "lon =")
		  lineString=""
		  for i=0 to nx-1
		    if (i mod 10)=0 and not(i=0) then
		      fileStream.WriteLine(lineString)
		      lineString=""
		    end
		    lineString=lineString + Str(App.grdminlon + (i*App.grdlonstep)) + ", "
		  next
		  fileStream.WriteLine( left(lineString,len(lineString)-2) + " ;")
		  
		  // lat
		  fileStream.WriteLine("")
		  fileStream.WriteLine(tab + "lat =")
		  lineString=""
		  for i=0 to ny-1
		    if (i mod 10)=0 and not(i=0) then
		      fileStream.WriteLine(lineString)
		      lineString=""
		    end
		    lineString=lineString + Str(App.grdminlat + (i*App.grdlatstep)) + ", "
		  next
		  fileStream.WriteLine( left(lineString,len(lineString)-2) + " ;")
		  
		  // z
		  fileStream.WriteLine("")
		  fileStream.WriteLine(tab + "z = 0 ;")
		  
		  // ============Writing measured data ==============================
		  
		  'Here there is an OutOfBoundsException that occurs sometimes when multiple sessions are building a CDL file simultaneously... Make sure nv is within bounds before doing something.
		  if varNames.Ubound<(nv-1) then
		    log.Write(false,"OutOfBoundsException while creating CDF file.")
		    MsgBox(kPleaseTryAgain(cfgDV.language))
		    
		    'Trying to exit without downloading nothing... first check in which page we are
		    if OceanDataPage.Enabled then
		      OceanDataPage.cancelLoading=true
		    else
		      TimeDataPage.cancelLoading=true
		    end
		    return(file)
		  end if
		  
		  for l=0 to (nv-1)  // for each variable
		    fileStream.WriteLine("")
		    
		    try
		      lineString=tab + varNames(l) + " ="
		    catch e as OutOfBoundsException
		      log.Write(false,"OutOfBoundsException while creating CDF file.")
		      MsgBox(kPleaseTryAgain(cfgDV.language))
		      'Trying to exit without downloading nothing... first check in which page we are
		      if OceanDataPage.Enabled then
		        OceanDataPage.cancelLoading=true
		      else
		        TimeDataPage.cancelLoading=true
		      end
		      return(file)
		    end try
		    fileStream.WriteLine(lineString)
		    lineString=""
		    for k=0 to (nt-1)
		      for j=0 to (ny-1)
		        for i=0 to (nx-1)
		          if (i mod 10)=0 and not(i=0) then
		            fileStream.WriteLine(lineString)
		            lineString=""
		          end
		          if data(l,k,j,i)=-999 then    // Missing value!!
		            lineString=lineString + "_, "
		          else
		            lineString=lineString + CStr(data(l,k,j,i)) + ", "
		          end
		        next
		        if j<(ny-1) or k<(nt-1) then
		          fileStream.WriteLine(lineString)
		        else
		          fileStream.WriteLine( left(lineString,len(lineString)-2) + " ;")
		        end
		        lineString=""
		      next
		    next
		  next
		  
		  fileStream.WriteLine("}")
		  
		  fileStream.Close
		  
		  return(file)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_acc(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("acc")
		  
		  fileStream.WriteLine(tab + "float acc(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "acc:long_name = " + quotes + "Accuracy of surface current velocity in radial direction" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "acc:units = " + quotes + "m s-1" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "acc:valid_min = 0. ;")
		  fileStream.WriteLine(tab + tab + "acc:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_pow(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("pow")
		  
		  fileStream.WriteLine(tab + "float pow(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "pow:long_name = " + quotes + "relative power" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "pow:units = " + quotes + "_" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "pow:valid_min = 0. ;")
		  fileStream.WriteLine(tab + tab + "pow:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_sig_wv_dir(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("sig_wv_dir")
		  
		  fileStream.WriteLine(tab + "float sig_wv_dir(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "sig_wv_dir:standard_name = " + quotes + "northward_current" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "sig_wv_dir:long_name = " + quotes + "significant wave mean direction" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "sig_wv_dir:units = " + quotes + "degrees_true" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "sig_wv_dir:reference = " + quotes + "clockwise from true north" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "sig_wv_dir:valid_range = 0., 360. ;")
		  fileStream.WriteLine(tab + tab + "sig_wv_dir:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_sig_wv_ht(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("sig_wv_ht")
		  
		  fileStream.WriteLine(tab + "float sig_wv_ht(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "sig_wv_ht:standard_name = " + quotes + "northward_current" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "sig_wv_ht:long_name = " + quotes + "significant wave height" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "sig_wv_ht:units = " + quotes + "meter" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "sig_wv_ht:valid_min = 0. ;")
		  fileStream.WriteLine(tab + tab + "sig_wv_ht:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_var(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("var")
		  
		  fileStream.WriteLine(tab + "float var(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "var:long_name = " + quotes + "variance of surface current velocity in radial direction" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "var:units = " + quotes + "m s-1" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "var:valid_min = 0. ;")
		  fileStream.WriteLine(tab + tab + "var:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_Var1(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("var1")
		  fileStream.WriteLine(tab + "float var1(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "var1:long_name = " + quotes + "Variable 1" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "var1:valid_range = -500., 500. ;")
		  fileStream.WriteLine(tab + tab + "var1:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_Var2(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("var2")
		  fileStream.WriteLine(tab + "float var2(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "var2:long_name = " + quotes + "Variable 2" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "var2:valid_range = -500., 500. ;")
		  fileStream.WriteLine(tab + tab + "var2:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_vel(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("vel")
		  
		  fileStream.WriteLine(tab + "float vel(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "vel:standard_name = " + quotes + "temporal_radial_velocity" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "vel:long_name = " + quotes + "surface current velocity in radial direction" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "vel:units = " + quotes + "m s-1" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "vel:valid_range = -500., 500. ;")
		  fileStream.WriteLine(tab + tab + "vel:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_water_dir(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("water_dir")
		  
		  fileStream.WriteLine(tab + "float water_dir(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "water_dir:standard_name = " + quotes + "current_to_direction" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_dir:long_name = " + quotes + "direction of current relative to true north" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_dir:units = " + quotes + "degrees_true" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_dir:reference = " + quotes + "clockwise from true north" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_dir:valid_range = 0., 360. ;")
		  fileStream.WriteLine(tab + tab + "water_dir:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_water_spd(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("water_spd")
		  
		  fileStream.WriteLine(tab + "float water_spd(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "water_spd:standard_name = " + quotes + "current_speed" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_spd:long_name = " + quotes + "speed of current" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_spd:units = " + quotes + "cm s-1" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_spd:valid_range = 0., 500. ;")
		  fileStream.WriteLine(tab + tab + "water_spd:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_water_u(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("water_u")
		  
		  fileStream.WriteLine(tab + "float water_u(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "water_u:standard_name = " + quotes + "eastward_current" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_u:long_name = " + quotes + "east/west component of current" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_u:units = " + quotes + "cm s-1" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_u:valid_range = -500., 500. ;")
		  fileStream.WriteLine(tab + tab + "water_u:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_water_u_acc(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("water_u_acc")
		  
		  fileStream.WriteLine(tab + "float water_u_acc(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "water_u_acc:long_name = " + quotes + "accuracy of east/west component of current" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_u_acc:units = " + quotes + "cm s-1" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_u_acc:valid_min = 0. ;")
		  fileStream.WriteLine(tab + tab + "water_u_acc:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_water_v(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("water_v")
		  
		  fileStream.WriteLine(tab + "float water_v(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "water_v:standard_name = " + quotes + "northward_current" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_v:long_name = " + quotes + "north/south component of current" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_v:units = " + quotes + "cm s-1" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_v:valid_range = -500., 500. ;")
		  fileStream.WriteLine(tab + tab + "water_v:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_water_v_acc(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("water_v_acc")
		  
		  fileStream.WriteLine(tab + "float water_v_acc(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "water_v_acc:long_name = " + quotes + "accuracy of north/south component of current" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_v_acc:units = " + quotes + "cm s-1" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "water_v_acc:valid_min = 0. ;")
		  fileStream.WriteLine(tab + tab + "water_v_acc:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_wdir(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("wdir")
		  
		  fileStream.WriteLine(tab + "float wdir(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "wdir:standard_name = " + quotes + "wind_to_direction" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "wdir:long_name = " + quotes + "true wind direction oceanographic convention" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "wdir:units = " + quotes + "degrees_true" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "wdir:reference = " + quotes + "clockwise from true north" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "wdir:valid_range = 0., 360. ;")
		  fileStream.WriteLine(tab + tab + "wdir:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Declare_wspd(fileStream As TextOutputStream)
		  dim tab as string=Chr(9)
		  dim quotes as string=Chr(34)
		  
		  varNames.Append("wspd")
		  
		  fileStream.WriteLine(tab + "float wspd(time,z,lat,lon) ;")
		  fileStream.WriteLine(tab + tab + "wspd:standard_name = " + quotes + "wind_speed" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "wspd:long_name = " + quotes + "true wind speed" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "wspd:units = " + quotes + "m s-1" + quotes + " ;")
		  fileStream.WriteLine(tab + tab + "wspd:valid_range = 0., 150. ;")
		  fileStream.WriteLine(tab + tab + "wspd:_FillValue = -999.f ;")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowCopyRightNotice()
		  dim tmpString as String
		  
		  
		  if cfgDV.aboutText<>"default" then
		    tmpString=cfgDV.aboutText + EndOfLine + EndOfLine
		  end
		  
		  if App.StageCode=3 then
		    tmpString=tmpString +std.kRadName + " DataViewer V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + " (" + App.ExecutableFile.ModificationDate.SQLDate + ")" + EndOfLine
		  else
		    tmpString=tmpString +std.kRadName + " DataViewer Beta V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + " (" + App.ExecutableFile.ModificationDate.SQLDate + ")" + EndOfLine
		  end
		  tmpString=tmpString + "(C) " + CStr(App.ExecutableFile.ModificationDate.Year) + " " + std.kComName + ". " + App.kAllRightsReserved(cfgDV.language) + EndOfLine
		  tmpString=tmpString + kLicence(cfgDV.language) +": " + key.kg.UserInfo.Lookup("company","NoCompany!").StringValue + ", " + key.kg.UserInfo.Lookup("user","NoUser!").StringValue+ EndOfLine
		  tmpString=tmpString + kSession(cfgDV.language) + " "+ CStr(1+App.sessionsIDs.IndexOf(Session.Identifier)) + " " + kOf(cfgDV.language) + " " + CStr(1+App.sessionsIDs.Ubound)
		  
		  MsgBox(tmpString)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowPage(n as Integer)
		  OceanDataPage.ODiag.Close
		  HomePage.ODiag.Close
		  HomePage.clickMarkVisible=false
		  OceanDataPage.clickMarkVisible=false
		  
		  RadarDataPage.Enabled=false
		  RadarDataPage.Visible=false
		  
		  HomePage.Enabled=false
		  HomePage.Visible=false
		  
		  OceanDataPage.Enabled=false
		  OceanDataPage.Visible=false
		  
		  TimeDataPage.Enabled=false
		  TimeDataPage.Visible=false
		  
		  WSConfigPage.Enabled=false
		  WSConfigPage.Visible=false
		  
		  'HomePage.AuxTimer.Mode=Timer.ModeOff 'Turning off timer for automatic update in home page
		  'HomePage.blink.Mode=Timer.ModeOff 'Turning off timer for warning blinking
		  Select case n
		  case 0
		    HomePage.Enabled=true
		    HomePage.Visible=true
		    HomePage.UpdateMapHome()
		    
		    // Restart timers in Home page
		    'HomePage.AuxTimer.Mode=Timer.ModeMultiple
		    if HomePage.timerCounter=10 then
		      UpdateToServer.Period=15000
		    else
		      UpdateToServer.Period=1500
		    end
		    UpdateToServer.Reset()
		    
		    if App.OceanWarningSystemEnabled then
		      
		      if cfgDV.WS_blink then
		        HomePage.blink.Mode=Timer.ModeMultiple
		      else
		        HomePage.blink.Mode=Timer.ModeOff
		        HomePage.canvasVisible=true
		      end
		      'HomePage.blink.Reset() //This does not really seems to be required.
		      
		      HomePage.WarningsList.Visible=false
		      HomePage.WarningsList.Visible=true
		      HomePage.UpdateOceanAlertData
		    end
		    
		    
		  case 1
		    RadarDataPage.Enabled=true
		    RadarDataPage.Visible=true
		    RadarDataPage.UpdateStationStatus
		    
		    // In case the browser zoomed in or out (Ctrl + scroll), while WebListBox.Visible was false then it is not shown properly. Next is done to repaint the WebListBox and fix it.
		    RadarDataPage.StationsTable.Visible=false
		    RadarDataPage.StationsTable.Visible=true
		    'self.Show
		    
		  case 2
		    OceanDataPage.Enabled=true
		    OceanDataPage.Visible=true
		    OceanDataPage.LstAvailMeas.Visible=false
		    OceanDataPage.LstAvailMeas.Visible=true
		    
		    if OceanDataPage.LstAvailMeas.ListIndex<>-1 then
		      OceanDataPage.LstAvailMeas.ScrollTo(OceanDataPage.LstAvailMeas.ListIndex)
		    end
		  case 3
		    TimeDataPage.Enabled=true
		    TimeDataPage.Visible=true
		    TimeDataPage.LstAvailMeas.Visible=false
		    TimeDataPage.LstAvailMeas.Visible=true
		    
		    if TimeDataPage.LstAvailMeas.ListIndex<>-1 then
		      TimeDataPage.LstAvailMeas.ScrollTo(TimeDataPage.LstAvailMeas.ListIndex)
		    end
		    
		  case 4
		    WSConfigPage.Enabled=true
		    WSConfigPage.Visible=true
		    
		  end
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateToServerMethod()
		  
		  
		  if HomePage.timerCounter=10 then   'If the map was loaded succesfully last time
		    
		    // Update system monitor status indicator
		    if sm.warningAvail(0) or sm.warningAvail(1) or (sm.extraWarnings<>EndOfLine) then
		      SystemStatusPicto.Picture=pwr_o
		      SystemStatusPicto.Cursor=2
		      SystemStatusLabel.Style=CommonCenterDarkSmall
		      SystemStatusLabel.cursor=2
		    else
		      SystemStatusPicto.Picture=pwr_a
		      SystemStatusPicto.Cursor=0
		      SystemStatusLabel.Style=SmallFont
		      SystemStatusLabel.cursor=0
		    end if
		    
		    if RadarDataPage.Visible then RadarDataPage.UpdateStationStatus
		    
		    if App.OceanWarningSystemEnabled then
		      
		      if HomePage.Visible then
		        if HomePage.statusAlertCur<>ws.statusCur OR HomePage.statusAlertWav<>ws.statusWav OR HomePage.statusAlertWin<>ws.statusWin OR HomePage.timeStampGlobalAlertCur<>ws.globalAlertCurTimeStamp OR HomePage.timeStampGlobalAlertWav<>ws.globalAlertWavTimeStamp then
		          HomePage.UpdateOceanAlertData  'If status level from any type of data changed, we may need to update the colors of the buttons. The idea here is to check for the data types that are not selected.
		        else
		          select case HomePage.currentPictureType 'Here checking specifically on the selected one.
		          case 0
		            if ws.WarnTypeCur.Ubound<>HomePage.typeAlertSelected.Ubound then 'If number of probe warnings changed, update
		              HomePage.UpdateOceanAlertData
		            elseif HomePage.typeAlertSelected.Ubound>=0 AND ws.WarnTypeCur.Ubound>=0 then 'If it is actually the same number of warnigns, and they are not empty (=-1), then...
		              if HomePage.valAlertSelected(0)<>ws.ValCur(0) OR HomePage.namAlertSelected(0)<>ws.NameCur(0) then HomePage.UpdateOceanAlertData   '... check if the content of the first warning is the same as the one from before... otherwise update.
		            end
		          case 10
		            if ws.WarnTypeWav.Ubound<>HomePage.typeAlertSelected.Ubound then
		              HomePage.UpdateOceanAlertData
		            elseif HomePage.typeAlertSelected.Ubound>=0 AND ws.WarnTypeWav.Ubound>=0 then
		              if HomePage.valAlertSelected(0)<>ws.ValWav(0) OR HomePage.namAlertSelected(0)<>ws.NameWav(0) then HomePage.UpdateOceanAlertData
		            end
		          case 20
		            if ws.WarnTypeWin.Ubound<>HomePage.typeAlertSelected.Ubound then
		              HomePage.UpdateOceanAlertData
		            elseif HomePage.typeAlertSelected.Ubound>=0 AND ws.WarnTypeWin.Ubound>=0 then
		              if HomePage.valAlertSelected(0)<>ws.ValWin(0) OR HomePage.namAlertSelected(0)<>ws.NameWin(0) then HomePage.UpdateOceanAlertData
		            end
		          end
		        end
		      end
		      
		      //Update ocean alert status indicator
		      if ws.statusCur>1 or ws.statusWav>1 or ws.statusWin>1 or ws.globalAlertCurRedX.Ubound>=0 or ws.globalAlertCurYellowX.Ubound>=0 or ws.globalAlertWavRedX.Ubound>=0 or ws.globalAlertWavYellowX.Ubound>=0 then
		        AlertPicto.Picture=warn_a
		        AlertPicto.Cursor=2
		        AlertLabel.Cursor=2
		        AlertLabel.Style=CommonCenterSmallRed
		      else
		        AlertPicto.Picture=warn_i
		        AlertPicto.Cursor=0
		        AlertLabel.Cursor=0
		        AlertLabel.Style=SmallFont
		      end
		      
		    end
		    
		    
		  end
		  
		  
		  if HomePage.Visible then
		    dim mustUpdateHome as Boolean=false
		    //Update home maps
		    select case HomePage.currentPictureType
		    case 0
		      if HomePage.currentPictureDate.SQLDateTime<>App.lastCurMapDate.SQLDateTime AND app.lastCurMapQual then mustUpdateHome=true  // Otherwise, no need to update map... it is already updated
		    case 10
		      if HomePage.currentPictureDate.SQLDateTime<>App.lastWavMapDate.SQLDateTime then mustUpdateHome=true  // Otherwise, no need to update map... it is already updated
		    case 20
		      if HomePage.currentPictureDate.SQLDateTime<>App.lastWinMapDate.SQLDateTime then mustUpdateHome=true  // Otherwise, no need to update map... it is already updated
		    else
		      if HomePage.currentPictureType>0 AND HomePage.currentPictureType<=cfgDV.nStations then
		        if HomePage.currentPictureDate.SQLDateTime<>App.lastCurMapDate.SQLDateTime AND app.lastCurMapQual then mustUpdateHome=true  // Otherwise, no need to update map... it is already updated
		      end
		    end select
		    
		    if mustUpdateHome or HomePage.timerCounter<>10 then
		      HomePage.UpdateMapHome()
		      HomePage.CanvasMap.Refresh
		    end
		    
		  else
		    if HomePage.timerCounter<>10 then UpdateToServer.period=15000
		  end
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		#tag Note
			This variable should always be set just before calling ParallelThread.Run to indicate which type of process should be executed by the parallel thread.
			
			1=Create plot image
			2=Create Animation
			3=Create NetCDF file
		#tag EndNote
		threadProcessRequired As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		varNames() As String
	#tag EndProperty


	#tag Constant, Name = kAbout, Type = String, Dynamic = True, Default = \"About", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"About"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Acerca de"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Acerca de"
		#Tag Instance, Platform = Any, Language = de, Definition  = \"\xC3\x9Cber"
	#tag EndConstant

	#tag Constant, Name = kAcc, Type = String, Dynamic = True, Default = \"Acc.", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Acc."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Pre."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Pre."
	#tag EndConstant

	#tag Constant, Name = kCanvasMapHelpTag, Type = String, Dynamic = True, Default = \"Click on map to get information on measured data.", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Click on map to get information on measured data."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Haga clic en el mapa para obtener informaci\xC3\xB3n de los datos medidos."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Clique no mapa para obter informa\xC3\xA7\xC3\xB5es sobre os dados medidos."
	#tag EndConstant

	#tag Constant, Name = kFoundNMeas, Type = String, Dynamic = True, Default = \"Found <n> meas.", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Found <n> meas."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"<n> med. encontradas"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"<n> med. encontrados"
	#tag EndConstant

	#tag Constant, Name = kFrom, Type = String, Dynamic = True, Default = \"From", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"From"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Inicio"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"De"
	#tag EndConstant

	#tag Constant, Name = kHelp, Type = String, Dynamic = True, Default = \"H E L P", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"H E L P"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"A Y U D A"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"A J U D A"
	#tag EndConstant

	#tag Constant, Name = kHelpTagMaritimeAlert, Type = String, Dynamic = True, Default = \"Indicator for extreme oceanographic measurements.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Indicator for extreme oceanographic measurements."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Indicador de condiciones mar\xC3\xADtimas extremas."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Indicador do condi\xC3\xA7\xC3\xB5es de mar extremas."
	#tag EndConstant

	#tag Constant, Name = kHelpTagStatus, Type = String, Dynamic = True, Default = \"Indicator of status of WERA system.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Indicator of status of WERA system."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Indicador del estado del sistema WERA."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Indicador do estado do sistema WERA."
	#tag EndConstant

	#tag Constant, Name = kLat, Type = String, Dynamic = True, Default = \"Latitude", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Latitude"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Latitud"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Latitude"
	#tag EndConstant

	#tag Constant, Name = kLicence, Type = String, Dynamic = True, Default = \"Licence", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Licence"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Licencia"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Licen\xC3\xA7a"
	#tag EndConstant

	#tag Constant, Name = kLon, Type = String, Dynamic = True, Default = \"Longitude", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Longitude"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Longitud"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Longitude"
	#tag EndConstant

	#tag Constant, Name = kMaritimeAlert, Type = String, Dynamic = True, Default = \"Maritime alert:", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Maritime alert:"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Alerta mar\xC3\xADtima:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Alerta mar\xC3\xADtima:"
	#tag EndConstant

	#tag Constant, Name = kNoDataAvail, Type = String, Dynamic = True, Default = \"(No data available)", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"(No data available)"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"(No hay datos disponibles)"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"(Dados n\xC3\xA3o dispon\xC3\xADveis)"
	#tag EndConstant

	#tag Constant, Name = kNoDateSelected, Type = String, Dynamic = True, Default = \"(No date selected)", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"(No date selected)"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"(No hay d\xC3\xADa seleccionado)"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"(Sem data selecionada)"
	#tag EndConstant

	#tag Constant, Name = kNoRecord, Type = String, Dynamic = True, Default = \"No record!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No record!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC2\xA1No hay mediciones!"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"N\xC3\xA3o h\xC3\xA1 medidas!"
	#tag EndConstant

	#tag Constant, Name = kNoTimeIsSelected, Type = String, Dynamic = True, Default = \"No time is selected!", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No time is selected!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Por favor seleccione una medici\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Sem tempo \xC3\xA9 selecionado!"
	#tag EndConstant

	#tag Constant, Name = kOf, Type = String, Dynamic = True, Default = \"of", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"of"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"de"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"de"
	#tag EndConstant

	#tag Constant, Name = kPleaseTryAgain, Type = String, Dynamic = True, Default = \"Please try again.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Please try again."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Por favor intente de nuevo."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Por favor tente novamente."
	#tag EndConstant

	#tag Constant, Name = kQualityFilter, Type = String, Dynamic = True, Default = \"Quality filter:", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Quality filter:"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Filtrar calidad:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Filtro de qualidade:"
	#tag EndConstant

	#tag Constant, Name = kSelectDate, Type = String, Dynamic = True, Default = \"Select date", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select date"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Elija d\xC3\xADa"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Escolha data"
	#tag EndConstant

	#tag Constant, Name = kSelectTime, Type = String, Dynamic = True, Default = \"Select time (UTC)", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select time (UTC)"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Elija hora (UTC)"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Escolha tempo (UTC)"
	#tag EndConstant

	#tag Constant, Name = kSession, Type = String, Dynamic = True, Default = \"Session", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Session"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Sesi\xC3\xB3n"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Sess\xC3\xA3o"
	#tag EndConstant

	#tag Constant, Name = kStartDateMustBeBeforeEndDate, Type = String, Dynamic = True, Default = \"Start date must be before end date", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Start date must be before end date\r"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Medici\xC3\xB3n inicial debe de estar antes de la medici\xC3\xB3n final"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Data de in\xC3\xADcio deve ser anterior \xC3\xA0 data de t\xC3\xA9rmino"
	#tag EndConstant

	#tag Constant, Name = kSystemAdmin, Type = String, Dynamic = True, Default = \"System Administrator:", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"System Administrator:"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Admin. del Sistema:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Administrador do Sistema:"
	#tag EndConstant

	#tag Constant, Name = kSystemStatus, Type = String, Dynamic = True, Default = \"System status:", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"System status:"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Estado sistema:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Estado do sistema:"
	#tag EndConstant

	#tag Constant, Name = kTo, Type = String, Dynamic = True, Default = \"To", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"To"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Final"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"At\xC3\xA9"
	#tag EndConstant


#tag EndWindowCode

#tag Events CopyRightNotice
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ShowCopyRightNotice
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events RadarName
	#tag Event
		Sub DoubleClick(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  
		  if App.StageCode<3 then //is an alpha or beta version... then use this trick to activate
		    if not(cfgDV.enableGF) AND not(Session.gapFillingEnabled) then OceanDataPage.QualFilter.AddRow("Gaps filled")
		    Session.gapFillingEnabled=true
		  end
		  
		  
		  //Used for updating the parameters of the DataViewer configuration file cfgDV.
		  if (1+App.sessionsIDs.Ubound)=1 and Session.account=cfgDV.WS_admin then
		    if not(cfgDV.InitializeConfig(" ")) then
		      log.Write(true,"A problem occurred while attempting to read the parameters of the DataViewerParams.cfg file!")
		    end
		    
		    //Check database
		    db.archive=new MySQLDatabase
		    if db.ReadDBSetupFile(App.execDir + "../DataArchivePrograms/DataArchive_setup.txt") then
		    end
		    db.grid=CStr(cfgDV.gridID)
		    db.sites=cfgDV.nStations
		    
		    if db.CheckDatabaseConnection() then
		    end
		    
		    if siteInfo.Initialize() then
		    end
		    
		    //Initiate the CriticalSection instances
		    App.plotFilePass= new CriticalSection
		    App.animFilePass= new CriticalSection
		    
		    if App.UpdateAppVariables() then
		      log.Write(true,"A problem occurred while attempting to update App variables!")
		      //Error... log
		    end
		    
		    //Avoid problems due to the absence of parameters for station 2
		    if cfgDV.nStations=1 then
		      cfgDV.sFolder_st.Append(cfgDV.sFolder_st(0))
		      cfgDV.sSufix_st.Append(cfgDV.sSufix_st(0))
		    end
		    
		    log.Write(true,"Program parameters reloaded!")
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events KeepDBAlive
	#tag Event
		Sub Action()
		  if not(db.archive.Connect) then
		    log.Write(true,"Lost connexion to database!")
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events SystemStatusPicto
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  if SystemStatusPicto.Cursor=2 then
		    PageNavi1.SOClicked
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Label1
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  Dim file as FolderItem
		  Dim path as String
		  
		  // Download Manual file================================================================
		  path="/home/wera/Documents/Manual-DataViewer.pdf"
		  file = GetFolderItem(path)
		  if file<>Nil then
		    if file.Exists then
		      Session.StdWebFile=WebFile.Open(file)
		      Session.StdWebFile.ForceDownload=true
		      
		      ExecuteJavaScript("window.open('"+ Session.StdWebFile.URL + "');")
		    else
		      log.Write(true,std.kRadName + " DataViewer manual not found!")
		      log.Write(false,"Searched in " + path)
		    end
		  else
		    log.Write(true,std.kRadName + " DataViewer manual not found!")
		    log.Write(false,"Searched in " + path)
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ParallelThread
	#tag Event
		Sub Run()
		  // This parallel thread is used for some processes which can take a significant ammount of time. The idea is to run this processes on a separated thread so that the GUI stays responsive for the user. This way the user can, e.g. click on the cancel button to cancel the process.
		  
		  // What kind of process should we do?
		  select case threadProcessRequired
		  case 1 'Plot creation
		    Dim context as new WebSessionContext(TimeDataPage.MySession)
		    TimeDataPage.LoadPlotMethod
		  case 2 'Animation creation
		    Dim context as new WebSessionContext(OceanDataPage.MySession)
		    OceanDataPage.LoadAnimationMethod
		  case 3 'NetCDF file creation from maps
		    Dim context as new WebSessionContext(OceanDataPage.MySession)
		    OceanDataPage.ExportNetCDF
		  case 4 'NetCDF file creation from time series
		    Dim context as new WebSessionContext(TimeDataPage.MySession)
		    TimeDataPage.ExportNetCDF
		  case 5 'CSV file creation from a time serie
		    Dim context as new WebSessionContext(TimeDataPage.MySession)
		    TimeDataPage.ExportCSV
		  case 6 'Opening the WuT web interfaces using ping
		    Dim context as new WebSessionContext(RadarDataPage.MySession)
		    RadarDataPage.CheckOnWuTConnection
		  else
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ImageView3
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  Dim file as FolderItem
		  Dim path as String
		  
		  // Download Manual================================================================
		  path="/home/wera/Documents/Manual-DataViewer.pdf"
		  file = GetFolderItem(path)
		  if file<>Nil then
		    Session.StdWebFile=WebFile.Open(file)
		    Session.StdWebFile.ForceDownload=true
		    
		    ExecuteJavaScript("window.open('"+ Session.StdWebFile.URL + "');")
		  else
		    log.Write(true,std.kRadName + " DataViewer manual not found!")
		    log.Write(false,"Searched in " + path)
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events CopyRightNotice1
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ShowCopyRightNotice
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events AlertPicto
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  if ws.statusCur>1 or ws.statusWav>1 or ws.statusWin>1 or ws.globalAlertCurRedX.Ubound>=0 or ws.globalAlertCurYellowX.Ubound>=0 or ws.globalAlertWavRedX.Ubound>=0 or ws.globalAlertWavYellowX.Ubound>=0 then
		    PageNavi1.HomeClicked
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events SystemStatusLabel
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  if SystemStatusPicto.Cursor=2 then
		    PageNavi1.SOClicked
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events AlertLabel
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  if ws.statusCur>1 or ws.statusWav>1 or ws.statusWin>1 or ws.globalAlertCurRedX.Ubound>=0 or ws.globalAlertCurYellowX.Ubound>=0 or ws.globalAlertWavRedX.Ubound>=0 or ws.globalAlertWavYellowX.Ubound>=0 then
		    PageNavi1.HomeClicked
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events UpdateToServer
	#tag Event
		Sub Action()
		  
		  UpdateToServerMethod()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Group="ID"
	#tag EndViewProperty
	#tag ViewProperty
		Name="IsImplicitInstance"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Group="Position"
		InitialValue="0"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Behavior"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Behavior"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Behavior"
		InitialValue="Untitled"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Group="Position"
		InitialValue="0"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ImplicitInstance"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
