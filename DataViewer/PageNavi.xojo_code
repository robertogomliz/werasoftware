#tag WebPage
Begin WebContainer PageNavi
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   250
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "WindowStyle"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   164
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   150
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "2134417407"
      TabOrder        =   -1
      Top             =   25
      VerticalCenter  =   0
      Visible         =   True
      Width           =   15
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IconHome
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   563156991
      ProtectImage    =   True
      Scope           =   0
      Style           =   "2066319359"
      TabOrder        =   -1
      Top             =   0
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IconSO
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   2128383999
      ProtectImage    =   True
      Scope           =   0
      Style           =   "2066319359"
      TabOrder        =   -1
      Top             =   50
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IconOM
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1109856255
      ProtectImage    =   True
      Scope           =   0
      Style           =   "2066319359"
      TabOrder        =   -1
      Top             =   100
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IconTS
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1111920639
      ProtectImage    =   True
      Scope           =   0
      Style           =   "2066319359"
      TabOrder        =   -1
      Top             =   150
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IconWS
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1763618815
      ProtectImage    =   True
      Scope           =   0
      Style           =   "2066319359"
      TabOrder        =   -1
      Top             =   200
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IconHomeA
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1787764735
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   0
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IconSOA
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   153737215
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   50
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IconOMA
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   2037622783
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   100
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IconTSA
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   33288191
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   150
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IconWSA
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1093081087
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   200
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator Separator1
      Cursor          =   0
      Enabled         =   True
      Height          =   2
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   49
      VerticalCenter  =   0
      Visible         =   True
      Width           =   36
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator Separator2
      Cursor          =   0
      Enabled         =   True
      Height          =   2
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   99
      VerticalCenter  =   0
      Visible         =   True
      Width           =   36
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator Separator3
      Cursor          =   0
      Enabled         =   True
      Height          =   2
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   149
      VerticalCenter  =   0
      Visible         =   True
      Width           =   36
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LabelHome
      Cursor          =   2
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "929097727"
      TabOrder        =   0
      Text            =   "#kRealTime"
      Top             =   1
      VerticalCenter  =   0
      Visible         =   True
      Width           =   114
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LabelSO
      Cursor          =   2
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "929097727"
      TabOrder        =   1
      Text            =   "#kSystemInfo"
      Top             =   50
      VerticalCenter  =   0
      Visible         =   True
      Width           =   114
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LabelOM
      Cursor          =   2
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "929097727"
      TabOrder        =   2
      Text            =   "#kMapsArchive"
      Top             =   100
      VerticalCenter  =   0
      Visible         =   True
      Width           =   114
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LabelTS
      Cursor          =   2
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "929097727"
      TabOrder        =   3
      Text            =   "#kTimeSeries"
      Top             =   150
      VerticalCenter  =   0
      Visible         =   True
      Width           =   114
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LabelWS
      Cursor          =   2
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "929097727"
      TabOrder        =   4
      Text            =   "#kOceanWarningConfig"
      Top             =   200
      VerticalCenter  =   0
      Visible         =   False
      Width           =   114
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator Separator5
      Cursor          =   0
      Enabled         =   True
      Height          =   2
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   199
      VerticalCenter  =   0
      Visible         =   False
      Width           =   36
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  if App.OceanWarningSystemEnabled AND Session.account=cfgDV.WS_admin then
		    IconWS.Visible=true
		    LabelWS.Visible=true
		    Separator5.Visible=true
		    Rectangle1.Height=200
		  end
		  
		  LabelHome.Text=kRealTime(cfgDV.language)
		  LabelSO.Text=kSystemInfo(cfgDV.language)
		  LabelOM.Text=kMapsArchive(cfgDV.language)
		  LabelTS.Text=kTimeSeries(cfgDV.language)
		  LabelWS.Text=kOceanWarningConfig(cfgDV.language)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub HomeClicked()
		  IconHomeA.Visible=true
		  IconHomeA.Enabled=true
		  IconSOA.Visible=false
		  IconSOA.Enabled=false
		  IconOMA.Visible=false
		  IconOMA.Enabled=false
		  IconTSA.Visible=false
		  IconTSA.Enabled=false
		  IconWSA.Visible=false
		  IconWSA.Enabled=false
		  
		  IconHome.Style=StyLeftToolbarBtnSelected
		  IconSO.Style=StyLeftToolbarBtn
		  IconOM.Style=StyLeftToolbarBtn
		  IconTS.Style=StyLeftToolbarBtn
		  IconWS.Style=StyLeftToolbarBtn
		  
		  LabelHome.Style=StyRightToolbarBtnSelected
		  LabelSO.Style=StyRightToolbarBtn
		  LabelOM.Style=StyRightToolbarBtn
		  LabelTS.Style=StyRightToolbarBtn
		  LabelWS.Style=StyRightToolbarBtn
		  
		  Main.ShowPage(0)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub OMClicked()
		  IconHomeA.Visible=false
		  IconHomeA.Enabled=false
		  IconSOA.Visible=false
		  IconSOA.Enabled=false
		  IconOMA.Visible=true
		  IconOMA.Enabled=true
		  IconTSA.Visible=false
		  IconTSA.Enabled=false
		  IconWSA.Visible=false
		  IconWSA.Enabled=false
		  
		  IconHome.Style=StyLeftToolbarBtn
		  IconSO.Style=StyLeftToolbarBtn
		  IconOM.Style=StyLeftToolbarBtnSelected
		  IconTS.Style=StyLeftToolbarBtn
		  IconWS.Style=StyLeftToolbarBtn
		  
		  LabelHome.Style=StyRightToolbarBtn
		  LabelSO.Style=StyRightToolbarBtn
		  LabelOM.Style=StyRightToolbarBtnSelected
		  LabelTS.Style=StyRightToolbarBtn
		  LabelWS.Style=StyRightToolbarBtn
		  
		  Main.ShowPage(2)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SOClicked()
		  IconHomeA.Visible=false
		  IconHomeA.Enabled=false
		  IconSOA.Visible=true
		  IconSOA.Enabled=true
		  IconOMA.Visible=false
		  IconOMA.Enabled=false
		  IconTSA.Visible=false
		  IconTSA.Enabled=false
		  IconWSA.Visible=false
		  IconWSA.Enabled=false
		  
		  IconHome.Style=StyLeftToolbarBtn
		  IconSO.Style=StyLeftToolbarBtnSelected
		  IconOM.Style=StyLeftToolbarBtn
		  IconTS.Style=StyLeftToolbarBtn
		  IconWS.Style=StyLeftToolbarBtn
		  
		  LabelHome.Style=StyRightToolbarBtn
		  LabelSO.Style=StyRightToolbarBtnSelected
		  LabelOM.Style=StyRightToolbarBtn
		  LabelTS.Style=StyRightToolbarBtn
		  LabelWS.Style=StyRightToolbarBtn
		  
		  Main.ShowPage(1)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TSClicked()
		  IconHomeA.Visible=false
		  IconHomeA.Enabled=false
		  IconSOA.Visible=false
		  IconSOA.Enabled=false
		  IconOMA.Visible=false
		  IconOMA.Enabled=false
		  IconTSA.Visible=true
		  IconTSA.Enabled=true
		  IconWSA.Visible=false
		  IconWSA.Enabled=false
		  
		  IconHome.Style=StyLeftToolbarBtn
		  IconSO.Style=StyLeftToolbarBtn
		  IconOM.Style=StyLeftToolbarBtn
		  IconTS.Style=StyLeftToolbarBtnSelected
		  IconWS.Style=StyLeftToolbarBtn
		  
		  LabelHome.Style=StyRightToolbarBtn
		  LabelSO.Style=StyRightToolbarBtn
		  LabelOM.Style=StyRightToolbarBtn
		  LabelTS.Style=StyRightToolbarBtnSelected
		  LabelWS.Style=StyRightToolbarBtn
		  
		  Main.ShowPage(3)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub WSClicked()
		  IconHomeA.Visible=false
		  IconHomeA.Enabled=false
		  IconSOA.Visible=false
		  IconSOA.Enabled=false
		  IconOMA.Visible=false
		  IconOMA.Enabled=false
		  IconTSA.Visible=false
		  IconTSA.Enabled=false
		  IconWSA.Visible=true
		  IconWSA.Enabled=true
		  
		  IconHome.Style=StyLeftToolbarBtn
		  IconSO.Style=StyLeftToolbarBtn
		  IconOM.Style=StyLeftToolbarBtn
		  IconTS.Style=StyLeftToolbarBtn
		  IconWS.Style=StyLeftToolbarBtnSelected
		  
		  LabelHome.Style=StyRightToolbarBtn
		  LabelSO.Style=StyRightToolbarBtn
		  LabelOM.Style=StyRightToolbarBtn
		  LabelTS.Style=StyRightToolbarBtn
		  LabelWS.Style=StyRightToolbarBtnSelected
		  
		  Main.ShowPage(4)
		End Sub
	#tag EndMethod


	#tag Constant, Name = kMapsArchive, Type = String, Dynamic = True, Default = \"Maps Archive", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Maps Archive"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Archivo de Mapas"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Mapas Arquivados"
	#tag EndConstant

	#tag Constant, Name = kOceanWarningConfig, Type = String, Dynamic = True, Default = \"Maritime Alert", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Maritime Alert"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Alerta Mar\xC3\xADtima"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Alerta Mar\xC3\xADtima"
	#tag EndConstant

	#tag Constant, Name = kRealTime, Type = String, Dynamic = True, Default = \"Last Measurement", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Last Measurement"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC3\x9Altima Medici\xC3\xB3n"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"\xC3\x9Altima Medi\xC3\xA7\xC3\xA3o"
	#tag EndConstant

	#tag Constant, Name = kSystemInfo, Type = String, Dynamic = True, Default = \"System Information", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"System Information"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Informaci\xC3\xB3n del Sistema"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Informa\xC3\xA7\xC3\xB5es do Sistema"
	#tag EndConstant

	#tag Constant, Name = kTimeSeries, Type = String, Dynamic = True, Default = \"Time Series", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Time Series"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Series de Tiempo"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"S\xC3\xA9ries Temporais"
	#tag EndConstant


#tag EndWindowCode

#tag Events IconHome
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  HomeClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IconSO
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  SOClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IconOM
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  OMClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IconTS
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  TSClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IconWS
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  WSClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IconHomeA
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  HomeClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IconSOA
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  SOClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IconOMA
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  OMClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IconTSA
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  TSClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IconWSA
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  WSClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events LabelHome
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  HomeClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events LabelSO
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  SOClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events LabelOM
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  OMClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events LabelTS
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  TSClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events LabelWS
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  WSClicked
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
