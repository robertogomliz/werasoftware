#tag WebPage
Begin WebContainer Home
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   820
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "StyContainer"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   999
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle RecFcstCurasdf
      Cursor          =   0
      Enabled         =   True
      Height          =   40
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   753
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "862912511"
      TabOrder        =   -1
      Top             =   71
      VerticalCenter  =   0
      Visible         =   False
      Width           =   185
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   717
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   728
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "862912511"
      TabOrder        =   -1
      Top             =   10
      VerticalCenter  =   0
      Visible         =   True
      Width           =   27
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView ImgHome
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   717
      HelpTag         =   "Click for point information."
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1613078527
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   720
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LbLastComb
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   765
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1279453183"
      TabOrder        =   0
      Text            =   "Last available map from"
      Top             =   594
      VerticalCenter  =   0
      Visible         =   True
      Width           =   224
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LbLastSt1
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   765
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1279453183"
      TabOrder        =   1
      Text            =   "Last available map from"
      Top             =   655
      VerticalCenter  =   0
      Visible         =   True
      Width           =   224
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LbLastSt2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   765
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1279453183"
      TabOrder        =   2
      Text            =   "Last available map from"
      Top             =   708
      VerticalCenter  =   0
      Visible         =   True
      Width           =   224
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin OceanDialog ODiag
      Cursor          =   0
      Enabled         =   True
      Height          =   91
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   100
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinHeight       =   0
      MinWidth        =   0
      Resizable       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Title           =   "Grid(ix,iy)"
      Top             =   100
      Type            =   2
      VerticalCenter  =   0
      Visible         =   True
      Width           =   122
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LbLastComb1
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   31
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   765
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   4
      Text            =   "#kLastCombinedCurrentMap"
      Top             =   562
      VerticalCenter  =   0
      Visible         =   True
      Width           =   224
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LbLastSt2_2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   765
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   5
      Text            =   "Last measurement in station 2:"
      Top             =   688
      VerticalCenter  =   0
      Visible         =   True
      Width           =   224
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LbLastSt1_2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   765
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   6
      Text            =   "Last measurement in station 1:"
      Top             =   635
      VerticalCenter  =   0
      Visible         =   True
      Width           =   224
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator Separator1
      Cursor          =   0
      Enabled         =   True
      Height          =   2
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   777
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   626
      VerticalCenter  =   0
      Visible         =   True
      Width           =   200
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCanvas CanvasMap
      Cursor          =   21
      Enabled         =   True
      Height          =   717
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      VerticalCenter  =   0
      Visible         =   True
      Width           =   720
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTimer blink
      Cursor          =   0
      Enabled         =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   60
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Mode            =   2
      Period          =   2000
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Top             =   60
      VerticalCenter  =   0
      Visible         =   True
      Width           =   32
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin SpecDialog SpectrumDialog
      buoy_index      =   0
      buoy_ixiy       =   ""
      Cursor          =   0
      dirAvail        =   False
      Enabled         =   True
      header          =   0
      Height          =   180
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinHeight       =   0
      MinWidth        =   0
      Resizable       =   False
      Scope           =   0
      ShowingWaveBuoy =   False
      Style           =   "0"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Title           =   "Wave Spectrum Plot"
      Top             =   20
      Type            =   2
      VerticalCenter  =   0
      Visible         =   True
      Width           =   360
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle RecCurrent
      Cursor          =   0
      Enabled         =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   753
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "862912511"
      TabOrder        =   -1
      Top             =   25
      VerticalCenter  =   0
      Visible         =   False
      Width           =   185
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnCurrent
      AutoDisable     =   False
      Caption         =   "#kBtnCurrents"
      Cursor          =   0
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#kBtnCurrentsHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   771
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "434558975"
      TabOrder        =   7
      Top             =   35
      VerticalCenter  =   0
      Visible         =   False
      Width           =   157
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle RecWave
      Cursor          =   0
      Enabled         =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   753
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "862912511"
      TabOrder        =   -1
      Top             =   120
      VerticalCenter  =   0
      Visible         =   False
      Width           =   185
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnWave
      AutoDisable     =   False
      Caption         =   "#kBtnWaves"
      Cursor          =   0
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#kBtnWavesHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   771
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "434558975"
      TabOrder        =   8
      Top             =   130
      VerticalCenter  =   0
      Visible         =   False
      Width           =   157
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle RecWind
      Cursor          =   0
      Enabled         =   True
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   753
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "862912511"
      TabOrder        =   -1
      Top             =   170
      VerticalCenter  =   0
      Visible         =   False
      Width           =   185
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnWind
      AutoDisable     =   False
      Caption         =   "#kBtnWinds"
      Cursor          =   0
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#kBtnWindsHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   771
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "434558975"
      TabOrder        =   9
      Top             =   180
      VerticalCenter  =   0
      Visible         =   False
      Width           =   157
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebListBox WarningsList
      AlternateRowColor=   &c44444400
      ColumnCount     =   2
      ColumnWidths    =   "100"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "0"
      Height          =   117
      HelpTag         =   "#kWarningsListHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "Location	Value"
      Left            =   775
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   False
      PrimaryRowColor =   &c44444400
      Scope           =   0
      SelectionStyle  =   "0"
      Style           =   "1137727487"
      TabOrder        =   -1
      Top             =   266
      VerticalCenter  =   0
      Visible         =   False
      Width           =   175
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea WarningsInfo
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   111
      HelpTag         =   "#kWarningsInfoHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   775
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1143713791"
      TabOrder        =   10
      Text            =   ""
      Top             =   427
      VerticalCenter  =   0
      Visible         =   False
      Width           =   175
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblWarningList
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   775
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   21
      Text            =   "Warnings List"
      Top             =   241
      VerticalCenter  =   0
      Visible         =   False
      Width           =   110
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblWarningInfo
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   775
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   22
      Text            =   "Warning Info"
      Top             =   402
      VerticalCenter  =   0
      Visible         =   False
      Width           =   110
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WaveBuoyDialog WaBuDialog
      Cursor          =   0
      Enabled         =   True
      Height          =   92
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinHeight       =   0
      MinWidth        =   0
      Resizable       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Title           =   "Synthetic Buoy"
      Top             =   20
      Type            =   2
      VerticalCenter  =   0
      Visible         =   True
      Width           =   122
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebProgressWheel ProgressWheelMap
      Cursor          =   0
      Enabled         =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   354
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   210
      Type            =   0
      VerticalCenter  =   0
      Visible         =   False
      Width           =   32
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnFcstCur
      AutoDisable     =   False
      Caption         =   "#kBtnFrcstCur"
      Cursor          =   0
      Enabled         =   True
      Height          =   24
      HelpTag         =   "#kBtnFrcstCurHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   800
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "434558975"
      TabOrder        =   9
      Top             =   76
      VerticalCenter  =   0
      Visible         =   False
      Width           =   128
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  dim i as integer
		  
		  currentPictureType=cfgDV.initialMapType
		  currentPictureDate=new date
		  currentPictureDate.SQLDateTime="1111-11-11 11:11:11"
		  
		  if currentPictureType<10 then
		    BtnCurrent.Enabled=false
		    BtnCurrent.Style=BtnCommonFontDisabled
		    RecCurrent.Visible=true
		  elseif currentPictureType<20 then
		    BtnWave.Enabled=false
		    BtnWave.Style=BtnCommonFontDisabled
		    RecWave.Visible=true
		  elseif currentPictureType<30 then
		    BtnWind.Enabled=false
		    BtnWind.Style=BtnCommonFontDisabled
		    RecWind.Visible=true
		  else
		    'BtnFcstCur.Enabled=false
		    'BtnFcstCur.Style=BtnCommonFontDisabled
		    ''RecFcstCur.Visible=true
		  end if
		  
		  if cfgDV.enableCCV or (cfgDV.initialMapType>0 AND cfgDV.initialMapType<10) then
		    BtnCurrent.Visible=true
		  end
		  
		  if cfgDV.enableCSWH then
		    BtnWave.Visible=true
		  end if
		  
		  if cfgDV.enableWD then
		    BtnWind.Visible=true
		  end
		  
		  if cfgDV.enableForecastCCV then
		    BtnFcstCur.Visible=true
		  end
		  
		  // If Ocean Warning system is licenced then display it.
		  if App.OceanWarningSystemEnabled then
		    'blink.Enabled=true
		    for i=0 to WarningsList.ColumnCount-1
		      WarningsList.ColumnStyle(i)=CommonCenter
		    next
		    LblWarningInfo.Visible=true
		    LblWarningList.Visible=true
		    WarningsList.Visible=true
		    WarningsInfo.Visible=true
		  end
		  
		  
		  
		  //Localization (language dynamic strings)
		  
		  BtnCurrent.Caption= kBtnCurrents(cfgDV.language)
		  BtnCurrent.HelpTag= kBtnCurrentsHelpTag(cfgDV.language)
		  BtnWave.Caption=kBtnWaves(cfgDV.language)
		  BtnWave.HelpTag=kBtnWavesHelpTag(cfgDV.language)
		  BtnWind.Caption=kBtnWinds(cfgDV.language)
		  BtnWind.HelpTag=kBtnWindsHelpTag(cfgDV.language)
		  BtnFcstCur.Caption=kBtnFrcstCur(cfgDV.language)
		  BtnFcstCur.HelpTag=kBtnFrcstCurHelpTag(cfgDV.language)
		  
		  LblWarningList.Text=kWarningsList(cfgDV.language)
		  LblWarningList.HelpTag=kWarningsListHelpTag(cfgDV.language)
		  WarningsList.Heading(0)=kWarnLocation(cfgDV.language)
		  WarningsList.Heading(1)=kWarnValue(cfgDV.language)
		  LblWarningInfo.Text=kWarningInfo(cfgDV.language)
		  LblWarningInfo.HelpTag=kWarningsInfoHelpTag(cfgDV.language)
		  
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub ResetWarningIndicators()
		  dim i as integer
		  
		  indexAlertSelected=0
		  WarningsList.DeleteAllRows
		  WarningsInfo.Text=kNoWarningIsSelected(cfgDV.language)
		  
		  for i=0 to namAlertSelected.Ubound
		    WarningsList.AddRow(namAlertSelected(i),CStr(valAlertSelected(i)))
		  next
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateGlobalAlertsCur()
		  
		  dim i as integer
		  
		  Redim GACurRedX(ws.globalAlertCurRedX.Ubound)
		  Redim GACurRedY(ws.globalAlertCurRedX.Ubound)
		  for i=0 to GACurRedX.Ubound
		    GACurRedX(i)=ws.globalAlertCurRedX(i)
		    GACurRedY(i)=ws.globalAlertCurRedY(i)
		  next
		  
		  Redim GACurYellowX(ws.globalAlertCurYellowX.Ubound)
		  Redim GACurYellowY(ws.globalAlertCurYellowX.Ubound)
		  for i=0 to GACurYellowX.Ubound
		    GACurYellowX(i)=ws.globalAlertCurYellowX(i)
		    GACurYellowY(i)=ws.globalAlertCurYellowY(i)
		  next
		  
		  GACurMax=ws.globalAlertCurMax
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateGlobalAlertsWav()
		  
		  dim i as integer
		  
		  Redim GAWavRedX(ws.globalAlertWavRedX.Ubound)
		  Redim GAWavRedY(ws.globalAlertWavRedX.Ubound)
		  for i=0 to GAWavRedX.Ubound
		    GAWavRedX(i)=ws.globalAlertWavRedX(i)
		    GAWavRedY(i)=ws.globalAlertWavRedY(i)
		  next
		  
		  Redim GAWavYellowX(ws.globalAlertWavYellowX.Ubound)
		  Redim GAWavYellowY(ws.globalAlertWavYellowX.Ubound)
		  for i=0 to GAWavYellowX.Ubound
		    GAWavYellowX(i)=ws.globalAlertWavYellowX(i)
		    GAWavYellowY(i)=ws.globalAlertWavYellowY(i)
		  next
		  
		  GAWavMax=ws.globalAlertWavMax
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateLabels()
		  
		  // Updates the text labels shown under the picture (information of last measured times) and the labels on the dialogs shown when clicking the map.
		  
		  Select Case currentPictureType
		  case 0
		    sFolder=cfgDV.sFolder_cur_asc
		    sSufix=cfgDV.sSufix_cur_asc
		    ODiag.Lbl1.Text="Vel."
		    ODiag.Lbl2.Text="Dir."
		    ODiag.Lbl1.HelpTag=App.kNameCCVAbs(cfgDV.language)
		    ODiag.Lbl2.HelpTag=App.kNameCCVDir(cfgDV.language)
		    ODiag.Lbl1Text.HelpTag=App.kNameCCVAbs(cfgDV.language)
		    ODiag.Lbl2Text.HelpTag=App.kNameCCVDir(cfgDV.language)
		    LbLastComb1.Text=kLastCombinedCurrentMap(cfgDV.language)
		    if cfgDV.homeMapTypeCur<>0 then
		      select case cfgDV.homeMapTypeCur
		      case 3
		        sSufix=Replace(sSufix,".cur_asc","_ar.cur_asc")
		      case 1
		        sSufix=Replace(sSufix,".cur_asc","_q1.cur_asc")
		      case 2
		        sSufix=Replace(sSufix,".cur_asc","_q2.cur_asc")
		      case 4
		        sSufix=Replace(sSufix,".cur_asc","_q4.cur_asc")
		      end select
		    end
		  case 10
		    sFolder=cfgDV.sFolder_wauv_asc
		    sSufix=cfgDV.sSufix_wauv_asc
		    ODiag.Lbl1.Text="Hs"
		    ODiag.Lbl2.Text="Dir."
		    ODiag.Lbl1.HelpTag=App.kNameCSWHVal(cfgDV.language)
		    ODiag.Lbl2.HelpTag=App.kNameCSWHDir(cfgDV.language)
		    ODiag.Lbl1Text.HelpTag=App.kNameCSWHVal(cfgDV.language)
		    ODiag.Lbl2Text.HelpTag=App.kNameCSWHDir(cfgDV.language)
		    LbLastComb1.Text=kLastCombinedWaveHeightMap
		    if cfgDV.homeMapTypeWav<>0 then
		      select case cfgDV.homeMapTypeWav
		      case 3
		        sSufix=Replace(sSufix,".wav_asc","_ar.wav_asc")
		      case 1
		        sSufix=Replace(sSufix,".wav_asc","_q1.wav_asc")
		      case 2
		        sSufix=Replace(sSufix,".wav_asc","_q2.wav_asc")
		      case 4
		        sSufix=Replace(sSufix,".wav_asc","_q4.wav_asc")
		      end select
		    end
		  case 20
		    sFolder=cfgDV.sFolder_wiuv_asc
		    sSufix=cfgDV.sSufix_wiuv_asc
		    ODiag.Lbl1.Text="Speed"
		    ODiag.Lbl2.Text="Dir."
		    ODiag.Lbl1.HelpTag=App.kNameWS(cfgDV.language)
		    ODiag.Lbl2.HelpTag=App.kNameWD(cfgDV.language)
		    ODiag.Lbl1Text.HelpTag=App.kNameWS(cfgDV.language)
		    ODiag.Lbl2Text.HelpTag=App.kNameWD(cfgDV.language)
		    LbLastComb1.Text=kLastCombinedWindMap(cfgDV.language)
		    if cfgDV.homeMapTypeWin<>0 then
		      select case cfgDV.homeMapTypeWin
		      case 3
		        sSufix=Replace(sSufix,".win_asc","_ar.win_asc")
		      case 1
		        sSufix=Replace(sSufix,".win_asc","_q1.win_asc")
		      case 2
		        sSufix=Replace(sSufix,".win_asc","_q2.win_asc")
		      case 4
		        sSufix=Replace(sSufix,".win_asc","_q4.win_asc")
		      end select
		    end
		  else
		    if currentPictureType<=cfgDV.nStations then
		      sFolder=cfgDV.sFolder_st(currentPictureType-1)
		      sSufix=cfgDV.sSufix_st(currentPictureType-1)
		      ODiag.Lbl1.Text="Vel."
		      ODiag.Lbl2.Text=Main.kAcc(cfgDV.language)
		      LbLastComb1.Text=kLastRadialCurrentMap(cfgDV.language)
		      if cfgDV.homeMapTypeCur<>0 then
		        select case cfgDV.homeMapTypeCur
		        case 3
		          sSufix=Replace(sSufix,".crad","_ar.crad")
		        case 1
		          sSufix=Replace(sSufix,".crad","_q1.crad")
		        case 2
		          sSufix=Replace(sSufix,".crad","_q2.crad")
		        case 4
		          sSufix=Replace(sSufix,".crad","_q4.crad")
		        end select
		      end
		    else
		      log.Write(false,"InitialMapType parameter not recognized. Using combined currents.")
		      sFolder=cfgDV.sFolder_cur_asc
		      sSufix=cfgDV.sSufix_cur_asc
		      ODiag.Lbl1.Text="Vel."
		      ODiag.Lbl2.Text="Dir."
		      LbLastComb1.Text=kLastCombinedCurrentMap(cfgDV.language)
		      if cfgDV.homeMapTypeCur<>0 then
		        select case cfgDV.homeMapTypeCur
		        case 3
		          sSufix=Replace(sSufix,".cur_asc","_ar.cur_asc")
		        case 1
		          sSufix=Replace(sSufix,".cur_asc","_q1.cur_asc")
		        case 2
		          sSufix=Replace(sSufix,".cur_asc","_q2.cur_asc")
		        case 4
		          sSufix=Replace(sSufix,".cur_asc","_q4.cur_asc")
		        end select
		      end
		    end if
		  End select
		  
		  if cfgDV.nStations>=2 then
		    LbLastSt1_2.Text=kLastDatasetIn(cfgDV.language) + " " + siteInfo.longname(0)
		    LbLastSt2_2.Text=kLastDatasetIn(cfgDV.language) + " " + siteInfo.longname(1)
		  else
		    LbLastSt1_2.Visible=false
		    LbLastSt2_2.Visible=false
		    LbLastSt1.Visible=false
		    LbLastSt2.Visible=false
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateMapHome()
		  dim tempdate as date
		  
		  if cfgDV.nStations>1 then
		    if currentPictureType <10 then
		      if App.lastSt1CurDate.SQLDateTime="1111-11-11 11:11:11" then LbLastSt1.Text=Main.kNoRecord(cfgDV.language) else LbLastSt1.Text=App.lastSt1CurDate.SQLDateTime
		      if App.lastSt2CurDate.SQLDateTime="1111-11-11 11:11:11" then LbLastSt2.Text=Main.kNoRecord(cfgDV.language) else LbLastSt2.Text=App.lastSt2CurDate.SQLDateTime
		    else
		      if App.lastSt1WavDate.SQLDateTime="1111-11-11 11:11:11" then LbLastSt1.Text=Main.kNoRecord(cfgDV.language) else LbLastSt1.Text=App.lastSt1WavDate.SQLDateTime
		      if App.lastSt2WavDate.SQLDateTime="1111-11-11 11:11:11" then LbLastSt2.Text=Main.kNoRecord(cfgDV.language) else LbLastSt2.Text=App.lastSt2WavDate.SQLDateTime
		    end if
		  end
		  
		  select case currentPictureType
		  case 0
		    'if currentPictureDate.SQLDateTime=App.lastCurMapDate.SQLDateTime then return  // No need to update map... it is already updated
		    tempdate=App.lastCurMapDate
		  case 10
		    'if currentPictureDate.SQLDateTime=App.lastWavMapDate.SQLDateTime then return  // No need to update map... it is already updated
		    tempdate=App.lastWavMapDate
		  case 20
		    'if currentPictureDate.SQLDateTime=App.lastWinMapDate.SQLDateTime then return  // No need to update map... it is already updated
		    tempdate=App.lastWinMapDate
		  case 30
		    tempdate=App.lastFcstCurDate
		  else
		    if currentPictureType<=cfgDV.nStations then
		      tempdate=App.lastCurMapDate
		    end if
		  end select
		  
		  UpdateLabels
		  
		  if tempdate.SQLDateTime="1111-11-11 11:11:11" then
		    LbLastComb.Text=Main.kNoRecord(cfgDV.language)
		    currentPictureDate=tempdate
		    timerCounter=10
		    ProgressWheelMap.Visible=false
		    log.Write(true,kCouldNotUpdate(cfgDV.language))
		  else
		    
		    LbLastComb.Text=tempdate.SQLDateTime
		    
		    pathLoading=Str(tempdate.Year) + Str(tempdate.DayOfYear,"00#") + Str(tempdate.Hour,"0#") + Str(tempdate.Minute,"0#")
		    if cfgDV.useSeconds then
		      pathLoading=pathLoading + Str(tempdate.Second,"0#")
		    end
		    
		    Dim FI as New FolderItem
		    Dim Pict1 as WebPicture
		    
		    // Creating a FolderItem object from path string
		    if cfgDV.mapsOrganized then
		      FI=GetFolderItem(cfgDV.stdFolder+sFolder+   left(pathLoading,4) + "/"    + mid(pathLoading, 5, 3) + "/"  + pathLoading+sSufix+cfgDV.graphicsFormat)   // pathLoading='YYYYDDDHHMM...
		    else
		      FI=GetFolderItem(cfgDV.stdFolder+sFolder+pathLoading+sSufix+cfgDV.graphicsFormat)
		    end
		    
		    
		    if FI=Nil then
		      
		      if timerCounter=10 then
		        timerCounter=0
		        ProgressWheelMap.Visible=true
		      elseif timerCounter=5 then
		        timerCounter=10
		        if cfgDV.mapsOrganized then
		          log.Write(true,"Folder does not exists!" + " ("+cfgDV.stdFolder+sFolder+   left(pathLoading,4) + "/"    + mid(pathLoading, 5, 3) + "/"+")")
		        else
		          log.Write(true,"Folder does not exists! ("+cfgDV.stdFolder+sFolder+")")
		        end if
		        ProgressWheelMap.Visible=false
		      else
		        timerCounter=timerCounter+1
		      end
		    else
		      if FI.Exists then
		        // Creating a Picture object from the image file (requires a FolderItem object)
		        try
		          Pict1=new WebPicture(FI)
		        catch err As UnsupportedFormatException
		          
		        end
		        
		        if Pict1 = Nil then  // A 'Nil' value means a problem during Picture object creation
		          if timerCounter=10 then
		            timerCounter=0
		            ProgressWheelMap.Visible=true
		          elseif timerCounter=5 then
		            timerCounter=10
		            log.Write(true,"Error loading image in Home page update! ("+FI.AbsolutePath+").  File is probably corrupted.")
		            ProgressWheelMap.Visible=false
		          else
		            timerCounter=timerCounter+1
		          end
		        else
		          ImgHome.Picture=Pict1
		          timerCounter=10
		          ProgressWheelMap.Visible=false
		          currentPictureDate=tempdate
		          if currentPictureType > 0 AND currentPictureType <= cfgDV.nStations then
		            IdHeaderCurrentPict=App.lastCurMapId
		          elseif currentPictureType >10 AND currentPictureType <= (10+cfgDV.nStations) then
		            IdHeaderCurrentPict=App.lastWavMapId
		          elseif currentPictureType = 0 then
		            IdHeaderCurrentPict=App.lastCurMapId
		          elseif currentPictureType = 10 then
		            IdHeaderCurrentPict=App.lastWavMapId
		          else
		            IdHeaderCurrentPict=App.lastWinMapId
		          end
		          
		        end
		        
		      else
		        if timerCounter=10 then
		          timerCounter=0
		          ProgressWheelMap.Visible=true
		        elseif timerCounter=5 then
		          timerCounter=10
		          log.Write(true,"Image " + FI.AbsolutePath + " does not exists!")
		          ProgressWheelMap.Visible=false
		        else
		          timerCounter=timerCounter+1
		        end
		      end
		      
		    end
		    
		  end
		  
		  if timerCounter=10 then       // timerCounter=10 when it was successfully updated, otherwise is the number of times it has tried to update but the picture is still not available.
		    Main.UpdateToServer.Period=15000
		  elseif timerCounter=0 then
		    Main.UpdateToServer.Period=1500
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateOceanAlertData()
		  dim tmpName as string
		  dim tmpVal as Double
		  dim i as integer
		  
		  if timeStampGlobalAlertCur<>ws.globalAlertCurTimeStamp then UpdateGlobalAlertsCur
		  if timeStampGlobalAlertWav<>ws.globalAlertWavTimeStamp then UpdateGlobalAlertsWav
		  
		  if indexAlertSelected>0 then
		    tmpName=namAlertSelected(indexAlertSelected-1)
		    tmpVal=valAlertSelected(indexAlertSelected-1)
		  end
		  
		  statusAlertCur=ws.statusCur
		  statusAlertWav=ws.statusWav
		  statusAlertWin=ws.statusWin
		  
		  Select case currentPictureType
		  case 0
		    Redim typeAlertSelected(ws.WarnTypeCur.Ubound)
		    Redim namAlertSelected(ws.NameCur.Ubound)
		    Redim latAlertSelected(ws.LatCur.Ubound)
		    Redim lonAlertSelected(ws.LonCur.Ubound)
		    Redim valAlertSelected(ws.ValCur.Ubound)
		    for i=0 to ws.WarnTypeCur.Ubound
		      typeAlertSelected(i)=ws.WarnTypeCur(i)
		      namAlertSelected(i)=ws.NameCur(i)
		      latAlertSelected(i)=ws.LatCur(i)
		      lonAlertSelected(i)=ws.LonCur(i)
		      valAlertSelected(i)=ws.ValCur(i)
		    next
		  case 10
		    Redim typeAlertSelected(ws.WarnTypeWav.Ubound)
		    Redim namAlertSelected(ws.NameWav.Ubound)
		    Redim latAlertSelected(ws.LatWav.Ubound)
		    Redim lonAlertSelected(ws.LonWav.Ubound)
		    Redim valAlertSelected(ws.ValWav.Ubound)
		    for i=0 to ws.WarnTypeWav.Ubound
		      typeAlertSelected(i)=ws.WarnTypeWav(i)
		      namAlertSelected(i)=ws.NameWav(i)
		      latAlertSelected(i)=ws.LatWav(i)
		      lonAlertSelected(i)=ws.LonWav(i)
		      valAlertSelected(i)=ws.ValWav(i)
		    next
		  case 20
		    Redim typeAlertSelected(ws.WarnTypeWin.Ubound)
		    Redim namAlertSelected(ws.NameWin.Ubound)
		    Redim latAlertSelected(ws.LatWin.Ubound)
		    Redim lonAlertSelected(ws.LonWin.Ubound)
		    Redim valAlertSelected(ws.ValWin.Ubound)
		    for i=0 to ws.WarnTypeWin.Ubound
		      typeAlertSelected(i)=ws.WarnTypeWin(i)
		      namAlertSelected(i)=ws.NameWin(i)
		      latAlertSelected(i)=ws.LatWin(i)
		      lonAlertSelected(i)=ws.LonWin(i)
		      valAlertSelected(i)=ws.ValWin(i)
		    next
		  End Select
		  
		  ResetWarningIndicators()
		  
		  if indexAlertSelected>0 then
		    if indexAlertSelected-1 <= namAlertSelected.Ubound then
		      if tmpName=namAlertSelected(indexAlertSelected-1) AND tmpVal=valAlertSelected(indexAlertSelected-1) then WarningListCellClick(indexAlertSelected-1)
		    end
		  end
		  
		  if cfgDV.WS_blink=false then
		    if statusAlertCur>1 or GACurRedX.Ubound>=0 or GACurYellowX.Ubound>=0 then
		      if statusAlertCur=4 or GACurRedX.Ubound>=0 then
		        BtnCurrent.Style=BtnCommonFontRed
		      else
		        BtnCurrent.Style=BtnCommonFontYellow
		      end
		    else
		      BtnCurrent.Style=BtnCommonFont
		    end
		    if statusAlertWav>1 or GAWavRedX.Ubound>=0 or GAWavYellowX.Ubound>=0 then
		      if statusAlertWav=4 or GACurRedX.Ubound>=0 then
		        BtnWave.Style=BtnCommonFontRed
		      else
		        BtnWave.Style=BtnCommonFontYellow
		      end
		    else
		      BtnWave.Style=BtnCommonFont
		    end
		    if statusAlertWin>1 then
		      if statusAlertWin=4 then
		        BtnWind.Style=BtnCommonFontRed
		      else
		        BtnWind.Style=BtnCommonFontYellow
		      end
		    else
		      BtnWind.Style=BtnCommonFont
		    end
		  end
		  
		  
		  //At the end, just refresh the canvas to reflect whatever is changed...
		  CanvasMap.Refresh
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub WarningListCellClick(row as Integer)
		  
		  indexAlertSelected=Row+1
		  CanvasMap.Refresh
		  
		  
		  WarningsInfo.Text=namAlertSelected(row) + EndOfLine
		  if latAlertSelected(row)<0 then
		    WarningsInfo.Text=WarningsInfo.Text+CSTr(abs(latAlertSelected(row))) + " S, "
		  else
		    WarningsInfo.Text=WarningsInfo.Text+CSTr(abs(latAlertSelected(row))) + " N, "
		  end if
		  if lonAlertSelected(row)<0 then
		    WarningsInfo.Text=WarningsInfo.Text+CSTr(abs(lonAlertSelected(row))) + " W" + EndOfLine
		  else
		    WarningsInfo.Text=WarningsInfo.Text+CSTr(abs(lonAlertSelected(row))) + " E" + EndOfLine
		  end if
		  
		  if currentPictureType=10 then
		    WarningsInfo.Text=WarningsInfo.Text+kMeasured(cfgDV.language) + CStr(valAlertSelected(row)) + " m"
		  else
		    WarningsInfo.Text=WarningsInfo.Text+kMeasured(cfgDV.language) + CStr(valAlertSelected(row)) + " m/s"
		  end
		  
		  WarningsInfo.Text=WarningsInfo.Text+Endofline+currentPictureDate.SQLDateTime
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		canvasVisible As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		clickMarkVisible As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		clickMarkX As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		clickMarkY As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		currentPictureDate As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		currentPictureType As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		GACurMax As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		GACurRedX(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		GACurRedY(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		GACurYellowX(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		GACurYellowY(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		GAWavMax As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		GAWavRedX(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		GAWavRedY(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		GAWavYellowX(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		GAWavYellowY(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		IdHeaderCurrentPict As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		indexAlertSelected As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		latAlertSelected(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		lonAlertSelected(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		namAlertSelected(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		pathLoading As string
	#tag EndProperty

	#tag Property, Flags = &h0
		sFolder As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sSufix As string
	#tag EndProperty

	#tag Property, Flags = &h0
		statusAlertCur As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		statusAlertWav As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		statusAlertWin As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		timerCounter As Integer = 10
	#tag EndProperty

	#tag Property, Flags = &h0
		timeStampGlobalAlertCur As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timeStampGlobalAlertWav As String
	#tag EndProperty

	#tag Property, Flags = &h0
		typeAlertSelected(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		valAlertSelected(-1) As Double
	#tag EndProperty


	#tag Constant, Name = kBtnCurrents, Type = String, Dynamic = True, Default = \"Currents", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Currents"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Corrientes"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Correntes"
	#tag EndConstant

	#tag Constant, Name = kBtnCurrentsHelpTag, Type = String, Dynamic = True, Default = \"Display map of currents", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Display map of currents"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Muestra el mapa de corrientes"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Apresenta\xC3\xA7\xC3\xA3o do mapa das correntes"
	#tag EndConstant

	#tag Constant, Name = kBtnFrcstCur, Type = String, Dynamic = True, Default = \"Forecast", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Forecast"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Pron\xC3\xB3stico"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Previs\xC3\xA3o"
	#tag EndConstant

	#tag Constant, Name = kBtnFrcstCurHelpTag, Type = String, Dynamic = True, Default = \"Display animation with forecast of current fields.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Display animation with forecast of current fields."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Muestra animaci\xC3\xB3n con pron\xC3\xB3stico de campos de corriente."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Com display de anima\xC3\xA7\xC3\xA3o previs\xC3\xA3o dos campos de correntes."
	#tag EndConstant

	#tag Constant, Name = kBtnWaves, Type = String, Dynamic = True, Default = \"Waves", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Waves"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Oleaje"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Ondas"
	#tag EndConstant

	#tag Constant, Name = kBtnWavesHelpTag, Type = String, Dynamic = True, Default = \"Display map of waves.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Display map of waves."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Muestra mapa de campo de oleaje."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Apresenta\xC3\xA7\xC3\xA3o do mapa de ondas."
	#tag EndConstant

	#tag Constant, Name = kBtnWinds, Type = String, Dynamic = True, Default = \"Wind", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Wind"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Viento"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Vento"
	#tag EndConstant

	#tag Constant, Name = kBtnWindsHelpTag, Type = String, Dynamic = True, Default = \"Display map of winds.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Display map of winds."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Muestra mapa de vientos."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Apresenta\xC3\xA7\xC3\xA3o do mapa dos ventos."
	#tag EndConstant

	#tag Constant, Name = kCouldNotUpdate, Type = String, Dynamic = True, Default = \"Could not update to last dataset map. No records found for this type of data!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Could not update to last dataset map. No records found for this type of data!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"No ha sido posible mostrar el \xC3\xBAltimo mapa medido. \xC2\xA1No se encontraron mediciones de este tipo de datos!"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"N\xC3\xA3o foi poss\xC3\xADvel atualizar para o \xC3\xBAltimo mapa de conjunto de dados. Nenhum registro encontrado para este tipo de dados!"
	#tag EndConstant

	#tag Constant, Name = kLastCombinedCurrentMap, Type = String, Dynamic = True, Default = \"Last combined current map:", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Last combined current map:"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC3\x9Altimo mapa combinado de corrientes totales:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"\xC3\x9Altima mapa de correntes combinados:"
	#tag EndConstant

	#tag Constant, Name = kLastCombinedWaveHeightMap, Type = String, Dynamic = True, Default = \"Last combined wave map:", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Last combined wave map:"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC3\x9Altimo mapa combinado de oleaje:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"\xC3\x9Altima mapa de ondas combinados:"
	#tag EndConstant

	#tag Constant, Name = kLastCombinedWindMap, Type = String, Dynamic = True, Default = \"Last combined wind map:", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Last combined wind map:"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC3\x9Altimo mapa combinado de viento:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"\xC3\x9Altima mapa de vento combinado:"
	#tag EndConstant

	#tag Constant, Name = kLastDatasetIn, Type = String, Dynamic = True, Default = \"Last dataset in", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Last dataset in"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC3\x9Altima medici\xC3\xB3n en"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"\xC3\x9Altima medi\xC3\xA7\xC3\xA3o"
	#tag EndConstant

	#tag Constant, Name = kLastRadialCurrentMap, Type = String, Dynamic = True, Default = \"Last radial current map:", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Last radial current map:"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC3\x9Altimo mapa de corriente radial:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"\xC3\x9Altima mapa de corrente radial:"
	#tag EndConstant

	#tag Constant, Name = kMeasured, Type = String, Dynamic = True, Default = \"Measured ", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Measured "
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Se ha medido "
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Medido "
	#tag EndConstant

	#tag Constant, Name = kNoWarningIsSelected, Type = String, Dynamic = True, Default = \"No warning is selected", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No warning is selected"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"No hay ninguna alerta seleccionada"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Nenhum alerta \xC3\xA9 selecionado"
	#tag EndConstant

	#tag Constant, Name = kWarningInfo, Type = String, Dynamic = True, Default = \"Warning Info", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Warning Info"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Informaci\xC3\xB3n de alerta"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Informa\xC3\xA7\xC3\xB5es alerta"
	#tag EndConstant

	#tag Constant, Name = kWarningsInfoHelpTag, Type = String, Dynamic = True, Default = \"Display information on warning selected from the list.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Display information on warning selected from the list."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Muestra informaci\xC3\xB3n de la alerta seleccionada en la lista de alertas."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Muestra informaci\xC3\xB3n de la alerta seleccionada en la lista de alertas."
	#tag EndConstant

	#tag Constant, Name = kWarningsList, Type = String, Dynamic = True, Default = \"Warnings List", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Warnings List"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Lista de alertas"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Lista de alertas"
	#tag EndConstant

	#tag Constant, Name = kWarningsListHelpTag, Type = String, Dynamic = True, Default = \"List of warnings for latest dataset.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"List of warnings for latest dataset."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Lista de alertas de la \xC3\xBAltima medici\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Lista de alertas de la \xC3\xBAltima medici\xC3\xB3n."
	#tag EndConstant

	#tag Constant, Name = kWarnLocation, Type = String, Dynamic = True, Default = \"Location", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Location"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Zona"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Zona"
	#tag EndConstant

	#tag Constant, Name = kWarnValue, Type = String, Dynamic = True, Default = \"Value", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Value"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Valor"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Valor"
	#tag EndConstant


#tag EndWindowCode

#tag Events ImgHome
	#tag Event
		Sub PictureChanged()
		  'dim script as string
		  '
		  'script = "var elt = document.getElementById('" + me.ControlID + "_image');" + EndOfLine + _
		  '"elt.style.left = '0px';" + EndOfLine + _
		  '"elt.style.top = '0px';" + EndOfLine + _
		  '"elt.style.marginTop = '0px';" + EndOfLine + _
		  '"elt.style.marginLeft = '0px';" + EndOfLine + _
		  '"elt.style.width = '" + str(me.Width, "0") + "px';" + EndOfLine + _
		  '"elt.style.height = '" + str(me.Height, "0") + "px';"
		  '
		  'ExecuteJavaScript(script)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ODiag
	#tag Event
		Sub Dismissed()
		  SpectrumDialog.Hide
		  WaBuDialog.Hide
		  
		  clickMarkVisible=false
		  CanvasMap.Refresh
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events CanvasMap
	#tag Event
		Sub Paint(g as WebGraphics)
		  dim i, d, h as Integer
		  
		  d=cfgDV.WS_circleSize
		  h=floor(d/2)
		  g.ClearRect(0,0,me.Width,me.Height) //Delete everything
		  
		  if canvasVisible OR cfgDV.WS_blink=false then
		    
		    // Draw the warnings
		    for i=0 to typeAlertSelected.Ubound
		      if typeAlertSelected(i)=4 then
		        g.ForeColor = &cFF000066
		      else
		        g.ForeColor = &cFFC00066
		      end
		      g.PenWidth = 2
		      g.FillRoundRect(   round((lonAlertSelected(i)-App.blon )/App.mlon)-h   ,   round((latAlertSelected(i)-App.blat )/App.mlat)-h   ,d,d,h)
		    next
		    
		    if currentPictureType=0 then //Draw global warning if there is any
		      dim os as integer =round((cfgDV.WS_gridPixels-1)/2)
		      g.ForeColor = RGB(255,192,0,cfgDV.OA_Transparency)
		      g.PenWidth=2
		      for i=0 to GACurYellowX.Ubound
		        g.FillRoundRect(  GACurYellowX(i) - os  ,  GACurYellowY(i) - os  , cfgDV.WS_gridPixels , cfgDV.WS_gridPixels , 2 )
		      next
		      g.ForeColor = RGB(255,0,0,cfgDV.OA_Transparency)
		      g.PenWidth=2
		      for i=0 to GACurRedX.Ubound
		        g.FillRoundRect(  GACurRedX(i) - os  ,  GACurRedY(i) - os  , cfgDV.WS_gridPixels , cfgDV.WS_gridPixels , 2 )
		      next
		    end
		    
		    if currentPictureType=10 then //Draw global warning if there is any
		      dim os as integer =round((cfgDV.WS_gridPixels-1)/2)
		      g.ForeColor = RGB(255,192,0,cfgDV.OA_Transparency)
		      g.PenWidth=2
		      for i=0 to GAWavYellowX.Ubound
		        g.FillRoundRect(  GAWavYellowX(i) - os  ,  GAWavYellowY(i) - os  , cfgDV.WS_gridPixels , cfgDV.WS_gridPixels , 2 )
		      next
		      g.ForeColor = RGB(255,0,0,cfgDV.OA_Transparency)
		      g.PenWidth=2
		      for i=0 to GAWavRedX.Ubound
		        g.FillRoundRect(  GAWavRedX(i) - os  ,  GAWavRedY(i) - os  , cfgDV.WS_gridPixels , cfgDV.WS_gridPixels , 2 )
		      next
		    end
		    
		  end
		  
		  if indexAlertSelected>0 then
		    g.ForeColor = &c00000066
		    g.PenWidth = 2
		    g.DrawRoundRect(round((lonAlertSelected(indexAlertSelected-1)-App.blon )/App.mlon)-h   ,   round((latAlertSelected(indexAlertSelected-1)-App.blat )/App.mlat)-h   ,d,d,h)
		  end
		  
		  if app.SyWaBuSeaViewEnabled AND (currentPictureType=10 or currentPictureType=20) then
		    for i=0 to svwb.nPts-1
		      g.ForeColor = &cFFFFFF80
		      g.FillRoundRect(   svwb.pixelX(i) -5   ,    svwb.pixelY(i) -5   ,11,11,3)
		      
		      //SWB label background
		      g.FillRoundRect(   svwb.pixelX(i) +8   ,    svwb.pixelY(i) -2   , 7*svwb.ptName_pt(i).Len, 16, 1)
		      
		      g.ForeColor = &c00000000
		      g.PenWidth = 2
		      g.DrawRoundRect(   svwb.pixelX(i) -5   ,    svwb.pixelY(i) -5   ,11,11,3)
		      g.PenWidth = 1
		      g.DrawRoundRect(  svwb.pixelX(i) -2   ,    svwb.pixelY(i) -2    ,5,5,2)
		      
		      //SWB label
		      g.TextFont = "Helvetica"
		      g.TextSize = 12
		      g.DrawString(svwb.ptName_pt(i),svwb.pixelX(i)+10,svwb.pixelY(i)+10)
		      
		    next
		  end
		  
		  if clickMarkVisible then
		    g.ForeColor = &c00000066
		    g.PenWidth = 2
		    g.DrawRoundRect(   clickMarkX-5   ,   clickMarkY-5   ,10,10,3)
		    g.ForeColor = &cFF000066
		    g.PenWidth = 2
		    g.DrawLine(clickMarkX-8, clickMarkY, clickMarkX+8, clickMarkY)
		    g.DrawLine(clickMarkX, clickMarkY-8, clickMarkX, clickMarkY+8)
		  end if
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  dim rs as RecordSet
		  dim idixiy as string
		  dim qualityFilter, i as Integer
		  Dim ShowSpectrum as Boolean
		  ShowSpectrum = false
		  
		  clickMarkVisible=true
		  clickMarkX=X
		  clickMarkY=Y
		  CanvasMap.Refresh
		  
		  select case currentPictureType
		  case 0
		    qualityFilter=cfgDV.homeMapTypeCur
		  case 10
		    qualityFilter=cfgDV.homeMapTypeWav
		  case 20
		    qualityFilter=cfgDV.homeMapTypeWin
		  else
		    if currentPictureType>0 AND currentPictureType<=cfgDV.nStations then
		      qualityFilter=cfgDV.homeMapTypeCur
		    end
		  end
		  
		  // Display dialog with basic WERA measurement data
		  // NOTE that rs is passed ByRef to use the results further below
		  idixiy=ODiag.CheckAndDisplay(rs,X,Y,currentPictureType,IdHeaderCurrentPict,qualityFilter)
		  
		  ODiag.Left=X + me.Parent.Left + me.Left
		  ODiag.Top=Y + me.Parent.Top + me.Top
		  ODiag.Show
		  
		  // Do we need to display spectrum?
		  if currentPictureType=10 AND cfgDV.enableSPEC AND ODiag.BtnTD.Enabled AND rs<>Nil then   //ODiag.BtnTD.Enabled is used as condition to ensure that a valid point was clicked (inside grid, no watt). rs<>Nil is used to be sure that no query error ocurred, and is required before the rs.BOF on the next line.
		    
		    if not(rs.BOF) then  'If rs.BOF then quality of this point is not good enough, so we are problably clicking in an empty spot on the map... we should not show spectrum in this case.
		      
		      rs=db.GetWaveSpectralDensity(idixiy,IdHeaderCurrentPict)
		      
		      if rs=nil then
		      elseif rs.BOF then
		      else
		        dim data0(-1) as Double = array(rs.Field("f05").DoubleValue, rs.Field("f06").DoubleValue, rs.Field("f07").DoubleValue, rs.Field("f08").DoubleValue, rs.Field("f09").DoubleValue, rs.Field("f10").DoubleValue, rs.Field("f11").DoubleValue, rs.Field("f12").DoubleValue, rs.Field("f13").DoubleValue, rs.Field("f14").DoubleValue, rs.Field("f15").DoubleValue, rs.Field("f16").DoubleValue, rs.Field("f17").DoubleValue, rs.Field("f18").DoubleValue, rs.Field("f19").DoubleValue, rs.Field("f20").DoubleValue, rs.Field("f21").DoubleValue, rs.Field("f22").DoubleValue, rs.Field("f23").DoubleValue, rs.Field("f24").DoubleValue, rs.Field("f25").DoubleValue)
		        
		        rs=db.GetWaveMeanDirection(idixiy,IdHeaderCurrentPict)
		        if rs=nil then
		          return
		        elseif rs.BOF then
		          SpectrumDialog.DisplaySpectrum(X + me.Parent.Left + me.Left + ODiag.Width,Y + me.Parent.Top + me.Top, data0, data0, false)
		        else
		          dim data1(-1) as Double = array(rs.Field("f05").DoubleValue, rs.Field("f06").DoubleValue, rs.Field("f07").DoubleValue, rs.Field("f08").DoubleValue, rs.Field("f09").DoubleValue, rs.Field("f10").DoubleValue, rs.Field("f11").DoubleValue, rs.Field("f12").DoubleValue, rs.Field("f13").DoubleValue, rs.Field("f14").DoubleValue, rs.Field("f15").DoubleValue, rs.Field("f16").DoubleValue, rs.Field("f17").DoubleValue, rs.Field("f18").DoubleValue, rs.Field("f19").DoubleValue, rs.Field("f20").DoubleValue, rs.Field("f21").DoubleValue, rs.Field("f22").DoubleValue, rs.Field("f23").DoubleValue, rs.Field("f24").DoubleValue, rs.Field("f25").DoubleValue)
		          SpectrumDialog.DisplaySpectrum(X + me.Parent.Left + me.Left + ODiag.Width,Y + me.Parent.Top + me.Top, data0, data1, true)
		        end
		        ShowSpectrum=true
		        SpectrumDialog.SwitchDataBtn.Visible=false
		        SpectrumDialog.FullDirBtn.Visible=false
		        SpectrumDialog.Title=replace(SpectrumDialog.kTitleWERA(cfgDV.language),"WERA",std.kRadName)
		      end
		      
		    end if
		  end
		  
		  //Do we need to display sysnthetic wave buoy data?
		  if (currentPictureType=10 or currentPictureType=20) and app.SyWaBuSeaViewEnabled then
		    for i=0 to svwb.nPts-1
		      if CStr(svwb.id_ixiys(i))=idixiy then exit   'Searching if the point corresponds to a SWB
		    next
		    if i<svwb.nPts then 'The point corresponds to a SWB... we have to check if there is data...
		      rs=GetSVWaBuData(currentPictureDate.SQLDateTime,idixiy)
		      if rs=nil then
		      elseif rs.BOF then
		      else
		        dim wQual, invData, dirQual as Boolean
		        dim qual as integer
		        dim var1 as Double
		        qual=rs.Field("qual").IntegerValue
		        qual=svwb.DecodeQualVal(qual, wQual, invData, dirQual) 'Note that in this method, the inputs are defined as ByRef var as vartype, which means, that they would be modified during the method (something like having more that one input from one method...
		        
		        if (currentPictureType=10 AND (qual<=qualityFilter OR qualityFilter=0) ) OR (currentPictureType=20 AND ( (wQual AND qualityFilter>=3) OR qualityFilter=0) ) then 'Something has to be shown at least...
		          
		          WaBuDialog.Title=svwb.ptName_pt(i)
		          
		          if currentPictureType=10 then 'Wave data... only Hs or the whole thing?
		            if invData AND ((dirQual AND qualityFilter>=3) OR qualityFilter=0) then 'The whole thing!
		              WaBuDialog.PrepareForWave
		              WaBuDialog.HsText.Text=rs.Field("wa_height").StringValue + " m"
		              WaBuDialog.T1Text.Text=rs.Field("wa_mean_T").StringValue + " s"
		              WaBuDialog.TpText.Text=rs.Field("wa_peak_T").StringValue + " s"
		              var1=rs.Field("wa_mean_dir").IntegerValue
		              if var1<0 then var1=var1+360
		              WaBuDialog.T1dText.Text= CStr(var1) + " °"
		              var1=rs.Field("wa_peak_dir").IntegerValue
		              if var1<0 then var1=var1+360
		              WaBuDialog.TpdText.Text= CStr(var1) + " °"
		              
		              if svwb.ReadSpectrum(cfgDV.stdFolder + "SeaViewWaveBuoy/" + CStr(currentPictureDate.Year) + Str(currentPictureDate.Month,"0#") + Str(currentPictureDate.Day,"0#") + Str(currentPictureDate.Hour,"0#") + Str(currentPictureDate.Minute,"0#") + "_" + svwb.specPrefix_pt(i) + ".wbdat") then
		                SpectrumDialog.DisplaySpectrum(X + me.Parent.Left + me.Left + ODiag.Width,Y + me.Parent.Top + me.Top, svwb.energy, svwb.dir, true)
		                SpectrumDialog.buoy_index=i
		                if ShowSpectrum then 'There is WERA data available
		                  SpectrumDialog.SwitchDataBtn.Visible=true
		                  SpectrumDialog.SwitchDataBtn.Caption=replace(SpectrumDialog.kWERAData(cfgDV.language),"WERA",std.kRadName)
		                  SpectrumDialog.buoy_ixiy=idixiy
		                  SpectrumDialog.header=IdHeaderCurrentPict
		                else
		                  SpectrumDialog.SwitchDataBtn.Visible=false
		                  ShowSpectrum=true
		                end
		                SpectrumDialog.FullDirBtn.Visible=true
		                SpectrumDialog.CurrentDate=currentPictureDate
		                SpectrumDialog.Title=SpectrumDialog.kTitleBuoy(cfgDV.language)
		                SpectrumDialog.ShowingWaveBuoy=true
		              end if
		              
		            else
		              WaBuDialog.PrepareForOnlyHs
		              WaBuDialog.HsText.Text=rs.Field("wa_height").StringValue + " m"
		            end if
		            
		            
		            if qual=0 then
		              WaBuDialog.qText.Text=cfgDV.labelQual0
		            elseif qual=1 then
		              WaBuDialog.qText.Text=cfgDV.labelQual1
		            elseif qual=2 then
		              WaBuDialog.qText.Text=cfgDV.labelQual2
		            elseif qual=3 then
		              WaBuDialog.qText.Text=cfgDV.labelQual3
		            else
		              WaBuDialog.qText.Text=cfgDV.labelQual4
		            end if
		            
		            
		          else
		            WaBuDialog.PrepareForWind
		            WaBuDialog.HsText.Text=rs.Field("wi_spd").StringValue + " m/s"
		            var1=rs.Field("wi_dir").IntegerValue
		            if var1<0 then var1=var1+360
		            if cfgDV.windDirectionFrom then
		              if var1<180 then var1=var1+180 else var1=var1-180
		            end
		            WaBuDialog.T1Text.Text=CStr(var1) + " °"
		            
		            if wQual then WaBuDialog.qText.Text=cfgDV.labelQual3 else WaBuDialog.qText.Text=cfgDV.labelQual4
		            
		          end if
		          
		          WaBuDialog.Left=X + me.Parent.Left + me.Left
		          WaBuDialog.Top=Y + me.Parent.Top + me.Top + ODiag.Height+20
		          WaBuDialog.Show
		          
		        end
		        
		      end
		    else
		      WaBuDialog.Hide
		    end
		  else
		    WaBuDialog.Hide
		  end
		  
		  //The following will check if either WERA or SWB have a spectrum to show, and then show it.
		  if ShowSpectrum then
		    SpectrumDialog.show
		  else
		    SpectrumDialog.hide
		  end
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.HelpTag=Main.kCanvasMapHelpTag(cfgDV.language)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events blink
	#tag Event
		Sub Action()
		  
		  if canvasVisible then
		    canvasVisible=false
		  else
		    canvasVisible=true
		  end
		  
		  CanvasMap.Refresh
		  
		  // Update warning in buttons
		  
		  if BtnCurrent.Enabled then
		    if canvasVisible AND ( statusAlertCur>1 OR GACurRedX.Ubound>=0 OR GACurYellowX.Ubound>=0 )then
		      if statusAlertCur=4 then
		        BtnCurrent.Style=BtnCommonFontRed
		      else
		        BtnCurrent.Style=BtnCommonFontYellow
		      end
		    else
		      BtnCurrent.Style=BtnCommonFont
		    end
		  end
		  
		  if BtnWave.Enabled then
		    if canvasVisible AND statusAlertWav>1 then
		      if statusAlertWav=4 then
		        BtnWave.Style=BtnCommonFontRed
		      else
		        BtnWave.Style=BtnCommonFontYellow
		      end
		    else
		      BtnWave.Style=BtnCommonFont
		    end
		  end
		  
		  if BtnWind.Enabled then
		    if canvasVisible AND statusAlertWin>1 then
		      if statusAlertWin=4 then
		        BtnWind.Style=BtnCommonFontRed
		      else
		        BtnWind.Style=BtnCommonFontYellow
		      end
		    else
		      BtnWind.Style=BtnCommonFont
		    end
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnCurrent
	#tag Event
		Sub Action()
		  ODiag.Close
		  
		  BtnCurrent.Enabled=false
		  BtnCurrent.Style=BtnCommonFontDisabled
		  RecCurrent.Visible=true
		  
		  BtnWave.Enabled=true
		  BtnWave.Style=BtnCommonFont
		  RecWave.Visible=false
		  
		  BtnWind.Enabled=true
		  BtnWind.Style=BtnCommonFont
		  RecWind.Visible=false
		  
		  'BtnFcstCur.Enabled=true
		  'BtnFcstCur.Style=BtnCommonFont
		  ''RecFcstCur.Visible=false
		  
		  if cfgDV.initialMapType<10 then  // this should be used for the case there is only one station.
		    currentPictureType=cfgDV.initialMapType
		  else
		    currentPictureType=0
		  end
		  
		  UpdateMapHome
		  
		  if App.OceanWarningSystemEnabled then
		    UpdateOceanAlertData()
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnWave
	#tag Event
		Sub Action()
		  ODiag.Close
		  
		  BtnCurrent.Enabled=true
		  BtnCurrent.Style=BtnCommonFont
		  RecCurrent.Visible=false
		  
		  BtnWave.Enabled=false
		  BtnWave.Style=BtnCommonFontDisabled
		  RecWave.Visible=true
		  
		  BtnWind.Enabled=true
		  BtnWind.Style=BtnCommonFont
		  RecWind.Visible=false
		  
		  'BtnFcstCur.Enabled=true
		  'BtnFcstCur.Style=BtnCommonFont
		  ''RecFcstCur.Visible=false
		  
		  currentPictureType=10
		  
		  UpdateMapHome
		  
		  if App.OceanWarningSystemEnabled then
		    UpdateOceanAlertData()
		  end
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnWind
	#tag Event
		Sub Action()
		  ODiag.Close
		  
		  BtnCurrent.Enabled=true
		  BtnCurrent.Style=BtnCommonFont
		  RecCurrent.Visible=false
		  
		  BtnWave.Enabled=true
		  BtnWave.Style=BtnCommonFont
		  RecWave.Visible=false
		  
		  BtnWind.Enabled=false
		  BtnWind.Style=BtnCommonFontDisabled
		  RecWind.Visible=true
		  
		  'BtnFcstCur.Enabled=true
		  'BtnFcstCur.Style=BtnCommonFont
		  ''RecFcstCur.Visible=false
		  
		  currentPictureType=20
		  
		  UpdateMapHome
		  
		  if App.OceanWarningSystemEnabled then
		    UpdateOceanAlertData()
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events WarningsList
	#tag Event
		Sub CellClick(Row As Integer, Column As Integer)
		  WarningListCellClick(row)
		End Sub
	#tag EndEvent
	#tag Event
		Sub SelectionChanged()
		  if WarningsList.ListIndex=-1 then
		    indexAlertSelected=0
		    CanvasMap.Refresh
		    WarningsInfo.Text=kNoWarningIsSelected
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnFcstCur
	#tag Event
		Sub Action()
		  ODiag.Close
		  
		  
		  Dim FI as New FolderItem
		  Dim Pict1 as WebPicture
		  FI=GetFolderItem(cfgDV.stdFolder + "Arret_outbox/curextrap/animation.gif")
		  if FI=Nil then
		    MsgBox("Forecast animation path is Nil")
		  else
		    if FI.Exists then
		      Pict1=new WebPicture(FI)
		      if Pict1 = Nil then  // A 'Nil' value means a problem during Picture object creation
		        MsgBox("Forecast animation file is Nil")
		      else
		        Session.StdWebFile=Pict1  // if the WebFile is initialized like this then ForceDownload does not work
		        Session.StdWebFile.ForceDownload=false
		        ExecuteJavaScript("window.open('"+ Session.StdWebFile.URL + "');") // If using this, the WebFile will be opened (or downloaded if ForceDownload works) in a new window... but pop ups must be allowed.
		        
		      end
		    else
		      MsgBox("Forecast animation files does not exists")
		    end
		  end if
		  
		  
		  
		  
		  
		  
		  '
		  'BtnCurrent.Enabled=true
		  'BtnCurrent.Style=BtnCommonFont
		  'RecCurrent.Visible=false
		  '
		  'BtnWave.Enabled=true
		  'BtnWave.Style=BtnCommonFont
		  'RecWave.Visible=false
		  '
		  'BtnWind.Enabled=true
		  'BtnWind.Style=BtnCommonFont
		  'RecWind.Visible=false
		  '
		  'BtnFcstCur.Enabled=false
		  'BtnFcstCur.Style=BtnCommonFontDisabled
		  ''RecFcstCur.Visible=true
		  '
		  'currentPictureType=30
		  '
		  'UpdateMapHome
		  '
		  'if App.OceanWarningSystemEnabled then
		  'UpdateOceanAlertData()
		  'end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="canvasVisible"
		Group="Behavior"
		InitialValue="false"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="clickMarkVisible"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="clickMarkX"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="clickMarkY"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="currentPictureType"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="GACurMax"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="GAWavMax"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="IdHeaderCurrentPict"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="indexAlertSelected"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="pathLoading"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="sFolder"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="sSufix"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="statusAlertCur"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="statusAlertWav"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="statusAlertWin"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="timerCounter"
		Group="Behavior"
		InitialValue="10"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="timeStampGlobalAlertCur"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="timeStampGlobalAlertWav"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
