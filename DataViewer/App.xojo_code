#tag Class
Protected Class App
Inherits WebApplication
	#tag Event
		Sub Open(args() as String)
		  
		  
		  if Right(System.CommandLine,2)="-v" then ' the user only wants to know the version number and not execute the DataViewer server. Show it and quit.
		    dim tmpString as String
		    if App.StageCode=3 then
		      tmpString=kAppName + " V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + " (" + App.ExecutableFile.ModificationDate.SQLDate + ")" + EndOfLine
		    else
		      tmpString=kAppName + " V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + " (" + App.ExecutableFile.ModificationDate.SQLDate + ")" + " Beta" + EndOfLine
		    end
		    MsgBox(tmpString)
		    Quit
		    
		  else
		    
		    
		    CDBaseChartMBS.SetLicenseCode "Helzel Meßtechnik GmbH", 201208, 843479311, -1493023638
		    
		    // Chart Director uses microsoft truetype fonts (arial, comic sans and so on)... if using linux then these fonts must be downloaded and the location of these fonts must be indicated
		    // You have to install the 'fetchmsttfonts' package that contains the fetchmsttfonts helper script, which on running retrieves and unpacks the freely available MS Truetype fonts.
		    // Downloading the package seems to be neccesary only for compiling. After compiling the DataViewer can be copied to a linux computer that does not have the packages installed.
		    if TargetLinux then
		      CDBaseChartMBS.SetFontSearchPath "/usr/share/fonts/truetype"
		    else
		      // on Mac and Windows, system fonts are used.
		    end if
		    
		    if not(std.ActivateMBSComplete()) then log.Write(true,"MBS plugin not valid?")
		    
		    // Are we running in debbuging mode?
		    if DebugBuild or Right(App.ExecutableFile.Parent.AbsolutePath,8)="cgi-bin/" then  // Also check if running as cgi, and assign a fixed folder for everything else to work properly.
		      App.execDir="/home/wera/ApplicationSoftware/DataViewer/"
		    else
		      App.execDir=App.ExecutableFile.Parent.AbsolutePath
		    end
		    
		    if not(cfgDV.InitializeConfig(" ")) then
		      log.Write(true,kOpenErrParams(cfgDV.language))
		      StartErrorMessages=StartErrorMessages +"-" + kOpenErrParams(cfgDV.language) + EndOfLine
		    end
		    
		    //Check database
		    db.archive=new MySQLDatabase
		    if not(db.ReadDBSetupFile(App.execDir + "../DataArchivePrograms/DataArchive_setup.txt")) then
		    end
		    db.grid=CStr(cfgDV.gridID)
		    db.sites=cfgDV.nStations
		    
		    if not(db.CheckDatabaseConnection()) then
		      StartErrorMessages=StartErrorMessages + "-" + db.lastMessage_db + EndOfLine
		    end
		    
		    if siteInfo.Initialize() then
		      StartErrorMessages=StartErrorMessages +"-" + kOpenErrSiteParams(cfgDV.language) + EndOfLine
		    end
		    
		    //Initiate the CriticalSection instances
		    plotFilePass= new CriticalSection
		    animFilePass= new CriticalSection
		    
		    if UpdateAppVariables() then
		      log.Write(true,kOpenErrUpdateVars(cfgDV.language))
		      StartErrorMessages=StartErrorMessages +"-" + kOpenErrUpdateVars(cfgDV.language) + EndOfLine
		    end
		    
		    //=========Is the system composed only by one station?==========
		    if cfgDV.nStations=1 then  //Avoid problems due to the absence of parameters for station 2
		      cfgDV.sFolder_st.Append(cfgDV.sFolder_st(0))
		      cfgDV.sSufix_st.Append(cfgDV.sSufix_st(0))
		    end
		    
		    //============Load access accounts==============
		    if access.LoadAccounts then
		      log.Write(true,kOpenErrLoadAccess(cfgDV.language))
		      StartErrorMessages=StartErrorMessages +"-" + kOpenErrLoadAccess(cfgDV.language) + EndOfLine
		    end
		    
		    
		    //============Licence check================
		    if key.LoadLicenceFile(App.execDir + "../") then
		      
		      if key.LicenceIsValidOnThisPC then
		        
		        if not( key.PackageIsValid(key.Packages.DataViewer))  then
		          
		          log.Write(true,kOpenErrUnlicensedPack(cfgDV.language) + "DataViewer.")
		          StartErrorMessages=StartErrorMessages +"-" + kOpenErrUnlicensedPack(cfgDV.language) + "DataViewer." + EndOfLine
		          
		        else
		          
		          if cfgDV.WS_enabled then
		            
		            if not( key.PackageIsValid(key.Packages.OceanWarning))  then
		              log.Write(true,kOpenErrUnlicensedPack(cfgDV.language) + "Ocean Warning.")
		              StartErrorMessages=StartErrorMessages +"-" + kOpenErrUnlicensedPack(cfgDV.language) + "Ocean Warning." + EndOfLine
		            else
		              OceanWarningSystemEnabled=true
		            end
		            
		          end
		          
		          if cfgDV.enableForecastCCV then
		            
		            if not( key.PackageIsValid(key.Packages.GF_Forecast))  then
		              log.Write(true,kOpenErrUnlicensedPack(cfgDV.language) + "Forecasting.")
		              StartErrorMessages=StartErrorMessages +"-" + kOpenErrUnlicensedPack(cfgDV.language) + "Forecasting." + EndOfLine
		            else
		              forecastEnabled=true
		            end
		            
		          end
		          
		          if cfgDV.WaBuSV_enabled then
		            
		            if not( key.PackageIsValid(key.Packages.SV_SWB))  then
		              log.Write(true,kOpenErrUnlicensedPack(cfgDV.language) + "Seaview's Synthetic Wave Buoy.")
		              StartErrorMessages=StartErrorMessages +"-" + kOpenErrUnlicensedPack(cfgDV.language) + "Seaview's Synthetic Wave Buoy." + EndOfLine
		            else
		              SyWaBuSeaViewEnabled=true
		              
		              if not(svwb.InitializeConfig(execDir + "../SeaViewWaveBuoy/config.cfg")) then
		                log.Write(true,kOpenErrLoadSWB(cfgDV.language))
		                StartErrorMessages=StartErrorMessages +"-" + kOpenErrLoadSWB(cfgDV.language) + EndOfLine
		              end
		              if CalculateForSVWaBu then
		                log.Write(true,kOpenErrCalcSWB(cfgDV.language))
		                StartErrorMessages=StartErrorMessages +"-" + kOpenErrCalcSWB(cfgDV.language) + EndOfLine
		              end
		            end
		            
		          end
		          
		        end if
		        
		      else
		        log.Write(true,key.lastMessage_key)
		        StartErrorMessages=StartErrorMessages +"-" + kOpenErrValidLic(cfgDV.language) + " " + key.lastMessage_key + EndOfLine
		      end
		      
		    else
		      
		      log.Write(true,kOpenErrLoadLic(cfgDV.language) + " " + key.lastMessage_key)
		      StartErrorMessages=StartErrorMessages +"-" + kOpenErrLoadLic(cfgDV.language) + " " + key.lastMessage_key + EndOfLine
		      
		    end
		    
		    
		    
		    
		    
		    
		    //================ Updater (Used to update ocean alerts, and system status) ====================
		    lastCurMapDate=new Date
		    lastCurMapDate.SQLDateTime="1111-11-11 11:11:11"
		    lastWavMapDate=new Date
		    lastWavMapDate.SQLDateTime="1111-11-11 11:11:11"
		    lastWinMapDate=new Date
		    lastWinMapDate.SQLDateTime="1111-11-11 11:11:11"
		    lastSt1CurDate=new Date
		    lastSt1CurDate.SQLDateTime="1111-11-11 11:11:11"
		    lastSt2CurDate=new Date
		    lastSt2CurDate.SQLDateTime="1111-11-11 11:11:11"
		    lastSt1WavDate=new Date
		    lastSt1WavDate.SQLDateTime="1111-11-11 11:11:11"
		    lastSt2WavDate=new Date
		    lastSt2WavDate.SQLDateTime="1111-11-11 11:11:11"
		    lastFcstCurDate=new Date
		    lastFcstCurDate.SQLDateTime="1111-11-11 11:11:11"
		    
		    
		    if StartErrorMessages="" then
		      
		      // Update all things NOW... and while the update runs in parallel, start a timer that will trigger the automatic update every certain time
		      Dim UpdaterThread as new UpdaterThreadClass
		      UpdaterThread.Run
		      
		      UpdaterTimer= new UpdaterTimerClass
		      UpdaterTimer.Period=15000
		      UpdaterTimer.Mode=2
		      UpdaterTimer.Reset
		    end
		    
		    
		  end if
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function UnhandledException(Error As RuntimeException) As Boolean
		  
		  
		  dim errorString, stackArray() as String
		  dim i as integer
		  
		  errorString=kUEstr1(cfgDV.language) + EndOfLine + EndOfLine
		  
		  if App.StageCode=3 then
		    errorString=errorString + replace(kUEstr2(cfgDV.language), "Helzel Messtechnik GmbH", std.kComName)
		  else
		    errorString=errorString + replace(kUEBeta(cfgDV.language), "WERA", std.kRadName) + " " + replace(kUEstr2(cfgDV.language), "Helzel Messtechnik GmbH", std.kComName)
		  end
		  
		  errorString=errorString + EndOfLine + EndOfLine + kUEExcepDetails(language) + EndOfLine + kUEErrNum + " " + CStr(Error.ErrorNumber) + ": " + Error.Message + EndOfLine + EndOfLine
		  errorString=errorString + kUEExcepSource + EndOfLine
		  
		  stackArray=error.Stack
		  
		  for i=0 to stackArray.Ubound
		    errorString=errorString + stackArray(i) + EndOfLine
		  next
		  
		  log.Write(true,errorString)
		  
		  return true   ' To avoid the standard window from Xojo which says anyway the same as the previous message.
		End Function
	#tag EndEvent


	#tag Method, Flags = &h0
		Function CalculateForSVWaBu() As Boolean
		  
		  // Calculates many parameters that are required for DataViewer to display  and the synthetic wave buoys and their data
		  
		  dim i as Integer
		  dim rs as RecordSet
		  
		  Redim svwb.id_ixiys(svwb.nPts-1)
		  Redim svwb.pixelX(svwb.nPts-1)
		  Redim svwb.pixelY(svwb.nPts-1)
		  Redim svwb.gridX(svwb.nPts-1)
		  Redim svwb.gridY(svwb.nPts-1)
		  
		  for i=0 to (svwb.nPts-1)
		    rs=db.GetPtInfoFromApproxCoords(svwb.lat_pt(i)-halflatstep, svwb.lat_pt(i)+halflatstep, svwb.lon_pt(i)-halflonstep, svwb.lon_pt(i)+halflonstep)
		    
		    if rs=nil then
		      return true
		    elseif rs.BOF then
		      log.Write(false,"No grid point found in database for synthetic wave buoy coordinates " + CStr(svwb.lat_pt(i)) + ", " + CStr(svwb.lon_pt(i)) + ".")
		      svwb.id_ixiys(i)=-999
		      svwb.gridX(i)=-999
		      svwb.gridY(i)=-999
		    else
		      svwb.id_ixiys(i)=rs.Field("id_tb_ixiy").IntegerValue
		      svwb.gridX(i)=rs.Field("ix").IntegerValue
		      svwb.gridY(i)=rs.Field("iy").IntegerValue
		    end if
		    
		  next
		  
		  for i=0 to (svwb.nPts-1)
		    svwb.pixelX(i)= round((svwb.lon_pt(i)-App.blon )/App.mlon)
		    svwb.pixelY(i)= round((svwb.lat_pt(i)-App.blat )/App.mlat)
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function convert_gurgeltime_to_dbstring(input as String) As String
		  'Funktion wird der Dateiname übergeben wandelt Gurgels Zeitcodierung ins Datenbakformat um
		  'z.B. 20080011200 --> 2008-01-01 12:00:00
		  Dim i As Integer
		  Dim sec as String
		  Dim min As String
		  Dim hour As String
		  Dim dayofyear As integer
		  Dim day As integer
		  Dim month As integer
		  Dim year As integer
		  
		  Dim montharray1(13) as Integer
		  Dim montharray2(13) as Integer
		  
		  Dim sk As integer                      'Korrekturwert für Schaltjahr
		  Dim result As String
		  
		  montharray1 = Array(0,31,59,90,120,151,181,212,243,273,304,334,365)  'Anzahl Tage ohne Schaltjahr
		  montharray2 = Array(0,31,60,91,121,152,182,213,244,274,305,335,366)  'Anzahl Tage mit Schaltjahr
		  
		  year=Val(Left(input,4))                'Jahr auslesen, Stelle 1-4
		  dayofyear=Val(Mid(input,5,3))    'Tag des Jahres auslesen, Stelle 5-8
		  hour=Mid(input,8,2)                     'Stunden auslesen, Stelle 8-9
		  min=Mid(input,10,2)                     'Minuten auslesen, Stelle 10-11
		  
		  if IsNumeric(Mid(input,12,2)) then
		    // It also has seconds!!!
		    sec=Mid(input,12,2)
		  else
		    sec="00"
		  end
		  
		  'Schaltjahr prüfen
		  If year Mod 4 = 0 And year Mod 100 = 0 Then
		    If year Mod 400 = 0 Then                                   ' Durch 400 teilbar?
		      sk=1
		    Else ' Nicht ganzzahlig teilbar.
		      sk=0
		    End If
		    
		  ElseIf year Mod 4 = 0 Then
		    sk=1
		  Else
		    sk=0
		  End If
		  
		  'Umrechnung TagdesJahres in Monat und Tag
		  
		  for i=0 to 12 Step 1
		    if sk=0 Then  'kein Schaltjahr
		      if dayofyear>=montharray1(i) and dayofyear<=montharray1(i+1) Then
		        day=dayofyear-montharray1(i)
		        month=i+1
		        exit
		      end if
		      
		    else
		      if dayofyear>=montharray2(i) and dayofyear<=montharray2(i+1) Then
		        day=dayofyear-montharray2(i)
		        month=i+1
		        exit
		      end if
		    end if
		    
		  next
		  
		  result= CStr(year)+"-"+Str(month,"0#") +"-"+ Str(day,"0#") + " "+hour+":"+min+":"+sec
		  'MsgBox result
		  
		  Return result                                         'Ergebnis in Minuten zurückgeben
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UpdateAppVariables() As Boolean
		  // This method is usually used after reading the parameters of DataViewerParams.cfg with cfgDV.InitializeConfig, and updates the value of parameters used by the app in general so that DataViewer works correctly.
		  
		  // Calculation of line equation parameters (slope and bias values) used to estimate geographic coordinates out of pixel coordinates of click at maps
		  blon=((cfgDV.LonRefPt1*cfgDV.PxRefPt2)-(cfgDV.LonRefPt2*PxRefPt1))/(PxRefPt2-PxRefPt1)
		  mlon=(cfgDV.LonRefPt2-cfgDV.LonRefPt1)/(PxRefPt2-PxRefPt1)
		  blat=((cfgDV.LatRefPt1*cfgDV.PyRefPt2)-(cfgDV.LatRefPt2*PyRefPt1))/(PyRefPt2-PyRefPt1)
		  mlat=(cfgDV.LatRefPt2-cfgDV.LatRefPt1)/(PyRefPt2-PyRefPt1)
		  
		  // latyp1 and latyp2 = latitude value in the y direction for point 1 or 2 (top to bottom hierarchy)
		  // lon = longitude value in the x direction for point 1 or 2 (left to right hierarchy)
		  // x = x pixel value in the x direction for point 1 or 2 (left to right hierarchy)
		  // y = y pixel value in the y direction for point 1 or 2 (top to bottom hierarchy)
		  // The location of the stations may be used to get these values
		  
		  if UpdateGridData(cfgDV.gridID) then
		    return true
		  end
		  
		  // calculation of the half of the step distance between one index and the next (in the WERA grid)
		  halflonstep=abs(Ceil(grdlonstep*100000)/100000)/2  //abs(Ceil(lonstep*100000)/100000)/2
		  halflatstep=abs(Ceil(grdlatstep*100000)/100000)/2 //abs(Ceil(latstep*100000)/100000)/2
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UpdateGridData(idGrid as Integer) As boolean
		  dim sqlRecordSet as RecordSet
		  dim i, tmpnx, tmpny, total as integer
		  
		  'App.AutoQuit=false //For CGI applications this is set to true as standard. We set it to false.
		  //call Daemonize   'Should we use this?
		  
		  // Get grid size
		  sqlRecordSet=db.GetGridData()
		  if sqlRecordSet=Nil then
		    return true
		  else
		    if sqlRecordSet.BOF then
		      log.Write(true,"No grid found for grid id = " + CStr(idGrid))
		      return true
		    end
		    //Temporarily storing the grid size
		    tmpnx=sqlRecordSet.Field("nx").IntegerValue
		    tmpny=sqlRecordSet.Field("ny").IntegerValue
		  end
		  
		  // Get indexes of each point
		  sqlRecordSet=GetIdIxIyFromGridID(0)
		  
		  if sqlRecordSet=Nil then
		    return true
		  else
		    if sqlRecordSet.BOF then
		      log.Write(true,"No points found for the grid with id = " + Cstr(idGrid) + " , update interrupted.")
		      return true
		    else  // we now know that there is valid data... proceed to update previous values
		      
		      total=sqlRecordSet.RecordCount
		      grdnx=tmpnx
		      grdny=tmpny
		      Redim grdid(total-1)
		      Redim grdx(total-1)
		      Redim grdy(total-1)
		      
		      for i=0 to total-1
		        grdid(i)=sqlRecordSet.Field("id_tb_ixiy").IntegerValue
		        grdx(i)=sqlRecordSet.Field("ix").IntegerValue
		        grdy(i)=sqlRecordSet.Field("iy").IntegerValue
		        sqlRecordSet.MoveNext
		      next
		      
		      sqlRecordSet=db.GetPtDataFromIxIy(1,1)
		      grdminlon=sqlRecordSet.Field("lon_grd").DoubleValue
		      grdminlat=sqlRecordSet.Field("lat_grd").DoubleValue
		      
		      sqlRecordSet=db.GetPtDataFromIxIy(grdnx,grdny)
		      grdmaxlon=sqlRecordSet.Field("lon_grd").DoubleValue
		      grdmaxlat=sqlRecordSet.Field("lat_grd").DoubleValue
		      
		      grdlonstep=(grdmaxlon-grdminlon)/(grdnx-1)
		      grdlatstep=(grdmaxlat-grdminlat)/(grdny-1)
		    end
		  end
		  
		  return false
		End Function
	#tag EndMethod


	#tag Note, Name = Version_Notes
		
		
		HOW TO FILL THIS NOTES...
		
		=Filename= (Notes... if applies. e.g. Version compiled for MELCO)
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		App version (Built using App.MajorVersion and App.NonReleaseVersion. CHECK THIS VALUES AFTER COMPILING... COMPILING INCREASES THE VALUE AUTOMATICALLY!), 
		Real Studio version when complied (check it on Hilfe>Über RealStudio) / Operative system (compiled with linux or windows)
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new version in the next line, not at the end!!! Latest version info should ALWAYS be displayed first!!!
		
		=DataViewer_V4_160122.xojo_project=
		2016.01.22
		Roberto Gomez
		Version 3.7 BETA
		Xojo 2015 R1 / Windows 7
		-Version compiled for testing purposes only, came back to major version 3 for this.
		
		=================================================================================================
		=================================================================================================
		
		All following notes correspond to the changes done by Roberto Gomez while working in Mexico
		with the DataViewer translation to portuguese for IACIT customer. GIT methodology for keeping
		track of source code changes was used and following notes were extracted from GIT client program
		(SourceTree).
		
		>
		
		Date: lunes, 08 de agosto de 2016 10:13:23 a.m.
		-Changed RADH to WERA, and changed folder name from CommonObjects to CommonCodeObjects.
		-Last commit before moving to new folder structure in HZM server so that it can be easily synced.
		
		Date: lunes, 08 de agosto de 2016 08:42:27 a.m.
		-Changed RADH instances back to WERA and made some spanish text smaller.
		
		Date: jueves, 18 de febrero de 2016 12:18:01 a.m.
		-Updated the portuguese text in several localizable strings.
		
		Date: miércoles, 17 de febrero de 2016 10:28:17 p.m.
		-Finalized implementation of localizable strings (language dynamic) on the whole app. Spanish is fully implemented already and portuguese partially (need to be verified by native speaker, some strings are still in spanish).
		-Changes several cosmetic properties in several controls.
		
		Date: martes, 16 de febrero de 2016 09:58:28 p.m.
		-Implemented language dynamic strings (localization) in OceanData, except on the Methods. Methods are still pending to check.
		
		Date: martes, 16 de febrero de 2016 06:00:04 p.m.
		-Implemented language dynamic strings (localization) in OceanData page, as well as some other common strings in TimeData and OceanData.
		-Changed selection style of the LstAvailMeas list box in both OceanData and TimeData to match with the gray color used in the calendar and other objects.
		
		Date: martes, 16 de febrero de 2016 01:19:47 p.m.
		-Finalized localization of RadarData page
		
		Date: martes, 16 de febrero de 2016 11:53:39 a.m.
		-Added pending localization of strings in HomeData.open event
		-Started localization of RadarData page
		
		Date: martes, 16 de febrero de 2016 11:40:59 a.m.
		-Implemented localization (language dynamic strings) in home page.
		
		Date: martes, 16 de febrero de 2016 09:41:55 a.m.
		-Revert "Inserted a simple note to test reverse commit"
		-This reverts previous commit
		
		Date: martes, 16 de febrero de 2016 09:41:13 a.m.
		-Inserted a simple note to test reverse commit
		
		Date: lunes, 15 de febrero de 2016 10:57:12 p.m.
		UPDATE FROM LAST COMMIT:
		-Plots generated are already language dynamic
		
		Date: lunes, 15 de febrero de 2016 10:52:33 p.m.
		-Made all constants that start with kName... to be language dynamic. Added more kName constants to account for 'accuracy' in the name as well as some other situations. Implemented such constants to be language dynamic all over the source code.
		-Changed several case statements to if then, elseif then... and so on in order to compile language dynamic constants inside case statements.
		
		Date: miércoles, 10 de febrero de 2016 11:26:41 p.m.
		-Additional localizable (language-dynamic) strings in OceanDialog and SpecDialog.
		-Updated MouseDown events of canvas maps to correctly use localized strings on dialogs.
		-Modified WebCalendar to localizable (language-dynamic) month names.
		
		Date: miércoles, 10 de febrero de 2016 09:15:57 p.m.
		-Made several localizable strings on the dialog objects inside the Dialogs folder (the loading dialogs when creating animations, graphics or files, and the dialogs after clicking in the map).
		-Moved some code from the MouseDown event of the canvas objects in home page and OceanData page (where the map is displayed) to a method inside OceanDialog to make it more readable and avoid repeating code.
		
		Date: domingo, 07 de febrero de 2016 09:00:01 p.m.
		-Deleted the strDV module and distributed the localizable strings on the object where they are used. It is better like this to avoid so many constants in one module, to ease the search for them when developing the program and it is not really required to have everything in a module for the lingua app.
		-All strings used on all the web app except the webcontainers have been localized already. Many still have a real portuguese translation. The ones without portuguese translation have a spanish one for the moment.
		-Improved visualization and added style to the LoginDialog.
		-Changed all Data Viewer strings to DataViewer (no space)
		-Adding http:// to the cfgDV.ci_link string when assigning to the URL property of the WebLink, because parameter ci_link cannot contain // since it will be read as a comment.
		-Created Main.ShowCopyRightNotice method to avoid repeating code
		
		Date: viernes, 05 de febrero de 2016 08:22:06 a.m.
		-Implemented localizable or language-dinamic strings for Login and LoginDialogDef windows, and for Session.ShowWelcomeMessage method. Still pending localization of error strings during App.Open event.
		
		Date: martes, 02 de febrero de 2016 09:47:53 a.m.
		-Implemented localizable or language-dynamic string constants for the main page.
		
		Date: lunes, 01 de febrero de 2016 05:52:15 p.m.
		-Inserted the module strDV to store all strings which will be localizable according to language. Implemented already the name of pages in the page navigator for spanish, english and portuguese (kRealTime, kMapsArchive, kSystemInfo, kTimeSeries, kOceanWarningConfig).
		-Changed language to english in shared build settings
		-Added the parameter language into cfgDV to fix to one language only. Optionally can be set to 'auto' for automatic detection of preferred language on client's browser. Deleted additional method AssignParameterValue1 which I do not know where it came from.
		
		Date: lunes, 01 de febrero de 2016 11:51:04 a.m.
		-Now changed all sub-branding constants to the std module (kRadName, kComName, kContactEmail), so the key module can access to those as well. Also added a new constant std.kWebPage also used on some strings and some properties.
		
		Date: lunes, 01 de febrero de 2016 11:00:56 a.m.
		-Added constant App.kComName and App.kContactEmail to use in case of sub-branding WERA to another commercial name with details from another company. Set values of constants to IACIT and info@iacit.com.br. Implemented usage of those constants where neccesary.
		-Also set constants kAppName and kRadName to RADH DataViewer and RADH.
		
		Date: viernes, 29 de enero de 2016 06:50:21 p.m.
		-implemented usage of App.kRadName for cases of sub-branding the name of WERA (for IACIT, the name is RAD200H)
		
		Date: viernes, 22 de enero de 2016 08:05:49 p.m.
		-Introduced pattern in SourceTree program to ignore all files with "Debug"
		-Inserted a constant App.kRadName which is thought to be used later to replace all words of WERA with a subbrand of radar (like RAD200H for IACIT)
		
		Date: viernes, 22 de enero de 2016 07:29:47 p.m.
		-Changed top and height values of corporate identity objects under the navigation bar
		
		Date: viernes, 22 de enero de 2016 07:16:24 p.m.
		-First version testing git source control using GitHub and SourceTree
		
		=================================================================================================
		=================================================================================================
		
		=DataViewer_V4_160122.xojo_project=
		2016.01.22
		Roberto Gomez
		Not yet compiled
		-Changed to xojo_project format (text format usable with GIT source control)
		-Increased to major version 4, due to changed in xojo format
		-Implemented usage of cfgDV.ci_headerImage
		
		=DataViewer_V3_150709.xojo_binary_project=
		2015.07.09
		Marek Swirgon
		Version 3.6 BETA
		Xojo 2015 R1 / Windows 7
		-Slightly increased the width and height of the WebCalendar controls in OceanData and TimeData pages to avoid trim of right and lower border lines. (didn't happen before... don't know why now)
		-Slightly increased the width and height of the PageNavi1 control in Main page to avoid trimming appearance of displacement bars. (didn't happen before... don't know why now)
		-The login window is not resizable anymore.
		-Changed the pop-up message when UnhandledException occurs. Not telling anymore to the user to send the report, instead telling that the report will be automatically sent.
		-Included a return true at the end of App.UnhandledException event to suppress the standard Xojo window for unhandled exceptions.
		-Included a Try ... catch err As UnsupportedFormatException statement when loading corrupted maps in Home page and Ocean Maps page.
		
		=DataViewer_V3_150701.xojo_binary_project=
		2015.07.01
		Marek Swirgon
		Version 3.5 BETA
		Xojo 2015 R1 / Windows 7
		-Change Licence Validation (for CPU names without available speed, and reading numbers of processors from /proc/cpuinfo)
		-There is a problem with  'ct.MsgLog(true,db.lastMessage_db,true )
		
		=DataViewer_V3_150219.rbp=
		2015.02.19
		Roberto Gomez
		V3.4 BETA
		Xojo 2014 R3.2 / Windows7
		-Modified the shown event of RadarData page to not do anything when cfgDV.useWuTWebIO=false. This would avoid an unhandled exception for the case of more than 2 stations.
		
		=DataViewer_V3_150213_restored.rbp=
		2015.02.13
		Roberto Gomez
		V3.3 BETA
		Xojo 2014 R3.2 / Windows7
		-Compiled with Xojo finally!! This means DataViewer looks good now on InternetExplorer too.
		-Modified the style BtnCommonFontDisabled so that is looks also good in InternetExplorer. Disabled buttons are not in Black and are now Italic
		-Using MouseUp event instead of MouseDown to close ODiag in several popup menus in OceanData page to allow for SelectionChanged event to fire in InternetExplorer (see SelectionChanged event documentation).
		-Making constant weblabels invisible and visible at Shown event in all Dialogs to overcome bug 37085.
		-Now including helptag on webcanvas--> "Click on map to get information on measured data."
		-Now using Crosshair cursor on webcanvas.
		-Increase the width of cbFixScaleY in TimeSeries page to avoid cutting the last words.
		-Increase the width of Label3 in TimeSeries page to avoid cutting the '(UTC)' word at the end.
		-Implemented cfgDV.enableForecastCCV to integrate animation output of Actimar's CurExtrap package (for the moment only for current) in Home Page. Enabling this flag
		requires for key.Packages.GF_Forecast to be licenced otherwise would not work. At the moment only the animation is being downloaded (no integration with WERA DB).
		
		
		=DataViewer_V3_150108.rbp=
		2015.01.08
		Roberto Gomez
		Not yet compiled
		-Increase the width of LblMeasInRange in TimeSeries page to avoid cutting the 'range' word at the end.
		-Increase the width of QualFilterLabel2 in OceanData page to avoid cutting the 'filter' word at the end. The QualFilter popup menu was also moved slightly to the right.
		
		=DataViewer_V3_141117.rbp=
		2014.11.27
		Roberto Gomez
		V3.2 BETA
		RS 2012 2.1 / Windows7
		-Fixed increment of HomePage.timerCounter when map file in home page is not available yet.
		-No unhandled exception when map .gif file in home page is NIL (folder of map file does not exists).
		-OceanDataPage.Shown event now considers initial quality level when querying for last measured map. In this case, it will not automatically select the last available timeslot in the list, but the last being qualified already.
		
		=DataViewer_V3_141117.rbp=
		2014.11.17
		Roberto Gomez
		V3.1 BETA
		RS 2012 2.1 / Windows7
		-Increased major version to 3 (mainly due to licence check implementation... should have done that before).
		
		=DataViewer_V2_141013.rbp=
		Roberto Gomez
		Version 2.32 BETA
		Real Studio 2012 R2.1 / Windows7
		-When initialMapType is not 0 (combined current) but 1 or 2 (radial current measurement from station 1 or 2)
		    --> And nStations=2, the last current map date in home page corresponds now to the correct radial map (and not combined map as before)
		    --> Using the 'use in time-series' button in pop-up window after clicking will automatically render the correct quality level filter in Time-Series page (instead of quality level 1 as before).
		
		
		=DataViewer_V2_141010.rbp=  
		Roberto Gomez
		Version 2.31 BETA
		Real Studio 2012 R2.1 / Window 7
		-Implemented modifications on sm module to avoid error when checking for status when nStations=1.
		-Updated to correct caption and help messages in Export to CSV button.
		-In time-series page, when changing date and the previously selected measurement is not found, the first one selected. Now the SelectedMeas variable is updated too (before it was not updated).
		
		=DataViewer_V2_140929.rbp=  
		Roberto Gomez
		Version 2.30 BETA
		Real Studio 2012 R2.1 / Window 7
		-Implemented modifications on license routines.
		
		=DataViewer_V2_140926.rbp=  
		Roberto Gomez
		Version 2.29 BETA
		Real Studio 2012 R2.1 / Window 7
		-Implemented new licence checking routines.
		-About message, also displays licensed user and company.
		
		=DataViewer_V2_140923.rbp=   (Compiled to port 8080)
		Roberto Gomez
		Version 2.28 BETA
		Real Studio 2012 R2.1 / Window 7
		-Changed to port 8080
		-Using App.ExecutableFile.ModificationDate again instead of App.ExecutableFile.CreationDate, to indicate a date reference for the version.
		
		=DataViewer_V2_140910.rbp=   (Compiled to port 8081!!!)
		Roberto Gomez
		Version 2.27 BETA
		Real Studio 2012 R2.1 / Window 7
		-Updated to methods db.CheckDatabaseConnection and db.ReadDBSetupFile
		-Now using App.ExecutableFile.CreationDate instead of App.ExecutableFile.ModificationDate, to indicate the date of creation of the version.
		-Map animations are now considering cfgDV.mapsOrganized parameter.
		-Changing to another date or time in Ocean Maps page will not reset the quality filter to another value.
		-Changing to another data type in Ocean Maps page will reset the quality filter to the default ones indicated in cfgDV.homeMapTypeCur, cfgDV.homeMapTypeWav and cfgDV.homeMapTypeWin. 
		
		=DataViewer_V2_140908.rbp=
		Roberto Gomez
		Not yet compiled
		-Property hoursInBetween not used anymore. Also std.CalculateHours is not used anymore. Now using minutesInBetween and std.CalculateSeconds
		-Plots are now correctly shown in the time axis (in cases of missing files, the hole in time is shown as it should) in both stick plots and normal plots.
		-Implemented option (parameter cfgDV.sampInterval) of having a fixed sampling interval for better time axis representation.
		-Implemented option (parameter cfgDV.mapsOrganized) of getting maps from organized folder structure (/YYYY/DoY/). 
		
		=DataViewer_V2_140905.rbp=
		Roberto Gomez
		Not yet compiled
		-Added ProgressWheelMap in HomePage to indicate when a map is not found and it is waiting to see if comes.
		-Home page now indicates when the map file is corrupted, not found, etc. 
		
		=DataViewer_V2_140903.rbp=
		Roberto Gomez
		Not yet compiled
		-No unhandled exception when no data is available in database.
		
		=DataViewer_V2_140528_2.rbp=
		Roberto Gomez
		Version 2.27 BETA
		-Version compiled using port 8081
		
		=DataViewer_V2_140528.rbp=
		Roberto Gomez
		Version 2.26 BETA
		-Version compiled using port 8080
		-Debugged home page to correctly display radial maps and automatic update of the map  when only one station is available.
		-The quit method in App.Open event does not seems to exit, but it continues execution. Therefore, all code after the quit is inside an else statement to avoid being executed.
		
		=DataViewer_V2_140527.rbp=
		Roberto Gomez
		Version 2.25
		-Version compiled using port 8081
		-Version compiled using 2nd-DataViewer as App name (to differentiate from the other DataViewer running).
		-Updated relative path location of OceanAlert and SystemMonitor external modules.
		
		=DataViewer_V2_140425.rbp=
		Roberto Gomez
		Version 2.24 BETA
		-Version compiled using port 8081
		
		=DataViewer_V2_140214.rbp=
		Roberto Gomez
		Version 2.23 BETA
		-Method Home.UpdateOceanAlertData will do CanvasMap.Refresh at the end. This will update the warnings in the map after being updated. CanvasMap.Refresh was deleted at some points in the program since it is not required anymore.
		-Method Home.UpdateOceanAlertData will set the style of Home.BtnWave while also considering status of GeneralWarnings.
		-Created the UpdateToServerMethod method with the contents of run event of the UpdateToServer timer. Now the run event will call the method.
		-The UpdateToServerMethod is also called now at the shown event of Main, to update everything at the very beginning of the program withouth having to wait for the timer to trigger.
		-Implemented showing general warnings for waves.
		
		=DataViewer_V2_140214.rbp=
		Roberto Gomez
		Version 2.22 BETA
		-Avoid showing the WERA wave spectrum on removed data points when quality filter is active.
		-Quality filter for netCDF maps export works now.
		-Labels used inside the 'Apply quality filter' popup menu in timeseries page and ocean page can be optionally set from cfgDV.
		-Clicking the button 'Use in time-series' on the popup dialog when clicking on a map, will use the currently selected quality level to update the 'Apply quality filter' pop up menu accordingly.
		-Deleted the 'Remove artifacts' checkbox (cbRemArtif) since it was replaced by a popup menu.
		-When time series page is made visible for the first time, the pmDataType menu always showed the first entry, even when another entry was actually selected (ListIndex >0). FINALLY found a workaround to this bug... look at pmDataType.Shown event. 
		
		=DataViewer_V2_140213.rbp=
		Roberto Gomez
		Not yet compiled
		-Other minor bug fixes, mostly related to the iinitial state/appeareance of some controls.
		-Quality filter works now when clicking on maps.
		-Quality filter works now when doing time-series and exporting time-series to NetCDF or CSV.
		-# of missing values displayed in the upper side of a time-serie picture is now more precise for some type of data (wind speed, wave direction and some of the data from SWB).
		
		=DataViewer_V2_140212.rbp=
		Roberto Gomez
		Not yet compiled
		-Modified layout from time series page. Now included/implemented the possiblity of fixing the values for the y-axis manually.
		-Also included the quality filter for time series, however, this does not works good yet for all types of data.
		-Changed tag 'use synthetic buoy coords' to 'select synthetic buoy coords'
		-If time-series comes from a SWB, then the title will indicate it.
		-Minor bug fixes regarding the iinitial disabled state of some controls.
		
		=DataViewer_V2_140207.rbp=
		Roberto Gomez
		Not yet compiled
		-Added label above map in ocean maps page, to indicate the type of data being displayed. The text label used inside the canvas to do the same, is not used anymore.
		-Pop ups due to click on maps now display data with bold letters to make them more visible.
		-Improvement/addition of help tags where required.
		-Executing UpdaterThread.Run at App.Open event to immediatly update all variables without having to wait for the timer to trigger for the first time.
		
		=DataViewer_V2_140206.rbp=
		Roberto Gomez
		Not yet compiled
		-Now displaying the name of the SWB instead of the label 'SYNTHETIC WAVE BUOY
		-In build automation, added save project command before buidling project into debugging mode.
		-Added a label at the side of the button 'use synthetic buoy coords' to indicate which SWB is currently selected.
		-Tried to fix the time axis distance when there are missing data during plotting but it is not yet correct.
		-Added transparency of ocean alerts as exterrnal parameter... not tested yet.
		
		=DataViewer_V2_140205.rbp=
		Roberto Gomez
		Not yet compiled
		-General debugging.
		-Now correctly displaying wind speed in time-series and exports.
		-The button 'use synthetic buoy coords' in time series will select the next SWB coordinates in case there are more than one SWB included in the viewer.
		-In build automation, added save project command after compiling project into a version release.
		
		=DataViewer_V2_140204.rbp=
		Roberto Gomez
		Not yet compiled
		-Changed the name of method IsMeterPerSecond to IsCurrentVelocity to avoid confusion due to the fact that wind speed is also meters per second.
		
		=DataViewer_V2_140203.rbp=
		Roberto Gomez
		Not yet compiled
		-Implemented the parameter cfgDV.enableWS, and the constants app.kNameWS and app.knameWSD for also having the possiblity of displaying wind speed. Labels are correctly shown now depending on cfgDV.enableWS but export, time-series does not work yet. 
		
		=DataViewer_V2_140131.rbp=
		Roberto Gomez
		Version 2.20 BETA
		RS 2012 r2.1 / Windows7
		-Implemented possibility to run the program with -v as input parameter and return the version information.
		-If there is an error during the app.open event, the program will not start, and the errors will be displayed when logging in.
		
		=DataViewer_V2_140114.rbp=
		Roberto Gomez
		Version 2.19 BETA
		RS 2012 r2.1 / Linux
		-Changed 'Height' label on wave map click pop-up for 'Hs'
		-Changed 'Wind direction' label on select type pop up menu in ocean page for 'Wind speed and direction'
		
		=DataViewer_V2_131212.rbp=
		Roberto Gomez
		Version 2.18
		RS 2012 r2.1 / Linux
		-Order of quality filter options in OceanDataPage modifed from more to less data.
		-Implemented the possibility of having an auxiliar link to connect to the WuT climate cards.
		
		
		=DataViewer_V2_131210.rbp=
		Roberto Gomez
		Version 2.17
		RS 2012 r2.1 / Windows 7
		-Changed update timers to App, not to session (really messed up with the program structure)
		-Imiplemented glboal warnings for current.
		
		=DataViewer_V2_131107.rbp=
		Roberto Gomez
		Not yet compiled
		-Changed the RefreshLstAvailMeas method to use db.GetHeaderWithData
		-Implemented new parameter cfgDV.windDirectionFrom 
		
		=DataViewer_V2_131107.rbp=
		Roberto Gomez
		Version 2.15
		RS 2012 r2.1 / Windows 7
		-Searching for the Ocean Warning configuration file is only done when the Ocean warning is enabled. 
		
		=DataViewer_V2_131107.rbp=
		Roberto Gomez
		Version 2.15
		RS 2012 r2.1 / Windows 7
		-Implementing the possibility of displaying maps of different levels of quality. This is only for displaying the maps... showing the correct information when clicking on a map, exporting data and creating time series with different levels of quality may take some time to implement.
		
		=DataViewer_V2_131028.rbp=
		Roberto Gomez
		Not yet compiled
		-Removed google maps object from WebDialog1 that was first inserted in the project (on 1.October.2013) to test google maps but never used. However, this object caused the web interface to freeze at the beginning when trying to reach maps.google and no internet connection was available.
		
		=DataViewer_V2_131015_CGI.rbp= (Compiled as CGI, not too stable since it is an old version of RS)
		Roberto Gomez
		Version 2.14
		RS 2012 r2.1 / Windows 7
		
		=DataViewer_V2_131015.rbp= (Compiled as StandAlone, not too stable since it is an old version of RS)
		Roberto Gomez
		Version 2.13
		RS 2012 r2.1 / Windows 7
		-Recompiled for changes in module svwb.
		-Now prepared for multiple synthetic wave buoys by using the prefixes for each synthetic buoy (svwb.specPrefix_pt).
		
		=DataViewer_V2_131010_CGI.rbp= (Compiled as CGI, not too stable since it is an old version of RS)
		Roberto Gomez
		Version 2.12
		RS 2012 r2.1 / Windows 7
		
		=DataViewer_V2_131010.rbp= (Compiled as StandAlone, not too stable since it is an old version of RS)
		Roberto Gomez
		Version 2.11
		RS 2012 r2.1 / Windows 7
		-Sample interval is now rounded before being displayed on the statistics of time series.
		-Data from wave buoy can now be exported to netcdf without problems.
		-When creating plots out of direction data from the wave buoy, the range is converted from (-180,180) to (0,360)
		-Improved method IsMeterPerSecond to also consider wind direction, wind speed and all data from sinthetic wave buoy. This will avoid multiply the data by 100 before exporting a netCDF file.
		-Increased the valid range of current velocity related variables in netCDF files from 300 cm/s to 500 cm/s. This is done during the declaration of the netCDF variable.
		
		=DataViewer_V2_131009.rbp=
		Roberto Gomez
		Not yet compiled
		-Direction spectrum is not anymore displayed as a line but as points. It avoids to see a line when the value jumps from little less than 360 to a little more than 0.
		-Added constant App.kNameWS, to set the name for wind speed data.
		-Data from wave buoy can also be plotted in time series.
		-Fixed errors when displaying statistics on time series.
		
		=DataViewer_V2_131008.rbp=
		Roberto Gomez
		Not yet compiled
		-Implemented changes for SeaView's synthetic wave buoy new version to display also empirical significant wave height.
		
		=DataViewer_V2_130919.rbp=
		Roberto Gomez
		Not yet compiled
		-Implemented SeaViesw's synthetic wave buoy data display on Ocean Maps page.
		
		=DataViewer_V2_130919.rbp=
		Roberto Gomez
		Not yet compiled
		-Implemented SeaViesw's synthetic wave buoy data display on Ocean Maps page.
		
		=DataViewer_V2_130916.rbp=
		Roberto Gomez
		Not yet compiled
		-Changed the name of SpecialDataFlag (which is very ambigous) to artifCur2D (which is not perfect, but better).
		
		=DataViewer_V2_130909.rbp=
		Roberto Gomez
		Not yet compiled
		-SiteInfo module is now used as external module
		-Added new module for SeaView Synthetic Wave Buoy.
		-Using updated version of wc ans ws (not anymore WarningSystem but OceanWarning, .status files).
		-Using updated version of sm (relative paths, .status files).
		
		=DataViewer_V2_130904.rbp=  (Version compiled as cgi-bin to be used for FAT in Turkey as Stand Alone version)
		2013.09.04
		Roberto Gomez
		Version 2.8 (??)
		Xojo 2013 r2 / Windows 7
		-SystemMonitor implemented.
		-Double click on radar name will:
		    When user with admin rights, reload DataViewer parameters.
		    When no final version (Beta or Alpha, App.StageCode<3) then display gap-filling buttons.
		-No license check for the moment.
		
		=DataViewer_V2_130830.rbp=  (Version compiled as cgi-bin to be used for FAT in Turkey.)
		2013.08.30
		Roberto Gomez
		Version 2.3
		RS 2012 r2.1 / Windows 7
		-Status and warning pictograms were corrected to match with the background color.
		
		=DataViewer_V2_130828.rbp=
		Roberto Gomez
		Not yet compiled
		-Rearranged main windows to include status and warning pictograms as well as a WERA DataViewer footnote.
		-Added the posibility to customize logo in left side.
		-When an Ocean warning exists, the warning pictograms changes to red and becames a link to home page.
		-Changed timer in home page to update all the time, also when home page is not enabled (to also update warnings).
		
		
		=DataViewer_V2_130827.rbp=
		Roberto Gomez
		Not yet compiled
		-Using icons provided by Herr Lippke from Reizvoll. However, I am still not happy with them... there is a blue contour on the icons and they do not really reflect the content.
		-Changed test folder to ApplicationSoftware (also applicable when compiling to cgi web application).
		-Modified styles to match with Herr Lippke proposals.
		
		=DataViewer_V2_130821.rbp=
		Roberto Gomez
		Not yet compiled
		-When a time series of 2D current map without artifacts is generated, the plot will show modified values due to artifact removing when they exists.
		-When exporting maps of 2D current without artifacts to NetCDF, measurement values modified due to artifact removing will replace original data when they exist.
		-Changed all labels regarding significant wave-height direction to Wave mean direction or something more correct.
		-Corrected vertical scale for stick plots. Lower limit has to be -0.2 or lower and upper limit has to be 0.2 or higher.
		-When clicking on a different day in the calendar, the same time should be automatically selected (and displayed in the case of OceanData page).
		-When clicking on 'from' or 'to' buttons in both OceanData and TimeData pages, a valid date should be selected otherwise a pop up will indicate it.
		
		=DataViewer_V2_130820.rbp=
		Roberto Gomez
		Not yet compiled
		-When a 2D current map without artifacts is displayed, clicking on a measurement modified due to artifact removing, will now display the modified value.
		
		=DataViewer_V2_130819.rbp=
		Roberto Gomez
		Not yet compiled
		-Modifications on stations table in radar data page to be shown correctly.
		-Added units on Alerts configuration page.
		-Added a red cross in the map click mark.
		-Corrected units of wave spectra to m^2/Hz.
		-Since leftPadding in page navigator text labels did not work, added spaces at the beginning. But it does not work neither.
		
		=DataViewer_V2_130816.rbp=
		Roberto Gomez
		Not yet compiled
		-When a map without artifacts is displayed, clicking on an empty point where an artifact was removed, no longer returns a measurement value.
		-debugging of cfgDV.graphicsFormat correct implementation.
		-Added leftPadding to the styles of right text labels on page navigator. But does not seems to work... or at least in RS 2012 r2.1
		-Changed System Overview to Radar System and Warning System to Alerts Config.
		-Added a layer on top of maps displayed to indicate if map is with artifacts removed or raw measurements.
		
		=DataViewer_V2_130815.rbp=
		Roberto Gomez
		Not yet compiled
		-Change layout on TimeData page to look like Lippke proposal.
		-Add check box on TimeData page for removing artifacts when plotting.
		-Added BtnSmallFontDisabled to be used for ODiag.BtnTD
		-Added property QualAvailData() on TimeData page and OceanData page to know if artifacts are removed or not. 
		-Session.account is now stored after succesful login.
		-If Session.account is cfgDV.WS_admin and Warning system is enabled then Warning Configuration is available.
		-License check at App.open for oceanographic warning system and for synthetic wave buoy from sea view. If not licensed, logged into file.
		-Circle size to display warning is not fixed anymore but configured on cfgDV
		-Graphics format from maps is not fixed anymore but configured on cfgDV
		-Wave spectra is displayed when cfgDV.enableSPEC is true
		-Range limits for animation generation, NetCDF export of maps and plots may be set on cfgDV
		
		=DataViewer_V2_130814.rbp=
		Roberto Gomez
		Not yet compiled
		-Parallel processing thread is now used for animation creation. Cancel button implemented.
		-Parallel processing thread is now used for NetCDF files creation (both animations/single maps as time series). Cancel button implemented.
		-Parallel processing thread is now used for creating CSV files. Cancel button implemented.
		-CSV filenames are now, as with NetCDF files, identified with a session number to avoid two sessions writing to the same file.
		-Moved method CreateCDL and all Declare methods -Declare_(something)- to Main. Also the property varNames() was moved from App to Main. 
		(The reason is that varNames contains the variables to be written to a NetCDF file, and since it is possible that two sessions are creating a NetCDF file parallely, then each session should have their own varNames() variable, _
		 All Declare methods access this variable, and CreateCDL uses the Declare methods... so everything is moved better to Main.
		
		=DataViewer_V2_130812.rbp=
		Roberto Gomez
		Not yet compiled
		-Added LicenseModule (key) to verify at the beginning the license for software permissions.
		-Verify license at App.Open and if license is not valid then quit application.
		
		=DataViewer_V2_130809.rbp=
		Roberto Gomez
		Not yet compiled
		-Added warning configuration WebContainer as an easy user interface to set the thresholds for warning generation.
		
		=DataViewer_V2_130807.rbp=
		Roberto Gomez
		Not yet compiled
		-Included external module for Warning configuration (wc).
		-Added WebContainer for Warning configuration. Contents are under development.
		-Modified Page Navigator to access the new Warning configuration container.
		-Modified all features related to the Ocean Warning system to be displayed/activated when the boolean OceanWarningSystemEnabled is true.
		
		=DataViewer_V2_130806.rbp=
		Roberto Gomez
		Not yet compiled
		-Added list of warnings in home page. Selecting a warning on the list will show it on the map and display information.
		
		=DataViewer_V2_130805.rbp=
		Roberto Gomez
		Not yet compiled
		-Cancel button in dialog when creating plot now works OK (using Dim context as new WebSessionContext(TimeDataPage.MySession) at beginning of thread.run)
		-Added buttons on home page to be able to display wind and wave data too.
		-Changed blinking delay to 2 seconds.
		-Added blinking of Current, wave and wind buttons in homepage to indicate that a warning is present.
		-Added gray background in home buttons to see which type of map is currently selected.
		
		=DataViewer_V2_130731.rbp=
		Roberto Gomez
		Not yet compiled
		-Using now db.ReadDBParams for setting up db connection
		-Changed stations table in RadarData to include new columns whose values are taken from DB.
		
		=DataViewer_V2_130730.rbp=
		Roberto Gomez
		Not yet compiled
		-Added link to download manual
		-Animations are set with initial and end dates.
		-Trying to implement cancel button during plot creation (does not work yet)
		
		=DataViewer_V2_130729.rbp=
		2013.07.29
		Roberto Gomez
		Not yet compiled
		-Added Wave spectrum plot
		-Included in OceanDialog the quality of each measurement
		
		=DataViewer_V2_130726.rbp=
		2013.07.26
		Roberto Gomez
		Not yet compiled
		-Continue changing styles to match with Lippke design.
		-Added overlay canvas in home page to display Warnings.
		-Created external module WarningStatus (ws) to contain what needs to be displayed in the canvas overaly.
		
		=DataViewer_V2_130725.rbp=
		2013.07.25
		Roberto Gomez
		Not yet compiled
		-Added page navigator container to be very similar with Lippke design for DataViewer.
		-Changed colors of fonts and backgrounds to match with Lippke design.
		-Using now font size in em, not anymore in pixels.
		-Added blue header, warning picture, gray helzel logo and adjusted layout.
		
		=DataViewer_V2_130723.rbp=
		2013.07.23
		Roberto Gomez
		Not yet compiled
		-Added standards external module (std)
		
		=DataViewer_V2_130710.rbp=
		2013.07.10
		Roberto Gomez
		Version 2.1
		RS 2012 1.2 / Windows 7
		-Fixed app compiled name to kAppName
		
		=DataViewer_V2_130709.rbp=
		2013.07.09
		Roberto Gomez
		Not yet compiled
		-Migration to new database seems to work ok now.
		
		=DataViewer_V2_130708.rbp=
		2013.07.08
		Roberto Gomez
		Not yet compiled
		-Started to migrate to new database
		
		=DataViewer_V1_130705.rbp=
		2013.07.05
		Version 1.27
		Roberto Gomez
		RS 2012 1.2 / Windows 7
		-A LOT of debbuging due to the implementation of dataTypeID.
		-To make debugging and testing easily, double-clicking the white square behind the wera logo will also reload the parameters stored in DataViewer configuration file (cfgDV).
		
		=DataViewer_V1_130704.rbp=
		2013.07.04
		Not yet compiled
		Roberto Gomez
		RS 2012 1.2 / Windows 7
		-Added the possibility to detect if an app is deployed as cgi-bin to fix the App.execDir variable to a specific folder.
		-Implemented dataTypeID, for better scalability in the future
		-Made all database related actions via an external module db, also for better scalability.
		
		=DataViewer_V1_130703.rbp=
		2013.07.03
		Version 1.26
		Roberto Gomez
		Not yet compiled
		-Fixed bug that occurs when selecting artifact removing and changing data type in Ocean maps page. 
		
		=DataViewer_V1_130702.rbp=
		2013.07.02
		Version 1.25
		Roberto Gomez
		RS 2012 1.2 / Windows 7
		-General debugging of previous version.
		-Option to activate gap filling is now with left double click in the white square behind the WERA logo.
		
		=DataViewer_V2_130701.rbp=
		2013.07.01
		Not yet compiled
		Roberto Gomez
		Not yet compiled
		-Changes in cfgDV from 2013.07.01 are updated.
		-About message is now shown with one click and not with double click on the copyright notice.
		-Implemented the cfgDV.aboutText to personalize the about message.
		-Option to activate gap filling button if double clicking with right click on the white background square behing the WERA logo (do not click the logo!!!).
		-Replaced names in OceanData: BtnNonFiltPoints with BtnArtifactRemove and BtnFiltered with BtnGapFilling, to have consistent names.
		-Implemented the cfgDV.enableAR and cfgDV.enableGF to enable/disable artifact removing and gap filling buttons.
		-Implemented the cfgDV.enableCCV, cfgDV.enableCSWH, cfgDV.enableWD and cfgDV.enableRC_st to personalize what kind of data is possible to display.
		-Added konstants App.kName... for fixing the label in pmDataType pop menus for type of data.
		-Rearranged open and shown events to be consistent with the idea of initializing possible values in open events and setting initial values in shown events.
		
		=DataViewer_V1_130628.rbp=
		2013.06.28
		Version 1.24
		Roberto Gomez
		RS 2012 1.2 / Windows 7
		-Change to App.execDir instead of using DebugBuild or App.mainPath in all instances where relative path is required. This way, App.execDir is defined only in app.open event and makes easier to change from DataCombiner1 to DataCombiner2 folder when debugging.
		-App.Write2LogFile method was replaced by an external module log.Write which can be shared with other applications too and make the changes to be updated automatically in all applications that use it (DataCombiner, DataViewerConfigurator, IdentifyArtifacts, etc.)
		-Changed cfg module with the extern cfgDV module, which is shared with DataViewerConfigurator.
		-Solved all warnings when doing Analyze Project.
		-Changed text in lateral navigator links in main page.
		-Increase width of copyright notice and web page navigator text to avoid being cutted when zooming in/out.
		-Implemented the posibility of having "none" as cfgDV.welcomeMsg, which means that no welcome message is shown.
		-Programatically show/hide the WuT external sensors interfaces out of cfgDV paramters.
		
		=DataViewer_V1_130627.rbp=
		Not compiled
		Roberto Gomez
		-Added buttons on the RadarDataPage to proove that zindex really does not work.
		
		=DataViewer_V1_130626.rbp= 
		Not compiled
		Roberto Gomez
		-Added the WebIO sensors for temperature, humidity and pressure as an extra page and also inside RadarPage. Note that zindex in RadarPage does not work RS 2012 1.2.
		
		NOTE: Compiling DataViewr from Xojo 2013 R1 results in the same instability of browser as indicated in =DataViewer_130325.rbp=
		
		=DataViewer_V1_130429.rbp= (fixed to port 8080, version for testing artifact removing and gap filling maps)
		2013.04.29
		Version 1.23
		Roberto Gomez
		RS 2012 1.2 / Linux
		-Enable looking at artifact removing maps and gap filling. For gap filling to be enabled you have to double-click the copyright notice.
		-Change button for artifact removing from 'Filtered Data' to 'Omit Artifacts'
		
		=DataViewer_V1_130426.rbp= (fixed to port 8080)
		2013.04.26
		Version 1.22
		Roberto Gomez
		RS 2012 1.2 / Linux
		-Corrected data insertion order in table from Radar Data page to match with column title (Lon and Lat were wrong).
		
		=DataViewer_V1_130423_2.rbp= (fixed to port 8081)
		2013.04.23
		Version 1.21
		Roberto Gomez
		RS 2012 1.2 / Linux
		-Version for port 8081
		
		=DataViewer_V1_130423.rbp= (fixed to port 8080)
		2013.04.23
		Version 1.20
		Roberto Gomez
		RS 2012 1.2 / Linux
		-Added 'welcomeMsg' parameter in DataViewerParams.cfg to personalize welcome pop up message.
		-Now using App.StageCode to put the word Beta when showing the name of the program or when unhandled exception occurs.
		-Again all files accessed inside the application are accessed using relative paths to executable.
		
		=DataViewer_V1_130405_2.rbp= (Stand alone, port 8081, fixed to /home/wera/data-combiner2/)
		2013.04.05, 
		Version 1.19 
		
		=DataViewer_V1_130405.rbp= (Stand alone, port 8080, fixed to /home/wera/data-combiner1/)
		2013.04.05, 
		Version 1.18, 
		Roberto Gomez
		RS 2012 1.2 / Linux
		-Displaying "No data" in wind speed when value is -999, ALSO IN HOME PAGE!!!
		-Displaying "No data" in wave height direction when value is -999, ALSO IN HOME PAGE!!!
		
		=DataViewer_V1_130328.rbp= (Stand alone, port 8080)
		2013.03.28, 
		Version 1.17, 
		Roberto Gomez
		RS 2012 1.2 / Linux
		-In OceanData, updating photo right after changing data type (avoids bug that occurs when clicking at -Use in time data- button after changing type without selecting time slot.
		-Added new parameters:
		    plotUseWaterMark    = true            // If true, water mark will be added into plots.
		    plotWaterMarkFile    = wera_small.jpg    // Name of image file to load as water mark
		    csvAddEmptyVals    = true            // If false, measurements where no data is available will not be exported into csv file.
		    csvEmptyVal        = -999            // If csvAddEmptyVals=true, measurements where no data is available will receive this value.
		    csvColSeparatorChr    = 59            // ASCII value for column separator in .csv file (for comma use 54, semicolon 59, tab 9).
		-csv file export now return all values (before, when there were measurements with no data, the last measured points were not exported).
		-Changed to version management using MajorVersion and NonReleaseVersion.
		-Change FillValue from NetCDF files to -999 to comply with wind speed (whose value is -999).
		-Displaying "No data" in wind speed when value is -999.
		-Displaying "No data" in wave height direction when value is -999.
		
		
		=DataViewer_130325.rbp= (Stand alone, port 8080)
		2013.03.25, 
		Compilation ID=16, 
		Roberto Gomez
		RS 2012 1.2 / Linux
		-Repaired incorrect implementation of login usernames and passwords.
		-IMPORTANT: Compiled using RS2012 1.2... is more stable when using browsers other than firefox or internet explorer.
		
		=DataViewer_130322.rbp= (Stand alone again, port 8080)
		2013.03.22, 
		Compilation ID=15, 
		Roberto Gomez
		RS 2012 2.1 / Linux
		-Added login window for restricted access. This includes access.txt file which contains allowed usernames and passwords. When access.txt does not contain any username, no login window appears.
		-Replace all MsgBox with Write2LogFile (except when logging does not apply).
		-Extended Write2LogFile method to be able to also NOT show messages to users, when needed to log warning or just info.
		-Log into file each session connection, account used and exit of session.
		
		
		=DataViewer_130320_3.rbp= (Relative paths in data-combiner2, automatic port)
		2013.03.21, 
		Compilation ID=14, 
		Roberto Gomez
		RS 2012 2.1 / Linux
		
		=DataViewer_130320_2.rbp= (Relative paths in data-combiner1, fixed port 8082)
		2013.03.21, 
		Compilation ID=13, 
		Roberto Gomez
		RS 2012 2.1 / Linux
		-Compiled to a CGI file
		-Modified code to not call App.ExecutableFile, when trying to reach files using relative paths.. instead created a property (App.mainPath), which replaces this use. The value is initialized in App.Open event.
		
		
		=DataViewer_130320.rbp=
		2013.03.20, 
		Compilation ID=12, 
		Roberto Gomez
		RS 2012 2.1 / Linux
		-Double click in copyright note will also show the current session id.
		-If session id is not found (session.close event), then do not remove it (avoids the strange error that logs infinitely... but it is just a work around... must be investigated!!!)
		-Added app.rePlotFilePass method and App.SessionFailed property to avoid locking plotFilePass when unhandled exception during plotting occurs. However, unhandled exception should not occur... this is also a workaround!!!
		
		=DataViewer_130319.rbp=
		2013.03.19, 
		Compilation ID=11, 
		Roberto Gomez
		RS 2012 2.1 / Windows
		-Added a CriticalSection object in App (plotFilePass) to control access to plot images and avoid problems when multiple users create a plot simultaneously.
		-Unhandled Exception in session will not log the whole error (only a small session id msg) since it will be anyway logged in the unhandled exception of the app.
		-Added message at beginning, to indicate that it is a Beta Version
		
		
		
		=DataViewer_130318.rbp=
		2013.03.18, 
		Compilation ID=10, 
		Roberto Gomez
		RS 2012 2.1 / Windows
		-Added actions on unhandled exception event on session.
		-Unhandled exception on log file are labeled as App or Session depending on where did it ocurred.
		-Error is written to log when using HandleDatabaseError.
		
		
		
		=DataViewer_130228.rbp=
		2013.02.28, 
		Compilation ID=9, 
		Roberto Gomez
		RS 2012 2.1 Linux
		-Write all time labels withouth PM or AM but with HH:MM.
		-Database connection time-out bug fixed using a timer which keeps the connection alive in main window (KeppDBAlive)
		-Edited cfg module to correctly show warning messages
		-Water mark image for plots is now retrieved from a relative folder inside DataViewer folder.
		-Click details pop-up from home page dissapears now after changing to another page section.
		-Always show first latitude and then longitude
		-Fixed bug of having to double click the calendar to correctly show available measurements in the list (hopefully)
		-Increased width of Click details pop-up window a little bit to avoid having 2 lines as window title when grid coordinates have 3 digits (>99)
		-Log file included, and method to write on it (App.Write2LogFile)
		
		
		=WERA-DataViewer= DataViewer_130226.rbp
		2013.02.26, Compilation ID=8, Roberto Gomez
		-Moved file to network server.
		-Add some of the features from Melco version to work with a WERA system of only one station.
		
		
		=WERA-DataViewer= DataViewer_130207.rbp
		2013.02.07, Compilation ID=4, Roberto Gomez
		-Updated icons.
		-Improved text labels that show last measurement in home page (adding bold letters to make easier to see the date/time).
		
		
		=WERA-DataViewer= DataViewer_130205.rbp (version compiled for meeting with Reizvoll on 2013.02.06)
		2013.02.06, Compilation ID=3, Roberto Gomez
		-Change home page to show the last available map.
		-Home page also shows last measurement from each station.
		-Home page updates every 15 seconds.
		-cfgDV.InitializeConfig does not shows an error message when succesfully loading configuration parameters (this was normally seen when opening from terminal window).
		
		
		=WERA-DataViewer=
		2013.01.28, Compilation ID=2, Roberto Gomez
		-Added date on RealBasic filename
		-Changed name of executable to WERA-DataViewer
		
		=WebWERA_V12= -2013.01.18, Compilation ID=1 -
		-First released version.
		
		
		
		
		IMPORTANT!!! Insert notes of new version at the beginning, not at the end!!! Latest version info should ALWAYS be displayed first!!!
		
		
		
		
		
		
		
		
	#tag EndNote


	#tag Property, Flags = &h0
		animFilePass As CriticalSection
	#tag EndProperty

	#tag Property, Flags = &h0
		blat As single
	#tag EndProperty

	#tag Property, Flags = &h0
		blon As single
	#tag EndProperty

	#tag Property, Flags = &h0
		execDir As String
	#tag EndProperty

	#tag Property, Flags = &h0
		forecastEnabled As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		grdid(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		grdlatstep As double
	#tag EndProperty

	#tag Property, Flags = &h0
		grdlonstep As double
	#tag EndProperty

	#tag Property, Flags = &h0
		grdmaxlat As double
	#tag EndProperty

	#tag Property, Flags = &h0
		grdmaxlon As double
	#tag EndProperty

	#tag Property, Flags = &h0
		grdminlat As double
	#tag EndProperty

	#tag Property, Flags = &h0
		grdminlon As double
	#tag EndProperty

	#tag Property, Flags = &h0
		grdnx As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		grdny As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		grdx(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		grdy(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		halflatstep As double
	#tag EndProperty

	#tag Property, Flags = &h0
		halflonstep As double
	#tag EndProperty

	#tag Property, Flags = &h0
		lastCurMapDate As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		lastCurMapId As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		lastCurMapQual As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		lastFcstCurDate As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		lastSt1CurDate As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		lastSt1WavDate As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		lastSt2CurDate As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		lastSt2WavDate As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		lastWavMapDate As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		lastWavMapId As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		lastWinMapDate As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		lastWinMapId As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		mlat As single
	#tag EndProperty

	#tag Property, Flags = &h0
		mlon As single
	#tag EndProperty

	#tag Property, Flags = &h0
		OceanWarningSystemEnabled As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		pathCfgDB As String = "../DataArchivePrograms/DataArchive_setup.txt"
	#tag EndProperty

	#tag Property, Flags = &h0
		plotFilePass As CriticalSection
	#tag EndProperty

	#tag Property, Flags = &h0
		SessionFailed As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		sessionsIDs(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		StartErrorMessages As String
	#tag EndProperty

	#tag Property, Flags = &h0
		SyWaBuSeaViewEnabled As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		UpdaterTimer As UpdaterTimerClass
	#tag EndProperty


	#tag Constant, Name = kAccept, Type = String, Dynamic = True, Default = \"Accept", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Accept"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Aceptar"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Aceitar"
		#Tag Instance, Platform = Any, Language = de, Definition  = \"Azeptieren"
	#tag EndConstant

	#tag Constant, Name = kAllRightsReserved, Type = String, Dynamic = True, Default = \"All rights reserved.", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"All rights reserved."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Todos los derechos reservados."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Todos os direitos reservados."
	#tag EndConstant

	#tag Constant, Name = kAppName, Type = String, Dynamic = False, Default = \"WERA-DataViewer", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kCancel, Type = String, Dynamic = True, Default = \"Cancel", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cancel"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Cancelar"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Cancelar"
		#Tag Instance, Platform = Any, Language = de, Definition  = \"Abbrechen"
	#tag EndConstant

	#tag Constant, Name = kDisconnectMessage, Type = String, Dynamic = True, Default = \"You are not connected to DataViewer anymore.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"You are not connected to DataViewer anymore."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Ya no est\xC3\xA1 conectado al DataViewer."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Voc\xC3\xAA n\xC3\xA3o est\xC3\xA1 conectado a DataViewer mais."
	#tag EndConstant

	#tag Constant, Name = kLaunchMessage, Type = String, Dynamic = True, Default = \"Connecting to DataViewer...", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Connecting to DataViewer..."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Conect\xC3\xA1ndose a DataViewer..."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Ligar a DataViewer..."
	#tag EndConstant

	#tag Constant, Name = kLogFile, Type = String, Dynamic = False, Default = \"WERA-DataViewer", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kNameAccuracy, Type = String, Dynamic = True, Default = \"Accuracy of ", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Accuracy of "
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Precisi\xC3\xB3n de "
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Precis\xC3\xA3o do "
	#tag EndConstant

	#tag Constant, Name = kNameCCV, Type = String, Dynamic = True, Default = \"Combined current velocity", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Combined current velocity"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Corrientes totales combinadas"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Corrente total combinada"
	#tag EndConstant

	#tag Constant, Name = kNameCCVAbs, Type = String, Dynamic = True, Default = \"Absolute current velocity", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Absolute current velocity"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Magnitud de corriente"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Magnitude da corrente"
	#tag EndConstant

	#tag Constant, Name = kNameCCVDir, Type = String, Dynamic = True, Default = \"Current velocity direction", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Current velocity direction"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Direcci\xC3\xB3n de la corriente"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Direc\xC3\xA7\xC3\xA3o da corrente"
	#tag EndConstant

	#tag Constant, Name = kNameCCVE, Type = String, Dynamic = True, Default = \"Eastward current velocity", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Eastward current velocity"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Componente de corriente hacia el este"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Corrente para o leste"
	#tag EndConstant

	#tag Constant, Name = kNameCCVN, Type = String, Dynamic = True, Default = \"Northward current velocity", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Northward current velocity"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Corriente hacia el norte"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Corrente para o norte"
	#tag EndConstant

	#tag Constant, Name = kNameCSWH, Type = String, Dynamic = True, Default = \"Combined wave-height and direction", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Combined wave-height and direction"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Direcci\xC3\xB3n y altura de ola combinada"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Dire\xC3\xA7\xC3\xA3o e onda altura combinada"
	#tag EndConstant

	#tag Constant, Name = kNameCSWHDir, Type = String, Dynamic = True, Default = \"Wave mean direction", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Wave mean direction"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Direcci\xC3\xB3n general de ola"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Dire\xC3\xA7\xC3\xA3o geral de onda"
	#tag EndConstant

	#tag Constant, Name = kNameCSWHVal, Type = String, Dynamic = True, Default = \"Significant wave height", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Significant wave height"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Altura de ola significante"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Altura significativa de onda"
	#tag EndConstant

	#tag Constant, Name = kNameRC, Type = String, Dynamic = True, Default = \"Radial current vel.", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Radial current vel."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Corriente radial"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Corrente radial"
	#tag EndConstant

	#tag Constant, Name = kNameSynthBuoy, Type = String, Dynamic = True, Default = \"synth. buoy", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"synth. buoy"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"boya sint."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"b\xC3\xB3ia sint."
	#tag EndConstant

	#tag Constant, Name = kNameWD, Type = String, Dynamic = True, Default = \"Wind direction", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Wind direction"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Direcci\xC3\xB3n de viento"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Dire\xC3\xA7\xC3\xA3o do vento"
	#tag EndConstant

	#tag Constant, Name = kNameWS, Type = String, Dynamic = True, Default = \"Wind speed", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Wind speed"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Velocidad de viento"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Velocidade do vento"
	#tag EndConstant

	#tag Constant, Name = kNameWSD, Type = String, Dynamic = True, Default = \"Wind speed and direction", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Wind speed and direction"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Velocidad y direcci\xC3\xB3n de viento"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Velocidade e dire\xC3\xA7\xC3\xA3o do vento"
	#tag EndConstant

	#tag Constant, Name = kOpenErrCalcSWB, Type = String, Dynamic = True, Default = \"There was a problem calculating required parameters for SeaView\'s synthetic wave buoy!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"There was a problem calculating required parameters for SeaView\'s synthetic wave buoy!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Ha ocurrido un problema mientras se calculaban par\xC3\xA1metros requeridos por la boya sint\xC3\xA9tica de Seaview."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Houve um problema no c\xC3\xA1lculo dos par\xC3\xA2metros necess\xC3\xA1rios para boia sint\xC3\xA9tica de SeaView!"
	#tag EndConstant

	#tag Constant, Name = kOpenErrLoadAccess, Type = String, Dynamic = True, Default = \"Error while attempting to load access accounts!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Error while attempting to load access accounts!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Ha ocurrido un problema mientras se intentaban cargar las cuentas de acceso."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Erro ao tentar carregar contas de acesso!"
	#tag EndConstant

	#tag Constant, Name = kOpenErrLoadLic, Type = String, Dynamic = True, Default = \"Could not load license file!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Could not load license file!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"No ha sido posible cargar el archivo de licencia."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"N\xC3\xA3o \xC3\xA9 poss\xC3\xADvel carregar o arquivo de licen\xC3\xA7a."
	#tag EndConstant

	#tag Constant, Name = kOpenErrLoadSWB, Type = String, Dynamic = True, Default = \"There was a problem loading configuration of SeaView\'s wave buoy!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"There was a problem loading configuration of SeaView\'s wave buoy!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Ha ocurrido un problema mientras se cargaba el archivo de configuraci\xC3\xB3n de la boya sint\xC3\xA9tica de Seaview."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Houve uma configura\xC3\xA7\xC3\xA3o de carregamento problema da boia de SeaView!"
	#tag EndConstant

	#tag Constant, Name = kOpenErrParams, Type = String, Dynamic = True, Default = \"A problem occurred while attempting to read the parameters of the DataViewerParams.cfg file!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"A problem occurred while attempting to read the parameters of the DataViewerParams.cfg file!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Ha ocurrido un problema mientras se intentaba leer los par\xC3\xA1metros de archivo DataViewerParams.cfg."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Ocorreu um problema ao tentar ler os par\xC3\xA2metros do arquivo DataViewerParams.cfg!"
	#tag EndConstant

	#tag Constant, Name = kOpenErrSiteParams, Type = String, Dynamic = True, Default = \"Could not load site parameters!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Could not load site parameters!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"No ha sido posible cargar par\xC3\xA1metros espec\xC3\xADficos de las estaciones de radar."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"N\xC3\xA3o foi poss\xC3\xADvel carregar par\xC3\xA2metros da esta\xC3\xA7\xC3\xA3o!"
	#tag EndConstant

	#tag Constant, Name = kOpenErrUnlicensedPack, Type = String, Dynamic = True, Default = \"License does not include following package: ", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"License does not include following package: "
		#Tag Instance, Platform = Any, Language = es, Definition  = \"La licencia no incluye el siguiente paquete de software: "
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Licen\xC3\xA7a n\xC3\xA3o inclui seguinte pacote:"
	#tag EndConstant

	#tag Constant, Name = kOpenErrUpdateVars, Type = String, Dynamic = True, Default = \"A problem ocurred while attempting to update app variables!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"A problem ocurred while attempting to update app variables!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Ha ocurrido un problema mientras se intentaban actualizar las variables de la aplicaci\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Ocorreu um problema durante a tentativa de atualizar as vari\xC3\xA1veis app!"
	#tag EndConstant

	#tag Constant, Name = kOpenErrValidLic, Type = String, Dynamic = True, Default = \"A problem ocurred while validating licence file!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"A problem ocurred while validating licence file!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Ha ocurrido un problema mientras se validaba el archivo de licencia."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Ocorreu um problema ao validar o arquivo de licen\xC3\xA7a!"
	#tag EndConstant

	#tag Constant, Name = kUEBeta, Type = String, Dynamic = True, Default = \"This is a beta version of WERA DataViewer.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"This is a beta version of WERA DataViewer."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Esta es una versi\xC3\xB3n BETA de WERA DataViewer."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Esta \xC3\xA9 uma vers\xC3\xA3o beta do WERA DataViewer."
	#tag EndConstant

	#tag Constant, Name = kUEErrNum, Type = String, Dynamic = True, Default = \"Error number", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Error number"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"C\xC3\xB3digo de error"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"N\xC3\xBAmero de erro"
	#tag EndConstant

	#tag Constant, Name = kUEExcepDetails, Type = String, Dynamic = True, Default = \"Details of the exception:", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Details of the exception:"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Detalle de la excepci\xC3\xB3n:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Detalhes da exce\xC3\xA7\xC3\xA3o:"
	#tag EndConstant

	#tag Constant, Name = kUEExcepSource, Type = String, Dynamic = True, Default = \"Source of the exception:", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Source of the exception:"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Historial de execuci\xC3\xB3n de la excepci\xC3\xB3n:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Fonte da exce\xC3\xA7\xC3\xA3o:"
	#tag EndConstant

	#tag Constant, Name = kUEstr1, Type = String, Dynamic = True, Default = \"An unhandled exception occurred!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"An unhandled exception occurred in the program!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC2\xA1Ha ocurrido una excepci\xC3\xB3n inesperada en el programa!"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Uma exce\xC3\xA7\xC3\xA3o n\xC3\xA3o tratada ocorreu!"
	#tag EndConstant

	#tag Constant, Name = kUEstr2, Type = String, Dynamic = True, Default = \"The exception report below may be sent to Helzel Messtechnik GmbH automatically for further improvements. Sorry for the inconvenience.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"The exception report below may be sent to Helzel Messtechnik GmbH automatically for further improvements. Sorry for the inconvenience."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"El reporte de excepci\xC3\xB3n abajo puede ser enviado autom\xC3\xA1ticamente a Helzel Messtechnik GmbH para mejora del software. Disculpe los incovenientes."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"O relat\xC3\xB3rio de exce\xC3\xA7\xC3\xA3o abaixo podem ser enviadas para Helzel Messtechnik GmbH automaticamente para futuras melhorias. Desculpe o transtorno."
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="blat"
			Group="Behavior"
			Type="single"
		#tag EndViewProperty
		#tag ViewProperty
			Name="blon"
			Group="Behavior"
			Type="single"
		#tag EndViewProperty
		#tag ViewProperty
			Name="execDir"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="forecastEnabled"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="grdlatstep"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="grdlonstep"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="grdmaxlat"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="grdmaxlon"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="grdminlat"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="grdminlon"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="grdnx"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="grdny"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="halflatstep"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="halflonstep"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lastCurMapId"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lastCurMapQual"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lastWavMapId"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lastWinMapId"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mlat"
			Group="Behavior"
			Type="single"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mlon"
			Group="Behavior"
			Type="single"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OceanWarningSystemEnabled"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="pathCfgDB"
			Group="Behavior"
			InitialValue="../DataArchivePrograms/DataArchive_setup.txt"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SessionFailed"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StartErrorMessages"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SyWaBuSeaViewEnabled"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
