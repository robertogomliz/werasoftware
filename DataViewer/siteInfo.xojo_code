#tag Module
Protected Module siteInfo
	#tag Method, Flags = &h0
		Function Initialize() As Boolean
		  
		  Redim acqTime(cfgDV.nStations-1)
		  Redim acronym(cfgDV.nStations-1)
		  Redim bandwidth(cfgDV.nStations-1)
		  Redim boresight(cfgDV.nStations-1)
		  Redim chirpTime(cfgDV.nStations-1)
		  Redim curIntegration(cfgDV.nStations-1)
		  Redim curLimDown(cfgDV.nStations-1)
		  Redim curLimUp(cfgDV.nStations-1)
		  Redim curMeasPerHour(cfgDV.nStations-1)
		  Redim curRange(cfgDV.nStations-1)
		  Redim freq(cfgDV.nStations-1)
		  Redim longname(cfgDV.nStations-1)
		  Redim slat(cfgDV.nStations-1)
		  Redim slon(cfgDV.nStations-1)
		  Redim sname(cfgDV.nStations-1)
		  Redim timeslot(cfgDV.nStations-1)
		  Redim tnor(cfgDV.nStations-1)
		  Redim typCurAcc(cfgDV.nStations-1)
		  Redim typWavAcc(cfgDV.nStations-1)
		  Redim typWinAcc(cfgDV.nStations-1)
		  Redim wavIntegration(cfgDV.nStations-1)
		  Redim wavLimDown(cfgDV.nStations-1)
		  Redim wavLimUp(cfgDV.nStations-1)
		  Redim wavMeasPerHour(cfgDV.nStations-1)
		  Redim wavRange(cfgDV.nStations-1)
		  Redim winLimDown(cfgDV.nStations-1)
		  Redim winLimUp(cfgDV.nStations-1)
		  Redim winRange(cfgDV.nStations-1)
		  
		  dim i as Integer
		  dim rs as RecordSet
		  
		  for i=0 to (cfgDV.nStations-1)
		    
		    rs=db.GetStationData(CStr(i+1))
		    
		    if rs=Nil then
		      log.Write(true,"Could not retrieve information about station "+ Cstr(i+1) + " from the database") 
		      return true
		    elseif rs.BOF then
		      log.Write(true,"Did not find information about station "+ Cstr(i+1) + " in the database") 
		      return true
		    end
		    
		    acqTime(i)=rs.Field("acqTime").StringValue
		    acronym(i)=rs.Field("acronym").StringValue
		    bandwidth(i)=rs.Field("bandwidth").IntegerValue
		    boresight(i)=rs.Field("boresight").StringValue
		    chirpTime(i)=rs.Field("chirpTime").DoubleValue
		    curIntegration(i)=rs.Field("curIntegration").StringValue
		    curLimDown(i)=rs.Field("curLimDown").StringValue
		    curLimUp(i)=rs.Field("curLimUp").StringValue
		    curMeasPerHour(i)=rs.Field("curMeasPerHour").StringValue
		    curRange(i)=rs.Field("curRange").StringValue
		    freq(i)=rs.Field("freq").DoubleValue
		    longname(i)=rs.Field("longname").StringValue
		    slat(i)=rs.Field("slat").StringValue
		    slon(i)=rs.Field("slon").StringValue
		    sname(i)=rs.Field("sname").StringValue
		    timeslot(i)=rs.Field("timeslot").StringValue
		    tnor(i)=rs.Field("tnor").StringValue
		    typCurAcc(i)=rs.Field("typCurVelAcc").StringValue
		    typWavAcc(i)=rs.Field("typWavHeightAcc").StringValue
		    typWinAcc(i)=rs.Field("typWinSpeedAcc").StringValue
		    wavIntegration(i)=rs.Field("wavIntegration").StringValue
		    wavLimDown(i)=rs.Field("wavLimDown").StringValue
		    wavLimUp(i)=rs.Field("wavLimUp").StringValue
		    wavMeasPerHour(i)=rs.Field("wavMeasPerHour").StringValue
		    wavRange(i)=rs.Field("wavRange").StringValue
		    winLimDown(i)=rs.Field("winLimDown").StringValue
		    winLimUp(i)=rs.Field("winLimUp").StringValue
		    winRange(i)=rs.Field("winRange").StringValue
		    
		  next
		  
		  return false
		End Function
	#tag EndMethod


	#tag Note, Name = Updates
		
		
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new updates in the next line, not at the end!!! latest update info should ALWAYS be displayed first!!!
		
		2015.02.13
		Roberto Gomez
		-Added chirpTime, frequency and bandwidth properties.
		
		2013.08.21
		Roberto Gomez
		-If no contact with database, or data from a station is missing, display error.
		
		2013.07.31
		Roberto Gomez
		-First version created.
		
		
		IMPORTANT!!! Insert notes of new update at the beginning, not at the end!!! Latest update info should ALWAYS be displayed first!!!
		
	#tag EndNote


	#tag Property, Flags = &h0
		acqTime() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		acronym() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		bandwidth() As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		boresight() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		chirpTime() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		curIntegration() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		curLimDown() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		curLimUp() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		curMeasPerHour() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		curRange() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		freq() As Single
	#tag EndProperty

	#tag Property, Flags = &h0
		longname() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		slat() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		slon() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sname() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timeslot() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		tnor() As string
	#tag EndProperty

	#tag Property, Flags = &h0
		typCurAcc() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		typWavAcc() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		typWinAcc() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		wavIntegration() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		wavLimDown() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		wavLimUp() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		wavMeasPerHour() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		wavRange() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		winLimDown() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		winLimUp() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		winRange() As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
