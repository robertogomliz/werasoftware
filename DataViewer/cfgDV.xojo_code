#tag Module
Protected Module cfgDV
	#tag Method, Flags = &h0
		Function AssignParameterValue(lineOfText as string) As boolean
		  // Assigns the value of a parameter from a line of text from the configuration file
		  
		  Dim auxStringArray(-1), auxString as String
		  Dim auxInteger as Integer
		  
		  auxStringArray=split(lineOfText,"=")
		  
		  if Ubound(auxStringArray)<=0 then
		    return false
		  else
		    auxString=Trim(auxStringArray(0))
		    cfgVarsName_dv.Append(auxString)
		    
		    auxStringArray=split(auxStringArray(1),"//")  // Remove comment
		    auxStringArray=split(auxStringArray(0),Chr(9))  // Isolate the parameter value by removing the tabs after the value
		    auxString=Trim(auxStringArray(0))  // The variable value should be in the first cell... we only remove spaces before and after
		    
		    Select Case cfgVarsName_dv(cfgVarsName_dv.Ubound)
		    case "gridID"
		      gridID=CLong(auxString)
		    case "systemName"
		      systemName=auxString
		    case "paramscfg"
		      paramscfg=auxString
		    case "enableCCV"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        enableCCV=true
		      else
		        enableCCV=false
		      end
		    case "enableCSWH"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        enableCSWH=true
		      else
		        enableCSWH=false
		      end
		    case "enableSPEC"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        enableSPEC=true
		      else
		        enableSPEC=false
		      end
		    case "enableWS"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        enableWS=true
		      else
		        enableWS=false
		      end
		    case "enableWD"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        enableWD=true
		      else
		        enableWD=false
		      end
		    case "enableAR"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        enableAR=true
		      else
		        enableAR=false
		      end
		    case "enableQ1"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        enableQ1=true
		      else
		        enableQ1=false
		      end
		    case "enableQ2"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        enableQ2=true
		      else
		        enableQ2=false
		      end
		    case "enableQ4"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        enableQ4=true
		      else
		        enableQ4=false
		      end
		    case "enableGF"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        enableGF=true
		      else
		        enableGF=false
		      end
		    case "enableForecastCCV"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        enableForecastCCV=true
		      else
		        enableForecastCCV=false
		      end
		    case "titleInBrowser"
		      titleInBrowser=auxString
		    case "welcomeText"
		      welcomeText=auxString
		    case "aboutText"
		      aboutText=auxString
		    case "language"
		      language=auxString
		      
		    case "labelQual0"
		      labelQual0=auxString
		    case "labelQual1"
		      labelQual1=auxString
		    case "labelQual2"
		      labelQual2=auxString
		    case "labelQual3"
		      labelQual3=auxString
		    case "labelQual4"
		      labelQual4=auxString
		    case "labelQualAllFilter"
		      labelQualAllFilter=auxString
		    case "labelQual1Filter"
		      labelQual1Filter=auxString
		    case "labelQual2Filter"
		      labelQual2Filter=auxString
		    case "labelQual3Filter"
		      labelQual3Filter=auxString
		    case "labelQual4Filter"
		      labelQual4Filter=auxString
		    case "artefThreshold"
		      artefThreshold=CLong(auxString)
		    case "stdFolder"
		      stdFolder=auxString
		    case "mapsOrganized"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        mapsOrganized=true
		      else
		        mapsOrganized=false
		      end
		    case "sFolder_cur_asc"
		      sFolder_cur_asc=auxString
		    case "sFolder_wauv_asc"
		      sFolder_wauv_asc=auxString
		    case "sFolder_wiuv_asc"
		      sFolder_wiuv_asc=auxString
		    case "sSufix_cur_asc"
		      sSufix_cur_asc=auxString
		    case "sSufix_wauv_asc"
		      sSufix_wauv_asc=auxString
		    case "sSufix_wiuv_asc"
		      sSufix_wiuv_asc=auxString
		    case "nStations"
		      nStations=CLong(auxString)
		      Redim enableRC_st(nStations-1)
		      Redim sFolder_st(nStations-1)
		      Redim sSufix_st(nStations-1)
		      Redim WuTDevIP_st(nStations-1)
		      Redim WuTDevPort_st(nStations-1)
		      Redim WuTDevIPAux_st(nStations-1)
		    case "graphicsFormat"
		      graphicsFormat=auxString
		    case "useSeconds"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        useSeconds=true
		      else
		        useSeconds=false
		      end
		    case "initialMapType"
		      initialMapType=CLong(auxString)
		    case "homeMapTypeCur"
		      homeMapTypeCur=CLong(auxString)
		    case "homeMapTypeWav"
		      homeMapTypeWav=CLong(auxString)
		    case "homeMapTypeWin"
		      homeMapTypeWin=CLong(auxString)
		    case "LonRefPt1"
		      LonRefPt1=CDbl(auxString)
		    case "LatRefPt1"
		      LatRefPt1=CDbl(auxString)
		    case "PxRefPt1"
		      PxRefPt1=CDbl(auxString)
		    case "PyRefPt1"
		      PyRefPt1=CDbl(auxString)
		    case "LonRefPt2"
		      LonRefPt2=CDbl(auxString)
		    case "LatRefPt2"
		      LatRefPt2=CDbl(auxString)
		    case "PxRefPt2"
		      PxRefPt2=CDbl(auxString)
		    case "PyRefPt2"
		      PyRefPt2=CDbl(auxString)
		    case "animLimitHours"
		      animLimitHours=CLong(auxString)
		    case "ncMapsLimitHours"
		      ncMapsLimitHours=CLong(auxString)
		    case "plotUseWaterMark"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        plotUseWaterMark=true
		      else
		        plotUseWaterMark=false
		      end
		    case "plotWaterMarkFile"
		      plotWaterMarkFile=auxString
		    case "plotLimit"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        plotLimit=true
		      else
		        plotLimit=false
		      end
		    case "sampInterval"
		      sampInterval=CDbl(auxString)
		      
		    case "nc_institution"
		      nc_institution=auxString
		    case "nc_institution_url"
		      nc_institution_url=auxString
		    case "nc_contact"
		      nc_contact=auxString
		    case "nc_source"
		      nc_source=auxString
		    case "nc_history"
		      nc_history=auxString
		    case "nc_references"
		      nc_references=auxString
		    case "nc_comment"
		      nc_comment=auxString
		    case "csvAddEmptyVals"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        csvAddEmptyVals=true
		      else
		        csvAddEmptyVals=false
		      end
		    case "csvEmptyVal"
		      csvEmptyVal=CLong(auxString)
		    case "csvColSeparatorChr"
		      csvColSeparatorChr=CLong(auxString)
		      
		    case "windDirectionFrom"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        windDirectionFrom=true
		      else
		        windDirectionFrom=false
		      end
		      
		    case "useWuTWebIO"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        useWuTWebIO=true
		      else
		        useWuTWebIO=false
		      end
		    case "enableAuxWuTWebIO"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        enableAuxWuTWebIO=true
		      else
		        enableAuxWuTWebIO=false
		      end
		    case "WS_enabled"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        WS_enabled=true
		      else
		        WS_enabled=false
		      end
		    case "WS_circleSize"
		      WS_circleSize=CLong(auxString)
		    case "WS_admin"
		      WS_admin=auxString
		    case "WS_blink"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        WS_blink=true
		      else
		        WS_blink=false
		      end
		    case "WS_gridPixels"
		      WS_gridPixels=CLong(auxString)
		    case "OA_Transparency"
		      OA_Transparency=CLong(auxString)
		    case "ci_headerImage"
		      ci_headerImage=auxString
		    case "ci_NaviBarLogo"
		      ci_NaviBarLogo=auxString
		    case "ci_linkText"
		      ci_linkText=auxString
		    case "ci_link"
		      ci_link=auxString
		    case "WaBuSV_enabled"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        WaBuSV_enabled=true
		      else
		        WaBuSV_enabled=false
		      end
		    else
		      // Looks like the parameter belongs to a station
		      if IsNumeric(Right(cfgVarsName_dv(cfgVarsName_dv.Ubound),1)) then  // If the last letter is a number
		        auxInteger=CLong(Right(cfgVarsName_dv(cfgVarsName_dv.Ubound),1))
		        if auxInteger>0 and auxInteger<=nStations then
		          Select case Left(cfgVarsName_dv(cfgVarsName_dv.Ubound),len(cfgVarsName_dv(cfgVarsName_dv.Ubound))-1)
		          case "enableRC_st"
		            if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		              enableRC_st(auxInteger-1)=true
		            else
		              enableRC_st(auxInteger-1)=false
		            end
		          case "sFolder_st"
		            sFolder_st(auxInteger-1)=auxString
		          case "sSufix_st"
		            sSufix_st(auxInteger-1)=auxString
		          case "WuTDevIP_st"
		            WuTDevIP_st(auxInteger-1)=auxString
		          case "WuTDevPort_st"
		            WuTDevPort_st(auxInteger-1)=auxString
		          case "WuTDevIPAux_st"
		            WuTDevIPAux_st(auxInteger-1)=auxString
		          else
		            //Warning log... parameter not recognized
		            log.Write(false,"WARNING... station parameter in configuration file not recognized: " + cfgVarsName_dv(cfgVarsName_dv.Ubound))
		            cfgVarsName_dv.Remove(cfgVarsName_dv.Ubound)
		            return false
		          end
		        else
		          //Warning log... parameter for non-existent station
		          log.Write(false,"WARNING... parameter for non-existent station (nStation=" + CStr(nStations) + ") in configuration file > " + cfgVarsName_dv(cfgVarsName_dv.Ubound))
		          cfgVarsName_dv.Remove(cfgVarsName_dv.Ubound)
		          return false
		        end
		      else
		        //Warning log... parameter not recognized!
		        log.Write(false,"WARNING... parameter in configuration file not recognized > " + cfgVarsName_dv(cfgVarsName_dv.Ubound))
		        cfgVarsName_dv.Remove(cfgVarsName_dv.Ubound)
		        return false
		      end
		    end
		    return true
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function InitializeConfig(path as string) As boolean
		  // Initializes all the parameters and properties used by the DataViewer to work correctly. This is done by reading the parameters configuration file of DataViewer 'DataViewerParams.cfg'. Returns true when the Initialization is successfully made.
		  
		  
		  // Opening DataViewerParams.cfg file =======================================================
		  Dim f As New FolderItem
		  
		  if path.Len>3 then
		    f=GetFolderItem(path)
		  else //empty string
		    f = GetFolderItem(App.execDir + "DataViewerParams.cfg")
		  end
		  
		  
		  Dim configFile As TextInputStream
		  
		  If f <> Nil Then
		    If f.Exists Then
		      // Be aware that TextInputStream.Open coud raise an exception
		      Try
		        configFile = TextInputStream.Open(f)
		        configFile.Encoding = Encodings.ASCII
		      Catch e As IOException
		        configFile.Close
		        log.Write(true,"Error accessing configuration file.")
		        return false
		      End Try
		    Else
		      log.Write(true,"Configuration file does not exists.")
		      return false
		    End If
		  Else
		    log.Write(true,"Configuration file is 'Nil'. Make sure folder exists.")
		    return false
		  End If
		  
		  
		  // Reading DataViewerParams.cfg file =======================================================
		  
		  Dim auxString As String
		  Dim cnt, i as Integer
		  
		  linesOfTxtFile_dv=Split(configFile.ReadAll,EndOfLine)  // Split the text content into lines
		  
		  cnt=0
		  ReDim lineIdxOfVars_dv(-1)
		  ReDim cfgVarsName_dv(-1)
		  for i=0 to linesOfTxtFile_dv.Ubound
		    
		    auxString=left(Trim(linesOfTxtFile_dv(i)),1)  // Getting the first written character to see if it is only a comment or a blank line
		    
		    if auxString="/" or auxString="!" or auxString="%" or auxString="" then
		      continue
		    else
		      if cfgDV.AssignParameterValue(linesOfTxtFile_dv(i)) then
		        lineIdxOfVars_dv.Append(i)
		        cnt=cnt+1
		      else
		        // If Assign... did not suceed then it is already logged to file as a warning
		      end
		    end
		  next
		  
		  if cfgDV.language="auto" then cfgDV.language=""   'Done like this so we can always call kConstant(cfgDV.language) and it will work automatically with empty string
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SaveConfig(path as string) As Boolean
		  // Saves the current DataViewer configuration parameters to 'DataViewerParams.cfg'. Returns true when successfull.
		  
		  Dim f As  FolderItem
		  Dim t as TextOutputStream
		  Dim i, count, auxInteger as Integer
		  Dim auxString, auxStringArray(-1) as String
		  
		  if path.Len>3 then
		    f=GetFolderItem(path)
		  else //empty string
		    f = GetFolderItem(App.execDir + "DataViewerParams.cfg")
		  end
		  
		  if f<>Nil Then
		    
		    t =t.Create(f)
		    
		    count=0
		    for i=0 to linesOfTxtFile_dv.Ubound
		      if count<=lineIdxOfVars_dv.Ubound then  'Avoid OutOfBoundsException for the last lines of file where there are no more variables, but there are still lines to write
		        if i=lineIdxOfVars_dv(count) then  'A line is being written where a variable is located
		          
		          //Build string of updated value of variable
		          Select case cfgVarsName_dv(count)
		          case "gridID"
		            auxString=CStr(gridID)
		          case "systemName"
		            auxString=systemName
		          case "paramscfg"
		            auxString=paramscfg
		          case "enableCCV"
		            if enableCCV then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "enableCSWH"
		            if enableCSWH then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "enableSPEC"
		            if enableSPEC then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "enableWS"
		            if enableWS then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "enableWD"
		            if enableWD then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "enableAR"
		            if enableAR then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "enableQ1"
		            if enableQ1 then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "enableQ2"
		            if enableQ2 then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "enableQ4"
		            if enableQ4 then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "enableGF"
		            if enableGF then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "enableForecastCCV"
		            if enableForecastCCV then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "titleInBrowser"
		            auxString=titleInBrowser
		          case "welcomeText"
		            auxString=welcomeText
		          case "aboutText"
		            auxString=aboutText
		          case "language"
		            auxString=language
		            
		          case "labelQual0"
		            auxString=labelQual0
		          case "labelQual1"
		            auxString=labelQual1
		          case "labelQual2"
		            auxString=labelQual2
		          case "labelQual3"
		            auxString=labelQual3
		          case "labelQual4"
		            auxString=labelQual4
		          case "labelQualAllFilter"
		            auxString=labelQualAllFilter
		          case "labelQual1Filter"
		            auxString=labelQual1Filter
		          case "labelQual2Filter"
		            auxString=labelQual2Filter
		          case "labelQual3Filter"
		            auxString=labelQual3Filter
		          case "labelQual4Filter"
		            auxString=labelQual4Filter
		          case "artefThreshold"
		            auxString=CStr(artefThreshold)
		          case "stdFolder"
		            auxString=stdFolder
		          case "mapsOrganized"
		            if mapsOrganized then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "sFolder_cur_asc"
		            auxString=sFolder_cur_asc
		          case "sFolder_wauv_asc"
		            auxString=sFolder_wauv_asc
		          case "sFolder_wiuv_asc"
		            auxString=sFolder_wiuv_asc
		          case "sSufix_cur_asc"
		            auxString=sSufix_cur_asc
		          case "sSufix_wauv_asc"
		            auxString=sSufix_wauv_asc
		          case "sSufix_wiuv_asc"
		            auxString=sSufix_wiuv_asc
		          case "nStations"
		            auxString=CStr(nStations)
		          case "graphicsFormat"
		            auxString=graphicsFormat
		          case "useSeconds"
		            if useSeconds then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "initialMapType"
		            auxString=CStr(initialMapType)
		          case "homeMapTypeCur"
		            auxString=CStr(homeMapTypeCur)
		          case "homeMapTypeWav"
		            auxString=CStr(homeMapTypeWav)
		          case "homeMapTypeWin"
		            auxString=CStr(homeMapTypeWin)
		          case "LonRefPt1"
		            auxString=CStr(LonRefPt1)
		          case "LatRefPt1"
		            auxString=CStr(LatRefPt1)
		          case "PxRefPt1"
		            auxString=CStr(PxRefPt1)
		          case "PyRefPt1"
		            auxString=CStr(PyRefPt1)
		          case "LonRefPt2"
		            auxString=CStr(LonRefPt2)
		          case "LatRefPt2"
		            auxString=CStr(LatRefPt2)
		          case "PxRefPt2"
		            auxString=CStr(PxRefPt2)
		          case "PyRefPt2"
		            auxString=CStr(PyRefPt2)
		          case "animLimitHours"
		            auxString=CStr(animLimitHours)
		          case "ncMapsLimitHours"
		            auxString=CStr(ncMapsLimitHours)
		          case "plotUseWaterMark"
		            if plotUseWaterMark then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "plotWaterMarkFile"
		            auxString=plotWaterMarkFile
		          case "plotLimit"
		            if plotLimit then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "sampInterval"
		            auxString=CStr(sampInterval)
		            
		          case "nc_institution"
		            auxString=nc_institution
		          case "nc_institution_url"
		            auxString=nc_institution_url
		          case "nc_contact"
		            auxString=nc_contact
		          case "nc_source"
		            auxString=nc_source
		          case "nc_history"
		            auxString=nc_history
		          case "nc_references"
		            auxString=nc_references
		          case "nc_comment"
		            auxString=nc_comment
		          case "csvAddEmptyVals"
		            if csvAddEmptyVals then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "csvEmptyVal"
		            auxString=CStr(csvEmptyVal)
		          case "csvColSeparatorChr"
		            auxString=CStr(csvColSeparatorChr)
		            
		          case "windDirectionFrom"
		            if windDirectionFrom then
		              auxString="true"
		            else
		              auxString="false"
		            end
		            
		          case "useWuTWebIO"
		            if useWuTWebIO then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "enableAuxWuTWebIO"
		            if enableAuxWuTWebIO then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "WS_enabled"
		            if WS_enabled then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "WS_circleSize"
		            auxString=CStr(WS_circleSize)
		          case "WS_admin"
		            auxString=WS_admin
		          case "WS_blink"
		            if WS_blink then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          case "WS_gridPixels"
		            auxString=CStr(WS_gridPixels)
		          case "OA_Transparency"
		            auxString=CStr(OA_Transparency)
		          case "ci_headerImage"
		            auxString=ci_headerImage
		          case "ci_NaviBarLogo"
		            auxString=ci_NaviBarLogo
		          case "ci_linkText"
		            auxString=ci_linkText
		          case "ci_link"
		            auxString=ci_link
		          case "WaBuSV_enabled"
		            if WaBuSV_enabled then
		              auxString="true"
		            else
		              auxString="false"
		            end
		          else
		            // Looks like the parameter belongs to a station
		            if IsNumeric(Right(cfgVarsName_dv(count),1)) then  // If the last letter is a number
		              auxInteger=CLong(Right(cfgVarsName_dv(count),1))
		              if auxInteger>0 and auxInteger<=nStations then
		                Select case Left(cfgVarsName_dv(count),len(cfgVarsName_dv(count))-1)
		                case "enableRC_st"
		                  if enableRC_st(auxInteger-1) then
		                    auxString="true"
		                  else
		                    auxString="false"
		                  end
		                case "sFolder_st"
		                  auxString=sFolder_st(auxInteger-1)
		                case "sSufix_st"
		                  auxString=sSufix_st(auxInteger-1)
		                case "WuTDevIP_st"
		                  auxString=WuTDevIP_st(auxInteger-1)
		                case "WuTDevPort_st"
		                  auxString=WuTDevPort_st(auxInteger-1)
		                case "WuTDevIPAux_st"
		                  auxString=WuTDevIPAux_st(auxInteger-1)
		                else
		                  //Warning log... parameter of station not recognized
		                  log.Write(false,cfgVarsName_dv(count) + " - Line " + CStr(i) + ": Parameter of station not recognized.")
		                  t.WriteLine(linesOfTxtFile_dv(i))
		                  count=count + 1
		                  continue
		                end
		              else
		                //Warning log... parameter for non-existent station
		                t.WriteLine(linesOfTxtFile_dv(i))
		                log.Write(false,cfgVarsName_dv(count) + " - Line " + CStr(i) + ": Parameter for non-existent station.")
		                count=count + 1
		                continue
		              end
		            else
		              //Warning log... parameter not recognized!
		              t.WriteLine(linesOfTxtFile_dv(i))
		              log.Write(false,cfgVarsName_dv(count) + " - Line " + CStr(i) + ": Parameter not recognized.")
		              count=count + 1
		              continue
		            end
		          end
		          
		          //Write line with updated variable
		          auxStringArray=Split(linesOfTxtFile_dv(i),"=")
		          
		          if auxStringArray.Ubound<=0 then
		            //Warning log... could not find = sign in a text line where a variable is defined
		            t.WriteLine(linesOfTxtFile_dv(i))
		            log.Write(false,cfgVarsName_dv(count) + " - Line " + CStr(i) + ": Could not find = sign in a text line where a variable seems to be defined.")
		            count=count + 1
		            continue
		          else
		            t.Write(auxStringArray(0) + "= " + auxString)
		            
		            auxStringArray=Split(linesOfTxtFile_dv(i),"//")
		            if auxStringArray.Ubound<=0 then 'Adding also the comment to the line
		              t.WriteLine("")
		            else
		              if len(auxString)>=22 then
		                t.WriteLine(" //"+auxStringArray(1))
		              elseif len(auxString)>=14 then
		                t.WriteLine(Chr(9)+"//"+auxStringArray(1))
		              elseif len(auxString)>=6 then
		                t.WriteLine(Chr(9)+Chr(9)+"//"+auxStringArray(1))
		              else
		                t.WriteLine(Chr(9)+Chr(9)+Chr(9)+"//"+auxStringArray(1))
		              end if
		            end if
		          end
		          count=count+1
		          
		        else
		          t.WriteLine(linesOfTxtFile_dv(i))
		        end
		      else
		        t.WriteLine(linesOfTxtFile_dv(i))
		      end
		    next
		    
		    t.close
		    
		    return true
		  else
		    log.Write(true,"Could not save file. Folder " + f.Parent.AbsolutePath + " may not exist.")
		    return false
		  end if
		  
		End Function
	#tag EndMethod


	#tag Note, Name = FileExample
		
		// File containing configuration parameters for WERA DataViewer.
		// Use '//' to insert comments.
		
		
		
		//======================General settings for DataViewer========================
		
		    gridID        = 1            // Database ID number of grid to be viewed with DataViewer
		    systemName        = Long Bay WERA Radar System // Name of Radar system
		    paramscfg        = params.cfg        // Configuration file to use
		    enableCCV        = true            // Enables viewing of combined current vector data
		    enableCSWH        = true            // Enables viewing of combined signfinicant wave height and direction
		    enableSPEC        = true            // Enables viewing of spectrum data plots for wave maps
		    enableWD        = true            // Enables viewing of combined wind direction data
		    enableAR        = true            // Enables viewing artifact removing data
		    enableGF        = false            // Enables viewing gap filled data
		    titleInBrowser    = WERA DataViewer    // Title that will appear in the browser title bar
		
		// Welcome text shown in a message box before displaying the home page.
		// Using "default" will display> WERA DataViewer. (C)opyright <year> Helzel Messtechnik GmbH. All rights reserved.
		// Using "none" will omit displaying any message box.
		    welcomeText        = none
		
		// Text shown in a message box when double clicking the copyright notice in the main window.
		// Using "default" will display> (C) <year> Helzel Messtechnik GmbH. All rights reserved.
		    aboutText        = default
		
		//________________________________________________________________________________
		
		
		
		
		//======================Quality flags=====================
		
		    labelQual0        = -            // Text label used to define measurement with a quality value of 0 (not yet qualified)
		    labelQual1        = Excellent meas.    // Text label used to define measurement with a quality value of 1 (best value possible)
		    labelQual2        = Good measurement    // Text label used to define measurement with a quality value of 2 (middle quality level)
		    labelQual3        = Probably good meas.    // Text label used to define measurement with a quality value of 3 (even lower)
		    labelQual4        = Possible artefact    // Text label used to define measurement with a quality value of 4 or higher (normally used for artefacts)
		    artefThreshold    = 3            // Maximum value allowed to still be considered ok. Quality values bigger  than this are considered artefacts.
		//_______________________________________________________________________________
		
		
		
		
		//======================Paths and filenames configuration========================
		
		// Folder where all data is stored. Usually /home/wera/data/
		
		    stdFolder         = /home/wera/data/
		
		
		// Name of folders inside -stdFolder- where map images of each type of data is located
		
		    sFolder_cur_asc     = cur_asc/        // For current maps, two-dimensions
		    sFolder_wauv_asc     = wav_asc/        // For wave-height maps, two-dimensions
		    sFolder_wiuv_asc     = wav_asc/        // For wind maps, two-dimensions
		
		
		// Suffix used for maps filenames of each type of data
		
		    sSufix_cur_asc     = _LNG.cur_asc        // For current maps, two-dimensions
		    sSufix_wauv_asc     = _LNG.wav_asc        // For wave-height maps, two-dimensions
		    sSufix_wiuv_asc     = _LNG.win_asc        // For wind maps, two-dimensions
		//________________________________________________________________________________
		
		
		
		
		//======================Parameters of each station========================
		
		//VARIABLE nStations SHOULD BE DECLARED BEFORE THE PARAMETERS OF EACH STATION!
		    nStations        = 2            // Number of stations in the system.
		
		//Station 1 >
		
		    enableRC_st1    = true            // Enables viewing of radial current data
		    sFolder_st1        = rcc/            // Folder inside -stdFolder- where maps are located
		    sSufix_st1        = _csw.crad        // Suffix used for maps filenames containing radial current
		    WuTDevIP_st1    = www.helzel.com/    // IP address of WuT sensors device
		//Station 2 >
		
		    enableRC_st2    = true
		    sFolder_st2        = rcc/
		    sSufix_st2        = _gtn.crad
		    WuTDevIP_st2    = www.onur.net/en
		//__________________________________________________________________________
		
		
		
		//======================Parameters for maps display========================
		
		    graphicsFormat    = .gif            // Specify format of maps graphics.
		    useSeconds        = false            // Filenames of maps include seconds (required for permanent mode)
		    initialMapType    = 0            // 0=Cur2D, 10=Wav2D, 20=Win2D, n=CurRadSt_n
		    homeMapTypeCur    = 0m            // 0=raw measured, 1=artifacts removed
		    homeMapTypeWav    = 0
		    homeMapTypeWin    = 0
		
		//Reference data of two points for estimation of coordinate of clicks on maps >
		
		    LonRefPt1        = -79.66667        // Longitude coordinate value for reference point 1
		    LatRefPt1        = 32.00216        // Latitude coordinate value for reference point 1
		    PxRefPt1        = 82            // Pixel coordinate value in X-axis for reference point 1
		    PyRefPt1        = 627            // Pixel coordinate value in Y-axis for reference point 1
		    LonRefPt2        = -77            // Longitude coordinate value for reference point 2
		    LatRefPt2        = 34            // Latitude coordinate value for reference point 2
		    PxRefPt2        = 688            // Pixel coordinate value in X-axis for reference point 2
		    PyRefPt2        = 83            // Pixel coordinate value in Y-axis for reference point 2
		//__________________________________________________________________________
		
		
		//======================Parameters for Ocean Maps section=====================
		
		    animLimitHours    = 24            // Maximum number of hours allowed for animation generation
		    ncMapsLimitHours    = 24            // Maximum number of hours allowed for netCDF file export
		//____________________________________________________________________________
		
		//======================Parameters for plot generation========================
		
		    plotUseWaterMark    = true            // If true, water mark will be added into plots.
		    plotWaterMarkFile    = wera_small.jpg    // Name of image file to load as water mark
		    plotLimit        = true            // If true, you cannot generate plots bigger than 1 month.
		//__________________________________________________________________________
		
		
		
		
		//======================Parameters for exporting data========================
		
		// Global attributes of generated NetCDF files
		
		    nc_institution    = Helzel Messtechnik GmbH
		    nc_institution_url    = www.helzel.com
		    nc_contact        = info@helzel.com
		    nc_source        = Long Bay WERA System
		    nc_history        = -
		    nc_references    = -
		    nc_comment        = File was generated from WERA DataViewer.
		
		// Attributes of generated comma separated value files (.csv)
		
		    csvAddEmptyVals    = true            // If false, measurements where no data is available will not be exported into csv file.
		    csvEmptyVal        = -999            // If csvAddEmptyVals=true, measurements where no data is available will receive this value.
		    csvColSeparatorChr    = 59            // ASCII value for column separator in .csv file (for comma use 44, semicolon 59, tab 9).
		//__________________________________________________________________________
		
		
		//======================Special settings for DataViewer========================
		
		    // If useWutWebIO is true, the system will also show the Web interface for WuT sensors for
		    // temperature, humidity and pressure in System Overview page. Set WuTDevIP on each station.
		    useWuTWebIO        = false
		
		    // enableWS activates the possibility to see warnings due to extreme oceanographic measurements. Works only when feature is licenced.
		    WS_enabled        = true
		    WS_circleSize    = 50            // Warnings are displayed with a semi-transparent circle with given diameter size in Pixels. Normally 40
		    WS_admin        = root            // Which user (from access file) is allowed to configure the ocean warning thresholds.
		
		    // Corporate identity
		    ci_headerImage    = default        // Personalize web page header to add another corporate identity. default is WERA header. (Not yet implemented)
		    ci_NaviBarLogo    = default        // Personalize logo shown on left navigation bar. default is HELZEL logo.
		    ci_linkText        = default        // Personalizes the text shown below logo. default is Helzel Messtechnick GmbH
		    ci_link        = default        // Web page address to which the linkText points to. default is www.helzel.com
		
		    // Synthetic Wave Buoy from SeaView. Works only when feature is licenced.
		    WaBuSV_enabled    = true            // Enables displaying of data from Synthetic Wave Buoy from SeaView
		//_______________________________________________________________________________
		
		
		
		
		
		
	#tag EndNote

	#tag Note, Name = Updates
		
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		IMPORTANT!!! Insert notes of new updates in the next line, not at the end!!! latest update info should ALWAYS be displayed first!!!
		
		2015.02.12
		Roberto Gomez
		-Added enableForecastCCV property to integrate forecast animation of current fields from Actimar's CurExtrap.
		
		2014.09.08
		Roberto Gomez
		-Added sampInterval parameter to fix the sample interval indicated on the statistics table.
		
		2014.02.14
		Roberto Gomez
		-Added string parameters to indicate how the different quality filters in the 'Apply quality filter' pop up menu, should be called/displayed (labelQualNFilter -where N is qual level- and labelQualAllFilter).
		
		2014.01.31
		Roberto Gomez
		-Added boolean parameter to enable using wind speed data (enableWS)
		
		2013.12.12
		Roberto Gomez
		-Added parameters for using an auxiliar network to connect to the WuT climate cards:
		    enableAuxWuTWebIO
		    WuTDevPort_st
		    WuTDevIPAux_st
		
		2013.12.10
		Roberto Gomez
		-Added parameters:
		    WS_blink                = -           // Enables the blinking of OceanAlerts in Home Page
		    WS_gridPixels        = -           // Global warnings display warning area in a grid point. This number indicates how many pixels are required to fill a grid point.
		
		2013.12.08
		Roberto Gomez
		-Added parameters:
		    windDirectionFrom        = -           // Set it to true when you want the values of wind direction to be 'comming from' (meteorological convention).
		
		2013.10.09
		Roberto Gomez
		-Added parameters:
		    labelQual0        = -            // Text label used to define measurement with a quality value of 0 (not yet qualified)
		    labelQual1        = Excellent meas.    // Text label used to define measurement with a quality value of 1 (best value possible)
		    labelQual2        = Good measurement    // Text label used to define measurement with a quality value of 2 (middle quality level)
		    labelQual3        = Probably good meas.    // Text label used to define measurement with a quality value of 3 (even lower)
		    labelQual4        = Possible artefact    // Text label used to define measurement with a quality value of 4 or higher (normally used for artefacts)
		    artefThreshold    = 3            // Maximum value allowed to still be considered ok. Quality values bigger  than this are considered artefacts.
		
		
		2013.08.15
		Roberto Gomez
		-Added parameters:
		    boolean enableSPEC            // Enables viewing of spectrum data plots for wave maps
		    string graphicsFormat            // Specify format of maps graphics.
		    integer homeMapTypeCur            // 0=raw measured, 1=artifacts removed (show in home page)
		    integer homeMapTypeWav
		    integer homeMapTypeWin
		    integer animLimitHours            // Maximum number of hours allowed for animation generation
		    integer ncMapsLimitHours            // Maximum number of hours allowed for netCDF file export
		    boolean plotLimit            // If true, you cannot generate plots bigger than 1 month.
		    boolean WS_enabled            // enableWS activates the possibility to see warnings due to extreme oceanographic measurements.
		    integer WS_circleSize            // Warnings are displayed with a semi-transparent circle with given diameter size in Pixels. Normally 40
		    string WS_admin            // Which user (from access file) is allowed to configure the ocean warning thresholds.
		    string ci_headerImage        // Personalize web page header to add another corporate identity. default is WERA header.
		    string ci_NaviBarLogo        // Personalize logo shown on left navigation bar. default is HELZEL logo.
		    string ci_linkText        // Personalizes the text shown below logo. default is Helzel Messtechnick GmbH
		    string ci_link        // Web page address to which the linkText points to. default is www.helzel.com
		    boolean WaBuSV_enabled            // Enables displaying of data from SyWaBu from SeaView. Works only when feature is licenced.
		-Assigned default values on all parameters.
		
		2013.07.31
		Roberto Gomez
		-Removed parameters for DB connection, DataViewer manages this parameters from db module now.
		-Removed some parameters from each station: shortName, sname, longName, lon, lat, boresight, timeslot and acqTime. DataViewer manages now these via sites module.
		-InitializeConfig has now an input parameter to point out the path for the configuration file. This is specially useful when loading the parameters from another program that is not DataViewer (e.g. DataViewerConfigurator).
		
		2013.07.01
		Roberto Gomez
		-Added parameters:
		    boolean enableCCV            // Enables viewing of combined current vector data
		    boolean enableCSWH            // Enables viewing of combined signfinicant wave height and direction
		    boolean enableWD            // Enables viewing of combined wind direction data
		    boolean enableAR            // Enables viewing artifact removing data
		    boolean enableGF            // Enables viewing gap filled data
		    string aboutText            // Text shown in a message box when double clicking the copyright notice in the main window.
		    boolean enableRC_st<N>            // Enables viewing of radial current data on station N
		
		2013.06.28
		Roberto Gomez
		-Modified cdfDV module to be compatible also with WERADataViewer.
		-Updated deprecated t=f.CreateTextFile and t=f.AppentToTextFile to t=t.Create(f) and t=t. Append
		-Included another note (FileExample) which contains an example of configuration file contents.
		-Included WuTDevIP and useWuTWebIO parameters for displaying of WuT sensors interface in DataViewer.
		
		
		
		IMPORTANT!!! Insert notes of new update at the beginning, not at the end!!! Latest update info should ALWAYS be displayed first!!!
		
		
		
		
		
	#tag EndNote


	#tag Property, Flags = &h0
		aboutText As String = "default"
	#tag EndProperty

	#tag Property, Flags = &h0
		animLimitHours As Integer = 24
	#tag EndProperty

	#tag Property, Flags = &h0
		artefThreshold As Integer = 3
	#tag EndProperty

	#tag Property, Flags = &h0
		cfgVarsName_dv(-1) As string
	#tag EndProperty

	#tag Property, Flags = &h0
		ci_headerImage As String = "default"
	#tag EndProperty

	#tag Property, Flags = &h0
		ci_link As String = "default"
	#tag EndProperty

	#tag Property, Flags = &h0
		ci_linkText As String = "default"
	#tag EndProperty

	#tag Property, Flags = &h0
		ci_NaviBarLogo As String = "default"
	#tag EndProperty

	#tag Property, Flags = &h0
		csvAddEmptyVals As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		csvColSeparatorChr As Integer = 44
	#tag EndProperty

	#tag Property, Flags = &h0
		csvEmptyVal As Integer = -999
	#tag EndProperty

	#tag Property, Flags = &h0
		enableAR As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		enableAuxWuTWebIO As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		enableCCV As Boolean = true
	#tag EndProperty

	#tag Property, Flags = &h0
		enableCSWH As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		enableForecastCCV As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		enableGF As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		enableQ1 As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		enableQ2 As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		enableQ4 As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		enableRC_st(-1) As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		enableSPEC As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		enableWD As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		enableWS As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		graphicsFormat As String = ".gif"
	#tag EndProperty

	#tag Property, Flags = &h0
		gridID As Int8 = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		homeMapTypeCur As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		homeMapTypeWav As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		homeMapTypeWin As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		initialMapType As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		labelQual0 As String = "-"
	#tag EndProperty

	#tag Property, Flags = &h0
		labelQual1 As String = "Excellent meas."
	#tag EndProperty

	#tag Property, Flags = &h0
		labelQual1Filter As String = "Quality level 1"
	#tag EndProperty

	#tag Property, Flags = &h0
		labelQual2 As String = "Good measurement"
	#tag EndProperty

	#tag Property, Flags = &h0
		labelQual2Filter As String = "Quality level 2"
	#tag EndProperty

	#tag Property, Flags = &h0
		labelQual3 As String = "Probably good meas."
	#tag EndProperty

	#tag Property, Flags = &h0
		labelQual3Filter As String = "Quality level 3"
	#tag EndProperty

	#tag Property, Flags = &h0
		labelQual4 As String = "Possible artefact"
	#tag EndProperty

	#tag Property, Flags = &h0
		labelQual4Filter As String = "Quality level 4"
	#tag EndProperty

	#tag Property, Flags = &h0
		labelQualAllFilter As String = "All measured data"
	#tag EndProperty

	#tag Property, Flags = &h0
		language As String = "auto"
	#tag EndProperty

	#tag Property, Flags = &h0
		LatRefPt1 As double = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		LatRefPt2 As double = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		lineIdxOfVars_dv(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		linesOfTxtFile_dv(-1) As string
	#tag EndProperty

	#tag Property, Flags = &h0
		LonRefPt1 As double = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		LonRefPt2 As double = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		mapsOrganized As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		ncMapsLimitHours As Integer = 24
	#tag EndProperty

	#tag Property, Flags = &h0
		nc_comment As string = "File was generated from WERA DataViewer."
	#tag EndProperty

	#tag Property, Flags = &h0
		nc_contact As string = "#std.kContactEmail"
	#tag EndProperty

	#tag Property, Flags = &h0
		nc_history As string = "-"
	#tag EndProperty

	#tag Property, Flags = &h0
		nc_institution As string = "#std.kComName"
	#tag EndProperty

	#tag Property, Flags = &h0
		nc_institution_url As string = "#std.kWebPage"
	#tag EndProperty

	#tag Property, Flags = &h0
		nc_references As string = "-"
	#tag EndProperty

	#tag Property, Flags = &h0
		nc_source As string = "WERA Radar System"
	#tag EndProperty

	#tag Property, Flags = &h0
		nStations As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		OA_Transparency As Integer = 100
	#tag EndProperty

	#tag Property, Flags = &h0
		paramscfg As string = "params.cfg"
	#tag EndProperty

	#tag Property, Flags = &h0
		plotLimit As boolean = true
	#tag EndProperty

	#tag Property, Flags = &h0
		plotUseWaterMark As Boolean = true
	#tag EndProperty

	#tag Property, Flags = &h0
		plotWaterMarkFile As String = "wera_small.jpg"
	#tag EndProperty

	#tag Property, Flags = &h0
		PxRefPt1 As single = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		PxRefPt2 As single = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		PyRefPt1 As single = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		PyRefPt2 As single = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		sampInterval As Single = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		sFolder_cur_asc As string = "cur_asc/"
	#tag EndProperty

	#tag Property, Flags = &h0
		sFolder_st(-1) As string
	#tag EndProperty

	#tag Property, Flags = &h0
		sFolder_wauv_asc As string = "wav_asc/"
	#tag EndProperty

	#tag Property, Flags = &h0
		sFolder_wiuv_asc As string = "wav_asc/"
	#tag EndProperty

	#tag Property, Flags = &h0
		sSufix_cur_asc As string = "_LNG.cur_asc"
	#tag EndProperty

	#tag Property, Flags = &h0
		sSufix_st(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sSufix_wauv_asc As string = "_LNG.wav_asc"
	#tag EndProperty

	#tag Property, Flags = &h0
		sSufix_wiuv_asc As string = "_LNG.win_asc"
	#tag EndProperty

	#tag Property, Flags = &h0
		stdFolder As String = "/home/wera/data/"
	#tag EndProperty

	#tag Property, Flags = &h0
		systemName As string = "WERA"
	#tag EndProperty

	#tag Property, Flags = &h0
		titleInBrowser As String = "WERA DataViewer"
	#tag EndProperty

	#tag Property, Flags = &h0
		useSeconds As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		useWuTWebIO As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		WaBuSV_enabled As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		welcomeText As String = "none"
	#tag EndProperty

	#tag Property, Flags = &h0
		windDirectionFrom As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		WS_admin As String = "root"
	#tag EndProperty

	#tag Property, Flags = &h0
		WS_blink As Boolean = true
	#tag EndProperty

	#tag Property, Flags = &h0
		WS_circleSize As Integer = 40
	#tag EndProperty

	#tag Property, Flags = &h0
		WS_enabled As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		WS_gridPixels As Integer = 10
	#tag EndProperty

	#tag Property, Flags = &h0
		WuTDevIPAux_st(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		WuTDevIP_st(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		WuTDevPort_st(-1) As string
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="aboutText"
			Group="Behavior"
			InitialValue="default"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="animLimitHours"
			Group="Behavior"
			InitialValue="24"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="artefThreshold"
			Group="Behavior"
			InitialValue="3"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ci_headerImage"
			Group="Behavior"
			InitialValue="default"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ci_link"
			Group="Behavior"
			InitialValue="default"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ci_linkText"
			Group="Behavior"
			InitialValue="default"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ci_NaviBarLogo"
			Group="Behavior"
			InitialValue="default"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="csvAddEmptyVals"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="csvColSeparatorChr"
			Group="Behavior"
			InitialValue="44"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="csvEmptyVal"
			Group="Behavior"
			InitialValue="-999"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="enableAR"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="enableAuxWuTWebIO"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="enableCCV"
			Group="Behavior"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="enableCSWH"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="enableForecastCCV"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="enableGF"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="enableQ1"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="enableQ2"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="enableQ4"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="enableSPEC"
			Group="Behavior"
			InitialValue="false"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="enableWD"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="enableWS"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="graphicsFormat"
			Group="Behavior"
			InitialValue=".gif"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="homeMapTypeCur"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="homeMapTypeWav"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="homeMapTypeWin"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="initialMapType"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelQual0"
			Group="Behavior"
			InitialValue="-"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelQual1"
			Group="Behavior"
			InitialValue="Excellent meas."
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelQual1Filter"
			Group="Behavior"
			InitialValue="Quality level 1"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelQual2"
			Group="Behavior"
			InitialValue="Good measurement"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelQual2Filter"
			Group="Behavior"
			InitialValue="Quality level 2"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelQual3"
			Group="Behavior"
			InitialValue="Probably good meas."
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelQual3Filter"
			Group="Behavior"
			InitialValue="Quality level 3"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelQual4"
			Group="Behavior"
			InitialValue="Possible artefact"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelQual4Filter"
			Group="Behavior"
			InitialValue="Quality level 4"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelQualAllFilter"
			Group="Behavior"
			InitialValue="All measured data"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="language"
			Group="Behavior"
			InitialValue="auto"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LatRefPt1"
			Group="Behavior"
			InitialValue="1"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LatRefPt2"
			Group="Behavior"
			InitialValue="1"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LonRefPt1"
			Group="Behavior"
			InitialValue="1"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LonRefPt2"
			Group="Behavior"
			InitialValue="1"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mapsOrganized"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ncMapsLimitHours"
			Group="Behavior"
			InitialValue="24"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nc_comment"
			Group="Behavior"
			InitialValue="File was generated from WERA DataViewer."
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nc_contact"
			Group="Behavior"
			InitialValue="info@helzel.com"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nc_history"
			Group="Behavior"
			InitialValue="-"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nc_institution"
			Group="Behavior"
			InitialValue="Helzel Messtechnik GmbH"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nc_institution_url"
			Group="Behavior"
			InitialValue="www.helzel.com"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nc_references"
			Group="Behavior"
			InitialValue="-"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nc_source"
			Group="Behavior"
			InitialValue="WERA Radar System"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nStations"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OA_Transparency"
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="paramscfg"
			Group="Behavior"
			InitialValue="params.cfg"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="plotLimit"
			Group="Behavior"
			InitialValue="true"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="plotUseWaterMark"
			Group="Behavior"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="plotWaterMarkFile"
			Group="Behavior"
			InitialValue="wera_small.jpg"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PxRefPt1"
			Group="Behavior"
			InitialValue="1"
			Type="single"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PxRefPt2"
			Group="Behavior"
			InitialValue="1"
			Type="single"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PyRefPt1"
			Group="Behavior"
			InitialValue="1"
			Type="single"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PyRefPt2"
			Group="Behavior"
			InitialValue="1"
			Type="single"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sampInterval"
			Group="Behavior"
			InitialValue="0"
			Type="Single"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sFolder_cur_asc"
			Group="Behavior"
			InitialValue="cur_asc/"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sFolder_wauv_asc"
			Group="Behavior"
			InitialValue="wav_asc/"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sFolder_wiuv_asc"
			Group="Behavior"
			InitialValue="wav_asc/"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sSufix_cur_asc"
			Group="Behavior"
			InitialValue="_LNG.cur_asc"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sSufix_wauv_asc"
			Group="Behavior"
			InitialValue="_LNG.wav_asc"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sSufix_wiuv_asc"
			Group="Behavior"
			InitialValue="_LNG.win_asc"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="stdFolder"
			Group="Behavior"
			InitialValue="/home/wera/data/"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="systemName"
			Group="Behavior"
			InitialValue="WERA"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="titleInBrowser"
			Group="Behavior"
			InitialValue="WERA Data Viewer"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="useSeconds"
			Group="Behavior"
			InitialValue="false"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="useWuTWebIO"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WaBuSV_enabled"
			Group="Behavior"
			InitialValue="false"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="welcomeText"
			Group="Behavior"
			InitialValue="none"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="windDirectionFrom"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WS_admin"
			Group="Behavior"
			InitialValue="root"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WS_blink"
			Group="Behavior"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WS_circleSize"
			Group="Behavior"
			InitialValue="40"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WS_enabled"
			Group="Behavior"
			InitialValue="false"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WS_gridPixels"
			Group="Behavior"
			InitialValue="5"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
