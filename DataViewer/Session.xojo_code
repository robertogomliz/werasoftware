#tag Class
Protected Class Session
Inherits WebSession
	#tag Event
		Sub Close()
		  
		  if App.sessionsIDs.IndexOf(me.Identifier) >=0 then  'To avoid an OutOfBoundsException which I do not know why it happens... (THIS IS A WORKAROUND... WE STILL HAVE TO FIX IT!!!)
		    
		    App.sessionsIDs.Remove( App.sessionsIDs.IndexOf(me.Identifier) )  // Remove itself from the sessions list
		    log.Write(false,"Session "+ left(me.Identifier,4) + " closed from " + me.RemoteAddress)
		    
		  else
		    'It does not exists anyway... let's try to remove it.
		    me.Timeout=1
		    me.Quit
		  end
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  
		  
		  log.Write(false,"Session "+ left(me.Identifier,4) + " opened from " + me.RemoteAddress)
		  
		  App.sessionsIDs.Append(me.Identifier)
		  
		  if not (db.archive.connect) then
		    if db.HandleDatabaseError("Could not connect to database when opening session!") then
		    end
		    db.archive.Close
		  end
		End Sub
	#tag EndEvent

	#tag Event
		Sub TimedOut()
		  
		  if App.sessionsIDs.IndexOf(me.Identifier) >=0 then  'To avoid an OutOfBoundsException which I do not know why it happens... (THIS IS A WORKAROUND... WE STILL HAVE TO FIX IT!!!)
		    
		    App.sessionsIDs.Remove( App.sessionsIDs.IndexOf(me.Identifier) )  // Remove itself from the sessions list
		    
		  else
		    'It does not exists anyway... let's try to remove it.
		    me.Quit
		  end
		End Sub
	#tag EndEvent

	#tag Event
		Function UnhandledException(Error As RuntimeException) As Boolean
		  
		  
		  'If the unhandled exception ocurred while plotting, then release the plotting pass for other sessions.
		  App.SessionFailed=true
		  
		  log.Write(false,"Session " + CStr(App.sessionsIDs.IndexOf(me.Identifier)) + " unhandled exception! Described next.")
		  
		End Function
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub ShowWelcomeMsg()
		  dim auxString as String
		  
		  if App.StageCode=3 then
		    auxString=App.kAppName + " V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + EndOfLine + EndOfLine
		  else
		    auxString=App.kAppName + " Beta V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + EndOfLine + EndOfLine
		  end
		  
		  if cfgDV.welcomeText="default" then
		    MsgBox(auxString + "(C) " + CStr(App.ExecutableFile.ModificationDate.Year) + " " + std.kComName + ". " + App.kAllRightsReserved(cfgDV.language))
		  ElseIf cfgDV.welcomeText="none" then
		    // No pop up
		  else
		    MsgBox(auxString + cfgDV.welcomeText)
		  end
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		account As string
	#tag EndProperty

	#tag Property, Flags = &h0
		cancelLoading As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		gapFillingEnabled As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		StdWebFile As WebFile
	#tag EndProperty


	#tag Constant, Name = ErrorDialogCancel, Type = String, Dynamic = True, Default = \"Do Not Send", Scope = Public
		#Tag Instance, Platform = Any, Language = es, Definition  = \"No enviar"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Do Not Send"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"N\xC3\xA3o enviar"
	#tag EndConstant

	#tag Constant, Name = ErrorDialogMessage, Type = String, Dynamic = True, Default = \"This application has encountered an exception and cannot continue.", Scope = Public
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Esta aplicaci\xC3\xB3n ha encontrado una excepci\xC3\xB3n y no puede continuar."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"This application has encountered an exception and cannot continue."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Este aplicativo encontrou uma exce\xC3\xA7\xC3\xA3o e n\xC3\xA3o pode continuar."
	#tag EndConstant

	#tag Constant, Name = ErrorDialogQuestion, Type = String, Dynamic = True, Default = \"Please describe what you were doing right before the exception occurred:", Scope = Public
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Por favor describa qu\xC3\xA9 es lo que estaba haciendo antes de que ocurriera la excepci\xC3\xB3n:"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Please describe what you were doing right before the exception occurred:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Por favor\x2C descreva o que voc\xC3\xAA estava fazendo para a direita antes a exce\xC3\xA7\xC3\xA3o ocorreu:"
	#tag EndConstant

	#tag Constant, Name = ErrorDialogSubmit, Type = String, Dynamic = True, Default = \"Send", Scope = Public
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Enviar"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Please describe what you were doing right before the exception occurred:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Enviar"
	#tag EndConstant

	#tag Constant, Name = ErrorThankYou, Type = String, Dynamic = True, Default = \"Thank You", Scope = Public
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Gracias"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Thank You"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Obrigado"
	#tag EndConstant

	#tag Constant, Name = ErrorThankYouMessage, Type = String, Dynamic = True, Default = \"Your feedback helps us make improvements.", Scope = Public
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Su retroalimentaci\xC3\xB3n nos ayuda a hacer mejoras."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Your feedback helps us make improvements."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"O seu feedback nos ajuda a fazer melhorias."
	#tag EndConstant

	#tag Constant, Name = NoJavascriptInstructions, Type = String, Dynamic = True, Default = \"To turn Javascript on\x2C please refer to your browser settings window.", Scope = Public
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Para activar Javascript\x2C por favor verifique los ajustes de su navegador."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"To turn Javascript on\x2C please refer to your browser settings window."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Para ativar o JavaScript\x2C por favor consulte a janela de configura\xC3\xA7\xC3\xB5es do navegador."
	#tag EndConstant

	#tag Constant, Name = NoJavascriptMessage, Type = String, Dynamic = True, Default = \"Javascript must be enabled to access this page.", Scope = Public
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Javascript debe estar habilitado para acceder a esta p\xC3\xA1gina."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Javascript must be enabled to access this page."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Javascript deve estar habilitado para acessar a p\xC3\xA1gina."
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="account"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ActiveConnectionCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Browser"
			Group="Behavior"
			Type="BrowserType"
			EditorType="Enum"
			#tag EnumValues
				"0 - Unknown"
				"1 - Safari"
				"2 - Chrome"
				"3 - Firefox"
				"4 - InternetExplorer"
				"5 - Opera"
				"6 - ChromeOS"
				"7 - SafariMobile"
				"8 - Android"
				"9 - Blackberry"
				"10 - OperaMini"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="BrowserVersion"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="cancelLoading"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ConfirmMessage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Connection"
			Group="Behavior"
			Type="ConnectionType"
			EditorType="Enum"
			#tag EnumValues
				"0 - AJAX"
				"1 - WebSocket"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="gapFillingEnabled"
			Group="Behavior"
			InitialValue="false"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="GMTOffset"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HashTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Identifier"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LanguageCode"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LanguageRightToLeft"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PageCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Platform"
			Group="Behavior"
			Type="PlatformType"
			EditorType="Enum"
			#tag EnumValues
				"0 - Unknown"
				"1 - Macintosh"
				"2 - Windows"
				"3 - Linux"
				"4 - Wii"
				"5 - PS3"
				"6 - iPhone"
				"7 - iPodTouch"
				"8 - Blackberry"
				"9 - WebOS"
				"10 - iPad"
				"11 - AndroidTablet"
				"12 - AndroidPhone"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Protocol"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RemoteAddress"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RenderingEngine"
			Group="Behavior"
			Type="EngineType"
			EditorType="Enum"
			#tag EnumValues
				"0 - Unknown"
				"1 - WebKit"
				"2 - Gecko"
				"3 - Trident"
				"4 - Presto"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="StatusMessage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Timeout"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Title"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="URL"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Expiration"
			Group="Behavior"
			InitialValue="-1"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_hasQuit"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_mConnection"
			Group="Behavior"
			Type="ConnectionType"
			EditorType="Enum"
			#tag EnumValues
				"0 - AJAX"
				"1 - WebSocket"
			#tag EndEnumValues
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
