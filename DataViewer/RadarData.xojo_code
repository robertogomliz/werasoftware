#tag WebPage
Begin WebContainer RadarData
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   1820
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   199
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "StyContainer"
   TabOrder        =   0
   Top             =   36
   VerticalCenter  =   0
   Visible         =   True
   Width           =   950
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebImageView ImageSites
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   299
      HelpTag         =   "#kImageSitesHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   12
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1815517183
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   246
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   300
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView ImageGDOP
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   299
      HelpTag         =   "#kImageGDOPHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   103571455
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   246
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   300
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView ImageGrid
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   299
      HelpTag         =   "#kImageGridHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   638
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   283262975
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   246
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   300
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebListBox StationsTable
      AlternateRowColor=   &c44444400
      ColumnCount     =   13
      ColumnWidths    =   "22, 90, 50, 70, 73, 50, 60, 90, 90, 65, 80,70,50"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "0"
      Height          =   98
      HelpTag         =   "#kStationsTableHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "ID	Name	Acronym	Latitude	Longitude	True N	Frequency	Acquisition time	Cur. Integration	Cur. Range	Cur. Accuracy	Cur. Limits	Status\n1	Casswell	csw	33.88916 N	79.02583 W	23|53							\n2	Georgetown	gtn	33.35611 N	79.1525 W	23|53							"
      Left            =   12
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   False
      PrimaryRowColor =   &c44444400
      Scope           =   0
      SelectionStyle  =   "548460543"
      Style           =   "1821235199"
      TabOrder        =   -1
      Top             =   64
      VerticalCenter  =   0
      Visible         =   True
      Width           =   875
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label1
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   31
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   12
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   0
      Text            =   "#kStationInformation"
      Top             =   21
      VerticalCenter  =   0
      Visible         =   True
      Width           =   300
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   31
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   170
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1077731327"
      TabOrder        =   1
      Text            =   "#kClickOnPicture"
      Top             =   203
      VerticalCenter  =   0
      Visible         =   True
      Width           =   619
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label3
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   31
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   27
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1077731327"
      TabOrder        =   2
      Text            =   "#kRadarCoverage"
      Top             =   550
      VerticalCenter  =   0
      Visible         =   True
      Width           =   254
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label4
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   31
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   350
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1077731327"
      TabOrder        =   3
      Text            =   "#kGDOP"
      Top             =   550
      VerticalCenter  =   0
      Visible         =   True
      Width           =   254
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label5
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   31
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   661
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1077731327"
      TabOrder        =   4
      Text            =   "#kMeasurementGrid"
      Top             =   550
      VerticalCenter  =   0
      Visible         =   True
      Width           =   254
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebHTMLViewer ExtSensorsViewer
      Cursor          =   0
      Enabled         =   False
      Height          =   600
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   0
      Left            =   -254
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   5
      Top             =   600
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   1200
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebHTMLViewer ExtSensorsViewer
      Cursor          =   0
      Enabled         =   False
      Height          =   600
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   1
      Left            =   -254
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   6
      Top             =   1210
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   1200
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView StationStatus
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   906
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   683591679
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   89
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   20
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel StationStatusLabel
      Cursor          =   2
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   892
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   True
      Scope           =   0
      Style           =   "637450239"
      TabOrder        =   7
      Text            =   "#kStatusInfo"
      Top             =   110
      VerticalCenter  =   0
      Visible         =   False
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  
		  Dim i as Integer
		  
		  
		  //===========Initialize StationsTable================
		  StationsTable.DeleteAllRows
		  for i=0 to cfgDV.nStations-1
		    StationsTable.InsertRow(i,CStr(i+1),siteInfo.longname(i),siteInfo.acronym(i),siteInfo.slat(i),siteInfo.slon(i),siteInfo.tnor(i) + "°",CStr(siteInfo.freq(i)) + " MHz",siteInfo.acqTime(i),siteInfo.curIntegration(i) + " samples",siteInfo.curRange(i) + " km","~" + siteInfo.typCurAcc(i) + " m/s","(" + siteInfo.curLimDown(i) + "," + siteInfo.curLimUp(i) + ") m/s","OK")
		  next
		  
		  for i=0 to StationsTable.ColumnCount-1
		    StationsTable.ColumnStyle(i)=CommonCenterSmall
		  next
		  
		  //=========== Load small images==============
		  
		  Dim FI as New FolderItem
		  Dim Pict1 as WebPicture
		  
		  FI=GetFolderItem(App.execDir + "WERA_sites_small.gif")
		  
		  if FI.Exists then
		    Pict1=new WebPicture(FI)
		    if Pict1 = Nil then
		      log.Write(true,"Could not load small sites image!")
		    else
		      ImageSites.Picture=Pict1
		    end
		  else
		    log.Write(true,"Could not find small sites image!")
		  end
		  
		  if cfgDV.nStations>1 then
		    
		    FI=GetFolderItem(App.execDir + "WERA_GDOP_small.gif")
		    
		    if FI.Exists then
		      Pict1=new WebPicture(FI)
		      if Pict1 = Nil then
		        log.Write(true,"Could not load small GDOP image!")
		      else
		        ImageGDOP.Picture=Pict1
		      end
		    else
		      log.Write(true,"Could not find small GDOP image!")
		    end
		  else
		    ImageGDOP.Enabled=false
		    ImageGDOP.Visible=false
		    Label4.Enabled=false
		    Label4.Visible=False
		    Label2.top=350
		  end
		  
		  FI=GetFolderItem(App.execDir + "WERA_grid_small.gif")
		  
		  if FI.Exists then
		    Pict1=new WebPicture(FI)
		    if Pict1 = Nil then
		      log.Write(true,"Could not load small grid image!")
		    else
		      ImageGrid.Picture=Pict1
		    end
		  else
		    log.Write(true,"Could not find small grid image!")
		  end
		  
		  // ===============Modify System Overview page in case there are WuT sensors in the containers.======================
		  if cfgDV.useWuTWebIO then
		    'self.Height=RadarDataPage.Height+(cfgDV.nStations*610)
		    
		    for i=0 to cfgDV.nStations-1
		      ExtSensorsViewer(i).Enabled=true
		      ExtSensorsViewer(i).Visible=true
		    next
		    
		  end
		  
		  
		  // Changing localizable strings
		  
		  Label1.Text=kStationInformation(cfgDV.language)
		  Label2.Text=kClickOnPicture(cfgDV.language)
		  Label3.Text=kRadarCoverage(cfgDV.language)
		  Label4.Text=kGDOP(cfgDV.language)
		  Label5.Text=kMeasurementGrid(cfgDV.language)
		  
		  StationsTable.HelpTag=kStationsTableHelpTag(cfgDV.language)
		  ImageSites.HelpTag=kImageSitesHelpTag(cfgDV.language)
		  ImageGDOP.HelpTag=kImageGDOPHelpTag(cfgDV.language)
		  ImageGrid.HelpTag=kImageGridHelpTag(cfgDV.language)
		  StationStatusLabel.Text=kStatusInfo(cfgDV.language)
		  
		  StationsTable.Heading(0)=kID(cfgDV.language)
		  StationsTable.Heading(1)=kStName(cfgDV.language)
		  StationsTable.Heading(2)=kAcronym(cfgDV.language)
		  StationsTable.Heading(3)=Main.kLat(cfgDV.language)
		  StationsTable.Heading(4)=Main.kLon(cfgDV.language)
		  StationsTable.Heading(5)=kBS(cfgDV.language)
		  StationsTable.Heading(6)=kFreq(cfgDV.language)
		  StationsTable.Heading(7)=kAcqTime(cfgDV.language)
		  StationsTable.Heading(8)=kTimeIntCur(cfgDV.language)
		  StationsTable.Heading(9)=kRangeCur(cfgDV.language)
		  StationsTable.Heading(10)=kAccCur(cfgDV.language)
		  StationsTable.Heading(11)=kLimitsCur(cfgDV.language)
		  StationsTable.Heading(12)=kStatus(cfgDV.language)
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub Shown()
		  
		  dim i as integer
		  
		  if cfgDV.useWuTWebIO then
		    if cfgDV.enableAuxWuTWebIO then
		      MySession=Session
		      Main.threadProcessRequired=6 // threadProcessRequired=2 means animation creation process
		      Main.ParallelThread.Run
		    else
		      
		      for i=0 to cfgDV.nStations-1
		        ExtSensorsViewer(i).URL="http://" + cfgDV.WuTDevIP_st(i) + cfgDV.WuTDevPort_st(i) + "/index"
		      next
		    end
		  end
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub CheckOnWuTConnection()
		  
		  
		  dim i as integer
		  dim sh as new shell
		  sh.mode=1
		  
		  for i=0 to cfgDV.nStations-1
		    
		    sh.Execute("ping -c 4 -q " + cfgDV.WuTDevIP_st(i))
		    
		    while(sh.IsRunning)
		      sh.Poll()
		    wend
		    
		    if sh.ErrorCode = 0 then 'Connection succesful, use primary
		      ExtSensorsViewer(i).URL="http://" + cfgDV.WuTDevIP_st(i) + cfgDV.WuTDevPort_st(i) + "/index"
		    else 'Connection failed... try auxiliar link
		      ExtSensorsViewer(i).URL="http://" + cfgDV.WuTDevIPAux_st(i) + cfgDV.WuTDevPort_st(i) + "/index"
		    end
		    
		  next
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateStationStatus()
		  if sm.warningAvail(0) or sm.warningAvail(1) or (sm.extraWarnings<>EndOfLine) then
		    StationStatus.Visible=true
		    StationStatusLabel.Visible=true
		  else
		    StationStatus.Visible=false
		    StationStatusLabel.Visible=false
		  end if
		  
		  
		  if sm.warningAvail(0) then
		    StationsTable.Cell(0,12)="(!)"
		    StationsTable.CellStyle(0,12)=CommonCenterSmallRed
		  else
		    StationsTable.Cell(0,12)="OK"
		    StationsTable.CellStyle(0,12)=CommonCenterSmall
		  end
		  
		  if cfgDV.nStations>1 then
		    if sm.warningAvail(1) then
		      StationsTable.Cell(1,12)="(!)"
		      StationsTable.CellStyle(1,12)=CommonCenterSmallRed
		    else
		      StationsTable.Cell(1,12)="OK"
		      StationsTable.CellStyle(1,12)=CommonCenterSmall
		    end
		  end
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		MySession As WebSession
	#tag EndProperty


	#tag Constant, Name = kAccCur, Type = String, Dynamic = True, Default = \"Cur. accuracy", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cur. accuracy"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Precisi\xC3\xB3n"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Precis\xC3\xA3o"
	#tag EndConstant

	#tag Constant, Name = kAcqTime, Type = String, Dynamic = True, Default = \"Acquisition time", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Acquisition time"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Tiempo de med."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Tempo de med."
	#tag EndConstant

	#tag Constant, Name = kAcronym, Type = String, Dynamic = True, Default = \"Acronym", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Acronym"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"C\xC3\xB3digo"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Acr\xC3\xB4nimo"
	#tag EndConstant

	#tag Constant, Name = kBS, Type = String, Dynamic = True, Default = \"Boresight", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Boresight"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Boresight"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Boresight"
	#tag EndConstant

	#tag Constant, Name = kClickOnPicture, Type = String, Dynamic = True, Default = \"Click on the following maps to see them full size.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Click on the following maps to see them full size."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Haga clic en los siguientes mapas para verlos en tama\xC3\xB1o completo."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Haga clic en los siguientes mapas para verlos en tama\xC3\xB1o completo."
	#tag EndConstant

	#tag Constant, Name = kFreq, Type = String, Dynamic = True, Default = \"Frequency", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Frequency"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Frecuencia"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Freq\xC3\xBC\xC3\xAAncia"
	#tag EndConstant

	#tag Constant, Name = kFromSitenameStation, Type = String, Dynamic = True, Default = \"From <sitename> station->", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"From <sitename> station->"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"De la estaci\xC3\xB3n <sitename>->"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Esta\xC3\xA7\xC3\xA3o <sitename>->"
	#tag EndConstant

	#tag Constant, Name = kGDOP, Type = String, Dynamic = True, Default = \"Geometric Dillution of Precision (GDOP)", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Geometric Dillution of Precision (GDOP)"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Diluci\xC3\xB3n geom\xC3\xA9trica de precisi\xC3\xB3n"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Dilui\xC3\xA7\xC3\xA3o geom\xC3\xA9trica da precis\xC3\xA3o (GDOP)"
	#tag EndConstant

	#tag Constant, Name = kID, Type = String, Dynamic = True, Default = \"ID", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"ID"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"#"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"No."
	#tag EndConstant

	#tag Constant, Name = kImageGDOPHelpTag, Type = String, Dynamic = True, Default = \"Map of Geometric Dilution of Precision.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Map of Geometric Dilution of Precision."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mapa de la diluci\xC3\xB3n geom\xC3\xA9trica de la precisi\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Mapa de Dilui\xC3\xA7\xC3\xA3o geom\xC3\xA9trica de precis\xC3\xA3o."
	#tag EndConstant

	#tag Constant, Name = kImageGridHelpTag, Type = String, Dynamic = True, Default = \"Map of grid arrangement.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Map of grid arrangement."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mapa de las celdas que conforman la cuadr\xC3\xADcula de cobertura."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Mapa do arranjo de grelha."
	#tag EndConstant

	#tag Constant, Name = kImageSitesHelpTag, Type = String, Dynamic = True, Default = \"Map of radar coverage.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Map of radar coverage."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mapa de cobertura de las estaciones de radar."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Mapa de cobertura de radar."
	#tag EndConstant

	#tag Constant, Name = kLimitsCur, Type = String, Dynamic = True, Default = \"Cur. limits", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cur. limits"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"L\xC3\xADmites"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Limites"
	#tag EndConstant

	#tag Constant, Name = kMeasurementGrid, Type = String, Dynamic = True, Default = \"Measurement grid", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Measurement grid"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Cuadr\xC3\xADcula de cobertura"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Grelha de medi\xC3\xA7\xC3\xA3o"
	#tag EndConstant

	#tag Constant, Name = kRadarCoverage, Type = String, Dynamic = True, Default = \"Radar Coverage", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Radar Coverage"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Cobertura de los radares"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Cobertura de radar"
	#tag EndConstant

	#tag Constant, Name = kRangeCur, Type = String, Dynamic = True, Default = \"Cur. Range", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cur. range"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Rango"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Alcance"
	#tag EndConstant

	#tag Constant, Name = kStationInformation, Type = String, Dynamic = True, Default = \"Information of stations", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Information of stations"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Informaci\xC3\xB3n de las estaciones"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Informa\xC3\xA7\xC3\xB5es de esta\xC3\xA7\xC3\xB5es"
	#tag EndConstant

	#tag Constant, Name = kStationsTableHelpTag, Type = String, Dynamic = True, Default = \"Relevant information of each radar station.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Relevant information of each radar station."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Informaci\xC3\xB3n relevante de cada una de las estaciones de radar."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Informa\xC3\xA7\xC3\xB5es relevantes de cada esta\xC3\xA7\xC3\xA3o de radar."
	#tag EndConstant

	#tag Constant, Name = kStatus, Type = String, Dynamic = True, Default = \"Status", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Status"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Estado"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Estado"
	#tag EndConstant

	#tag Constant, Name = kStatusInfo, Type = String, Dynamic = True, Default = \"Status info", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Status info"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Info de est\xC3\xA1tus"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Info de estado"
	#tag EndConstant

	#tag Constant, Name = kStName, Type = String, Dynamic = True, Default = \"Name", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Name"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Nombre"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Nome"
	#tag EndConstant

	#tag Constant, Name = kTimeIntCur, Type = String, Dynamic = True, Default = \"Cur. Integration", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cur. Integration"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Integraci\xC3\xB3n temporal"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Integra\xC3\xA7\xC3\xA3o em tempo"
	#tag EndConstant


#tag EndWindowCode

#tag Events ImageSites
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  Dim file As FolderItem
		  
		  file = GetFolderItem(App.execDir + "WERA_sites.gif")
		  
		  Session.StdWebFile=WebFile.Open(file)
		  Session.StdWebFile.ForceDownload=false
		  
		  ExecuteJavaScript("window.open('"+ Session.StdWebFile.URL + "');")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ImageGDOP
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  Dim file As FolderItem
		  
		  file = GetFolderItem(App.execDir + "WERA_GDOP.gif")
		  
		  Session.StdWebFile=WebFile.Open(file)
		  Session.StdWebFile.ForceDownload=false
		  
		  ExecuteJavaScript("window.open('"+ Session.StdWebFile.URL + "');")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ImageGrid
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  Dim file As FolderItem
		  
		  file = GetFolderItem(App.execDir + "WERA_grid.gif")
		  
		  Session.StdWebFile=WebFile.Open(file)
		  Session.StdWebFile.ForceDownload=false
		  
		  ExecuteJavaScript("window.open('"+ Session.StdWebFile.URL + "');")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events StationStatus
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  dim s as string
		  
		  if sm.warningAvail(0) then
		    s=replace(kFromSitenameStation,"<sitename>",siteInfo.longname(0)) + EndOfLine + sm.detailsString(0) + EndOfLine
		  end
		  
		  if sm.warningAvail(1) then
		    s=s+replace(kFromSitenameStation,"<sitename>",siteInfo.longname(1)) + EndOfLine + sm.detailsString(1) + EndOfLine
		  end
		  
		  if sm.extraWarnings<>EndOfLine then
		    s=s+sm.extraWarnings
		  end
		  
		  MsgBox(s)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events StationStatusLabel
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  dim s as string
		  
		  if sm.warningAvail(0) then
		    s=replace(kFromSitenameStation,"<sitename>",siteInfo.longname(0)) + EndOfLine + sm.detailsString(0) + EndOfLine
		  end
		  
		  if sm.warningAvail(1) then
		    s=s+replace(kFromSitenameStation,"<sitename>",siteInfo.longname(1)) + EndOfLine + sm.detailsString(1) + EndOfLine
		  end
		  
		  if sm.extraWarnings<>EndOfLine then
		    s=s+sm.extraWarnings
		  end
		  
		  MsgBox(s)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
