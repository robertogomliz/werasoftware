#tag WebPage
Begin WebPage Login
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   400
   HelpTag         =   ""
   HorizontalCenter=   0
   ImplicitInstance=   True
   Index           =   0
   IsImplicitInstance=   False
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   LockVertical    =   False
   MinHeight       =   400
   MinWidth        =   600
   Style           =   ""
   TabOrder        =   0
   Title           =   "#kLoginTitle"
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   600
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _ImplicitInstance=   False
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin LoginDialogDef LoginDialog
      Cursor          =   0
      Enabled         =   True
      Height          =   220
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinHeight       =   0
      MinWidth        =   0
      Resizable       =   False
      Scope           =   0
      Style           =   "84463615"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Title           =   "#kLoginDialogDefTitle"
      Top             =   20
      Type            =   3
      VerticalCenter  =   0
      Visible         =   True
      Width           =   270
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Lbl
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   268
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   40
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   ""
      Top             =   40
      VerticalCenter  =   0
      Visible         =   True
      Width           =   520
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  Title=replace(kLoginTitle(cfgDV.language), "WERA", std.kRadName)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Shown()
		  
		  'If no usernames are defined, then go directly to the DataViewer without login.
		  if access.usernames.Ubound=-1 AND App.StartErrorMessages="" then
		    Session.ShowWelcomeMsg
		    main.Show
		  else
		    if App.StartErrorMessages="" then
		      LoginDialog.Show
		    else
		      if App.StageCode=3 then
		        Lbl.Text=App.kAppName + " V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + " (" + App.ExecutableFile.ModificationDate.SQLDate + ")" + EndOfLine
		      else
		        Lbl.Text=App.kAppName + " V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + " (" + App.ExecutableFile.ModificationDate.SQLDate + ")" + " Beta" + EndOfLine
		      end
		      Lbl.Text=Lbl.Text + EndOfLine + EndOfLine + replace(kLoginErrorString1(cfgDV.language),"WERA",std.kRadName) + EndOfLine
		      Lbl.Text=Lbl.Text  + App.StartErrorMessages + EndOfLine
		      Lbl.Text=Lbl.Text + replace(kLoginErrorString2(cfgDV.language),"WERA",std.kRadName)
		      Lbl.Text=Lbl.Text + replace(replace(kLoginErrorString3(cfgDV.language),"Helzel Messtechnik GmbH",std.kComName), "hzm@helzel.com",std.kContactEmail)
		    end
		  end
		End Sub
	#tag EndEvent


	#tag Constant, Name = kLoginErrorString1, Type = String, Dynamic = True, Default = \"The following issues ocurred while starting the WERA DataViewer:", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"The following issues ocurred while starting the WERA DataViewer:"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Se encontraron los siguientes problemas al iniciar WERA DataViewer:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Os seguintes problemas ocorreu ao in\xC3\xADcio da WERA DataViewer:"
	#tag EndConstant

	#tag Constant, Name = kLoginErrorString2, Type = String, Dynamic = True, Default = \"Please check the log file for more information and restart the WERA DataViewer server after solving the issues.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Please check the log file for more information and restart the WERA DataViewer server after solving the issues."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Por favor revise el archivo log para m\xC3\xA1s informaci\xC3\xB3n y reinicie el servidor de WERA DataViewer despu\xC3\xA9s de haber resuelto los problemas."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Por favor\x2C verifique o arquivo de log para obter mais informa\xC3\xA7\xC3\xB5es e reiniciar o servidor WERA DataViewer depois de resolver os problemas."
	#tag EndConstant

	#tag Constant, Name = kLoginErrorString3, Type = String, Dynamic = True, Default = \"For technical assistance contact Helzel Messtechnik GmbH (hzm@helzel.com).", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"For technical assistance contact Helzel Messtechnik GmbH (hzm@helzel.com)."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Para asistencia t\xC3\xA9cnica\x2C contacte a Helzel Messtechnik GmbH (hzm@helzel.com)."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Para assist\xC3\xAAncia t\xC3\xA9cnica contactar a Helzel Messtechnik GmbH (hzm@helzel.com)."
	#tag EndConstant

	#tag Constant, Name = kLoginTitle, Type = String, Dynamic = True, Default = \"Starting WERA DataViewer", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Starting WERA DataViewer"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Iniciando WERA DataViewer"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Come\xC3\xA7ando WERA DataViewer"
	#tag EndConstant


#tag EndWindowCode

#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Group="ID"
	#tag EndViewProperty
	#tag ViewProperty
		Name="IsImplicitInstance"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Group="Position"
		InitialValue="0"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Behavior"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Behavior"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Behavior"
		InitialValue="Untitled"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Group="Position"
		InitialValue="0"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ImplicitInstance"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
