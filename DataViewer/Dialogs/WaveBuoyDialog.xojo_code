#tag WebPage
Begin WebDialog WaveBuoyDialog
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   92
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   0
   Left            =   408
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   LockVertical    =   False
   MinHeight       =   0
   MinWidth        =   0
   Resizable       =   False
   Style           =   ""
   TabOrder        =   0
   Title           =   "Synthetic Buoy"
   Top             =   376
   Type            =   2
   VerticalCenter  =   0
   Visible         =   True
   Width           =   122
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebLabel Label2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Significant wave-height"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   1
      Text            =   "Hs"
      Top             =   6
      VerticalCenter  =   0
      Visible         =   True
      Width           =   37
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label3
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Mean wave period"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   2
      Text            =   "T1"
      Top             =   19
      VerticalCenter  =   0
      Visible         =   True
      Width           =   37
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel HsText
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Significant wave-height"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   3
      Text            =   "-63.45"
      Top             =   6
      VerticalCenter  =   0
      Visible         =   True
      Width           =   65
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel T1Text
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Mean wave period"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   4
      Text            =   "78.58"
      Top             =   19
      VerticalCenter  =   0
      Visible         =   True
      Width           =   65
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Lbl2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Mean wave direction"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   5
      Text            =   "T1_d"
      Top             =   45
      VerticalCenter  =   0
      Visible         =   True
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Lbl1
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Peak wave period"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   6
      Text            =   "Tp"
      Top             =   32
      VerticalCenter  =   0
      Visible         =   True
      Width           =   37
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel T1dText
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Mean wave direction"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   7
      Text            =   "123"
      Top             =   45
      VerticalCenter  =   0
      Visible         =   True
      Width           =   65
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel TpText
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Peak wave period"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   8
      Text            =   "1.236"
      Top             =   32
      VerticalCenter  =   0
      Visible         =   True
      Width           =   65
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblQual
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Peak wave direction"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   11
      Text            =   "Tp_d"
      Top             =   58
      VerticalCenter  =   0
      Visible         =   True
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel TpdText
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Peak wave direction"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   12
      Text            =   "123"
      Top             =   58
      VerticalCenter  =   0
      Visible         =   True
      Width           =   65
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel qText
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Quality of data"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   13
      Text            =   "Good"
      Top             =   74
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub LostFocus()
		  me.Hide
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  me.Title=kTitle(cfgDV.language)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Shown()
		  //THIS SHOUDL NOT BE NEEDED AFTER XOJO 2015 RELEASE 1... THIS IS A WORKAROUND FOR BUG 37085
		  
		  Lbl2.Visible=false
		  Lbl1.Visible=false
		  Label2.Visible=false
		  Label3.Visible=false
		  LblQual.Visible=false
		  
		  Lbl2.Visible=true
		  Lbl1.Visible=true
		  Label2.Visible=true
		  Label3.Visible=true
		  LblQual.Visible=true
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub PrepareForOnlyHs()
		  Label2.Text="Hs"
		  Label2.HelpTag="Significant wave-height"
		  Label3.Text=""
		  Label3.HelpTag=""
		  HsText.HelpTag="Significant wave-height"
		  T1Text.HelpTag=""
		  T1Text.Text=""
		  
		  Lbl1.Visible=false
		  Lbl2.Visible=false
		  LblQual.Visible=false
		  
		  TpText.Visible=false
		  T1dText.Visible=false
		  TpdText.Visible=false
		  
		  qText.top=35
		  
		  self.Height=53+15
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PrepareForWave()
		  Label2.Text="Hs"
		  Label2.HelpTag="Significant wave-height"
		  Label3.Text="T1"
		  Label3.HelpTag="Mean wave period"
		  HsText.HelpTag="Significant wave-height"
		  T1Text.HelpTag="Mean wave period"
		  
		  Lbl1.Visible=true
		  Lbl2.Visible=true
		  LblQual.Visible=true
		  
		  TpText.Visible=true
		  T1dText.Visible=true
		  TpdText.Visible=true
		  
		  qText.top=74
		  
		  self.Height=92+15
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PrepareForWind()
		  Label2.Text="Speed"
		  Label2.HelpTag="Wind speed"
		  Label3.Text="dir."
		  Label3.HelpTag="Wind direction"
		  HsText.HelpTag="Wind speed"
		  T1Text.HelpTag="Wind direction"
		  
		  Lbl1.Visible=false
		  Lbl2.Visible=false
		  LblQual.Visible=false
		  
		  TpText.Visible=false
		  T1dText.Visible=false
		  TpdText.Visible=false
		  
		  qText.top=35
		  
		  self.Height=53+15
		End Sub
	#tag EndMethod


	#tag Constant, Name = kTitle, Type = String, Dynamic = True, Default = \"Synthetic Buoy", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Synthetic Buoy"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Boya Sint\xC3\xA9tica"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Boya Sint\xC3\xA9tica"
	#tag EndConstant


#tag EndWindowCode

#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Group="ID"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Minimum Size"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Minimum Size"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizable"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Behavior"
		InitialValue="Untitled"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Type"
		Visible=true
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"1 - Sheet"
			"2 - Palette"
			"3 - Modal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
