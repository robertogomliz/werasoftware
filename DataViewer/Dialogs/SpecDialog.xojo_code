#tag WebPage
Begin WebDialog SpecDialog
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   180
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   0
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   LockVertical    =   False
   MinHeight       =   0
   MinWidth        =   0
   Resizable       =   False
   Style           =   ""
   TabOrder        =   0
   Title           =   "#kTitleWERA"
   Top             =   0
   Type            =   2
   VerticalCenter  =   0
   Visible         =   True
   Width           =   360
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebImageView ImageView1
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   180
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   0
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   0
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   360
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton FullDirBtn
      AutoDisable     =   False
      Caption         =   "#kFullDirPlot"
      Cursor          =   0
      Enabled         =   True
      Height          =   18
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   252
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   0
      Top             =   158
      VerticalCenter  =   0
      Visible         =   False
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton SwitchDataBtn
      AutoDisable     =   False
      Caption         =   "WERA data"
      Cursor          =   0
      Enabled         =   True
      Height          =   18
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   1
      Top             =   158
      VerticalCenter  =   0
      Visible         =   False
      Width           =   115
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub LostFocus()
		  me.hide
		  ShowingWaveBuoy=false
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  me.Title=replace(kTitleWERA(cfgDV.language),"WERA",std.kRadName)
		  SwitchDataBtn.Caption=replace(kWERAData(cfgDV.language),"WERA",std.kRadName)
		  FullDirBtn.Caption=kFullDirPlot(cfgDV.language)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub DisplaySpectrum(cx as Integer, cy as Integer, specArray() as Double, dirArray() as Double, includeDir as Boolean)
		  dim IsNotWERAData as Boolean
		  dim labels(-1) as string
		  dim i as Integer
		  
		  //======================= make/display plot ==========================
		  
		  //self.Title="Wave spectrum"   'dont think this is required... it should be either WERA Wave Spectrum or Synth. Wave Buoy Spectrum
		  
		  self.Left=cx
		  self.Top=cy
		  
		  dim c as new CDXYChartMBS(ImageView1.Width,ImageView1.Height)
		  
		  call c.setPlotArea(50,20,ImageView1.Width-100,ImageView1.Height-60)
		  
		  
		  if not(specArray.Ubound=20) then IsNotWERAData=true  'If the number of frequency lines is different than 21 then is not a spectrum from WERA, but from SWB.
		  
		  
		  // The labels for the chart
		  if IsNotWERAData then
		    redim labels(svwb.freqs.Ubound)
		    for i=0 to svwb.freqs.Ubound
		      labels(i)=CStr(svwb.freqs(i))
		    next
		  else
		    labels = array("0.05", "0.06", "0.07", "0.08", "0.09", "0.1", "0.11", "0.12", "0.13", "0.14", "0.15", "0.16", "0.17", "0.18", "0.19", "0.2", "0.21", "0.22", "0.23", "0.24", "0.25")
		  end
		  
		  // Add a title to the x axis
		  call c.xAxis.setTitle( kFrequency(cfgDV.language) +" [Hz]")
		  
		  // Add a title to the y axis
		  call c.yAxis.setTitle(kSpectralDensity(cfgDV.language) + " [m²/Hz]")
		  
		  // Set the labels on the x axis
		  call c.xAxis.setLabels(labels)
		  c.xAxis.setLabelStep(5)
		  
		  // Set the axes width to 2 pixels
		  c.xAxis.setWidth(2)
		  c.yAxis.setWidth(2)
		  
		  // Add a spline layer to the chart
		  dim layer as CDSplineLayerMBS
		  layer = c.addSplineLayer
		  
		  // Set the default line width to 2 pixels
		  layer.setLineWidth(2)
		  
		  // Add a data set to the spline layer, using blue (0000c0) as the line color,
		  // with yellow (ffff00) circle symbols.
		  layer.addDataSet(specArray, &h0000c0, kSpectralDensity(cfgDV.language) ).setUseYAxis(c.yAxis)
		  
		  if includeDir then
		    // Add a title to the secondary (right) y axis
		    call c.yAxis2.setTitle( kMeanDirection(cfgDV.language) + " [deg]")
		    c.yAxis2.setWidth(2)
		    
		    dim xDir(-1) as Double
		    redim xDir(dirArray.Ubound)
		    for i=0 to dirArray.Ubound
		      xDir(i)=i
		    next
		    
		    dim layer2 as CDScatterLayerMBS
		    layer2 = c.addScatterLayer(xDir,dirArray, kMeanDirection(cfgDV.language) , c.kCircleSymbol, 5, &h00c000, &h00c000)
		    
		    layer2.setLineWidth(0)
		    layer2.setUseYAxis2
		    
		    c.yAxis2.setLinearScale(0,360,90,0)
		    
		  end
		  
		  call c.addLegend(55, -5, false, "arialbd.ttf", 9).setBackground(c.kTransparent)
		  
		  ImageView1.Picture=c.makeChartPicture
		  
		  Self.Show
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		buoy_index As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		buoy_ixiy As String
	#tag EndProperty

	#tag Property, Flags = &h0
		CurrentDate As date
	#tag EndProperty

	#tag Property, Flags = &h0
		dirAvail As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		header As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		ShowingWaveBuoy As Boolean = False
	#tag EndProperty


	#tag Constant, Name = kBuoyData, Type = String, Dynamic = True, Default = \"Synth. buoy data", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Synth. buoy data"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Datos boya sint."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Dados boya sint."
	#tag EndConstant

	#tag Constant, Name = kFrequency, Type = String, Dynamic = True, Default = \"Frequency", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Frequency"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Frecuencia"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Frequ\xC3\xAAncia"
	#tag EndConstant

	#tag Constant, Name = kFullDirPlot, Type = String, Dynamic = True, Default = \"Full dir. plot", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Full dir. spectrum"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Espectro direcc."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Espectro direcc."
	#tag EndConstant

	#tag Constant, Name = kMeanDirection, Type = String, Dynamic = True, Default = \"Mean Direction", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Mean Direction"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Direcci\xC3\xB3n General"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Dire\xC3\xA7\xC3\xA3o m\xC3\xA9dia"
	#tag EndConstant

	#tag Constant, Name = kSpectralDensity, Type = String, Dynamic = True, Default = \"Spectral Density", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Spectral Density"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Densidad Espectral"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Densidade Espectral"
	#tag EndConstant

	#tag Constant, Name = kTitleBuoy, Type = String, Dynamic = True, Default = \"Synth. Wave Buoy Spectrum", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Synth. Wave Buoy Spectrum"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Espectro de Oleaje Boya Sint."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Espectro de Oleaje Boya Sint."
	#tag EndConstant

	#tag Constant, Name = kTitleWERA, Type = String, Dynamic = True, Default = \"WERA Wave Spectrum", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"WERA Wave Spectrum"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Espectro de Oleaje WERA"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Espectro de Oleaje WERA"
	#tag EndConstant

	#tag Constant, Name = kWERAData, Type = String, Dynamic = True, Default = \"WERA data", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"WERA data"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Datos WERA"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Dados WERA"
	#tag EndConstant


#tag EndWindowCode

#tag Events FullDirBtn
	#tag Event
		Sub Action()
		  //===== load image file to WebFile =============================
		  
		  dim f as FolderItem
		  dim path as string
		  
		  path=cfgDV.stdFolder + "SeaViewWaveBuoy/" + CStr(CurrentDate.Year) + Str(CurrentDate.Month,"0#") + Str(CurrentDate.Day,"0#") + Str(CurrentDate.Hour,"0#") + Str(CurrentDate.Minute,"0#") + "_" + svwb.specPrefix_pt(buoy_index) + "-wbds.png"
		  f=GetFolderItem(path)
		  
		  if f<>Nil then
		    if f.Exists then
		      Session.StdWebFile=WebFile.Open(f)
		      //==== Setting ForceDownload to true to avoid opening the file in the browser ==========
		      Session.StdWebFile.ForceDownload=false
		      
		      //==== Download the file by opening it ===============
		      ExecuteJavaScript("window.open('"+ Session.StdWebFile.URL + "');") // If using this, the WebFile will be opened (or downloaded if ForceDownload works) in a new window... but pop ups must be allowed.
		      return
		    end if
		  end
		  
		  log.Write(True, "Could not find " + path)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events SwitchDataBtn
	#tag Event
		Sub Action()
		  dim rs as RecordSet
		  
		  if ShowingWaveBuoy then
		    
		    rs=db.GetWaveSpectralDensity(buoy_ixiy,header)
		    
		    if rs=nil then
		    elseif rs.BOF then
		    else
		      dim data0(-1) as Double = array(rs.Field("f05").DoubleValue, rs.Field("f06").DoubleValue, rs.Field("f07").DoubleValue, rs.Field("f08").DoubleValue, rs.Field("f09").DoubleValue, rs.Field("f10").DoubleValue, rs.Field("f11").DoubleValue, rs.Field("f12").DoubleValue, rs.Field("f13").DoubleValue, rs.Field("f14").DoubleValue, rs.Field("f15").DoubleValue, rs.Field("f16").DoubleValue, rs.Field("f17").DoubleValue, rs.Field("f18").DoubleValue, rs.Field("f19").DoubleValue, rs.Field("f20").DoubleValue, rs.Field("f21").DoubleValue, rs.Field("f22").DoubleValue, rs.Field("f23").DoubleValue, rs.Field("f24").DoubleValue, rs.Field("f25").DoubleValue)
		      
		      rs=db.GetWaveMeanDirection(buoy_ixiy,header)
		      if rs=nil then
		        return
		      elseif rs.BOF then
		        DisplaySpectrum( self.Left , self.Top, data0, data0, false)
		      else
		        dim data1(-1) as Double = array(rs.Field("f05").DoubleValue, rs.Field("f06").DoubleValue, rs.Field("f07").DoubleValue, rs.Field("f08").DoubleValue, rs.Field("f09").DoubleValue, rs.Field("f10").DoubleValue, rs.Field("f11").DoubleValue, rs.Field("f12").DoubleValue, rs.Field("f13").DoubleValue, rs.Field("f14").DoubleValue, rs.Field("f15").DoubleValue, rs.Field("f16").DoubleValue, rs.Field("f17").DoubleValue, rs.Field("f18").DoubleValue, rs.Field("f19").DoubleValue, rs.Field("f20").DoubleValue, rs.Field("f21").DoubleValue, rs.Field("f22").DoubleValue, rs.Field("f23").DoubleValue, rs.Field("f24").DoubleValue, rs.Field("f25").DoubleValue)
		        DisplaySpectrum(self.Left , self.Top, data0, data1, true)
		      end
		      me.Caption=kBuoyData(cfgDV.language)
		      self.Title=replace(kTitleWERA(cfgDV.language),"WERA",std.kRadName)
		      ShowingWaveBuoy=false
		    end
		    
		  else
		    
		    if svwb.ReadSpectrum("/home/wera/data/SeaViewWaveBuoy/" + CStr(CurrentDate.Year) + Str(CurrentDate.Month,"0#") + Str(CurrentDate.Day,"0#") + Str(CurrentDate.Hour,"0#") + Str(CurrentDate.Minute,"0#") + "_" + svwb.specPrefix_pt(buoy_index) + ".wbdat") then
		      DisplaySpectrum( self.Left , self.Top , energy, dir, true)
		      me.Caption=replace(kWERAData(cfgDV.language),"WERA",std.kRadName)
		      self.Title=kTitleBuoy(cfgDV.language)
		      ShowingWaveBuoy=true
		    end if
		    
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="buoy_index"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="buoy_ixiy"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="dirAvail"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="header"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Group="ID"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Minimum Size"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Minimum Size"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizable"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ShowingWaveBuoy"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Behavior"
		InitialValue="Untitled"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Type"
		Visible=true
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"1 - Sheet"
			"2 - Palette"
			"3 - Modal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
