#tag WebPage
Begin WebDialog OceanDialog
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   92
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   0
   Left            =   408
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   LockVertical    =   False
   MinHeight       =   0
   MinWidth        =   0
   Resizable       =   False
   Style           =   "0"
   TabOrder        =   0
   Title           =   "Grid (100,100)"
   Top             =   376
   Type            =   2
   VerticalCenter  =   0
   Visible         =   True
   Width           =   122
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebLabel Label2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Latitude coordinate"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1035042815"
      TabOrder        =   1
      Text            =   "Lat"
      Top             =   6
      VerticalCenter  =   0
      Visible         =   True
      Width           =   37
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label3
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Longitude coordinate"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1035042815"
      TabOrder        =   2
      Text            =   "Lon"
      Top             =   19
      VerticalCenter  =   0
      Visible         =   True
      Width           =   37
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel latText
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Latitude coordinate"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1035042815"
      TabOrder        =   3
      Text            =   "-63.45"
      Top             =   6
      VerticalCenter  =   0
      Visible         =   True
      Width           =   65
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lonText
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Longitude coordinate"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1035042815"
      TabOrder        =   4
      Text            =   "78.58"
      Top             =   19
      VerticalCenter  =   0
      Visible         =   True
      Width           =   65
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Lbl2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   5
      Text            =   "Dir."
      Top             =   45
      VerticalCenter  =   0
      Visible         =   True
      Width           =   37
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Lbl1
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   6
      Text            =   "Vel."
      Top             =   32
      VerticalCenter  =   0
      Visible         =   True
      Width           =   37
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Lbl2Text
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   7
      Text            =   "123"
      Top             =   45
      VerticalCenter  =   0
      Visible         =   True
      Width           =   65
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Lbl1Text
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   50
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   8
      Text            =   "1.236"
      Top             =   32
      VerticalCenter  =   0
      Visible         =   True
      Width           =   65
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnTD
      AutoDisable     =   False
      Caption         =   "Use in time series"
      Cursor          =   0
      Enabled         =   True
      Height          =   18
      HelpTag         =   "Use this point and data type for plotting in time series page"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   4
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   9
      Top             =   71
      VerticalCenter  =   0
      Visible         =   True
      Width           =   112
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblQual
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   12
      HelpTag         =   "Quality of data"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1211744255"
      TabOrder        =   11
      Text            =   "Excellent measurement qulity"
      Top             =   58
      VerticalCenter  =   0
      Visible         =   True
      Width           =   108
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub LostFocus()
		  me.Hide
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  me.BtnTD.Caption=kUseInTS(cfgDV.language)
		  me.BtnTD.HelpTag=kUseInTSHelpTag(cfgDV.language)
		  me.latText.HelpTag=kLatHelpTag(cfgDV.language)
		  me.lonText.HelpTag=kLonHelpTag(cfgDV.language)
		  me.Label2.HelpTag=kLatHelpTag(cfgDV.language)
		  me.Label3.HelpTag=kLonHelpTag(cfgDV.language)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Shown()
		  //THIS SHOUDL NOT BE NEEDED AFTER XOJO 2015 RELEASE 1... THIS IS A WORKAROUND FOR BUG 37085
		  
		  Lbl2.Visible=false
		  Lbl1.Visible=false
		  Label2.Visible=false
		  Label3.Visible=false
		  LblQual.Visible=false
		  
		  Lbl2.Visible=true
		  Lbl1.Visible=true
		  Label2.Visible=true
		  Label3.Visible=true
		  LblQual.Visible=true
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function CheckAndDisplay(ByRef rs as RecordSet, X as integer, Y as integer, currentPictureType as integer, IdHeaderCurrentPict as integer, qualityFilter as Integer) As string
		  dim idixiy as string
		  dim lat, lon as Double
		  
		  //approx coords from pixel clicked using the line equation already stablished
		  lon=App.mlon*X+App.blon
		  lat=App.mlat*Y+App.blat
		  
		  'get id_tb_ixiy for point of interest
		  rs=db.GetPtInfoFromApproxCoords(lat-App.halflatstep, lat+App.halflatstep, lon-App.halflonstep, lon+App.halflonstep)
		  
		  if rs=nil then
		    Lbl1Text.Text="-"
		    Lbl2Text.Text="-"
		    Title="Coord query err"
		    BtnTD.Enabled=false
		    BtnTD.Style=BtnSmallFontDisabled
		    LblQual.Text="-"
		  elseif rs.BOF then
		    Lbl1Text.Text="-"
		    Lbl2Text.Text="-"
		    Title=kOutOfGrid(cfgDV.language)
		    BtnTD.Enabled=false
		    BtnTD.Style=BtnSmallFontDisabled
		    LblQual.Text="-"
		  else
		    
		    Title=kGrid(cfgDV.language) + "("+rs.Field("ix").StringValue +","+ rs.Field("iy").StringValue +")"
		    
		    if rs.Field("watt").BooleanValue then
		      
		      Lbl1Text.Text=kLand(cfgDV.language)
		      Lbl2Text.Text=kLand(cfgDV.language)
		      BtnTD.Enabled=false
		      BtnTD.Style=BtnSmallFontDisabled
		      LblQual.Text="-"
		      
		    else
		      
		      lat=rs.Field("lat_grd").DoubleValue
		      lon=rs.Field("lon_grd").DoubleValue
		      BtnTD.Enabled=true
		      BtnTD.Style=BtnSmallFont
		      
		      idixiy = rs.Field("id_tb_ixiy").StringValue
		      
		      rs=db.GetMeasDataFromPtIdHeadID(currentPictureType,IdHeaderCurrentPict,idixiy, qualityFilter)
		      
		      
		      
		      dim var1,var2,auxdbl as double
		      dim qual as integer
		      
		      if rs=nil then
		        Lbl1Text.Text="-"
		        Lbl2Text.Text="-"
		        Title="Data query err"
		        BtnTD.Enabled=false
		        BtnTD.Style=BtnSmallFontDisabled
		        LblQual.Text="-"
		      elseif rs.BOF then
		        Lbl1Text.Text=kNoData(cfgDV.language)
		        Lbl2Text.Text=kNoData(cfgDV.language)
		        LblQual.Text="-"
		      else
		        
		        if currentPictureType=0 then // 2D current is a special type... there are some things to check
		          
		          if rs.FieldCount=2 then 'We got a replacement record from tb_custom_data_cuv
		            //NOTE: Theoretically, rs should contain 5 rows, each row with a val and valType field. Rows should be in such an order that the 1st, 2nd and 3rd rows correspond to velu, velv and kl, so I use these.
		            var1=rs.Field("val").DoubleValue
		            rs.MoveNext
		            var2=rs.Field("val").DoubleValue
		            rs.MoveNext
		            qual=std.DecodeKlCuv2Qual( rs.Field("val").DoubleValue )    //Quality value is encoded
		          else
		            var1=rs.Field("velu").DoubleValue
		            var2=rs.Field("velv").DoubleValue
		            qual=std.DecodeKlCuv2Qual(rs.Field("kl").IntegerValue )   //Quality value is encoded in kl
		          end
		          
		        else
		          qual=rs.Field("qual").IntegerValue
		        end if
		        
		        
		        Select Case currentPictureType
		        case 0
		          
		          //Var1 and Var2 were already filled up before
		          
		          Lbl1Text.Text=Str(Sqrt(var1*var1 + var2*var2),"##.##") + " m/s"
		          auxdbl=450 - (ATan2(var2,var1)*180/3.1416)
		          if auxdbl > 360 then
		            auxdbl=auxdbl-360
		          end
		          Lbl2Text.Text=Str(auxdbl,"###") + " °"
		          
		        case 10
		          
		          Lbl1Text.Text=Str(rs.Field("hwave").DoubleValue,"#.##") + " m"
		          var1=rs.Field("dir").IntegerValue
		          if var1=-999 then
		            Lbl2Text.Text=kNoData(cfgDV.language)
		          else
		            if var1<0 then var1=var1+360
		            Lbl2Text.Text=CStr(var1) + " °"
		          end
		          
		        case 20
		          
		          var1=rs.Field("speed").DoubleValue
		          if var1=-999 then
		            Lbl1Text.Text=kNoData(cfgDV.language)
		          else
		            Lbl1Text.Text=Str(var1,"#.##") + " m/s"
		          end
		          var1=rs.Field("dir").IntegerValue
		          if var1<0 then var1=var1+360
		          if cfgDV.windDirectionFrom then
		            if var1<180 then var1=var1+180 else var1=var1-180
		          end
		          Lbl2Text.Text= CStr(var1) + " °"
		          
		        case else
		          
		          Lbl1Text.Text=Str(rs.Field("vel").DoubleValue,"-#.##") + " m/s"
		          Lbl2Text.Text=Str(rs.Field("acc").DoubleValue,"#.###") + " m/s"
		          
		        end
		        
		        // Display quality value
		        Select Case qual
		        case 0
		          LblQual.Text=cfgDV.labelQual0
		        case 1
		          LblQual.Text=cfgDV.labelQual1
		        case 2
		          LblQual.Text=cfgDV.labelQual2
		        case 3
		          LblQual.Text=cfgDV.labelQual3
		        else
		          LblQual.Text=cfgDV.labelQual4
		        end
		        
		      End
		      
		      
		      
		    end
		    
		    
		  end
		  
		  if lon<0 then
		    lonText.Text=Str(abs(lon),"#.####") + " W"
		  else
		    lonText.Text=Str(abs(lon),"#.####") + " E"
		  end
		  if lat<0 then
		    latText.Text=Str(abs(lat),"#.####") + " S"
		  else
		    latText.Text=Str(abs(lat),"#.####") + " N"
		  end if
		  
		  return idixiy
		End Function
	#tag EndMethod


	#tag Constant, Name = kGrid, Type = String, Dynamic = True, Default = \"Grid", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Grid"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Cuadr."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Grelha"
	#tag EndConstant

	#tag Constant, Name = kLand, Type = String, Dynamic = True, Default = \"Land", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Land"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Tierra"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Terra"
	#tag EndConstant

	#tag Constant, Name = kLatHelpTag, Type = String, Dynamic = True, Default = \"Latitude coordinate", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Latitude coordinate"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Latitud geogr\xC3\xA1fica"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Coordenada de latitude"
	#tag EndConstant

	#tag Constant, Name = kLonHelpTag, Type = String, Dynamic = True, Default = \"Longitude coordinate", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Longitude coordinate"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Longitud geogr\xC3\xA1fica"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Coordenada de longitude"
	#tag EndConstant

	#tag Constant, Name = kNoData, Type = String, Dynamic = True, Default = \"No data", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No data"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Sin datos"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"N\xC3\xA3o dados"
	#tag EndConstant

	#tag Constant, Name = kOutOfGrid, Type = String, Dynamic = True, Default = \"Out of grid", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Out of grid"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Fuera de cuadr\xC3\xADc."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Fora da grelha"
	#tag EndConstant

	#tag Constant, Name = kUseInTS, Type = String, Dynamic = True, Default = \"Use in time series", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Use in time series"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Serie de tiempo"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"S\xC3\xA9rie temporal"
	#tag EndConstant

	#tag Constant, Name = kUseInTSHelpTag, Type = String, Dynamic = True, Default = \"Use this point and data type for plotting in time series page.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Use this point and data type for plotting in time series page."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Utilice este tipo de variable y punto geogr\xC3\xA1fico para hacer una gr\xC3\xA1fica en la p\xC3\xA1gina de series de tiempo."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Utilice este tipo de variable y punto geogr\xC3\xA1fico para hacer una gr\xC3\xA1fica en la p\xC3\xA1gina de series de tiempo."
	#tag EndConstant


#tag EndWindowCode

#tag Events BtnTD
	#tag Event
		Sub Action()
		  dim tempint, i as Integer
		  dim tempdate as date
		  dim tempstr as String
		  dim usingHomePage as Boolean
		  
		  // Update coordinates
		  Main.TimeDataPage.TxtLat.Text=latText.Text
		  Main.TimeDataPage.TxtLon.Text=lonText.Text
		  
		  usingHomePage=Main.HomePage.Enabled
		  
		  
		  // Update with type of data desired
		  If usingHomePage then
		    tempint=Main.HomePage.currentPictureType
		    tempdate=Main.HomePage.currentPictureDate
		  else
		    tempint=Main.OceanDataPage.dataTypeID
		    tempdate=Main.OceanDataPage.DateMapDisplayed
		  end
		  select case tempint
		  case 0
		    tempstr=App.kNameCCVAbs(cfgDV.language)
		  case 10
		    tempstr=App.kNameCSWHVal(cfgDV.language)
		  case 20
		    tempstr=App.kNameWD(cfgDV.language)
		  else
		    if tempint<=cfgDV.nStations then
		      for i=0 to cfgDV.nStations-1
		        if tempint=(i+1) then
		          tempstr=siteInfo.sname(i) + " - " + App.kNameRC(cfgDV.language)
		          exit
		        end
		      next
		    else
		      log.Write(true,"Data type for time series is not recognized!")
		    end
		  end
		  
		  if tempstr="" then
		    // data type not recognized... lets just let the one already selected
		  else
		    for i=0 to Main.TimeDataPage.pmDataType.ListCount-1
		      if Main.TimeDataPage.pmDataType.List(i)=tempstr then
		        Main.TimeDataPage.pmDataType.ListIndex=i
		      end
		    next
		  end
		  
		  Main.PageNavi1.TSClicked
		  
		  // Update calendar of time data page with date from ocean data page or last map from home page
		  Main.TimeDataPage.WebCalendar.ScrollBar1.Value=(tempdate.Year-1980)*12 + tempdate.Month - 1
		  Main.TimeDataPage.WebCalendar.now.Day=tempdate.Day
		  Main.TimeDataPage.WebCalendar.ShowMonth
		  
		  // Update list of available measurements and also select the correct row
		  Main.TimeDataPage.RefreshLstAvailMeas()
		  if usingHomePage then
		    Main.TimeDataPage.LstAvailMeas.ListIndex=Ubound(Main.TimeDataPage.DateAvailData)
		    Main.TimeDataPage.LstAvailMeas.ScrollTo(Ubound(Main.TimeDataPage.DateAvailData))
		  elseif (Ubound(Main.TimeDataPage.DateAvailData) >= Main.OceanDataPage.LstAvailMeas.ListIndex) AND Main.OceanDataPage.LstAvailMeas.ListIndex>=0 then  //Main.OceanDataPage.LstAvailMeas.ListInde>=0 will avoid having an exception when nothing is selected
		    Main.TimeDataPage.LstAvailMeas.ListIndex=Main.OceanDataPage.LstAvailMeas.ListIndex
		    Main.TimeDataPage.LstAvailMeas.ScrollTo(Main.OceanDataPage.LstAvailMeas.ListIndex)
		  else
		    Main.TimeDataPage.LstAvailMeas.ListIndex=Ubound(Main.TimeDataPage.DateAvailData)
		    Main.TimeDataPage.LstAvailMeas.ScrollTo(Ubound(Main.TimeDataPage.DateAvailData))
		  end if
		  Main.TimeDataPage.SelectedMeas=Main.TimeDataPage.DateAvailData(Main.TimeDataPage.LstAvailMeas.ListIndex).SQLDateTime
		  
		  // Use date as end date of time range
		  Main.TimeDataPage.LabelTo.Text=Main.TimeDataPage.SelectedMeas
		  Main.TimeDataPage.BtnNetCDF.Enabled=false
		  Main.TimeDataPage.BtnNetCDF.Style=BtnCommonFontDisabled
		  Main.TimeDataPage.BtnCSV.Enabled=false
		  Main.TimeDataPage.BtnCSV.Style=BtnCommonFontDisabled
		  Main.TimeDataPage.UpdateLblMeasInRange()
		  
		  // Use current quality level to update the quality filter selector too
		  if usingHomePage then
		    select case Main.HomePage.currentPictureType
		    case 0
		      tempint=cfgDV.homeMapTypeCur
		    case 10
		      tempint=cfgDV.homeMapTypeWav
		    case 20
		      tempint=cfgDV.homeMapTypeWin
		    else
		      if Main.HomePage.currentPictureType>0 AND Main.HomePage.currentPictureType<=cfgDV.nStations then
		        tempint=cfgDV.homeMapTypeCur
		      end
		    end
		  else
		    tempint=Main.OceanDataPage.qualLevel
		  end
		  select case tempint
		  case 0
		    tempstr=cfgDV.labelQualAllFilter
		  case 1
		    tempstr=cfgDV.labelQual1Filter
		  case 2
		    tempstr=cfgDV.labelQual2Filter
		  case 3
		    tempstr=cfgDV.labelQual3Filter
		  case 4
		    tempstr=cfgDV.labelQual4Filter
		  case 6
		    tempstr="Gaps filled"
		  end
		  for i=0 to Main.TimeDataPage.QualFilter.ListCount-1
		    if Main.TimeDataPage.QualFilter.List(i)=tempstr then
		      Main.TimeDataPage.QualFilter.ListIndex=i
		    end
		  next
		  
		  me.Parent.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Group="ID"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Minimum Size"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Minimum Size"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizable"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Behavior"
		InitialValue="Untitled"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Type"
		Visible=true
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"1 - Sheet"
			"2 - Palette"
			"3 - Modal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
