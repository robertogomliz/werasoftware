#tag WebPage
Begin WebContainer WSConfig
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   604
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   324
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "StyContainer"
   TabOrder        =   0
   Top             =   116
   VerticalCenter  =   0
   Visible         =   True
   Width           =   700
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle3
      Cursor          =   0
      Enabled         =   True
      Height          =   110
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "931829759"
      TabOrder        =   -1
      Top             =   135
      VerticalCenter  =   0
      Visible         =   True
      Width           =   660
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle Rectangle2
      Cursor          =   0
      Enabled         =   True
      Height          =   210
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "931829759"
      TabOrder        =   -1
      Top             =   267
      VerticalCenter  =   0
      Visible         =   True
      Width           =   660
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LabelAnim1
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   260
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1517381631"
      TabOrder        =   26
      Text            =   "Probe alert settings"
      Top             =   257
      VerticalCenter  =   0
      Visible         =   True
      Width           =   154
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   80
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "931829759"
      TabOrder        =   -1
      Top             =   30
      VerticalCenter  =   0
      Visible         =   True
      Width           =   660
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebPopupMenu probeNumber
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "Probe 1"
      Left            =   35
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   0
      Text            =   ""
      Top             =   288
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnReload
      AutoDisable     =   False
      Caption         =   "Reload from file"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   1
      Top             =   489
      VerticalCenter  =   0
      Visible         =   True
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnSave
      AutoDisable     =   False
      Caption         =   "Save to file"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   560
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   2
      Top             =   489
      VerticalCenter  =   0
      Visible         =   True
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnDeleteProbe
      AutoDisable     =   False
      Caption         =   "Delete probe"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   167
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   3
      Top             =   288
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnAddProbe
      AutoDisable     =   False
      Caption         =   "Add probe"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   302
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   4
      Top             =   288
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LabelAnim
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   268
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1517381631"
      TabOrder        =   25
      Text            =   "General settings"
      Top             =   22
      VerticalCenter  =   0
      Visible         =   True
      Width           =   134
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator Separator1
      Cursor          =   0
      Enabled         =   True
      Height          =   6
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   322
      VerticalCenter  =   0
      Visible         =   True
      Width           =   400
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField recipientsLowRisk
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   160
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   30
      Text            =   ""
      Top             =   528
      Type            =   0
      VerticalCenter  =   0
      Visible         =   False
      Width           =   501
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label1
      Cursor          =   1
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   31
      Text            =   "recipientsLowRisk"
      Top             =   528
      VerticalCenter  =   0
      Visible         =   False
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField recipientsHighRisk
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   160
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   32
      Text            =   ""
      Top             =   559
      Type            =   0
      VerticalCenter  =   0
      Visible         =   False
      Width           =   501
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label2
      Cursor          =   1
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   33
      Text            =   "recipientsHighRisk"
      Top             =   559
      VerticalCenter  =   0
      Visible         =   False
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckBox averageNeighbors
      Caption         =   "averageNeighbors"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   34
      Top             =   79
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   130
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckBox winEnabled
      Caption         =   "winEnabled"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   455
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   35
      Top             =   379
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckBox wavEnabled
      Caption         =   "wavEnabled"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   245
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   36
      Top             =   379
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckBox curEnabled
      Caption         =   "curEnabled"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   37
      Top             =   379
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField curLimHigh
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   140
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   38
      Text            =   ""
      Top             =   438
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   45
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label3
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   39
      Text            =   "curLimHigh"
      Top             =   438
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField curLimLow
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   140
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   40
      Text            =   ""
      Top             =   409
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   45
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label4
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   41
      Text            =   "curLimLow"
      Top             =   409
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label5
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   245
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   42
      Text            =   "wavLimLow"
      Top             =   409
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label6
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   245
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   43
      Text            =   "wavLimHigh"
      Top             =   438
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField wavLimHigh
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   350
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   44
      Text            =   ""
      Top             =   438
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   45
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField wavLimLow
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   350
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   45
      Text            =   ""
      Top             =   409
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   45
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField winLimLow
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   560
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   46
      Text            =   ""
      Top             =   409
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   45
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField winLimHigh
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   560
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   47
      Text            =   ""
      Top             =   438
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   45
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label7
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   455
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   48
      Text            =   "winLimHigh"
      Top             =   438
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label8
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   455
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   49
      Text            =   "winLimLow"
      Top             =   409
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField lon
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   573
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   50
      Text            =   ""
      Top             =   340
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   80
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label9
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   493
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   51
      Text            =   "Longitude"
      Top             =   340
      VerticalCenter  =   0
      Visible         =   True
      Width           =   75
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField lat
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   377
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   52
      Text            =   ""
      Top             =   340
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   80
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label10
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   302
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   53
      Text            =   "Latitude"
      Top             =   340
      VerticalCenter  =   0
      Visible         =   True
      Width           =   70
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField probeName
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   120
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   54
      Text            =   ""
      Top             =   340
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   115
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label11
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   55
      Text            =   "probeName"
      Top             =   340
      VerticalCenter  =   0
      Visible         =   True
      Width           =   85
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label12
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   190
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   56
      Text            =   "m/s"
      Top             =   409
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label13
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   190
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   57
      Text            =   "m/s"
      Top             =   438
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label14
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   610
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   58
      Text            =   "m/s"
      Top             =   409
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label15
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   610
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   59
      Text            =   "m/s"
      Top             =   438
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label16
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   400
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   60
      Text            =   "m"
      Top             =   438
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label17
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   400
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   61
      Text            =   "m"
      Top             =   409
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LabelAnim2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   260
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1517381631"
      TabOrder        =   62
      Text            =   "Global alert settings"
      Top             =   127
      VerticalCenter  =   0
      Visible         =   True
      Width           =   154
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label18
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   610
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   63
      Text            =   "m/s"
      Top             =   210
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField winLimHigh1
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   560
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   64
      Text            =   ""
      Top             =   210
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   45
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label19
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   455
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   65
      Text            =   "winGlobalHigh"
      Top             =   210
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label20
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   400
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   66
      Text            =   "m"
      Top             =   210
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField wavLimHigh1
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   350
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   67
      Text            =   ""
      Top             =   210
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   45
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label21
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   245
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   68
      Text            =   "wavGlobalHigh"
      Top             =   210
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label22
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   190
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   69
      Text            =   "m/s"
      Top             =   210
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField curLimHigh1
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   140
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   70
      Text            =   ""
      Top             =   210
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   45
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label23
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   71
      Text            =   "curGlobalHigh"
      Top             =   210
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label24
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   610
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   72
      Text            =   "m/s"
      Top             =   181
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField winLimLow1
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   560
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   73
      Text            =   ""
      Top             =   181
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   45
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label25
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   455
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   74
      Text            =   "winGlobalLow"
      Top             =   181
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label26
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   400
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   75
      Text            =   "m"
      Top             =   181
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField wavLimLow1
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   350
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   76
      Text            =   ""
      Top             =   181
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   45
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label27
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   245
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   77
      Text            =   "wavGlobalLow"
      Top             =   181
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label28
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   190
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   78
      Text            =   "m/s"
      Top             =   181
      VerticalCenter  =   0
      Visible         =   True
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField curLimLow1
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   140
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   79
      Text            =   ""
      Top             =   181
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   45
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label29
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   80
      Text            =   "curGlobalLow"
      Top             =   181
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckBox winEnabled1
      Caption         =   "useGlobalLimitWin"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   455
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   81
      Top             =   151
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   150
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckBox wavEnabled1
      Caption         =   "useGlobalLimitWav"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   245
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   82
      Top             =   151
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   150
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckBox curEnabled1
      Caption         =   "useGlobalLimitCur"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   83
      Top             =   151
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   150
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel label30
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   245
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   84
      Text            =   "wavUseQualLevel"
      Top             =   47
      VerticalCenter  =   0
      Visible         =   True
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField wavUseQualified
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   374
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   85
      Text            =   ""
      Top             =   48
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField winUseQualified
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   584
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   86
      Text            =   ""
      Top             =   48
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label31
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   455
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   87
      Text            =   "winUseQualLevel"
      Top             =   47
      VerticalCenter  =   0
      Visible         =   True
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField curUseQualified
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   164
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   88
      Text            =   ""
      Top             =   48
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label32
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   89
      Text            =   "curUseQualLevel"
      Top             =   47
      VerticalCenter  =   0
      Visible         =   True
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Shown()
		  if App.OceanWarningSystemEnabled then
		    if ReloadFromFile() then
		    end
		  end
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function ReloadFromFile() As Boolean
		  
		  Dim i as integer
		  
		  if wc.InitializeConfig(App.execDir + "../OceanWarning/OceanWarningConfig.cfg") then
		    
		    probeNumber.DeleteAllRows
		    for i=0 to wc.nProbes-1
		      probeNumber.AddRow("Probe " + CStr(i+1))
		    next
		    
		    curUseQualified.Text=CStr(wc.curUseQualLevel)
		    wavUseQualified.Text=CStr(wc.wavUseQualLevel)
		    winUseQualified.Text=CStr(wc.winUseQualLevel)
		    
		    recipientsLowRisk.Text=wc.recipientsLowRisk
		    recipientsHighRisk.Text=wc.recipientsHighRisk
		    
		    averageNeighbors.Value=wc.averageNeighbors
		    
		    curEnabled1.Value=wc.useGlobalLimitCur
		    wavEnabled1.Value=wc.useGlobalLimitWav
		    winEnabled1.Value=wc.useGlobalLimitWin
		    
		    curLimLow1.Text=Cstr(wc.curGlobalLow)
		    curLimHigh1.Text=Cstr(wc.curGlobalHigh)
		    wavLimLow1.Text=Cstr(wc.wavGlobalLow)
		    wavLimHigh1.Text=Cstr(wc.wavGlobalHigh)
		    winLimLow1.Text=Cstr(wc.winGlobalLow)
		    winLimHigh1.Text=Cstr(wc.winGlobalHigh)
		    
		    probeNumber.ListIndex=0
		    
		  else
		    log.Write(true,"Could not load Ocean warning system configuration")
		  end
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		selectedProbe As Integer
	#tag EndProperty


#tag EndWindowCode

#tag Events probeNumber
	#tag Event
		Sub SelectionChanged()
		  
		  if me.ListIndex<>-1 then
		    selectedProbe=me.ListIndex
		    
		    probeName.Text=wc.probeName_p(selectedProbe)
		    lat.Text=CStr(wc.lat_p(selectedProbe))
		    lon.Text=CStr(wc.lon_p(selectedProbe))
		    
		    curEnabled.Value=wc.curEnabled_p(selectedProbe)
		    curLimLow.Text=CStr(wc.curLimLow_p(selectedProbe))
		    curLimHigh.Text=CStr(wc.curLimHigh_p(selectedProbe))
		    
		    wavEnabled.Value=wc.wavEnabled_p(selectedProbe)
		    wavLimLow.Text=CStr(wc.wavLimLow_p(selectedProbe))
		    wavLimHigh.Text=CStr(wc.wavLimHigh_p(selectedProbe))
		    
		    winEnabled.Value=wc.winEnabled_p(selectedProbe)
		    winLimLow.Text=CStr(wc.winLimLow_p(selectedProbe))
		    winLimHigh.Text=CStr(wc.winLimHigh_p(selectedProbe))
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnReload
	#tag Event
		Sub Action()
		  if ReloadFromFile then
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnSave
	#tag Event
		Sub Action()
		  
		  if wc.SaveConfig(App.execDir + "../OceanWarning/OceanWarningConfig.cfg") then
		    log.Write(true,"Configuration saved succesfully!")
		  else
		    log.Write(true,"Problem while saving ocean warning configuration file!")
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnDeleteProbe
	#tag Event
		Sub Action()
		  
		  if probeNumber.ListCount=1 then
		    MsgBox("Cannot delete, at least 1 probe must exists!")
		  else
		    probeNumber.RemoveRow(selectedProbe)
		    
		    wc.nProbes=wc.nProbes-1
		    
		    wc.probeName_p.Remove(selectedProbe)
		    wc.lat_p.Remove(selectedProbe)
		    wc.lon_p.Remove(selectedProbe)
		    
		    wc.curEnabled_p.Remove(selectedProbe)
		    wc.curLimLow_p.Remove(selectedProbe)
		    wc.curLimHigh_p.Remove(selectedProbe)
		    
		    wc.wavEnabled_p.Remove(selectedProbe)
		    wc.wavLimLow_p.Remove(selectedProbe)
		    wc.wavLimHigh_p.Remove(selectedProbe)
		    
		    wc.winEnabled_p.Remove(selectedProbe)
		    wc.winLimLow_p.Remove(selectedProbe)
		    wc.winLimHigh_p.Remove(selectedProbe)
		    
		    probeNumber.ListIndex=0
		    
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnAddProbe
	#tag Event
		Sub Action()
		  
		  probeNumber.AddRow("Probe " + CStr(probeNumber.ListCount + 1))
		  
		  wc.nProbes=wc.nProbes+1
		  
		  wc.probeName_p.Append("NewProbe")
		  wc.lat_p.Append(0)
		  wc.lon_p.Append(0)
		  
		  wc.curEnabled_p.Append(false)
		  wc.curLimLow_p.Append(3)
		  wc.curLimHigh_p.Append(5)
		  
		  wc.wavEnabled_p.Append(false)
		  wc.wavLimLow_p.Append(3)
		  wc.wavLimHigh_p.Append(5)
		  
		  wc.winEnabled_p.Append(false)
		  wc.winLimLow_p.Append(15)
		  wc.winLimHigh_p.Append(20)
		  
		  probeNumber.ListIndex=probeNumber.ListCount-1
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events recipientsLowRisk
	#tag Event
		Sub LostFocus()
		  wc.recipientsLowRisk=me.Text
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events recipientsHighRisk
	#tag Event
		Sub LostFocus()
		  wc.recipientsHighRisk=me.Text
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events averageNeighbors
	#tag Event
		Sub ValueChanged()
		  wc.averageNeighbors=me.Value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events winEnabled
	#tag Event
		Sub ValueChanged()
		  wc.winEnabled_p(selectedProbe)=me.Value
		  
		  if me.Value then
		    winLimLow.Enabled=true
		    winLimHigh.Enabled=true
		  else
		    winLimLow.Enabled=false
		    winLimHigh.Enabled=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wavEnabled
	#tag Event
		Sub ValueChanged()
		  wc.wavEnabled_p(selectedProbe)=me.Value
		  
		  if me.Value then
		    wavLimLow.Enabled=true
		    wavLimHigh.Enabled=true
		  else
		    wavLimLow.Enabled=false
		    wavLimHigh.Enabled=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events curEnabled
	#tag Event
		Sub ValueChanged()
		  
		  wc.curEnabled_p(selectedProbe)=me.Value
		  
		  if me.Value then
		    curLimLow.Enabled=true
		    curLimHigh.Enabled=true
		  else
		    curLimLow.Enabled=false
		    curLimHigh.Enabled=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events curLimHigh
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.curLimHigh_p(selectedProbe)=auxDbl
		  else
		    me.Text=CStr(wc.curLimHigh_p(selectedProbe))
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It can be that it should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  
		  if not(curEnabled.Value) then
		    me.Enabled=true
		    me.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events curLimLow
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.curLimLow_p(selectedProbe)=auxDbl
		  else
		    me.Text=CStr(wc.curLimLow_p(selectedProbe))
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It can be that it should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  
		  if not(curEnabled.Value) then
		    me.Enabled=true
		    me.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wavLimHigh
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.wavLimHigh_p(selectedProbe)=auxDbl
		  else
		    me.Text=CStr(wc.wavLimHigh_p(selectedProbe))
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It can be that it should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  
		  if not(wavEnabled.Value) then
		    me.Enabled=true
		    me.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wavLimLow
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.wavLimLow_p(selectedProbe)=auxDbl
		  else
		    me.Text=CStr(wc.wavLimLow_p(selectedProbe))
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It can be that it should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  
		  if not(wavEnabled.Value) then
		    me.Enabled=true
		    me.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events winLimLow
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.winLimLow_p(selectedProbe)=auxDbl
		  else
		    me.Text=CStr(wc.winLimLow_p(selectedProbe))
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It can be that it should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  
		  if not(winEnabled.Value) then
		    me.Enabled=true
		    me.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events winLimHigh
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.winLimHigh_p(selectedProbe)=auxDbl
		  else
		    me.Text=CStr(wc.winLimHigh_p(selectedProbe))
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It can be that it should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  
		  if not(winEnabled.Value) then
		    me.Enabled=true
		    me.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lon
	#tag Event
		Sub LostFocus()
		  
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl > App.grdmaxlon then
		      auxDbl = App.grdmaxlon
		    elseif auxDbl < App.grdminlon then
		      auxDbl = App.grdminlon
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.lon_p(selectedProbe)=auxDbl
		  else
		    me.Text=CStr(wc.lon_p(selectedProbe))
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lat
	#tag Event
		Sub LostFocus()
		  
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl<App.grdmaxlat then
		      auxDbl=App.grdmaxlat
		    elseif auxDbl>App.grdminlat then
		      auxDbl=App.grdminlat
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.lat_p(selectedProbe)=auxDbl
		  else
		    me.Text=CStr(wc.lat_p(selectedProbe))
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events probeName
	#tag Event
		Sub LostFocus()
		  wc.probeName_p(selectedProbe)=me.Text
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events winLimHigh1
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.winGlobalHigh=auxDbl
		  else
		    me.Text=CStr(wc.winGlobalHigh)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It can be that it should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  
		  if not(winEnabled1.Value) then
		    me.Enabled=true
		    me.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wavLimHigh1
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.wavGlobalHigh=auxDbl
		  else
		    me.Text=CStr(wc.wavGlobalHigh)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It can be that it should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  
		  if not(wavEnabled1.Value) then
		    me.Enabled=true
		    me.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events curLimHigh1
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.curGlobalHigh=auxDbl
		  else
		    me.Text=CStr(wc.curGlobalHigh)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It can be that it should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  
		  if not(curEnabled1.Value) then
		    me.Enabled=true
		    me.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events winLimLow1
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.winGlobalLow=auxDbl
		  else
		    me.Text=CStr(wc.winGlobalLow)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It can be that it should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  
		  if not(winEnabled1.Value) then
		    me.Enabled=true
		    me.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wavLimLow1
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.wavGlobalLow=auxDbl
		  else
		    me.Text=CStr(wc.wavGlobalLow)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It can be that it should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  
		  if not(wavEnabled1.Value) then
		    me.Enabled=true
		    me.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events curLimLow1
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    
		    me.Text=CStr(auxDbl)
		    wc.curGlobalLow=auxDbl
		  else
		    me.Text=CStr(wc.curGlobalLow)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It can be that it should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  
		  if not(curEnabled1.Value) then
		    me.Enabled=true
		    me.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events winEnabled1
	#tag Event
		Sub ValueChanged()
		  wc.useGlobalLimitWin=me.Value
		  
		  if me.Value then
		    winLimLow1.Enabled=true
		    winLimHigh1.Enabled=true
		  else
		    winLimLow1.Enabled=false
		    winLimHigh1.Enabled=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wavEnabled1
	#tag Event
		Sub ValueChanged()
		  wc.useGlobalLimitWav=me.Value
		  
		  if me.Value then
		    wavLimLow1.Enabled=true
		    wavLimHigh1.Enabled=true
		  else
		    wavLimLow1.Enabled=false
		    wavLimHigh1.Enabled=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events curEnabled1
	#tag Event
		Sub ValueChanged()
		  
		  wc.useGlobalLimitCur=me.Value
		  
		  if me.Value then
		    curLimLow1.Enabled=true
		    curLimHigh1.Enabled=true
		  else
		    curLimLow1.Enabled=false
		    curLimHigh1.Enabled=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wavUseQualified
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    auxDbl=round(auxDbl)
		    me.Text=CStr(auxDbl)
		    wc.wavUseQualLevel=auxDbl
		  else
		    me.Text=CStr(wc.wavUseQualLevel)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events winUseQualified
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    auxDbl=round(auxDbl)
		    me.Text=CStr(auxDbl)
		    wc.winUseQualLevel=auxDbl
		  else
		    me.Text=CStr(wc.winUseQualLevel)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events curUseQualified
	#tag Event
		Sub LostFocus()
		  dim auxDbl as Double
		  
		  if IsNumeric(me.Text) then
		    auxDbl=CDbl(me.Text)
		    
		    if auxDbl < 0 then
		      auxDbl = abs(auxDbl)
		    end
		    auxDbl=round(auxDbl)
		    me.Text=CStr(auxDbl)
		    wc.curUseQualLevel=auxDbl
		  else
		    me.Text=CStr(wc.curUseQualLevel)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="selectedProbe"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
