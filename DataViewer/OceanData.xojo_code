#tag WebPage
Begin WebContainer OceanData
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   823
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "StyContainer"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   999
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle4
      Cursor          =   0
      Enabled         =   True
      Height          =   185
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   790
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "931829759"
      TabOrder        =   -1
      Top             =   619
      VerticalCenter  =   0
      Visible         =   True
      Width           =   191
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblToAnim
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   "#kToTimeHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   850
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   33
      Text            =   "-"
      Top             =   704
      VerticalCenter  =   0
      Visible         =   True
      Width           =   130
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnToAnim
      AutoDisable     =   False
      Caption         =   "#Main.kTo"
      Cursor          =   0
      Enabled         =   True
      Height          =   24
      HelpTag         =   "#kBtnToHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   805
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   36
      Top             =   699
      VerticalCenter  =   0
      Visible         =   True
      Width           =   44
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblFromAnim
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   "#kFromTimeHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   850
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   34
      Text            =   "-"
      Top             =   673
      VerticalCenter  =   0
      Visible         =   True
      Width           =   130
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnFromAnim
      AutoDisable     =   False
      Caption         =   "#Main.kFrom"
      Cursor          =   0
      Enabled         =   True
      Height          =   24
      HelpTag         =   "#kBtnFromHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   805
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   35
      Top             =   668
      VerticalCenter  =   0
      Visible         =   True
      Width           =   44
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblAnimDelay
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   809
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   26
      Text            =   "Delay [ms]"
      Top             =   639
      VerticalCenter  =   0
      Visible         =   True
      Width           =   83
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle Rectangle5
      Cursor          =   0
      Enabled         =   True
      Height          =   785
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "931829759"
      TabOrder        =   -1
      Top             =   21
      VerticalCenter  =   0
      Visible         =   True
      Width           =   752
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView OceanImage
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   717
      HelpTag         =   "Click for point information."
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   36
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1613078527
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   42
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   720
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnSaveAs
      AutoDisable     =   False
      Caption         =   "Save Image as..."
      Cursor          =   0
      Enabled         =   True
      Height          =   24
      HelpTag         =   "#kSaveImageAsHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   458
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   16
      Top             =   770
      VerticalCenter  =   0
      Visible         =   True
      Width           =   151
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebListBox LstAvailMeas
      AlternateRowColor=   &c44444400
      ColumnCount     =   1
      ColumnWidths    =   "*"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   False
      HeaderStyle     =   "0"
      Height          =   185
      HelpTag         =   "#kLstAvailMeasHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "(No date selected)"
      Left            =   790
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   20
      Multiline       =   False
      PrimaryRowColor =   &c44444400
      Scope           =   0
      SelectionStyle  =   "548460543"
      Style           =   "1143713791"
      TabOrder        =   -1
      Top             =   381
      VerticalCenter  =   0
      Visible         =   True
      Width           =   188
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebPopupMenu pmDataType
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   "#kPmDataTypeHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   790
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   19
      Text            =   ""
      Top             =   71
      VerticalCenter  =   0
      Visible         =   True
      Width           =   188
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblDataType
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   790
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   20
      Text            =   "Select data type"
      Top             =   44
      VerticalCenter  =   0
      Visible         =   True
      Width           =   150
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblDate
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   790
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   21
      Text            =   "#Main.kSelectDate"
      Top             =   116
      VerticalCenter  =   0
      Visible         =   True
      Width           =   188
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblTime
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   790
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   22
      Text            =   "#Main.kSelectTime"
      Top             =   357
      VerticalCenter  =   0
      Visible         =   True
      Width           =   188
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin iCalendarEuro WebCalendar
      Cursor          =   0
      Enabled         =   True
      Height          =   195
      HelpTag         =   "Select a day to show available maps from selected data type."
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   790
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   2
      SelectedDay     =   0
      Style           =   "1143713791"
      TabOrder        =   23
      Top             =   143
      VerticalCenter  =   0
      Visible         =   True
      Width           =   190
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LabelAnim
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   809
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1517381631"
      TabOrder        =   24
      Text            =   "Map animation"
      Top             =   613
      VerticalCenter  =   0
      Visible         =   True
      Width           =   150
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnAnim
      AutoDisable     =   False
      Caption         =   "Create Animation"
      Cursor          =   0
      Enabled         =   True
      Height          =   24
      HelpTag         =   "#kCreateAnimHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   805
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   29
      Top             =   736
      VerticalCenter  =   0
      Visible         =   True
      Width           =   158
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnNetCDF
      AutoDisable     =   False
      Caption         =   "Export to NetCDF"
      Cursor          =   0
      Enabled         =   True
      Height          =   24
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   805
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   30
      Top             =   769
      VerticalCenter  =   0
      Visible         =   True
      Width           =   158
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin LoadingDialog LDiag
      Cursor          =   0
      Enabled         =   True
      Height          =   100
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   80
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinHeight       =   0
      MinWidth        =   0
      Resizable       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Title           =   "Untitled"
      Top             =   80
      Type            =   1
      VerticalCenter  =   0
      Visible         =   True
      Width           =   246
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin LoadingAnimDialog LDiagAnim
      Cursor          =   0
      Enabled         =   True
      Height          =   100
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   100
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinHeight       =   0
      MinWidth        =   0
      Resizable       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Title           =   "Untitled"
      Top             =   100
      Type            =   1
      VerticalCenter  =   0
      Visible         =   True
      Width           =   246
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin OceanDialog ODiag
      Cursor          =   0
      Enabled         =   True
      Height          =   91
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   120
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinHeight       =   0
      MinWidth        =   0
      Resizable       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Title           =   "Point Info"
      Top             =   120
      Type            =   2
      VerticalCenter  =   0
      Visible         =   True
      Width           =   122
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblDataType1
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   33
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1517381631"
      TabOrder        =   31
      Text            =   "#kMapDisplay"
      Top             =   14
      VerticalCenter  =   0
      Visible         =   True
      Width           =   168
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnNetCDFSingle
      AutoDisable     =   False
      Caption         =   "Export to NetCDF"
      Cursor          =   0
      Enabled         =   True
      Height          =   24
      HelpTag         =   "#kExportMapToNetCDFHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   624
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   37
      Top             =   770
      VerticalCenter  =   0
      Visible         =   True
      Width           =   132
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TxtAnimDelay
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   "#kDelayHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   899
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   25
      Text            =   "500"
      Top             =   638
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   64
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblMeasFound
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   13
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   836
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1871122431"
      TabOrder        =   38
      Text            =   "#Main.kFoundNMeas"
      Top             =   569
      VerticalCenter  =   0
      Visible         =   True
      Width           =   122
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin SpecDialog SpectrumDialog
      buoy_index      =   0
      buoy_ixiy       =   ""
      Cursor          =   0
      dirAvail        =   False
      Enabled         =   True
      header          =   0
      Height          =   180
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinHeight       =   0
      MinWidth        =   0
      Resizable       =   False
      Scope           =   0
      ShowingWaveBuoy =   False
      Style           =   "0"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Title           =   "Wave Spectrum Plot"
      Top             =   20
      Type            =   2
      VerticalCenter  =   0
      Visible         =   True
      Width           =   360
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebCanvas CanvasMap
      Cursor          =   21
      Enabled         =   True
      Height          =   717
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   36
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   42
      VerticalCenter  =   0
      Visible         =   True
      Width           =   720
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WaveBuoyDialog WaBuDialog
      Cursor          =   0
      Enabled         =   True
      Height          =   92
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinHeight       =   0
      MinWidth        =   0
      Resizable       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Title           =   "Wave Buoy"
      Top             =   20
      Type            =   2
      VerticalCenter  =   0
      Visible         =   True
      Width           =   122
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebPopupMenu QualFilter
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   "#kQualFilterHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   200
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   39
      Text            =   ""
      Top             =   770
      VerticalCenter  =   0
      Visible         =   True
      Width           =   188
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel QualFilterLabel2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   36
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1313357823"
      TabOrder        =   40
      Text            =   "#Main.kQualityFilter"
      Top             =   773
      VerticalCenter  =   0
      Visible         =   True
      Width           =   150
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel QualFilterLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   321
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1279453183"
      TabOrder        =   41
      Text            =   "Displaying all measured data."
      Top             =   21
      VerticalCenter  =   0
      Visible         =   True
      Width           =   412
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  
		  // Set language dynamic strings (localization)
		  
		  LblDate.Text=Main.kSelectDate(cfgDV.language)
		  LblTime.Text=Main.kSelectTime(cfgDV.language)
		  LblMeasFound.Text=replace(Main.kFoundNMeas(cfgDV.language),"<n>","0")
		  BtnFromAnim.Caption=Main.kFrom(cfgDV.language)
		  BtnToAnim.Caption=Main.kTo(cfgDV.language)
		  QualFilterLabel2.Text = Main.kQualityFilter(cfgDV.language)
		  
		  
		  BtnFromAnim.HelpTag = kBtnFromHelpTag(cfgDV.language)
		  BtnToAnim.HelpTag = kBtnToHelpTag(cfgDV.language)
		  BtnAnim.Caption = kCreateAnimation(cfgDV.language)
		  BtnAnim.HelpTag=kCreateAnimHelpTag(cfgDV.language)
		  LblAnimDelay.Text=kDelay(cfgDV.language)
		  TxtAnimDelay.HelpTag=kDelayHelpTag(cfgDV.language)
		  BtnNetCDF.HelpTag=kExportAnimHelpTag(cfgDV.language)
		  BtnNetCDFSingle.HelpTag=kExportMapToNetCDFHelpTag(cfgDV.language)
		  BtnNetCDF.Caption=kExportToNetcdf(cfgDV.language)
		  BtnNetCDFSingle.Caption=kExportToNetcdf(cfgDV.language)
		  LblToAnim.HelpTag=kToTimeHelpTag(cfgDV.language)
		  LblFromAnim.HelpTag=kFromTimeHelpTag(cfgDV.language)
		  LstAvailMeas.HelpTag=kLstAvailMeasHelpTag(cfgDV.language)
		  LabelAnim.Text=kMapAnimation(cfgDV.language)
		  LblDataType1.Text=kMapDisplay(cfgDV.language)
		  pmDataType.HelpTag=kPmDataTypeHelpTag(cfgDV.language)
		  QualFilter.HelpTag=kQualFilterHelpTag(cfgDV.language)
		  BtnSaveAs.HelpTag=kSaveImageAsHelpTag(cfgDV.language)
		  BtnSaveAs.Caption=kSaveMapAs(cfgDV.language)
		End Sub
	#tag EndEvent

	#tag Event
		Sub Shown()
		  dim auxString as String
		  dim rs as RecordSet
		  dim tempdate as date
		  dim i as integer
		  
		  // Get last map and display it
		  dim qualRequired, qualRequired2 as Integer
		  dim qualTextRequired as String
		  select case cfgDV.initialMapType
		  case 0
		    auxString=App.kNameCCV(cfgDV.language)
		    qualRequired=cfgDV.homeMapTypeCur
		  case 10
		    auxString=App.kNameCSWH(cfgDV.language)
		    qualRequired=cfgDV.homeMapTypeWav
		  case 20
		    if cfgDV.enableWS then auxString=App.kNameWSD(cfgDV.language) else auxString=App.kNameWD(cfgDV.language)
		    qualRequired=cfgDV.homeMapTypeWin
		  else
		    if cfgDV.initialMapType<=cfgDV.nStations then
		      for i=1 to cfgDV.nStations
		        if cfgDV.initialMapType=i then
		          auxString=siteInfo.sname(i-1) + " " + lowercase(App.kNameRC(cfgDV.language))
		        end
		      next
		    end
		    qualRequired=cfgDV.homeMapTypeCur
		  end
		  
		  if qualRequired>0 and qualRequired<5 then
		    qualRequired2=2
		  else
		    qualRequired2=1
		  end
		  
		  if auxString="" then
		    log.Write(true,"WARNING, initial map type " + CStr(cfgDV.initialMapType) + " not recognized... trying to use combined current...")
		    cfgDV.initialMapType=0
		    pmDataType.ListIndex=0
		    rs=db.GetLastMeasured(0,qualRequired2)
		  else
		    for i=0 to pmDataType.ListCount-1
		      if pmDataType.List(i)=auxString then
		        pmDataType.ListIndex=i
		        exit
		      end
		    next
		    'pmDataTypeSelectionChanged() 'should be fired anyway... let's comment it.
		    rs=db.GetLastMeasured(cfgDV.initialMapType,qualRequired2)
		  end if
		  
		  if rs=nil then
		    
		  elseif rs.BOF then
		    'log.Write(true,"Could not find last current measurement record")
		  else
		    
		    tempdate=rs.Field("measTimeDB").DateValue
		    WebCalendar.ScrollBar1.Value=(tempdate.Year-1980)*12 + tempdate.Month - 1
		    WebCalendar.now.Day=tempdate.Day
		    WebCalendar.ShowMonth
		    
		    
		    RefreshLstAvailMeas()
		    
		    if qualRequired2=1 then 'No quality required... just take the last one
		      LstAvailMeas.ListIndex=Ubound(DateAvailData)
		      LstAvailMeas.ScrollTo(Ubound(DateAvailData))
		      LstAvailMeasCellClick(DateAvailData.Ubound)
		    else 'Maybe the last in the list is not the last one to be qualified... check!
		      for i=UBound(DateAvailData) DownTo 0
		        if DateAvailData(i).Hour = tempdate.Hour AND DateAvailData(i).Minute = tempdate.Minute then
		          LstAvailMeas.ListIndex=i
		          LstAvailMeas.ScrollTo(i)
		          LstAvailMeasCellClick(i)
		          exit
		        end
		      next
		    end if
		    
		    select case qualRequired
		    case 1
		      qualTextRequired=cfgDV.labelQual1Filter
		    case 2
		      qualTextRequired=cfgDV.labelQual2Filter
		    case 3
		      qualTextRequired=cfgDV.labelQual3Filter
		    case 4
		      qualTextRequired=cfgDV.labelQual4Filter
		    case 6
		      qualTextRequired="Gaps filled"
		    else
		      qualTextRequired=cfgDV.labelQualAllFilter
		    end select
		    
		    for i=0 to QualFilter.ListCount-1
		      if QualFilter.List(i)=qualTextRequired then
		        QualFilter.ListIndex=i
		        exit
		      end if
		    next
		    
		    
		    
		    'SelectedMeas=Str(DateAvailData(Ubound(DateAvailData)).Year) + Str(DateAvailData(Ubound(DateAvailData)).DayOfYear,"00#") + Str(DateAvailData(Ubound(DateAvailData)).Hour,"0#") + Str(DateAvailData(Ubound(DateAvailData)).Minute,"0#")
		    'if cfgDV.useSeconds then
		    'SelectedMeas=SelectedMeas + Str(DateAvailData(Ubound(DateAvailData)).Second,"0#")
		    'end
		    '
		    'if cfgDV.mapsOrganized then
		    'auxString=cfgDV.stdFolder+sFolder+   left(SelectedMeas,4) + "/"    + mid(SelectedMeas, 5, 3) + "/"  + SelectedMeas+Replace(sSufix,tmpSufix,qualiAcronym+tmpSufix)+ cfgDV.graphicsFormat    // SelectedMeas='YYYYDDDHHMM...
		    'else
		    'auxString=cfgDV.stdFolder+sFolder+SelectedMeas+Replace(sSufix,tmpSufix,qualiAcronym+tmpSufix)+ cfgDV.graphicsFormat
		    'end
		    '
		    'if LoadImage(auxString,Ubound(DateAvailData)) then
		    'log.Write(true,"Could not find image "+ SelectedMeas+sSufix)
		    'else
		    'DateMapDisplayed=DateAvailData(Ubound(DateAvailData))
		    'end
		  end
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function CalculateDate1(endDate as date, prevHrs as Integer) As string
		  dim startDate as date
		  
		  startDate=endDate
		  
		  if startDate.Hour >= prevHrs then  // if the substraction is possible withouth rendering a negative or zero number then do it
		    startDate.Hour=startDate.Hour - prevHrs
		  else // substract also a day
		    if startDate.Day=1 then // substract also a month
		      if startDate.Month=1 then  // substract also a year
		        startDate.Year=startDate.Year-1
		        startDate.Month=12
		        startDate.Day=31
		      else
		        startDate.Month=startDate.Month-1
		        if startDate.Month=2 then  // februar...
		          if ( startDate.Year mod 4 )= 0 then //leap year (schaltjahr) (the rule should be more complicated, but we should not worry until 2100
		            startDate.Day=29
		          else
		            startDate.Day=28
		          end
		        elseif startDate.Month=4 or startDate.Month=6 or startDate.Month=9 or startDate.Month=11 then // Months with 30 days
		          startDate.Day=30
		        else
		          startDate.Day=31
		        end
		      end
		    else
		      startDate.Day=startDate.Day-1
		    end
		    startDate.Hour=24-(prevHrs-startDate.Hour)
		  end
		  return(startDate.SQLDateTime)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExportNetCDF()
		  dim allDates(-1) as date   'tmpdate,
		  dim date1 as string
		  dim date2 as string
		  dim idgrid, i, j, k, l, idtbheader(-1), nData, q as Integer
		  Dim rs, rs2 as RecordSet
		  dim cnt, tmpidixiy, tmpidx, tmpx, tmpy as Integer
		  dim ShellObject as new Shell
		  ShellObject.Mode=1 //Asynchronous
		  dim netcdfData(-1,-1,-1,-1) as Single
		  dim CDL, file as FolderItem
		  Dim artifCur2D as Boolean  //initialized as false... this variable is only used to display 2D current measurements whose value is changed after artifact removing.
		  
		  
		  //============== Used previously================
		  '// Substract selected hours to current date==================================================================
		  'tmpdate=new date(DateAvailData(LstAvailMeas.ListIndex))
		  'date2=tmpdate.SQLDateTime
		  'date1= CalculateDate1(tmpdate,animHrs)
		  //=========================================
		  
		  date1=LblFromAnim.Text
		  date2=LblToAnim.Text
		  
		  nData=0
		  cnt=0
		  
		  // Check for measurement headers====================================
		  rs=db.GetHeaderData(dataTypeID,date1,date2,False)
		  
		  if rs.BOF then
		    LDiag.Hide
		    MsgBox(kNoDataFound(cfgDV.language))
		    return
		  else
		    nData=rs.RecordCount
		    
		    idgrid= IdGridAvailData(LstAvailMeas.LastIndex)
		    Redim allDates(nData-1)
		    Redim idtbheader(nData-1)
		    
		    for i=0 to nData-1
		      if rs.Field("id_tb_grid").IntegerValue = idgrid then
		        allDates(i)=rs.Field("measTimeDB").DateValue
		        if dataTypeID=0 then
		          idtbheader(i)=rs.Field("id_tb_cuv_headers").IntegerValue
		        elseif dataTypeID=10 or dataTypeID=20 then
		          idtbheader(i)=rs.Field("id_tb_wuv_headers").IntegerValue
		        else
		          idtbheader(i)=rs.Field("id_tb_crad_header").IntegerValue
		        end
		        cnt=cnt+1
		      end
		      rs.MoveNext
		    next
		    redim allDates(cnt-1)
		    redim idtbheader(cnt-1)
		  end
		  
		  '// If the current grid is not the correct one then the correct grid information must be retrieved================================
		  'if not(idgrid=cfgDV.gridID) then
		  'App.UpdateGridData(idgrid)
		  'end
		  
		  if cancelLoading then
		    LDiag.Hide
		    return
		  end
		  
		  // initialize NetCDF data matrix================================================================================================
		  if dataTypeID=10 OR dataTypeID=20 then
		    Redim netcdfData(1, cnt-1, App.grdny-1, App.grdnx-1)
		  else
		    Redim netcdfData(3, cnt-1, App.grdny-1, App.grdnx-1)
		  end
		  for i=0 to UBound(netcdfData,4)
		    for j=0 to UBound(netcdfData,3)
		      for k=0 to UBound(netcdfData,2)
		        for l=0 to UBound(netcdfData,1)
		          netcdfData(l,k,j,i)=-999                // Initiliazing matrix with '_FillValue' (values that are interpreted as missing values)
		        next
		      next
		    next
		  next
		  
		  if cancelLoading then
		    LDiag.Hide
		    return
		  end
		  
		  // Retrieve measurement data and store it to NetCDF data matrix===============================================================
		  for i=0 to cnt-1 // For each header in time
		    
		    rs=db.GetMeasurementDataSpace(idtbheader(i), dataTypeID) // Get all measurements
		    
		    if rs=nil then
		      LDiag.Hide
		      log.Write(true,"Data retrieving interrupted.") // This would occur if there was an error while querying measurements
		      return
		    else
		      nData=rs.RecordCount
		      tmpidx=0
		      
		      for j=0 to nData-1  // For each grid point measurement in space
		        
		        if qualLevel>0 then 'Apply quality control!
		          if dataTypeID=0 then
		            q=std.DecodeKlCuv2Qual(rs.Field("kl").IntegerValue) 'In 2d current measurements, quality is encoded in kl.
		            artifCur2D=false
		          else
		            q=rs.Field("qual").IntegerValue
		          end
		          if q>qualLevel then 'Should be removed
		            if dataTypeID=0 then  // probably there is still data. Lets see
		              rs2=db.GetMeasDataFromPtIdHeadID( 0, idtbheader(i), rs.Field("id_tb_ixiy").StringValue, qualLevel ) //Get artifact removed data using the 1 as last input parameter
		              if rs2.BOF then 'Nope... there is no replacemente data... let us continue then
		                rs.MoveNext
		                continue
		              else// There is data! Then we must activate artifCur2D, since we need to load this special data and not the rawMeasured.
		                artifCur2D=true
		              end
		            else
		              rs.MoveNext
		              continue
		            end
		          end
		        end
		        
		        tmpidixiy=rs.Field("id_tb_ixiy").IntegerValue
		        tmpidx=App.grdid.IndexOf(tmpidixiy ,tmpidx) // Search for the index that contains the point id number
		        tmpx=App.grdx(tmpidx)-1 // Get coordinate indexes and convert them to zero-based index
		        tmpy=App.grdy(tmpidx)-1
		        
		        // assign values to the correct cell in the array
		        Select Case dataTypeID
		        case 0
		          if artifCur2D then // Artifact removing is activated, and the original value is different as the one obtained after artifact removing. We must take the modified value.
		            //NOTE: Theoretically, rs2 should contain 5 rows, each row with a val and valType field. ValType should be 1, 2, 3, 4 and 5 in this order. 1, 2, 4 and 5 correspond to velu, velv, accu and accv, so I use these to replace data from raw measurement. Hopefully in practices this is also true.
		            netcdfData(0, i, tmpy, tmpx)=100*rs2.Field("val").DoubleValue
		            rs2.MoveNext
		            netcdfData(1, i, tmpy, tmpx)=100*rs2.Field("val").DoubleValue
		            rs2.MoveNext
		            rs2.MoveNext
		            netcdfData(2, i, tmpy, tmpx)=100*rs2.Field("val").DoubleValue
		            rs2.MoveNext
		            netcdfData(3, i, tmpy, tmpx)=100*rs2.Field("val").DoubleValue
		          else
		            netcdfData(0, i, tmpy, tmpx)=100*rs.field("velu").DoubleValue
		            netcdfData(1, i, tmpy, tmpx)=100*rs.field("velv").DoubleValue
		            netcdfData(2, i, tmpy, tmpx)=100*rs.field("accu").DoubleValue
		            netcdfData(3, i, tmpy, tmpx)=100*rs.field("accv").DoubleValue
		          end
		        case 10
		          netcdfData(0, i, tmpy, tmpx)=rs.field("hwave").DoubleValue
		          netcdfData(1, i, tmpy, tmpx)=rs.field("dir").IntegerValue
		        case 20
		          netcdfData(0, i, tmpy, tmpx)=rs.field("speed").DoubleValue
		          netcdfData(1, i, tmpy, tmpx)=rs.field("dir").IntegerValue
		        case else
		          netcdfData(0, i, tmpy, tmpx)=rs.field("vel").DoubleValue
		          netcdfData(1, i, tmpy, tmpx)=rs.field("acc").DoubleValue
		          netcdfData(2, i, tmpy, tmpx)=rs.field("var").DoubleValue
		          netcdfData(3, i, tmpy, tmpx)=rs.field("pow").DoubleValue
		        End select
		        
		        rs.MoveNext
		      next
		      
		    end
		    
		    if cancelLoading then
		      LDiag.Hide
		      return
		    end
		    
		  next
		  
		  // Build ASCII file in CDL format================================================================
		  Select Case dataTypeID
		  case 0
		    CDL=Main.CreateCDL(netcdfData(), allDates, 0)
		  case 10
		    CDL=Main.CreateCDL(netcdfData(), allDates, 10)
		  case 20
		    CDL=Main.CreateCDL(netcdfData(), allDates, 20)
		  else
		    if dataTypeID<=cfgDV.nStations then
		      CDL=Main.CreateCDL(netcdfData(), allDates, 1)
		    else
		      log.Write(true,"Data type " + CStr(dataTypeID) + " not recognized when creating CDL file!")
		      LDiag.Hide
		      'animHrs=Round(CDbl(TxtAnimHrs.Text)) //Update the value of animHrs in case that a single data set is exported to NetCDF
		      Return
		    end
		  End Select
		  
		  if cancelLoading then
		    LDiag.Hide
		    return
		  end
		  
		  // Build NetCDF binary file================================================================
		  ShellObject.Execute("ncgen -o " + CDL.AbsolutePath + ".nc " + CDL.AbsolutePath)
		  
		  while(ShellObject.IsRunning)
		    ShellObject.Poll()
		    if cancelLoading then
		      ShellObject.Close
		      LDiag.Hide
		      Return
		    end
		  wend
		  
		  if ShellObject.ErrorCode<>0 then
		    log.Write(true,"Error during creation of NetCDFfile. NetCDF packages may not be installed." + EndOfLine + EndOfLine + ShellObject.Result)
		  else
		    
		    // Download NetCDF file================================================================
		    file = GetFolderItem(CDL.AbsolutePath + ".nc")
		    if file<>Nil then
		      Session.StdWebFile=WebFile.Open(file)
		      Session.StdWebFile.ForceDownload=true
		      
		      ExecuteJavaScript("window.open('"+ Session.StdWebFile.URL + "');")
		    else
		      log.Write(true,"NetCDF file not found!")
		    end
		    
		  end
		  
		  LDiag.Hide
		  'animHrs=Round(CDbl(TxtAnimHrs.Text)) //Update the value of animHrs in case that a single data set is exported to NetCDF
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadAnimationMethod()
		  dim tmpdate as date
		  dim date1 as string
		  dim date2 as string
		  dim idgrid, i, nAvailData as Integer  'prevHrs,
		  Dim rs as RecordSet
		  dim tmpstring as string
		  dim fItems(-1) as FolderItem
		  dim cnt as Integer
		  dim ShellObject as new Shell
		  ShellObject.Mode=1  //asynchronous shell that runs in the background.
		  
		  
		  //============== Used previously================
		  '// Substract selected hours to current date
		  'tmpdate=new date(DateAvailData(LstAvailMeas.ListIndex))
		  'date2=tmpdate.SQLDateTime
		  'prevHrs= CLong(TxtAnimHrs.Text)
		  'date1=CalculateDate1(tmpdate,prevHrs)
		  //=========================================
		  
		  date1=LblFromAnim.Text
		  date2=LblToAnim.Text
		  
		  nAvailData=0
		  cnt=0
		  
		  // Check for measurement headers====================================
		  rs=db.GetHeaderData(dataTypeID,date1,date2,False)
		  
		  //retrieving dates and folder items from query result==========================
		  if rs.BOF then
		    LDiagAnim.Hide
		    MsgBox(kNoDataFound(cfgDV.language))
		    return
		  else
		    nAvailData=rs.RecordCount
		    
		    idgrid=IdGridAvailData(LstAvailMeas.ListIndex)
		    Redim fItems(nAvailData-1)
		    
		    for i=0 to nAvailData-1
		      if rs.Field("id_tb_grid").IntegerValue = idgrid then
		        tmpdate=rs.Field("measTimeDB").DateValue
		        
		        if cfgDV.mapsOrganized then
		          tmpstring=cfgDV.stdFolder + sFolder + Str(tmpdate.Year) + "/" + Str(tmpdate.DayOfYear,"00#") +"/" + Str(tmpdate.Year) + Str(tmpdate.DayOfYear,"00#") + Str(tmpdate.Hour,"0#") + Str(tmpdate.Minute,"0#")
		        else
		          tmpstring=cfgDV.stdFolder + sFolder + Str(tmpdate.Year) + Str(tmpdate.DayOfYear,"00#") + Str(tmpdate.Hour,"0#") + Str(tmpdate.Minute,"0#")
		        end
		        
		        if cfgDV.useSeconds then
		          tmpstring=tmpstring + Str(tmpdate.Second,"0#")
		        end
		        
		        
		        fItems(cnt)=GetFolderItem( tmpstring+Replace(sSufix,tmpSufix,qualiAcronym+tmpSufix)+ cfgDV.graphicsFormat  )
		        if fItems(cnt).Exists then
		          cnt=cnt+1
		        end
		      end
		      rs.MoveNext
		    next
		    
		    if cnt=0 then
		      LDiagAnim.Hide
		      log.Write(true,"No image files were found for animation.")
		      return
		    end
		    redim fItems(cnt-1)
		  end
		  
		  if cancelLoading then
		    LDiagAnim.Hide
		    return
		  end
		  
		  // Forming command for gif animation=====================================
		  tmpstring=CStr(Round(animDelay/10)) 'In convert, delay is given in centisecods not miliseconds, so we divide.
		  tmpstring="convert -delay " + tmpstring + " "
		  for i=0 to UBound(fItems)
		    tmpstring = tmpstring + fItems(i).ShellPath + " "
		  next
		  
		  tmpstring = tmpstring + "-loop 0 " + App.execDir + "animation.gif"
		  
		  // Asking for exclusive edition rights on image file resource (avoid that a parallel session modifies it while being modified here)
		  if App.animFilePass.TryEnter then
		  else
		    if App.SessionFailed then
		      log.Write(false,"It is detected that App.SessionFailed was true! Previous session probably encountered problems. Reseting animFilePass CriticalSection.")
		      App.animFilePass= new CriticalSection  // resets the CriticalSection plotFilePass, in case the previous session terminated with unhandled exception during plotting
		      App.SessionFailed=false
		    end
		    App.animFilePass.Enter
		  end
		  
		  // Creating gif animation=====================================
		  ShellObject.Execute(tmpstring)
		  
		  while(ShellObject.IsRunning)
		    ShellObject.Poll()
		    if cancelLoading then
		      ShellObject.Close
		      App.animFilePass.Leave 'Free resourse for someone else to use
		      LDiagAnim.Hide
		      return
		    end
		  wend
		  
		  // Display animation ==========================================================
		  Dim f as FolderItem
		  f=GetFolderItem(App.execDir + "animation.gif")
		  
		  Session.StdWebFile=WebFile.Open(f)
		  Session.StdWebFile.ForceDownload=false
		  ExecuteJavaScript("window.open('"+ Session.StdWebFile.URL + "');")
		  
		  //Now someone else can use this resource...
		  App.animFilePass.Leave
		  
		  LDiagAnim.Hide
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function LoadImage(path as String, LstIndex as integer) As Boolean
		  Dim FI as New FolderItem
		  Dim Pict1 as WebPicture
		  //Dim factor as Double
		  //Dim Pict2 as Picture
		  
		  // Creating a FolderItem object from path string
		  FI=GetFolderItem(path)
		  
		  if FI=Nil then
		    log.Write(true,"Path returns invalid object, check if folder exists!"+EndOfLine+path)
		    return true
		  else
		    if FI.Exists then
		      
		      // Creating a Picture object from the image file (requires a FolderItem object)
		      try
		        Pict1=new WebPicture(FI)
		      catch err As UnsupportedFormatException
		        
		      end
		      
		      if Pict1 = Nil then  // A 'Nil' value means a problem during Picture object creation
		        log.Write(true,"Error loading image in Ocean Maps page! ("+FI.AbsolutePath+"). File is probably corrupted.")
		        return true
		      else
		        // Scale and display image
		        'factor = Min( OceanImage.Width / Pict1.width , OceanImage.Height / Pict1.height )
		        'factor = Min(factor,1) // do not scale if is smaller!
		        'Pict2 = new Picture( Pict1.Width * factor , Pict1.Height * factor , 32)
		        'OceanImage.Picture=Pict2
		        OceanImage.Picture=Pict1
		        
		        if LstIndex>=0 then
		          IdGridCurrentPict=IdGridAvailData(LstIndex)
		          IdHeaderCurrentPict=IdUVAvailData(LstIndex)
		        end
		        return false
		      end
		      
		    else
		      log.Write(true,"Could not find image file!"+EndOfLine+path)
		      return true
		    end
		  end if
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LstAvailMeasCellClick(Row as Integer)
		  
		  if Row=0 AND Left(LstAvailMeas.List(Row),1)="(" then  // the '(' will simply indicate that the string '(No data available)' is there, which means that there is no photo to load
		    // do nothing
		  else
		    
		    SelectedMeas=Str(DateAvailData(Row).Year) + Str(DateAvailData(Row).DayOfYear,"00#") + Str(DateAvailData(Row).Hour,"0#") + Str(DateAvailData(Row).Minute,"0#")
		    if cfgDV.useSeconds then
		      SelectedMeas=SelectedMeas + Str(DateAvailData(Row).Second,"0#")
		    end
		    
		    dim auxString as string
		    if cfgDV.mapsOrganized then
		      auxString=cfgDV.stdFolder+sFolder+   left(SelectedMeas,4) + "/"    + mid(SelectedMeas, 5, 3) + "/"  + SelectedMeas+Replace(sSufix,tmpSufix,qualiAcronym+tmpSufix)+ cfgDV.graphicsFormat    // SelectedMeas='YYYYDDDHHMM...
		    else
		      auxString=cfgDV.stdFolder+sFolder+SelectedMeas+Replace(sSufix,tmpSufix,qualiAcronym+tmpSufix)+ cfgDV.graphicsFormat
		    end
		    
		    if LoadImage(auxString, Row) then
		    else
		      'QualFilter.ListIndex=0
		      DateMapDisplayed=DateAvailData(Row)
		    end
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub pmDataTypeSelectionChanged()
		  
		  Select Case pmDataType.Text
		  case App.kNameCCV(cfgDV.language)
		    sFolder=cfgDV.sFolder_cur_asc
		    sSufix=cfgDV.sSufix_cur_asc
		    ODiag.Lbl1.Text="Vel."
		    ODiag.Lbl2.Text="Dir."
		    ODiag.Lbl1.HelpTag=App.kNameCCVAbs(cfgDV.language)
		    ODiag.Lbl2.HelpTag=App.kNameCCVDir(cfgDV.language)
		    ODiag.Lbl1Text.HelpTag=App.kNameCCVAbs(cfgDV.language)
		    ODiag.Lbl2Text.HelpTag=App.kNameCCVDir(cfgDV.language)
		    dataTypeID=0
		    tmpSufix=".cur_asc"
		  case App.kNameCSWH(cfgDV.language)
		    sFolder=cfgDV.sFolder_wauv_asc
		    sSufix=cfgDV.sSufix_wauv_asc
		    ODiag.Lbl1.Text="Hs"
		    ODiag.Lbl2.Text="Dir."
		    ODiag.Lbl1.HelpTag=App.kNameCSWHVal(cfgDV.language)
		    ODiag.Lbl2.HelpTag=App.kNameCSWHDir(cfgDV.language)
		    ODiag.Lbl1Text.HelpTag=App.kNameCSWHVal(cfgDV.language)
		    ODiag.Lbl2Text.HelpTag=App.kNameCSWHDir(cfgDV.language)
		    dataTypeID=10
		    tmpSufix=".wav_asc"
		  case App.kNameWD(cfgDV.language)
		    sFolder=cfgDV.sFolder_wiuv_asc
		    sSufix=cfgDV.sSufix_wiuv_asc
		    ODiag.Lbl1.Text="Speed"
		    ODiag.Lbl2.Text="Dir."
		    ODiag.Lbl1.HelpTag=App.kNameWS(cfgDV.language)
		    ODiag.Lbl2.HelpTag=App.kNameWD(cfgDV.language)
		    ODiag.Lbl1Text.HelpTag=App.kNameWS(cfgDV.language)
		    ODiag.Lbl2Text.HelpTag=App.kNameWD(cfgDV.language)
		    dataTypeID=20
		    tmpSufix=".win_asc"
		  case App.kNameWSD(cfgDV.language)   ' If cfgDV.enableWS is true then the label is not App.kNameWD, but App.kNameWSD. This case is added for this reason.
		    sFolder=cfgDV.sFolder_wiuv_asc
		    sSufix=cfgDV.sSufix_wiuv_asc
		    ODiag.Lbl1.Text="Speed"
		    ODiag.Lbl2.Text="Dir."
		    dataTypeID=20
		    tmpSufix=".win_asc"
		  else
		    dim i as integer
		    if InStr(0, pmDataType.Text,App.kNameRC(cfgDV.language))>0 then
		      for i=0 to cfgDV.nStations-1
		        if InStr(0,pmDataType.Text,siteInfo.sname(i))>0 then
		          sFolder=cfgDV.sFolder_st(i)
		          sSufix=cfgDV.sSufix_st(i)
		          ODiag.Lbl1.Text="Vel."
		          ODiag.Lbl2.Text=Main.kAcc(cfgDV.language)
		          ODiag.Lbl1.HelpTag=App.kNameRC(cfgDV.language)
		          ODiag.Lbl2.HelpTag=App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameRC(cfgDV.language))
		          ODiag.Lbl1Text.HelpTag=App.kNameRC(cfgDV.language)
		          ODiag.Lbl2Text.HelpTag=App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameRC(cfgDV.language))
		          dataTypeID=1+i
		          tmpSufix=".crad"
		          exit
		        end
		      next
		    else
		      log.Write(true,"Data type selected ("+ pmDataType.text + ") not recognized!")
		    end
		  end select
		  
		  CanvasMap.Refresh
		  
		  RefreshLstAvailMeas()
		  
		  
		  
		  // We need to update now the correct initial quality to show for this type of data... if the initial quality level to show is the same as the currently available then the map must be update, otherwise will be automatically updated with QualFilterSelectionChanged method.
		  dim qualRequired as Integer
		  if dataTypeID=10 then
		    qualRequired=cfgDV.homeMapTypeWav
		  elseif dataTypeID=20 then
		    qualRequired=cfgDV.homeMapTypeWin
		  else
		    qualRequired=cfgDV.homeMapTypeCur
		  end
		  
		  dim QualFilterShouldBeChanged as Boolean
		  dim QualFilterTextRequired as string
		  
		  select case qualRequired
		  case 1
		    if (cfgDV.enableQ1 AND (QualFilter.Text <> cfgDV.labelQual1Filter))   OR      (not(cfgDV.enableQ1) AND QualFilter.Text <> cfgDV.labelQualAllFilter) then
		      QualFilterShouldBeChanged=true
		      QualFilterTextRequired=cfgDV.labelQual1Filter
		    end if
		  case 2
		    if (cfgDV.enableQ2 AND (QualFilter.Text <> cfgDV.labelQual2Filter))   OR      (not(cfgDV.enableQ2) AND QualFilter.Text <> cfgDV.labelQualAllFilter) then
		      QualFilterShouldBeChanged=true
		      QualFilterTextRequired=cfgDV.labelQual2Filter
		    end if
		  case 3
		    if (cfgDV.enableAR AND (QualFilter.Text <> cfgDV.labelQual3Filter))   OR      (not(cfgDV.enableAR) AND QualFilter.Text <> cfgDV.labelQualAllFilter) then
		      QualFilterShouldBeChanged=true
		      QualFilterTextRequired=cfgDV.labelQual3Filter
		    end if
		  case 4
		    if (cfgDV.enableQ4 AND (QualFilter.Text <> cfgDV.labelQual4Filter))   OR      (not(cfgDV.enableQ4) AND QualFilter.Text <> cfgDV.labelQualAllFilter) then
		      QualFilterShouldBeChanged=true
		      QualFilterTextRequired=cfgDV.labelQual4Filter
		    end if
		  case 6
		    if (cfgDV.enableGF AND (QualFilter.Text <> "Gaps filled"))   OR      (not(cfgDV.enableGF) AND QualFilter.Text <> cfgDV.labelQualAllFilter) then
		      QualFilterShouldBeChanged=true
		      QualFilterTextRequired="Gaps filled"
		    end if
		  else
		    if QualFilter.Text<>cfgDV.labelQualAllFilter then
		      QualFilterShouldBeChanged=true
		      QualFilterTextRequired=cfgDV.labelQualAllFilter
		    end if
		  end select
		  
		  if QualFilterShouldBeChanged then
		    
		    dim i as integer
		    for i=0 to QualFilter.ListCount-1
		      if QualFilter.List(i)=QualFilterTextRequired then
		        QualFilter.ListIndex=i
		        exit
		      end if
		      RefreshMap()
		    next
		    
		  else
		    RefreshMap()
		  end
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub QualFilterSelectionChanged()
		  
		  
		  select case QualFilter.Text
		  case cfgDV.labelQual1Filter
		    qualiAcronym="_q1"
		    qualLevel=1
		    QualFilterLabel.Text=kDisplayingQ1(cfgDV.language)
		  case cfgDV.labelQual2Filter
		    qualiAcronym="_q2"
		    qualLevel=2
		    QualFilterLabel.Text=kDisplayingQ2(cfgDV.language)
		  case cfgDV.labelQual3Filter
		    qualiAcronym="_ar"
		    qualLevel=3
		    QualFilterLabel.Text=kDisplayingQ3(cfgDV.language)
		  case cfgDV.labelQual4Filter
		    qualiAcronym="_q4"
		    qualLevel=4
		    QualFilterLabel.Text=kDisplayingQ4(cfgDV.language)
		  case "Gaps filled"
		    qualiAcronym="_gf"
		    qualLevel=6
		    QualFilterLabel.Text="Displaying data with quality control level 3. Gap filling active."
		  else
		    qualiAcronym=""
		    qualLevel=0
		    QualFilterLabel.Text=kDisplayingAllQ(cfgDV.language)
		  end
		  
		  if SelectedMeas="" then return
		  
		  
		  RefreshMap()
		  
		  'dim auxString as string
		  'if cfgDV.mapsOrganized then
		  'auxString=cfgDV.stdFolder+sFolder+   left(SelectedMeas,4) + "/"    + mid(SelectedMeas, 5, 3) + "/"  + SelectedMeas+Replace(sSufix,tmpSufix,qualiAcronym+tmpSufix)+ cfgDV.graphicsFormat   // SelectedMeas='YYYYDDDHHMM...
		  'else
		  'auxString=cfgDV.stdFolder+sFolder+SelectedMeas+Replace(sSufix,tmpSufix,qualiAcronym+tmpSufix)+ cfgDV.graphicsFormat
		  'end
		  '
		  'if LoadImage(auxString,-1) then
		  '
		  'else
		  'CanvasMap.Refresh
		  'end
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RefreshLstAvailMeas()
		  Dim date1, date2 as String
		  Dim i, nAvailData as integer
		  Dim RecordSetAvailMeas as RecordSet
		  
		  LstAvailMeas.DeleteAllRows
		  
		  date1=Str(WebCalendar.now.SQLDate) + " 00:00:00"
		  date2=Str(WebCalendar.now.SQLDate) + " 23:59:59"
		  
		  RecordSetAvailMeas=db.GetHeaderWithData(dataTypeID,date1,date2,False)
		  nAvailData=0
		  
		  if RecordSetAvailMeas=Nil then
		    LstAvailMeas.AddRow(Main.kNoDataAvail(cfgDV.language))
		    return
		  else
		    
		    if RecordSetAvailMeas.BOF then
		      LstAvailMeas.AddRow(Main.kNoDataAvail(cfgDV.language))
		    else
		      nAvailData=RecordSetAvailMeas.RecordCount
		      
		      Redim IdGridAvailData(nAvailData-1)
		      Redim IdUVAvailData(nAvailData-1)
		      Redim DateAvailData(nAvailData-1)
		      Redim QualAvailData(nAvailData-1)
		      
		      for i=0 to nAvailData-1
		        IdGridAvailData(i)=RecordSetAvailMeas.Field("id_tb_grid").IntegerValue
		        if dataTypeID=0 then
		          IdUVAvailData(i)=RecordSetAvailMeas.Field("id_tb_cuv_headers").IntegerValue
		          QualAvailData(i)=RecordSetAvailMeas.Field("dataQual").BooleanValue
		        elseif dataTypeID=10 then
		          IdUVAvailData(i)=RecordSetAvailMeas.Field("id_tb_wuv_headers").IntegerValue
		          QualAvailData(i)=RecordSetAvailMeas.Field("dataQualWa").BooleanValue
		        elseif dataTypeID=20 then
		          IdUVAvailData(i)=RecordSetAvailMeas.Field("id_tb_wuv_headers").IntegerValue
		          QualAvailData(i)=RecordSetAvailMeas.Field("dataQualWi").BooleanValue
		        else
		          IdUVAvailData(i)=RecordSetAvailMeas.Field("id_tb_crad_header").IntegerValue
		          QualAvailData(i)=RecordSetAvailMeas.Field("dataQual").BooleanValue
		        end
		        DateAvailData(i)=RecordSetAvailMeas.Field("measTimeDB").DateValue
		        LstAvailMeas.AddRow(right(DateAvailData(i).SQLDateTime,8))
		        RecordSetAvailMeas.MoveNext
		      next
		    end
		  end
		  
		  LblMeasFound.Text=replace ( Main.kFoundNMeas, "<n>", CStr(nAvailData) )
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RefreshMap()
		  //Check if the previously selected time slot is also present, if so then select it.
		  dim auxInt as Integer
		  dim auxStr as String
		  dim i as integer
		  auxStr=Right(App.convert_gurgeltime_to_dbstring(SelectedMeas),8)
		  auxInt=-1
		  for i=0 to LstAvailMeas.RowCount-1
		    if LstAvailMeas.List(i)=auxStr then
		      auxInt=i
		      exit
		    end
		  next
		  
		  
		  if auxInt=-1 then
		    LstAvailMeas.ListIndex=0
		    LstAvailMeas.ScrollTo(0)
		    LstAvailMeasCellClick(0)
		  else
		    LstAvailMeas.ListIndex=auxInt
		    LstAvailMeas.ScrollTo(auxInt)
		    LstAvailMeasCellClick(auxInt)
		  end
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		animDelay As Integer = 500
	#tag EndProperty

	#tag Property, Flags = &h0
		cancelLoading As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		clickMarkVisible As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		clickMarkX As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		clickMarkY As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		dataTypeID As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		DateAvailData(-1) As date
	#tag EndProperty

	#tag Property, Flags = &h0
		DateMapDisplayed As date
	#tag EndProperty

	#tag Property, Flags = &h0
		IdGridAvailData(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		IdGridCurrentPict As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		IdHeaderCurrentPict As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected IdUVAvailData(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		lat As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		lon As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		MySession As WebSession
	#tag EndProperty

	#tag Property, Flags = &h0
		QualAvailData(-1) As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		qualiAcronym As String
	#tag EndProperty

	#tag Property, Flags = &h0
		qualLevel As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected SelectedMeas As string
	#tag EndProperty

	#tag Property, Flags = &h0
		sFolder As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sSufix As String
	#tag EndProperty

	#tag Property, Flags = &h0
		tmpSufix As string = ".cur_asc"
	#tag EndProperty


	#tag Constant, Name = kAnimationCreationFeatureIsLimitedTo, Type = String, Dynamic = True, Default = \"Animation creation feature is limited to <n>  hours of duration!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Animation creation feature is limited to <n>  hours of duration!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC2\xA1La opci\xC3\xB3n de animaci\xC3\xB3n est\xC3\xA1 limitada a <n> horas de duraci\xC3\xB3n!"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"\xC2\xA1La opci\xC3\xB3n de animaci\xC3\xB3n est\xC3\xA1 limitada a <n> horas de duraci\xC3\xB3n!"
	#tag EndConstant

	#tag Constant, Name = kBtnFromHelpTag, Type = String, Dynamic = True, Default = \"Use currently selected measurement as first frame in animation.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Use currently selected measurement as first frame in animation."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Use la medici\xC3\xB3n seleccionada como primer cuadro en la animaci\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Use la medici\xC3\xB3n seleccionada como primer cuadro en la animaci\xC3\xB3n."
	#tag EndConstant

	#tag Constant, Name = kBtnToHelpTag, Type = String, Dynamic = True, Default = \"Use currently selected measurement as last frame in animation.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Use currently selected measurement as last frame in animation."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Use la medici\xC3\xB3n seleccionada como \xC3\xBAltimo cuadro en la animaci\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Use la medici\xC3\xB3n seleccionada como \xC3\xBAltimo cuadro en la animaci\xC3\xB3n."
	#tag EndConstant

	#tag Constant, Name = kCannotCreateAnimOfMoreThanAMonth, Type = String, Dynamic = True, Default = \"Cannot create animation of more than a month!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cannot create animation of more than a month!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC2\xA1No es posible crear animaci\xC3\xB3n de m\xC3\xA1s de un mes!"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"\xC2\xA1No es posible crear animaci\xC3\xB3n de m\xC3\xA1s de un mes!"
	#tag EndConstant

	#tag Constant, Name = kCannotCreateNetCDFOfMoreThanAMonth, Type = String, Dynamic = True, Default = \"Cannot create NetCDF file of more than a month!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cannot create NetCDF file of more than a month!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC2\xA1No es posible crear archivo NetCDF de m\xC3\xA1s de un mes!"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"\xC2\xA1No es posible crear archivo NetCDF de m\xC3\xA1s de un mes!"
	#tag EndConstant

	#tag Constant, Name = kCreateAnimation, Type = String, Dynamic = True, Default = \"Create animation", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Create animation"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Crea animaci\xC3\xB3n"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Criar anima\xC3\xA7\xC3\xA3o"
	#tag EndConstant

	#tag Constant, Name = kCreateAnimHelpTag, Type = String, Dynamic = True, Default = \"Creates an animation out of available maps in the time range selected.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Creates an animation out of available maps in the time range selected."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Crea una animaci\xC3\xB3n a partir de los mapas disponibles dentro del rango seleccionado."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Crea una animaci\xC3\xB3n a partir de los mapas disponibles dentro del rango seleccionado."
	#tag EndConstant

	#tag Constant, Name = kDelay, Type = String, Dynamic = True, Default = \"Delay [ms]", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Delay [ms]"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Retardo [ms]"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Atraso [ms]"
	#tag EndConstant

	#tag Constant, Name = kDelayHelpTag, Type = String, Dynamic = True, Default = \"Time delay between animation frames.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Time delay between animation frames."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Retraso de tiempo entre cada cuadro de la animaci\xC3\xB3n"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Retraso de tiempo entre cada cuadro de la animaci\xC3\xB3n"
	#tag EndConstant

	#tag Constant, Name = kDisplayingAllQ, Type = String, Dynamic = True, Default = \"Displaying all measured data.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Displaying all measured data."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mostrando todos los datos medidos."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exibindo todos os dados medidos."
	#tag EndConstant

	#tag Constant, Name = kDisplayingQ1, Type = String, Dynamic = True, Default = \"Displaying data with quality level 1.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Displaying data with quality level 1."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mostrando datos medidos con nivel de calidad 1."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exibi\xC3\xA7\xC3\xA3o de dados com n\xC3\xADvel de qualidade 1."
	#tag EndConstant

	#tag Constant, Name = kDisplayingQ2, Type = String, Dynamic = True, Default = \"Displaying data with quality level 2.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Displaying data with quality level 2."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mostrando datos medidos con nivel de calidad 2."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exibi\xC3\xA7\xC3\xA3o de dados com n\xC3\xADvel de qualidade 2."
	#tag EndConstant

	#tag Constant, Name = kDisplayingQ3, Type = String, Dynamic = True, Default = \"Displaying data with quality level 3.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Displaying data with quality level 3."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mostrando datos medidos con nivel de calidad 3."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exibi\xC3\xA7\xC3\xA3o de dados com n\xC3\xADvel de qualidade 3."
	#tag EndConstant

	#tag Constant, Name = kDisplayingQ4, Type = String, Dynamic = True, Default = \"Displaying data with quality level 4.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Displaying data with quality level 4."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mostrando datos medidos con nivel de calidad 4."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exibi\xC3\xA7\xC3\xA3o de dados com n\xC3\xADvel de qualidade 4."
	#tag EndConstant

	#tag Constant, Name = kExportAnimHelpTag, Type = String, Dynamic = True, Default = \"Export data contained in the time range selected as a NetCDF file.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Export data contained in the time range selected as a NetCDF file."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Exporta los datos contenidos dentro del rango seleccionado como archivo NetCDF."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exporta los datos contenidos dentro del rango seleccionado como archivo NetCDF."
	#tag EndConstant

	#tag Constant, Name = kExportMapToNetCDFHelpTag, Type = String, Dynamic = True, Default = \"Exports data in current map as a NetCDF file.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Exports data in current map as a NetCDF file."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Exporta los datos del mapa actual como archivo NetCDF."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exporta los datos del mapa actual como archivo NetCDF."
	#tag EndConstant

	#tag Constant, Name = kExportToNetcdf, Type = String, Dynamic = True, Default = \"Export to NetCDF", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Export to NetCDF"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Exporta a NetCDF"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exportar para NetCDF"
	#tag EndConstant

	#tag Constant, Name = kFromTimeHelpTag, Type = String, Dynamic = True, Default = \"Time stamp of first frame in animation.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Time stamp of first frame in animation."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Tiempo de medici\xC3\xB3n del primer cuadro en la animaci\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Tiempo de medici\xC3\xB3n del primer cuadro en la animaci\xC3\xB3n."
	#tag EndConstant

	#tag Constant, Name = kLstAvailMeasHelpTag, Type = String, Dynamic = True, Default = \"Select time slot to display in map.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select time slot to display in map."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Elija la hora de medici\xC3\xB3n a visualizar en el mapa."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Elija la hora de medici\xC3\xB3n a visualizar en el mapa."
	#tag EndConstant

	#tag Constant, Name = kMapAnimation, Type = String, Dynamic = True, Default = \"Map animation", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Map animation"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Animaci\xC3\xB3n de mapa"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Anima\xC3\xA7\xC3\xA3o mapa"
	#tag EndConstant

	#tag Constant, Name = kMapDisplay, Type = String, Dynamic = True, Default = \"Map display", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Map display"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Visualizaci\xC3\xB3n de mapa"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Visualiza\xC3\xA7\xC3\xA3o do mapa"
	#tag EndConstant

	#tag Constant, Name = kNetcdfAnimationExportFeatureIsLimitedTo, Type = String, Dynamic = True, Default = \"NetCDF animation export feature is limited to <n> hours of duration!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"NetCDF animation export feature is limited to <n> hours of duration!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC2\xA1Opci\xC3\xB3n para exportar animaci\xC3\xB3n a archivo NetCDF est\xC3\xA1 limitado a <n> horas de duraci\xC3\xB3n!"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"\xC2\xA1Opci\xC3\xB3n para exportar animaci\xC3\xB3n a archivo NetCDF est\xC3\xA1 limitado a <n> horas de duraci\xC3\xB3n!"
	#tag EndConstant

	#tag Constant, Name = kNoDataFound, Type = String, Dynamic = True, Default = \"No data found", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No data found"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"No se encontraron mediciones"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"N\xC3\xA3o foram encontrados dados"
	#tag EndConstant

	#tag Constant, Name = kPleaseSelectValidDates, Type = String, Dynamic = True, Default = \"Please select valid dates.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Please select valid dates."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Por favor seleccione tiempos v\xC3\xA1lidos."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Por favor seleccione datas v\xC3\xA1lidas."
	#tag EndConstant

	#tag Constant, Name = kPmDataTypeHelpTag, Type = String, Dynamic = True, Default = \"Select type of data to display in map.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select type of data to display in map."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Elija el tipo de datos a visualizar en el mapa."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Elija el tipo de datos a visualizar en el mapa."
	#tag EndConstant

	#tag Constant, Name = kQualFilterHelpTag, Type = String, Dynamic = True, Default = \"Select quality level threshold to be included in map.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select quality level threshold to be included in map."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Elija el nivel de calidad a incluir en el mapa."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Elija el nivel de calidad a incluir en el mapa."
	#tag EndConstant

	#tag Constant, Name = kSaveImageAsHelpTag, Type = String, Dynamic = True, Default = \"Download current picture (pop-ups may need to be enabled).", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Download current picture (pop-ups may need to be enabled)."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Descarga el mapa actual como imagen (tal vez sea necesario deshabilitar el bloqueo de pop-ups)."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Descarga el mapa actual como imagen (tal vez sea necesario deshabilitar el bloqueo de pop-ups)."
	#tag EndConstant

	#tag Constant, Name = kSaveMapAs, Type = String, Dynamic = True, Default = \"Save map as...", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Save map as..."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Guardar mapa como..."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Salve mapa como..."
	#tag EndConstant

	#tag Constant, Name = kToTimeHelpTag, Type = String, Dynamic = True, Default = \"Time stamp of last frame in animation.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Time stamp of last frame in animation."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Tiempo de medici\xC3\xB3n del \xC3\xBAltimo cuadro en la animaci\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Tiempo de medici\xC3\xB3n del \xC3\xBAltimo cuadro en la animaci\xC3\xB3n."
	#tag EndConstant


#tag EndWindowCode

#tag Events BtnToAnim
	#tag Event
		Sub Action()
		  If LstAvailMeas.ListIndex>=0 then
		    
		    if LstAvailMeas.ListIndex=0 AND Left(LstAvailMeas.List(0),1)="(" then
		    else
		      LblToAnim.Text=DateAvailData(LstAvailMeas.ListIndex).SQLDateTime
		      return
		    end
		  end
		  MsgBox(Main.kNoTimeIsSelected(cfgDV.language))
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnFromAnim
	#tag Event
		Sub Action()
		  
		  If LstAvailMeas.ListIndex>=0 then
		    
		    if LstAvailMeas.ListIndex=0 AND Left(LstAvailMeas.List(0),1)="(" then
		    else
		      LblFromAnim.Text=DateAvailData(LstAvailMeas.ListIndex).SQLDateTime
		      return
		    end
		  end
		  MsgBox(Main.kNoTimeIsSelected(cfgDV.language))
		  
		  
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OceanImage
	#tag Event
		Sub PictureChanged()
		  'dim script as string
		  '
		  'script = "var elt = document.getElementById('" + me.ControlID + "_image');" + EndOfLine + _
		  '"elt.style.left = '0px';" + EndOfLine + _
		  '"elt.style.top = '0px';" + EndOfLine + _
		  '"elt.style.marginTop = '0px';" + EndOfLine + _
		  '"elt.style.marginLeft = '0px';" + EndOfLine + _
		  '"elt.style.width = '" + str(me.Width, "0") + "px';" + EndOfLine + _
		  '"elt.style.height = '" + str(me.Height, "0") + "px';"
		  '
		  'ExecuteJavaScript(script)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnSaveAs
	#tag Event
		Sub Action()
		  //===== load image file to WebFile =============================
		  Session.StdWebFile=OceanImage.Picture  // if the WebFile is initialized like this then ForceDownload does not work
		  
		  'dim f as FolderItem  // Alternative... which then would render a false picture (or possible error) if user pushes the 'Save Image as...' button right after changing pmDataType
		  'f=GetFolderItem(cfgDV.stdFolder+sFolder+SelectedMeas+sSufix)
		  'Session.StdWebFile=WebFile.Open(f)
		  
		  
		  //==== Setting ForceDownload to true to avoid opening the file in the browser ==========
		  Session.StdWebFile.ForceDownload=true
		  
		  
		  //==== Download the file by opening it ===============
		  'ShowURL(Session.StdWebFile.URL)  // If ForceDownload does not work, using this will load only the picture and leave the DataViewer interface
		  
		  ExecuteJavaScript("window.open('"+ Session.StdWebFile.URL + "');") // If using this, the WebFile will be opened (or downloaded if ForceDownload works) in a new window... but pop ups must be allowed.
		  
		  'Main.HTMLViewer1.URL=Session.StdWebFile.URL  //test
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events LstAvailMeas
	#tag Event
		Sub CellClick(Row As Integer, Column As Integer)
		  ODiag.close
		  LstAvailMeasCellClick(Row)
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmDataType
	#tag Event
		Sub SelectionChanged()
		  pmDataTypeSelectionChanged()
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  Dim i as Integer
		  
		  'me.DeleteAllRows //After executing this line, the event quits and the next is not executed. That is why is commented. Be sure the pop up menu is empty when starting the program RG 130702
		  
		  if cfgDV.enableCCV then
		    me.AddRow(App.kNameCCV(cfgDV.language))
		  end
		  
		  for i=0 to cfgDV.nStations-1
		    if cfgDV.enableRC_st(i) then
		      me.AddRow(siteInfo.sname(i) + " - " + App.kNameRC(cfgDV.language))
		    end
		  next
		  
		  if cfgDV.enableCSWH then
		    me.AddRow(App.kNameCSWH(cfgDV.language))
		  end
		  
		  if cfgDV.enableWD then
		    if cfgDV.enableWS then me.AddRow(App.kNameWSD(cfgDV.language)) else me.AddRow(App.kNameWD(cfgDV.language))
		  end
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  'me.ListIndex=0  'We have already set everything up for displaying the latest measured map for cfgDV.initialMapType... if we do this then we trigger a lot of things that we do not want...
		  
		  dim mem as Integer
		  mem =me.ListIndex
		  me.ListIndex=0
		  me.ListIndex=mem
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseUp(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ODiag.close
		  
		  
		  'Using mouseUp instead of MouseDown because MouseDown causes SelectionChanged event to not fire in Internet explorer as in Xojo doc page:
		  '
		  'http://docs.xojo.com/index.php/WebPopupMenu.SelectionChanged
		  '
		  'WebPopupMenu.SelectionChanged
		  'Event
		  'WebPopupMenu.SelectionChanged ( )
		  'The selected item has changed, either by user interaction with the control or via code. Use ListIndex to change the selection via code.
		  '
		  'Notes
		  '
		  'This is the equivalent of the desktop PopupMenu's Change event. It is past tense to reflect the fact that the change has already occurred when the event is called.
		  'On Internet Explorer, this event is not called if you have implemented the MouseDown event. Consider using the MouseUp event instead.
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events WebCalendar
	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  Dim newDate as new Date
		  Dim Month as integer
		  newDate=WebCalendar.now
		  
		  Select Case Item.Text
		  Case "January"
		    Month=1
		  Case "February"
		    Month=2
		  Case "March"
		    Month=3
		  Case "April"
		    Month=4
		  Case "May"
		    Month=5
		  Case "June"
		    Month=6
		  Case "July"
		    Month=7
		  Case "August"
		    Month=8
		  Case "September"
		    Month=9
		  Case "October"
		    Month=10
		  Case "November"
		    Month=11
		  Case "December"
		    Month=12
		  end select
		  
		  WebCalendar.ScrollBar1.Value=(newDate.Year-1980)*12 + Month - 1
		  WebCalendar.ShowMonth
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  Dim menu As New WebMenuItem
		  
		  menu.Append(New WebMenuItem("January"))
		  menu.Append(New WebMenuItem("February"))
		  menu.Append(New WebMenuItem("March"))
		  menu.Append(New WebMenuItem("April"))
		  menu.Append(New WebMenuItem("May"))
		  menu.Append(New WebMenuItem("June"))
		  menu.Append(New WebMenuItem("July"))
		  menu.Append(New WebMenuItem("August"))
		  menu.Append(New WebMenuItem("September"))
		  menu.Append(New WebMenuItem("October"))
		  menu.Append(New WebMenuItem("November"))
		  menu.Append(New WebMenuItem("December"))
		  
		  Me.ContextualMenu = menu
		  
		  if Ubound(DateAvailData)=-1 then
		    //nothing
		  else
		    WebCalendar.now.Day=DateAvailData(Ubound(DateAvailData)).Day
		  end
		  
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ODiag.close
		  
		  if Details.Button=Details.RightMouseButton then
		    me.PresentContextualMenu
		  else
		    'RefreshLstAvailMeas()  //commented because this is now programmed inside the calendar object. Check notes on MouseDown event of iDay() in WebCalendar object.
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnAnim
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ODiag.close
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  dim i as double
		  
		  if LblFromAnim.Text<>"-" AND LblToAnim.Text<>"-" then
		    
		    i=std.CalculateSeconds(LblFromAnim.Text, LblToAnim.Text)/86400   'Converting seconds in days
		    if i<=0 then
		      MsgBox(Main.kStartDateMustBeBeforeEndDate(cfgDV.language))
		    elseif i>31 then
		      MsgBox(kCannotCreateAnimOfMoreThanAMonth(cfgDV.language))
		    elseif cfgDV.animLimitHours < i then
		      MsgBox(replace(kAnimationCreationFeatureIsLimitedTo(cfgDV.language),"<n>", CStr(cfgDV.animLimitHours) ) )
		    else
		      LDiagAnim.Show
		      
		      MySession=Session
		      Main.threadProcessRequired=2 // threadProcessRequired=2 means animation creation process
		      Main.ParallelThread.Run
		    end
		    
		  else
		    MsgBox(kPleaseSelectValidDates(cfgDV.language))
		  end
		  
		  
		  '// Check if a time is selected
		  'if LstAvailMeas.ListIndex=-1 then
		  'MsgBox("No time is selected. Please select a time.")
		  'elseif Left(LstAvailMeas.List(LstAvailMeas.ListIndex),1)="("  then
		  'MsgBox("No time is selected. Please select a time.")
		  'else
		  'LDiagAnim.Show
		  'end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnNetCDF
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ODiag.close
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  dim i as double
		  
		  if LblFromAnim.Text<>"-" AND LblToAnim.Text<>"-" then
		    
		    i=std.CalculateSeconds(LblFromAnim.Text, LblToAnim.Text)/86400   'Converting seconds in days
		    
		    if i<=0 then
		      MsgBox(Main.kStartDateMustBeBeforeEndDate(cfgDV.language))
		    elseif i>31 then
		      MsgBox(kCannotCreateNetCDFOfMoreThanAMonth(cfgDV.language))
		    elseif cfgDV.animLimitHours < i then
		      MsgBox(replace(kNetcdfAnimationExportFeatureIsLimitedTo(cfgDV.language), "<n>", CStr(cfgDV.ncMapsLimitHours) ) )
		    else
		      
		      LDiag.Show
		      
		      MySession=Session
		      Main.threadProcessRequired=3 // threadProcessRequired=3 means NetCDF export process from data from a range of maps (or a single map if the range comprises only one map)
		      Main.ParallelThread.Run
		    end
		    
		  else
		    MsgBox(kPleaseSelectValidDates(cfgDV.language))
		  end
		  
		  
		  '// Check if a time is selected
		  'if LstAvailMeas.ListIndex=-1 then
		  'MsgBox("No time is selected. Please select a time.")
		  'elseif Left(LstAvailMeas.List(LstAvailMeas.ListIndex),1)="("  then
		  'MsgBox("No time is selected. Please select a time.")
		  'else
		  'LDiag.Show
		  'end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ODiag
	#tag Event
		Sub Dismissed()
		  SpectrumDialog.Hide
		  WaBuDialog.Hide
		  
		  clickMarkVisible=false
		  CanvasMap.Refresh
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnNetCDFSingle
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ODiag.close
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  
		  'animHrs=0 // Setting animHrs=0 will give the actual photo as output.
		  
		  if DateMapDisplayed<>Nil then
		    LblFromAnim.Text=DateMapDisplayed.SQLDateTime // Setting LblFromAnim and LblToAnim both with the current photo being displayed will give only the date of the actual photo as output.
		    LblToAnim.Text=DateMapDisplayed.SQLDateTime
		    LDiag.Show
		    
		    MySession=Session
		    Main.threadProcessRequired=3 // threadProcessRequired=3 means NetCDF export process from data from a range of maps (or a single map if the range comprises only one map)
		    Main.ParallelThread.Run
		  end
		  
		  '// Check if a time is selected
		  'if LstAvailMeas.ListIndex=-1 then
		  'MsgBox("No time is selected. Please select a time.")
		  'elseif Left(LstAvailMeas.List(LstAvailMeas.ListIndex),1)="("  then
		  'MsgBox("No time is selected. Please select a time.")
		  'else
		  'LDiag.Show
		  'end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TxtAnimDelay
	#tag Event
		Sub TextChanged()
		  
		  //Check if input value is valid
		  if isnumeric(me.Text) then
		    me.Text=Cstr(Round(CDbl(me.Text)))
		    
		    if CDbl(me.Text)<10 then
		      me.Text=Cstr(animDelay)
		    else
		      if CDbl(me.Text)>1000 then
		        me.Text="1000"
		      end
		      animDelay=Round(CDbl(me.Text))
		    end
		  else
		    me.Text=Cstr(animDelay)
		  end
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ODiag.close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events CanvasMap
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  dim rs as RecordSet
		  dim idixiy as string
		  dim i as Integer
		  Dim ShowSpectrum as Boolean
		  ShowSpectrum=false
		  
		  clickMarkVisible=true
		  clickMarkX=X
		  clickMarkY=Y
		  CanvasMap.Refresh
		  
		  // Display dialog with basic WERA measurement data
		  // NOTE that rs is passed ByRef to use the results further below
		  idixiy=ODiag.CheckAndDisplay(rs,X,Y,dataTypeID,IdHeaderCurrentPict,qualLevel)
		  
		  ODiag.Left=X + me.Parent.Left + me.Left
		  ODiag.Top=Y + me.Parent.Top + me.Top
		  ODiag.Show
		  
		  // Do we need to display spectrum?
		  if dataTypeID=10 AND cfgDV.enableSPEC AND ODiag.BtnTD.Enabled AND rs<>Nil then   //ODiag.BtnTD.Enabled is used as condition to ensure that a valid point was clicked (inside grid, no watt). rs<>Nil is used to be sure that no query error ocurred, and is required before the rs.BOF on the next line.
		    
		    if not(rs.BOF) then  'If rs.BOF then quality of this point is not good enough, so we are problably clicking in an empty spot on the map... we should not show spectrum in this case.
		      
		      rs=db.GetWaveSpectralDensity(idixiy,IdHeaderCurrentPict)
		      
		      if rs=nil then
		      elseif rs.BOF then
		      else
		        dim data0(-1) as Double = array(rs.Field("f05").DoubleValue, rs.Field("f06").DoubleValue, rs.Field("f07").DoubleValue, rs.Field("f08").DoubleValue, rs.Field("f09").DoubleValue, rs.Field("f10").DoubleValue, rs.Field("f11").DoubleValue, rs.Field("f12").DoubleValue, rs.Field("f13").DoubleValue, rs.Field("f14").DoubleValue, rs.Field("f15").DoubleValue, rs.Field("f16").DoubleValue, rs.Field("f17").DoubleValue, rs.Field("f18").DoubleValue, rs.Field("f19").DoubleValue, rs.Field("f20").DoubleValue, rs.Field("f21").DoubleValue, rs.Field("f22").DoubleValue, rs.Field("f23").DoubleValue, rs.Field("f24").DoubleValue, rs.Field("f25").DoubleValue)
		        
		        rs=db.GetWaveMeanDirection(idixiy,IdHeaderCurrentPict)
		        if rs=nil then
		          return
		        elseif rs.BOF then
		          SpectrumDialog.DisplaySpectrum(X + me.Parent.Left + me.Left + ODiag.Width,Y + me.Parent.Top + me.Top, data0, data0, false)
		        else
		          dim data1(-1) as Double = array(rs.Field("f05").DoubleValue, rs.Field("f06").DoubleValue, rs.Field("f07").DoubleValue, rs.Field("f08").DoubleValue, rs.Field("f09").DoubleValue, rs.Field("f10").DoubleValue, rs.Field("f11").DoubleValue, rs.Field("f12").DoubleValue, rs.Field("f13").DoubleValue, rs.Field("f14").DoubleValue, rs.Field("f15").DoubleValue, rs.Field("f16").DoubleValue, rs.Field("f17").DoubleValue, rs.Field("f18").DoubleValue, rs.Field("f19").DoubleValue, rs.Field("f20").DoubleValue, rs.Field("f21").DoubleValue, rs.Field("f22").DoubleValue, rs.Field("f23").DoubleValue, rs.Field("f24").DoubleValue, rs.Field("f25").DoubleValue)
		          SpectrumDialog.DisplaySpectrum(X + me.Parent.Left + me.Left + ODiag.Width,Y + me.Parent.Top + me.Top, data0, data1, true)
		        end
		        ShowSpectrum=true
		        SpectrumDialog.SwitchDataBtn.Visible=false
		        SpectrumDialog.FullDirBtn.Visible=false
		        SpectrumDialog.Title=replace(SpectrumDialog.kTitleWERA(cfgDV.language),"WERA",std.kRadName)
		      end
		      
		    end if
		  end
		  
		  //Do we need to display wave buoy data?
		  if (dataTypeID=10 or dataTypeID=20) and app.SyWaBuSeaViewEnabled then
		    for i=0 to svwb.nPts-1
		      if CStr(svwb.id_ixiys(i))=idixiy then exit
		    next
		    if i<svwb.nPts then
		      rs=GetSVWaBuData(DateMapDisplayed.SQLDateTime,idixiy)
		      if rs=nil then
		      elseif rs.BOF then
		      else
		        dim wQual, invData, dirQual as Boolean
		        dim qual as integer
		        dim var1 as Double
		        qual=rs.Field("qual").IntegerValue
		        qual=svwb.DecodeQualVal(qual, wQual, invData, dirQual) 'Note that in this method, the inputs are defined as ByRef var as vartype, which means, that they would be modified during the method (something like having more that one input from one method...
		        
		        if (dataTypeID=10 AND (qual<=qualLevel OR qualLevel=0) ) OR (dataTypeID=20 AND ( (wQual AND qualLevel>=3) OR qualLevel=0) ) then 'Something has to be shown at least...
		          
		          WaBuDialog.Title=svwb.ptName_pt(i)
		          
		          if dataTypeID=10 then 'Wave data... only Hs or the whole thing?
		            if invData AND ((dirQual AND qualLevel>=3) OR qualLevel=0) then 'The whole thing!
		              WaBuDialog.PrepareForWave
		              WaBuDialog.HsText.Text=rs.Field("wa_height").StringValue + " m"
		              WaBuDialog.T1Text.Text=rs.Field("wa_mean_T").StringValue + " s"
		              WaBuDialog.TpText.Text=rs.Field("wa_peak_T").StringValue + " s"
		              var1=rs.Field("wa_mean_dir").IntegerValue
		              if var1<0 then var1=var1+360
		              WaBuDialog.T1dText.Text= CStr(var1) + " °"
		              var1=rs.Field("wa_peak_dir").IntegerValue
		              if var1<0 then var1=var1+360
		              WaBuDialog.TpdText.Text= CStr(var1) + " °"
		              
		              if svwb.ReadSpectrum(cfgDV.stdFolder + "SeaViewWaveBuoy/" + CStr(DateMapDisplayed.Year) + Str(DateMapDisplayed.Month,"0#") + Str(DateMapDisplayed.Day,"0#") + Str(DateMapDisplayed.Hour,"0#") + Str(DateMapDisplayed.Minute,"0#") + "_" + svwb.specPrefix_pt(i) + ".wbdat") then
		                SpectrumDialog.DisplaySpectrum(X + me.Parent.Left + me.Left + ODiag.Width,Y + me.Parent.Top + me.Top, svwb.energy, svwb.dir, true)
		                SpectrumDialog.buoy_index=i
		                if ShowSpectrum then 'There is WERA data available
		                  SpectrumDialog.SwitchDataBtn.Visible=true
		                  SpectrumDialog.SwitchDataBtn.Caption=replace(SpectrumDialog.kWERAData(cfgDV.language),"WERA",std.kRadName)
		                  SpectrumDialog.buoy_ixiy=idixiy
		                  SpectrumDialog.header=IdHeaderCurrentPict
		                else
		                  SpectrumDialog.SwitchDataBtn.Visible=false
		                  ShowSpectrum=true
		                end
		                SpectrumDialog.FullDirBtn.Visible=true
		                SpectrumDialog.CurrentDate=DateMapDisplayed
		                SpectrumDialog.Title=SpectrumDialog.kTitleBuoy(cfgDV.language)
		                SpectrumDialog.ShowingWaveBuoy=true
		              end if
		              
		            else
		              WaBuDialog.PrepareForOnlyHs
		              WaBuDialog.HsText.Text=rs.Field("wa_height").StringValue + " m"
		            end if
		            
		            
		            if qual=0 then
		              WaBuDialog.qText.Text=cfgDV.labelQual0
		            elseif qual=1 then
		              WaBuDialog.qText.Text=cfgDV.labelQual1
		            elseif qual=2 then
		              WaBuDialog.qText.Text=cfgDV.labelQual2
		            elseif qual=3 then
		              WaBuDialog.qText.Text=cfgDV.labelQual3
		            else
		              WaBuDialog.qText.Text=cfgDV.labelQual4
		            end if
		            
		            
		          else
		            WaBuDialog.PrepareForWind
		            WaBuDialog.HsText.Text=rs.Field("wi_spd").StringValue + " m/s"
		            var1=rs.Field("wi_dir").IntegerValue
		            if var1<0 then var1=var1+360
		            if cfgDV.windDirectionFrom then
		              if var1<180 then var1=var1+180 else var1=var1-180
		            end
		            WaBuDialog.T1Text.Text=CStr(var1) + " °"
		            
		            if wQual then WaBuDialog.qText.Text=cfgDV.labelQual3 else WaBuDialog.qText.Text=cfgDV.labelQual4
		            
		          end if
		          
		          WaBuDialog.Left=X + me.Parent.Left + me.Left
		          WaBuDialog.Top=Y + me.Parent.Top + me.Top + ODiag.Height+20
		          WaBuDialog.Show
		          
		        end
		        
		      end
		    else
		      WaBuDialog.Hide
		    end
		  else
		    WaBuDialog.Hide
		  end
		  
		  //The following will check if either WERA or SWB have a spectrum to show, and then show it.
		  if ShowSpectrum then
		    SpectrumDialog.show
		  else
		    SpectrumDialog.hide
		  end
		End Sub
	#tag EndEvent
	#tag Event
		Sub Paint(g as WebGraphics)
		  g.ClearRect(0,0,me.Width,me.Height) //Delete everything
		  
		  if app.SyWaBuSeaViewEnabled AND (dataTypeID=10 or dataTypeID=20) then
		    dim i as integer
		    for i=0 to svwb.nPts-1
		      g.ForeColor = &cFFFFFF80
		      g.FillRoundRect(   svwb.pixelX(i) -5   ,    svwb.pixelY(i) -5   ,11,11,3)
		      
		      //SWB label background
		      g.FillRoundRect(   svwb.pixelX(i) +8   ,    svwb.pixelY(i) -2   , 7*svwb.ptName_pt(i).Len, 16, 1)
		      
		      g.ForeColor = &c00000000
		      g.PenWidth = 2
		      g.DrawRoundRect(   svwb.pixelX(i) -5   ,    svwb.pixelY(i) -5   ,11,11,3)
		      g.PenWidth = 1
		      g.DrawRoundRect(  svwb.pixelX(i) -2   ,    svwb.pixelY(i) -2    ,5,5,2)
		      
		      //SWB label
		      g.TextFont = "Helvetica"
		      g.TextSize = 12
		      g.DrawString(svwb.ptName_pt(i),svwb.pixelX(i)+10,svwb.pixelY(i)+10)
		    next
		  end
		  
		  if clickMarkVisible then
		    g.ForeColor = &c00000066
		    g.PenWidth = 2
		    g.DrawRoundRect(   clickMarkX-5   ,   clickMarkY-5   ,10,10,3)
		    g.ForeColor = &cFF000066
		    g.PenWidth = 2
		    g.DrawLine(clickMarkX-8, clickMarkY, clickMarkX+8, clickMarkY)
		    g.DrawLine(clickMarkX, clickMarkY-8, clickMarkX, clickMarkY+8)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.HelpTag=Main.kCanvasMapHelpTag(cfgDV.language)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events QualFilter
	#tag Event
		Sub SelectionChanged()
		  QualFilterSelectionChanged
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  'me.ListIndex=0
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  
		  me.AddRow( cfgDV.labelQualAllFilter )
		  
		  if cfgDV.enableQ4 then
		    QualFilter.AddRow( cfgDV.labelQual4Filter )
		  end
		  
		  if cfgDV.enableAR then
		    QualFilter.AddRow( cfgDV.labelQual3Filter )
		  end
		  
		  if cfgDV.enableQ2 then
		    QualFilter.AddRow( cfgDV.labelQual2Filter )
		  end
		  
		  if cfgDV.enableQ1 then
		    QualFilter.AddRow( cfgDV.labelQual1Filter )
		  end
		  
		  if cfgDV.enableGF then
		    QualFilter.AddRow( "Gaps filled" )
		  end
		  
		  
		  
		  
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseUp(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ODiag.close
		  
		  
		  'Using mouseUp instead of MouseDown because MouseDown causes SelectionChanged event to not fire in Internet explorer as in Xojo doc page:
		  '
		  'http://docs.xojo.com/index.php/WebPopupMenu.SelectionChanged
		  '
		  'WebPopupMenu.SelectionChanged
		  'Event
		  'WebPopupMenu.SelectionChanged ( )
		  'The selected item has changed, either by user interaction with the control or via code. Use ListIndex to change the selection via code.
		  '
		  'Notes
		  '
		  'This is the equivalent of the desktop PopupMenu's Change event. It is past tense to reflect the fact that the change has already occurred when the event is called.
		  'On Internet Explorer, this event is not called if you have implemented the MouseDown event. Consider using the MouseUp event instead.
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="animDelay"
		Group="Behavior"
		InitialValue="500"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="cancelLoading"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="clickMarkVisible"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="clickMarkX"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="clickMarkY"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="dataTypeID"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="IdGridCurrentPict"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="IdHeaderCurrentPict"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="lat"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="lon"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="qualiAcronym"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="qualLevel"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="sFolder"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="sSufix"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="tmpSufix"
		Group="Behavior"
		InitialValue=".cur_asc"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
