#tag WebPage
Begin WebContainer TimeData
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   700
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   400
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "StyContainer"
   TabOrder        =   0
   Top             =   202
   VerticalCenter  =   0
   Visible         =   True
   Width           =   800
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle5
      Cursor          =   0
      Enabled         =   True
      Height          =   191
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   476
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "931829759"
      TabOrder        =   -1
      Top             =   138
      VerticalCenter  =   0
      Visible         =   True
      Width           =   296
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label13
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   490
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1517381631"
      TabOrder        =   55
      Text            =   "#kDisplayOptions"
      Top             =   129
      VerticalCenter  =   0
      Visible         =   True
      Width           =   191
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView PlotImage
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   False
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   670
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1613078527
      ProtectImage    =   False
      Scope           =   0
      Style           =   "84463615"
      TabOrder        =   -1
      Top             =   642
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle Rectangle3
      Cursor          =   0
      Enabled         =   True
      Height          =   265
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   29
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "931829759"
      TabOrder        =   -1
      Top             =   363
      VerticalCenter  =   0
      Visible         =   True
      Width           =   743
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle Rectangle2
      Cursor          =   0
      Enabled         =   True
      Height          =   191
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   29
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "931829759"
      TabOrder        =   -1
      Top             =   138
      VerticalCenter  =   0
      Visible         =   True
      Width           =   428
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   29
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "931829759"
      TabOrder        =   -1
      Top             =   37
      VerticalCenter  =   0
      Visible         =   True
      Width           =   743
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebListBox LstAvailMeas
      AlternateRowColor=   &c44444400
      ColumnCount     =   1
      ColumnWidths    =   "*"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   False
      HeaderStyle     =   "0"
      Height          =   175
      HelpTag         =   "Select measurement time"
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "(No date selected)"
      Left            =   276
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   20
      Multiline       =   False
      PrimaryRowColor =   &c44444400
      Scope           =   0
      SelectionStyle  =   "548460543"
      Style           =   "1143713791"
      TabOrder        =   -1
      Top             =   416
      VerticalCenter  =   0
      Visible         =   True
      Width           =   154
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton From
      AutoDisable     =   False
      Caption         =   "#Main.kFrom"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   "Use currently selected measurement as initial value for the time serie"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   472
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   4
      Top             =   500
      VerticalCenter  =   0
      Visible         =   True
      Width           =   58
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton From1
      AutoDisable     =   False
      Caption         =   "#Main.kTo"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   "Use currently selected measurement as last value for the time series"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   472
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   5
      Top             =   534
      VerticalCenter  =   0
      Visible         =   True
      Width           =   58
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LabelFrom
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   "Start of time series"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   540
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   6
      Text            =   "#Main.kNoDateSelected"
      Top             =   500
      VerticalCenter  =   0
      Visible         =   True
      Width           =   180
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LabelTo
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   "End of time series"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   540
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   7
      Text            =   "#Main.kNoDateSelected"
      Top             =   534
      VerticalCenter  =   0
      Visible         =   True
      Width           =   180
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnShowPlot
      AutoDisable     =   False
      Caption         =   "Generate Plot"
      Cursor          =   0
      Enabled         =   True
      Height          =   31
      HelpTag         =   "Generates time series as configured and displays it in a new window (pop-ups may need to be enabled)."
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   29
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "434558975"
      TabOrder        =   8
      Top             =   652
      VerticalCenter  =   0
      Visible         =   True
      Width           =   150
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin iCalendarEuro WebCalendar
      Cursor          =   0
      Enabled         =   True
      Height          =   195
      HelpTag         =   "Select a day to show available measurements from selected data type"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   49
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   2
      SelectedDay     =   0
      Style           =   "1143713791"
      TabOrder        =   9
      Top             =   416
      VerticalCenter  =   0
      Visible         =   True
      Width           =   190
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnNetCDF
      AutoDisable     =   False
      Caption         =   "Export to NetCDF"
      Cursor          =   0
      Enabled         =   False
      Height          =   31
      HelpTag         =   "Exports data in generated plot as a NetCDF file. Plot needs to be generated first to enable this button."
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   205
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "219525119"
      TabOrder        =   10
      Top             =   652
      VerticalCenter  =   0
      Visible         =   True
      Width           =   150
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label1
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   49
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1517381631"
      TabOrder        =   21
      Text            =   "Selecciona variable a graficar"
      Top             =   129
      VerticalCenter  =   0
      Visible         =   True
      Width           =   229
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   49
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   22
      Text            =   "#Main.kSelectDate"
      Top             =   385
      VerticalCenter  =   0
      Visible         =   True
      Width           =   190
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label3
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   276
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   23
      Text            =   "#Main.kSelectTime"
      Top             =   385
      VerticalCenter  =   0
      Visible         =   True
      Width           =   151
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label5
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   49
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1517381631"
      TabOrder        =   25
      Text            =   "#kPointCoordinates"
      Top             =   29
      VerticalCenter  =   0
      Visible         =   True
      Width           =   221
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TxtLat
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   24
      HelpTag         =   "#kTxtLatHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   125
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   26
      Text            =   "--"
      Top             =   58
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TxtLon
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   24
      HelpTag         =   "#kTxtLonHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   311
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   27
      Text            =   "--"
      Top             =   58
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label6
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   47
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1313357823"
      TabOrder        =   29
      Text            =   "#Main.kLat"
      Top             =   61
      VerticalCenter  =   0
      Visible         =   True
      Width           =   74
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label7
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   234
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1313357823"
      TabOrder        =   30
      Text            =   "#Main.kLon"
      Top             =   61
      VerticalCenter  =   0
      Visible         =   True
      Width           =   75
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnCSV
      AutoDisable     =   False
      Caption         =   "Export to CSV"
      Cursor          =   0
      Enabled         =   False
      Height          =   31
      HelpTag         =   "Exports data in generated plot as a comma separated value file. Plot needs to be generated first to enable this button."
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   380
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "219525119"
      TabOrder        =   37
      Top             =   652
      VerticalCenter  =   0
      Visible         =   True
      Width           =   150
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label8
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   19
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   49
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1517381631"
      TabOrder        =   38
      Text            =   "Select time range"
      Top             =   355
      VerticalCenter  =   0
      Visible         =   True
      Width           =   182
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckBox cb2ndplot
      Caption         =   "Add 2nd plot"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   "Include a 2nd plot below for comparison with other data"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   48
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   39
      Top             =   220
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   96
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebPopupMenu pmSymbols
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   "Select display mode"
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "Only points\nOnly lines\nPoints and lines\nStick plot"
      Left            =   590
      ListIndex       =   2
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   40
      Text            =   ""
      Top             =   171
      VerticalCenter  =   0
      Visible         =   True
      Width           =   150
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebPopupMenu pmSymbols2
      Cursor          =   0
      Enabled         =   False
      Height          =   25
      HelpTag         =   "Select display mode for 2nd plot. Only when 'Add 2nd plot' checkbox is enabled."
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "Only points\nOnly lines\nPoints and lines"
      Left            =   590
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   41
      Text            =   ""
      Top             =   218
      VerticalCenter  =   0
      Visible         =   True
      Width           =   150
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebPopupMenu pmDataType2
      Cursor          =   0
      Enabled         =   False
      Height          =   25
      HelpTag         =   "Select data for 2nd plot. Only when 'Add 2nd plot' checkbox is enabled."
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   147
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   42
      Text            =   ""
      Top             =   218
      VerticalCenter  =   0
      Visible         =   True
      Width           =   280
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin LoadNetcdfDialogTime LoadNetcdf
      Cursor          =   0
      Enabled         =   True
      Height          =   100
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinHeight       =   0
      MinWidth        =   0
      Resizable       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Title           =   "Untitled"
      Top             =   718
      Type            =   1
      VerticalCenter  =   0
      Visible         =   True
      Width           =   246
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin LoadPlotDialog LoadPlotDialogTime
      Cursor          =   0
      Enabled         =   True
      Height          =   100
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   60
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MinHeight       =   0
      MinWidth        =   0
      Resizable       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      TabPanelIndex   =   0
      Title           =   "Untitled"
      Top             =   718
      Type            =   1
      VerticalCenter  =   0
      Visible         =   True
      Width           =   246
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TxtIy
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   24
      HelpTag         =   "#kTxtIyHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   690
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   43
      Text            =   "--"
      Top             =   58
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   55
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label9
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   29
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   598
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1313357823"
      TabOrder        =   44
      Text            =   "Grid in y"
      Top             =   56
      VerticalCenter  =   0
      Visible         =   True
      Width           =   88
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TxtIx
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   24
      HelpTag         =   "#kTxtIxHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   529
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   45
      Text            =   "--"
      Top             =   58
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   55
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label10
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   29
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   435
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1313357823"
      TabOrder        =   46
      Text            =   "Grid in x"
      Top             =   56
      VerticalCenter  =   0
      Visible         =   True
      Width           =   89
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckBox cbVertGrid
      Caption         =   "Display vertical grid"
      Cursor          =   1
      Enabled         =   True
      Height          =   60
      HelpTag         =   "Include vertical grid lines on plot"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   670
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   47
      Top             =   257
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   80
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblMeasFound
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   13
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   276
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1871122431"
      TabOrder        =   48
      Text            =   "#Main.kFoundNMeas"
      Top             =   594
      VerticalCenter  =   0
      Visible         =   True
      Width           =   154
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel LblMeasInRange
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   472
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "511391743"
      TabOrder        =   49
      Text            =   "No range selected"
      Top             =   572
      VerticalCenter  =   0
      Visible         =   True
      Width           =   248
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label11
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   52
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   472
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   True
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   50
      Text            =   "Click ""From"" and ""To"" buttons to select the start and end of the desired range to be plotted."
      Top             =   416
      VerticalCenter  =   0
      Visible         =   True
      Width           =   254
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label12
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   49
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   51
      Text            =   "#k1stSinglePlot"
      Top             =   171
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BtnUseSWB
      AutoDisable     =   False
      Caption         =   "#kBtnSWB"
      Cursor          =   0
      Enabled         =   True
      Height          =   20
      HelpTag         =   "#kBtnSWBHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   493
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "536715263"
      TabOrder        =   53
      Top             =   11
      VerticalCenter  =   0
      Visible         =   False
      Width           =   170
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel SWBName
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   "#kSWBNameHelpTag"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   674
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1143713791"
      TabOrder        =   54
      Text            =   "-"
      Top             =   10
      VerticalCenter  =   0
      Visible         =   False
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label14
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   500
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   56
      Text            =   "1st plot type"
      Top             =   171
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label15
      Cursor          =   1
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   500
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   57
      Text            =   "2nd plot type"
      Top             =   218
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckBox cbFixScaleY
      Caption         =   "Set y-axis scale limits to:"
      Cursor          =   1
      Enabled         =   True
      Height          =   40
      HelpTag         =   "Activate to avoid using automatic scaling in y-axis. Works on all plots, except for direction data."
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   496
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   58
      Top             =   253
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   130
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fixScaleLow
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   24
      HelpTag         =   "Lower y-axis scale limit"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   499
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   59
      Text            =   "0"
      Top             =   294
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fixScaleHigh
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   24
      HelpTag         =   "Higher y-axis scale limit"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   566
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   60
      Text            =   "1"
      Top             =   294
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebPopupMenu QualFilter
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   "Select quality level to be included in time series."
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   147
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   61
      Text            =   ""
      Top             =   280
      VerticalCenter  =   0
      Visible         =   True
      Width           =   280
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel Label16
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   57
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "805541887"
      TabOrder        =   62
      Text            =   "#Main.kQualityFilter"
      Top             =   282
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebPopupMenu pmDataType
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   "#kSelectDataToPlot"
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   147
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1581844479"
      TabOrder        =   2
      Text            =   ""
      Top             =   171
      VerticalCenter  =   0
      Visible         =   True
      Width           =   280
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  
		  dataTypeID=0
		  pmDataType.ListIndex=0
		  
		  
		  // Set language dynamic strings (localization)
		  
		  Label2.Text=Main.kSelectDate(cfgDV.language)
		  Label3.Text=Main.kSelectTime(cfgDV.language)
		  LblMeasFound.Text=replace(Main.kFoundNMeas(cfgDV.language),"<n>","0")
		  From.Caption=Main.kFrom(cfgDV.language)
		  From1.Caption=Main.kTo(cfgDV.language)
		  Label16.Text=Main.kQualityFilter(cfgDV.language)
		  LabelFrom.Text=Main.kNoDateSelected(cfgDV.language)
		  LabelTo.Text=Main.kNoDateSelected(cfgDV.language)
		  Label6.Text=Main.kLat(cfgDV.language)
		  Label7.Text=Main.kLon(cfgDV.language)
		  
		  
		  Label14.Text = k1stPlotType(cfgDV.language)
		  Label12.Text = k1stSinglePlot(cfgDV.language)
		  pmSymbols.HelpTag= k1stSinglePlotHelpTag(cfgDV.language)
		  Label15.Text = k2ndPlotType(cfgDV.language)
		  cb2ndplot.Caption = kAdd2ndPlot(cfgDV.language)
		  cb2ndplot.HelpTag = kAdd2ndPlotHelpTag(cfgDV.language)
		  BtnUseSWB.Caption = kBtnSWB(cfgDV.language)
		  BtnUseSWB.HelpTag = kBtnSWBHelpTag(cfgDV.language)
		  Label13.Text = kDisplayOptions(cfgDV.language)
		  cbVertGrid.Caption = kDisplayVerticalGrid(cfgDV.language)
		  cbVertGrid.HelpTag = kDisplayVerticalGridHelpTag(cfgDV.language)
		  LabelTo.HelpTag = kEndTSHelpTag(cfgDV.language)
		  BtnCSV.Caption = kExportToCSV(cfgDV.language)
		  BtnCSV.HelpTag = kExportToCSVHelpTag(cfgDV.language)
		  BtnNetCDF.Caption = kExportToNetCDF(cfgDV.language)
		  BtnNetCDF.HelpTag = kExportToNetCDFHelpTag(cfgDV.language)
		  From.HelpTag = kFromBtnHelpTag(cfgDV.language)
		  BtnShowPlot.Caption = kGeneratePlot(cfgDV.language)
		  BtnShowPlot.HelpTag = kGeneratePlotHelpTag(cfgDV.language)
		  Label10.Text = kGridInX(cfgDV.language)
		  Label9.Text = kGridInY(cfgDV.language)
		  fixScaleLow.HelpTag = kLowerYLimitHelpTag(cfgDV.language)
		  LblMeasInRange.Text = kNoRangeSelected(cfgDV.language)
		  pmDataType2.HelpTag = kPm2ndPlotHelpTag(cfgDV.language)
		  pmSymbols2.HelpTag=kPm2ndPlotModeHelpTag(cfgDV.language)
		  Label5.Text = kPointCoordinates(cfgDV.language)
		  QualFilter.HelpTag = kQualFilterHelpTag(cfgDV.language)
		  Label1.Text = kSelectDataToPlot(cfgDV.language)
		  Label8.Text = kSelectTimeRange(cfgDV.language)
		  cbFixScaleY.Caption = kSetYAxisScaleLimits(cfgDV.language)
		  cbFixScaleY.HelpTag = kSetYAxisScaleLimitsHelpTag(cfgDV.language)
		  LabelFrom.HelpTag = kStartTSHelpTag(cfgDV.language)
		  SWBName.HelpTag = kSWBNameHelpTag(cfgDV.language)
		  Label11.Text = kTimeSeriesNote(cfgDV.language)
		  From1.HelpTag = kToBtnHelpTag(cfgDV.language)
		  TxtIx.HelpTag = kTxtIxHelpTag(cfgDV.language)
		  TxtIy.HelpTag = kTxtIyHelpTag(cfgDV.language)
		  TxtLat.HelpTag = kTxtLatHelpTag(cfgDV.language)
		  TxtLon.HelpTag = kTxtLonHelpTag(cfgDV.language)
		  fixScaleHigh.HelpTag = kUpperYLimitHelpTag(cfgDV.language)
		  
		  
		  
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub Shown()
		  
		  
		  TxtIx.Text=Cstr(Round(App.grdnx/2))
		  TxtIy.Text=Cstr(Round(App.grdny/2))
		  
		  BtnNetCDF.Enabled=false
		  BtnNetCDF.Style=BtnCommonFontDisabled
		  BtnCSV.Enabled=false
		  BtnCSV.Style=BtnCommonFontDisabled
		  fixScaleLow.Enabled=false
		  fixScaleHigh.Enabled=false
		  
		  dim auxString as String
		  dim rs as RecordSet
		  dim tempdate as date
		  dim i as integer
		  
		  // Get last map and display it
		  
		  select case cfgDV.initialMapType
		  case 0
		    auxString=App.kNameCCVAbs(cfgDV.language)
		  case 10
		    auxString=App.kNameCSWHVal(cfgDV.language)
		  case 20
		    auxString=App.kNameWD(cfgDV.language)
		  else
		    if cfgDV.initialMapType<=cfgDV.nStations then
		      for i=1 to cfgDV.nStations
		        if cfgDV.initialMapType=i then
		          auxString=siteInfo.sname(i-1) + " " + lowercase(App.kNameRC(cfgDV.language))
		        end
		      next
		    end
		  end
		  
		  if auxString="" then
		    log.Write(true,"WARNING, initial map type " + CStr(cfgDV.initialMapType) + " not recognized... trying to use combined current...")
		    cfgDV.initialMapType=0
		    pmDataType.ListIndex=0
		    rs=db.GetLastMeasured(0,0)
		  else
		    for i=0 to pmDataType.ListCount-1
		      if pmDataType.List(i)=auxString then
		        pmDataType.ListIndex=i
		      end
		    next
		    'pmDataTypeSelectionChanged() 'should be fired anyway
		    rs=db.GetLastMeasured(cfgDV.initialMapType,0)
		  end if
		  
		  if rs=nil then
		    
		  elseif rs.BOF then
		    'log.Write(true,"Could not find last current measurement record")
		  else
		    
		    tempdate=rs.Field("measTimeDB").DateValue
		    WebCalendar.ScrollBar1.Value=(tempdate.Year-1980)*12 + tempdate.Month - 1
		    WebCalendar.now.Day=tempdate.Day
		    WebCalendar.ShowMonth
		    SelectedMeas=tempdate.SQLDateTime
		    
		    RefreshLstAvailMeas()
		    
		  end
		  
		  if App.SyWaBuSeaViewEnabled AND svwb.nPts>0 then
		    BtnUseSWB.Visible=true
		    SWBName.Visible=true
		  end
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub CheckIfIsBuoy()
		  
		  dim i as integer
		  
		  for i=0 to (svwb.nPts-1)
		    if svwb.gridX(i)=ix AND svwb.gridY(i)=iy then
		      if pmDataType.List(pmDataType.ListCount-1) <> (App.kNameWD(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		        pmDataType.AddRow(App.kNameCSWHVal(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")")
		        pmDataType.AddRow(App.kNameCSWHDir(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")")
		        pmDataType.AddRow(App.kNameWS(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")")
		        pmDataType.AddRow(App.kNameWD(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")")
		      end
		      usingSynthBuoy=i+1
		      SWBName.Text=svwb.ptName_pt(i)
		      return
		    end
		  next
		  
		  if pmDataType.List(pmDataType.ListCount-1) = (App.kNameWD(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		    for i=0 to 3
		      pmDataType.RemoveRow(pmDataType.ListCount-1)
		    next
		    usingSynthBuoy=0
		    SWBName.Text="---"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DataTyp2Int(textStr as String) As Integer
		  // Function used to identify specific data
		  
		  if textStr = App.kNameCCVAbs(cfgDV.language) then
		    return 0
		  elseif textStr = App.kNameCCVDir(cfgDV.language) then
		    return 1
		  elseif textStr = App.kNameCCVE(cfgDV.language) then
		    return 2
		  elseif textStr = (App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVE(cfgDV.language))) then
		    return 3
		  elseif textStr = App.kNameCCVN(cfgDV.language) then
		    return 4
		  elseif textStr = (App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVN(cfgDV.language))) then
		    return 5
		  elseif textStr = App.kNameCSWHVal(cfgDV.language) then
		    return 6
		  elseif textStr = App.kNameCSWHDir(cfgDV.language) then
		    return 7
		  elseif textStr = App.kNameWD(cfgDV.language) then
		    return 8
		  elseif textStr = App.kNameWS(cfgDV.language) then
		    return 9
		  else
		    Dim i as Integer
		    for i=0 to cfgDV.nStations-1
		      if textStr = (siteInfo.sname(i) + " - " + App.kNameRC(cfgDV.language)) then
		        return(11+i*2)
		      elseif textStr = (siteInfo.sname(i) + " - " + App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameRC(cfgDV.language))) then
		        return(12+i*2)
		      end
		    next
		  end
		  
		  if usingSynthBuoy>0 AND right(pmDataType.Text,1+len(App.kNameSynthBuoy(cfgDV.language)))=App.kNameSynthBuoy(cfgDV.language) + ")" then
		    if textStr = (App.kNameCSWHVal(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		      return 21
		    elseif textStr = (App.kNameCSWHDir(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		      return 22
		    elseif textStr = (App.kNameWD(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		      return 23
		    elseif textStr = (App.kNameWS(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")") then
		      return 24
		    end
		  end
		  
		  return 99
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExportCSV()
		  Dim lineString as String
		  Dim file As FolderItem
		  Dim fileStream As TextOutputStream
		  Dim i, cnt, max as Integer
		  Dim sep as String
		  
		  sep=Chr(cfgDV.csvColSeparatorChr)
		  
		  //================ Creating output file ===============
		  
		  file = GetFolderItem(App.execDir + "data" + CStr(App.sessionsIDs.IndexOf(Session.Identifier)) + ".csv")
		  
		  If file <> Nil Then
		    fileStream = TextOutputStream.Create(file)
		    
		    // Writing header
		    lineString="label"+sep+"year"+sep+"month"+sep+"day"+sep+"dayofyear"+sep+"hour"+sep+"minute"+sep+"value"
		    if cb2ndplot.value then
		      lineString=lineString + "1"+sep+"value2"
		    end
		    fileStream.WriteLine(lineString)
		    
		    // Writing values of each time point
		    max=Ubound(X2)
		    cnt=0
		    for i=0 to UBound(Xall)
		      
		      if cnt>max then
		        if cfgDV.csvAddEmptyVals then
		          lineString=Replace(Xall(i),EndOfLine,"-") + sep
		          lineString=lineString + CStr(Xdates(i).Year) + sep
		          lineString=lineString + CStr(Xdates(i).Month) + sep
		          lineString=lineString + CStr(Xdates(i).Day) + sep
		          lineString=lineString + CStr(Xdates(i).DayOfYear) + sep
		          lineString=lineString + CStr(Xdates(i).Hour) + sep
		          lineString=lineString + CStr(Xdates(i).Minute) + sep
		          lineString=lineString + CStr(cfgDV.csvEmptyVal)
		          if cb2ndplot.value then
		            lineString=lineString + sep + CStr(cfgDV.csvEmptyVal)
		          end
		          fileStream.WriteLine(ConvertEncoding(lineString,Encodings.ASCII))
		          continue
		        else
		          exit
		        end
		      end
		      if i=X2(cnt) then
		        
		        lineString=Replace(Xall(i),EndOfLine,"-") + sep
		        lineString=lineString + CStr(Xdates(i).Year) + sep
		        lineString=lineString + CStr(Xdates(i).Month) + sep
		        lineString=lineString + CStr(Xdates(i).Day) + sep
		        lineString=lineString + CStr(Xdates(i).DayOfYear) + sep
		        lineString=lineString + CStr(Xdates(i).Hour) + sep
		        lineString=lineString + CStr(Xdates(i).Minute) + sep
		        lineString=lineString + CStr(Y2(cnt))
		        if cb2ndplot.value then
		          lineString=lineString + sep + CStr(Y2_2(cnt))
		        end
		        fileStream.WriteLine(ConvertEncoding(lineString,Encodings.ASCII))
		        
		        cnt=cnt+1
		      else
		        if cfgDV.csvAddEmptyVals then
		          lineString=Replace(Xall(i),EndOfLine,"-") + sep
		          lineString=lineString + CStr(Xdates(i).Year) + sep
		          lineString=lineString + CStr(Xdates(i).Month) + sep
		          lineString=lineString + CStr(Xdates(i).Day) + sep
		          lineString=lineString + CStr(Xdates(i).DayOfYear) + sep
		          lineString=lineString + CStr(Xdates(i).Hour) + sep
		          lineString=lineString + CStr(Xdates(i).Minute) + sep
		          lineString=lineString + CStr(cfgDV.csvEmptyVal)
		          if cb2ndplot.value then
		            lineString=lineString + sep + CStr(cfgDV.csvEmptyVal)
		          end
		          fileStream.WriteLine(ConvertEncoding(lineString,Encodings.ASCII))
		        end
		      end
		      
		      if cancelLoading then
		        LoadNetcdf.Hide
		        fileStream.Close
		        return
		      end
		      
		    next
		    
		    fileStream.Close
		  else
		    log.Write(true,"Not able to create CSV file!")
		  End If
		  
		  StdWebFile=WebFile.Open(file)
		  StdWebFile.ForceDownload=true
		  
		  ExecuteJavaScript("window.open('"+ StdWebFile.URL + "');")
		  
		  LoadNetcdf.Hide
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExportNetCDF()
		  
		  
		  dim cdfData(-1,-1,-1,-1) as single
		  dim i, nt, cnt as Integer
		  dim f, file as FolderItem
		  dim ShellObject as new Shell
		  ShellObject.Mode=1 //Asynchronous
		  
		  nt=Xdates.Ubound
		  
		  if cb2ndplot.Value then
		    Redim cdfData(1, nt, 0, 0)
		  else
		    Redim cdfData(0, nt, 0, 0)
		  end
		  
		  cnt=0
		  for i=0 to nt
		    if cnt<=X2.Ubound then  // to avoid OutOfBoundsException when the last time instants have missing values
		      if i=X2(cnt) then
		        cdfData(0,i,0,0)=Y2(cnt)
		        if IsCurrentVelocity(pmDataType.Text) then
		          cdfData(0,i,0,0)=100*cdfData(0,i,0,0)  // converting to cm per second
		        end
		        if cb2ndplot.Value then
		          cdfData(1,i,0,0)=Y2_2(cnt)
		          if IsCurrentVelocity(pmDataType2.Text) then
		            cdfData(1,i,0,0)=100*cdfData(1,i,0,0)  // converting to cm per second
		          end
		        end
		        cnt=cnt+1
		        continue
		      end
		    end
		    cdfData(0,i,0,0)=-999
		    if cb2ndplot.Value then
		      cdfData(1,i,0,0)=-999
		    end
		    
		    if cancelLoading then
		      LoadNetcdf.Hide
		      return
		    end
		    
		  next
		  
		  // Build ASCII file in CDL format================================================================
		  f=Main.CreateCDL(cdfData(),Xdates,100)
		  
		  if cancelLoading then
		    LoadNetcdf.Hide
		    return
		  end
		  
		  // Build NetCDF binary file================================================================
		  ShellObject.Execute("ncgen -o " + f.AbsolutePath + ".nc " + f.AbsolutePath)
		  
		  while(ShellObject.IsRunning)
		    ShellObject.Poll()
		    if cancelLoading then
		      ShellObject.Close
		      LoadNetcdf.Hide
		      Return
		    end
		  wend
		  
		  if ShellObject.ErrorCode<>0 then
		    log.Write(true,"Error during creation of NetCDFfile. NetCDF packages may not be installed." + EndOfLine + EndOfLine + ShellObject.Result)
		  else
		    
		    // Download NetCDF file================================================================
		    file = GetFolderItem(f.AbsolutePath + ".nc")
		    if file<>Nil then
		      Session.StdWebFile=WebFile.Open(file)
		      Session.StdWebFile.ForceDownload=true
		      
		      ExecuteJavaScript("window.open('"+ Session.StdWebFile.URL + "');")
		    else
		      log.Write(true,"NetCDF file not found!")
		    end
		    
		  end
		  
		  LoadNetcdf.Hide
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetMonthString(m as Integer) As string
		  Select Case m
		  case 1
		    return("Jan")
		  case 2
		    return("Feb")
		  case 3
		    return("Mar")
		  case 4
		    return("Apr")
		  case 5
		    return("May")
		  case 6
		    return("Jun")
		  case 7
		    return("Jul")
		  case 8
		    return("Ago")
		  case 9
		    return("Sep")
		  case 10
		    return("Oct")
		  case 11
		    return("Nov")
		  case 12
		    return("Dec")
		  End select
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function IsCurrentVelocity(tempstr as string) As Boolean
		  
		  //This function is searching for data types that describe current velocity. This method is usually used when exporting NetCDF files. Data that corresponds to current velocity is by standard expressed in cm per second in a NetCDF file. Since WERA provides this in meter per second we have to identify if it is current velocity data and multiply it by 100.
		  
		  if tempstr=App.kNameCCVDir(cfgDV.language) OR tempstr=App.kNameCSWHVal(cfgDV.language) OR tempstr=App.kNameCSWHDir(cfgDV.language) OR tempstr=App.kNameWD(cfgDV.language) OR tempstr=App.kNameWS OR Right(tempstr,13)="(synth. buoy)"  then
		    return false
		  else
		    return true
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadPlotMethod()
		  
		  Dim date1, date2 as String
		  Dim i as integer
		  Dim RecordSetAvailMeas as RecordSet
		  Dim nAvailData2 as integer
		  Dim IdGridAvailData2(-1) as Integer
		  Dim IdUVAvailData2(-1) as Integer
		  Dim idixiy as string
		  Dim auxInt as Integer
		  auxInt=0
		  
		  Redim Xdates(-1)
		  Redim Xall(-1)
		  Redim Yall(-1)
		  Redim X2(-1)
		  Redim Y2(-1)
		  Redim Y2_2(-1)
		  
		  date1=LabelFrom.Text
		  date2=LabelTo.Text
		  
		  minutesInBetween=std.CalculateSeconds(date1,date2)/60
		  'hoursInBetween=std.CalculateHours(date1,date2)
		  
		  if usingSynthBuoy>0 AND right(pmDataType.Text,1+len(App.kNameSynthBuoy(cfgDV.language)))=App.kNameSynthBuoy(cfgDV.language) + ")" then
		    RecordSetAvailMeas=db.GetSVWaBuDataRange(date1,date2,CStr(svwb.id_ixiys(usingSynthBuoy-1)))
		  else
		    RecordSetAvailMeas=db.GetHeaderData(dataTypeID,date1,date2,False)
		  end if
		  
		  nAvailData2=0
		  
		  if RecordSetAvailMeas.BOF then
		    LoadPlotDialogTime.Hide
		    MsgBox(Main.kNoDataAvail)
		    return
		  else
		    nAvailData2=RecordSetAvailMeas.RecordCount
		    
		    Redim IdGridAvailData2(nAvailData2-1)
		    Redim IdUVAvailData2(nAvailData2-1)
		    Redim Xdates(nAvailData2-1)
		    Redim Xall(nAvailData2-1)
		    Redim Yall(nAvailData2-1)
		    
		    for i=0 to nAvailData2-1
		      
		      if cancelLoading then
		        LoadPlotDialogTime.Hide
		        return
		      end
		      
		      if usingSynthBuoy>0 AND right(pmDataType.Text,1+len(App.kNameSynthBuoy(cfgDV.language)))=App.kNameSynthBuoy(cfgDV.language) + ")" then
		        IdGridAvailData2(i)=svwb.gridUsed
		        IdUVAvailData2(i)=RecordSetAvailMeas.Field("id_tb_svwabu").IntegerValue
		      else
		        IdGridAvailData2(i)=RecordSetAvailMeas.Field("id_tb_grid").IntegerValue
		        if dataTypeID>0 and dataTypeID<=cfgDV.nStations then
		          IdUVAvailData2(i)=RecordSetAvailMeas.Field("id_tb_crad_header").IntegerValue
		        elseif dataTypeID=0 then
		          IdUVAvailData2(i)=RecordSetAvailMeas.Field("id_tb_cuv_headers").IntegerValue
		        else
		          IdUVAvailData2(i)=RecordSetAvailMeas.Field("id_tb_wuv_headers").IntegerValue
		        end
		      end
		      Xdates(i)=RecordSetAvailMeas.Field("measTimeDB").DateValue
		      Xall(i)=Str(Xdates(i).Day,"00") + "/" + Str(Xdates(i).Month,"00") + "/" + Right(CStr(Xdates(i).Year),2) + EndOfLine + Right(Xdates(i).SQLDateTime,8)
		      'main.TimeDataPage.Xall(i)=CStr(main.TimeDataPage.Xdates(i).Day) + " " + main.TimeDataPage.GetMonthString(main.TimeDataPage.Xdates(i).Month) + EndOfLine + Right(RecordSetAvailMeas.Field("datetime").StringValue,5)
		      RecordSetAvailMeas.MoveNext
		    next
		    
		    // get id_tb_ixiy for point of interest App.
		    RecordSetAvailMeas=db.GetPtInfoFromApproxCoords(lat-App.halflatstep, lat+App.halflatstep, lon-App.halflonstep, lon+App.halflonstep)
		    if RecordSetAvailMeas.BOF then
		      LoadPlotDialogTime.Hide
		      log.Write(true,"Could not find point coordinates in grid " + CStr(IdGridAvailData2(0)))
		      return
		    elseif RecordSetAvailMeas.Field("watt").BooleanValue then
		      LoadPlotDialogTime.Hide
		      MsgBox(kCoordinatesCorrespondToLand(cfgDV.language))
		      return
		    else
		      idixiy=RecordSetAvailMeas.Field("id_tb_ixiy").StringValue
		      lat=RecordSetAvailMeas.Field("lat_grd").DoubleValue
		      lon=RecordSetAvailMeas.Field("lon_grd").DoubleValue
		    end
		    
		    //Update Yall, X2, Y2 and Y2_2 values
		    if pmSymbols.Text=kStickPlot(cfgDV.language) then
		      UpdateValuesXY(IdUVAvailData2, idixiy, 0, 1, nAvailData2)
		    else
		      auxInt=99
		      if cb2ndplot.Value then
		        auxInt=DataTyp2Int(pmDataType2.Text)
		      end
		      
		      UpdateValuesXY(IdUVAvailData2, idixiy,DataTyp2Int( pmDataType.Text), auxInt, nAvailData2)
		    end
		    
		    if cancelLoading then
		      LoadPlotDialogTime.Hide
		      return
		    end
		    
		    // Asking for exclusive edition rights on image file resource (avoid that a parallel session modifies it while being modified here)
		    if App.plotFilePass.TryEnter then
		    else
		      if App.SessionFailed then
		        log.Write(false,"It is detected that App.SessionFailed was true! Previous session probably encountered problems. Reseting plotFilePass CriticalSection.")
		        App.plotFilePass= new CriticalSection  // resets the CriticalSection plotFilePass, in case the previous session terminated with unhandled exception during plotting
		        App.SessionFailed=false
		      end
		      App.plotFilePass.Enter
		    end
		    
		    // Now create plot...
		    if pmSymbols.Text=kStickPlot(cfgDV.language) then
		      StickPlot()
		    else
		      // plot graph for plot 1
		      PlotGraph(1)
		      
		      if cb2ndplot.Value then
		        // plot graph for plot 2
		        PlotImage.Height=250  //Should be smaller, since it does not include statistics
		        PlotGraph(2)
		        PlotImage.Height=350
		        
		        dim ShellObject as new Shell
		        ShellObject.Mode=0
		        ShellObject.Execute("convert " + App.execDir + "plotpicture.png " + App.execDir + "plotpicture2.png -append " + App.execDir + "plotpicture.png")
		        
		      end
		    end
		    
		    //Now we can export the data.
		    BtnNetCDF.Enabled=true
		    BtnNetCDF.Style=BtnCommonFont
		    BtnCSV.Enabled=true
		    BtnCSV.Style=BtnCommonFont
		  end
		  
		  //Download resource
		  Session.StdWebFile=WebFile.Open(GetFolderItem(App.execDir + "plotpicture.png"))
		  Session.StdWebFile.ForceDownload=false
		  if Session.StdWebFile<>Nil then
		    'ShowURL(Session.StdWebFile.URL)
		    ExecuteJavaScript("window.open('"+ Session.StdWebFile.URL + "');")
		  else
		    log.Write(true, "Plot image file is not available for download!")
		  end
		  
		  //Now someone else can use this resource...
		  App.plotFilePass.Leave
		  
		  LoadPlotDialogTime.Hide
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PlotGraph(plotNo as integer)
		  dim c as new CDXYChartMBS(PlotImage.Width,PlotImage.Height)
		  dim yaxistitle as string
		  dim auxInt, i as Integer
		  
		  if plotNo=1 then
		    call c.setPlotArea(50,112,PlotImage.Width-70,PlotImage.Height-167)
		  else
		    call c.setPlotArea(50,12,PlotImage.Width-70,PlotImage.Height-67)
		  end if
		  
		  if cfgDV.plotUseWaterMark then
		    dim weraphoto as FolderItem
		    weraphoto=GetFolderItem(App.execDir + cfgDV.plotWaterMarkFile)
		    c.getPlotArea.setBackground(weraphoto, c.kTopRight)
		  end
		  
		  
		  
		  Dim auxStr, siString, radarName as string
		  if plotNo=1 then
		    
		    
		    Dim si as Double
		    
		    //Calculate sample interval for the statistics table
		    if Xdates.Ubound>0 then
		      if cfgDV.sampInterval=0 then
		        si=Xdates(1).TotalSeconds-Xdates(0).TotalSeconds
		        for i=2 to Xdates.Ubound-1
		          si=min( si, Xdates(i).TotalSeconds - Xdates(i-1).TotalSeconds)
		        next
		      else
		        si=cfgDV.sampInterval
		      end
		      siString=CStr(round((si*10)/60)/10) + " min."   'Rounded to one decimal should be enough
		    else
		      siString="-"
		    end
		    
		    
		    
		    //use si double variable to adjust ticks number and labels in Xall  =================
		    redim Xall2(-1)
		    dim auxDate as Date
		    auxDate=new Date
		    
		    if minutesInBetween<=(12*si/60) then   'if 12 times the sample interval is already bigger than the whole timespan of the plot then use exact labels.
		      'Use exact labels...
		      auxDate.TotalSeconds=Xdates(0).TotalSeconds
		    else
		      if minutesInBetween<=12 then   'less than 12 minutes, then 1 minute ticks
		        si = 60
		      elseif minutesInBetween<=24 then   'less than 24 minutes, then 2 minute ticks
		        si = 2*60
		      elseif minutesInBetween<=36 then   'less than 36 minutes, then 3 minute ticks
		        si = 3*60
		      elseif minutesInBetween<=75 then   'less than 75 minutes, then 5 minute ticks
		        si = 5*60
		      elseif minutesInBetween<=120 then   'less than 2 hours, then 10 minute ticks
		        si = 10*60
		      elseif minutesInBetween<=180 then   'less than 3 hours, then 15 minute ticks
		        si = 15*60
		      elseif minutesInBetween<=240 then   'less than 4 hours, then 20 minute ticks
		        si = 20*60
		      elseif minutesInBetween<=360 then   'less than 6 hours, then 30 minute ticks
		        si = 30*60
		      elseif minutesInBetween<=(12*60) then   'less than 12 hours, then 1 hour ticks
		        si = 60*60
		      elseif minutesInBetween<=(24*60) then   'less than a day, then 2 hour ticks
		        si = 2*60*60
		      elseif minutesInBetween<=(36*60) then   'less than 1.5 days, then 3 hour ticks
		        si = 3*60*60
		      elseif minutesInBetween<=(72*60) then   'less than 3 days, then 6 hour ticks
		        si = 6*60*60
		      elseif minutesInBetween<=(144*60) then   'less than 6 days, then 12 hour ticks
		        si = 12*60*60
		      elseif minutesInBetween<=(12*60*24) then   'less than 12 days, then 1 day ticks
		        si = 60*60*24
		      elseif minutesInBetween<=(24*60*24) then   'less than 24 days, then 2 days ticks
		        si = 2*60*60*24
		      elseif minutesInBetween<=(36*60*24) then   'less than 36 days, then 3 days ticks
		        si = 3*60*60*24
		      elseif minutesInBetween<=(60*60*24) then   'less than 60 days, then 5 days ticks
		        si = 5*60*60*24
		      else   ' then 10 days ticks
		        si = 10*60*60*24
		      end if
		      
		      //Now set the initial date of the plot...
		      if si>=60*60*24 then
		        auxDate.TotalSeconds=Floor(Xdates(0).TotalSeconds/(60*60*24))*(60*60*24)
		      else
		        auxDate.TotalSeconds=Floor(Xdates(0).TotalSeconds/si)*si
		      end if
		      
		    end
		    
		    'Set the initial reference to be used later for corrected X2 (X2_2)
		    dim startSecs as Double
		    startSecs=auxDate.TotalSeconds
		    
		    //Set the initial label of the plot and continue using the tick interval stored in si
		    Xall2.Append(  Str(auxDate.Day,"00") + "/" + Str(auxDate.Month,"00") + "/" + Right(CStr(auxDate.Year),2) + EndOfLine + Right(auxDate.SQLDateTime,8)  )
		    auxDate.TotalSeconds=auxDate.TotalSeconds+si
		    do Until Xdates(Xdates.Ubound).TotalSeconds < auxDate.TotalSeconds
		      Xall2.Append(  Str(auxDate.Day,"00") + "/" + Str(auxDate.Month,"00") + "/" + Right(CStr(auxDate.Year),2) + EndOfLine + Right(auxDate.SQLDateTime,8)  )
		      auxDate.TotalSeconds=auxDate.TotalSeconds+si
		    Loop
		    
		    
		    //Calculate correct X2 (X2_2)
		    Redim X2_2(X2.Ubound)
		    for i=0 to X2.Ubound
		      X2_2(i)=(Xdates(X2(i)).TotalSeconds-startSecs)/si
		    next
		    
		    
		    
		    if pmSymbols.ListIndex=0 or pmSymbols.ListIndex=2 then
		      auxInt=5
		    else
		      auxInt=0
		    end
		    
		    
		  else
		    
		    //Correct Xall2 and X2_2 were already calculated...
		    
		    if pmSymbols2.ListIndex=0 or pmSymbols2.ListIndex=2 then
		      auxInt=5
		    else
		      auxInt=0
		    end
		    
		  end
		  
		  
		  call c.xAxis.setLabelStyle("normal",8,&hffff0002,70)
		  call c.xAxis.setLabels(Xall2)
		  'c.xAxis.setLabelStep(Floor(Ubound(Xall)/10))
		  if cbVertGrid.Value then
		    c.getPlotArea.setGridColor(&haa333333,&haa333333,0,0)
		  end
		  
		  
		  dim layer as CDScatterLayerMBS
		  if plotNo=1 then
		    layer=c.addScatterLayer(X2_2,Y2,"",-1,auxInt,-1,-1)
		  else
		    layer=c.addScatterLayer(X2_2,Y2_2,"",-1,auxInt,-1,-1)
		  end
		  
		  if plotNo=1 then
		    if pmSymbols.ListIndex=1 or pmSymbols.ListIndex=2 then
		      layer.setLineWidth(2)
		    end
		    auxInt=DataTyp2Int(pmDataType.Text)
		  else
		    if pmSymbols2.ListIndex=1 or pmSymbols2.ListIndex=2 then
		      layer.setLineWidth(2)
		    end
		    auxInt=DataTyp2Int(pmDataType2.Text)
		  end
		  
		  
		  if plotNo=1 then
		    //Make statistics table.=======================
		    //Form title...
		    radarName=cfgDV.systemName
		    if dataTypeID=0 then
		      auxStr=kMeasuredCurrentData(cfgDV.language)
		    elseif dataTypeID=20 then
		      auxStr=kMeasuredWindData(cfgDV.language)
		    elseif dataTypeID=10 then
		      auxStr=kMeasuredWaveData(cfgDV.language)
		    else
		      auxStr=kMeasuredRadialCurrent(cfgDV.language) + siteInfo.sname( Floor( (auxInt-11)/2 ) )
		      radarName=siteInfo.longname( Floor( (auxInt-11)/2 ) )
		    end
		    
		    if auxInt>=21 and auxInt<=24 then auxStr=auxStr + kFromSwb(cfgDV.language)  'In case the data comes from a SWB
		    
		    //Include point coordinates in title
		    auxStr=auxStr + " @ " + Str(abs(lat),"#.####")
		    if lat<0 then
		      auxStr=auxStr + " S, "
		    else
		      auxStr=auxStr + " N, "
		    end
		    auxStr=auxStr + Str(abs(lon),"#.####")
		    if lon<0 then
		      auxStr=auxStr + " W"
		    else
		      auxStr=auxStr + " E"
		    end
		    
		    call c.addTitle(auxStr, "arialbi.ttf", 16)
		    call c.addText(50,40,kRadarName(cfgDV.language)+ EndOfLine + kStartTime(cfgDV.language)+ EndOfLine + kEndTime(cfgDV.language)+ EndOfLine + kSampleInterval(cfgDV.language),"boldItalic",8, &hffff0002 ,7,0,false)
		    call c.addText(160,40,radarName+ EndOfLine + Xdates(0).SQLDateTime + " UTC" + EndOfLine + Xdates(Xdates.Ubound).SQLDateTime + " UTC" + EndOfLine + siString,"normal",8, &hffff0002 ,7,0,false)
		    call c.addText(350,40,kNoOfHours(cfgDV.language)+ EndOfLine + kNoOfRecords(cfgDV.language)+ EndOfLine + kNoMissing(cfgDV.language)+ EndOfLine + kPercentAvailable(cfgDV.language),"boldItalic",8, &hffff0002 ,7,0,false)
		    call c.addText(460,40,CStr(Round(minutesInBetween/60))+ EndOfLine + CStr(Xdates.Ubound + 1) + EndOfLine + CStr(Xdates.Ubound-Y2.Ubound) + EndOfLine + CStr(round(100*(Y2.Ubound + 1)/(Xdates.Ubound + 1))),"normal",8, &hffff0002 ,7,0,false)
		  end
		  
		  yaxistitle="Unknown"
		  select case auxInt
		  case 0
		    yaxistitle=kAbsoluteVelocity(cfgDV.language) + " [m/s]"
		    if cbFixScaleY.Value then c.yAxis.setLinearScale(CDbl(fixScaleLow.Text),CDbl(fixScaleHigh.Text),0,0)
		  case 1
		    yaxistitle=kAbsoluteVelocityDirection(cfgDV.language) + " [deg]"
		    c.yAxis.setLinearScale(0,360,90,0)
		  case 2
		    yaxistitle=kEastwardVelocity(cfgDV.language) + " [m/s]"
		    if cbFixScaleY.Value then c.yAxis.setLinearScale(CDbl(fixScaleLow.Text),CDbl(fixScaleHigh.Text),0,0)
		  case 3
		    yaxistitle=App.kNameAccuracy(cfgDV.language) + Lowercase(kEastwardVelocity(cfgDV.language)) + " [m/s]"
		    if cbFixScaleY.Value then c.yAxis.setLinearScale(CDbl(fixScaleLow.Text),CDbl(fixScaleHigh.Text),0,0)
		  case 4
		    yaxistitle=kNorthwardVelocity(cfgDV.language) + " [m/s]"
		    if cbFixScaleY.Value then c.yAxis.setLinearScale(CDbl(fixScaleLow.Text),CDbl(fixScaleHigh.Text),0,0)
		  case 5
		    yaxistitle=App.kNameAccuracy(cfgDV.language) + Lowercase(kNorthwardVelocity(cfgDV.language)) + " [m/s]"
		    if cbFixScaleY.Value then c.yAxis.setLinearScale(CDbl(fixScaleLow.Text),CDbl(fixScaleHigh.Text),0,0)
		  case 6
		    yaxistitle=App.kNameCSWHVal(cfgDV.language) + " [m]"
		    if cbFixScaleY.Value then c.yAxis.setLinearScale(CDbl(fixScaleLow.Text),CDbl(fixScaleHigh.Text),0,0)
		  case 7
		    yaxistitle=App.kNameCSWHDir(cfgDV.language) + " [deg]"
		    c.yAxis.setLinearScale(0,360,90,0)
		  case 8
		    yaxistitle=App.kNameWD(cfgDV.language) + " [deg]"
		    c.yAxis.setLinearScale(0,360,90,0)
		  case 9
		    yaxistitle=App.kNameWS(cfgDV.language) + " [m/s]"
		    
		    if cbFixScaleY.Value then
		      c.yAxis.setLinearScale(CDbl(fixScaleLow.Text),CDbl(fixScaleHigh.Text),0,0)
		    else
		      // get maximum to correctly scale y axis
		      dim maxVal as double
		      if plotNo=1 then maxVal=Y2(0) else maxVal=Y2_2(0)
		      for i=1 to Y2.Ubound
		        if plotNo=1 then maxVal=max(Y2(i),maxVal) else maxVal=max(Y2_2(i),maxVal)
		      next
		      if maxVal<0 then maxVal=10 //This is done in case all values have -999 (no data available), we set the max value to a standard value of 10 m/s
		      c.yAxis.setLinearScale(0, ceil(maxVal), 1, 0)
		    end
		  case 21
		    yaxistitle=App.kNameCSWHVal(cfgDV.language) + " [m]"
		    if cbFixScaleY.Value then c.yAxis.setLinearScale(CDbl(fixScaleLow.Text),CDbl(fixScaleHigh.Text),0,0)
		  case 22
		    yaxistitle=App.kNameCSWHDir(cfgDV.language) + " [deg]"
		    c.yAxis.setLinearScale(0,360,90,0)
		  case 23
		    yaxistitle=App.kNameWD(cfgDV.language) + " [deg]"
		    c.yAxis.setLinearScale(0,360,90,0)
		  case 24
		    yaxistitle=App.kNameWS(cfgDV.language) + " [m/s]"
		    if cbFixScaleY.Value then c.yAxis.setLinearScale(CDbl(fixScaleLow.Text),CDbl(fixScaleHigh.Text),0,0)
		  else
		    for i=0 to cfgDV.nStations-1
		      if auxInt =11 + i*2 then
		        yaxistitle=App.kNameRC(cfgDV.language) + " [m/s]"
		        exit
		      elseif auxInt = 12+i*2 then
		        yaxistitle=App.kNameAccuracy(cfgDV.language) + Lowercase(App.kNameRC(cfgDV.language)) + " [m/s]"
		        exit
		      end
		    next
		    if cbFixScaleY.Value then c.yAxis.setLinearScale(CDbl(fixScaleLow.Text),CDbl(fixScaleHigh.Text),0,0)
		  end
		  
		  c.yAxis.setTitle(yaxistitle).setFontAngle(90)
		  'call c.xAxis.setTitle("(measurement intervals = 30 min)")
		  
		  PlotImage.Picture=c.makeChartPicture
		  
		  dim f as new FolderItem
		  dim p as Picture
		  
		  if plotNo=1 then
		    
		    f=GetFolderItem(App.execDir + "plotpicture.png")
		    
		  else
		    
		    f=GetFolderItem(App.execDir + "plotpicture2.png")
		    
		  end
		  p=PlotImage.Picture
		  p.Save(f,Picture.SaveAsPNG)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub pmDataTypeSelectionChanged()
		  dim auxStr as string
		  
		  if pmDataType.ListIndex=-1 then
		    pmDataType.ListIndex=0
		    return
		  end
		  
		  if pmDataType.Text=App.kNameCCVAbs(cfgDV.language) or pmDataType.Text=App.kNameCCVDir(cfgDV.language) then 'If selected absolute current velocity or current direction, then enable possiblity for stick plot and to mark 2nd plot
		    if pmSymbols.List(pmSymbols.ListCount-1)=kPointsAndLines(cfgDV.language) then pmSymbols.AddRow(kStickPlot(cfgDV.language)) 'Add stick plot option only if not yet added
		    cb2ndplot.Enabled=true
		    
		  else 'no absolute current velocity or current direction selected...
		    if pmSymbols.ListIndex=3 then 'If stick plot selected, then deselect it
		      pmSymbols.ListIndex=0
		    end
		    if pmSymbols.List(pmSymbols.ListCount-1)=kStickPlot(cfgDV.language) then  'If stick plot is there as option, then remove it
		      pmSymbols.RemoveRow(pmSymbols.ListCount-1)
		    end
		    if pmDataType.Text=App.kNameWD(cfgDV.language) AND not(cfgDV.enableWS) then
		      cb2ndplot.Enabled=false
		      cb2ndplot.Value=false
		    else
		      cb2ndplot.Enabled=true
		    end
		  end if
		  
		  
		  // Creating options for second plot
		  pmDataType2.DeleteAllRows
		  dataTypeID=0
		  auxStr=pmDataType.Text
		  if auxStr = App.kNameCCVAbs(cfgDV.language) then
		    pmDataType2.AddRow(App.kNameCCVDir(cfgDV.language))
		    pmDataType2.AddRow(App.kNameCCVE(cfgDV.language))
		    pmDataType2.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVE(cfgDV.language)))
		    pmDataType2.AddRow(App.kNameCCVN(cfgDV.language))
		    pmDataType2.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVN(cfgDV.language)))
		  elseif auxStr = App.kNameCCVDir(cfgDV.language) then
		    pmDataType2.AddRow(App.kNameCCVAbs(cfgDV.language))
		    pmDataType2.AddRow(App.kNameCCVE(cfgDV.language))
		    pmDataType2.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVE(cfgDV.language)))
		    pmDataType2.AddRow(App.kNameCCVN(cfgDV.language))
		    pmDataType2.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVN(cfgDV.language)))
		  elseif auxStr = App.kNameCCVE(cfgDV.language) then
		    pmDataType2.AddRow(App.kNameCCVN(cfgDV.language))
		    pmDataType2.AddRow(App.kNameCCVAbs(cfgDV.language))
		    pmDataType2.AddRow(App.kNameCCVDir(cfgDV.language))
		    pmDataType2.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVE(cfgDV.language)))
		    pmDataType2.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVN(cfgDV.language)))
		  elseif auxStr = (App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVE(cfgDV.language))) then
		    pmDataType2.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVN(cfgDV.language)))
		    pmDataType2.AddRow(App.kNameCCVAbs(cfgDV.language))
		    pmDataType2.AddRow(App.kNameCCVDir(cfgDV.language))
		    pmDataType2.AddRow(App.kNameCCVE(cfgDV.language))
		    pmDataType2.AddRow(App.kNameCCVN(cfgDV.language))
		  elseif auxStr = App.kNameCCVN(cfgDV.language) then
		    pmDataType2.AddRow(App.kNameCCVE(cfgDV.language))
		    pmDataType2.AddRow(App.kNameCCVAbs(cfgDV.language))
		    pmDataType2.AddRow(App.kNameCCVDir(cfgDV.language))
		    pmDataType2.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVE(cfgDV.language)))
		    pmDataType2.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVN(cfgDV.language)))
		  elseif auxStr = (App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVN(cfgDV.language))) then
		    pmDataType2.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVE(cfgDV.language)))
		    pmDataType2.AddRow(App.kNameCCVAbs(cfgDV.language))
		    pmDataType2.AddRow(App.kNameCCVDir(cfgDV.language))
		    pmDataType2.AddRow(App.kNameCCVE(cfgDV.language))
		    pmDataType2.AddRow(App.kNameCCVN(cfgDV.language))
		  elseif auxStr = App.kNameCSWHVal(cfgDV.language) then
		    pmDataType2.AddRow(App.kNameCSWHDir(cfgDV.language))
		    dataTypeID=10
		  elseif auxStr = App.kNameCSWHDir(cfgDV.language) then
		    pmDataType2.AddRow(App.kNameCSWHVal(cfgDV.language))
		    dataTypeID=10
		  elseif auxStr = App.kNameWD(cfgDV.language) then
		    if cfgDV.enableWS then pmDataType2.AddRow(App.kNameWS(cfgDV.language)) else pmDataType2.AddRow(" ") // Add speed as option if enabled, otherwise add an empty string
		    dataTypeID=20
		  elseif auxStr = App.kNameWS(cfgDV.language) then
		    pmDataType2.AddRow(App.kNameWD(cfgDV.language))
		    dataTypeID=20
		  else
		    Dim i as Integer
		    for i=0 to cfgDV.nStations-1
		      if auxStr = (siteInfo.sname(i) + " - " + App.kNameRC(cfgDV.language)) then
		        pmDataType2.AddRow(siteInfo.sname(i) + " - " + App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameRC(cfgDV.language)))
		        exit
		      elseif auxStr = (siteInfo.sname(i) + " - " + App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameRC(cfgDV.language))) then
		        pmDataType2.AddRow(siteInfo.sname(i) + " - " + App.kNameRC(cfgDV.language))
		        exit
		      end
		    next
		    dataTypeID=i+1
		    
		    // If using synthetic wave buoy and such data is selected, update accordingly
		    if App.SyWaBuSeaViewEnabled  AND usingSynthBuoy>0 then
		      if Right(pmDataType.Text, 2 + len(App.kNameSynthBuoy(cfgDV.language)) )="(" + App.kNameSynthBuoy(cfgDV.language) + ")" then 'Synthetic wave buoy data is selected!
		        
		        auxStr=left(pmDataType.Text, pmDataType.Text.Len-(3 + len(App.kNameSynthBuoy(cfgDV.language))))
		        if auxStr = App.kNameCSWHVal(cfgDV.language) then
		          pmDataType2.AddRow(App.kNameCSWHDir(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")")
		          dataTypeID=10
		        elseif auxStr = App.kNameCSWHDir(cfgDV.language) then
		          pmDataType2.AddRow(App.kNameCSWHVal(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")")
		          dataTypeID=10
		        elseif auxStr = App.kNameWD(cfgDV.language) then
		          pmDataType2.AddRow(App.kNameWS(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")")
		          dataTypeID=20
		        elseif auxStr = App.kNameWS(cfgDV.language) then
		          pmDataType2.AddRow(App.kNameWD(cfgDV.language) + " (" + App.kNameSynthBuoy(cfgDV.language) + ")")
		          dataTypeID=20
		        end
		        
		      end
		    end
		    
		  end
		  
		  pmDataType2.ListIndex=0
		  
		  RefreshLstAvailMeas()
		  
		  BtnNetCDF.Enabled=false
		  BtnNetCDF.Style=BtnCommonFontDisabled
		  BtnCSV.Enabled=false
		  BtnCSV.Style=BtnCommonFontDisabled
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RefreshLstAvailMeas()
		  Dim date1, date2 as String
		  Dim i, nAvailData as integer
		  Dim RecordSetAvailMeas as RecordSet
		  
		  LstAvailMeas.DeleteAllRows
		  
		  if WebCalendar.now=Nil then
		    Dim d as new Date
		    date1=d.SQLDate  + " 00:00:00"
		    date2=d.SQLDate  + " 23:59:59"
		  else
		    date1=Str(WebCalendar.now.SQLDate) + " 00:00:00"
		    date2=Str(WebCalendar.now.SQLDate) + " 23:59:59"
		  end
		  
		  if usingSynthBuoy>0 AND right(pmDataType.Text,1+len(App.kNameSynthBuoy(cfgDV.language)))=App.kNameSynthBuoy(cfgDV.language) + ")" then
		    RecordSetAvailMeas=db.GetSVWaBuDataRange(date1, date2, CStr(svwb.id_ixiys(usingSynthBuoy-1)) )
		  else
		    RecordSetAvailMeas=db.GetHeaderWithData(dataTypeID,date1,date2,False)
		  end
		  
		  nAvailData=0
		  
		  if RecordSetAvailMeas=Nil then
		    LstAvailMeas.AddRow(Main.kNoDataAvail(cfgDV.language))
		    return
		  else
		    if RecordSetAvailMeas.BOF then
		      LstAvailMeas.AddRow(Main.kNoDataAvail(cfgDV.language))
		    else
		      nAvailData=RecordSetAvailMeas.RecordCount
		      
		      Redim IdGridAvailData(nAvailData-1)
		      Redim IdUVAvailData(nAvailData-1)
		      Redim DateAvailData(nAvailData-1)
		      Redim QualAvailData(nAvailData-1)
		      
		      for i=0 to nAvailData-1
		        if usingSynthBuoy>0 AND right(pmDataType.Text,1+len(App.kNameSynthBuoy(cfgDV.language)))=App.kNameSynthBuoy(cfgDV.language) + ")" then
		          IdGridAvailData(i)=svwb.gridUsed
		        else
		          IdGridAvailData(i)=RecordSetAvailMeas.Field("id_tb_grid").IntegerValue
		        end if
		        if usingSynthBuoy>0 AND right(pmDataType.Text,1+len(App.kNameSynthBuoy(cfgDV.language)))=App.kNameSynthBuoy(cfgDV.language) + ")" then
		          IdUVAvailData(i)=RecordSetAvailMeas.Field("id_tb_svwabu").IntegerValue
		          QualAvailData(i)=true
		        elseif dataTypeID=0 then
		          IdUVAvailData(i)=RecordSetAvailMeas.Field("id_tb_cuv_headers").IntegerValue
		          QualAvailData(i)=RecordSetAvailMeas.Field("dataQual").BooleanValue
		        elseif dataTypeID=10 then
		          IdUVAvailData(i)=RecordSetAvailMeas.Field("id_tb_wuv_headers").IntegerValue
		          QualAvailData(i)=RecordSetAvailMeas.Field("dataQualWa").BooleanValue
		        elseif dataTypeID=20 then
		          IdUVAvailData(i)=RecordSetAvailMeas.Field("id_tb_wuv_headers").IntegerValue
		          QualAvailData(i)=RecordSetAvailMeas.Field("dataQualWi").BooleanValue
		        else
		          IdUVAvailData(i)=RecordSetAvailMeas.Field("id_tb_crad_header").IntegerValue
		          QualAvailData(i)=RecordSetAvailMeas.Field("dataQual").BooleanValue
		        end
		        DateAvailData(i)=RecordSetAvailMeas.Field("measTimeDB").DateValue
		        LstAvailMeas.AddRow(right(DateAvailData(i).SQLDateTime,8))
		        RecordSetAvailMeas.MoveNext
		      next
		    end
		  end
		  
		  LblMeasFound.Text=replace(Main.kFoundNMeas(cfgDV.language),"<n>",CStr(nAvailData))
		  
		  
		  //Check if the previously selected time slot is also present, if so then select it.
		  dim auxInt as Integer
		  dim auxStr as String
		  auxStr=Right(SelectedMeas,8)
		  auxInt=-1
		  for i=0 to LstAvailMeas.RowCount-1
		    if LstAvailMeas.List(i)=auxStr then
		      auxInt=i
		      exit
		    end
		  next
		  
		  
		  if auxInt=-1 then 'Not found... so the first available will be selected...
		    LstAvailMeas.ListIndex=0
		    LstAvailMeas.ScrollTo(0)
		    if not(Left(LstAvailMeas.List(0),1)="(") then  // the '(' will simply indicate that the string '(No data available)' is there, which means that there is no measurement, otherwise, update time too.
		      SelectedMeas=DateAvailData(0).SQLDateTime
		    end
		  else
		    LstAvailMeas.ListIndex=auxInt
		    LstAvailMeas.ScrollTo(auxInt)
		    SelectedMeas=DateAvailData(auxInt).SQLDateTime
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub StickPlot()
		  dim zerosVector(-1), maxVal, minVal, auxVal as Double
		  dim i, xOffset as Integer
		  dim yaxistitle, auxStr as string
		  dim auxX2(-1), auxY2(-1), auxY2_2(-1) as Double
		  dim auxXall(-1) as string
		  
		  // Create a XYChart object of size 450 x 390 pixels
		  dim c as new CDXYChartMBS(PlotImage.Width,PlotImage.Height)
		  
		  call c.setPlotArea(50,112,PlotImage.Width-70,PlotImage.Height-167)
		  
		  
		  if cfgDV.plotUseWaterMark then
		    dim weraphoto as FolderItem
		    weraphoto=GetFolderItem(App.execDir + cfgDV.plotWaterMarkFile)
		    c.getPlotArea.setBackground(weraphoto, c.kTopRight)
		  end
		  
		  
		  // get maximum and minimum values in the stick plot to correctly scale y axis
		  maxVal=Y2(0)*cos(Y2_2(0)*3.1416/180)
		  minVal=maxVal
		  for i=1 to Y2.Ubound
		    auxVal=Y2(i)*cos(Y2_2(i)*3.1416/180)
		    maxVal=max(maxVal,auxVal)
		    minVal=min(minVal,auxVal)
		  next
		  maxVal=max(Ceil(maxVal/0.2)*0.2,0.2)
		  minVal=min(Floor(minVal/0.2)*0.2,-0.2)
		  c.yAxis.setLinearScale(minVal, maxVal, 0.2, 0.1)
		  
		  
		  
		  
		  
		  Dim siString as string
		  
		  Dim si as Double
		  
		  //Calculate sample interval for the statistics table
		  if Xdates.Ubound>0 then
		    if cfgDV.sampInterval=0 then
		      si=Xdates(1).TotalSeconds-Xdates(0).TotalSeconds
		      for i=2 to Xdates.Ubound-1
		        si=min( si, Xdates(i).TotalSeconds - Xdates(i-1).TotalSeconds)
		      next
		    else
		      si=cfgDV.sampInterval
		    end
		    siString=CStr(round((si*10)/60)/10) + " min."   'Rounded to one decimal should be enough
		  else
		    siString="-"
		  end
		  
		  
		  
		  //use si double variable to adjust ticks number and labels in Xall  =================
		  redim Xall2(-1)
		  dim auxDate as Date
		  auxDate=new Date
		  
		  if minutesInBetween<=(12*si/60) then   'if 12 times the sample interval is already bigger than the whole timespan of the plot then use exact labels.
		    'Use exact labels...
		    auxDate.TotalSeconds=Xdates(0).TotalSeconds
		  else
		    if minutesInBetween<=12 then   'less than 12 minutes, then 1 minute ticks
		      si = 60
		    elseif minutesInBetween<=24 then   'less than 24 minutes, then 2 minute ticks
		      si = 2*60
		    elseif minutesInBetween<=36 then   'less than 36 minutes, then 3 minute ticks
		      si = 3*60
		    elseif minutesInBetween<=75 then   'less than 75 minutes, then 5 minute ticks
		      si = 5*60
		    elseif minutesInBetween<=120 then   'less than 2 hours, then 10 minute ticks
		      si = 10*60
		    elseif minutesInBetween<=180 then   'less than 3 hours, then 15 minute ticks
		      si = 15*60
		    elseif minutesInBetween<=240 then   'less than 4 hours, then 20 minute ticks
		      si = 20*60
		    elseif minutesInBetween<=360 then   'less than 6 hours, then 30 minute ticks
		      si = 30*60
		    elseif minutesInBetween<=(12*60) then   'less than 12 hours, then 1 hour ticks
		      si = 60*60
		    elseif minutesInBetween<=(24*60) then   'less than a day, then 2 hour ticks
		      si = 2*60*60
		    elseif minutesInBetween<=(36*60) then   'less than 1.5 days, then 3 hour ticks
		      si = 3*60*60
		    elseif minutesInBetween<=(72*60) then   'less than 3 days, then 6 hour ticks
		      si = 6*60*60
		    elseif minutesInBetween<=(144*60) then   'less than 6 days, then 12 hour ticks
		      si = 12*60*60
		    elseif minutesInBetween<=(12*60*24) then   'less than 12 days, then 1 day ticks
		      si = 60*60*24
		    elseif minutesInBetween<=(24*60*24) then   'less than 24 days, then 2 days ticks
		      si = 2*60*60*24
		    elseif minutesInBetween<=(36*60*24) then   'less than 36 days, then 3 days ticks
		      si = 3*60*60*24
		    elseif minutesInBetween<=(60*60*24) then   'less than 60 days, then 5 days ticks
		      si = 5*60*60*24
		    else   ' then 10 days ticks
		      si = 10*60*60*24
		    end if
		    
		    //Now set the initial date of the plot...
		    if si>=60*60*24 then
		      auxDate.TotalSeconds=Floor(Xdates(0).TotalSeconds/(60*60*24))*(60*60*24)
		    else
		      auxDate.TotalSeconds=Floor(Xdates(0).TotalSeconds/si)*si
		    end if
		    
		  end
		  
		  'Set the initial reference to be used later for corrected X2 (X2_2)
		  dim startSecs as Double
		  startSecs=auxDate.TotalSeconds
		  
		  //Set the initial label of the plot and continue using the tick interval stored in si
		  Xall2.Append(  Str(auxDate.Day,"00") + "/" + Str(auxDate.Month,"00") + "/" + Right(CStr(auxDate.Year),2) + EndOfLine + Right(auxDate.SQLDateTime,8)  )
		  auxDate.TotalSeconds=auxDate.TotalSeconds+si
		  do Until Xdates(Xdates.Ubound).TotalSeconds < auxDate.TotalSeconds
		    Xall2.Append(  Str(auxDate.Day,"00") + "/" + Str(auxDate.Month,"00") + "/" + Right(CStr(auxDate.Year),2) + EndOfLine + Right(auxDate.SQLDateTime,8)  )
		    auxDate.TotalSeconds=auxDate.TotalSeconds+si
		  Loop
		  
		  
		  //Calculate correct X2 (X2_2)
		  Redim X2_2(X2.Ubound)
		  for i=0 to X2.Ubound
		    X2_2(i)=(Xdates(X2(i)).TotalSeconds-startSecs)/si
		  next
		  
		  
		  
		  
		  
		  
		  // free space on the sides on x axis
		  xOffset=0.1*(Xall2.Ubound)
		  if xOffset=0 then
		    xOffset=1
		  end
		  
		  Redim auxX2(X2_2.Ubound+(2*xOffset))
		  Redim auxY2(auxX2.Ubound)
		  Redim auxY2_2(auxX2.Ubound)
		  Redim auxXall(Xall2.Ubound+(2*xOffset))
		  Redim zerosVector(auxX2.Ubound)  // the values are by default initialized as 0, the value that we need
		  
		  for i=-xOffset to X2_2.Ubound+xOffset
		    if i<0 or i>X2_2.Ubound then
		      auxY2(i+xOffset)=0
		      auxY2_2(i+xOffset)=0
		      if i<0 then
		        auxX2(i+xOffset)=i+xOffset
		      else
		        auxX2(i+xOffset)=X2_2(X2_2.Ubound) + xOffset + (i-X2_2.Ubound)
		      end
		    else
		      auxY2(i+xOffset)=Y2(i)
		      auxY2_2(i+xOffset)=Y2_2(i)
		      auxX2(i+xOffset)=X2_2(i)+xOffset
		    end
		  next
		  
		  for i=-xOffset to Xall2.Ubound+xOffset
		    if i<0 or i>Xall2.Ubound then
		      auxXall(i+xOffset)=""
		    else
		      auxXall(i+xOffset)=Xall2(i)
		    end if
		  next
		  
		  call c.xAxis.setLabelStyle("",8,&hffff0002,70)
		  call c.xAxis.setLabels(auxXall)
		  'c.xAxis.setLabelStep(Floor(Ubound(auxXall)/10))
		  
		  
		  // Add a vector layer to the chart using blue (0000CC) color, with vector arrow
		  // size set to 11 pixels
		  
		  //dim layer as CDVectorLayerMBS
		  call c.addVectorLayer(auxX2, zerosVector, auxY2, auxY2_2, 2, &h0000cc,"").setArrowHead(0)
		  
		  yaxistitle=kSurfaceCurrentVelocity(cfgDV.language) + " [m/s]"
		  
		  
		  auxStr=kMeasuredCurrentData(cfgDV.language)
		  
		  auxStr=auxStr + " ( " + Str(abs(lat),"#.####")
		  if lat<0 then
		    auxStr=auxStr + " S, "
		  else
		    auxStr=auxStr + " N, "
		  end
		  
		  auxStr=auxStr + Str(abs(lon),"#.####")
		  if lon<0 then
		    auxStr=auxStr + " W )"
		  else
		    auxStr=auxStr + " E )"
		  end
		  
		  
		  
		  
		  
		  
		  
		  //Make statistics table.=======================
		  //Form title...
		  
		  call c.addTitle(auxStr, "arialbi.ttf", 16)
		  call c.addText(50,40,kRadarName(cfgDV.language)+ EndOfLine + kStartTime(cfgDV.language)+ EndOfLine + kEndTime(cfgDV.language)+ EndOfLine + kSampleInterval(cfgDV.language),"boldItalic",8, &hffff0002 ,7,0,false)
		  call c.addText(160,40,cfgDV.systemName+ EndOfLine + Xdates(0).SQLDateTime + " UTC" + EndOfLine + Xdates(Xdates.Ubound).SQLDateTime + " UTC" + EndOfLine + siString,"normal",8, &hffff0002 ,7,0,false)
		  call c.addText(350,40,kNoOfHours(cfgDV.language)+ EndOfLine + kNoOfRecords(cfgDV.language)+ EndOfLine + kNoMissing(cfgDV.language)+ EndOfLine + kPercentAvailable(cfgDV.language),"boldItalic",8, &hffff0002 ,7,0,false)
		  call c.addText(460,40,CStr(Round(minutesInBetween/60))+ EndOfLine + CStr(Xdates.Ubound + 1) + EndOfLine + CStr(Xdates.Ubound-Y2.Ubound) + EndOfLine + CStr(round(100*(Y2.Ubound + 1)/(Xdates.Ubound + 1))),"normal",8, &hffff0002 ,7,0,false)
		  'Dim box as CDBoxWhiskerLayerMBS
		  'box=c.
		  
		  c.yAxis.setTitle(yaxistitle).setFontAngle(90)
		  'call c.xAxis.setTitle("(measurement intervals = 30 min)")
		  
		  PlotImage.Picture=c.makeChartPicture
		  
		  dim f as new FolderItem
		  dim p as Picture
		  
		  f=GetFolderItem(App.execDir + "plotpicture.png")
		  
		  p=PlotImage.Picture
		  p.Save(f,Picture.SaveAsPNG)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateCoordVals()
		  // Updates the text values of the latitude longitude coordinates with the values that correspond to the current grid indexes
		  
		  Dim sqlString as String
		  Dim rs as RecordSet
		  Dim tmpLon as single
		  Dim tmpLat as single
		  
		  sqlString="SELECT lon_grd, lat_grd FROM tb_ixiy WHERE id_tb_grid=" + Cstr(cfgDV.gridID) + " AND ix=" + CStr(ix) + " AND " +"iy=" + CStr(iy)
		  rs=db.archive.SQLSelect(sqlString)
		  
		  if db.HandleDatabaseError("Error while trying to get latitude and longitude values using ix=" + CStr(ix) + " and " +"iy=" + CStr(iy)) then
		    return
		  elseif rs.RecordCount=0 then
		    if not(ix=0 or iy=0) then //avoids error message when initializing
		      log.Write(true,"No latitude and longitude values found for grid indexes ix=" + CStr(ix) + " and " +"iy=" + CStr(iy))
		    end
		    return
		  end
		  
		  tmpLon=rs.Field("lon_grd").DoubleValue
		  tmpLat=rs.Field("lat_grd").DoubleValue
		  
		  if not(lon=tmpLon) then
		    TxtLon.Text=rs.Field("lon_grd").StringValue
		  end
		  
		  if not(lat=tmpLat) then
		    TxtLat.Text=rs.Field("lat_grd").StringValue
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateGridVals()
		  // Updates the text values of the latitude longitude coordinates with values that correspond to the closest grid point.
		  // It also updates the values of the grid ix iy coordinates with the values that correspond to the grid point found.
		  
		  Dim rs as RecordSet
		  Dim tmpLon as single
		  Dim tmpLat as single
		  Dim tmpIx as Integer
		  Dim tmpIy as integer
		  
		  rs=db.GetPtInfoFromApproxCoords(lat-App.halflatstep, lat+App.halflatstep, lon-App.halflonstep, lon+App.halflonstep)
		  
		  if rs=nil then
		    return
		  elseif rs.RecordCount=0 then
		    if not(lon=0 or lat=0) then //avoids error message when initializing
		      log.Write(true,"No latitude and longitude values found for coordinates lat=" + CStr(lat) + " and " + "lon=" + CStr(lon))
		    end
		    return
		  end
		  
		  tmpLon=rs.Field("lon_grd").DoubleValue
		  tmpLat=rs.Field("lat_grd").DoubleValue
		  tmpIx=rs.Field("ix").IntegerValue
		  tmpIy=rs.Field("iy").IntegerValue
		  
		  if not(lon=tmpLon) then
		    TxtLon.Text=rs.Field("lon_grd").StringValue
		  end
		  
		  if not(lat=tmpLat) then
		    TxtLat.Text=rs.Field("lat_grd").StringValue
		  end
		  
		  if not(ix=tmpIx) then
		    TxtIx.Text=rs.Field("ix").StringValue
		  end if
		  
		  if not(iy=tmpIy) then
		    TxtIy.Text=rs.Field("iy").StringValue
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateLblMeasInRange()
		  if Left(LabelFrom.Text,1)="N" OR Left(LabelTo.Text,1)="N" then
		    return
		  end
		  
		  Dim date1, date2 as String
		  Dim RecordSetAvailMeas as RecordSet
		  Dim nAvailData2 as integer
		  
		  date1=LabelFrom.Text
		  date2=LabelTo.Text
		  
		  if usingSynthBuoy>0 AND right(pmDataType.Text,1+len(App.kNameSynthBuoy(cfgDV.language)))=App.kNameSynthBuoy(cfgDV.language) + ")" then
		    RecordSetAvailMeas=db.GetSVWaBuDataRange(date1, date2, CStr(svwb.id_ixiys(usingSynthBuoy-1)))
		  else
		    RecordSetAvailMeas=db.GetHeaderData(dataTypeID,date1,date2,False)
		  end if
		  
		  nAvailData2=0
		  
		  if RecordSetAvailMeas=Nil then
		  else
		    nAvailData2=RecordSetAvailMeas.RecordCount
		    
		    LblMeasInRange.Text=replace(kFoundNMeasInRange(cfgDV.language), "<n>",CStr(nAvailData2))
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateValuesXY(iduv() as integer, idixiy as string, LstIdx as Integer, LstIdx2 as integer, n as Integer)
		  // Updates the values of the arrays X2, Y2, and so on, used for creating plots.
		  
		  dim var1, var2, var3, var4 as Double
		  dim i, qual as Integer
		  dim rs as RecordSet
		  dim wQualSWB, invDataSWB, dirQualSWB as Boolean
		  
		  Redim X2(-1)
		  Redim Y2(-1)
		  Redim Y2_2(-1)
		  
		  // for each measurement found, look for variable to plot
		  
		  for i=0 to n-1
		    App.DoEvents
		    if cancelLoading then
		      return
		    end
		    
		    if usingSynthBuoy>0 AND right(pmDataType.Text,1+len(App.kNameSynthBuoy(cfgDV.language)))=App.kNameSynthBuoy(cfgDV.language) + ")" then
		      if InStr(0,pmDataType.Text,App.kNameCSWHVal(cfgDV.language))>0 OR InStr(0,pmDataType.Text,App.kNameCSWHDir(cfgDV.language))>0 then 'It is wave data...
		        rs=db.GetSVWaBuDataFromID(CStr(iduv(i)), qualFilterLevel)
		      else 'It is wind data
		        rs=db.GetSVWaBuDataFromID(CStr(iduv(i)), 0)
		      end
		      qual=rs.Field("qual").IntegerValue
		      qual=svwb.DecodeQualVal(qual,wQualSWB,invDataSWB,dirQualSWB)
		    else
		      rs=db.GetMeasDataFromPtIdHeadID(dataTypeID,iduv(i),idixiy, qualFilterLevel)  //Ask only for values with quality control included
		    end if
		    
		    if rs.BOF then
		      Yall(i)=0
		    else
		      
		      if LstIdx>=0 and LstIdx<=5 then
		        
		        
		        if rs.FieldCount=2 then 'We got a replacement record from tb_custom_data_cuv
		          //NOTE: Theoretically, rs should contain 5 rows, each row with a val and valType field. Rows should be in such an order that the 1st, 2nd, 3rd, 4th and 5th rows correspond to velu, velv, kl, accu and accv so I use these.
		          var1=rs.Field("val").DoubleValue
		          rs.MoveNext
		          var2=rs.Field("val").DoubleValue
		          rs.MoveNext
		          rs.MoveNext 'We skip the third one cause quality is not required for the plot...
		          var3=rs.Field("val").DoubleValue
		          rs.MoveNext
		          var4=rs.Field("val").DoubleValue
		        else
		          var1=rs.Field("velu").DoubleValue
		          var2=rs.Field("velv").DoubleValue
		          var3=rs.Field("accu").DoubleValue
		          var4=rs.Field("accv").DoubleValue
		        end
		        
		      end
		      
		      select case LstIdx
		        
		      case 0
		        Yall(i)=Sqrt(var1*var1 + var2*var2)
		      case 1
		        Yall(i)=450 - (ATan2(var2,var1)*180/3.1416)
		        if Yall(i)>360 then
		          Yall(i)=Yall(i)-360
		        end
		      case 2
		        Yall(i)=var1
		      case 3
		        Yall(i)=var3
		      case 4
		        Yall(i)=var2
		      case 5
		        Yall(i)=var4
		      case 6
		        Yall(i)=rs.Field("hwave").DoubleValue
		      case 7
		        Yall(i)=rs.Field("dir").DoubleValue
		      case 8
		        var1=rs.Field("dir").DoubleValue
		        if cfgDV.windDirectionFrom then
		          if var1<180 then var1=var1+180 else var1=var1-180
		        end
		        Yall(i)=var1
		      case 9
		        Yall(i)=rs.Field("speed").DoubleValue
		      case 21
		        Yall(i)=rs.Field("wa_height").DoubleValue
		      case 22
		        if invDataSWB AND (qualFilterLevel=0 OR (qualFilterLevel>=3 AND dirQualSWB)) then
		          var1=rs.Field("wa_mean_dir").DoubleValue
		          if var1<0 AND var1<>-999 then var1=var1+360
		          Yall(i)=var1
		        else
		          Yall(i)=-999
		        end
		      case 23
		        if (wQualSWB AND qualFilterLevel>=3) OR qualFilterLevel=0 then
		          var1=rs.Field("wi_dir").DoubleValue
		          if var1<0 AND var1 <>-999 then var1=var1+360
		          if cfgDV.windDirectionFrom then
		            if var1<180 then var1=var1+180 else var1=var1-180
		          end
		          Yall(i)=var1
		        else
		          Yall(i)=0
		          continue 'If there is a second plot does not matter... we know already that the quality is not good enough
		        end
		      case 24
		        if (wQualSWB AND qualFilterLevel>=3) OR qualFilterLevel=0 then
		          Yall(i)=rs.Field("wi_spd").DoubleValue
		        else
		          Yall(i)=0
		          continue 'If there is a second plot does not matter... we know already that the quality is not good enough
		        end
		      else
		        if (LstIdx mod 2)=1 then
		          Yall(i)=rs.Field("vel").DoubleValue
		        else
		          Yall(i)=rs.Field("acc").DoubleValue
		        end
		      end
		      
		      if not(cb2ndplot.Value) AND Yall(i)=-999 then 'Let us not append this values... this way it will be counted as a missing value at the statistics table (the way it should be).
		        Yall(i)=0
		        continue
		      end
		      
		      Y2.Append(Yall(i))
		      'X2.Append(Xdates(i).TotalSeconds - Xdates(0).TotalSeconds)   //This should correctly scale the x axis in case there are timeslots where no data files were created by the WERA system.   DOES NOT WORK AS EXPECTED!!!
		      X2.Append(i)
		      
		      if cb2ndplot.Value then
		        select case LstIdx2
		        case 0
		          Y2_2.Append(Sqrt(var1*var1 + var2*var2))
		        case 1
		          Y2_2.Append(450 - (ATan2(var2,var1)*180/3.1416))
		          if Y2_2(Y2_2.Ubound)>360 then
		            Y2_2(Y2_2.Ubound)=Y2_2(Y2_2.Ubound)-360
		          end
		        case 2
		          Y2_2.Append(var1)
		        case 3
		          Y2_2.Append(var3)
		        case 4
		          Y2_2.Append(var2)
		        case 5
		          Y2_2.Append(var4)
		        case 6
		          Y2_2.Append(rs.Field("hwave").DoubleValue)
		        case 7
		          Y2_2.Append(rs.Field("dir").DoubleValue)
		        case 8
		          var1=rs.Field("dir").DoubleValue
		          if cfgDV.windDirectionFrom then
		            if var1<180 then var1=var1+180 else var1=var1-180
		          end
		          Y2_2.Append(var1)
		        case 9
		          Y2_2.Append(rs.Field("speed").DoubleValue)
		        case 21
		          Y2_2.Append(rs.Field("wa_height").DoubleValue)
		        case 22
		          if invDataSWB AND (qualFilterLevel=0 OR (qualFilterLevel>=3 AND dirQualSWB)) then
		            var1=rs.Field("wa_mean_dir").DoubleValue
		            if var1<0 AND var1<>-999 then var1=var1+360
		            Y2_2.Append(var1)
		          else
		            Y2_2.Append(-999)
		          end
		        case 23
		          var1=rs.Field("wi_dir").DoubleValue
		          if var1<0 AND var1 <>-999 then var1=var1+360
		          if cfgDV.windDirectionFrom then
		            if var1<180 then var1=var1+180 else var1=var1-180
		          end
		          Y2_2.Append(var1)
		        case 24
		          Y2_2.Append(rs.Field("wi_spd").DoubleValue)
		        else
		          if (LstIdx2 mod 2)=1 then
		            Y2_2.Append(rs.Field("vel").DoubleValue)
		          else
		            Y2_2.Append(rs.Field("acc").DoubleValue)
		          end
		        end
		      end
		      
		    end
		    
		  next
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		cancelLoading As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		dataTypeID As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		DateAvailData(-1) As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		flagToAvoidExceptionDueToBug As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		IdGridAvailData(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected IdUVAvailData(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		ix As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		iy As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		lat As single
	#tag EndProperty

	#tag Property, Flags = &h0
		lon As Single
	#tag EndProperty

	#tag Property, Flags = &h0
		minutesInBetween As Single
	#tag EndProperty

	#tag Property, Flags = &h0
		MySession As WebSession
	#tag EndProperty

	#tag Property, Flags = &h0
		QualAvailData(-1) As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		qualFilterLevel As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		SelectedMeas As String
	#tag EndProperty

	#tag Property, Flags = &h0
		StdWebFile As WebFile
	#tag EndProperty

	#tag Property, Flags = &h0
		usingSynthBuoy As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			Contains only the indexes of points that will be plotted (no data available and bad quality are not included here)
		#tag EndNote
		X2() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			Correct values to be used instead of X2, so that the time series are correct displayed in the time axis
		#tag EndNote
		X2_2(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			Contains the time stamp of all data point between time range as a string variable as:
			
			DD/MM/YY
			HH:MM:SS
		#tag EndNote
		Xall() As string
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			The same as Xall, but this is for each of the ticks used in the plot, instead of each of the data points. This one is plotted together with X2_2 and delivers plots with correct time axis representation.
		#tag EndNote
		Xall2(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			Contains the time stamp of all data point between time range as a date variable
		#tag EndNote
		Xdates() As date
	#tag EndProperty

	#tag Property, Flags = &h0
		Y2() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		Y2_2() As double
	#tag EndProperty

	#tag Property, Flags = &h0
		Yall() As Double
	#tag EndProperty


	#tag Constant, Name = k1stPlotType, Type = String, Dynamic = True, Default = \"1st plot type", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"1st plot type"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Tipo de curva"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Tipo de gr\xC3\xA1fico"
	#tag EndConstant

	#tag Constant, Name = k1stSinglePlot, Type = String, Dynamic = True, Default = \"1st / Single plot", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"1st / Single plot"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"1er gr\xC3\xA1fico"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"1ro gr\xC3\xA1fico"
	#tag EndConstant

	#tag Constant, Name = k1stSinglePlotHelpTag, Type = String, Dynamic = True, Default = \"Select display mode.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select display mode."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Elija modo de visualizaci\xC3\xB3n de curva."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Elija modo de visualizaci\xC3\xB3n de curva."
	#tag EndConstant

	#tag Constant, Name = k2ndPlotType, Type = String, Dynamic = True, Default = \"2nd plot type", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"2nd plot type"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Tipo de curva"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Tipo de gr\xC3\xA1fico"
	#tag EndConstant

	#tag Constant, Name = kAbsoluteVelocity, Type = String, Dynamic = True, Default = \"Absolute velocity", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Absolute velocity"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Magnitud de la velocidad"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Magnitude da velocidade"
	#tag EndConstant

	#tag Constant, Name = kAbsoluteVelocityDirection, Type = String, Dynamic = True, Default = \"Absolute velocity direction", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Absolute velocity direction"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Direcci\xC3\xB3n de la velocidad"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Direc\xC3\xA7\xC3\xA3o da velocidade"
	#tag EndConstant

	#tag Constant, Name = kAdd2ndPlot, Type = String, Dynamic = True, Default = \"Add 2nd plot", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Add 2nd plot"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Incluir 2do gr\xC3\xA1fico"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Adicionar 2do gr\xC3\xA1fico"
	#tag EndConstant

	#tag Constant, Name = kAdd2ndPlotHelpTag, Type = String, Dynamic = True, Default = \"Select variable for 2nd plot.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Enable to include a second plot below the first plot for comparison."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Habilita casilla para incluir un segundo gr\xC3\xA1fico debajo del primero para comparaci\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Habilita casilla para incluir un segundo gr\xC3\xA1fico debajo del primero para comparaci\xC3\xB3n."
	#tag EndConstant

	#tag Constant, Name = kBtnSWB, Type = String, Dynamic = True, Default = \"Select synthetic buoy", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select synthetic buoy"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Selecciona boya sint\xC3\xA9tica"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Selecciona boya sint\xC3\xA9tica"
	#tag EndConstant

	#tag Constant, Name = kBtnSWBHelpTag, Type = String, Dynamic = True, Default = \"Update point coordinates to the synthetic wave buoy location", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Update point coordinates to the synthetic wave buoy location"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Actualiza las coordenadas a las de una boya sint\xC3\xA9tica"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Actualiza las coordenadas a las de una boya sint\xC3\xA9tica"
	#tag EndConstant

	#tag Constant, Name = kCannotCreatePlotOfMoreThanAMonth, Type = String, Dynamic = True, Default = \"Cannot create plot of more than a month!", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cannot create plot of more than a month!"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"\xC2\xA1No es posible crear gr\xC3\xA1fico de m\xC3\xA1s de un mes!"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"\xC2\xA1No es posible crear gr\xC3\xA1fico de m\xC3\xA1s de un mes!"
	#tag EndConstant

	#tag Constant, Name = kCantPlotWithoutAnEndDate, Type = String, Dynamic = True, Default = \"Can\'t plot without an end date and time. Please pick a date and a measurement time and click the button -To-.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Can\'t plot without an end date and time. Please pick a date and a measurement time and click the button -To-."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"No es posible hacer gr\xC3\xA1fico sin tiempo final. Por favor seleccione una fecha y una medici\xC3\xB3n y haga clic en el bot\xC3\xB3n -Final-."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"No es posible hacer gr\xC3\xA1fico sin tiempo final. Por favor seleccione una fecha y una medici\xC3\xB3n y haga clic en el bot\xC3\xB3n -Final-."
	#tag EndConstant

	#tag Constant, Name = kCantPlotWithoutAStartDate, Type = String, Dynamic = True, Default = \"Can\'t plot without a start date and time. Please pick a date and a measurement time and click the button -From-.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Can\'t plot without a start date and time. Please pick a date and a measurement time and click the button -From-."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"No es posible hacer gr\xC3\xA1fico sin tiempo inicial. Por favor seleccione una fecha y una medici\xC3\xB3n y haga clic en el bot\xC3\xB3n -Inicio-."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"No es posible hacer gr\xC3\xA1fico sin tiempo inicial. Por favor seleccione una fecha y una medici\xC3\xB3n y haga clic en el bot\xC3\xB3n -Inicio-."
	#tag EndConstant

	#tag Constant, Name = kCoordinatesCorrespondToLand, Type = String, Dynamic = True, Default = \"Coordinates correspond to land.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Coordinates correspond to land."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Las coordenadas corresponden a tierra firme."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Las coordenadas corresponden a tierra firme."
	#tag EndConstant

	#tag Constant, Name = kDisplayOptions, Type = String, Dynamic = True, Default = \"Display options", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Display options"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Opciones de visualizaci\xC3\xB3n"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Op\xC3\xA7\xC3\xB5es de exibi\xC3\xA7\xC3\xA3o"
	#tag EndConstant

	#tag Constant, Name = kDisplayVerticalGrid, Type = String, Dynamic = True, Default = \"Display vertical lines", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Display vertical lines"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Incluye lineas verticales"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exibir linhas verticais"
	#tag EndConstant

	#tag Constant, Name = kDisplayVerticalGridHelpTag, Type = String, Dynamic = True, Default = \"Include vertical lines in plot for better visualization.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Include vertical lines in plot for better visualization."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Incluye lineas verticales en el gr\xC3\xA1fico para mejor visualizaci\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Incluye lineas verticales en el gr\xC3\xA1fico para mejor visualizaci\xC3\xB3n."
	#tag EndConstant

	#tag Constant, Name = kEastwardVelocity, Type = String, Dynamic = True, Default = \"Eastward velocity", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Eastward velocity"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Velocidad hacia el este"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Velocidade para o leste"
	#tag EndConstant

	#tag Constant, Name = kEndTime, Type = String, Dynamic = True, Default = \"End time", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"End time"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Tiempo final"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Tempo do fim"
	#tag EndConstant

	#tag Constant, Name = kEndTSHelpTag, Type = String, Dynamic = True, Default = \"End of time series.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"End of time series."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Final de la serie de tiempo."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Fim da s\xC3\xA9rie temporal."
	#tag EndConstant

	#tag Constant, Name = kExportToCSV, Type = String, Dynamic = True, Default = \"Export to CSV", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Export to CSV"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Exportar a CSV"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exportar para CSV"
	#tag EndConstant

	#tag Constant, Name = kExportToCSVHelpTag, Type = String, Dynamic = True, Default = \"Exports data in generated plot as a Comma Separated Value file. Plot needs to be generated first to enable this button.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Exports data in generated plot as a Comma Separated Value file. Plot needs to be generated first to enable this button."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Exporta los datos incluidos en el gr\xC3\xA1fico generado en un archivo CSV (Comma Separated Value). Es necesario generar el gr\xC3\xA1fico primero para habilitar este bot\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exporta los datos incluidos en el gr\xC3\xA1fico generado en un archivo CSV (Comma Separated Value). Es necesario generar el gr\xC3\xA1fico primero para habilitar este bot\xC3\xB3n."
	#tag EndConstant

	#tag Constant, Name = kExportToNetCDF, Type = String, Dynamic = True, Default = \"Export to NetCDF", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Export to NetCDF"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Exportar a NetCDF"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exportar para NetCDF"
	#tag EndConstant

	#tag Constant, Name = kExportToNetCDFHelpTag, Type = String, Dynamic = True, Default = \"Exports data in generated plot as a NetCDF file. Plot needs to be generated first to enable this button.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Exports data in generated plot as a NetCDF file. Plot needs to be generated first to enable this button."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Exporta los datos inclu\xC3\xADdos en el gr\xC3\xA1fico generado en un archivo NetCDF. Es necesario generar el gr\xC3\xA1fico primero para habilitar este bot\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Exporta los datos inclu\xC3\xADdos en el gr\xC3\xA1fico generado en un archivo NetCDF. Es necesario generar el gr\xC3\xA1fico primero para habilitar este bot\xC3\xB3n."
	#tag EndConstant

	#tag Constant, Name = kFoundNMeasInRange, Type = String, Dynamic = True, Default = \"Found <n> meas. in range.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Found <n> meas. in range."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Existen <n> mediciones dentro del rango."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Encontrados 2 medi\xC3\xA7\xC3\xB5es na faixa."
	#tag EndConstant

	#tag Constant, Name = kFromBtnHelpTag, Type = String, Dynamic = True, Default = \"Use currently selected measurement as initial value for the time series.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Use currently selected measurement as initial value for the time series."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Utilice la medici\xC3\xB3n seleccionada como valor inicial en la serie de tiempo."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Utilice la medici\xC3\xB3n seleccionada como valor inicial en la serie de tiempo."
	#tag EndConstant

	#tag Constant, Name = kFromSwb, Type = String, Dynamic = True, Default = \" from SWB", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \" from SWB"
		#Tag Instance, Platform = Any, Language = es, Definition  = \" de boya sint."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \" de b\xC3\xB3ia sint."
	#tag EndConstant

	#tag Constant, Name = kGeneratePlot, Type = String, Dynamic = True, Default = \"Generate Plot", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Generate Plot"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Generar Gr\xC3\xA1fico"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Gerar Gr\xC3\xA1fico"
	#tag EndConstant

	#tag Constant, Name = kGeneratePlotHelpTag, Type = String, Dynamic = True, Default = \"Generates a time series plot as configured and displays it in a new window (pop ups may need to be enabled).", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Generates a time series plot as configured and displays it in a new window (pop ups may need to be enabled)."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Genera un gr\xC3\xA1fico de serie de tiempo de acuerdo a lo configurado y lo muestra en una ventana nueva (es probable que los pop ups deban esar habilitados)."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Genera un gr\xC3\xA1fico de serie de tiempo de acuerdo a lo configurado y lo muestra en una ventana nueva (es probable que los pop ups deban esar habilitados)."
	#tag EndConstant

	#tag Constant, Name = kGridInX, Type = String, Dynamic = True, Default = \"Grid in x", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Grid in x"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"X de cuadr\xC3\xADcula"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Grelha em x"
	#tag EndConstant

	#tag Constant, Name = kGridInY, Type = String, Dynamic = True, Default = \"Grid in y", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Grid in y"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Y de cuadr\xC3\xADcula"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Grelha em y"
	#tag EndConstant

	#tag Constant, Name = kLinesPlot, Type = String, Dynamic = True, Default = \"Line plot", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Line plot"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Gr\xC3\xA1fico de l\xC3\xADnea"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Gr\xC3\xA1fico de linha"
	#tag EndConstant

	#tag Constant, Name = kLowerYLimitHelpTag, Type = String, Dynamic = True, Default = \"Lower y-axis scale limit.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Lower y-axis scale limit."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"L\xC3\xADmite inferior en el eje y."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"L\xC3\xADmite inferior en el eje y."
	#tag EndConstant

	#tag Constant, Name = kMeasuredCurrentData, Type = String, Dynamic = True, Default = \"Measured current data", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Measured current data"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mediciones de corriente"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Medi\xC3\xA7\xC3\xB5es de corrente"
	#tag EndConstant

	#tag Constant, Name = kMeasuredRadialCurrent, Type = String, Dynamic = True, Default = \"Measured radial current\x2C ", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Measured radial current\x2C "
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mediciones de corriente radial\x2C "
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Medi\xC3\xA7\xC3\xB5es de corrente radiais\x2C "
	#tag EndConstant

	#tag Constant, Name = kMeasuredWaveData, Type = String, Dynamic = True, Default = \"Measured wave data", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Measured wave data"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mediciones de oleaje"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Medi\xC3\xA7\xC3\xB5es de onda"
	#tag EndConstant

	#tag Constant, Name = kMeasuredWindData, Type = String, Dynamic = True, Default = \"Measured wind data", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Measured wind data"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Mediciones de viento"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Medi\xC3\xA7\xC3\xB5es de vento"
	#tag EndConstant

	#tag Constant, Name = kNoMissing, Type = String, Dynamic = True, Default = \"# Missing", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"# with no value"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"# sin valor medido"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"# sem valor medido"
	#tag EndConstant

	#tag Constant, Name = kNoOfHours, Type = String, Dynamic = True, Default = \"# of hours", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"# of hours"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"# de horas"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"# de horas"
	#tag EndConstant

	#tag Constant, Name = kNoOfRecords, Type = String, Dynamic = True, Default = \"# of Records", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"# of records"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"# de mediciones"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"# de medi\xC3\xA7\xC3\xB5es"
	#tag EndConstant

	#tag Constant, Name = kNoRangeSelected, Type = String, Dynamic = True, Default = \"No range selected", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No range selected"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Rango no seleccionado"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"No intervalo selecionado"
	#tag EndConstant

	#tag Constant, Name = kNorthwardVelocity, Type = String, Dynamic = True, Default = \"Northward velocity", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Northward velocity"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Velocidad hacia el norte"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Velocidade para o norte"
	#tag EndConstant

	#tag Constant, Name = kPercentAvailable, Type = String, Dynamic = True, Default = \"% Available", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"% Available"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"% Disponible"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"% Dispon\xC3\xADvel"
	#tag EndConstant

	#tag Constant, Name = kPm2ndPlotHelpTag, Type = String, Dynamic = True, Default = \"Select data for 2nd plot. Only when \"Add 2nd plot\" checkbox is enabled.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select data for 2nd plot. Only when \"Add 2nd plot\" checkbox is enabled."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Seleccione la variable para el segundo gr\xC3\xA1fico. Solo cuando la casilla \"Incluir 2do gr\xC3\xA1fico\" est\xC3\xA9 habilitada."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Seleccione la variable para el segundo gr\xC3\xA1fico. Solo cuando la casilla \'Incluir 2do gr\xC3\xA1fico\' est\xC3\xA9 habilitada."
	#tag EndConstant

	#tag Constant, Name = kPm2ndPlotModeHelpTag, Type = String, Dynamic = True, Default = \"Select display mode for 2nd plot. Only when \"Add 2nd plot\" checkbox is enabled.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select display mode for 2nd plot. Only when \"Add 2nd plot\" checkbox is enabled."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Seleccione el modo de visualizaci\xC3\xB3n para el segundo gr\xC3\xA1fico. Solo cuando la casilla \"Incluir 2do gr\xC3\xA1fico\" est\xC3\xA9 habilitada."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Seleccione el modo de visualizaci\xC3\xB3n para el segundo gr\xC3\xA1fico. Solo cuando la casilla \'Incluir 2do gr\xC3\xA1fico\' est\xC3\xA9 habilitada."
	#tag EndConstant

	#tag Constant, Name = kPointCoordinates, Type = String, Dynamic = True, Default = \"Point coordinates to plot", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Point coordinates to plot"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Coordenadas de punto a graficar"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Ponto de coordenadas para tra\xC3\xA7ar"
	#tag EndConstant

	#tag Constant, Name = kPointsAndLines, Type = String, Dynamic = True, Default = \"Line with points", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Line with points"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"L\xC3\xADnea con puntos"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Linha com os pontos"
	#tag EndConstant

	#tag Constant, Name = kPointsPlot, Type = String, Dynamic = True, Default = \"Points plot", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Points plot"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Gr\xC3\xA1fica de puntos"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Gr\xC3\xA1fica de pontos"
	#tag EndConstant

	#tag Constant, Name = kQualFilterHelpTag, Type = String, Dynamic = True, Default = \"Select quality level to be included in time series.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select quality level to be included in time series."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Selecciona el nivel de calidad a incluir en la serie de tiempo."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Selecciona el nivel de calidad a incluir en la serie de tiempo."
	#tag EndConstant

	#tag Constant, Name = kRadarName, Type = String, Dynamic = True, Default = \"Radar system", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Radar system"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Sistema de radar"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Sistema de radar"
	#tag EndConstant

	#tag Constant, Name = kSampleInterval, Type = String, Dynamic = True, Default = \"Sample interval", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Sample interval"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Intervalo de muestreo"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Intervalo de amostragem"
	#tag EndConstant

	#tag Constant, Name = kSelectDataToPlot, Type = String, Dynamic = True, Default = \"Select variable to plot", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select variable to plot"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Selecciona variable a graficar"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Selecione a vari\xC3\xA1vel para tra\xC3\xA7ar"
	#tag EndConstant

	#tag Constant, Name = kSelectTimeRange, Type = String, Dynamic = True, Default = \"Select time range", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select time range"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Elije rango de tiempo"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Escolha intervalo"
	#tag EndConstant

	#tag Constant, Name = kSetYAxisScaleLimits, Type = String, Dynamic = True, Default = \"Set y-axis scale limits to:", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Set y-axis scale limits to:"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Fije de forma manual los limites del eje y:"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Definir limites do eixo y para:"
	#tag EndConstant

	#tag Constant, Name = kSetYAxisScaleLimitsHelpTag, Type = String, Dynamic = True, Default = \"Activate to avoid using automatic scaling in y-axis. Works on all variables except for direction data.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Activate to avoid using automatic scaling in y-axis. Works on all variables except for direction data."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Habilite para evitar escala autom\xC3\xA1tica en el eje y. Funciona con todas las variables excepto direcci\xC3\xB3n."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Habilite para evitar escala autom\xC3\xA1tica en el eje y. Funciona con todas las variables excepto direcci\xC3\xB3n."
	#tag EndConstant

	#tag Constant, Name = kStartTime, Type = String, Dynamic = True, Default = \"Start time", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Start time"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Tiempo inicial"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Tempo inicial"
	#tag EndConstant

	#tag Constant, Name = kStartTSHelpTag, Type = String, Dynamic = True, Default = \"Start of time series.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Start of time series."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Inicio de la serie de tiempo."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Inicio de la serie de tiempo."
	#tag EndConstant

	#tag Constant, Name = kStickPlot, Type = String, Dynamic = True, Default = \"Stick plot", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Stick plot"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Stick plot"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Stick plot"
	#tag EndConstant

	#tag Constant, Name = kSurfaceCurrentVelocity, Type = String, Dynamic = True, Default = \"Surface current velocity", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Surface current velocity"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Velocidad de corriente superficial"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Velocidad de corriente superficial"
	#tag EndConstant

	#tag Constant, Name = kSWBNameHelpTag, Type = String, Dynamic = True, Default = \"Currently selected synthetic wave buoy", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Currently selected synthetic wave buoy"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Boya sint\xC3\xA9tica seleccionada actualmente"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Boya sint\xC3\xA9tica seleccionada actualmente"
	#tag EndConstant

	#tag Constant, Name = kTimeSeriesNote, Type = String, Dynamic = True, Default = \"Click \"From\" and \"To\" buttons to select the start and end of the desired range to be plotted.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Click \"From\" and \"To\" buttons to select the start and end of the desired range to be plotted."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Presiona los botones \"Inicio\" y \"Final\" para seleccionar el inicio y fin del rango de tiempo que desee graficar."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Clique em \"De\" e \"At\xC3\xA9\" para selecionar o in\xC3\xADcio eo fim do intervalo desejado a ser tra\xC3\xA7ada."
	#tag EndConstant

	#tag Constant, Name = kToBtnHelpTag, Type = String, Dynamic = True, Default = \"Use currently selected measurement as final value for the time series.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Use currently selected measurement as final value for the time series."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Utilice la medici\xC3\xB3n seleccionada como valor final en la serie de tiempo."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Utilice la medici\xC3\xB3n seleccionada como valor final en la serie de tiempo."
	#tag EndConstant

	#tag Constant, Name = kTxtIxHelpTag, Type = String, Dynamic = True, Default = \"Index of grid cell in the horizontal direction of point to be plotted", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Index of grid cell in the horizontal direction of point to be plotted"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"N\xC3\xBAmero de celda de cuadr\xC3\xADcula a graficar\x2C contado de izquierda a derecha."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"N\xC3\xBAmero de celda de cuadr\xC3\xADcula a graficar\x2C contado de izquierda a derecha."
	#tag EndConstant

	#tag Constant, Name = kTxtIyHelpTag, Type = String, Dynamic = True, Default = \"Index of grid cell in the vertical direction of point to be plotted", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Index of grid cell in the vertical direction of point to be plotted"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"N\xC3\xBAmero de celda de cuadr\xC3\xADcula a graficar\x2C contado de arriba a abajo."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"N\xC3\xBAmero de celda de cuadr\xC3\xADcula a graficar\x2C contado de arriba a abajo."
	#tag EndConstant

	#tag Constant, Name = kTxtLatHelpTag, Type = String, Dynamic = True, Default = \"Latitude coordinate of point to be plotted", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Latitude coordinate of point to be plotted"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Latitud geogr\xC3\xA1fica del punto a graficar"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Latitud geogr\xC3\xA1fica del punto a graficar"
	#tag EndConstant

	#tag Constant, Name = kTxtLonHelpTag, Type = String, Dynamic = True, Default = \"Longitude coordinate of point to be plotted", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Longitude coordinate of point to be plotted"
		#Tag Instance, Platform = Any, Language = es, Definition  = \"Longitud geogr\xC3\xA1fica del punto a graficar"
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"Longitud geogr\xC3\xA1fica del punto a graficar"
	#tag EndConstant

	#tag Constant, Name = kUpperYLimitHelpTag, Type = String, Dynamic = True, Default = \"Upper y-axis scale limit.", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Upper y-axis scale limit."
		#Tag Instance, Platform = Any, Language = es, Definition  = \"L\xC3\xADmite superior en el eje y."
		#Tag Instance, Platform = Any, Language = pt, Definition  = \"L\xC3\xADmite superior en el eje y."
	#tag EndConstant


#tag EndWindowCode

#tag Events PlotImage
	#tag Event
		Sub Open()
		  
		  me.Width=675
		  me.Height=350
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events LstAvailMeas
	#tag Event
		Sub CellClick(Row As Integer, Column As Integer)
		  
		  
		  if Row=0 AND Left(me.List(Row),1)="(" then  // the '(' will simply indicate that the string '(No data available)' is there, which means that there is no photo to load
		    // do nothing
		  else
		    'SelectedMeas=Right(Str(DateAvailData(Row).Year),2) + "-" + Str(DateAvailData(Row).Month,"00#") + "-" + Str(DateAvailData(Row).Day,"00#") + " " + Str(DateAvailData(Row).Hour,"0#") + ":" + Str(DateAvailData(Row).Minute,"0#")
		    SelectedMeas=DateAvailData(Row).SQLDateTime
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events From
	#tag Event
		Sub Action()
		  if LstAvailMeas.ListIndex>=0 then
		    if LstAvailMeas.ListIndex=0 AND Left(LstAvailMeas.List(0),1)="(" then
		    else
		      LabelFrom.Text=SelectedMeas
		      
		      BtnNetCDF.Enabled=false
		      BtnNetCDF.Style=BtnCommonFontDisabled
		      BtnCSV.Enabled=false
		      BtnCSV.Style=BtnCommonFontDisabled
		      
		      UpdateLblMeasInRange()
		      return
		    end
		  end
		  
		  MsgBox(Main.kNoTimeIsSelected(cfgDV.language))
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events From1
	#tag Event
		Sub Action()
		  if LstAvailMeas.ListIndex>=0 then
		    if LstAvailMeas.ListIndex=0 AND Left(LstAvailMeas.List(0),1)="(" then
		    else
		      LabelTo.Text=SelectedMeas
		      
		      BtnNetCDF.Enabled=false
		      BtnNetCDF.Style=BtnCommonFontDisabled
		      BtnCSV.Enabled=false
		      BtnCSV.Style=BtnCommonFontDisabled
		      
		      UpdateLblMeasInRange()
		      return
		    end
		  end
		  
		  MsgBox(Main.kNoTimeIsSelected(cfgDV.language))
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnShowPlot
	#tag Event
		Sub Action()
		  dim i as double
		  
		  if LabelFrom.Text=Main.kNoDateSelected(cfgDV.language) then
		    MsgBox(kCantPlotWithoutAStartDate(cfgDV.language) )
		    Return
		  end
		  
		  if LabelTo.Text=Main.kNoDateSelected(cfgDV.language) then
		    MsgBox(kCantPlotWithoutAnEndDate(cfgDV.language))
		    Return
		  end
		  
		  
		  i=std.CalculateSeconds(LabelFrom.Text, LabelTo.Text)/86400   'Converting seconds in days
		  
		  if i<=0 then
		    MsgBox(Main.kStartDateMustBeBeforeEndDate(cfgDV.language))
		  elseif i>31 AND cfgDV.plotLimit then
		    MsgBox(kCannotCreatePlotOfMoreThanAMonth(cfgDV.language))
		  else
		    LoadPlotDialogTime.Show
		    
		    MySession=Session
		    Main.threadProcessRequired=1 // threadProcessRequired=1 means plot creation process
		    Main.ParallelThread.Run
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events WebCalendar
	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  Dim newDate as new Date
		  Dim Month as integer
		  newDate=WebCalendar.now
		  
		  Select Case Item.Text
		  Case "January"
		    Month=1
		  Case "February"
		    Month=2
		  Case "March"
		    Month=3
		  Case "April"
		    Month=4
		  Case "May"
		    Month=5
		  Case "June"
		    Month=6
		  Case "July"
		    Month=7
		  Case "August"
		    Month=8
		  Case "September"
		    Month=9
		  Case "October"
		    Month=10
		  Case "November"
		    Month=11
		  Case "December"
		    Month=12
		  end select
		  
		  WebCalendar.ScrollBar1.Value=(newDate.Year-1980)*12 + Month - 1
		  WebCalendar.ShowMonth
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  if Details.Button=Details.RightMouseButton then
		    me.PresentContextualMenu
		  else
		    'RefreshLstAvailMeas()  //commented because this is now programmed inside the calendar object. Check notes on MouseDown event of iDay() in WebCalendar object.
		  end
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  Dim menu As New WebMenuItem
		  
		  menu.Append(New WebMenuItem("January"))
		  menu.Append(New WebMenuItem("February"))
		  menu.Append(New WebMenuItem("March"))
		  menu.Append(New WebMenuItem("April"))
		  menu.Append(New WebMenuItem("May"))
		  menu.Append(New WebMenuItem("June"))
		  menu.Append(New WebMenuItem("July"))
		  menu.Append(New WebMenuItem("August"))
		  menu.Append(New WebMenuItem("September"))
		  menu.Append(New WebMenuItem("October"))
		  menu.Append(New WebMenuItem("November"))
		  menu.Append(New WebMenuItem("December"))
		  
		  Me.ContextualMenu = menu
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnNetCDF
	#tag Event
		Sub Action()
		  LoadNetcdf.Show
		  
		  MySession=Session
		  Main.threadProcessRequired=4 // threadProcessRequired=4 means NetCDF export process from a time serie
		  Main.ParallelThread.Run
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TxtLat
	#tag Event
		Sub TextChanged()
		  // First avoid StackOverflowException
		  if lat<0 then
		    if me.Text=(Str(abs(lat),"#.####") + " S") then
		      return
		    end
		  else
		    if me.Text=(Str(abs(lat),"#.####") + " N") then
		      return
		    end
		  end
		  
		  
		  Dim tmpText as String
		  tmpText=Trim(me.Text)
		  
		  if IsNumeric(tmpText) then
		    lat=Cdbl(tmpText)
		  elseif ( Right(tmpText,1)="S" OR Right(tmpText,1)="s" ) AND IsNumeric( Trim(Left(tmpText,len(tmpText)-1)) ) then
		    lat=-1*abs(Cdbl( Trim(Left(tmpText,len(tmpText)-2)) ))
		  elseif ( Right(tmpText,1)="N" OR Right(tmpText,1)="n" ) AND IsNumeric( Trim(Left(tmpText,len(tmpText)-1)) ) then
		    lat=abs(Cdbl( Trim(Left(tmpText,len(tmpText)-2)) ))
		  end
		  
		  if lat<App.grdmaxlat then
		    lat=App.grdmaxlat
		  elseif lat>App.grdminlat then
		    lat=App.grdminlat
		  end
		  
		  if lat<0 then
		    me.Text=Str(abs(lat),"#.####") + " S"
		  else
		    me.Text=Str(abs(lat),"#.####") + " N"
		  end if
		  
		  BtnNetCDF.Enabled=false
		  BtnNetCDF.Style=BtnCommonFontDisabled
		  BtnCSV.Enabled=false
		  BtnCSV.Style=BtnCommonFontDisabled
		  
		  UpdateGridVals()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TxtLon
	#tag Event
		Sub TextChanged()
		  // First avoid StackOverflowException
		  if lon<0 then
		    if me.Text=(Str(abs(lon),"#.####") + " W") then
		      return
		    end
		  else
		    if me.Text=(Str(abs(lon),"#.####") + " E") then
		      return
		    end
		  end
		  
		  
		  Dim tmpText as String
		  tmpText=Trim(me.Text)
		  
		  if IsNumeric(tmpText) then
		    lon=CDbl(tmpText)
		  elseif ( Right(tmpText,1)="W" OR Right(tmpText,1)="w" ) AND IsNumeric( Trim(Left(tmpText,len(tmpText)-1)) ) then
		    lon=-1*abs(Cdbl( Trim(Left(tmpText,len(tmpText)-2)) ))
		  elseif ( Right(tmpText,1)="e" OR Right(tmpText,1)="e" ) AND IsNumeric( Trim(Left(tmpText,len(tmpText)-1)) ) then
		    lon=abs(Cdbl( Trim(Left(tmpText,len(tmpText)-2)) ))
		  end
		  
		  if lon > App.grdmaxlon then
		    lon = App.grdmaxlon
		  elseif lon < App.grdminlon then
		    lon = App.grdminlon
		  end
		  
		  if lon<0 then
		    me.Text=Str(abs(lon),"#.####") + " W"
		  else
		    me.Text=Str(abs(lon),"#.####") + " E"
		  end
		  
		  BtnNetCDF.Enabled=false
		  BtnNetCDF.Style=BtnCommonFontDisabled
		  BtnCSV.Enabled=false
		  BtnCSV.Style=BtnCommonFontDisabled
		  
		  UpdateGridVals()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnCSV
	#tag Event
		Sub Action()
		  // We will use the same dialog as when generating a NetCDF since it is the same interface...
		  LoadNetcdf.Show
		  
		  MySession=Session
		  Main.threadProcessRequired=5 // threadProcessRequired=5 means CSV (Comma Separated Value) export process from a time serie
		  Main.ParallelThread.Run
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cb2ndplot
	#tag Event
		Sub ValueChanged()
		  if me.value and not(pmSymbols.text=kStickPlot(cfgDV.language)) then
		    pmSymbols2.Enabled=true
		    pmDataType2.Enabled=true
		    Label15.Enabled=true
		  elseif me.value and pmSymbols.text=kStickPlot(cfgDV.language) then
		    pmSymbols2.Enabled=false
		    pmDataType2.Enabled=false
		    Label15.Enabled=false
		  else
		    pmSymbols2.Enabled=false
		    pmDataType2.Enabled=false
		    Label15.Enabled=false
		  end
		  
		  
		  BtnNetCDF.Enabled=false
		  BtnNetCDF.Style=BtnCommonFontDisabled
		  BtnCSV.Enabled=false
		  BtnCSV.Style=BtnCommonFontDisabled
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmSymbols
	#tag Event
		Sub SelectionChanged()
		  if me.ListIndex=3 then
		    cb2ndplot.value=false  // to activate value change event
		    cb2ndplot.value=true
		    cb2ndplot.Enabled=false
		    
		  else
		    if cb2ndplot.Value then
		      cb2ndplot.Value=false  //to activate valuechanged event
		      cb2ndplot.Value=true
		    end
		    cb2ndplot.Enabled=true
		  end
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.DeleteAllRows
		  
		  me.AddRow(kPointsPlot(cfgDV.language))
		  me.AddRow(kLinesPlot(cfgDV.language))
		  me.AddRow(kPointsAndLines(cfgDV.language))
		  me.AddRow(kStickPlot(cfgDV.language))
		  
		  me.ListIndex=0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmSymbols2
	#tag Event
		Sub Open()
		  me.DeleteAllRows
		  
		  me.AddRow(kPointsPlot(cfgDV.language))
		  me.AddRow(kLinesPlot(cfgDV.language))
		  me.AddRow(kPointsAndLines(cfgDV.language))
		  
		  me.ListIndex=0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmDataType2
	#tag Event
		Sub SelectionChanged()
		  BtnNetCDF.Enabled=false
		  BtnNetCDF.Style=BtnCommonFontDisabled
		  BtnCSV.Enabled=false
		  BtnCSV.Style=BtnCommonFontDisabled
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TxtIy
	#tag Event
		Sub TextChanged()
		  if me.Text=CStr(iy) then
		    return
		  end
		  
		  me.Text=Trim(me.Text)
		  
		  if IsNumeric(me.Text) then
		    if CDbl(me.Text)<1 then
		      iy=1
		    elseif CDbl(me.Text)>App.grdny then
		      iy=App.grdny
		    else
		      iy=round(CDbl(me.Text))
		    end
		    BtnNetCDF.Enabled=false
		    BtnNetCDF.Style=BtnCommonFontDisabled
		    BtnCSV.Enabled=false
		    BtnCSV.Style=BtnCommonFontDisabled
		    if App.SyWaBuSeaViewEnabled then CheckIfIsBuoy
		  end
		  
		  me.Text=Cstr(iy)
		  
		  UpdateCoordVals()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TxtIx
	#tag Event
		Sub TextChanged()
		  if me.Text=CStr(ix) then
		    return
		  end
		  
		  me.Text=Trim(me.Text)
		  
		  if IsNumeric(me.Text) then
		    if CDbl(me.Text)<1 then
		      ix=1
		    elseif CDbl(me.Text)>App.grdnx then
		      ix=App.grdnx
		    else
		      ix=round(CDbl(me.Text))
		    end
		    BtnNetCDF.Enabled=false
		    BtnNetCDF.Style=BtnCommonFontDisabled
		    BtnCSV.Enabled=false
		    BtnCSV.Style=BtnCommonFontDisabled
		    if App.SyWaBuSeaViewEnabled then CheckIfIsBuoy
		  end
		  
		  me.Text=Cstr(ix)
		  
		  UpdateCoordVals()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BtnUseSWB
	#tag Event
		Sub Action()
		  
		  if svwb.nPts>0 then
		    if svwb.nPts>1 then
		      dim i as integer
		      for i=0 to svwb.nPts-1
		        if TxtIx.Text=CStr(svwb.gridX(i)) AND TxtIy.Text=CStr(svwb.gridY(i)) then // A SWB is currently selected, select the next one.
		          if i=svwb.nPts-1 then // the next one is OutOfBoundsException... select the first one.
		            TxtIx.Text=CStr(svwb.gridX(0))
		            TxtIy.Text=CStr(svwb.gridY(0))
		            return
		          else
		            TxtIx.Text=CStr(svwb.gridX(i+1))
		            TxtIy.Text=CStr(svwb.gridY(i+1))
		            return
		          end
		        end
		      next
		    end
		    TxtIx.Text=CStr(svwb.gridX(0))
		    TxtIy.Text=CStr(svwb.gridY(0))
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Label15
	#tag Event
		Sub Shown()
		  // It should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  me.Enabled=true
		  me.Enabled=false
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbFixScaleY
	#tag Event
		Sub ValueChanged()
		  
		  if me.Value then
		    fixScaleLow.Enabled=true
		    fixScaleHigh.Enabled=true
		  else
		    fixScaleLow.Enabled=false
		    fixScaleHigh.Enabled=false
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events fixScaleLow
	#tag Event
		Sub LostFocus()
		  
		  
		  if IsNumeric(me.Text) then me.Text=Cstr(CDbl(me.Text)) else me.Text="0"
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  me.Enabled=true
		  me.Enabled=false
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events fixScaleHigh
	#tag Event
		Sub LostFocus()
		  
		  
		  if IsNumeric(me.Text) then me.Text=Cstr(CDbl(me.Text)) else me.Text="1"
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  // It should be disabled from the beginning but somehow is not working (maybe because the control lies on a container that is never visible, and is visible after everything is initiated)... this is a workaround
		  me.Enabled=true
		  me.Enabled=false
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events QualFilter
	#tag Event
		Sub SelectionChanged()
		  BtnNetCDF.Enabled=false
		  BtnNetCDF.Style=BtnCommonFontDisabled
		  BtnCSV.Enabled=false
		  BtnCSV.Style=BtnCommonFontDisabled
		  
		  select case me.Text
		  case "Gaps filled"
		    qualFilterLevel=6
		  case cfgDV.labelQual4Filter
		    qualFilterLevel=4
		  case cfgDV.labelQual3Filter
		    qualFilterLevel=3
		  case cfgDV.labelQual2Filter
		    qualFilterLevel=2
		  case cfgDV.labelQual1Filter
		    qualFilterLevel=1
		  else
		    qualFilterLevel=0 'nothing will be filtered
		  end
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  
		  me.AddRow( cfgDV.labelQualAllFilter )
		  
		  if cfgDV.enableQ4 then
		    me.AddRow( cfgDV.labelQual4Filter )
		  end
		  
		  if cfgDV.enableAR then
		    me.AddRow( cfgDV.labelQual3Filter )
		  end
		  
		  if cfgDV.enableQ2 then
		    me.AddRow( cfgDV.labelQual2Filter )
		  end
		  
		  if cfgDV.enableQ1 then
		    me.AddRow( cfgDV.labelQual1Filter )
		  end
		  
		  if cfgDV.enableGF then
		    me.AddRow( "Gaps filled" )
		  end
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  me.ListIndex=0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmDataType
	#tag Event
		Sub SelectionChanged()
		  
		  // the flagToAvoidExceptionDueToBug is only because of a bug... the whole shown event from pmDataType is actually to avoid this bug.
		  if not(flagToAvoidExceptionDueToBug) then pmDataTypeSelectionChanged
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  
		  
		  Dim i as Integer
		  
		  me.DeleteAllRows
		  
		  if cfgDV.enableCCV then
		    me.AddRow(App.kNameCCVAbs(cfgDV.language))
		    me.AddRow(App.kNameCCVDir(cfgDV.language))
		    me.AddRow(App.kNameCCVE(cfgDV.language))
		    me.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVE(cfgDV.language)))
		    me.AddRow(App.kNameCCVN(cfgDV.language))
		    me.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVN(cfgDV.language)))
		  end
		  
		  for i=0 to cfgDV.nStations-1
		    if cfgDV.enableRC_st(i) then
		      me.AddRow(siteInfo.sname(i) + " - " + App.kNameRC(cfgDV.language))
		      me.AddRow(siteInfo.sname(i) + " - " + App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameRC(cfgDV.language)))
		    end
		  next
		  
		  if cfgDV.enableCSWH then
		    me.AddRow(App.kNameCSWHVal(cfgDV.language))
		    me.AddRow(App.kNameCSWHDir(cfgDV.language))
		  end
		  
		  if cfgDV.enableWD then
		    if cfgDV.enableWS then me.AddRow(App.kNameWS(cfgDV.language))
		    me.AddRow(App.kNameWD(cfgDV.language))
		  end
		  
		  me.ListIndex=0
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Shown()
		  //There is a strange bug in which this control, when is shown for the very first time (time series page is made visible in the browser for the first time), it will display always the the first index (usually absolute current velocity), even though ListIndex is set to somewhere else.
		  //
		  // According to the 4th post in http://forums.realsoftware.com/viewtopic.php?f=23&t=36845, this seems to happen because the initialization of the control was done prior to the control shown event. Well now it is displayed, let us initialize again...
		  
		  dim mem as Integer 'storing the current position
		  mem=me.ListIndex
		  
		  
		  //initialize the control again
		  Dim i as Integer
		  
		  //Deleting all rows will fire selection changed event, which will cause an error since there are no rows... I have implemented a workaround with flagToAvoidExceptionDueToBug which will avoid running the actual things to be run in the selection changed event.
		  flagToAvoidExceptionDueToBug=true
		  me.DeleteAllRows
		  flagToAvoidExceptionDueToBug=false
		  
		  if cfgDV.enableCCV then
		    me.AddRow(App.kNameCCVAbs(cfgDV.language))
		    me.AddRow(App.kNameCCVDir(cfgDV.language))
		    me.AddRow(App.kNameCCVE(cfgDV.language))
		    me.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVE(cfgDV.language)))
		    me.AddRow(App.kNameCCVN(cfgDV.language))
		    me.AddRow(App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameCCVN(cfgDV.language)))
		  end
		  
		  for i=0 to cfgDV.nStations-1
		    if cfgDV.enableRC_st(i) then
		      me.AddRow(siteInfo.sname(i) + " - " + App.kNameRC(cfgDV.language))
		      me.AddRow(siteInfo.sname(i) + " - " + App.kNameAccuracy(cfgDV.language) + lowercase(App.kNameRC(cfgDV.language)))
		    end
		  next
		  
		  if cfgDV.enableCSWH then
		    me.AddRow(App.kNameCSWHVal(cfgDV.language))
		    me.AddRow(App.kNameCSWHDir(cfgDV.language))
		  end
		  
		  if cfgDV.enableWD then
		    if cfgDV.enableWS then me.AddRow(App.kNameWS(cfgDV.language))
		    me.AddRow(App.kNameWD(cfgDV.language))
		  end
		  
		  
		  //Back to where it was
		  me.ListIndex=mem
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="cancelLoading"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Auto"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow North East"
			"12 - Arrow North West"
			"13 - Arrow South East"
			"14 - Arrow South West"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="dataTypeID"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="flagToAvoidExceptionDueToBug"
		Group="Behavior"
		InitialValue="false"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ix"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="iy"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="lat"
		Group="Behavior"
		Type="single"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="lon"
		Group="Behavior"
		Type="Single"
	#tag EndViewProperty
	#tag ViewProperty
		Name="minutesInBetween"
		Group="Behavior"
		Type="Single"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="qualFilterLevel"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="SelectedMeas"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="usingSynthBuoy"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
