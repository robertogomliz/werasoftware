#tag Module
Protected Module db
	#tag Method, Flags = &h0
		Function CheckDatabaseConnection() As Boolean
		  
		  //=================== Checking connection to database=====================
		  
		  if not (archive.connect) then
		    lastMessage_db="Could not connect to database!"
		    archive.Close
		    return false
		  end
		  
		  lastMessage_db="Connection successfull -> " + archive.DatabaseName
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetCurUVDataSpaceQual(idheader as integer, qualLevel as integer) As recordSet
		  // Returns the RecordSet that results of doing a data query to the custom values table for combined currents, filtering only those values with the desired quality.
		  // NOTE that the result is returned ordered by id_tb_ixiy in increasing order. This is required for some tools to work properly (e.g. dataviewer, UpdateCuvArtifacts, IdentifyArtifacts maybe).
		  // It basically returns the data of a time instant on the specified data type
		  
		  dim rs as RecordSet
		  dim sqlString as String
		  
		  // Measurements query request
		  sqlString="SELECT * FROM tb_custom_data_cuv WHERE id_tb_cuv_headers=" + Cstr(idheader) + " AND valType>" + CStr(5*(qualLevel-1)) + " AND valType<" + CStr(5*qualLevel+1) + " ORDER BY id_tb_ixiy, valType ASC"
		  
		  rs=archive.SQLSelect(sqlString)
		  if HandleDatabaseError("Error while querying custom cuv data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(rs)
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetGridData() As RecordSet
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  // Get grid size
		  sqlString="SELECT * FROM tb_grid WHERE id_tb_grid=" + grid
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if db.HandleDatabaseError("Error while retrieving grid data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetHeaderData(dataType as Integer, date1 as String, date2 as String, orderDesc as Boolean) As RecordSet
		  // Returns the RecordSet that results of doing a query to the data table on the type of data specified by dataType and constrained to the time range [date1,date2].
		  // If orderDesc is TRUE, the results are ordered from newer to older.
		  
		  dim rs as RecordSet
		  dim sqlString as String
		  dim orderstring as String
		  dim i as Integer
		  
		  if orderDesc then
		    orderstring="measTimeDB DESC"
		  else
		    orderstring="measTimeDB ASC"
		  end
		  
		  
		  // Check for measurement headers
		  if dataType=0 then
		    sqlString="SELECT * FROM tb_cuv_headers WHERE  id_tb_grid = " + grid + " AND measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "' ORDER BY " + orderstring
		  elseif dataType<10 then
		    for i=1 to sites
		      if dataType=i then
		        sqlString="SELECT * FROM tb_crad_header WHERE id_tb_grid=" + grid + " AND id_tb_stations=" + CStr(i) + " AND measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "' ORDER BY " + orderstring
		        exit
		      end
		    next
		  elseif dataType=10 then
		    sqlString="SELECT * FROM tb_wuv_headers WHERE  id_tb_grid = " + grid + " AND measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "' ORDER BY " + orderstring
		  elseif dataType<20 then
		    for i=11 to 10+sites
		      if dataType=i then
		        sqlString="SELECT * FROM tb_wrad_header" + EndOfLine + "WHERE id_tb_grid=" + grid + EndOfLine + "AND id_tb_stations=" + CStr(i-10) + EndOfLine + "AND measTimeDB>='" + date1 + "'" + EndOfLine + "AND measTimeDB<='" + date2 + "'" '+ EndOfLine + "ORDER BY " + orderstring
		        exit
		      end
		    next
		  elseif dataType=20 then
		    sqlString="SELECT * FROM tb_wuv_headers WHERE  id_tb_grid = " + grid + " AND measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "' ORDER BY " + orderstring
		  elseif dataType<30 then
		    //Wind direction from each station... not yet implemented!
		  End
		  
		  if sqlString="" then
		    log.Write(true,"Cannot query measurement header! Data type "+ CStr(dataType) + " not recognized!")
		  else
		    rs=archive.SQLSelect(sqlString)
		    if HandleDatabaseError("Error while querying measurement headers") then
		      'ct.MsgLog(true,db.lastMessage_db,true )
		    end
		  end
		  return(rs)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetHeaderDataFromHeaderID(dataType as Integer, head as String) As RecordSet
		  // Returns the RecordSet that results of doing a query to the header table on the type of data specified by dataType and constrained to the header head.
		  
		  dim rs as RecordSet
		  dim sqlString as String
		  dim i as integer
		  
		  // Check for measurement headers
		  if dataType=0 then
		    sqlString="SELECT * FROM tb_cuv_headers WHERE  id_tb_grid = " + grid + " AND id_tb_cuv_headers=" + head
		  elseif dataType<10 then
		    for i=1 to sites
		      if dataType=i then
		        sqlString="SELECT * FROM tb_crad_header WHERE id_tb_grid=" + grid + " AND id_tb_crad_header=" + head
		        exit
		      end
		    next
		  elseif dataType=10 then
		    sqlString="SELECT * FROM tb_wuv_headers WHERE  id_tb_grid = " + grid + " AND id_tb_wuv_headers=" + head
		  elseif dataType<20 then
		    for i=11 to 10+sites
		      if dataType=i then
		        sqlString="SELECT * FROM tb_wrad_header WHERE id_tb_grid=" + grid + " AND id_tb_wrad_header=" + head
		        exit
		      end
		    next
		  elseif dataType=20 then
		    sqlString="SELECT * FROM tb_wuv_headers WHERE  id_tb_grid = " + grid + " AND id_tb_wuv_headers=" + head
		  elseif dataType<30 then
		    //Wind direction from each station... not yet implemented!
		  End
		  
		  if sqlString="" then
		    log.Write(true,"Cannot query measurement header! Data type "+ CStr(dataType) + " not recognized!")
		  else
		    rs=archive.SQLSelect(sqlString)
		    if HandleDatabaseError("Error while querying measurement headers") then
		      'ct.MsgLog(true,db.lastMessage_db,true )
		    end
		  end
		  return(rs)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetHeaderWithData(dataType as Integer, date1 as String, date2 as String, orderDesc as Boolean) As RecordSet
		  // Returns the RecordSet that results of doing a query to the data table on the type of data specified by dataType and constrained to the time range [date1,date2].
		  // If orderDesc is TRUE, the results are ordered from newer to older.
		  
		  dim rs as RecordSet
		  dim sqlString as String
		  dim orderstring as String
		  dim i as Integer
		  
		  if orderDesc then
		    orderstring="measTimeDB DESC"
		  else
		    orderstring="measTimeDB ASC"
		  end
		  
		  
		  // Check for measurement headers
		  if dataType=0 then
		    sqlString="SELECT * FROM tb_cuv_headers WHERE dataAvail=true AND id_tb_grid = " + grid + " AND measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "' ORDER BY " + orderstring
		  elseif dataType<10 then
		    for i=1 to sites
		      if dataType=i then
		        sqlString="SELECT * FROM tb_crad_header WHERE dataAvail=true AND id_tb_grid=" + grid + " AND id_tb_stations=" + CStr(i) + " AND measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "' ORDER BY " + orderstring
		        exit
		      end
		    next
		  elseif dataType=10 then
		    sqlString="SELECT * FROM tb_wuv_headers WHERE dataAvailWa=true AND id_tb_grid = " + grid + " AND measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "' ORDER BY " + orderstring
		  elseif dataType<20 then
		    for i=11 to 10+sites
		      if dataType=i then
		        sqlString="SELECT * FROM tb_wrad_header" + EndOfLine + "WHERE dataAvailWa=true AND id_tb_grid=" + grid + EndOfLine + "AND id_tb_stations=" + CStr(i-10) + EndOfLine + "AND measTimeDB>='" + date1 + "'" + EndOfLine + "AND measTimeDB<='" + date2 + "'" '+ EndOfLine + "ORDER BY " + orderstring
		        exit
		      end
		    next
		  elseif dataType=20 then
		    sqlString="SELECT * FROM tb_wuv_headers WHERE dataAvailWi=true AND id_tb_grid = " + grid + " AND measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "' ORDER BY " + orderstring
		  elseif dataType<30 then
		    //Wind direction from each station... not yet implemented!
		  End
		  
		  if sqlString="" then
		    log.Write(true,"Cannot query measurement header with data! Data type "+ CStr(dataType) + " not recognized!")
		  else
		    rs=archive.SQLSelect(sqlString)
		    if HandleDatabaseError("Error while querying measurement headers") then
		      'ct.MsgLog(true,db.lastMessage_db,true )
		    end
		  end
		  return(rs)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIdIxIyFromGridID(mode as Integer) As RecordSet
		  //Reads data from table tb_ixiy, depending on the mode selected. The data returned is given in ascending order according to id_tb_ixiy.
		  //
		  // mode=0    Returns id_tb_ixiy, ix and iy of all points in the grid
		  // mode=1    Returns id_tb_ixiy, ix and iy of all points in the grid where watt=FALSE (No land)
		  // mode=2    Returns all columns of all points in the grid.
		  
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  
		  if mode=1 then
		    sqlString="SELECT id_tb_ixiy, ix, iy FROM tb_ixiy WHERE id_tb_grid=" + grid + " AND watt=FALSE ORDER BY id_tb_ixiy ASC"    // in case we want to avoid watt points (sometimes there are measurements on land points and may create troubles)
		  elseif mode=0 then
		    sqlString="SELECT id_tb_ixiy, ix, iy FROM tb_ixiy WHERE id_tb_grid=" + grid + " ORDER BY id_tb_ixiy ASC"
		  else
		    sqlString="SELECT * FROM tb_ixiy WHERE id_tb_grid=" + grid + " ORDER BY id_tb_ixiy ASC"
		  end
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  
		  if db.HandleDatabaseError("Error while retrieving grid points data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return sqlRecordSet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetLastMeasured(dataType as Integer, mode as integer) As RecordSet
		  // Mode input parameter is to include conditions :
		  //     0= no condition, get just the latest header
		  //     1= with dataAvail=true
		  //     2= with dataQual=true
		  //     3= with dataGapFille=true
		  
		  dim rs as RecordSet
		  dim sqlString, condition as string
		  
		  if mode<>0 then
		    select case mode
		    case 1
		      condition="dataAvail"
		    case 2
		      condition="dataQual"
		    end
		    
		    if dataType>=10 and dataType<20 then condition=condition+"Wa"
		    if dataType=20 then condition=condition+"Wi"
		    condition=condition+"=true AND "
		  end
		  
		  // Get last map and display it
		  if dataType=0 then
		    sqlString="SELECT measTimeDB, id_tb_cuv_headers, measQual FROM tb_cuv_headers WHERE " + condition + "id_tb_grid = " + grid + " ORDER BY measTimeDB DESC LIMIT 1"
		  elseif dataType<=sites then
		    sqlstring="SELECT measTimeDB, id_tb_crad_header, measQual FROM tb_crad_header WHERE " + condition + "id_tb_grid=" + grid + " AND id_tb_stations=" + CStr(dataType) + " ORDER BY measTimeDB DESC LIMIT 1"
		  elseif dataType= 10 then
		    sqlString="SELECT measTimeDB, id_tb_wuv_headers, measQualWa FROM tb_wuv_headers WHERE " + condition + "id_tb_grid = " + grid + " ORDER BY measTimeDB DESC LIMIT 1"
		  elseif dataType<=(10+sites) then
		    sqlstring="SELECT measTimeDB, id_tb_wrad_header, measQualWa FROM tb_wrad_header WHERE " + condition + "id_tb_grid=" + grid + " AND id_tb_stations=" + CStr(dataType-10) + " ORDER BY measTimeDB DESC LIMIT 1"
		  elseif dataType= 20 then
		    sqlString="SELECT measTimeDB, id_tb_wuv_headers FROM tb_wuv_headers WHERE " + condition + "id_tb_grid = " + grid + " ORDER BY measTimeDB DESC LIMIT 1"
		    'sqlString="SELECT measTimeDB, id_tb_wuv_headers, measQualWi FROM tb_wuv_headers WHERE " + condition + "id_tb_grid = " + grid + " ORDER BY measTimeDB DESC LIMIT 1"
		  end
		  
		  rs=archive.SQLSelect(sqlString)
		  
		  if HandleDatabaseError("Cannot query for last measurement using data type " + CStr(dataType)) then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return rs
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetLastSVWaBuData(id_tb_ixiy as string) As RecordSet
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  // Get last Wave Buoy data from specified point
		  sqlString="SELECT * FROM tb_svwabu  WHERE id_tb_ixiy= " + id_tb_ixiy + " ORDER BY measTimeDB DESC LIMIT 1"
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if db.HandleDatabaseError("Error while retrieving last SeaView's wave buoy data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetLastSVWaBuDataFromAll() As RecordSet
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  // Get last Wave Buoy data from specified point
		  sqlString="SELECT * FROM tb_svwabu ORDER BY measTimeDB DESC LIMIT 1"
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if db.HandleDatabaseError("Error while retrieving last SeaView's wave buoy data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetMeasDataFromPtIdHeadID(dataType as integer, idheader as integer, idixiy as string, qualLevel as integer) As RecordSet
		  //Returns the measured values of a single point in the grid (given by idixiy), for a specific type of data (dataType) at a specific time instant (idheader).
		  //Additionally it is possible to filter the type of data to a certain quality level. Use 0 to skip using a quality filter
		  
		  Dim sqlString as String
		  Dim rs as RecordSet
		  Dim qualFilterString as string = ""
		  
		  if qualLevel<>0 then
		    qualFilterString=" AND qual>0 AND qual<=" + CStr(qualLevel)
		  end
		  
		  // 'get measurement data from a point from a specific type of measurement in a specific time
		  Select Case dataType
		  case 0
		    // Quality for combined currents is a special case.
		    if qualLevel<>0 then
		      sqlString="SELECT velu, velv, accu, accv, kl FROM tb_cuv_data WHERE id_tb_ixiy=" + idixiy + " AND id_tb_cuv_headers=" + Cstr(idheader)
		      rs=archive.SQLSelect(sqlString)
		      if HandleDatabaseError("Error while retrieving point measurement data!") then
		        'ct.MsgLog(true,db.lastMessage_db,true )
		      end
		      
		      if rs.BOF then return(rs)  //No measurement at all. Due to this we can also be sure that there is no replacement record on table tb_custom_data_cuv. So let us just return the empty RecordSet
		      
		      // We found a measurement... Maybe there is a replacement?
		      dim rs2 as RecordSet
		      sqlString="SELECT val, valType FROM tb_custom_data_cuv WHERE id_tb_ixiy=" + idixiy + " AND id_tb_cuv_headers=" + Cstr(idheader) + " AND valType>" + CStr(5*(qualLevel-1)) + " AND valType<" + CStr(5*qualLevel+1)
		      rs2=archive.SQLSelect(sqlString)
		      if HandleDatabaseError("Error while retrieving point measurement data!") then
		        'ct.MsgLog(true,db.lastMessage_db,true )
		      end
		      if rs2.BOF then 'There is no replacement
		        
		        'If quality is enough then return the measurement, if not then return the empty RecordSet, which means that there was nothing found.
		        dim qualComb as integer
		        qualComb=std.DecodeKlCuv2Qual(rs.Field("kl").IntegerValue)
		        if qualComb > 0 AND qualComb <=qualLevel then return(rs) else return(rs2)
		        
		      else 'There is a replacement value stored, return this value.
		        return (rs2)
		      end
		    else
		      sqlString="SELECT velu, velv, accu, accv, kl FROM tb_cuv_data WHERE id_tb_ixiy=" + idixiy + " AND id_tb_cuv_headers=" + Cstr(idheader)
		    end
		  case 10
		    sqlString="SELECT hwave, dir, qual FROM tb_wauv_data WHERE id_tb_ixiy=" + idixiy + " AND id_tb_wuv_headers=" + Cstr(idheader) + qualFilterString
		  case 20
		    sqlString="SELECT speed, dir, qual FROM tb_wiuv_data WHERE id_tb_ixiy=" + idixiy + " AND id_tb_wuv_headers=" + Cstr(idheader) + qualFilterString
		  case else
		    sqlString="SELECT vel, acc, var, pow, qual FROM tb_crad_data WHERE id_tb_ixiy=" + idixiy + " AND id_tb_crad_header=" + Cstr(idheader) + qualFilterString
		  End select
		  
		  rs=archive.SQLSelect(sqlString)
		  if HandleDatabaseError("Error while retrieving point measurement data!") then
		  end
		  
		  return(rs)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetMeasurementDataSpace(idheader as integer, dataType as integer) As recordSet
		  // Returns the RecordSet that results of doing a data query to the corresponding table of the type of data specified by dataType that belong the measurement specified by idheader.
		  // NOTE that the result is returned ordered by id_tb_ixiy in increasing order. This is required for some tools to work properly (e.g. dataviewer, UpdateCuvArtifacts, IdentifyArtifacts maybe).
		  // It basically returns the data of a time instant on the specified data type
		  
		  dim rs as RecordSet
		  dim sqlString as String
		  
		  // Measurements query request
		  
		  if dataType  =0 then
		    sqlString="SELECT id_tb_ixiy, velu, velv, accu, accv, kl FROM tb_cuv_data WHERE id_tb_cuv_headers=" + CStr(idheader) + " ORDER BY id_tb_ixiy ASC"
		  elseif dataType  <=sites then
		    sqlString="SELECT id_tb_ixiy, vel, acc, var, pow, qual FROM tb_crad_data WHERE id_tb_crad_header=" + CStr(idheader) + " ORDER BY id_tb_ixiy ASC"
		  elseif dataType  =10 then
		    sqlString="SELECT id_tb_ixiy, hwave, dir, qual FROM tb_wauv_data WHERE id_tb_wuv_headers=" + CStr(idheader) + " ORDER BY id_tb_ixiy ASC"
		  elseif dataType  <=10+sites then
		    // Significant Wave Height from each station... not yet implemented!
		  elseif dataType  =20 then
		    sqlString="SELECT id_tb_ixiy, speed, dir, qual FROM tb_wiuv_data WHERE id_tb_wuv_headers=" + CStr(idheader) + " ORDER BY id_tb_ixiy ASC"
		  elseif dataType   <=20+sites then
		    //Wind direction from each station... not yet implemented!
		  End
		  
		  if sqlString="" then
		    log.Write(true,"Cannot query measurement data! Data type "+ CStr(dataType ) + " not recognized!")
		  else
		    rs=archive.SQLSelect(sqlString)
		    if HandleDatabaseError("Error while querying measured data!") then
		      'ct.MsgLog(true,db.lastMessage_db,true )
		    end
		  end
		  return(rs)
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetPtDataFromIxIy(ix as Integer, iy as Integer) As RecordSet
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  // Get grid size
		  sqlString="SELECT id_tb_ixiy, lon_grd, lat_grd, watt, topo FROM tb_ixiy WHERE id_tb_grid=" + grid + " AND ix=" + CStr(ix) + " AND iy=" + CStr(iy)
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if db.HandleDatabaseError("Error while retrieving grid point data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetPtInfoFromApproxCoords(lat1 as Double, lat2 as Double, lon1 as Double, lon2 as Double) As RecordSet
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  // 'get id_tb_ixiy for point of interest
		  sqlString="SELECT id_tb_ixiy, lat_grd, lon_grd, watt, ix, iy FROM tb_ixiy WHERE id_tb_grid=" + grid + " AND lat_grd>" + CStr(lat1) + " AND lat_grd<" + CStr(lat2) + " AND lon_grd>" + CStr(lon1) + " AND lon_grd<" + CStr(lon2)
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if HandleDatabaseError("Error while retrieving approx. grid point data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetPtInfoFromPtID(idixiy as String) As RecordSet
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  // 'get point information from point id
		  sqlString="SELECT lat_grd, lon_grd, watt, ix, iy FROM tb_ixiy WHERE id_tb_ixiy=" + idixiy + " AND id_tb_grid=" + grid
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if HandleDatabaseError("Error while retrieving grid point data from id number!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetStationData(st as String) As RecordSet
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  //Get data of station identified by 'st'
		  
		  if st.Len=1 AND IsNumeric(st) then
		    sqlString="SELECT * FROM tb_stations WHERE id_tb_stations=" + st
		  else
		    sqlString="SELECT * FROM tb_stations WHERE sname='" + st + "' OR acronym='" + st + "' OR longname='" + st + "'"
		  end if
		  
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if HandleDatabaseError("Error while retrieving station data out of '" + st + "'!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetSVWaBuData(measTimeDB as String, id_tb_ixiy as string) As RecordSet
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  // Get Wave Buoy data from specified data
		  sqlString="SELECT * FROM tb_svwabu WHERE measTimeDB='" + measTimeDB + "' AND id_tb_ixiy=" + id_tb_ixiy
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if db.HandleDatabaseError("Error while retrieving SeaView's wave buoy data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetSVWaBuDataFromID(id as String, qualLevel as integer) As RecordSet
		  
		  //Returns SWB measured values of a single point in the grid (given by idixiy)
		  //Additionally it is possible to filter the type of data to a certain quality level. Use 0 to skip using a quality filter
		  
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  Dim qualFilterString as string = ""
		  
		  if qualLevel<>0 then
		    qualFilterString=" AND (qual MOD 10) <=" + CStr(qualLevel)
		  end
		  
		  // Get Wave Buoy data from specified id
		  sqlString="SELECT * FROM tb_svwabu WHERE id_tb_svwabu=" + id + qualFilterString
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if db.HandleDatabaseError("Error while retrieving SeaView's wave buoy data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetSVWaBuDataRange(measTimeDB1 as String, measTimeDB2 as String, id_tb_ixiy as string) As RecordSet
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  // Get Wave Buoy data from specified data
		  sqlString="SELECT * FROM tb_svwabu WHERE measTimeDB>='" + measTimeDB1 + "' AND measTimeDB<='" + measTimeDB2 + "' AND id_tb_ixiy=" + id_tb_ixiy + " ORDER BY measTimeDB ASC"
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if db.HandleDatabaseError("Error while retrieving SeaView's wave buoy data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetSywabData(measTimeDB as String, id_tb_ixiy as string) As RecordSet
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  // Get Wave Buoy data from specified data
		  sqlString="SELECT * FROM tb_sywab WHERE measTimeDB='" + measTimeDB + "' AND id_tb_ixiy=" + id_tb_ixiy
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if db.HandleDatabaseError("Error while retrieving Actimar's wave buoy data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetTimeSeries()
		  // Something like:
		  
		  //SELECT measTimeDB, vel, acc, var, pow, qual
		  //FROM tb_crad_header INNER JOIN tb_crad_data
		  //ON tb_crad_header.id_tb_crad_header=tb_crad_data.id_tb_crad_header
		  //WHERE id_tb_ixiy=23639 AND id_tb_stations=2 AND measTimeDB>='2013-05-01 00:00:00' AND measTimeDB<='2014-01-31 23:59:00'
		  //ORDER BY measTimeDB ASC
		  
		  // but not yet implemented
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetWaveMeanDirection(ixiy as string, header as Integer) As RecordSet
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  // Get Wave mean direction
		  sqlString="SELECT * FROM tb_wauv_dir WHERE id_tb_ixiy=" + ixiy + " AND id_tb_wuv_headers= " + CStr(header)
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if db.HandleDatabaseError("Error while retrieving wave mean direction data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetWaveSpectralDensity(ixiy as string, header as Integer) As RecordSet
		  Dim sqlString as String
		  Dim sqlRecordSet as RecordSet
		  
		  // Get Wave spectral density
		  sqlString="SELECT * FROM tb_wauv_height WHERE id_tb_ixiy=" + ixiy + " AND id_tb_wuv_headers= " + CStr(header)
		  sqlRecordSet=archive.SQLSelect(sqlString)
		  if db.HandleDatabaseError("Error while retrieving wave spectral density data!") then
		    'ct.MsgLog(true,db.lastMessage_db,true )
		  end
		  
		  return(sqlRecordSet)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function HandleDatabaseError(msg as String) As Boolean
		  if archive.error then
		    lastMessage_db="Error code  " + Str(archive.ErrorCode) + ": " + archive.ErrorMessage
		    #if TargetWeb then  'Is a web page applications... e.g. DataViewer
		      log.Write(false,msg + " " + lastMessage_db )
		    #Else
		      #if TargetDesktop then  'Is a desktop applicaitons... e.g. DataManager
		        log.Write(true,msg + " " + lastMessage_db)
		      #Else   'Is a console applications...
		        ct.MsgLog(true,msg,false )
		      #Endif
		    #endif
		    return true
		  else
		    return false
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ReadDBSetupFile(path as string) As Boolean
		  // Reads the DB connection parameters from the file given in path input. If the input is empty (or just one space " ") the parameters will be taken from the same path as executable from a file DataArchive_setup.txt.
		  dim f as new FolderItem
		  dim SourceStream as TextInputStream
		  dim result as Boolean
		  
		  if path.Len>3 then
		    f=GetFolderItem(path)
		  else //empty string... used when loading DB parameters from console apps in DataArchivePrograms folder.
		    f=GetFolderItem(App.execDir + "DataArchive_setup.txt")  // Relative path ist besser
		  end
		  
		  if f = Nil then
		    lastMessage_db="Invalid pathname for database setup file!"
		    result=false
		  elseif not(f.Exists) then
		    lastMessage_db="Database setup file does not exists!"
		    result=false
		  else
		    SourceStream=TextInputStream.Open(f)
		    if SourceStream = Nil then
		      lastMessage_db="Error opening database setup file!"
		      result=false
		    else
		      archive.DatabaseName = SourceStream.ReadLine
		      archive.Host = SourceStream.ReadLine
		      archive.Username = SourceStream.ReadLine
		      archive.Password = SourceStream.ReadLine
		      SourceStream.Close
		      lastMessage_db="DB parameters loaded from " + f.AbsolutePath
		      return true
		    end
		  end
		  
		  log.Write(true,"DB setup file not reached! Loading standard configuration.")
		  //================ Database default config =================
		  archive.DatabaseName = "data-archive"
		  archive.Host = "127.0.0.1"
		  archive.Username = "root"
		  archive.Password = "ros2003"
		  
		  return result
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SetGridAutomatically(dataType as Integer, date1 as String, date2 as String, avail as Boolean) As Boolean
		  
		  // It uses the dates given as input to find out which are the grid number for which there is data available in the date range given. If more than one grid is found, it returns false, otherwise, it will set the 'grid' to the grid found.
		  //Optionally, it can be set to look only for headers where data is available using the input avail=true
		  
		  
		  dim rs as RecordSet
		  dim sqlString as String
		  dim i as Integer
		  
		  if avail then
		    if dataType=20 then
		      sqlString=" AND dataAvailWi=true"
		    elseif dataType>=10 then
		      sqlString=" AND dataAvailWa=true"
		    else
		      sqlString=" AND dataAvail=true"
		    end
		  end if
		  
		  // Check for measurement headers
		  if dataType=0 then
		    sqlString="SELECT MAX(id_tb_grid) - MIN(id_tb_grid) FROM tb_cuv_headers WHERE measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "'" + sqlString
		  elseif dataType<10 then
		    for i=1 to sites
		      if dataType=i then
		        sqlString="SELECT MAX(id_tb_grid) - MIN(id_tb_grid) FROM tb_crad_header WHERE measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "' AND id_tb_stations=" + CStr(i)+ sqlString
		        exit
		      end
		    next
		  elseif dataType=10 then
		    sqlString="SELECT MAX(id_tb_grid) - MIN(id_tb_grid) FROM tb_wuv_headers WHERE measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "'"+ sqlString
		  elseif dataType<20 then
		    for i=11 to 10+sites
		      if dataType=i then
		        sqlString="SELECT MAX(id_tb_grid) - MIN(id_tb_grid) FROM tb_wrad_header" + EndOfLine + "WHERE measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "' AND id_tb_stations=" + CStr(i)+ sqlString
		        exit
		      end
		    next
		  elseif dataType=20 then
		    sqlString="SELECT MAX(id_tb_grid) - MIN(id_tb_grid) FROM tb_wuv_headers WHERE measTimeDB>='" + date1 + "' AND measTimeDB<= '" + date2 + "'"+ sqlString
		  End
		  
		  if sqlString="" then
		    log.Write(true,"Cannot set grid for data type "+ CStr(dataType) + ". Data type not recognized!")
		    return false
		  else
		    rs=archive.SQLSelect(sqlString)
		    if HandleDatabaseError("Error during query to set grid automatically.") then
		      'ct.MsgLog(true,db.lastMessage_db,true )
		      return false
		    end
		    
		    
		    'EOF is not the way to check if no answer came, because MAX()-MIN() will always return.
		    'In this case, it return Nil... which can be detected by finding an empty string...
		    'if rs.EOF then
		    if rs.Field("MAX(id_tb_grid) - MIN(id_tb_grid)").StringValue = "" then
		      lastMessage_db="No data found within time range given! Could not set grid automatically."
		      return false
		    end
		    
		    if rs.Field("MAX(id_tb_grid) - MIN(id_tb_grid)").IntegerValue<>0 then
		      lastMessage_db="More than 1 grid ID was found in time range given! Could not automatically set the grid."
		      return false
		    end
		    
		    sqlString=replace(sqlString,"Max(id_tb_grid) - MIN(id_tb_grid)", "id_tb_grid") + " LIMIT 1"
		    rs=archive.SQLSelect(sqlString)
		    if HandleDatabaseError("Error during query to set grid automatically.") then return false
		    
		    grid=rs.Field("id_tb_grid").StringValue
		    lastMessage_db="Grid ID was automatically set to " + grid
		    return true
		    
		  end
		  
		End Function
	#tag EndMethod


	#tag Note, Name = Updates
		
		
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new updates in the next line, not at the end!!! latest update info should ALWAYS be displayed first!!!
		
		2015.07.14
		Roberto Gomez
		-Updated method SetGridAutomatically to correctly detect when no measurement is found within time range.
		-Updated method SetGridAutomatically to correctly set the lastMessage_db property in all cases.
		
		2015.07.01
		Roberto Gomez / Marek Swirgon
		-Found ocurrences of ct.msglog on each of the GetSomthing methods... This is assuming that all programs that runthis methods are 
		  console tools (which is not the case of the DataViewer for example). These ct.MsgLog are now commented to avoid errors on DV.
		  A more elegant solution should be implemented to output and maybe log the lastMessage_db contents on console tools.
		
		2015.01.22
		Roberto Gomez
		-Changed property lastMessage to lastMessage_db to avoid compilation error in Xojo
		
		2014.11.07
		Roberto Gomez
		-Logging error messages in HandleDatabaseError method is using now conditional compilation.
		
		2014.09.10
		Roberto Gomez
		-Added lastMessage property.
		-Changed name of methods ReadDBParams and CheckDatabase to ReadDBSetupFile and CheckDatabaseConnection, and implemented lastMessage property in them. This change allows better handling of errors within different types of applications (console, web, desktop).
		
		2014.08.12
		Roberto Gomez
		-Added GetHeaderDataFromHeaderID method
		
		2014.08.06
		Roberto Gomez
		-Added SetGridAutomatically method
		
		2014.07.28
		Roberto Gomez
		-Added GetSywabData method
		
		2014.07.08
		Roberto Gomez
		-Modified db.ReadDBParams and db.CheckDatabase so that in console applications (and as long as no error occurs) there is nothing logged into file.
		
		???
		Roberto Gomez
		-Changed from log.Write(true, "Connection successfull -> " + archive.DatabaseName)   ------>to------> log.Write(false, "Connection successfull -> " + archive.DatabaseName)
		
		2014.02.12
		Roberto Gomez
		-Modified GetSVWaBuDataFromID by adding another parameter to retrict the quality of data returned.
		
		2014.02.06
		Roberto Gomez
		-Modified GetLastMeasured to no return measQualWi field anymore... all databases have measQuallWi (two times the 'l'). So I better disable it... i do not use it anyway.
		
		2014.02.05
		Roberto Gomez
		-Modified GetLastMeasured to also return measQual[Wa,Wi] field.
		
		2013.12.09
		Roberto Gomez
		-Modified GetLastMeasured to optionally return only headers with dataAvail=true.
		
		2013.12.08
		Roberto Gomez
		-Added method GetHeaderWithData, to only return headers with dataAvail=true.
		
		2013.12.06
		Roberto Gomez
		-Input 'mode' from method GetMeasDataFromPtIdHeadID was modified to be a quality filter.
		-Added method GetCurUVDataSpaceQual to get all replacement values stored in tb_custom_data_cuv at a specific time from a specific quality value.
		
		2013.11.01
		Roberto Gomez
		-Method GetHeaderData return all columns available (SELECT *) instead of only some.
		
		2013.10.09
		Roberto Gomez
		-Added method GetSVWaBuDataRange to specify a time range.
		-Added method GetSVWaBuDataFromID to get a specific measurement via Id number.
		
		2013.09.13
		Roberto Gomez
		-Added method GetSVWaBuData to get wave and wind parameters from the SeaView's synthetic wave buoy.
		
		2013.08.20
		Roberto Gomez
		-Added an input parameter 'mode' in GetMeasDataFromPtIdHeadID method to indicate if raw measured data is required, or artifact removed (specially useful for combined current data).
		-Added method GetPtInfoFromPtID to get information of grid point by giving the point identifier number  (id_tb_ixiy).
		
		2013.08.19
		Roberto Gomez
		-Changed GetMeasurementDataSpace method input parameter from LstIdx to dataType so that it is easier to understand.
		-Added method GetPtInfoFromPtID to get information of grid point by giving the point identifier number  (id_tb_ixiy).
		
		2013.08.15
		Roberto Gomez
		-Changed GetMeasurementDataSpace method to also return quality data.
		
		2013.08.14
		Roberto Gomez
		-Changed CheckDatabase method, since TargetConsole was also true not only for console, but also for Web applications. Now using if TargetConsole AND not(TargetWeb) then...
		
		2013.08.5
		Roberto Gomez
		-Changed GetLastMeasured method to also give information about radial waves (implemented due to the neccesity of knowing the last measured .wrad file for home page).
		
		2013.07.31
		Roberto Gomez
		-Modified methods GetLastMeasured and GetHeaderData to not use JOIN queries anymore, since structure of database is updated to have id_tb_grid on tb_cuv_headers and tb_wuv_headers tables
		
		2013.07.15
		Roberto Gomez
		-Added ReadDBParams to comply with using this module for console applications
		-Added method GetStationData
		-Modified GetHeaderData to also return dataAvail for all types of data
		
		2013.07.09
		Roberto Gomez
		-Added different methods to supply DataViewer with information required.
		
		2013.07.08
		Roberto Gomez
		-Modified to work with new database
		-Removed dependencies with cfgDV, so that it can be used by programs other than DataViewer (grid and sites properties were added). 
		
		2013.07.04
		Roberto Gomez
		-First version created.
		
		
		
		IMPORTANT!!! Insert notes of new update at the beginning, not at the end!!! Latest update info should ALWAYS be displayed first!!!
		
		
		
		
		
	#tag EndNote


	#tag Property, Flags = &h0
		archive As MySQLDatabase
	#tag EndProperty

	#tag Property, Flags = &h0
		grid As String
	#tag EndProperty

	#tag Property, Flags = &h0
		lastMessage_db As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sites As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="grid"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lastMessage_db"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
