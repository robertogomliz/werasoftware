#tag Module
Protected Module log
	#tag Method, Flags = &h0
		Function CreateSystemMonitorStatusFile(station as string, dest as integer, prior as integer, msg as string) As Boolean
		  
		  return CreateSystemMonitorStatusFilePrgrm(App.kAppName,station,dest,prior,msg)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CreateSystemMonitorStatusFilePrgrm(prgrm as string, station as string, dest as integer, prior as integer, msg as string) As Boolean
		  
		  // This method allows for setting a different program name, which avoiding overwriting status files
		  // e.g. in a case where different variables are monitored by the same program (VerifyLastDataSetDB).
		  
		  dim now as date
		  now=new date
		  
		  msg=ReplaceAll(msg,EndOfLine," *_* ")   'Replace end of lines with code for end of line
		  msg=now.SQLDateTime + Chr(9) + station + Chr(9) + prgrm + Chr(9) + CStr(prior) + Chr(9) + CStr(dest) + Chr(9) + msg
		  
		  dim path as string
		  path=App.execDir + "../SystemMonitor/inbox/" + std.Date2Gurgeltime(now,true) + "_" + station + "_" + prgrm + ".status"
		  
		  if WriteToFile(path,msg,true) then
		    return false
		  else
		    return true
		  end
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LogFileGrowing(maxLines as integer) As Boolean
		  
		  //This method verifies if the log file has more than 'maxLines' logged messages. If so, then it will make a backup of the log file and clear the content.
		  
		  if maxLines=0 then return false
		  
		  Dim f As  FolderItem
		  Dim tIn as TextInputStream
		  Dim tOut as TextOutputStream
		  Dim d As New Date
		  Dim auxStrArr() as string
		  
		  f =  GetFolderItem(App.execDir + App.kLogFile + ".log")
		  
		  if f<>Nil Then
		    if f.Exists then
		      
		      tIn=tIn.Open(f)
		      if tIn = Nil then
		        log.Write(false,"Error opening log file! Unable to check if log file is growing: " + f.AbsolutePath )
		        return false
		      else
		        auxStrArr=Split(tIn.ReadAll, EndOfLine)
		        tIn.Close
		      end
		      
		      if Ubound(auxStrArr)>maxLines then
		        
		        MsgBox(f.Name + " has more than " + CStr(maxLines) + " lines. A back up copy will be created.")
		        
		        //backup the file
		        if not(std.CopyFileFromTo(App.execDir + App.kLogFile + ".log",App.execDir + App.kLogFile + "_" + ReplaceAll(d.SQLDate,"-","") + ".log")) then
		          log.Write(false,"Unable to create backup copy: " + f.Name )
		        end
		        
		        if log.WriteToFile(f.AbsolutePath, "", false) then
		        end
		        MsgBox("Content of " + f.Name + " was deleted. It is now empty.")
		        
		        return true
		        
		      else
		        return false
		      end
		      
		    else
		      MsgBox("Log file is missing!!! Creating an empty one...")
		      tOut=tOut.Create(f)
		      tOut.WriteLine(d.SQLDateTime+"  Since no log file exists, created automatically.")
		      tOut.close
		    end
		    
		  else
		    log.Write(false,"Log file is 'Nil'... check if folder of executable exists!!!")
		  end if
		  
		  return false
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Write(verbose as Boolean, input as string)
		  'Diese Funktion schreibt die übergebene Textnachricht zusammen mit dem aktuellen Datum in das logfile
		  
		  Dim f As  FolderItem
		  Dim t as TextOutputStream
		  Dim d As New Date
		  Dim output As String
		  
		  
		  f =  GetFolderItem(App.execDir + App.kLogFile + ".log")
		  
		  if f<>Nil Then
		    if f.Exists then
		      if f.IsWriteable Then
		        t=t.Append(f)
		        output=d.SQLDateTime+"  "+input   'Datum+Zeit+Meldung an Logfile anhängen
		        t.WriteLine output                              'Zeile schreiben
		        t.close
		        
		      else
		        // We do not have the permission to write to the log file...
		        MsgBox("Log file is write-protected. Check permissions!!!")
		      end if
		    else
		      MsgBox("Log file is missing!!! Creating an empty one...")
		      t=t.Create(f)
		      t.WriteLine(d.SQLDateTime+"  Since no log file exists, created automatically.")
		      output=d.SQLDateTime+"  "+input   'Datum+Zeit+Meldung an Logfile anhängen
		      t.WriteLine output                              'Zeile schreiben
		      t.close
		    end
		  else
		    MsgBox("Log file is 'Nil'... check if folder of executable exists!!!")
		  end if
		  
		  if verbose then
		    MsgBox(input)
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function WriteToFile(path as string, input as string, append as Boolean) As Boolean
		  'Diese Funktion schreibt die übergebene Textnachricht zusammen mit dem aktuellen Datum in das logfile
		  
		  Dim f As  FolderItem
		  Dim t as TextOutputStream
		  Dim output As String
		  
		  
		  f =  GetFolderItem(path)
		  
		  if f<>Nil Then
		    if f.Exists and append then
		      if f.IsWriteable Then
		        t=t.Append(f)
		        output=input   'Datum+Zeit+Meldung an Logfile anhängen
		        t.WriteLine output                              'Zeile schreiben
		        t.close
		        
		      else
		        // We do not have the permission to write to the log file...
		        Write(true, "File " + f.AbsolutePath + " is write-protected. Check permissions!!!" + EndOfLine + input)
		        return true
		      end if
		    else
		      t=t.Create(f)
		      output=input   'Datum+Zeit+Meldung an Logfile anhängen
		      if output="" then t.Write(output) else t.WriteLine(output)       'If the output string is empty then do not write a line... just empty the file.
		      t.close
		    end
		  else
		    Write(true, "File " + path + " was found to be invalid (found to be 'Nil', check if folder exists!!!) when trying to log the following message:" + EndOfLine + EndOfLine + input)
		    return true
		  end if
		  
		  return false
		End Function
	#tag EndMethod


	#tag Note, Name = SystemMonitorNotes
		
		//
		// For WERA SystemMonitor to know who should receive a warning email, he looks at the destination
		// column (5th column) of each warning message line. The 5th column contains an integer number. The
		// number goes from 0 to 15 and it is decoded as 4 bits or flags. Each bit indicates the following:
		//
		// bit 1: Message should be sent to Helzel Messtechnik
		// bit 2: Message should be sent to WERA system manager
		// bit 3: Message should be sent to WERA system user
		// bit 4: Message should be displayed on WERA-DataViewer interface
		//
		// Examples:
		// Destination value = 15 = 1111 --> All will receive an email, and DataViewer will also display it.
		// Destination value = 8  = 1000 --> Only DataViewer will display it.
		// Destination value = 3  = 0011 --> Warning will be sent to Helzel and to system manager email accounts.
		// Destination value = 6  = 0110 --> System manager and system user will be informed.
		// Destination value = 0  = 0000 --> Message will be logged but nobody will be informed.
		
		
	#tag EndNote

	#tag Note, Name = Updates
		
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited the module. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new version in the next line, not at the end!!! latest version info should ALWAYS be displayed first!!!
		
		2015.07.14
		Roberto Gomez
		-Corrected method CreateSystemMonitorStatusFilePrgrm to refer to SystemMonitor instead of SystemMontor.
		
		2015.07.13
		Roberto Gomez
		-Re-added the method CreateSystemMonitorStatusFile (for some reason it dissapeared).
		-Included another method CreateSystemMonitorStatusFilePrgrm, which is the same as previous one but allows for setting different program name.
		-Fused CreateSystemMonitorStatusFile method to better use CreateSystemMonitorStatusFile so modification to code are done in one place only.
		
		2013.10.29
		Roberto Gomez
		-Created LogFileGrowing method to verify if the log file has more than a certain amount of lines, and when it does, backup and empty log file. Used for the first time in SystemMonitorHub program to asure that is working OK.
		
		2013.10.29
		Roberto Gomez
		-When WriteToFile method fails, it will try to log it using Write method. Additionally, the string which was intented to be written using WriteToFile method will also be logged using the Write method.
		-Implemented using kLogFile to know the name of the log file when using Write method
		
		2013.10.14
		Roberto Gomez
		-In WriteToFile method, when overwriting the content with an empty string, do not write an end of line... This way we get a really empty file.
		
		2013.09.05
		Roberto Gomez
		-Changed AppendToFile method to WriteToFile method, where there is the option to append to file or overwrite the whole file
		
		2013.09.05
		Roberto Gomez
		-Added AppendToFile method to log to specific files.
		
		2013.06.28
		Roberto Gomez
		-First version of external module
		-Updated deprecated t=f.CreateTextFile and t=f.AppentToTextFile to t=t.Create(f) and t=t. 
		
		
		
		IMPORTANT!!! Insert notes of new version at the beginning, not at the end!!! Latest version info should ALWAYS be displayed first!!!
		
	#tag EndNote


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
