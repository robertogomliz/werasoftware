#tag Class
Protected Class netcdfFile
	#tag Method, Flags = &h0
		Function AddDimension(dimName as string, dimSize as Integer) As Boolean
		  
		  if dimName ="" then
		    returnMsg="Name of dimension is empty."
		    return(false)
		  elseif dimSize  < 0 then
		    returnMsg="Dimension size has to be bigger than 0 (or 0 for unlimited dimensions)."
		    return(false)
		  else
		    dim i as integer
		    for i=1 to nDimensions
		      if dimensionNames(i-1)=dimName then
		        returnMsg="Dimension is already added!"
		        return(false)
		      end
		    next
		  end if
		  
		  dimensionNames.Append(dimName )
		  dimensionSizes.Append(dimSize)
		  nDimensions=nDimensions+1
		  
		  return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddGlobalAttribute(AtribName as string, AtribContent as string) As Boolean
		  
		  
		  if AtribName="" then
		    returnMsg="Name of global attribute is empty."
		    return(false)
		  elseif AtribContent="" then
		    returnMsg="Global attribute content is empty."
		    return(false)
		  else
		    dim i as integer
		    for i=1 to nGlobalAttributes
		      if globAttributeNames(i-1)=AtribName then
		        returnMsg="Global attribute is already added!"
		        return(false)
		      end
		    next
		  end if
		  
		  globAttributeNames.Append(AtribName)
		  
		  //Check for CDL notation
		  if IsNumeric(AtribContent ) then
		    
		  elseif   (AtribContent .Right(1)="b" OR AtribContent .Right(1)="B" OR AtribContent .Right(1)="f" OR AtribContent .Right(1)="F" OR AtribContent .Right(1)="L" ) AND IsNumeric(AtribContent .Left(AtribContent .len -1))   then
		    
		  else
		    dim quotes as string=chr(34)
		    if Left(AtribContent ,1)<>quotes then AtribContent =quotes+AtribContent
		    if Right(AtribContent ,1)<>quotes then AtribContent =AtribContent +quotes
		  end if
		  
		  globAttributeContent.Append(AtribContent)
		  nGlobalAttributes=nGlobalAttributes+1
		  
		  return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddVariable(varName as String, dimensions() as Integer, data(,,,) as Single) As Boolean
		  
		  dim i,j,k,l as integer
		  
		  //Checking that dimension sizes are consistent
		  if dimensions.Ubound<>3 then
		    returnMsg="Number of dimensions does not match with data given!"
		    return false
		  end if
		  
		  //Change data variable to a 4 dimensional array of variants (this would also change the pointer)
		  dim auxData(-1,-1,-1,-1) as Variant
		  redim auxData(Ubound(data,1),Ubound(data,2),Ubound(data,3),Ubound(data,4))
		  for l=0 to UBound(data,1)
		    for k=0 to UBound(data,2)
		      for j=0 to UBound(data,3)
		        for i=0 to UBound(data,4)
		          auxData(l,k,j,i)=data(l,k,j,i)
		        next
		      next
		    next
		  next
		  
		  //Making a copy of dimensions in another pointer...
		  dim copyDimensions(-1) as integer
		  redim copyDimensions(dimensions.Ubound)
		  for i=0 to dimensions.Ubound
		    copyDimensions(i)=dimensions(i)
		  next
		  
		  return RealAddVariable(varName,copyDimensions, auxData)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddVariable(varName as String, dimensions() as Integer, data(,,,) as Variant) As Boolean
		  
		  dim i,j,k,l as integer
		  
		  //Checking if it is a string array with only one character on each element
		  if VarType(data(0,0,0,0)) = Variant.TypeString then
		    
		    for l=0 to UBound(data,1)
		      for k=0 to UBound(data,2)
		        for j=0 to UBound(data,3)
		          for i=0 to UBound(data,4)
		            if len(data(l,k,j,i))>1 then
		              returnMsg="Strings are bigger than 1 character. Since it is a 4 dimensions array (maximum) cannot parse data into a 5 dimensions array."
		              return false
		            end if
		          next
		        next
		      next
		    next
		    
		  end if
		  
		  //Checking that dimension sizes are consistent
		  if dimensions.Ubound<>3 then
		    returnMsg="Number of dimensions does not match with data given!"
		    return false
		  end if
		  
		  //Change data variable to a 4 dimensional array of variants (this would also change the pointer)
		  dim auxData(-1,-1,-1,-1) as Variant
		  redim auxData(Ubound(data,1),Ubound(data,2),Ubound(data,3),Ubound(data,4))
		  for l=0 to UBound(data,1)
		    for k=0 to UBound(data,2)
		      for j=0 to UBound(data,3)
		        for i=0 to UBound(data,4)
		          auxData(l,k,j,i)=data(l,k,j,i)
		        next
		      next
		    next
		  next
		  
		  //Making a copy of dimensions in another pointer...
		  dim copyDimensions(-1) as integer
		  redim copyDimensions(dimensions.Ubound)
		  for i=0 to dimensions.Ubound
		    copyDimensions(i)=dimensions(i)
		  next
		  
		  return RealAddVariable(varName,copyDimensions, auxData)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddVariable(varName as String, dimensions() as Integer, data(,,) as Integer) As Boolean
		  
		  dim i,j,k as integer
		  
		  
		  //Checking that dimension sizes are consistent
		  if dimensions.Ubound<>2 then
		    returnMsg="Number of dimensions does not match with data given!"
		    return false
		  end if
		  
		  //Change data variable to a 4 dimensional array of variants (this would also change the pointer)
		  dim auxData(-1,-1,-1,-1) as Variant
		  redim auxData(0,Ubound(data,1),Ubound(data,2),Ubound(data,3))
		  for k=0 to UBound(data,1)
		    for j=0 to UBound(data,2)
		      for i=0 to UBound(data,3)
		        auxData(0,k,j,i)=data(k,j,i)
		      next
		    next
		  next
		  
		  //Making a copy of dimensions in another pointer...
		  dim copyDimensions(-1) as integer
		  redim copyDimensions(dimensions.Ubound)
		  for i=0 to dimensions.Ubound
		    copyDimensions(i)=dimensions(i)
		  next
		  
		  return RealAddVariable(varName,copyDimensions, auxData)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddVariable(varName as String, dimensions() as Integer, data(,,) as Single) As Boolean
		  
		  dim i,j,k as integer
		  
		  //Checking that dimension sizes are consistent
		  if dimensions.Ubound<>2 then
		    returnMsg="Number of dimensions does not match with data given!"
		    return false
		  end if
		  
		  //Change data variable to a 4 dimensional array of variants (this would also change the pointer)
		  dim auxData(-1,-1,-1,-1) as Variant
		  redim auxData(0,Ubound(data,1),Ubound(data,2),Ubound(data,3))
		  for k=0 to UBound(data,1)
		    for j=0 to UBound(data,2)
		      for i=0 to UBound(data,3)
		        auxData(0,k,j,i)=data(k,j,i)
		      next
		    next
		  next
		  
		  //Making a copy of dimensions in another pointer...
		  dim copyDimensions(-1) as integer
		  redim copyDimensions(dimensions.Ubound)
		  for i=0 to dimensions.Ubound
		    copyDimensions(i)=dimensions(i)
		  next
		  
		  return RealAddVariable(varName,copyDimensions, auxData)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddVariable(varName as String, dimensions() as Integer, data(,,) as Variant) As Boolean
		  
		  dim i,j,k as integer
		  
		  //Checking if it is a string array with only one character on each element
		  if VarType(data(0,0,0)) = Variant.TypeString then
		    dim maxSize as integer=0
		    for k=0 to UBound(data,1)
		      for j=0 to UBound(data,2)
		        for i=0 to UBound(data,3)
		          if maxSize<len(data(k,j,i)) then maxSize=len(data(k,j,i))
		        next
		      next
		    next
		    
		    if maxSize>1 then
		      
		      if maxSize>abs(dimensionSizes(dimensions(dimensions.Ubound)-1)) AND (dimensionSizes(dimensions(dimensions.Ubound)-1))<> 0 then
		        returnMsg="Strings are bigger than 1 character and size of last dimension cannot contain them."
		        return false
		      else
		        
		        //We need to insert an extra dimension on the data array to parse the strings as characters.
		        dim auxData2(-1,-1,-1,-1) as Variant
		        if (dimensionSizes(dimensions(dimensions.Ubound)-1))<> 0 then
		          redim auxData2(  Ubound(data,1)  ,  Ubound(data,2)  ,  Ubound(data,3)  ,  abs(dimensionSizes(dimensions(dimensions.Ubound)-1))-1   )
		        else
		          redim auxData2( Ubound(data,1)  ,  Ubound(data,2)  ,  Ubound(data,3)  ,   maxSize-1   )
		        end if
		        dim l as integer
		        
		        for l=0 to UBound(data,1)
		          for k=0 to UBound(data,2)
		            for j=0 to UBound(data,3)
		              for i=0 to (len(data(l,k,j))-1)
		                auxData2(l,k,j,i)=mid(data(l,k,j),i+1,1)
		              next
		            next
		          next
		        next
		        
		        dim auxBool as Boolean
		        auxBool=AddVariable(varName,dimensions,auxData2)
		        returnMsg="Added an extra dimension to parse strings bigger than 1 character." + returnMsg
		        return auxBool
		        
		      end if
		      
		    end if
		    
		  end if
		  
		  //Checking that dimension sizes are consistent
		  if dimensions.Ubound<>2 then
		    returnMsg="Number of dimensions does not match with data given!"
		    return false
		  end if
		  
		  //Change data variable to a 4 dimensional array of variants (this would also change the pointer)
		  dim auxData(-1,-1,-1,-1) as Variant
		  redim auxData(0,Ubound(data,1),Ubound(data,2),Ubound(data,3))
		  for k=0 to UBound(data,1)
		    for j=0 to UBound(data,2)
		      for i=0 to UBound(data,3)
		        auxData(0,k,j,i)=data(k,j,i)
		      next
		    next
		  next
		  
		  //Making a copy of dimensions in another pointer...
		  dim copyDimensions(-1) as integer
		  redim copyDimensions(dimensions.Ubound)
		  for i=0 to dimensions.Ubound
		    copyDimensions(i)=dimensions(i)
		  next
		  
		  return RealAddVariable(varName,copyDimensions, auxData)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddVariable(varName as String, dimensions() as Integer, data(,) as Variant) As Boolean
		  
		  dim i,j as integer
		  
		  //Checking if it is a string array with only one character on each element
		  if VarType(data(0,0)) = Variant.TypeString then
		    dim maxSize as integer=0
		    for j=0 to UBound(data,1)
		      for i=0 to UBound(data,2)
		        if maxSize<len(data(j,i)) then maxSize=len(data(j,i))
		      next
		    next
		    
		    if maxSize>1 then
		      
		      if maxSize>abs(dimensionSizes(dimensions(dimensions.Ubound)-1)) AND (dimensionSizes(dimensions(dimensions.Ubound)-1))<> 0 then
		        returnMsg="Strings are bigger than 1 character and size of last dimension cannot contain them."
		        return false
		      else
		        
		        //We need to insert an extra dimension on the data array to parse the strings as characters.
		        dim auxData2(-1,-1,-1) as Variant
		        if (dimensionSizes(dimensions(dimensions.Ubound)-1))<> 0 then
		          redim auxData2(  Ubound(data,1)  ,  Ubound(data,2)  ,  abs(dimensionSizes(dimensions(dimensions.Ubound)-1))-1  )
		        else
		          redim auxData2(  Ubound(data,1)  ,  Ubound(data,2)  ,  maxSize-1   )
		        end if
		        dim k as integer
		        
		        for k=0 to UBound(data,1)
		          for j=0 to UBound(data,2)
		            for i=0 to (len(data(k,j))-1)
		              auxData2(k,j,i)=mid(data(k,j),i+1,1)
		            next
		          next
		        next
		        
		        dim auxBool as Boolean
		        auxBool=AddVariable(varName,dimensions,auxData2)
		        returnMsg="Added an extra dimension to parse strings bigger than 1 character." + returnMsg
		        return auxBool
		        
		      end if
		      
		    end if
		    
		  end if
		  
		  
		  //Checking that dimension sizes are consistent
		  if dimensions.Ubound<>1 then
		    returnMsg="Number of dimensions does not match with data given!"
		    return false
		  end if
		  
		  //Change data variable to a 4 dimensional array of variants (this would also change the pointer)
		  dim auxData(-1,-1,-1,-1) as Variant
		  redim auxData(0,0,Ubound(data,1),Ubound(data,2))
		  for j=0 to UBound(data,1)
		    for i=0 to UBound(data,2)
		      auxData(0,0,j,i)=data(j,i)
		    next
		  next
		  
		  //Making a copy of dimensions in another pointer...
		  dim copyDimensions(-1) as integer
		  redim copyDimensions(dimensions.Ubound)
		  for i=0 to dimensions.Ubound
		    copyDimensions(i)=dimensions(i)
		  next
		  
		  return RealAddVariable(varName,copyDimensions, auxData)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddVariable(varName as String, dimensions() as Integer, data() as Variant) As Boolean
		  
		  dim i as integer
		  
		  //Checking if it is a string array with only one character on each element
		  if VarType(data(0)) = Variant.TypeString then
		    dim maxSize as integer=0
		    for i=0 to UBound(data)
		      if maxSize<len(data(i)) then maxSize=len(data(i))
		    next
		    
		    if maxSize>1 then
		      
		      if maxSize>abs(dimensionSizes(dimensions(dimensions.Ubound)-1)) AND (dimensionSizes(dimensions(dimensions.Ubound)-1))<> 0 then
		        returnMsg="Strings are bigger than 1 character and size of last dimension cannot contain them."
		        return false
		      else
		        
		        //We need to insert an extra dimension on the data array to parse the strings as characters.
		        dim auxData2(-1,-1) as Variant
		        if (dimensionSizes(dimensions(dimensions.Ubound)-1))<> 0 then
		          redim auxData2(data.Ubound  ,  abs(dimensionSizes(dimensions(dimensions.Ubound)-1))-1   )
		        else
		          redim auxData2(data.Ubound  ,  maxSize-1   )
		        end if
		        dim j as integer
		        
		        for j=0 to data.Ubound
		          for i=0 to (len(data(j))-1)
		            auxData2(j,i)=mid(data(j),i+1,1)
		          next
		        next
		        
		        dim auxBool as Boolean
		        auxBool=AddVariable(varName,dimensions,auxData2)
		        returnMsg="Added an extra dimension to parse strings bigger than 1 character." + returnMsg
		        return auxBool
		        
		      end if
		      
		    end if
		    
		  end if
		  
		  
		  //Checking that dimension sizes are consistent
		  if dimensions.Ubound<>0 then
		    returnMsg="Number of dimensions does not match with data given!"
		    return false
		  end if
		  
		  //Change data variable to a 4 dimensional array of variants (this would also change the pointer)
		  dim auxData(-1,-1,-1,-1) as Variant
		  redim auxData(0,0,0,data.Ubound)
		  for i=0 to data.Ubound
		    auxData(0,0,0,i)=data(i)
		  next
		  
		  //Making a copy of dimensions in another pointer...
		  dim copyDimensions(-1) as integer
		  redim copyDimensions(dimensions.Ubound)
		  for i=0 to dimensions.Ubound
		    copyDimensions(i)=dimensions(i)
		  next
		  
		  return RealAddVariable(varName,copyDimensions, auxData)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CDL2netcdf() As Boolean
		  
		  
		  
		  
		  dim strAux as string
		  
		  if cdlFile.AbsolutePath.Right(4)= ".cdl" then
		    strAux=Left(cdlFile.AbsolutePath, cdlFile.AbsolutePath.Len - 4) + ".nc"
		  elseif cdlFile.AbsolutePath.Right(3)= ".nc" then
		    strAux=cdlFile.AbsolutePath
		  else
		    strAux=cdlFile.AbsolutePath + ".nc"
		  end
		  
		  if not(std.ShellExec("ncgen -o " + strAux + " " + cdlFile.AbsolutePath)) then
		    returnMsg="Problem using ncgen to convert from CDL to netCDF: " + std.lastMessage_std
		    return false
		  end if
		  
		  if std.CheckForFolderitem(false, strAux)<>0 then
		    returnMsg="Couldn't find netcdf file (" + strAux + ") after being created!"
		    return false
		  end if
		  
		  ncFile=GetFolderItem(strAux)
		  
		  return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Create(path as string, setToNilAfterCDL as Boolean) As Boolean
		  
		  
		  // create CDL file
		  if path.Right(3)=".nc" then
		    cdlFile =  GetFolderItem(path.Left(path.len-3) + ".cdl")
		  else
		    cdlFile =  GetFolderItem(path + ".cdl")
		  end if
		  if cdlFile<>Nil Then
		    if not(CreateCDL() ) then return(false)
		  end if
		  
		  if setToNilAfterCDL then
		    me.variables=Nil  //Hopefully this will release memory usage!!!
		  end
		  
		  // convert CDL file to netcdf
		  if not( CDL2netcdf() ) then return false
		  
		  if Not(DebugBuild) then cdlFile.Delete
		  
		  returnMsg="Netcdf file succesfully created!"
		  
		  Return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function CreateCDL() As Boolean
		  
		  dim t as TextOutputStream
		  Dim tab, quotes as string
		  dim i,j,k,l,m as integer
		  
		  tab=Chr(9)
		  quotes=Chr(34)
		  
		  
		  // create CDL text file and write on it.
		  t=t.Create(cdlFile)
		  
		  
		  // First line with name of file...
		  if fileName<>"" then
		    t.WriteLine("netcdf " + fileName + " {")
		  elseif right(cdlFile.Name,7)=".nc.cdl" then
		    t.WriteLine("netcdf " + left(cdlFile.Name,cdlFile.Name.Len-7) + " {")
		  elseif right(cdlFile.Name,3)=".nc" then
		    t.WriteLine("netcdf " + left(cdlFile.Name,cdlFile.Name.Len-3) + " {")
		  else
		    t.WriteLine("netcdf " + cdlFile.Name + " {")
		  end if
		  
		  
		  
		  // Writing declaration of dimensions...
		  t.WriteLine("")
		  t.WriteLine("dimensions:")
		  for i=1 to nDimensions
		    if dimensionSizes(i-1)<=0 then
		      dimensionSizes(i-1)=abs(dimensionSizes(i-1))
		      t.WriteLine(tab + dimensionNames(i-1) + " = UNLIMITED ; // (" + CStr(dimensionSizes(i-1)) + " currently)")
		    else
		      t.WriteLine(tab + dimensionNames(i-1) + " = " + CStr(dimensionSizes(i-1)) + " ;")
		    end
		  next
		  
		  
		  // Writing declaration of variables...
		  t.WriteLine("")
		  t.WriteLine("variables:")
		  for i=1 to nVariables
		    
		    //Write data type and name
		    select case GetVariable(i).dataType
		    case 1
		      t.Write(tab + "byte " + GetVariable(i).name + "(")
		    case 2
		      t.Write(tab + "int " + GetVariable(i).name + "(")
		    case 3
		      t.Write(tab + "long " + GetVariable(i).name + "(")   //long is CDL clasic model and is deprecated... and it is now a synonymous of int... maybe int64 should be used
		    case 4
		      t.Write(tab + "float " + GetVariable(i).name + "(")
		    case 5
		      t.Write(tab + "double " + GetVariable(i).name + "(")
		    case 6
		      t.Write(tab + "char " + GetVariable(i).name + "(")
		    case 7
		      t.Write(tab + "double " + GetVariable(i).name + "(")
		    end select
		    
		    //Write dimensions
		    t.Write(dimensionNames(  GetVariable(i).usedDimensions(0) - 1   ) )
		    for j=1 to GetVariable(i).usedDimensions.Ubound
		      t.Write(", " + dimensionNames(  GetVariable(i).usedDimensions(j) - 1   ))
		    next
		    t.WriteLine(") ;")
		    
		    //Write attributes
		    for j=1 to GetVariable(i).nAttributes
		      t.WriteLine(tab + tab + GetVariable(i).name + ":" + GetVariable(i).attributeName(j-1) + " = " + GetVariable(i).attributeContent(j-1) + " ;")
		    next
		    
		  next
		  
		  
		  //Write Global Attributes
		  t.WriteLine("")
		  t.WriteLine("// global attributes:")
		  for i=1 to nGlobalAttributes
		    t.WriteLine(tab + tab + ":" + globAttributeNames(i-1) + " = " + globAttributeContent(i-1) + " ;")
		  next
		  
		  
		  //Write data:
		  t.WriteLine("")
		  t.WriteLine("")
		  t.WriteLine("")
		  t.WriteLine("")
		  t.WriteLine("data:")
		  
		  dim auxStr as string
		  for m=0 to nVariables-1
		    
		    t.WriteLine("")
		    t.WriteLine("")
		    t.Write(" " + variables(m).name + " =")
		    for l=0 to UBound(variables(m).dataArray,1)
		      t.WriteLine("")
		      t.WriteLine("//")
		      for k=0 to UBound(variables(m).dataArray,2)
		        t.WriteLine("")
		        for j=0 to UBound(variables(m).dataArray,3)
		          auxStr=tab
		          for i=0 to UBound(variables(m).dataArray,4)
		            
		            if auxStr=tab then
		              if variables(m).dataType=6 then auxStr=auxStr+quotes
		            else
		              if (i mod 100) = 0 AND variables(m).dataType<>6 then auxStr=auxStr+EndOfLine   //Doing this will affect strings formed by chars, so it is not done for chars.
		            end if
		            
		            
		            //depending on data type
		            if variables(m).dataType=6 then
		              auxStr=auxStr+CStr(variables(m).dataArray(l,k,j,i))
		            elseif variables(m).dataType=4 then
		              auxStr=auxStr + replace(Format(variables(m).dataArray(l,k,j,i),"-#.######e"),",",".") + ","
		            elseif variables(m).dataType=5 then
		              auxStr=auxStr + replace(Format(variables(m).dataArray(l,k,j,i),"-#.##############e"),",",".") + ","
		            else
		              auxStr=auxStr+CStr(variables(m).dataArray(l,k,j,i)) + ","
		            end
		            
		          next
		          
		          if variables(m).dataType=6 then auxStr=auxStr+quotes + ","
		          
		          if l<UBound(variables(m).dataArray,1) OR k<UBound(variables(m).dataArray,2) OR j<UBound(variables(m).dataArray,3) then
		            t.WriteLine(auxStr)
		          else
		            t.Write(left(auxStr,auxStr.Len -1))
		          end if
		        next
		      next
		    next
		    
		    t.WriteLine(" ;")
		  next
		  
		  
		  
		  //closing file
		  t.Write("}")
		  t.close
		  
		  return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CreateOnlyCDL(path as string) As Boolean
		  
		  // create CDL file
		  if path.Right(3)=".nc" then
		    cdlFile =  GetFolderItem(path.Left(path.len-3) + ".cdl")
		  else
		    cdlFile =  GetFolderItem(path + ".cdl")
		  end if
		  if cdlFile<>Nil Then
		    if not(CreateCDL() ) then return(false)
		  end if
		  
		  Return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DateString2NetCDFTime(dateStr as String) As Double
		  
		  
		  dim auxDate as new Date
		  
		  auxDate.SQLDateTime=dateStr
		  
		  dim outDbl as Double
		  outDbl=auxDate.TotalSeconds-netcdfTimeRef
		  
		  if netcdfTimeUnits>=2 then  outDbl=outDbl/60  'Converting seconds to minutes
		  
		  if netcdfTimeUnits>=3 then  outDbl=outDbl/60  'Converting minutes to hours
		  
		  if netcdfTimeUnits>=4 then  outDbl=outDbl/24  'Converting hours to days
		  
		  return outDbl
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetVariable(idx as integer) As netcdfVariable
		  
		  return(variables(idx-1))
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetVariable(inputName as string) As netcdfVariable
		  
		  dim i as integer
		  
		  for i=0 to variables.Ubound
		    if variables(i).name=inputName then return variables(i)
		  next
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Load(path as string) As Boolean
		  
		  
		  
		  if path="" then
		    returnMsg="Cannot load empty file."
		    return false
		  end
		  
		  ncFile=GetFolderItem(path)
		  if ncFile=Nil then
		    returnMsg="Invalid path. Directory may not exist."
		    return false
		  end if
		  
		  if not(ncFile.Exists) then
		    returnMsg="File not found."
		    return false
		  end if
		  
		  if not(netcdf2CDL()) then return false
		  
		  if LoadCDL then
		    if Not(DebugBuild) then cdlFile.Delete
		    returnMsg="Netcdf file succesfully loaded!"
		    return(true)
		  else
		    if Not(DebugBuild) then cdlFile.Delete
		    return(false)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function LoadCDL() As Boolean
		  
		  dim t as TextInputStream
		  Dim tab, quotes, varNames(-1), dimNames(-1), attributeLines(-1)  as string
		  dim i,j,k,l,m,n, lineCnt, currentData, varTypes(-1), idx, auxDimensions(-1), auxInt1, auxInt2, auxVarArraySizes(3) as integer
		  dim auxStr, fileContent(-1), auxStrArray1(-1), auxStrArray2(-1) as string
		  dim varFills(-1), globalFillVal, auxVar, auxVarArray1(-1), auxVarArray2(-1,-1), auxVarArray3(-1,-1,-1), auxVarArray4(-1,-1,-1,-1) as Variant
		  dim auxSgl as Single
		  dim auxDbl as Double
		  dim auxLng as Int64
		  dim auxShr as int8
		  
		  tab=Chr(9)
		  quotes=Chr(34)
		  lineCnt=0
		  currentData=0
		  
		  
		  // Open CDL text file to read from it.
		  t=t.Open(cdlFile)
		  if t=Nil then
		    returnMsg="Error when opening cdl file for reading!"
		    return false
		  end if
		  
		  fileContent=Split(    ReplaceAll(      ReplaceAll(t.ReadAll,","+EndOfLine ,",")     ,  ", "+EndOfLine   ,  ","     )       , EndOfLine)
		  
		  // First line with name of file...
		  auxStrArray1=split(fileContent(lineCnt)," ")
		  if auxStrArray1.Ubound<2 then
		    returnMsg="Invalid first line on cdl file."
		    return false
		  elseif auxStrArray1(0)<>"netcdf" then
		    returnMsg="Could not find 'netcdf' word in first line"
		    return false
		  end
		  
		  auxStr=""
		  for i=1 to auxStrArray1.Ubound
		    if auxStrArray1(i)="{" then exit
		    if auxStrArray1(i)=" " then Continue
		    auxStr=auxStr+auxStrArray1(i)
		  next
		  
		  if auxStr="" then
		    returnMsg="Could not find file name in first line."
		    return false
		  end if
		  
		  fileName=auxStr
		  lineCnt=lineCnt+1
		  
		  
		  // Process the rest of the file
		  auxStr=""
		  for lineCnt=lineCnt to fileContent.Ubound
		    auxStr= auxStr + ReadLineNC(fileContent(lineCnt))
		    
		    if auxStr="dimensions:" then
		      currentData=1
		      auxStr=""
		      Continue
		    elseif auxStr="variables:" then
		      currentData=2
		      auxStr=""
		      Continue
		    elseif auxStr="data:" then
		      currentData=3
		      
		      //Check for missing_value or _FillValue attributes before reading the data======================
		      for m=0 to attributeLines.Ubound
		        if InStr(0,attributeLines(m),"missing_value")>0 OR InStr(0,attributeLines(m),"_FillValue")>0 then
		          
		          auxStrArray1=split(attributeLines(m),":")  'Now we should have the variable name in auxStrArray1(0)
		          
		          auxStrArray2=split(auxStrArray1(1),"=") 'Now we should have the actual _FillValue in auxStrArray2(1)
		          auxStrArray2(1)=auxStrArray2(1).Trim
		          
		          if auxStrArray1(0).Trim="" then   'It is a global attribute
		            
		            //Check for CDL notation
		            if (auxStrArray2(1).Right(1)="f" OR auxStrArray2(1).Right(1)="F") AND IsNumeric(auxStrArray2(1).Left(auxStrArray2(1).len -1)) then
		              auxSgl=CDbl(auxStrArray2(1))
		              globalFillVal=auxSgl
		            elseif auxStrArray2(1).Right(1)="L" AND IsNumeric(auxStrArray2(1).Left(auxStrArray2(1).len -1)) then
		              auxLng=CLong(auxStrArray2(1))
		              globalFillVal=auxLng
		            elseif (auxStrArray2(1).Right(1)="b" OR auxStrArray2(1).Right(1)="B") AND IsNumeric(auxStrArray2(1).Left(auxStrArray2(1).len -1)) then
		              auxShr=CLong(auxStrArray2(1))
		              globalFillVal=auxShr
		            elseif IsNumeric(auxStrArray2(1)) then
		              if InStr(auxStrArray2(1),".")>0 then
		                auxDbl=CDbl(auxStrArray2(1))
		                globalFillVal=auxDbl
		              else
		                auxInt1=CLong(auxStrArray2(1))
		                globalFillVal=auxInt1
		              end
		            elseif auxStrArray2(1).Left(1)=quotes AND auxStrArray2(1).Right(1)=quotes then
		              globalFillVal=mid(auxStrArray2(1),2,auxStrArray2(1).len-1)
		            else
		              globalFillVal=auxStrArray2(1)
		            end if
		            
		          else 'It is a variable attribute
		            
		            idx=varNames.IndexOf(auxStrArray1(0).Trim)
		            if idx=-1 then 'Not found
		              
		              returnMsg=" _FillValue or missing_value attribute points to an unknown variable."
		              return false
		              
		            else
		              
		              //Check for CDL notation
		              if auxStrArray2(1).Left(1)=quotes AND auxStrArray2(1).Right(1)=quotes AND varTypes(idx)=6 then
		                varFills(idx)=mid(auxStrArray2(1),2,auxStrArray2(1).len-1)
		              elseif IsNumeric(auxStrArray2(1)) OR (  (auxStrArray2(1).Right(1)="b" OR auxStrArray2(1).Right(1)="B" OR auxStrArray2(1).Right(1)="f" OR auxStrArray2(1).Right(1)="F" OR auxStrArray2(1).Right(1)="L" ) AND IsNumeric(auxStrArray2(1).Left(auxStrArray2(1).len -1))   )  then
		                
		                if auxStrArray2(1).Right(1)="b" OR auxStrArray2(1).Right(1)="B" OR auxStrArray2(1).Right(1)="f" OR auxStrArray2(1).Right(1)="F" OR auxStrArray2(1).Right(1)="L" then auxStrArray2(1)=auxStrArray2(1).Left(auxStrArray2(1).len -1)
		                
		                if varTypes(idx)=1 then
		                  auxShr=CLong(auxStrArray2(1))
		                  varFills(idx)=auxShr
		                elseif varTypes(idx)=2 then
		                  auxInt1=CLong(auxStrArray2(1))
		                  varFills(idx)=auxInt1
		                elseif varTypes(idx)=3 then
		                  auxLng=CLong(auxStrArray2(1))
		                  varFills(idx)=auxLng
		                elseif varTypes(idx)=4 then
		                  auxSgl=CDbl(auxStrArray2(1))
		                  varFills(idx)=auxSgl
		                elseif varTypes(idx)=5 then
		                  auxDbl=CDbl(auxStrArray2(1))
		                  varFills(idx)=auxDbl
		                else
		                  returnMsg="variable data type not yet implemented!"
		                  return false
		                end if
		              else
		                varFills(idx)=auxStrArray2(1)
		              end if
		              
		            end
		          end if
		          
		        end
		      next
		      //============================================================
		      
		      auxStr=""
		      Continue
		    end
		    
		    if Right(auxStr,1)=";" then
		      
		      auxStr=left(auxStr,auxStr.Len-1)  'remove the ';' at the end
		      
		      if currentData=1 then
		        
		        //Add dimensions
		        auxStrArray1=split(auxStr,",")   'Sometimes more than one dimension is defined in the same line...
		        for m=0 to auxStrArray1.Ubound
		          auxStrArray2=split(auxStrArray1(m),"=")
		          if auxStrArray2.Ubound<>1 then
		            returnMsg="Dimension assignment line not understood!"
		            return false
		          end
		          if Not(IsNumeric(auxStrArray2(1).Trim)) then
		            if Uppercase(auxStrArray2(1).Trim)<>"UNLIMITED" then
		              returnMsg="Dimension size is not numeric!"
		              return false
		            end
		            auxStrArray2(1)="0" 'This dimension is unlimited...
		          end
		          if not(AddDimension(auxStrArray2(0).Trim,CLong(auxStrArray2(1).Trim)) ) then return false
		        next
		        
		        auxStr=""
		        
		      elseif currentData=2 then
		        
		        //Add variable definition and metadata
		        if InStr(0,auxStr,":")>0 AND InStr(0,auxStr,"=")>0 then  'It is a variable attribute
		          attributeLines.Append(auxStr)
		        else
		          
		          'It is probably a variable definition
		          auxStrArray1=split(auxStr," ")
		          
		          if auxStrArray1(0) ="char" then
		            varTypes.Append(6)
		            varFills.Append(Nil)  // which means '|'
		          elseif auxStrArray1(0) ="byte" then
		            varTypes.Append(1)
		            varFills.Append(Nil)
		          elseif auxStrArray1(0) ="short" OR auxStrArray1(0) ="int" then
		            varTypes.Append(2)
		            varFills.Append(Nil)
		          elseif auxStrArray1(0) ="long" OR auxStrArray1(0) ="int64" then
		            varTypes.Append(3)
		            varFills.Append(Nil)
		          elseif auxStrArray1(0) ="float" OR auxStrArray1(0) ="real" then
		            varTypes.Append(4)
		            varFills.Append(Nil)
		          elseif auxStrArray1(0) ="double" then
		            varTypes.Append(5)
		            varFills.Append(Nil)
		          else
		            returnMsg="Data type " + auxStrArray1(0) + " not recognized."
		            return false
		          end if
		          
		          auxStrArray1=split(auxStrArray1(1).Trim,"(")
		          varNames.Append(auxStrArray1(0).Trim)
		          
		          auxStrArray1=split(auxStr,"(")
		          auxStrArray1=split(auxStrArray1(1).Trim,")")
		          dimNames.Append(auxStrArray1(0).Trim)
		          
		        end if
		        
		        auxStr=""
		        
		      elseif currentData=3 then
		        
		        //Getting name of variable and comma-separated data
		        auxStrArray1=split(auxStr,"=")   'Variable name should now be in auxStrArray1(0)
		        auxStrArray2=split(auxStrArray1(1),",")   'auxStrArray2 is now the data in 1-D array.
		        
		        // Is the variable declared?
		        idx=varNames.IndexOf(auxStrArray1(0).Trim)
		        if idx=-1 then
		          returnMsg="Cannot load data from an undeclared variable! (" + auxStrArray1(0) + ")"
		          return false
		        else
		          
		          //Are the dimensions required by the variable declared?
		          auxStrArray1=split(dimNames(idx),",")
		          redim auxDimensions(auxStrArray1.Ubound)
		          for n=0 to auxStrArray1.Ubound
		            auxDimensions(n)=dimensionNames.IndexOf(auxStrArray1(n).Trim) +1
		            if auxDimensions(n)=0 then 'Dimension was not found!
		              returnMsg="Cannot load data that uses an undeclared dimension! (" + auxStrArray1(n) + ")"
		              return false
		            end
		          next
		          
		          
		          //Setting fill value
		          if varFills(idx)=Nil then
		            if globalFillVal<>Nil then
		              varFills(idx)=globalFillVal
		            else
		              varFills(idx)=-999
		            end if
		          end if
		          
		          
		          //Check for unlimited dimension
		          auxInt1=1
		          auxInt2=-1
		          for n=0 to auxDimensions.Ubound
		            if abs(dimensionSizes(auxDimensions(n)-1))=0 then 'is an unlimited dimension with unknown size... let us store the index of unlimited dimension.
		              auxInt2=n
		            else
		              auxInt1=auxInt1*abs(dimensionSizes(auxDimensions(n)-1))
		            end
		          next
		          
		          if varTypes(idx)=6 AND auxInt2>=0 then
		            returnMsg="Case of string data with unlimited dimension not yet supported!"
		            return false
		          end if
		          
		          if auxInt2>=0 then auxInt1=(auxStrArray2.Ubound + 1)/auxInt1  //an unlimited dimension was found before!! Then calculate the size of the 'unlimited dimension' according to data size.
		          
		          
		          //Setting the size of auxVarArray...
		          for n=0 to 3
		            if auxDimensions.Ubound  >= (3-n) then   'This dimension is used... let us set the size according to data size.
		              auxVarArraySizes(n)= abs(       dimensionSizes(   auxDimensions(n-3+auxDimensions.Ubound) -1   )            )        -1
		              if auxVarArraySizes(n)=-1 then 'This is the unlimited dimension... let us use the size obtained before...
		                auxVarArraySizes(n)=auxInt1-1
		              end if
		            else
		              auxVarArraySizes(n)=0
		            end if
		          next
		          
		          if auxDimensions.Ubound = 0 then
		            redim auxVarArray1(auxVarArraySizes(3))
		          elseif auxDimensions.Ubound = 1 then
		            redim auxVarArray2(auxVarArraySizes(2),auxVarArraySizes(3))
		          elseif auxDimensions.Ubound = 2 then
		            redim auxVarArray3(auxVarArraySizes(1),auxVarArraySizes(2),auxVarArraySizes(3))
		          elseif auxDimensions.Ubound = 3 then
		            redim auxVarArray4(auxVarArraySizes(0),auxVarArraySizes(1),auxVarArraySizes(2),auxVarArraySizes(3))
		          else
		            returnMsg="Maximum number of dimensions suported is 4!"
		            return false
		          end if
		          
		          
		          //here transform the string unidimensional array to variant multidimensional array of correct datatype...
		          
		          for l=0 to auxVarArraySizes(0)
		            for k=0 to auxVarArraySizes(1)
		              for j=0 to auxVarArraySizes(2)
		                
		                if varTypes(idx)=6 then 'Is an array of characters!!!
		                  //If it is an array of chars... they should be given as strings in the cdl file, which means the last dimension is not separated by commas.
		                  //The last dimension is eliminated and i now indicates the character position
		                  auxStr=auxStrArray2(   j   +  (k*(auxVarArraySizes(2)+1))  + (l*(auxVarArraySizes(2)+1)*(auxVarArraySizes(1)+1))   )
		                  auxStr=Trim(auxStr)
		                  auxStr=Mid(auxStr,2,auxStr.len-2)
		                end if
		                
		                for i=0 to auxVarArraySizes(3)
		                  
		                  if varTypes(idx)<>6 then
		                    auxStr=auxStrArray2(   i + (j*(auxVarArraySizes(3)+1))   +  (k*(auxVarArraySizes(3)+1)*(auxVarArraySizes(2)+1))  + (l*(auxVarArraySizes(3)+1)*(auxVarArraySizes(2)+1)*(auxVarArraySizes(1)+1))   )
		                    auxStr=auxStr.Trim
		                  end
		                  
		                  if auxStr="_" OR auxStr="NaN" then auxStr=CStr(varFills(idx))
		                  
		                  if varTypes(idx)=1 then
		                    auxShr=CLong(auxStr)
		                    auxVar=auxShr
		                  elseif varTypes(idx)=2 then
		                    auxInt1=CLong(auxStr)
		                    auxVar=auxInt1
		                  elseif varTypes(idx)=3 then
		                    auxLng=CLong(auxStr)
		                    auxVar=auxLng
		                  elseif varTypes(idx)=4 then
		                    auxSgl=CDbl(auxStr)
		                    auxVar=auxSgl
		                  elseif varTypes(idx)=5 then
		                    auxDbl=CDbl(auxStr)
		                    auxVar=auxDbl
		                  else
		                    auxVar=auxStr.Mid(i+1,1)
		                  end if
		                  
		                  if auxDimensions.Ubound = 0 then
		                    auxVarArray1(i)=auxVar
		                  elseif auxDimensions.Ubound = 1 then
		                    auxVarArray2(j,i)=auxVar
		                  elseif auxDimensions.Ubound = 2 then
		                    auxVarArray3(k,j,i)=auxVar
		                  elseif auxDimensions.Ubound = 3 then
		                    auxVarArray4(l,k,j,i)=auxVar
		                  end if
		                  
		                  
		                next
		              next
		            next
		          next
		          
		          
		          //FINALLY!!! add variable
		          if auxDimensions.Ubound = 0 then
		            if not(AddVariable(varNames(idx),auxDimensions,auxVarArray1)) then return false
		          elseif auxDimensions.Ubound = 1 then
		            if not(AddVariable(varNames(idx),auxDimensions,auxVarArray2)) then return false
		          elseif auxDimensions.Ubound = 2 then
		            if not(AddVariable(varNames(idx),auxDimensions,auxVarArray3)) then return false
		          elseif auxDimensions.Ubound = 3 then
		            if not(AddVariable(varNames(idx),auxDimensions,auxVarArray4)) then return false
		          end if
		          
		          variables(variables.Ubound).FillValue=varFills(idx)
		          
		        end
		        
		        auxStr=""
		        
		      end if
		      
		      
		    end
		    
		  next
		  
		  
		  // Set the variable attributes and global attributes
		  for m=0 to attributeLines.Ubound
		    
		    auxStrArray1=split(attributeLines(m),":")  'Now we should have the variable name in auxStrArray1(0)
		    for n=2 to auxStrArray1.Ubound 'In case the attribute value contains ':'...
		      auxStrArray1(1)=auxStrArray1(1) + ":" + auxStrArray1(n)
		    next
		    
		    auxStrArray2=split(auxStrArray1(1),"=")    'Now we should have the attribute name in auxStrArray2(0) and the actual value of attribute in auxStrArray2(1)
		    for n=2 to auxStrArray2.Ubound 'In case the attribute value contains '='...
		      auxStrArray2(1)=auxStrArray2(1) + "=" + auxStrArray2(n)
		    next
		    
		    if auxStrArray1(0).Trim="" then  'Global attribute
		      
		      if not(AddGlobalAttribute(auxStrArray2(0).Trim,auxStrArray2(1).Trim)) then return false
		      
		    else    'Variable attribute
		      
		      idx=varNames.IndexOf(auxStrArray1(0).Trim)
		      if idx=-1 then 'Not found
		        
		        returnMsg="Attribute points to an unknown variable."
		        return false
		        
		      else
		        
		        if not(GetVariable(varNames(idx)).AddAttribute(auxStrArray2(0).Trim,auxStrArray2(1).Trim)) then return false
		        
		      end
		    end if
		    
		  next
		  
		  
		  //closing file
		  t.close
		  
		  return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function netcdf2CDL() As Boolean
		  
		  
		  if not(std.ShellExec("ncdump " + ncFile.AbsolutePath + " &> " + ncFile.AbsolutePath + ".cdl")) then
		    returnMsg=std.lastMessage_std
		    return false
		  end if
		  
		  if std.CheckForFolderitem(false, ncFile.AbsolutePath + ".cdl")<>0 then
		    returnMsg="Couldn't find CDL file (" + ncFile.AbsolutePath + ".cdl" + ") after being created!"
		    return false
		  end if
		  
		  cdlFile=GetFolderItem(ncFile.AbsolutePath + ".cdl")
		  
		  return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function netCDFTimeToDate(netcdfTimeVal as Double) As date
		  
		  
		  if netcdfTimeUnits>=4 then netcdfTimeVal=netcdfTimeVal*24  'Converting days to hours
		  
		  if netcdfTimeUnits>=3 then netcdfTimeVal=netcdfTimeVal*60  'Converting hours to minutes
		  
		  if netcdfTimeUnits>=2 then netcdfTimeVal=netcdfTimeVal*60  'Converting minutes to seconds
		  
		  netcdfTimeVal=round(netcdfTimeVal*100000)/100000
		  
		  dim outDate as new Date
		  
		  outDate.TotalSeconds=netcdfTimeRef+netcdfTimeVal
		  
		  return outDate
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ReadLineNC(inputString as String) As string
		  dim auxStringArray(-1) as string
		  
		  if inputString ="" then return ""
		  
		  auxStringArray=split(inputString,"//")
		  
		  return Trim(ReplaceAll(auxStringArray(0), Chr(9),""))
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function RealAddVariable(varName as String, dimensions() as Integer, data(,,,) as Variant) As Boolean
		  
		  dim dataType,i,j,k,l as integer
		  dim auxNC as new netcdfVariable
		  
		  
		  for i=0 to dimensions.Ubound
		    if Ubound(data,4-i)+1<> abs(dimensionSizes( dimensions(dimensions.Ubound-i)-1 )) then
		      //Maybe is an unlimited dimension???
		      if dimensionSizes(dimensions(dimensions.Ubound-i)-1)=0 then   'If dimensionSizes is 0, then it is an unlimited dimension... let us indicate the size using a negative number.
		        dimensionSizes(dimensions(dimensions.Ubound-i)-1)=-Ubound(data,4-i)-1
		      else
		        returnMsg="Dimension sizes do not match!"
		        return false
		      end
		    end
		  next
		  
		  
		  //Checking if variable is repeated
		  for i=1 to nVariables
		    if GetVariable(i).name=varName  then
		      returnMsg="Variable is already added!"
		      return false
		    end
		  next
		  
		  
		  //Checking type of data
		  dataType=VarType(data(0,0,0,0))
		  
		  select case dataType
		    
		  case Variant.TypeBoolean
		    dataType=1
		  case Variant.TypeInteger
		    dataType=2
		  case Variant.TypeLong
		    dataType=3
		  case Variant.TypeSingle
		    dataType=4
		  case Variant.TypeDouble
		    dataType=5
		  case Variant.TypeString
		    dataType=6
		  case Variant.TypeDate
		    dataType=7
		  else
		    returnMsg="Data type is not valid!"
		    return false
		  end
		  
		  
		  '//Check if it is string and if chars are stored instead of actual strings!
		  'if dataType=6 then
		  'for l=0 to UBound(data,1)
		  'for k=0 to UBound(data,2)
		  'for j=0 to UBound(data,3)
		  'for i=0 to UBound(data,4)
		  'if len(data(l,k,j,i))>1 then
		  'result.error=true
		  'returnMsg="Arrays with string data will be parsed as 'char' data in the NetCDF file. Therefore, string elements must be one character long. The array given has elements with more than one character."
		  'return false
		  'end if
		  'next
		  'next
		  'next
		  'next
		  '
		  'end
		  
		  //creating variable according to data type
		  auxNC.nDimensions=dimensions.Ubound+1
		  auxNC.dataType=dataType
		  auxNC.name=varName
		  auxNC.usedDimensions=dimensions
		  
		  if dataType=1 then
		    
		    dim int8_true, int8_false as int8
		    int8_true=1
		    int8_false=0
		    
		    for l=0 to UBound(data,1)
		      for k=0 to UBound(data,2)
		        for j=0 to UBound(data,3)
		          for i=0 to UBound(data,4)
		            if data(l,k,j,i) then data(l,k,j,i)=int8_true else data(l,k,j,i)=int8_false
		          next
		        next
		      next
		    next
		    
		  end if
		  
		  
		  auxNC.SetData(data)
		  variables.Append(auxNC)
		  nVariables=nVariables+1
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function RealTrim(inputStr as string) As String
		  // It will first remove all tabs in the string before applying trim
		  
		  dim auxString as String
		  
		  auxString=ReplaceAll(inputStr,Chr(9),"")
		  return auxString.Trim
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function setNetCDFTimeReference(timeRefStr as string) As Boolean
		  // As input, the 'units' string attribute of the 'time' variable in a netcdf file is expected... something like 'days since 1900-01-01 00:00:00'.
		  // This time string is read, the type of units (days, hours, minutes or seconds) is stored into 'netcdfTimeUnits', a date is created with the time reference and the TotalSeconds of the created date is stored in 'netcdfTimeRef' property.
		  
		  dim auxStrArray(-1) as string
		  dim i as integer
		  
		  if timeRefStr.Left(1)=Chr(34) then timeRefStr=timeRefStr.Right(timeRefStr.len-1)
		  
		  auxStrArray=split(timeRefStr," ")
		  
		  if auxStrArray.Ubound<=1 then
		    returnMsg="Could not decode time reference string --> " + timeRefStr + "."
		    return false
		  end
		  
		  if auxStrArray(1)<>"since" then
		    returnMsg="Could not decode time reference string --> " + timeRefStr + "."
		    return false
		  end
		  
		  if auxStrArray(0)="seconds" then
		    netcdfTimeUnits=1
		  elseif auxStrArray(0)="minutes" then
		    netcdfTimeUnits=2
		  elseif auxStrArray(0)="hours" then
		    netcdfTimeUnits=3
		  elseif auxStrArray(0)="days" then
		    netcdfTimeUnits=4
		  else
		    returnMsg="Units '" + auxStrArray(0) + "' are not understood, while decoding time reference string."
		    return false
		  end
		  
		  
		  //Now storing in timeRefStr the date and time reference
		  timeRefStr=""
		  for i=2 to auxStrArray.Ubound
		    timeRefStr=timeRefStr+auxStrArray(i)
		  next
		  
		  
		  //Separate the date information...
		  auxStrArray=split(timeRefStr,"-")
		  
		  if auxStrArray.Ubound<=1 then
		    returnMsg="Reference date not understood --> " + timeRefStr + "."
		    return false
		  end
		  
		  //Now storing in timeRefStr the time reference (if any!) for later checking
		  timeRefStr=auxStrArray(2).Right(auxStrArray(2).Len-2)
		  for i=3 to auxStrArray.Ubound
		    timeRefStr=timeRefStr+auxStrArray(i)
		  next
		  
		  
		  
		  //Check the date reference...
		  auxStrArray(0)=auxStrArray(0).Right(4) '<-- here should be the year
		  auxStrArray(2)=auxStrArray(2).Left(2)  '<-- here should be the day
		  
		  if not(IsNumeric(auxStrArray(0))) then
		    returnMsg="Reference year not understood --> " + auxStrArray(0) + "."
		    return false
		  end
		  
		  if not(IsNumeric(auxStrArray(1))) then
		    returnMsg="Reference month not understood --> " + auxStrArray(1) + "."
		    return false
		  end
		  
		  if IsNumeric(auxStrArray(2).Right(1)) then
		    if not(IsNumeric(auxStrArray(2).Left(1))) then
		      returnMsg="Reference day not understood --> " + auxStrArray(2) + "."
		      return false
		    end if
		  elseif IsNumeric(auxStrArray(2).left(1)) then
		    auxStrArray(2)=auxStrArray(2).Left(1)
		  else
		    returnMsg="Reference day not understood --> " + auxStrArray(2) + "."
		    return false
		  end if
		  
		  
		  
		  //Create date object
		  dim outDate as new Date
		  outDate.Day=CLong(auxStrArray(2))
		  outDate.Month=CLong(auxStrArray(1))
		  outDate.Year=CLong(auxStrArray(0))
		  
		  
		  //Now check if time is also given...
		  auxStrArray=split(timeRefStr,":")
		  if auxStrArray.Ubound<=1 then
		    
		  else
		    
		    //Check the time reference...
		    auxStrArray(0)=auxStrArray(0).Right(2) '<-- here should be the hour
		    auxStrArray(2)=auxStrArray(2).Left(2)  '<-- here should be the seconds
		    
		    if IsNumeric(auxStrArray(2).Right(1)) then
		      if IsNumeric(auxStrArray(2).Left(1)) then
		        
		        if IsNumeric(auxStrArray(0)) AND IsNumeric(auxStrArray(1)) then
		          outDate.Second=CLong(auxStrArray(2))
		          outDate.Minute=CLong(auxStrArray(1))
		          outDate.Hour=CLong(auxStrArray(0))
		          returnMsg=""
		          netcdfTimeRef=outDate.TotalSeconds
		          return true
		        end
		        
		      end if
		    elseif IsNumeric(auxStrArray(2).left(1)) then
		      auxStrArray(2)=auxStrArray(2).Left(1)
		      
		      if IsNumeric(auxStrArray(0)) AND IsNumeric(auxStrArray(1)) then
		        outDate.Second=CLong(auxStrArray(2))
		        outDate.Minute=CLong(auxStrArray(1))
		        outDate.Hour=CLong(auxStrArray(0))
		        returnMsg=""
		        netcdfTimeRef=outDate.TotalSeconds
		        return true
		      end
		      
		    end if
		  end
		  
		  returnMsg="WARNING, time reference seems to not be given. Assuming 00:00:00."
		  outDate.Second=0
		  outDate.Minute=0
		  outDate.Hour=0
		  
		  netcdfTimeRef=outDate.TotalSeconds
		  return true
		End Function
	#tag EndMethod


	#tag Note, Name = Known_Bugs
		
		
		14.07.17 RG
		-When a multidimensional data variable has an UNLIMITED dimension in the second or higher dimension, then the CDL file fails to convert to netcdf.
		I think that UNLIMITED dimensions are only allowed on the first dimension of a multidimensional data variable.
		
		
		14.07.23 RG
		-Detailed testing is required for booleans-byte, string-char and date values. Integer types and floating types should work ok.
	#tag EndNote

	#tag Note, Name = Version_Notes
		
		
		15.02.06
		Roberto Gomez
		-Included method CreateOnlyCDL, so that it is possible setting the netcdfFile object to Nil, free memory and then create the netcdf file from CDL file outside the netcdfFile class.
		
		14.08.11
		Roberto Gomez
		-Added AddVariable methods that use single arrays as data inputs.
		
		
		14.07.23
		Roberto Gomez
		-First version.
	#tag EndNote


	#tag Property, Flags = &h0
		cdlFile As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		dimensionNames(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		dimensionSizes(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		fileName As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private globAttributeContent(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private globAttributeNames(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private ncFile As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		nDimensions As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			contains the 'TotalSeconds' value of a date object which is considered to be the time reference
			Use setNetCDFTimeReference method to set this value.
		#tag EndNote
		netcdfTimeRef As Double = 0
	#tag EndProperty

	#tag Property, Flags = &h1
		#tag Note
			1=seconds
			2=minutes
			3=hours
			4=days
		#tag EndNote
		Protected netcdfTimeUnits As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		nGlobalAttributes As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		nVariables As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		returnMsg As String
	#tag EndProperty

	#tag Property, Flags = &h0
		variables(-1) As netcdfVariable
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="fileName"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nDimensions"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="netcdfTimeRef"
			Group="Behavior"
			InitialValue="0"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nGlobalAttributes"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nVariables"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="returnMsg"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
