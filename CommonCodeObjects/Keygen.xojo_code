#tag Class
Protected Class Keygen
	#tag Method, Flags = &h0
		Sub Constructor()
		  Dim ref As new Date
		  ref.GMTOffset = 0
		  ref.Year = 2010
		  ref.Month = 12
		  ref.Day = 30
		  ref.Hour = 23 - ref.GMTOffset
		  ref.Minute = 59
		  ref.Second = 59
		  me.EXPIRY_REF = ref
		  
		  
		  me.UserInfo = new Dictionary
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(Key As String)
		  // call the other constructor
		  me.constructor
		  
		  // and go ahead and perform the validation
		  call me.ValidateKey(key)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DecodeFlags(Chars As String) As Boolean()
		  // convert chars to a numeric value
		  Dim intrep As UInt32 = Self.DecodeInt( chars )
		  
		  'this number indicates what flags are set. for example,
		  'intrep=20 = 0001 0100
		  'the LSB is flag[0], so an intrep of 20 gives us:
		  'flag[0] = 0
		  'flag[1] = 0
		  'flag[2] = 1
		  'flag[3] = 0
		  'flag[4] = 1
		  
		  Dim result() As Boolean
		  
		  While intrep > Zero
		    result.Append( intrep Mod 2 = 1 )
		    intrep = Bitwise.ShiftRight( intrep, 1 )
		  Wend
		  Return result
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DecodeInt(S As String) As UInt32
		  Dim retval As Uint32 = 0
		  
		  For i As UInt8 = 1 To S.Len
		    Dim c As String = S.Mid( i, 1 )
		    
		    retval = Bitwise.BitOr( _
		    Bitwise.ShiftLeft( retval, me.BPC ), _
		    me.ALPHABET.InStrB( c ) - 1 _
		    )
		  Next
		  Return retval
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function EncodeFlags(FlagArray() As Boolean, Width As UInt8 = 0) As String
		  Dim intrep As UInt32 = 0
		  
		  For i As Integer = 0 To FlagArray.Ubound
		    
		    If FlagArray( i ) Then
		      intrep = Bitwise.BitOr( intrep, Bitwise.ShiftLeft( 1, i ) )
		    End If
		  Next
		  
		  Return Self.EncodeInt(intrep, width)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function EncodeInt(IntVal As UInt32, Width As UInt8=0) As String
		  // Turn an integer into an encoded string, optionally padding the string to width characters.
		  Dim retval As String
		  static MASK As UInt32 = Pow( 2, me.BPC ) - 1
		  
		  If IntVal = 0 Then
		    retval = me.ALPHABET.LeftB( 1 )
		  Else
		    While( intval > Zero )
		      Dim tmpval As UInt32 = Bitwise.BitAnd( IntVal, MASK )
		      IntVal =Bitwise.ShiftRight( intval, me.BPC )
		      retval = me.ALPHABET.MidB( tmpval + 1, 1 ) + retval
		    Wend
		  End If
		  
		  While retval.LenB < Width
		    retval = me.ALPHABET.LeftB(1) + retval
		  Wend
		  
		  Return retval
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GenerateKey() As String
		  If me.Expiration <> Nil Then
		    // let's talk in UTC:
		    me.Expiration.Hour = me.Expiration.Hour - me.Expiration.GMTOffset
		    me.Expiration.Hour = 23
		    me.Expiration.Minute = 59
		    me.Expiration.Second = 59
		  End If
		  
		  
		  // Come up with a seed to use
		  Dim seed As UInt32 = 0
		  While (0 = seed)
		    seed = Microseconds
		    seed = BitWise.BitAnd( seed, &h7FFF )
		  Wend
		  
		  Return me.GenerateKey( seed )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GenerateKey(seedval As UInt32) As String
		  me.srand( seedval )
		  
		  me.SortAlphabet
		  ' random seed is encoded using sorted valid chars, so encode it prior to shuffling
		  ' encode the seed, padding to 31 bits, or 7 characters
		  Dim randomSeedEncoded As String = me.EncodeInt(me.mSeed, 7)
		  
		  ' now we know how to seed the random number generator, we can properly shuffle the alphabet for obfuscation
		  me.ShuffleAlphabet
		  
		  
		  Dim userInfoRaw As String = me.GetHashableUserInfo()
		  // hash and mask down to 20 bits
		  Dim userInfoHash As UInt32 = Bitwise.BitAnd( me.hash( userInfoRaw ), 1048575 ) // (Pow(2, 20)-1)
		  
		  Dim userDataHashEncoded As String = me.EncodeInt(userInfoHash, 4)
		  
		  // key metadata -- one character only
		  Dim metadata_flags As UInt32 = 0
		  If me.InputIsCaseSensitive Then metadata_flags = Bitwise.BitOr( metadata_flags, 1 )
		  If me.Expiration <> Nil Then metadata_flags = Bitwise.BitOr( metadata_flags, 2 )
		  Dim metaFlagsEncoded As String = me.EncodeInt( metadata_flags )
		  
		  
		  Dim expirationDateEncoded As String = ""
		  If me.Expiration <> Nil Then
		    'Dim daysDiff As UInt32 = Ceil( (me.Expiration.TotalSeconds - me.Expiration.GMTOffset*3600 - me.EXPIRY_REF.TotalSeconds)/86400 )
		    'Dim expTS As UInt32 = me.Expiration.TotalSeconds
		    'Dim refTS As UInt32 = me.EXPIRY_REF.TotalSeconds
		    
		    Dim daysDiff As UInt32 = Ceil( (me.Expiration.TotalSeconds - me.EXPIRY_REF.TotalSeconds)/86400 )
		    expirationDateEncoded = me.EncodeInt( daysDiff, 3 )
		  Else
		    // no expiration encoded. generate 3 random characters to fill the void
		    expirationDateEncoded = me.GenerateRandomCharacters(3)
		  End If
		  Dim appFlagsEncoded As String = me.EncodeFlags(me.Flags, 2)
		  
		  // generate random characters to pad the key to 24 characters.
		  // since we know (based on design) how the size of other fields, we also
		  // know we'll need 3 characters for random padding.
		  Dim randomFiller As String = me.GenerateRandomCharacters(3)
		  
		  
		  // now perform an introspective consistency check in 16 bits
		  // concatenate all other fields in order
		  'Dim parts() As array = 
		  
		  Dim preMetaHash As String = Join( Array( userDataHashEncoded, metaFlagsEncoded, expirationDateEncoded, appFlagsEncoded, randomSeedEncoded, randomFiller ), "" )
		  Dim metaHash As UInt32 = bitwise.BitAnd( me.Hash( preMetaHash ), Pow(2,20) - 1)
		  Dim metaHashEncoded As String = me.EncodeInt( metaHash, 4 )
		  
		  // and finally, combine everything
		  Dim key As String = preMetaHash + metaHashEncoded
		  
		  Return key
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GenerateRandomCharacters(numChars As UInt8) As String
		  Dim ret As String
		  
		  For i As UInt8 = 1 To numChars
		    // Using an Int64, we can  maintain the full range of values in the UInt32 returned by RandInt.
		    // If we used Uint32, it appears REALbasic would coerce the value down to an Integer, losing half
		    // the range, thereby causing serious problems here.
		    Dim rnd As Int64 = me.RandInt
		    ret = ret + me.ALPHABET.MidB( (rnd mod me.ALPHABET.Len) + 1, 1 )
		  next
		  Return ret
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetHashableUserInfo() As String
		  'Deterministic encoding of user info.
		  'For example, "user" => "John Smith", "company" => "Acme Towing"
		  'will produce "company=Acme Towing&user=John Smith".
		  '
		  'use a basic approach to hashing a string: join all (key,value) pairs with '=', then all pairs with '&'.
		  'pre-sort the keys to guarantee determinism.
		  
		  Dim keys(), values() As String
		  
		  For Each v As variant In Self.UserInfo.keys
		    If Self.InputIsCaseSensitive Then
		      keys.Append( v.StringValue )
		      values.Append( Self.UserInfo.Value( v ).StringValue )
		    Else
		      // user input is not considered case-sensitive, so lowercase everything up-front.
		      keys.Append( v.StringValue.Lowercase )
		      values.Append( Self.UserInfo.Value( v ).StringValue.Lowercase )
		    End If
		  Next
		  
		  keys.SortWith( values )
		  
		  Dim parts() As String
		  For i As UInt16 = 0 To keys.Ubound
		    Dim k As String = EncodeURLComponent( keys( i ) )
		    Dim v As String = EncodeURLComponent( values( i ) )
		    
		    parts.Append( k + "=" + v )
		  Next
		  
		  Return Join( parts, "&" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Hash(S As String) As UInt32
		  // Peter Weinberger's PJW hash
		  // http://www.cs.hmc.edu/~geoff/classes/hmc.cs070.200101/homework10/hashfuncs.html
		  Dim h As UInt64 = 0
		  For i As UInt16 = 1 To s.LenB
		    Dim c As String = s.MidB( i, 1 )
		    // The top 4 bits of h are all zero
		    h = BitWise.ShiftLeft(h, 4) + c.AscB // shift h 4 bits left, add in ki
		    Dim g As UInt64 = BitWise.BitAnd( h, &hf0000000 )            // get the top 4 bits of h
		    If g <> 0 Then                     // if the top 4 bits aren't zero,
		      h = BitWise.BitXOR( h, BitWise.ShiftRight(g, 24))           //   move them to the low end of h
		    End If
		    h = BitWise.BitXOR(h, g)
		  next
		  // The top 4 bits of h are again all zero
		  Return h
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function RandInt() As UInt32
		  // Blum Blum Shub (BBS)
		  // http://www.cs.indiana.edu/~kapadia/project2/node11.html
		  Dim modval As Int64 = 9777
		  Dim tmpVal As Int64 = me.mSeed
		  tmpVal = ( tmpVal * tmpVal ) mod modval
		  me.mSeed = tmpVal // x(n+1) = x(n)^2 % M, where M = 3259*3
		  Return me.mSeed
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ShuffleAlphabet()
		  Dim letters() As String = Me.ALPHABET.Split("")
		  Dim one As UInt32 = 1 // to counteract Integer+Uint32 --> unexpected results
		  For i As UInt32 = Me.ALPHABET.Len -1 DownTo 1
		    Dim j As UInt32 = me.RandInt mod (i+one)
		    
		    Dim tmp As String = letters( j )
		    letters(j) = letters(i)
		    letters(i) = tmp
		  next
		  Me.ALPHABET = Join( letters, "" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SortAlphabet()
		  Dim chars() As String = me.ALPHABET.Split( "" )
		  chars.Sort()
		  me.ALPHABET = Join( chars, "" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub srand(SeedVal As UInt32)
		  // because BBS squares me.mSeed with each iteration, and because we allow the initial seed
		  // to be any value up to 31 bits
		  // we should keep only the lower  here.
		  me.mSeed = BitWise.BitAnd( SeedVal, &h7FFF)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		 Shared Sub Test()
		  Dim kg As New Keygen
		  kg.Expiration = new Date( 2013, 07, 31 )
		  kg.UserInfo.Value( "user" ) = "John Q User"
		  
		  kg.Flags.Append( True )
		  kg.Flags.Append( False )
		  kg.Flags.Append( True )
		  kg.Flags.Append( True )
		  
		  Dim key As String = kg.GenerateKey
		  
		  Dim msg As String = "Generated '" + key + "'"
		  If kg.Expiration = Nil Then
		    msg = msg + EndOfLine + "   with no expiration"
		  Else
		    msg = msg + EndOfLine + "   with expiration of " + kg.Expiration.SQLDateTime
		  End If
		  msg = msg + EndOfLine + "   with flags: ["
		  For Each b As Boolean In kg.Flags
		    msg = msg + Str(b) + ", "
		  Next
		  msg = msg + "]"
		  
		  MsgBox( msg )
		  
		  kg = new Keygen
		  kg.UserInfo.Value( "user" ) = "John Q User" // user data must be presented identically both when generating and validating a key
		  If kg.ValidateKey( key ) Then
		    If kg.Expiration = Nil Then
		      MsgBox( "'" + key + "' is valid with no expiration." )
		    Else
		      MsgBox( "'" + key + "' is valid with an expiration of " + kg.Expiration.SQLDateTime )
		    End If
		  Else
		    MsgBox( "'" + key + "' is invalid." )
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ValidateKey(Key As String) As Boolean
		  If key.LenB <> me.KEY_LENGTH Then
		    me.mRegistrationStatus = EnumStatus.KEY_INVALID
		    Return False
		  End If
		  
		  // check that there are no invalid characters
		  For i As UInt8 = 1 To KEY_LENGTH
		    Dim c As String = Key.MidB( i, 1 )
		    
		    If me.ALPHABET.InStrB( c ) = 0 Then
		      me.mRegistrationStatus = EnumStatus.KEY_INVALID
		      Return False
		    End If
		  next
		  Dim userDataHashEncoded As String = key.MidB( 1, 4 )
		  Dim metaFlagsEncoded As String = key.MidB( 5, 1 )
		  Dim expirationDateEncoded As String = key.MidB( 6, 3 )
		  Dim appFlagsEncoded As String = key.MidB( 9, 2 )
		  Dim randomSeedEncoded As String = key.MidB( 11, 7 )
		  Dim randomFiller As String = key.MidB( 18, 3 )
		  Dim metaHashEncoded As String = key.MidB( 21, 4 )
		  
		  // seed the PRNG
		  // ensure the alphabet is sorted for proper random seed decoding
		  me.SortAlphabet
		  Dim randomSeedDecoded As UInt32 = me.DecodeInt( randomSeedEncoded )
		  me.srand( randomSeedDecoded )
		  // and shuffle the alphabet
		  me.ShuffleAlphabet
		  // should the user input be considered case-sensitive?
		  Dim metadataDecoded As UInt32 = me.DecodeInt( metaFlagsEncoded )
		  me.InputIsCaseSensitive = Bitwise.BitAnd( metadataDecoded, 1 ) > 0 // metadataDecoded isn't particularly large, so no need to compare againt Zero
		  
		  Dim userInfoRaw As String = Me.GetHashableUserInfo()
		  // hash and mask down to 20 bits
		  Dim userInfoHash As UInt32 = Bitwise.BitAnd( me.hash( userInfoRaw ), 1048575 ) // (Pow(2, 20)-1)
		  
		  Dim userDataHashEncoded_generated As String = Me.EncodeInt(userInfoHash, 4)
		  
		  if userDataHashEncoded <> userDataHashEncoded_generated Then
		    Me.mRegistrationStatus = EnumStatus.KEY_INVALID
		    Return False
		  End If
		  
		  // and is there an expiration?
		  If Bitwise.BitAnd( metadataDecoded, 2 ) > 0 Then // same as above
		    // an expiration is encoded. decode it now.
		    Dim daysDiff As UInt32 = self.DecodeInt( expirationDateEncoded )
		    Dim expDate As New Date
		    expDate.TotalSeconds = me.EXPIRY_REF.TotalSeconds + daysDiff*86400
		    
		    Dim now As new Date
		    
		    If expDate < now Then
		      // the key is expired. don't return until we know the key is otherwise valid
		      me.Expiration = expDate
		      me.mRegistrationStatus = EnumStatus.KEY_EXPIRED
		    Else
		      // the key is good. save the expiration date
		      me.Expiration = expDate
		    End If
		    
		  Else
		    
		    // no expiration encoded into the key. 3 random characters were used instead. verify they match.
		    me.Expiration = Nil
		    Dim expirationDateEncoded_rand As String = me.GenerateRandomCharacters( 3 )
		    If expirationDateEncoded_rand <> expirationDateEncoded Then
		      me.mRegistrationStatus = EnumStatus.KEY_INVALID
		      Return False
		    End If
		  End If
		  
		  
		  // decode flags
		  me.Flags = me.DecodeFlags( appFlagsEncoded )
		  
		  // check that the PRNG sequence matches what was used during generation
		  Dim randomFiller_generated As String = me.GenerateRandomCharacters(3)
		  
		  If randomFiller <> randomFiller_generated Then
		    // sequence isn't correct
		    me.mRegistrationStatus = EnumStatus.KEY_INVALID
		    Return False
		  End If
		  
		  
		  // now perform an introspective consistency check in 16 bits
		  // concatenate all other fields in order
		  'Dim parts() As array = 
		  
		  Dim preMetaHash As String = Join( Array( userDataHashEncoded, metaFlagsEncoded, expirationDateEncoded, appFlagsEncoded, randomSeedEncoded, randomFiller ), "" )
		  Dim metaHash As UInt32 = bitwise.BitAnd( me.Hash( preMetaHash ), Pow(2,20) - 1)
		  Dim metaHashEncoded_generated As String = me.EncodeInt( metaHash, 4 )
		  If metaHashEncoded <> metaHashEncoded_generated Then
		    me.mRegistrationStatus = EnumStatus.KEY_INVALID
		    Return False
		  End If
		  
		  // bootstrap with GenerateKey and compare to what was passed in.
		  Dim generated As String = me.GenerateKey( randomSeedDecoded )
		  If generated <> key Then
		    me.mRegistrationStatus = EnumStatus.KEY_INVALID
		    Return False
		  End If
		  
		  If me.mRegistrationStatus <> EnumStatus.KEY_EXPIRED Then
		    me.mRegistrationStatus = EnumStatus.KEY_VALID
		    Return True
		  End If
		  
		End Function
	#tag EndMethod


	#tag Note, Name = Untitled
		
		You can alter EXPIRY_REF to any valid YYYYMMDD value if you so choose. Granularity remains at the day level, and 15 bits (3 characters) are allocated for the number of days past the reference. This should be more than enough for any valid expiration dates.
		
		All other constants must remain as defined in your generated code. Altering them will almost certainly break this code.
	#tag EndNote


	#tag Property, Flags = &h21
		Private ALPHABET As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			The date on which the key expires -- good until 23:59:59 on this day. Nil means no expiration.
		#tag EndNote
		Expiration As Date
	#tag EndProperty

	#tag Property, Flags = &h21
		Private EXPIRY_REF As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		Flags() As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			should user input be considered case sensitive?
		#tag EndNote
		InputIsCaseSensitive As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRegistrationStatus As EnumStatus = EnumStatus.KEY_STATUS_UNKNOWN
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSeed As UInt64
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mRegistrationStatus
			End Get
		#tag EndGetter
		RegistrationStatus As EnumStatus
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		UserInfo As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h21
		#tag Note
			REALbasic has a problem with comparing large (>2**32-1) values against a literal zero. This property is used in a few places and allows us to avoid that problem.
		#tag EndNote
		Private Zero As Uint32 = 0
	#tag EndProperty


	#tag Constant, Name = BPC, Type = Double, Dynamic = False, Default = \"5", Scope = Private
	#tag EndConstant

	#tag Constant, Name = KEY_LENGTH, Type = Double, Dynamic = False, Default = \"24", Scope = Private
	#tag EndConstant


	#tag Enum, Name = EnumStatus, Type = Integer, Flags = &h0
		KEY_EXPIRED=-2
		  KEY_INVALID
		  KEY_STATUS_UNKNOWN
		KEY_VALID
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InputIsCaseSensitive"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RegistrationStatus"
			Group="Behavior"
			Type="EnumStatus"
			EditorType="Enum"
			#tag EnumValues
				"-2 - KEY_EXPIRED"
				"-1 - KEY_INVALID"
				"0 - KEY_STATUS_UNKNOWN"
				"1 - KEY_VALID"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
