#tag Module
Protected Module std
	#tag Method, Flags = &h0
		Function ActivateMBSComplete() As Boolean
		  
		  //Activate MBS Plugins
		  // Matthias Kniephoff, MBS Complete, 201308, 196748220
		  dim x as integer = 100
		  dim s as string = "MBS "
		  dim n as string = decodeBase64("TWF0dGhpYXMgS25pZXBob2Zm", encodings.UTF8)
		  dim y as integer = 20
		  
		  dim result as Boolean
		  result=registerMBSPlugin(n, s+"Complete", 2013*x+08, 1967482*x+y)
		  return result
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CalculateHours(date_1 as string, date_2 as String) As Integer
		  //NOTE:
		  
		  // THIS METHOD IS TOO COMPLICATED... PROBABLY YOU SHOULD USED CALCULATESECONDS INSTEAD, WHICH IS SIMPLER.
		  
		  
		  // Calculates the number of hours between two SQL date strings. If the time in between is more than 1 month then -1 is returned. If date_2 is before date_1 then -2 is returned.
		  // It is assumed that this function is used for checking if the user asked for an enourmous plot (bigger than 31 days) and if not, calculate the hours to be displayed in the stats of the plot.
		  
		  Dim y1, y2, m1, m2, d1, d2, h1, h2, mi1, mi2, hours as integer
		  
		  y1=CLong(mid(date_1, 1, 4))
		  m1=CLong(mid(date_1, 6, 2))
		  d1=CLong(mid(date_1, 9, 2))
		  h1=CLong(mid(date_1, 12, 2))
		  mi1=CLong(mid(date_1, 15, 2))
		  
		  y2=CLong(mid(date_2, 1, 4))
		  m2=CLong(mid(date_2, 6, 2))
		  d2=CLong(mid(date_2, 9, 2))
		  h2=CLong(mid(date_2, 12, 2))
		  mi2=CLong(mid(date_2, 15, 2))
		  
		  if (y2-y1)>=2 then
		    return(-1)
		  elseif (y2-y1)=1 then
		    if m1=12 and m2=1 then
		      if d2<d1 then
		        
		        //CalculateHours
		        hours=(31-d1-1)*24   'Hours of days left to finish the year1
		        hours=hours + 24-h1-1   'Hours left to count from day1
		        hours=hours + (24*(d2-1))   'Hours of completed days from year2
		        hours=hours + h2   'Hours left to count from day2
		        if mi2>=mi1 then   'Check if the minutes may count for an extra hour
		          return(hours+1)
		        else
		          return(hours)
		        end
		        
		      else
		        return(-1)
		      end
		    else
		      return(-1)
		    end
		  elseif y2=y1 then
		    if (m2-m1)>=2 then
		      return(-1)
		    elseif (m2-m1)=1 then
		      if d2<d1 then
		        
		        //CalculateHours
		        if m1=2 then   'month1 is februar...
		          if ( y1 mod 4 )= 0 then //leap year (schaltjahr) (the rule should be more complicated, but we should not worry until year 2100
		            hours=(29-d1-1)*24   'Hours of days left to finish month1
		          else
		            hours=(28-d1-1)*24   'Hours of days left to finish month1
		          end
		          
		        elseif m1=4 or m1=6 or m1=9 or m1=11 then   'month1 has 30 days
		          hours=(30-d1-1)*24   'Hours of days left to finish month1
		        else   'month1 has 31 days
		          hours=(31-d1-1)*24   'Hours of days left to finish month1
		        end
		        hours=hours + 24-h1-1   'Hours left to count from day1
		        hours=hours + (24*(d2-1))   'Hours of completed days from month2
		        hours=hours + h2   'Hours left to count from day2
		        if mi2>=mi1 then   'Check if the minutes may count for an extra hour
		          return(hours+1)
		        else
		          return(hours)
		        end
		        
		      else
		        return(-1)
		      end
		    elseif m2=m1 then
		      if d2<d1 then
		        return(-2)
		      elseif d2=d1 then
		        if h2<h1 then
		          return(-2)
		        elseif h2=h1 then
		          if mi2<mi1 then
		            return(-2)
		          else
		            return(0)
		          end
		        else
		          
		          //Calculate hours
		          if mi2>=mi1 then   'Check if the minutes may count for an extra hour
		            return(h2-h1)
		          else
		            return(h2-h1-1)
		          end
		          
		        end if
		      else
		        
		        //Calculate hours
		        hours=(d2-d1-1)*24  'Hours of all days in between
		        hours=hours+24-h1-1   'Hours left to count from day1
		        hours=hours+h2   'Hours left to count from day2
		        if mi2>=mi1 then   'Check if the minutes may count for an extra hour
		          return(hours+1)
		        else
		          return(hours)
		        end
		        
		      end
		    else
		      return(-2)
		    end
		  else
		    return(-2)
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CalculateSeconds(date_1 as string, date_2 as String) As Integer
		  // Calculates the number of seconds between two SQL date strings.
		  
		  Dim date1, date2 as Date
		  
		  date1=new Date
		  date2=new Date
		  
		  try
		    date1.SQLDateTime=date_1
		    date2.SQLDateTime=date_2
		  Catch e As UnsupportedFormatException
		    log.Write(true, "Unsupported format exception when calculating seconds difference between two SQL date strings!")
		    return 0
		  end try
		  return(round(date2.TotalSeconds-date1.TotalSeconds))
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CheckForFolderitem(verbose as boolean, FileName as string) As integer
		  // checks for the path given as input. It returns:
		  //    0   If the path points to an existent file.
		  //    1   If the path points to an existent folder.
		  //  -1   If the path cannot be opened or does not exists
		  //
		  // In case of -1, the reason is logged using log.Write
		  
		  Dim File as FolderItem
		  
		  if FileName="" then
		    if verbose then log.Write(false, "Cannot check empty path!")
		    lastMessage_std="Cannot check empty path!"
		    return -1
		  end if
		  
		  if TargetLinux then
		    // Check if the File exists:
		    File= GetFolderItem(FileName)
		    if File<>NIL then
		      
		      if File.Exists then
		        if File.Directory then
		          lastMessage_std=FileName+" is a folder."
		          return 1
		        else
		          lastMessage_std=FileName+" exists."
		          return 0
		        end
		      else
		        if verbose then log.Write(false,FileName+" does not exists.")
		        lastMessage_std=FileName+" does not exists."
		        return -1
		      end if
		      
		    else
		      if verbose then log.Write(false,"Error opening "+FileName)
		      lastMessage_std="Error opening "+FileName
		      return -1
		    end if
		    
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CopyFileFromTo(source As String, destination As String) As Boolean
		  'Funktion kopiert Datei von einem Ordner in einen anderen
		  'Bei Erfolg wird der Wert true zurückgegeben
		  'Fängt alle Eventualitäten wie Quell/Zielordner/Quelldatei nicht vorhanden ab.
		  
		  Dim fsource, fdestination As FolderItem
		  
		  fsource=GetFolderItem(source,FolderItem.PathTypeAbsolute)    'Quelldatei öffnen
		  
		  if fsource <>Nil Then
		    fsource.Permissions= &o777                                                           'FolderItem hat alle Rechte
		    
		    if fsource.exists then
		      fdestination=GetFolderItem(destination,FolderItem.PathTypeAbsolute)   'Zieldatei öffnen
		      
		      if fdestination<>Nil Then
		        fdestination.Permissions= &o777                                                                 'FolderItem hat alle Rechte
		        
		        fsource.CopyFileTo fdestination       'Quelldatei verschieben
		        if fsource.LastErrorCode=0  then        'Verschiebt Datei nur, wenn kein Fehler aufgetreten ist(z.B.File not found, File in use, out of memory...)
		          
		          Return true
		        else
		          log.Write(false,"Copy error or file in use: ("+source+" -> "+destination+") Error codes "+Str(fsource.LastErrorCode)+" "+Str(fdestination.LastErrorCode))
		          Return false
		        end if
		        
		      else
		        log.Write(false,"Copy error: destination "+destination+" returns Nil.")
		        Return false
		      end if
		      
		    else
		      log.Write(false,"Copy error: source "+source+" does not exist.")
		      Return false
		    end if
		  else
		    log.Write(false,"Copy error: source "+source+" returns Nil.")
		    Return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CreateFolderWhenNotExistent(StartingFolder as string, folders as string) As Boolean
		  dim fitemp as FolderItem
		  dim auxStr(-1) as string
		  dim i as integer
		  
		  std.lastMessage_std=""
		  
		  fitemp=GetFolderItem(StartingFolder)
		  
		  if fitemp=nil then
		    std.lastMessage_std="Starting folder is not valid (Nil)!"
		    return false
		  else
		    if not(fitemp.Exists) then
		      std.lastMessage_std="Starting folder does not exists!"
		      return false
		    end if
		  end if
		  
		  
		  auxStr=split(folders,"/")
		  
		  for i=0 to auxStr.Ubound
		    
		    if auxStr(i)="" then Continue
		    
		    fitemp=fitemp.Child(auxStr(i))
		    if not(fitemp.Exists) then
		      fitemp.CreateAsFolder
		      if not(fitemp.Exists) then
		        std.lastMessage_std="Could not  create subfolder!"
		        return false
		      end if
		      std.lastMessage_std=std.lastMessage_std + "Created folder " + auxStr(i) + ". "
		    end if
		    
		  next
		  
		  if std.lastMessage_std="" then std.lastMessage_std="No folders were created!"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Date2Gurgeltime(now as date, useSeconds as Boolean) As string
		  
		  if useSeconds then
		    return( CStr(now.Year) + Str(now.DayOfYear,"00#") + Str(now.Hour,"0#") + Str(now.Minute,"0#") + Str(now.Second,"0#") )
		  else
		    return( CStr(now.Year) + Str(now.DayOfYear,"00#") + Str(now.Hour,"0#") + Str(now.Minute,"0#") )
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Date2yyyymmddhhmmss(now as date, useSeconds as Boolean) As string
		  
		  if useSeconds then
		    return( CStr(now.Year) + Str(now.Month,"0#") +Str(now.Day,"0#")+Str(now.Hour,"0#") + Str(now.Minute,"0#") + Str(now.Second,"0#") )
		  else
		    return( CStr(now.Year) + Str(now.Month,"0#") +Str(now.Day,"0#")+Str(now.Hour,"0#") + Str(now.Minute,"0#"))
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DecodeKlCuv2Qual(kl as integer) As integer
		  
		  //Removing extrapolation flag included (100 before QC and -100 after QC)
		  if kl>90 then kl=kl-100
		  if kl<-10 then kl=kl+100
		  
		  return floor(  (kl+1)/10   )
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Gurgeltime2DBstring(input as String) As String
		  'Funktion wird der Dateiname übergeben wandelt Gurgels Zeitcodierung ins Datenbakformat um
		  'z.B. 20080011200 --> 2008-01-01 12:00:00
		  Dim i As Integer
		  Dim sec as String
		  Dim min As String
		  Dim hour As String
		  Dim dayofyear As integer
		  Dim day As integer
		  Dim month As integer
		  Dim year As integer
		  
		  Dim montharray1(13) as Integer
		  Dim montharray2(13) as Integer
		  
		  Dim sk As integer                      'Korrekturwert für Schaltjahr
		  Dim result As String
		  
		  montharray1 = Array(0,31,59,90,120,151,181,212,243,273,304,334,365)  'Anzahl Tage ohne Schaltjahr
		  montharray2 = Array(0,31,60,91,121,152,182,213,244,274,305,335,366)  'Anzahl Tage mit Schaltjahr
		  
		  year=Val(Left(input,4))                'Jahr auslesen, Stelle 1-4
		  dayofyear=Val(Mid(input,5,3))    'Tag des Jahres auslesen, Stelle 5-8
		  hour=Mid(input,8,2)                     'Stunden auslesen, Stelle 8-9
		  min=Mid(input,10,2)                     'Minuten auslesen, Stelle 10-11
		  
		  if IsNumeric(Mid(input,12,2)) then
		    // It also has seconds!!!
		    sec=Mid(input,12,2)
		  else
		    sec="00"
		  end
		  
		  'Schaltjahr prüfen
		  If year Mod 4 = 0 And year Mod 100 = 0 Then
		    If year Mod 400 = 0 Then                                   ' Durch 400 teilbar?
		      sk=1
		    Else ' Nicht ganzzahlig teilbar.
		      sk=0
		    End If
		    
		  ElseIf year Mod 4 = 0 Then
		    sk=1
		  Else
		    sk=0
		  End If
		  
		  'Umrechnung TagdesJahres in Monat und Tag
		  
		  for i=0 to 12 Step 1
		    if sk=0 Then  'kein Schaltjahr
		      if dayofyear>=montharray1(i) and dayofyear<=montharray1(i+1) Then
		        day=dayofyear-montharray1(i)
		        month=i+1
		        exit
		      end if
		      
		    else
		      if dayofyear>=montharray2(i) and dayofyear<=montharray2(i+1) Then
		        day=dayofyear-montharray2(i)
		        month=i+1
		        exit
		      end if
		    end if
		    
		  next
		  
		  result= CStr(year)+"-"+CStr(month)+"-"+CStr(day)+" "+hour+":"+min+":"+sec
		  'MsgBox result
		  
		  
		  
		  Return result                                         'Ergebnis schon mit Sekunden zurückgeben
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Gurgeltime2yyyymmddhhmm(input as String) As String
		  'Funktion wird der Dateiname übergeben wandelt Gurgels Zeitcodierung ins Datenbakformat um
		  'z.B. 20080011200 --> 2008-01-01 12:00:00
		  Dim i As Integer
		  Dim sec as String
		  Dim min As String
		  Dim hour As String
		  Dim dayofyear As integer
		  Dim day As integer
		  Dim month As integer
		  Dim year As integer
		  
		  Dim montharray1(13) as Integer
		  Dim montharray2(13) as Integer
		  
		  Dim sk As integer                      'Korrekturwert für Schaltjahr
		  Dim result As String
		  
		  montharray1 = Array(0,31,59,90,120,151,181,212,243,273,304,334,365)  'Anzahl Tage ohne Schaltjahr
		  montharray2 = Array(0,31,60,91,121,152,182,213,244,274,305,335,366)  'Anzahl Tage mit Schaltjahr
		  
		  year=Val(Left(input,4))                'Jahr auslesen, Stelle 1-4
		  dayofyear=Val(Mid(input,5,3))    'Tag des Jahres auslesen, Stelle 5-8
		  hour=Mid(input,8,2)                     'Stunden auslesen, Stelle 8-9
		  min=Mid(input,10,2)                     'Minuten auslesen, Stelle 10-11
		  
		  if IsNumeric(Mid(input,12,2)) then
		    // It also has seconds!!!
		    sec=Mid(input,12,2)
		  else
		    sec="00"
		  end
		  
		  'Schaltjahr prüfen
		  If year Mod 4 = 0 And year Mod 100 = 0 Then
		    If year Mod 400 = 0 Then                                   ' Durch 400 teilbar?
		      sk=1
		    Else ' Nicht ganzzahlig teilbar.
		      sk=0
		    End If
		    
		  ElseIf year Mod 4 = 0 Then
		    sk=1
		  Else
		    sk=0
		  End If
		  
		  'Umrechnung TagdesJahres in Monat und Tag
		  
		  for i=0 to 12 Step 1
		    if sk=0 Then  'kein Schaltjahr
		      if dayofyear>=montharray1(i) and dayofyear<=montharray1(i+1) Then
		        day=dayofyear-montharray1(i)
		        month=i+1
		        exit
		      end if
		      
		    else
		      if dayofyear>=montharray2(i) and dayofyear<=montharray2(i+1) Then
		        day=dayofyear-montharray2(i)
		        month=i+1
		        exit
		      end if
		    end if
		    
		  next
		  
		  result= Str(year)+Str(month,"0#")+Str(day,"0#")+hour+min
		  'MsgBox result
		  
		  Return result
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MonthNo2String(mode as integer, monthNo as Integer) As string
		  
		  select case monthNo
		  case 1
		    select case mode
		    case 0
		      return "JAN"
		    case 1
		      return "Jan"
		    case 2
		      return "January"
		    case 3
		      return "JANUARY"
		    else
		      return "JAN"
		    end
		  case 2
		    select case mode
		    case 0
		      return "FEB"
		    case 1
		      return "Feb"
		    case 2
		      return "February"
		    case 3
		      return "FEBRUARY"
		    else
		      return "FEB"
		    end
		  case 3
		    select case mode
		    case 0
		      return "MAR"
		    case 1
		      return "Mar"
		    case 2
		      return "March"
		    case 3
		      return "MARCH"
		    else
		      return "MAR"
		    end
		  case 4
		    select case mode
		    case 0
		      return "APR"
		    case 1
		      return "Apr"
		    case 2
		      return "April"
		    case 3
		      return "APRIL"
		    else
		      return "APR"
		    end
		  case 5
		    select case mode
		    case 0
		      return "MAY"
		    case 1
		      return "May"
		    case 2
		      return "May"
		    case 3
		      return "MAY"
		    else
		      return "MAY"
		    end
		  case 6
		    select case mode
		    case 0
		      return "JUN"
		    case 1
		      return "Jun"
		    case 2
		      return "June"
		    case 3
		      return "JUNE"
		    else
		      return "JUN"
		    end
		  case 7
		    select case mode
		    case 0
		      return "JUL"
		    case 1
		      return "Jul"
		    case 2
		      return "July"
		    case 3
		      return "JULY"
		    else
		      return "JUL"
		    end
		  case 8
		    select case mode
		    case 0
		      return "AUG"
		    case 1
		      return "Aug"
		    case 2
		      return "August"
		    case 3
		      return "AUGUST"
		    else
		      return "AUG"
		    end
		  case 9
		    select case mode
		    case 0
		      return "SEP"
		    case 1
		      return "Sep"
		    case 2
		      return "September"
		    case 3
		      return "SEPTEMBER"
		    else
		      return "SEP"
		    end
		  case 10
		    select case mode
		    case 0
		      return "OCT"
		    case 1
		      return "Oct"
		    case 2
		      return "October"
		    case 3
		      return "OCTOBER"
		    else
		      return "OCT"
		    end
		  case 11
		    select case mode
		    case 0
		      return "NOV"
		    case 1
		      return "Nov"
		    case 2
		      return "November"
		    case 3
		      return "NOVEMBER"
		    else
		      return "NOV"
		    end
		  case 12
		    select case mode
		    case 0
		      return "DEC"
		    case 1
		      return "Dec"
		    case 2
		      return "December"
		    case 3
		      return "DECEMBER"
		    else
		      return "DEC"
		    end
		  else
		    return "---"
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MonthString2No(month as string) As string
		  
		  Select Case month
		  Case "JAN"
		    return "01"
		  Case "FEB"
		    return "02"
		  Case "MAR"
		    return "03"
		  Case "APR"
		    return "04"
		  Case "MAY"
		    return "05"
		  Case "JUN"
		    return "06"
		  Case "JUL"
		    return "07"
		  Case "AUG"
		    return "08"
		  Case "SEP"
		    return "09"
		  Case "OCT"
		    return "10"
		  Case "NOV"
		    return "11"
		  Case "DEC"
		    return "12"
		  else
		    return "--"
		  End Select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MoveFileFromTo(source As String, destination As String) As Boolean
		  'Funktion kopiert Datei von einem Ordner in einen anderen
		  'Bei Erfolg wird der Wert true zurückgegeben
		  'Fängt alle Eventualitäten wie Quell/Zielordner/Quelldatei nicht vorhanden ab.
		  
		  Dim fsource, fdestination As FolderItem
		  
		  fsource=GetFolderItem(source,FolderItem.PathTypeAbsolute)    'Quelldatei öffnen
		  
		  if fsource <>Nil Then
		    fsource.Permissions= &o777                                                           'FolderItem hat alle Rechte
		    
		    if fsource.exists then
		      fdestination=GetFolderItem(destination,FolderItem.PathTypeAbsolute)   'Zieldatei öffnen
		      
		      if fdestination<>Nil Then
		        fdestination.Permissions= &o777                                                                 'FolderItem hat alle Rechte
		        
		        fsource.CopyFileTo fdestination       'Quelldatei verschieben
		        if fsource.LastErrorCode=0  then        'Verschiebt Datei nur, wenn kein Fehler aufgetreten ist(z.B.File not found, File in use, out of memory...)
		          fsource.Delete 'Bei Erfolg auf jeden Fall Quelldatei löschen
		          Return true
		        else
		          log.Write(false,"Copy error or file in use: ("+source+" -> "+destination+") Error codes "+Str(fsource.LastErrorCode)+" "+Str(fdestination.LastErrorCode))
		          Return false
		        end if
		        
		      else
		        log.Write(false,"Copy error: destination "+destination+" returns Nil.")
		        Return false
		      end if
		      
		    else
		      log.Write(false,"Copy error: source "+source+" does not exist.")
		      Return false
		    end if
		  else
		    log.Write(false,"Copy error: source "+source+" returns Nil.")
		    Return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ProblemOn(input as Boolean) As Boolean
		  return input
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Quote(str2Quote as string) As string
		  
		  return(chr(34)+str2Quote+chr(34))
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ReadFromParamsCfgSingle(paramToRead as string, cfgFileName as string) As string
		  //Method used to read the value stored in only one parameter indicated by <paramToRead> in the params.cfg file indicated by <cfgFileName>.
		  //If the file cannot be opened, it will return the string "***CannotOpen***"
		  //If the parameter is not found, it will return the string "***NotFound***"
		  
		  // Opening params.cfg file =======================================================
		  
		  Dim f As New FolderItem
		  
		  if cfgFileName.Len>3 then
		    f=GetFolderItem("/home/wera/etc/" + cfgFileName)
		  else //empty string
		    f = GetFolderItem("/home/wera/etc/params.cfg")
		  end
		  
		  Dim configFile As TextInputStream
		  
		  If f <> Nil Then
		    If f.Exists Then
		      // Be aware that TextInputStream.Open coud raise an exception
		      Try
		        configFile = TextInputStream.Open(f)
		        configFile.Encoding = Encodings.ASCII
		      Catch e As IOException
		        configFile.Close
		        log.Write(false,"Error accessing " + cfgFileName  + " configuration file.")
		        return "***CannotOpen***"
		      End Try
		    Else
		      log.Write(false,cfgFileName  + " configuration file does not exists.")
		      return "***CannotOpen***"
		    End If
		  Else
		    log.Write(false,cfgFileName  + " configuration file is 'Nil'. Make sure folder exists.")
		    return "***CannotOpen***"
		  End If
		  
		  
		  // Reading params.cfg file =======================================================
		  
		  Dim linesOfTxtFile(-1), auxString, auxStringArray(-1) As String
		  Dim i as Integer
		  
		  linesOfTxtFile=Split(configFile.ReadAll,EndOfLine)  // Split the text content into lines
		  
		  configFile.Close // not needed anymore
		  
		  for i=0 to linesOfTxtFile.Ubound
		    
		    linesOfTxtFile(i)=linesOfTxtFile(i).ReplaceAll(chr(9)," ") //replace all tabs against a space
		    linesOfTxtFile(i) = Trim(linesOfTxtFile(i))  // remove spaces at beginning and end of string
		    auxString=left( linesOfTxtFile(i) , 1 )  // Getting the first written character to see if it is only a comment or a blank line
		    if auxString="/" or auxString="!" or auxString="%" or auxString=""  then  continue 'Empty line or comment
		    
		    auxStringArray=split(linesOfTxtFile(i),"=")
		    if Ubound(auxStringArray)<=0 then  continue  'No equal sign... this is no parameter
		    
		    auxString=Trim(auxStringArray(0))  'Storing everything what is left of the = sign
		    if InStr(0,auxString," ")>0 then continue   'If there are spaces in between, then it is no parameter
		    
		    if auxString=paramToRead then
		      auxStringArray=split(auxStringArray(1),"!")  // Remove comment
		      auxStringArray(0)=ReplaceAll(auxStringArray(0),Chr(9), " ")  //replace all tabs against a space
		      auxString=Trim(auxStringArray(0)) // The variable value should be in the first cell... we only remove spaces before and after
		      
		      return auxString
		    else
		      continue
		    end
		    
		    
		  next
		  
		  return "***NotFound***"
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ShellExec(command as String) As Boolean
		  'Executes the command given in a terminal window.
		  'Returns true when success... false otherwise
		  
		  Dim ShellObject as Shell
		  ShellObject= new Shell
		  ShellObject.Mode=0
		  
		  ShellObject.Execute(command)
		  
		  if ShellObject.ErrorCode<>0 then
		    std.lastMessage_std="fail___" + CStr(ShellObject.ErrorCode) + "___" + ShellObject.Result
		    return false
		  else
		    std.lastMessage_std=ShellObject.Result
		  end
		  
		  return true
		End Function
	#tag EndMethod


	#tag Note, Name = ActivateMBSPlugin
		
		  //Activate MBS Plugins
		  // Matthias Kniephoff, MBS Complete, 201308, 196748220
		  dim x as integer = 100
		  dim s as string = "MBS "
		  dim n as string = decodeBase64("TWF0dGhpYXMgS25pZXBob2Zm", encodings.UTF8)
		  dim y as integer = 20
		  if not registerMBSPlugin(n, s+"Complete", 2013*x+08, 1967482*x+y) then
		    ct.MsgLog(true,"MBS Plugin serial not valid?",True)
		   ct.SendOutputReport("3","1")
		  end if 
	#tag EndNote

	#tag Note, Name = ConsoleConstants
		NOTE: All constants should be global.
		
		kAppName as string 
		kLogFile as string
		
		// For the next ones use double semicolon ';;' to separate different lines (when more than one is available).
		kDescription = "description1;;description2;;description3"        <-- If you need to introduce an EndOfLine within the description, use a space, an asterisk, an underscore, an asterisk and another space ( *_* ). This is done, since introducing an EndOfLine in the constant content while programming in windows, will be wrong interpreted when executed from a linux system.
		kNotes= "note1;;note2;;note3"
		kSyntax= "syntax1;;syntax2;;syntax3"
		
		NOTE: When the application is used without input parameters... kSyntax should contain just a blank space (kSyntax=" ")
		
		kUse= "This console app is used for blabla"
		kInputTypes = "p;s;t"                 <HERE ONLY ONE ';'
	#tag EndNote

	#tag Note, Name = ConsoleMethods
		
		Function AssignInput(char as string, content as string) as boolean
		
		
		if StrComp(char,"p",0)=0 then
		//actions to assign input p
		elseif StrComp(char,"s",0)=0 then
		//actions to assign input s
		else
		return true
		end
		
		return false
		
		
		
		
		Protected Function CheckEverything() as Boolean
		
		//=================== Checking input arguments ===========================
		
		
	#tag EndNote

	#tag Note, Name = ConsoleNotes
		
		
		- Use external ConsoleTools module (ct) located in commontools folder
		
		- If comuncating to database, use external db module located in commontools folder
		
		- If no log file is required, just add local module to replace log.Write method inside db module.
		
		                    Protected Sub Write(display as Boolean, msg as string) 
		                                    ct.MsgLog(false,msg, true)
		
		- If log into file is required, use log external module in commontools folder.
		
		- In properties of app:
		  ---- Properly set MajorVersion and NonReleaseVersion for version handling
		  ---- Activate the AutoIncrementVersionInformation checkbox
		  ---- Use #App.kAppName for AppName in linux and Mac. For windows use #App.kAppName + ".exe"
		  ---- Activate the IncludeFunctionsName checkbox
		  ---- CompanyName=Helzel Messtechnik GmbH
		
		-Use Builds folder when compiling.
		
		
	#tag EndNote

	#tag Note, Name = ConsoleProperties
		
		execDir as string   GLOBAL VARIABLE... NOT PROTECTED OR PRIVATE!!!
	#tag EndNote

	#tag Note, Name = ConsoleRun
		  
		  if DebugBuild then
		    ct.MsgLog(false," ",True)
		    ct.MsgLog(false,"AAHH... ICH LAUFE GERADE IN DEBUG MODE. VERGISS NICHT DIE QUELLCODE ÄNDERUNGEN ZU DOKUMENTIEREN!!! (mach es in App.Version_Notes).",True)
		    execDir="/home/wera/ApplicationSoftware/DataArchivePrograms/"
		    break
		  else
		    execDir=App.ExecutableFile.Parent.AbsolutePath
		  end
		  
		  ct.ConsoleOutput=Join(args," ") + EndOfLine
		  
		  //================ Check input arguments ============
		  if ct.CheckInputs(args) then
		    ct.MsgLog(false," ",True)
		    Quit
		  end
		  
		
		  //============Licence check================
		  if key.LoadLicenceFile(App.execDir + "../") then
		if not(std.ActivateMBSComplete) then  ct.MsgLog(false,"MBS Plugin serial not valid?",True)
		    if key.LicenceIsValidOnThisPC then
		      if not( key.PackageIsValid(key.Packages.???))  then
		        ct.MsgLog(true,"Licence does not include WERA "???" tools! Application terminated.",true)
		        Quit
		      end
		    else
		      ct.MsgLog(true,"Could not validate licence! Application terminated.",true)
		      ct.MsgLog(false, key.lastMessage_key,true)
		      Quit
		    end
		  else
		    ct.MsgLog(true, "Could not load licence file! Application terminated.", true)
		    ct.MsgLog(false, key.lastMessage_key,true)
		    Quit
		  end
		
		
		  //================ Other checks required ============
		  if CheckEverything() then
		    ct.MsgLog(false," ",True)
		    Quit
		  end
		  
		
		  //================ Check database connection ============
		  db.archive=new MySQLDatabase
		  if not(db.ReadDBSetupFile(execDir+"../DataArchivePrograms/DataArchive_setup.txt")) then
		    ct.MsgLog(true,db.lastMessage_db,True)
		    'Quit
		  end
		  if db.CheckDatabaseConnection then
		    ct.MsgLog(false,db.lastMessage_db,True)
		  else
		    ct.MsgLog(true,db.lastMessage_db + " Application terminated.",True)
		    Quit
		  end
		
		
		  
		  //============== This is the actual program ==============
		
		  //========================================================
		
		  db.archive.Close
		  ct.MsgLog(false,"Finished!",True)
		  ct.MsgLog(false," ",True)
		
	#tag EndNote

	#tag Note, Name = ConsoleUnhandledException
		
		
		
		  ct.MsgLog(false," ",True)
		  ct.MsgLog(false,"Unhandled Exception! Application terminated.",True)
		  ct.MsgLog(false,error.Message + ". Error code: " + Str(error.ErrorNumber),True)
		  ct.MsgLog(false,"Please contact " + std.kContactEmail + " for technical support.",True)
		  
		
		
		
		
		or when log file is available...
		
		 ct.MsgLog(false," ",True)
		  ct.MsgLog(false,"Unhandled Exception! Application terminated.",True)
		log.Write(true, "Unhandled Exception! " + EndOfLine + error.Message + ". Error code: " + Str(error.ErrorNumber) + EndOfLine + "Please contact " + std.kContactEmail + " for technical support.")
		
		
		
		
		
		
		when error stack is available and we want to notify Helzel using the Monitoring System emails...
		  
		  dim errorString, stackArray() as String
		  dim i as integer
		  
		  errorString=EndOfLine + EndOfLine + "There was an unhandled exception!"
		  errorString=errorString + EndOfLine + EndOfLine + "Error number " + CStr(error.ErrorNumber) + ": " + error.Message + EndOfLine + EndOfLine
		  errorString=errorString + "Error stack:" + EndOfLine
		  
		  stackArray=error.Stack
		  for i=stackArray.Ubound DownTo 0
		    errorString=errorString + stackArray(i) + EndOfLine
		  next
		  
		  ct.MsgLog(false," ",True)
		  ct.MsgLog(false,"Unhandled Exception! Application terminated.",True)
		  
		  ct.MsgLog(false, errorString,True)
		
		log.Write(false, ct.ConsoleOutput)
		  
		  ct.SendOutputReport("3","1")
	#tag EndNote

	#tag Note, Name = DesktopAboutMenu
		
		  dim tmpString as String
		  
		  if App.StageCode=3 then
		    tmpString=App.kAppName + " V" + CStr(App.MajorVersion) + "." + CStr(App.NonReleaseVersion) + " (" + App.BuildDate.SQLDate + ")" + EndOfLine + EndOfLine
		  else
		    tmpString=App.kAppName + " V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + "_BETA (" + App.BuildDate.SQLDate + ")" + EndOfLine
		  end
		  
		  tmpString=tmpString + "(C)opyright " + CStr(App.BuildDate.Year) + " Helzel Messtechnik GmbH - www.helzel.com. All rights reserved." + EndOfLine
		  tmpString=tmpString + "Licence: " + key.kg.UserInfo.Lookup("company","NoCompany!").StringValue + ", " + key.kg.UserInfo.Lookup("user","NoUser!").StringValue+ EndOfLine
		  
		  MsgBox(tmpString)
		  
		  Return True
	#tag EndNote

	#tag Note, Name = SomeNotesToRemember
		===============================
		In General...
		
		
		-To convert doubles or singles to strings, do not use CStr... use better the following to avoid losing precision:
		for singles:
		              auxSgl=CDbl(replace(Format(auxStr,"-#.######e"),",","."))
		for doubles:
		              auxDbl=CDbl(replace(Format(auxStr,"-#.##############e"),",","."))
		
		================================
		-For Desktop applications,
		
		
		-App.open is executed first, and then window.open.
		-Calling Quit method in App.open will not avoid executing window.open event.
		
		
		
		
		
		================================
		-For Web applications,
		
		
		-App.open is executed first, then the Session.open event is fired, then the open event of the lowest level object in the main window, going up to the open event of the main window. Then the shown event of the main window going down to the shown event of the lowest level object of the window.
		
		
		
		
		
		
	#tag EndNote

	#tag Note, Name = stdUpdates
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new updates in the next line, not at the end!!! latest update info should ALWAYS be displayed first!!!
		
		2016.03.21
		Roberto Gomez
		-Modified ShellExec method to include the string "fail___" at the beginning of lastMessage_std and correctly indicate a failure of execution for ct.ShellExecC method.
		
		2016.08.05
		Roberto Gomez
		-Modified method's name to Gurgeltime2yyyymmddhhmm and Date2yyyymmddhhmmss (these methods were created before, but not documented).
		
		2015.02.18
		Roberto Gomez
		-Using lastMessage_std property in CheckForFolderitem method.
		
		2015.01.22
		Roberto Gomez
		-Changed property lastMessage to lastMessage_std to avoid compilation error in Xojo
		
		2014.11.25
		Roberto Gomez
		-Updated ConsoleRun notes to include activation of MBS plugin before checking WERA licence to avoid warning.
		
		2014.09.29
		Roberto Gomez
		-Updated ConsoleRun notes to include licence check.
		
		2014.09.29
		Roberto Gomez
		-Added DesktopAbout notes.
		
		2014.07.28
		Roberto Gomez
		-Added CreateFolderWhenNotExistent Method.
		
		2014.07.28
		Roberto Gomez
		-Added lastMessage property for a better error handling scheme.
		-Modified ShellExec method to use lastMessage and return a boolean
		
		2014.07.08
		Roberto Gomez
		-Modified ConsoleRun notes to display results of db.ReadDBParams and db.CheckDatabase outside the method.
		
		2014.02.12
		Roberto Gomez
		-Added DecodeKlCuv2Qual to decode the kl value from Cuv data in database to a quality value according to the QC levels of WERA 
		
		2013.12.01
		Roberto Gomez
		-Added ActivateMBSComplete method to activate complete license of MBS Plugins.
		
		2013.12.01
		Roberto Gomez
		-Corrected in method MonthString2No for December
		
		2013.11.26
		Roberto Gomez
		-Now using ReplaceAll for notifying via monitoring system on ConsoleUnhandledException note
		
		2013.11.25
		Roberto Gomez
		-Introduced method ReadFromParamsCfgSingle, to read the content of only one parameter in the params.cfg file.
		
		2013.10.10
		Roberto Gomez
		-Introduced method Date2Gurgeltime
		
		2013.10.10
		Roberto Gomez
		-Introduced method CalculateSeconds
		
		2013.08.22
		-Changed methods from Check_For_Folderitem, copy_file_from_to, move_file_from_to  -->  CheckForFolderitem, CopyFileFromTo, MoveFileFromTo     this is to avoid problems with same methods in DataManager while DataManager is not yet standardized.
		-CheckForFolderitem has now a verbose input to indicate if we want to log if the file does not exist or could not be opened.
		
		2013.08.19
		-Removed the first empy line on run event handler. This way the first line that is returned when using a console app with -v as input, will return the app name and version in the first line. 
		
		2013.08.01
		-Introduced method CalculateHours
		
		2013.07.15
		-Introduced methods used commonly (Check_For_Folderitem, copy_file_from_to, move_file_from_to)
		
		2013.07.15
		-First version created with all standards for console applications
		
		
		IMPORTANT!!! Insert notes of new update at the beginning, not at the end!!! Latest update info should ALWAYS be displayed first!!!
		
	#tag EndNote

	#tag Note, Name = Updates
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new updates in the next line, not at the end!!! latest update info should ALWAYS be displayed first!!!
		
		2013.07.15
		-First version created.
		
		
		
		IMPORTANT!!! Insert notes of new update at the beginning, not at the end!!! Latest update info should ALWAYS be displayed first!!!
		
	#tag EndNote

	#tag Note, Name = VersionNotes
		
		
		HOW TO FILL THIS NOTES...
		
		=Filename= (Notes... if applies. e.g. Version compiled for MELCO)
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		App version (Built using App.MajorVersion and App.NonReleaseVersion. CHECK THE VALUE OF App.NonRealeaseVersion AFTER COMPILING... COMPILING INCREASES THE VALUE AUTOMATICALLY!), 
		Real Studio version when compiled (check it on Hilfe>Über RealStudio) / Operative system (compiled with linux or windows)
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		IMPORTANT!!! Insert notes of new version in the next line, not at the end!!! last version info should ALWAYS be displayed first!!!
		
		=AddWrad2DataArchive_V2_130715=
		2013.07.15, 
		Roberto Gomez
		Version 2.1
		RealStudio 2012 r2.1 / Windows 7
		-Beta version to work with the new database structure
		
		
		IMPORTANT!!! Insert notes of new version at the beginning, not at the end!!! last version info should ALWAYS be displayed first!!!
	#tag EndNote


	#tag Property, Flags = &h0
		lastMessage_std As String
	#tag EndProperty


	#tag Constant, Name = kComName, Type = String, Dynamic = False, Default = \"Helzel Messtechnik GmbH", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = kContactEmail, Type = String, Dynamic = False, Default = \"hzm@helzel.com", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = kRadName, Type = String, Dynamic = False, Default = \"WERA", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = kWebPage, Type = String, Dynamic = False, Default = \"http://www.helzel.com", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = NOTE_IACIT, Type = String, Dynamic = False, Default = \"Company Name: IACIT SOLU\xC3\x87\xC3\x95ES TECNOL\xC3\x93GICAS\rContact Email: info@iacit.com.br\rRadar Name: RADH\rWeb Page: http://www.iacit.com.br", Scope = Protected
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lastMessage_std"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
