#tag Module
Protected Module ct
	#tag Method, Flags = &h1
		Protected Function CheckInputs(args() as string) As Boolean
		  // Check the input arguments as defined from App.kInputTypes, complying with the input arguments of the specific console app. For each argument found calls App.AssignInput(<letter>).
		  
		  // NOTE: App.kInputTypes defines how many inputs arguments are and with which letter they are identified.
		  // Example: App.kInputTypes="s;t;m" consits of three input arguments identified with -s=<argument_s> -t=<argument_t> -m=<argument_m>. Order is not important.
		  
		  //   *************** THE FOLLOWING NOTE IS NOT VALID AFTER XOJO!!!!  ***********************
		  // NOTE: on linux the contents of args is:
		  // args(0)  -  The path to exectuable file
		  // args(1)  -  Input parameter 1
		  // args(2)  -  Input parameter 2
		  // .....
		  // args(N-1) -  Input parameter N-1
		  // args(N) -  Last parameter is always an empty string (this does not happens in windows)   <---THE EXTRA EMPTY STRING IS NOT THERE AFTER XOJO
		  //*************************************************************
		  
		  Dim i, j as Integer
		  Dim auxStr(-1) as String
		  dim input as String
		  
		  auxStr=Split(App.kInputTypes,";")
		  //=================== Checking input arguments ===========================
		  
		  if UBound(args)=0 AND App.kInputTypes="" then   // No input arguments only allowed when the console does not require inputs at all.
		    return false
		  end
		  
		  if UBound(args) = 1 then
		    if args(1) = "-h" then
		      ShowHelp()
		      Return true
		    elseif args(1) = "-v" then
		      if App.StageCode<3 then
		        MsgLog(false,App.kAppName + " V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + " (" + App.BuildDate.SQLDate + ") Beta" ,true)
		      else
		        MsgLog(false,App.kAppName + " V" + CStr(App.MajorVersion) +  "." + CStr(App.NonReleaseVersion) + " (" + App.BuildDate.SQLDate + ")" ,true)
		      end if
		      Return true
		    end
		  end if
		  
		  if UBound(args) >1+auxStr.Ubound then
		    MsgLog(false,"Too many input arguments! Application terminated.",True)
		    MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		  end
		  
		  if UBound(args) = 0 then
		    MsgLog(false,"No input arguments! Application terminated.",True)
		    MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		  end
		  
		  for i=1 to args.Ubound
		    
		    input=left(args(i),3)
		    
		    if input.Len=2 then   //This is for the case an input argument does not have a content (input flag)
		      input=input + "="
		    end
		    
		    for j=0 to auxStr.Ubound
		      if StrComp(input,"-"+ auxStr(j) + "=",0)=0 then
		        if App.AssignInput(auxStr(j),Right(args(i),args(i).Len-3)) then
		          Return True
		        end
		        continue for i
		      end
		    next
		    
		    MsgLog(false,"Input argument not identified! Application terminated.",True)
		    MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		    
		  next
		  
		  MsgLog(false,"    Input arguments OK",true)
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub CopyrightNote()
		  MsgLog(false," ",True)
		  MsgLog(false,"(C)opyright " + CStr(App.BuildDate.Year) + " Helzel Messtechnik GmbH - www.helzel.com. All rights reserved." ,true)
		  
		  
		  //============Licence check================
		  if key.LoadLicenceFile(App.execDir + "../") then
		    if not(std.ActivateMBSComplete) then  ct.MsgLog(false,"MBS Plugin serial not valid?",True)
		    if key.LicenceIsValidOnThisPC then
		      MsgLog(false,"Licence: " + key.kg.UserInfo.Lookup("company","NoCompany!").StringValue + ", " + key.kg.UserInfo.Lookup("user","NoUser!").StringValue,true)
		      MsgLog(false," ",true)
		      quit
		    end
		  end
		  
		  
		  MsgLog(false, key.lastMessage_key,true)
		  MsgLog(false," ",true)
		  
		  'MsgLog(false,"This material may not be reproduced, displayed, modified or distributed without the express prior written permission of Helzel Messtechnik GmbH. For permission, contact [hzm@helzel.com].",true)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub MsgLog(logToFile as Boolean, msg as String, newLine as Boolean)
		  // Prints the contents of msg in the console screen.
		  If ct.MODE=0 then
		    if newLine then
		      stdout.WriteLine(msg)
		    else
		      stdout.Write(msg + " ")
		    end
		    
		    // Update ConsoleOutput property
		    if newLine then
		      ConsoleOutput=ConsoleOutput + msg + EndOfLine
		    else
		      ConsoleOutput=ConsoleOutput + msg + " "
		    end
		    
		    if logToFile then
		      log.Write(false,msg)
		    end
		    
		  else
		    log.Write(false,msg)
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SendOutputReport(priority as string, destination as string)
		  
		  dim now as new Date
		  dim msgHelzel as string   // Create message file to email Helzel and notify about the unhandled exception via the WERA Monitoring System.
		  msgHelzel = now.SQLDateTime + Chr(9) + "Server" + Chr(9) + App.kAppName + Chr(9) + priority + Chr(9) +destination + Chr(9) + ReplaceAll(ConsoleOutput,EndOfLine," *_* ")
		  if log.WriteToFile(App.execDir + "../SystemMonitor/inbox/" + std.Date2Gurgeltime(now,true) + "_Server_" + App.kAppName + ".status", msgHelzel , true ) then
		    MsgLog(false,"Could not create error report file!",true)
		    MsgLog(false,"Please contact Helzel GmbH (hzm@helzel.com) for technical support and supply the displayed error details. Thanks!",true)
		  else
		    if priority="3" AND destination="1" then
		      MsgLog(false,"Error report sent to Helzel Messtechnik!",true)
		    else
		      MsgLog(false,"Error report created with priority " + priority + " and destination " + destination,true)
		    end
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ShellExecC(command as String, message as string) As Boolean
		  'Executes the command given in a terminal window using std.ShellExec.
		  
		  'Returns true when failure, false otherwise.
		  
		  dim auxStr(0) as String
		  
		  if not(std.ShellExec(command)) then
		    
		    if left(std.lastMessage_std,7)="fail___" then
		      auxStr=Split(auxStr(0),"___")
		      MsgLog(true,message + " Application terminated.",true)
		      MsgLog(true,"Error " + auxStr(1) + EndOfLine + auxStr(2),true)
		      return true
		      
		    end
		    
		  end if
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ShowHelp()
		  // Function to display the help information of a general console application.
		  // The content of this function should be declared as app constants. For cases where more than one line needs to be displayed use double semicolon to separate one line from the other (;;)
		  
		  dim syntaxes(-1), descriptions(-1), notes(-1), auxStr1, auxStr2 as String
		  dim i as integer
		  
		  // Displays how to use the console application input arguments
		  
		  auxStr1=App.kAppName + " V" + CStr(App.MajorVersion) + "." + CStr(App.NonReleaseVersion)
		  
		  auxStr2=""
		  for i=0 to len(auxStr1)+5
		    auxStr2=auxStr2+"="
		  next
		  
		  MsgLog(false," ",true)
		  MsgLog(false,auxStr2,true)
		  MsgLog(false,"   " + auxStr1 + "   ",true)
		  MsgLog(false,auxStr2,true)
		  MsgLog(false," ",true)
		  MsgLog(false,"    " + App.kUse,true)
		  
		  
		  syntaxes=Split(App.kSyntax,";;")
		  descriptions=Split(App.kDescription,";;")
		  notes=Split(App.kNotes,";;")
		  
		  MsgLog(false," ",true)
		  MsgLog(false,"USAGE",true)
		  MsgLog(false," ",true)
		  MsgLog(false,"-" + App.kAppName + " -h",true)
		  MsgLog(false,"    " + "Shows the help contents of the application.",true)
		  MsgLog(false," ",true)
		  MsgLog(false,"-" + App.kAppName + " -v",true)
		  MsgLog(false,"    " + "Shows only the name and version number of the application.",true)
		  
		  if syntaxes.Ubound>-1 then
		    for i=0 to syntaxes.Ubound
		      MsgLog(false," ",true)
		      MsgLog(false,"-" + App.kAppName + " " + syntaxes(i),true)
		      descriptions(i)=ReplaceAll(descriptions(i)," *_* ",EndOfLine + "    ")
		      MsgLog(false,"    " +descriptions(i) ,true)
		    next
		  end
		  
		  if notes.Ubound>-1 then
		    MsgLog(false," ",true)
		    MsgLog(false,"NOTES",true)
		    for i=0 to notes.Ubound
		      MsgLog(false,"    " + notes(i),true)
		    next
		  end
		  
		  CopyrightNote()
		  
		End Sub
	#tag EndMethod


	#tag Note, Name = Updates
		
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new updates in the next line, not at the end!!! latest update info should ALWAYS be displayed first!!!
		
		2016.03.21
		Roberto Gomez
		-Modified ShellExecC to correctly detect failure of shell execution.
		
		2015.01.23
		Roberto Gomez
		-Changed App.ExecutableFile.CreationDate to App.BuildDate in CopyrightNote method
		
		2015.01.23
		Roberto Gomez
		-Now that Xojo is being used, the extra empty argument is not there anymore (see note at beginning of CheckInputs method). Modified code to cope with that.
		
		2015.01.22
		Roberto Gomez
		-Now that Xojo is being used, changed App.ExecutableFile.CreationDate to App.BuildDate in CheckInputs method when giving -v as input, because is not really the same.
		
		2014.25.11 (I guess this should be 2014.11.25)
		Roberto Gomez
		-Activating MBS plugin serial before checking if WERA licence is valid on method CopyrightNote, to avoid warning on MBS licence.
		
		2014.09.10
		Roberto Gomez
		-Included licence details in CopyrightNote
		
		2014.09.10
		Roberto Gomez
		-Changed from App.ModificationDate to App.CreationDate
		
		2014.08.07
		Roberto Gomez
		-Using StrComp to differentiate inputs that are different only by case.
		
		2014.08.06
		Roberto Gomez
		-Added the possibility to introduce end of lines in the description constants by using ' *_* '.
		
		2014.07.01
		Roberto Gomez
		-Modified other methods according to new input in MsgLog method (introduced on 2014.04.25).
		-Inverted logic of IF statement in SendOutputReport method to avoid stating that message could not be created when it was created succesfully. 
		
		2014.04.25
		Roberto Gomez
		-Added inputs in method MsgLog to enable log to file
		-Added method SendOutputReport to send the console output to the WERA SystemMonitor email infraestructure.
		
		2013.10.29
		Roberto Gomez
		-If console tool is beta it will be shown when using -v input
		-Added ConsoleOutput property to store the accumulated output text of the console. Implemented it on MsgLog method.
		
		2013.10.14
		Roberto Gomez
		-Updated line from < if input= ("-"+ auxStr(i-1) + "=") then > to < if input= ("-"+ auxStr(j) + "=") then >. Not sure how long this line was incorrect and if it has affeccted other programs.
		
		2013.08.20
		Roberto Gomez
		-Giving -v as input will also give the date of compilation.
		-Modified ShowHelp method to not show the date of compilation anymore. It also shows now the name of the program as a title.
		
		2013.08.19
		-Added the possibility of give -v as input, to only show the name and version of the console application.
		
		2013.08.16
		-Modified CheckInputs method in case an input argument does not have a content. Useful for input arguments which are just flags (if -e is there, then activate certain feature... without having to indicate a content: -e=somevalue).
		-Modified CheckInputs method to work in case the console application does not require inputs at all... for example ClearDataArchive, or UpdateStation2DataArchive.
		
		2013.07.15
		-First version created.
		
		
		
		IMPORTANT!!! Insert notes of new update at the beginning, not at the end!!! Latest update info should ALWAYS be displayed first!!!
		
	#tag EndNote


	#tag Property, Flags = &h1
		Protected ConsoleOutput As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Mode As Integer = 0
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Mode"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
