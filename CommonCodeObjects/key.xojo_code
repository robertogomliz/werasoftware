#tag Module
Protected Module key
	#tag Method, Flags = &h0
		Function LicenceIsValidOnThisPC() As Boolean
		  
		  dim m as String
		  dim s as Shell
		  dim strArray1(-1), strArray2(-1), cpumodel, cpuspeed as String
		  s=new shell
		  s.mode=0
		  dim i,j as integer
		  
		  if not(key.skipMac) then
		    //Get MAC address
		    s.Execute("/sbin/ifconfig")
		    if s.ErrorCode<>0 then
		      lastMessage_key="Code 0.0 - Unable to check licence! Contact " + std.kComName + " (" + std.kContactEmail + ")"
		      return false
		    end
		    strArray1=Split(s.Result,EndOfLine)
		    
		    for i=0 to strArray1.Ubound
		      strArray2=Split(strArray1(i).Trim," ")
		      m=ReplaceAll(strArray2(strArray2.Ubound),":","")
		      if m.Len=12 then
		        exit
		      end
		    next
		    if m.Len<>12 then
		      lastMessage_key="Code 0.1 - Unable to check licence! Contact " + std.kComName + " (" + std.kContactEmail + ")"
		      return false
		    end
		    kg.UserInfo.Value("mac")=m
		  end
		  
		  
		  //Get number of cores written in format "##"
		  
		  s.Execute("grep --count processor /proc/cpuinfo")
		  
		  if s.ErrorCode<>0 then
		    lastMessage_key="Code 1.0 - Unable to check licence! Contact " + std.kComName + " (" + std.kContactEmail + ")"
		    return false
		  end
		  kg.UserInfo.Value("cpuproc")=CStr(CLong(s.result))
		  
		  
		  //Get Vendor ID and CPU MHz (old method)
		  's.Execute("lscpu")
		  'if s.ErrorCode<>0 then
		  'lastMessage_key="Code 1.1 - Unable to check licence! Contact " + std.kComName + " (" + std.kContactEmail + ")"
		  'return false
		  'end
		  'strArray1=Split(s.Result,EndOfLine)
		  'for i=0 to strArray1.Ubound
		  'strArray2=Split(strArray1(i).Trim,":")
		  'if strArray2.Ubound>0 then
		  'strArray2(0)=strArray2(0).Trim
		  'if strArray2(0)="Vendor ID" then vendid=strArray2(1).Trim
		  'if strArray2(0)="CPU MHz" then mhz=strArray2(1).Trim
		  'end if
		  'next
		  'if vendid.Len>15 then vendid=vendid.Left(15)
		  'if vendid.len=0 then
		  'lastMessage_key="Code 1.2 - Unable to check licence! Contact " + std.kComName + " (" + std.kContactEmail + ")"
		  'return false
		  'end if
		  'kg.UserInfo.Value("cpuvendor")=vendid
		  '
		  'if mhz.Len>10 then mhz=mhz.Left(10)
		  'if mhz.len=0 then
		  'lastMessage_key="Code 1.3 - Unable to check licence! Contact " + std.kComName + " (" + std.kContactEmail + ")"
		  'return false
		  'end if
		  'kg.UserInfo.Value("cpuspeed")=mhz
		  
		  
		  //Get CPU model and speed
		  dim c as new CPUIDMBS
		  strArray1=split(c.BrandString,"@")
		  if strArray1.Ubound<1 then
		    cpuspeed = "N/A"
		    //lastMessage_key="Code 1.1 - Unable to check licence! Contact " + std.kComName + " (" + std.kContactEmail + ")"
		    //return false
		  else
		    cpuspeed=strArray1(1).Trim
		  end if
		  cpumodel=ReplaceAll(strArray1(0)," ","").Trim
		  
		  //cpumodel=ReplaceAll(strArray1(0)," ","").Trim
		  if cpumodel.Len>30 then cpumodel=cpumodel.Left(30)
		  if cpumodel.len=0 then
		    lastMessage_key="Code 1.2 - Unable to check licence! Contact " + std.kComName + " (" + std.kContactEmail + ")"
		    return false
		  end if
		  kg.UserInfo.Value("cpumodel")=cpumodel
		  //cpuspeed=strArray1(1).Trim
		  if cpuspeed.Len>10 then cpuspeed=cpuspeed.Left(10)
		  if cpuspeed.len=0 then
		    lastMessage_key="Code 1.3 - Unable to check licence! Contact " + std.kComName + " (" + std.kContactEmail + ")"
		    return false
		  end if
		  kg.UserInfo.Value("cpuspeed")=cpuspeed
		  
		  
		  
		  //Get number of usb ports (using -t to identify which are main usb ports and not be affected by usb hubs or similar)
		  s.Execute("lsusb -t")
		  if s.ErrorCode<>0 then
		    lastMessage_key="Code 2.0 - Unable to check licence! Contact " + std.kComName + " (" + std.kContactEmail + ")"
		    return false
		  end
		  strArray1=Split(s.Result,EndOfLine)
		  j=0
		  for i=0 to strArray1.Ubound
		    if strArray1(i).Left(2)="/:" then j=j+1
		  next
		  kg.UserInfo.Value("usbports")=CStr(j)
		  
		  
		  
		  // Verify hardware with key...
		  If not(kg.ValidateKey( key_string )) Then
		    
		    //The key is not valid... it could be by two reasons:
		    //     1-The key provided does not match to hardware information. In such case kg.Expiration should be a NIL object, since this is loaded out of a valid key.
		    //     2-The key provided matches the hardware information, but the licence expired already. In such case kg.Expiration should contain a date object.
		    //So lets check on kg.Expiration to know what happened...
		    if kg.Expiration<>Nil then
		      lastMessage_key = "Your licence expired since " + kg.Expiration.SQLDateTime
		    else
		      lastMessage_key="WERA licence is not valid!"
		    end if
		    return false
		    
		  end
		  
		  lastMessage_key="WERA Licence is valid!"
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadLicenceFile(somePath as string) As Boolean
		  
		  // Opening key.asc file =======================================================
		  Dim f As New FolderItem
		  Dim t As TextInputStream
		  
		  if somePath.Trim="" then
		    f=GetFolderItem("/home/wera/wera.key")
		  else
		    f=GetFolderItem(somePath)
		  end
		  
		  If f <> Nil Then
		    if f.Directory then
		      f=f.Child("wera.key")
		    end
		    
		    If f.Exists Then
		      // Be aware that TextInputStream.Open coud raise an exception
		      Try
		        t = t.Open(f)
		        t.Encoding = Encodings.ASCII
		      Catch e As IOException
		        t.Close
		        lastMessage_key= "Error accessing licence file."
		        return false
		      End Try
		    Else
		      lastMessage_key= "Licence file does not exists."
		      return false
		    End If
		  Else
		    lastMessage_key= "Licence file is 'Nil'. Make sure folder exists."
		    return false
		  End If
		  
		  
		  // Reading key.asc file ======================================================
		  
		  dim txt, txtLines(-1) as string
		  txt=t.ReadAll
		  t.close
		  
		  txtLines=split(txt,Chr(10))
		  
		  if txtLines.Ubound<>3 then
		    lastMessage_key="Incorrect number of lines found in licence file!"
		    return false
		  end if
		  
		  
		  if txtLines(0)="000" then
		    skipMac=false
		  elseif txtLines(0)="001" then
		    skipMac=true
		  else
		    lastMessage_key="Unkown licence mode " + txtLines(0)
		    return false
		  end if
		  
		  kg=new Keygen
		  kg.UserInfo.Value( "user" ) =txtLines(1)
		  kg.UserInfo.Value( "company" ) =txtLines(2)
		  key_string = txtLines(3)
		  
		  lastMessage_key= "Licence file successfully loaded."
		  Return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function PackageIsValid(PackageToCheck as Integer) As Boolean
		  
		  if kg.Flags.Ubound<PackageToCheck then
		    return false
		  elseif kg.Flags(PackageToCheck) then
		    return true
		  else
		    return false
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function PackageIsValid(PackageToCheck as Packages) As Boolean
		  
		  dim i as integer
		  
		  select case PackageToCheck
		  case Packages.SystemMonitor
		    i=0
		  case Packages.DataManager
		    i=1
		  case Packages.DataArchive
		    i=2
		  case Packages.DataViewer
		    i=3
		  case Packages.QualityControl
		    i=4
		  case Packages.SV_SWB
		    i=5
		  case Packages.OceanWarning
		    i=6
		  case Packages.GF_Forecast
		    i=7
		  case Packages.ActimarSywab
		    i=8
		  case Packages.ActimarArret
		    i=9
		  end
		  
		  return PackageIsValid(i)
		End Function
	#tag EndMethod


	#tag Note, Name = LicenceBooleanValues
		
		
		
		2014.09.26
		
		
		0.- SystemMonitor
		1.- DataManager
		2.- DataArchive Tools
		3.- DataViewer
		4.- QualityControl (artefact removing or cleanmaps)
		5.- SWB SeaView
		6.- OceanWarning
		7.- Gap Filling / Forecast / CurExtrap Actimar
		8.- SWB Actimar
		9.- Artefact removing Actimar
		
		
		
		
		OLD==================
		
		1.- DataManager
		2.- DataArchive Tools
		3.- DataViewer
		4.- Export tools (???)
		5.- Clean Maps (artifact removing)
		6.- Gap Filling
		7.- Ocean Warning
		8.- SWB SeaView
		
	#tag EndNote

	#tag Note, Name = Updates
		
		
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new updates in the next line, not at the end!!! latest update info should ALWAYS be displayed first!!!
		
		2015.01.22
		Roberto Gomez
		-Changed property lastMessage to lastMessage_key to avoid compilation error in Xojo
		
		2014.09.29
		-Modified to use CPUIDMBS class to obtain cpu information (cpuspeed and cpumodel)
		-LicenceIsValidOnThisPC now returns true when successful.
		
		2014.09.26
		-Included lastMessage property to avoid using log.write
		-Implemented using also CPU speed, vendor and number of processors, as well as number of usb ports.
		-Included skipMac property to make it possible to include or not include the MAC address into the key.
		
		2013.????
		Roberto Gomez
		-First version created.
		
		
		
		IMPORTANT!!! Insert notes of new update at the beginning, not at the end!!! Latest update info should ALWAYS be displayed first!!!
		
		
		
		
		
	#tag EndNote


	#tag Property, Flags = &h0
		key_string As string
	#tag EndProperty

	#tag Property, Flags = &h0
		kg As Keygen
	#tag EndProperty

	#tag Property, Flags = &h0
		lastMessage_key As String
	#tag EndProperty

	#tag Property, Flags = &h0
		skipMac As Boolean
	#tag EndProperty


	#tag Enum, Name = Packages, Type = Integer, Flags = &h0
		SystemMonitor
		  DataManager
		  DataArchive
		  DataViewer
		  QualityControl
		  SV_SWB
		  OceanWarning
		  GF_Forecast
		  ActimarSywab
		ActimarArret
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="key_string"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lastMessage_key"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="skipMac"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
