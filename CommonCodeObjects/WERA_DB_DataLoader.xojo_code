#tag Class
Protected Class WERA_DB_DataLoader
	#tag Method, Flags = &h0
		Function LoadBasicGrid(gridID as integer) As Boolean
		  
		  db.grid=CStr(gridID)
		  grid_ID=0
		  BasicGridInfoLoaded=false
		  
		  dim rs as RecordSet
		  dim i as integer
		  
		  // Get grid size =================================================
		  rs=db.GetGridData()
		  if rs=nil then
		    lastMessage="Grid size could not be loaded"
		    return false
		  end
		  if rs.EOF then
		    lastMessage="Grid not found!"
		    return false
		  end
		  
		  nx=rs.Field("nx").IntegerValue
		  ny=rs.Field("ny").IntegerValue
		  
		  // Redim all basic grid variables...
		  Redim id_ixiys(nx*ny-1)
		  Redim ixs(nx*ny-1)
		  Redim iys(nx*ny-1)
		  Redim id_ixiy(nx-1,ny-1)
		  Redim lat(ny-1)
		  Redim lon(nx-1)
		  
		  
		  // Get grid information =================================================
		  
		  rs=db.GetIdIxIyFromGridID(2)
		  if rs=nil then
		    lastMessage="Data of grid points could not be loaded"
		    return false
		  end
		  if rs.EOF then
		    lastMessage="No points found for grid "+ CStr(gridID)+ "!"
		    return false
		  end
		  
		  //Verify that the number of records makes sense with the size of grid...
		  if nx*ny<>rs.RecordCount then
		    lastMessage="Found only " + CStr(rs.RecordCount) + " point records instead of " + CStr(nx*ny) + " in grid " + CStr(gridID) + "!"
		    return false
		  end
		  
		  //Fill the variables basic grid information...
		  dim tmpNx, tmpNy as integer
		  for i=0 to (nx*ny-1)
		    id_ixiys(i)=rs.Field("id_tb_ixiy").IntegerValue
		    ixs(i)=rs.Field("ix").IntegerValue
		    iys(i)=rs.Field("iy").IntegerValue
		    
		    //Minus 1, since they are array indexes...
		    tmpNx=ixs(i)-1
		    tmpNy=iys(i)-1
		    
		    id_ixiy(tmpNx,tmpNy)=id_ixiys(i)
		    lat(tmpNy)=rs.Field("lat_grd").DoubleValue
		    lon(tmpNx)=rs.Field("lon_grd").DoubleValue
		    
		    rs.MoveNext
		  next
		  
		  if not(rs.EOF) then
		    break
		  end
		  
		  //===========================
		  
		  grid_ID=gridID
		  BasicGridInfoLoaded=true
		  lastMessage="Succesfully loaded basic grid information!"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadBasicStInfo() As Boolean
		  
		  if BasicStInfoLoaded then
		    lastMessage="Basic information of stations already loaded."
		    return false
		  end
		  
		  db.sites=0
		  
		  dim rs as RecordSet
		  dim i as integer
		  dim auxStrArr1(-1), auxStrArr2(-1) as string
		  dim auxDbl as Double
		  
		  // Get number of stations =================================================
		  for i=1 to 4
		    rs=db.GetStationData(CStr(i))
		    if rs=nil then
		      lastMessage="Problem while checking station existence!"
		      return false
		    end
		    if rs.EOF then exit
		    
		    db.sites=i
		    
		    auxStrArr1=split(rs.Field("slat").StringValue," ")
		    auxStrArr2=split(auxStrArr1(0),"-")
		    auxDbl=CDbl(auxStrArr2(0)) + CDbl(auxStrArr2(1))/60 + CDbl(auxStrArr2(2))/3600
		    if auxStrArr1(1)="S" then auxDbl=auxDbl*(-1)
		    latSt.Append(auxDbl)
		    
		    auxStrArr1=split(rs.Field("slon").StringValue," ")
		    auxStrArr2=split(auxStrArr1(0),"-")
		    auxDbl=CDbl(auxStrArr2(0)) + CDbl(auxStrArr2(1))/60 + CDbl(auxStrArr2(2))/3600
		    if auxStrArr1(1)="W" then auxDbl=auxDbl*(-1)
		    lonSt.Append(auxDbl)
		    
		    trueNorthSt.Append( rs.Field("tnor").DoubleValue )
		    nameSt.Append(rs.Field("sname").StringValue )
		    longNameSt.Append(rs.Field("longname").StringValue)
		    codeSt.Append(rs.Field("acronym").StringValue)
		  next
		  
		  if db.sites=0 then
		    lastMessage="No station information found!"
		    return false
		  end if
		  
		  //===========================
		  
		  BasicStInfoLoaded=true
		  lastMessage="Succesfully loaded basic station information!"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadDatesAndHeadersWithinRange(dates() as date, headers() as string, dataType as integer, timePoint as date, timePointEnd as date) As Boolean
		  // It queries from the database the measurement times and headers at which there is a measurement of data type <dataType> that contains measured data (dataAvail=true).
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The type of data loaded depends of course on the datatype and on the number of 2D arrays given as input.
		  //
		  // The data returned by this method which is contained in the arrays given as input are ordered from older to newer.
		  //
		  // The method returns true when success, false otherwise.
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  dim rs as RecordSet
		  dim i as Integer
		  
		  rs=db.GetHeaderWithData(dataType, timePoint.SQLDateTime, timePointEnd.SQLDateTime, false)
		  if rs=nil then
		    lastMessage="Problem while querying for data within range!"
		    Return false
		  end
		  if rs.EOF then
		    lastMessage="No data found within specified time!"
		    Return false
		  end
		  
		  Redim headers(rs.RecordCount-1)
		  Redim dates(headers.Ubound)
		  
		  for i=0 to headers.Ubound
		    if dataType=0 then
		      headers(i)=rs.Field("id_tb_cuv_headers").StringValue
		    elseif dataType<10 then
		      headers(i)=rs.Field("id_tb_crad_header").StringValue
		    elseif dataType=10 or dataType=20 then
		      headers(i)=rs.Field("id_tb_wuv_headers").StringValue
		    else
		      headers(i)=rs.Field("id_tb_wrad_header").StringValue
		    end
		    dates(i)=rs.Field("measTimeDB").DateValue
		    rs.MoveNext
		  next
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadDatesAndHeadersWithinRange(dates() as date, headers() as string, dataType as integer, timePoint as date, timePointEnd as date, radHeaders(,) as string) As Boolean
		  // It queries from the database the measurement times and headers at which there is a measurement of data type <dataType> that contains measured data (dataAvail=true).
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The type of data loaded depends of course on the datatype and on the number of 2D arrays given as input.
		  //
		  // The data returned by this method which is contained in the arrays given as input are ordered from older to newer.
		  //
		  // The method returns true when success, false otherwise.
		  //
		  // In this method, when radHeaders is supplied as input, the header numbers of radial measurements used for combined measurements will be stored too.
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  dim rs as RecordSet
		  dim i,j as Integer
		  
		  rs=db.GetHeaderWithData(dataType, timePoint.SQLDateTime, timePointEnd.SQLDateTime, false)
		  if rs=nil then
		    lastMessage="Problem while querying for data within range!"
		    Return false
		  end
		  if rs.EOF then
		    lastMessage="No data found within specified time!"
		    Return false
		  end
		  
		  Redim headers(rs.RecordCount-1)
		  Redim dates(headers.Ubound)
		  Redim radHeaders(headers.Ubound,3)
		  
		  for i=0 to headers.Ubound
		    if dataType=0 then
		      headers(i)=rs.Field("id_tb_cuv_headers").StringValue
		      for j=0 to 3
		        radHeaders(i,j)=rs.Field("id_tb_crad_header"+CStr(j+1)).StringValue
		      next
		    elseif dataType<10 then
		      headers(i)=rs.Field("id_tb_crad_header").StringValue
		    elseif dataType=10 or dataType=20 then
		      headers(i)=rs.Field("id_tb_wuv_headers").StringValue
		      for j=0 to 3
		        radHeaders(i,j)=rs.Field("id_tb_wrad_header"+CStr(j+1)).StringValue
		      next
		    else
		      headers(i)=rs.Field("id_tb_wrad_header").StringValue
		    end
		    dates(i)=rs.Field("measTimeDB").DateValue
		    rs.MoveNext
		  next
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadGDOP(gdopData(,) as single) As Boolean
		  //Loads into gdopData, the GDOP values of each grid Cell.
		  
		  dim rs as RecordSet
		  dim i as integer
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Redim variable...
		  Redim gdopData(nx-1,ny-1)
		  
		  // Get information =================================================
		  
		  rs=db.GetIdIxIyFromGridID(2)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Data of grid points could not be loaded"
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"No points found for grid "+ CStr(grid_ID)+ "!"
		    return false
		  end
		  
		  //Verify that the number of records makes sense with the size of grid...
		  if nx*ny<>rs.RecordCount then
		    lastMessage=lastMessage+EndOfLine +"Found only " + CStr(rs.RecordCount) + " point records instead of " + CStr(nx*ny) + " in grid " + CStr(grid_ID) + "!"
		    return false
		  end
		  
		  //Fill the variables...
		  dim tmpNx, tmpNy as integer
		  for i=0 to (nx*ny-1)
		    
		    //Minus 1, since they are array indexes...
		    tmpNx=rs.Field("ix").IntegerValue-1
		    tmpNy=rs.Field("iy").IntegerValue-1
		    
		    gdopData(tmpNx,tmpNy)=rs.Field("GDOP").DoubleValue
		    
		    rs.MoveNext
		  next
		  
		  if not(rs.EOF) then
		    break
		  end
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded GDOP values"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadGDOP(gdopxData(,) as single, gdopyData(,) as single) As Boolean
		  //Loads into gdopData, the GDOP values of each grid Cell.
		  
		  dim rs as RecordSet
		  dim i as integer
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Redim variable...
		  Redim gdopxData(nx-1,ny-1)
		  Redim gdopyData(nx-1,ny-1)
		  
		  // Get information =================================================
		  
		  rs=db.GetIdIxIyFromGridID(2)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Data of grid points could not be loaded"
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"No points found for grid "+ CStr(grid_ID)+ "!"
		    return false
		  end
		  
		  //Verify that the number of records makes sense with the size of grid...
		  if nx*ny<>rs.RecordCount then
		    lastMessage=lastMessage+EndOfLine +"Found only " + CStr(rs.RecordCount) + " point records instead of " + CStr(nx*ny) + " in grid " + CStr(grid_ID) + "!"
		    return false
		  end
		  
		  //Fill the variables...
		  dim tmpNx, tmpNy as integer
		  for i=0 to (nx*ny-1)
		    
		    //Minus 1, since they are array indexes...
		    tmpNx=rs.Field("ix").IntegerValue-1
		    tmpNy=rs.Field("iy").IntegerValue-1
		    
		    gdopxData(tmpNx,tmpNy)=rs.Field("GDOPx").DoubleValue
		    gdopyData(tmpNx,tmpNy)=rs.Field("GDOPy").DoubleValue
		    
		    rs.MoveNext
		  next
		  
		  if not(rs.EOF) then
		    break
		  end
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded GDOP values"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadGDOP(gdopData(,) as single, gdopxData(,) as single, gdopyData(,) as single) As Boolean
		  //Loads into gdopData, the GDOP values of each grid Cell.
		  
		  dim rs as RecordSet
		  dim i as integer
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Redim variable...
		  Redim gdopData(nx-1,ny-1)
		  Redim gdopxData(nx-1,ny-1)
		  Redim gdopyData(nx-1,ny-1)
		  
		  // Get information =================================================
		  
		  rs=db.GetIdIxIyFromGridID(2)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Data of grid points could not be loaded"
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"No points found for grid "+ CStr(grid_ID)+ "!"
		    return false
		  end
		  
		  //Verify that the number of records makes sense with the size of grid...
		  if nx*ny<>rs.RecordCount then
		    lastMessage=lastMessage+EndOfLine +"Found only " + CStr(rs.RecordCount) + " point records instead of " + CStr(nx*ny) + " in grid " + CStr(grid_ID) + "!"
		    return false
		  end
		  
		  //Fill the variables...
		  dim tmpNx, tmpNy as integer
		  for i=0 to (nx*ny-1)
		    
		    //Minus 1, since they are array indexes...
		    tmpNx=rs.Field("ix").IntegerValue-1
		    tmpNy=rs.Field("iy").IntegerValue-1
		    
		    gdopData(tmpNx,tmpNy)=rs.Field("GDOP").DoubleValue
		    gdopxData(tmpNx,tmpNy)=rs.Field("GDOPx").DoubleValue
		    gdopyData(tmpNx,tmpNy)=rs.Field("GDOPy").DoubleValue
		    
		    rs.MoveNext
		  next
		  
		  if not(rs.EOF) then
		    break
		  end
		  
		  //===========================
		  
		  lastMessage="Succesfully loaded GDOP values"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadRadAngle(st as Integer, radData(,) as single) As Boolean
		  //Loads into radData, the radialAngles of each grid Cell. The direction is from station to grid cell.
		  
		  //North is zero and increases clockwise. This way if the given direction values are used together with velocity values, positive velocities are away from station and negative comming to station. 
		  
		  dim rs as RecordSet
		  dim i as integer
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  if st<0 OR st>4 then
		    lastMessage="Station parameter has to be from 1 to 4!"
		    return false
		  end
		  
		  // Getting true north value...
		  if not(BasicStInfoLoaded) then
		    if not(LoadBasicStInfo) then return false
		  end
		  if st>db.sites then
		    lastMessage=lastMessage+EndOfLine +"No data from station " + CStr(st) + " is available"
		    return false
		  end
		  dim tn as Double
		  tn=trueNorthSt(st-1)
		  
		  // Redim variable...
		  Redim radData(nx-1,ny-1)
		  
		  // Get information =================================================
		  
		  rs=db.GetIdIxIyFromGridID(2)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Data of grid points could not be loaded"
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"No points found for grid "+ CStr(grid_ID)+ "!"
		    return false
		  end
		  
		  //Verify that the number of records makes sense with the size of grid...
		  if nx*ny<>rs.RecordCount then
		    lastMessage=lastMessage+EndOfLine +"Found only " + CStr(rs.RecordCount) + " point records instead of " + CStr(nx*ny) + " in grid " + CStr(grid_ID) + "!"
		    return false
		  end
		  
		  //Fill the variables...
		  dim tmpNx, tmpNy as integer
		  for i=0 to (nx*ny-1)
		    
		    //Minus 1, since they are array indexes...
		    tmpNx=rs.Field("ix").IntegerValue-1
		    tmpNy=rs.Field("iy").IntegerValue-1
		    
		    radData(tmpNx,tmpNy)=tn-90+rs.Field("rad_angle" + CStr(st)).DoubleValue
		    if radData(tmpNx,tmpNy)<0 then radData(tmpNx,tmpNy)=radData(tmpNx,tmpNy)+360
		    if radData(tmpNx,tmpNy)>360 then radData(tmpNx,tmpNy)=radData(tmpNx,tmpNy)-360
		    
		    rs.MoveNext
		  next
		  
		  if not(rs.EOF) then
		    break
		  end
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded radial angles"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadSpatialData(timePoint as string, dataType as integer, dataAvail(,) as Boolean, spData1(,) as single) As Boolean
		  // It loads  data (2D data map) from the database that corresponds to the dataType and to the timePoint indicated in the inputs.
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The dataAvail boolean array indicates on which grid points there is data available.
		  // The type of data loaded depends of course on the datatype and on the number of 2D arrays given as input.
		  //
		  // Different cases for 2D arrays given as input for this function are:
		  //
		  // dataAvail (Boolean) + one spatial data (Single)
		  // dataAvail (Boolean) + one spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + two spatial data(Single)
		  // dataAvail (Boolean) + two spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + four spatial data (Single)
		  // dataAvail (Boolean) + four spatial data (Single) + qual (int)
		  //
		  // The order of dimensions is (longitude, latitude)
		  //
		  // The method returns true when success, false otherwise.
		  
		  dim i, j as integer
		  dim rs as RecordSet
		  lastMessage=""
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Check for invalid datatype...
		  if dataType=0 then
		  elseif dataType<10 then
		    if not(BasicStInfoLoaded) then 
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=10 then
		  elseif dataType<20 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=20 then
		  else
		    lastMessage="Invalid data type!"
		    return false
		  end
		  
		  // Get header =================================================
		  rs=db.GetHeaderWithData(dataType, timePoint , timePoint, false)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Problem while querying for measurement at " + timePoint + " with data type " + CStr(dataType)
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"Could not find measurement at " + timePoint + " with data type " + CStr(dataType)
		    return false
		  end
		  
		  dim header as integer
		  if dataType=0 then
		    header=rs.Field("id_tb_cuv_headers").IntegerValue
		  elseif dataType<10 then
		    header=rs.Field("id_tb_crad_header").IntegerValue
		  elseif dataType=10 OR dataType=20 then
		    header=rs.Field("id_tb_wuv_headers").IntegerValue
		  elseif dataType<20 then
		    header=rs.Field("id_tb_wrad_header").IntegerValue
		  end
		  
		  // Get data =================================================
		  rs=db.GetMeasurementDataSpace(header, dataType)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Problem while querying for data on header " + CStr(header) + " with data type " + CStr(dataType)
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"Could not find data on header " + CStr(header) + " with data type " + CStr(dataType)
		    return false
		  end
		  
		  // Redim variables...
		  Redim dataAvail(nx-1,ny-1)
		  Redim spData1(nx-1,ny-1)
		  
		  //Replace old values with 'noData'
		  for i=0 to nx-1
		    for j=0 to ny-1
		      dataAvail(i,j)=false
		      spData1(i,j)=-999
		    next
		  next
		  
		  dim ix, iy, idixiy, idx as integer
		  idx=0
		  while not(rs.EOF)
		    
		    idixiy=rs.Field("id_tb_ixiy").IntegerValue
		    idx=id_ixiys.IndexOf(idixiy,idx)
		    
		    //Minus 1, since they are array indexes...
		    ix=ixs(idx)-1
		    iy=iys(idx)-1
		    
		    dataAvail(ix,iy)=true
		    if dataType=0 then
		      spData1(ix,iy)=rs.Field("velu").DoubleValue
		    elseif dataType<10 then
		      spData1(ix,iy)=rs.Field("vel").DoubleValue
		    elseif dataType=10 then
		      spData1(ix,iy)=rs.Field("hwave").DoubleValue
		    elseif dataType<20 then
		      spData1(ix,iy)=rs.Field("hwave").DoubleValue
		    elseif dataType=20 then
		      spData1(ix,iy)=rs.Field("dir").DoubleValue
		    end
		    
		    rs.MoveNext
		    
		  wend
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded data"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadSpatialData(timePoint as string, dataType as integer, dataAvail(,) as Boolean, spData1(,) as single, qual(,) as Integer) As Boolean
		  // It loads  data (2D data map) from the database that corresponds to the dataType and to the timePoint indicated in the inputs.
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The dataAvail boolean array indicates on which grid points there is data available.
		  // The type of data loaded depends of course on the datatype and on the number of 2D arrays given as input.
		  //
		  // Different cases for 2D arrays given as input for this function are:
		  //
		  // dataAvail (Boolean) + one spatial data (Single)
		  // dataAvail (Boolean) + one spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + two spatial data(Single)
		  // dataAvail (Boolean) + two spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + four spatial data (Single)
		  // dataAvail (Boolean) + four spatial data (Single) + qual (int)
		  //
		  // The order of dimensions is (longitude, latitude)
		  //
		  // The method returns true when success, false otherwise.
		  
		  dim i, j as integer
		  dim rs as RecordSet
		  lastMessage=""
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Check for invalid datatype...
		  if dataType=0 then
		  elseif dataType<10 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=10 then
		  elseif dataType<20 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=20 then
		  else
		    lastMessage="Invalid data type!"
		    return false
		  end
		  
		  // Get header =================================================
		  rs=db.GetHeaderWithData(dataType, timePoint , timePoint, false)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Problem while querying for measurement at " + timePoint + " with data type " + CStr(dataType)
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"Could not find measurement at " + timePoint + " with data type " + CStr(dataType)
		    return false
		  end
		  
		  dim header as integer
		  if dataType=0 then
		    header=rs.Field("id_tb_cuv_headers").IntegerValue
		  elseif dataType<10 then
		    header=rs.Field("id_tb_crad_header").IntegerValue
		  elseif dataType=10 OR dataType=20 then
		    header=rs.Field("id_tb_wuv_headers").IntegerValue
		  elseif dataType<20 then
		    header=rs.Field("id_tb_wrad_header").IntegerValue
		  end
		  
		  // Get data =================================================
		  rs=db.GetMeasurementDataSpace(header, dataType)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Problem while querying for data on header " + CStr(header) + " with data type " + CStr(dataType)
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"Could not find data on header " + CStr(header) + " with data type " + CStr(dataType)
		    return false
		  end
		  
		  // Redim variables...
		  Redim dataAvail(nx-1,ny-1)
		  Redim spData1(nx-1,ny-1)
		  Redim qual(nx-1,ny-1)
		  
		  //Replace old values with 'noData'
		  for i=0 to nx-1
		    for j=0 to ny-1
		      dataAvail(i,j)=false
		      spData1(i,j)=-999
		      qual(i,j)=-99
		    next
		  next
		  
		  dim ix, iy, idixiy, idx as integer
		  idx=0
		  while not(rs.EOF)
		    
		    idixiy=rs.Field("id_tb_ixiy").IntegerValue
		    idx=id_ixiys.IndexOf(idixiy,idx)
		    
		    //Minus 1, since they are array indexes...
		    ix=ixs(idx)-1
		    iy=iys(idx)-1
		    
		    dataAvail(ix,iy)=true
		    if dataType=0 then
		      spData1(ix,iy)=rs.Field("velu").DoubleValue
		      qual(ix,iy)=rs.Field("kl").IntegerValue
		    elseif dataType<10 then
		      spData1(ix,iy)=rs.Field("vel").DoubleValue
		      qual(ix,iy)=rs.Field("qual").IntegerValue
		    elseif dataType=10 then
		      spData1(ix,iy)=rs.Field("hwave").DoubleValue
		      qual(ix,iy)=rs.Field("qual").IntegerValue
		    elseif dataType<20 then
		      spData1(ix,iy)=rs.Field("hwave").DoubleValue
		      qual(ix,iy)=rs.Field("qual").IntegerValue
		    elseif dataType=20 then
		      spData1(ix,iy)=rs.Field("dir").DoubleValue
		      qual(ix,iy)=rs.Field("qual").IntegerValue
		    end
		    
		    rs.MoveNext
		    
		  wend
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded data"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadSpatialData(timePoint as string, dataType as integer, dataAvail(,) as Boolean, spData1(,) as single, spData2(,) as single) As Boolean
		  // It loads  data (2D data map) from the database that corresponds to the dataType and to the timePoint indicated in the inputs.
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The dataAvail boolean array indicates on which grid points there is data available.
		  // The type of data loaded depends of course on the datatype and on the number of 2D arrays given as input.
		  //
		  // Different cases for 2D arrays given as input for this function are:
		  //
		  // dataAvail (Boolean) + one spatial data (Single)
		  // dataAvail (Boolean) + one spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + two spatial data(Single)
		  // dataAvail (Boolean) + two spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + four spatial data (Single)
		  // dataAvail (Boolean) + four spatial data (Single) + qual (int)
		  //
		  // The order of dimensions is (longitude, latitude)
		  //
		  // The method returns true when success, false otherwise.
		  
		  dim i, j as integer
		  dim rs as RecordSet
		  lastMessage=""
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Check for invalid datatype...
		  if dataType=0 then
		  elseif dataType<10 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=10 then
		  elseif dataType<20 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=20 then
		  else
		    lastMessage="Invalid data type!"
		    return false
		  end
		  
		  // Get header =================================================
		  rs=db.GetHeaderWithData(dataType, timePoint , timePoint, false)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Problem while querying for measurement at " + timePoint + " with data type " + CStr(dataType)
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"Could not find measurement at " + timePoint + " with data type " + CStr(dataType)
		    return false
		  end
		  
		  dim header as integer
		  if dataType=0 then
		    header=rs.Field("id_tb_cuv_headers").IntegerValue
		  elseif dataType<10 then
		    header=rs.Field("id_tb_crad_header").IntegerValue
		  elseif dataType=10 OR dataType=20 then
		    header=rs.Field("id_tb_wuv_headers").IntegerValue
		  elseif dataType<20 then
		    header=rs.Field("id_tb_wrad_header").IntegerValue
		  end
		  
		  // Get data =================================================
		  rs=db.GetMeasurementDataSpace(header, dataType)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Problem while querying for data on header " + CStr(header) + " with data type " + CStr(dataType)
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"Could not find data on header " + CStr(header) + " with data type " + CStr(dataType)
		    return false
		  end
		  
		  // Redim variables...
		  Redim dataAvail(nx-1,ny-1)
		  Redim spData1(nx-1,ny-1)
		  Redim spData2(nx-1,ny-1)
		  
		  //Replace old values with 'noData'
		  for i=0 to nx-1
		    for j=0 to ny-1
		      dataAvail(i,j)=false
		      spData1(i,j)=-999
		      spData2(i,j)=-999
		    next
		  next
		  
		  dim ix, iy, idixiy, idx as integer
		  idx=0
		  while not(rs.EOF)
		    
		    idixiy=rs.Field("id_tb_ixiy").IntegerValue
		    idx=id_ixiys.IndexOf(idixiy,idx)
		    
		    //Minus 1, since they are array indexes...
		    ix=ixs(idx)-1
		    iy=iys(idx)-1
		    
		    dataAvail(ix,iy)=true
		    if dataType=0 then
		      spData1(ix,iy)=rs.Field("velu").DoubleValue
		      spData2(ix,iy)=rs.Field("velv").DoubleValue
		    elseif dataType<10 then
		      spData1(ix,iy)=rs.Field("vel").DoubleValue
		      spData2(ix,iy)=rs.Field("acc").DoubleValue
		    elseif dataType=10 then
		      spData1(ix,iy)=rs.Field("hwave").DoubleValue
		      spData2(ix,iy)=rs.Field("dir").DoubleValue
		    elseif dataType<20 then
		      spData1(ix,iy)=rs.Field("hwave").DoubleValue
		    elseif dataType=20 then
		      spData1(ix,iy)=rs.Field("dir").DoubleValue
		      spData2(ix,iy)=rs.Field("speed").DoubleValue
		    end
		    
		    rs.MoveNext
		    
		  wend
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded data"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadSpatialData(timePoint as string, dataType as integer, dataAvail(,) as Boolean, spData1(,) as single, spData2(,) as single, qual(,) as Integer) As Boolean
		  // It loads  data (2D data map) from the database that corresponds to the dataType and to the timePoint indicated in the inputs.
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The dataAvail boolean array indicates on which grid points there is data available.
		  // The type of data loaded depends of course on the datatype and on the number of 2D arrays given as input.
		  //
		  // Different cases for 2D arrays given as input for this function are:
		  //
		  // dataAvail (Boolean) + one spatial data (Single)
		  // dataAvail (Boolean) + one spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + two spatial data(Single)
		  // dataAvail (Boolean) + two spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + four spatial data (Single)
		  // dataAvail (Boolean) + four spatial data (Single) + qual (int)
		  //
		  // The order of dimensions is (longitude, latitude)
		  //
		  // The method returns true when success, false otherwise.
		  
		  dim i, j as integer
		  dim rs as RecordSet
		  lastMessage=""
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Check for invalid datatype...
		  if dataType=0 then
		  elseif dataType<10 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=10 then
		  elseif dataType<20 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=20 then
		  else
		    lastMessage="Invalid data type!"
		    return false
		  end
		  
		  // Get header =================================================
		  rs=db.GetHeaderWithData(dataType, timePoint , timePoint, false)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Problem while querying for measurement at " + timePoint + " with data type " + CStr(dataType)
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"Could not find measurement at " + timePoint + " with data type " + CStr(dataType)
		    return false
		  end
		  
		  dim header as integer
		  if dataType=0 then
		    header=rs.Field("id_tb_cuv_headers").IntegerValue
		  elseif dataType<10 then
		    header=rs.Field("id_tb_crad_header").IntegerValue
		  elseif dataType=10 OR dataType=20 then
		    header=rs.Field("id_tb_wuv_headers").IntegerValue
		  elseif dataType<20 then
		    header=rs.Field("id_tb_wrad_header").IntegerValue
		  end
		  
		  // Get data =================================================
		  rs=db.GetMeasurementDataSpace(header, dataType)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Problem while querying for data on header " + CStr(header) + " with data type " + CStr(dataType)
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"Could not find data on header " + CStr(header) + " with data type " + CStr(dataType)
		    return false
		  end
		  
		  // Redim variables...
		  Redim dataAvail(nx-1,ny-1)
		  Redim spData1(nx-1,ny-1)
		  Redim spData2(nx-1,ny-1)
		  Redim qual(nx-1,ny-1)
		  
		  //Replace old values with 'noData'
		  for i=0 to nx-1
		    for j=0 to ny-1
		      dataAvail(i,j)=false
		      spData1(i,j)=-999
		      spData2(i,j)=-999
		      qual(i,j)=-99
		    next
		  next
		  
		  dim ix, iy, idixiy, idx as integer
		  idx=0
		  while not(rs.EOF)
		    
		    idixiy=rs.Field("id_tb_ixiy").IntegerValue
		    idx=id_ixiys.IndexOf(idixiy,idx)
		    
		    //Minus 1, since they are array indexes...
		    ix=ixs(idx)-1
		    iy=iys(idx)-1
		    
		    dataAvail(ix,iy)=true
		    if dataType=0 then
		      spData1(ix,iy)=rs.Field("velu").DoubleValue
		      spData2(ix,iy)=rs.Field("velv").DoubleValue
		      qual(ix,iy)=rs.Field("kl").IntegerValue
		    elseif dataType<10 then
		      spData1(ix,iy)=rs.Field("vel").DoubleValue
		      spData2(ix,iy)=rs.Field("acc").DoubleValue
		      qual(ix,iy)=rs.Field("qual").IntegerValue
		    elseif dataType=10 then
		      spData1(ix,iy)=rs.Field("hwave").DoubleValue
		      spData2(ix,iy)=rs.Field("dir").DoubleValue
		      qual(ix,iy)=rs.Field("qual").IntegerValue
		    elseif dataType<20 then
		      spData1(ix,iy)=rs.Field("hwave").DoubleValue
		      qual(ix,iy)=rs.Field("qual").IntegerValue
		    elseif dataType=20 then
		      spData1(ix,iy)=rs.Field("dir").DoubleValue
		      spData2(ix,iy)=rs.Field("speed").DoubleValue
		      qual(ix,iy)=rs.Field("qual").IntegerValue
		    end
		    
		    rs.MoveNext
		    
		  wend
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded data"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadSpatialData(timePoint as string, dataType as integer, dataAvail(,) as Boolean, spData1(,) as single, spData2(,) as single, spData3(,) as single, spData4(,) as single) As Boolean
		  // It loads  data (2D data map) from the database that corresponds to the dataType and to the timePoint indicated in the inputs.
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The dataAvail boolean array indicates on which grid points there is data available.
		  // The type of data loaded depends of course on the datatype and on the number of 2D arrays given as input.
		  //
		  // Different cases for 2D arrays given as input for this function are:
		  //
		  // dataAvail (Boolean) + one spatial data (Single)
		  // dataAvail (Boolean) + one spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + two spatial data(Single)
		  // dataAvail (Boolean) + two spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + four spatial data (Single)
		  // dataAvail (Boolean) + four spatial data (Single) + qual (int)
		  //
		  // The order of dimensions is (longitude, latitude)
		  //
		  // The method returns true when success, false otherwise.
		  
		  dim i, j as integer
		  dim rs as RecordSet
		  lastMessage=""
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Check for invalid datatype...
		  if dataType=0 then
		  elseif dataType<10 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=10 then
		  elseif dataType<20 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=20 then
		  else
		    lastMessage="Invalid data type!"
		    return false
		  end
		  
		  // Get header =================================================
		  rs=db.GetHeaderWithData(dataType, timePoint , timePoint, false)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Problem while querying for measurement at " + timePoint + " with data type " + CStr(dataType)
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"Could not find measurement at " + timePoint + " with data type " + CStr(dataType)
		    return false
		  end
		  
		  dim header as integer
		  if dataType=0 then
		    header=rs.Field("id_tb_cuv_headers").IntegerValue
		  elseif dataType<10 then
		    header=rs.Field("id_tb_crad_header").IntegerValue
		  elseif dataType=10 OR dataType=20 then
		    header=rs.Field("id_tb_wuv_headers").IntegerValue
		  elseif dataType<20 then
		    header=rs.Field("id_tb_wrad_header").IntegerValue
		  end
		  
		  // Get data =================================================
		  rs=db.GetMeasurementDataSpace(header, dataType)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Problem while querying for data on header " + CStr(header) + " with data type " + CStr(dataType)
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"Could not find data on header " + CStr(header) + " with data type " + CStr(dataType)
		    return false
		  end
		  
		  // Redim variables...
		  Redim dataAvail(nx-1,ny-1)
		  Redim spData1(nx-1,ny-1)
		  Redim spData2(nx-1,ny-1)
		  Redim spData3(nx-1,ny-1)
		  Redim spData4(nx-1,ny-1)
		  
		  //Replace old values with 'noData'
		  for i=0 to nx-1
		    for j=0 to ny-1
		      dataAvail(i,j)=false
		      spData1(i,j)=-999
		      spData2(i,j)=-999
		      spData3(i,j)=-999
		      spData4(i,j)=-999
		    next
		  next
		  
		  dim ix, iy, idixiy, idx as integer
		  idx=0
		  while not(rs.EOF)
		    
		    idixiy=rs.Field("id_tb_ixiy").IntegerValue
		    idx=id_ixiys.IndexOf(idixiy,idx)
		    
		    //Minus 1, since they are array indexes...
		    ix=ixs(idx)-1
		    iy=iys(idx)-1
		    
		    dataAvail(ix,iy)=true
		    if dataType=0 then
		      spData1(ix,iy)=rs.Field("velu").DoubleValue
		      spData2(ix,iy)=rs.Field("velv").DoubleValue
		      spData3(ix,iy)=rs.Field("accu").DoubleValue
		      spData4(ix,iy)=rs.Field("accv").DoubleValue
		    elseif dataType<10 then
		      spData1(ix,iy)=rs.Field("vel").DoubleValue
		      spData2(ix,iy)=rs.Field("acc").DoubleValue
		      spData3(ix,iy)=rs.Field("var").DoubleValue
		      spData4(ix,iy)=rs.Field("pow").DoubleValue
		    elseif dataType=10 then
		      spData1(ix,iy)=rs.Field("hwave").DoubleValue
		      spData2(ix,iy)=rs.Field("dir").DoubleValue
		    elseif dataType<20 then
		      spData1(ix,iy)=rs.Field("hwave").DoubleValue
		    elseif dataType=20 then
		      spData1(ix,iy)=rs.Field("dir").DoubleValue
		      spData2(ix,iy)=rs.Field("speed").DoubleValue
		    end
		    
		    rs.MoveNext
		    
		  wend
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded data"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadSpatialData(timePoint as string, dataType as integer, dataAvail(,) as Boolean, spData1(,) as single, spData2(,) as single, spData3(,) as single, spData4(,) as single, qual(,) as Integer) As Boolean
		  // It loads  data (2D data map) from the database that corresponds to the dataType and to the timePoint indicated in the inputs.
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The dataAvail boolean array indicates on which grid points there is data available.
		  // The type of data loaded depends of course on the datatype and on the number of 2D arrays given as input.
		  //
		  // Different cases for 2D arrays given as input for this function are:
		  //
		  // dataAvail (Boolean) + one spatial data (Single)
		  // dataAvail (Boolean) + one spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + two spatial data(Single)
		  // dataAvail (Boolean) + two spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + four spatial data (Single)
		  // dataAvail (Boolean) + four spatial data (Single) + qual (int)
		  //
		  // The order of dimensions is (longitude, latitude)
		  //
		  // The method returns true when success, false otherwise.
		  
		  dim i, j as integer
		  dim rs as RecordSet
		  lastMessage=""
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Check for invalid datatype...
		  if dataType=0 then
		  elseif dataType<10 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=10 then
		  elseif dataType<20 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=20 then
		  else
		    lastMessage="Invalid data type!"
		    return false
		  end
		  
		  // Get header =================================================
		  rs=db.GetHeaderWithData(dataType, timePoint , timePoint, false)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Problem while querying for measurement at " + timePoint + " with data type " + CStr(dataType)
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"Could not find measurement at " + timePoint + " with data type " + CStr(dataType)
		    return false
		  end
		  
		  dim header as integer
		  if dataType=0 then
		    header=rs.Field("id_tb_cuv_headers").IntegerValue
		  elseif dataType<10 then
		    header=rs.Field("id_tb_crad_header").IntegerValue
		  elseif dataType=10 OR dataType=20 then
		    header=rs.Field("id_tb_wuv_headers").IntegerValue
		  elseif dataType<20 then
		    header=rs.Field("id_tb_wrad_header").IntegerValue
		  end
		  
		  // Get data =================================================
		  rs=db.GetMeasurementDataSpace(header, dataType)
		  if rs=nil then
		    lastMessage=lastMessage+EndOfLine +"Problem while querying for data on header " + CStr(header) + " with data type " + CStr(dataType)
		    return false
		  end
		  if rs.EOF then
		    lastMessage=lastMessage+EndOfLine +"Could not find data on header " + CStr(header) + " with data type " + CStr(dataType)
		    return false
		  end
		  
		  // Redim variables...
		  Redim dataAvail(nx-1,ny-1)
		  Redim spData1(nx-1,ny-1)
		  Redim spData2(nx-1,ny-1)
		  Redim spData3(nx-1,ny-1)
		  Redim spData4(nx-1,ny-1)
		  Redim qual(nx-1,ny-1)
		  
		  //Replace old values with 'noData'
		  for i=0 to nx-1
		    for j=0 to ny-1
		      dataAvail(i,j)=false
		      spData1(i,j)=-999
		      spData2(i,j)=-999
		      spData3(i,j)=-999
		      spData4(i,j)=-999
		      qual(i,j)=-99
		    next
		  next
		  
		  dim ix, iy, idixiy, idx as integer
		  idx=0
		  while not(rs.EOF)
		    
		    idixiy=rs.Field("id_tb_ixiy").IntegerValue
		    idx=id_ixiys.IndexOf(idixiy,idx)
		    
		    //Minus 1, since they are array indexes...
		    ix=ixs(idx)-1
		    iy=iys(idx)-1
		    
		    dataAvail(ix,iy)=true
		    if dataType=0 then
		      spData1(ix,iy)=rs.Field("velu").DoubleValue
		      spData2(ix,iy)=rs.Field("velv").DoubleValue
		      spData3(ix,iy)=rs.Field("accu").DoubleValue
		      spData4(ix,iy)=rs.Field("accv").DoubleValue
		      qual(ix,iy)=rs.Field("kl").IntegerValue
		    elseif dataType<10 then
		      spData1(ix,iy)=rs.Field("vel").DoubleValue
		      spData2(ix,iy)=rs.Field("acc").DoubleValue
		      spData3(ix,iy)=rs.Field("var").DoubleValue
		      spData4(ix,iy)=rs.Field("pow").DoubleValue
		      qual(ix,iy)=rs.Field("qual").IntegerValue
		    elseif dataType=10 then
		      spData1(ix,iy)=rs.Field("hwave").DoubleValue
		      spData2(ix,iy)=rs.Field("dir").DoubleValue
		      qual(ix,iy)=rs.Field("qual").IntegerValue
		    elseif dataType<20 then
		      spData1(ix,iy)=rs.Field("hwave").DoubleValue
		      qual(ix,iy)=rs.Field("qual").IntegerValue
		    elseif dataType=20 then
		      spData1(ix,iy)=rs.Field("dir").DoubleValue
		      spData2(ix,iy)=rs.Field("speed").DoubleValue
		      qual(ix,iy)=rs.Field("qual").IntegerValue
		    end
		    
		    rs.MoveNext
		    
		  wend
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded data"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadTemporalSpatialData(headers() as string, dataType as integer, dataAvail(,,) as Boolean, spData1(,,) as single) As Boolean
		  // It loads measured data from the database that corresponds to the dataType and that correspond to the headers given in <headers()>.
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The dataAvail boolean array indicates on which grid points there is data available.
		  // The type of data loaded depends of course on the datatype and on the number of 3D arrays given as input.
		  //
		  // Different cases for 3D arrays given as input for this function are:
		  //
		  // dataAvail (Boolean) + one spatial data (Single)
		  // dataAvail (Boolean) + one spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + two spatial data(Single)
		  // dataAvail (Boolean) + two spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + four spatial data (Single)
		  // dataAvail (Boolean) + four spatial data (Single) + qual (int)
		  //
		  // The order of dimensions is (time, latitude, longitude) 
		  //
		  // The method returns true when success, false otherwise. lastMessage property is used for error handling.
		  
		  dim i, j, k as integer
		  dim rs as RecordSet
		  lastMessage=""
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Check for invalid datatype...
		  if dataType=0 then
		  elseif dataType<10 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=10 then
		  elseif dataType<20 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=20 then
		  else
		    lastMessage="Invalid data type!"
		    return false
		  end
		  
		  
		  
		  // Redim variables...
		  Redim dataAvail(headers.ubound, ny-1, nx-1)
		  Redim spData1(headers.ubound, ny-1, nx-1)
		  
		  //Replace old values with 'noData'
		  for k=0 to headers.Ubound
		    for j=0 to ny-1
		      for i=0 to nx-1
		        dataAvail(k,j,i)=false
		      next
		    next
		  next
		  
		  
		  // Get data =================================================
		  
		  dim ix, iy, idixiy, idx as integer
		  
		  for k=0 to headers.Ubound
		    
		    rs=db.GetMeasurementDataSpace(CLong(headers(k)), dataType)
		    if rs=nil then
		      lastMessage=lastMessage+EndOfLine +"Problem while querying for data on header " + headers(k) + " with data type " + CStr(dataType)
		      return false
		    end
		    if rs.EOF then continue
		    
		    idx=0
		    while not(rs.EOF)
		      
		      idixiy=rs.Field("id_tb_ixiy").IntegerValue
		      idx=id_ixiys.IndexOf(idixiy,idx)
		      
		      //Minus 1, since they are array indexes...
		      ix=ixs(idx)-1
		      iy=iys(idx)-1
		      
		      dataAvail(k,iy,ix)=true
		      if dataType=0 then
		        spData1(k,iy,ix)=rs.Field("velu").DoubleValue
		      elseif dataType<10 then
		        spData1(k,iy,ix)=rs.Field("vel").DoubleValue
		      elseif dataType=10 then
		        spData1(k,iy,ix)=rs.Field("hwave").DoubleValue
		      elseif dataType<20 then
		        spData1(k,iy,ix)=rs.Field("hwave").DoubleValue
		      elseif dataType=20 then
		        spData1(k,iy,ix)=rs.Field("dir").DoubleValue
		      end
		      
		      rs.MoveNext
		      
		    wend
		    
		  next
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded data"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadTemporalSpatialData(headers() as string, dataType as integer, dataAvail(,,) as Boolean, spData1(,,) as single, qual(,,) as Integer) As Boolean
		  // It loads measured data from the database that corresponds to the dataType and that correspond to the headers given in <headers()>.
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The dataAvail boolean array indicates on which grid points there is data available.
		  // The type of data loaded depends of course on the datatype and on the number of 3D arrays given as input.
		  //
		  // Different cases for 3D arrays given as input for this function are:
		  //
		  // dataAvail (Boolean) + one spatial data (Single)
		  // dataAvail (Boolean) + one spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + two spatial data(Single)
		  // dataAvail (Boolean) + two spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + four spatial data (Single)
		  // dataAvail (Boolean) + four spatial data (Single) + qual (int)
		  //
		  // The order of dimensions is (time, latitude, longitude) 
		  //
		  // The method returns true when success, false otherwise. lastMessage property is used for error handling.
		  
		  dim i, j, k as integer
		  dim rs as RecordSet
		  lastMessage=""
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Check for invalid datatype...
		  if dataType=0 then
		  elseif dataType<10 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=10 then
		  elseif dataType<20 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=20 then
		  else
		    lastMessage="Invalid data type!"
		    return false
		  end
		  
		  
		  
		  // Redim variables...
		  Redim dataAvail(headers.ubound, ny-1, nx-1)
		  Redim spData1(headers.ubound, ny-1, nx-1)
		  Redim qual(headers.ubound, ny-1, nx-1)
		  
		  //Replace old values with 'noData'
		  for k=0 to headers.Ubound
		    for j=0 to ny-1
		      for i=0 to nx-1
		        dataAvail(k,j,i)=false
		      next
		    next
		  next
		  
		  
		  // Get data =================================================
		  
		  dim ix, iy, idixiy, idx as integer
		  
		  for k=0 to headers.Ubound
		    
		    rs=db.GetMeasurementDataSpace(CLong(headers(k)), dataType)
		    if rs=nil then
		      lastMessage=lastMessage+EndOfLine +"Problem while querying for data on header " + headers(k) + " with data type " + CStr(dataType)
		      return false
		    end
		    if rs.EOF then continue
		    
		    idx=0
		    while not(rs.EOF)
		      
		      idixiy=rs.Field("id_tb_ixiy").IntegerValue
		      idx=id_ixiys.IndexOf(idixiy,idx)
		      
		      //Minus 1, since they are array indexes...
		      ix=ixs(idx)-1
		      iy=iys(idx)-1
		      
		      dataAvail(k,iy,ix)=true
		      if dataType=0 then
		        spData1(k,iy,ix)=rs.Field("velu").DoubleValue
		        qual(k,iy,ix)=rs.Field("kl").IntegerValue
		      elseif dataType<10 then
		        spData1(k,iy,ix)=rs.Field("vel").DoubleValue
		        qual(k,iy,ix)=rs.Field("qual").IntegerValue
		      elseif dataType=10 then
		        spData1(k,iy,ix)=rs.Field("hwave").DoubleValue
		        qual(k,iy,ix)=rs.Field("qual").IntegerValue
		      elseif dataType<20 then
		        spData1(k,iy,ix)=rs.Field("hwave").DoubleValue
		        qual(k,iy,ix)=rs.Field("qual").IntegerValue
		      elseif dataType=20 then
		        spData1(k,iy,ix)=rs.Field("dir").DoubleValue
		        qual(k,iy,ix)=rs.Field("qual").IntegerValue
		      end
		      
		      rs.MoveNext
		      
		    wend
		    
		  next
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded data"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadTemporalSpatialData(headers() as string, dataType as integer, dataAvail(,,) as Boolean, spData1(,,) as single, spData2(,,) as single) As Boolean
		  // It loads measured data from the database that corresponds to the dataType and that correspond to the headers given in <headers()>.
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The dataAvail boolean array indicates on which grid points there is data available.
		  // The type of data loaded depends of course on the datatype and on the number of 3D arrays given as input.
		  //
		  // Different cases for 3D arrays given as input for this function are:
		  //
		  // dataAvail (Boolean) + one spatial data (Single)
		  // dataAvail (Boolean) + one spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + two spatial data(Single)
		  // dataAvail (Boolean) + two spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + four spatial data (Single)
		  // dataAvail (Boolean) + four spatial data (Single) + qual (int)
		  //
		  // The order of dimensions is (time, latitude, longitude) 
		  //
		  // The method returns true when success, false otherwise. lastMessage property is used for error handling.
		  
		  dim i, j, k as integer
		  dim rs as RecordSet
		  lastMessage=""
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Check for invalid datatype...
		  if dataType=0 then
		  elseif dataType<10 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=10 then
		  elseif dataType<20 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=20 then
		  else
		    lastMessage="Invalid data type!"
		    return false
		  end
		  
		  
		  
		  // Redim variables...
		  Redim dataAvail(headers.ubound, ny-1, nx-1)
		  Redim spData1(headers.ubound, ny-1, nx-1)
		  Redim spData2(headers.ubound, ny-1, nx-1)
		  
		  //Replace old values with 'noData'
		  for k=0 to headers.Ubound
		    for j=0 to ny-1
		      for i=0 to nx-1
		        dataAvail(k,j,i)=false
		      next
		    next
		  next
		  
		  
		  // Get data =================================================
		  
		  dim ix, iy, idixiy, idx as integer
		  
		  for k=0 to headers.Ubound
		    
		    rs=db.GetMeasurementDataSpace(CLong(headers(k)), dataType)
		    if rs=nil then
		      lastMessage=lastMessage+EndOfLine +"Problem while querying for data on header " + headers(k) + " with data type " + CStr(dataType)
		      return false
		    end
		    if rs.EOF then continue
		    
		    idx=0
		    while not(rs.EOF)
		      
		      idixiy=rs.Field("id_tb_ixiy").IntegerValue
		      idx=id_ixiys.IndexOf(idixiy,idx)
		      
		      //Minus 1, since they are array indexes...
		      ix=ixs(idx)-1
		      iy=iys(idx)-1
		      
		      dataAvail(k,iy,ix)=true
		      if dataType=0 then
		        spData1(k,iy,ix)=rs.Field("velu").DoubleValue
		        spData2(k,iy,ix)=rs.Field("velv").DoubleValue
		      elseif dataType<10 then
		        spData1(k,iy,ix)=rs.Field("vel").DoubleValue
		        spData2(k,iy,ix)=rs.Field("acc").DoubleValue
		      elseif dataType=10 then
		        spData1(k,iy,ix)=rs.Field("hwave").DoubleValue
		        spData2(k,iy,ix)=rs.Field("dir").DoubleValue
		      elseif dataType<20 then
		        spData1(k,iy,ix)=rs.Field("hwave").DoubleValue
		      elseif dataType=20 then
		        spData1(k,iy,ix)=rs.Field("dir").DoubleValue
		        spData2(k,iy,ix)=rs.Field("speed").DoubleValue
		      end
		      
		      rs.MoveNext
		      
		    wend
		    
		  next
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded data"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadTemporalSpatialData(headers() as string, dataType as integer, dataAvail(,,) as Boolean, spData1(,,) as single, spData2(,,) as single, qual(,,) as Integer) As Boolean
		  // It loads measured data from the database that corresponds to the dataType and that correspond to the headers given in <headers()>.
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The dataAvail boolean array indicates on which grid points there is data available.
		  // The type of data loaded depends of course on the datatype and on the number of 3D arrays given as input.
		  //
		  // Different cases for 3D arrays given as input for this function are:
		  //
		  // dataAvail (Boolean) + one spatial data (Single)
		  // dataAvail (Boolean) + one spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + two spatial data(Single)
		  // dataAvail (Boolean) + two spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + four spatial data (Single)
		  // dataAvail (Boolean) + four spatial data (Single) + qual (int)
		  //
		  // The order of dimensions is (time, latitude, longitude) 
		  //
		  // The method returns true when success, false otherwise. lastMessage property is used for error handling.
		  
		  dim i, j, k as integer
		  dim rs as RecordSet
		  lastMessage=""
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Check for invalid datatype...
		  if dataType=0 then
		  elseif dataType<10 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=10 then
		  elseif dataType<20 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=20 then
		  else
		    lastMessage="Invalid data type!"
		    return false
		  end
		  
		  
		  
		  // Redim variables...
		  Redim dataAvail(headers.ubound, ny-1, nx-1)
		  Redim spData1(headers.ubound, ny-1, nx-1)
		  Redim spData2(headers.ubound, ny-1, nx-1)
		  Redim qual(headers.ubound, ny-1, nx-1)
		  
		  //Replace old values with 'noData'
		  for k=0 to headers.Ubound
		    for j=0 to ny-1
		      for i=0 to nx-1
		        dataAvail(k,j,i)=false
		      next
		    next
		  next
		  
		  
		  // Get data =================================================
		  
		  dim ix, iy, idixiy, idx as integer
		  
		  for k=0 to headers.Ubound
		    
		    rs=db.GetMeasurementDataSpace(CLong(headers(k)), dataType)
		    if rs=nil then
		      lastMessage=lastMessage+EndOfLine +"Problem while querying for data on header " + headers(k) + " with data type " + CStr(dataType)
		      return false
		    end
		    if rs.EOF then continue
		    
		    idx=0
		    while not(rs.EOF)
		      
		      idixiy=rs.Field("id_tb_ixiy").IntegerValue
		      idx=id_ixiys.IndexOf(idixiy,idx)
		      
		      //Minus 1, since they are array indexes...
		      ix=ixs(idx)-1
		      iy=iys(idx)-1
		      
		      dataAvail(k,iy,ix)=true
		      if dataType=0 then
		        spData1(k,iy,ix)=rs.Field("velu").DoubleValue
		        spData2(k,iy,ix)=rs.Field("velv").DoubleValue
		        qual(k,iy,ix)=rs.Field("kl").IntegerValue
		      elseif dataType<10 then
		        spData1(k,iy,ix)=rs.Field("vel").DoubleValue
		        spData2(k,iy,ix)=rs.Field("acc").DoubleValue
		        qual(k,iy,ix)=rs.Field("qual").IntegerValue
		      elseif dataType=10 then
		        spData1(k,iy,ix)=rs.Field("hwave").DoubleValue
		        spData2(k,iy,ix)=rs.Field("dir").DoubleValue
		        qual(k,iy,ix)=rs.Field("qual").IntegerValue
		      elseif dataType<20 then
		        spData1(k,iy,ix)=rs.Field("hwave").DoubleValue
		        qual(k,iy,ix)=rs.Field("qual").IntegerValue
		      elseif dataType=20 then
		        spData1(k,iy,ix)=rs.Field("dir").DoubleValue
		        spData2(k,iy,ix)=rs.Field("speed").DoubleValue
		        qual(k,iy,ix)=rs.Field("qual").IntegerValue
		      end
		      
		      rs.MoveNext
		      
		    wend
		    
		  next
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded data"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadTemporalSpatialData(headers() as string, dataType as integer, dataAvail(,,) as Boolean, spData1(,,) as single, spData2(,,) as single, spData3(,,) as single, spData4(,,) as single) As Boolean
		  // It loads measured data from the database that corresponds to the dataType and that correspond to the headers given in <headers()>.
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The dataAvail boolean array indicates on which grid points there is data available.
		  // The type of data loaded depends of course on the datatype and on the number of 3D arrays given as input.
		  //
		  // Different cases for 3D arrays given as input for this function are:
		  //
		  // dataAvail (Boolean) + one spatial data (Single)
		  // dataAvail (Boolean) + one spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + two spatial data(Single)
		  // dataAvail (Boolean) + two spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + four spatial data (Single)
		  // dataAvail (Boolean) + four spatial data (Single) + qual (int)
		  //
		  // The order of dimensions is (time, latitude, longitude) 
		  //
		  // The method returns true when success, false otherwise. lastMessage property is used for error handling.
		  
		  dim i, j, k as integer
		  dim rs as RecordSet
		  lastMessage=""
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Check for invalid datatype...
		  if dataType=0 then
		  elseif dataType<10 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=10 then
		  elseif dataType<20 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=20 then
		  else
		    lastMessage="Invalid data type!"
		    return false
		  end
		  
		  
		  
		  // Redim variables...
		  Redim dataAvail(headers.ubound, ny-1, nx-1)
		  Redim spData1(headers.ubound, ny-1, nx-1)
		  Redim spData2(headers.ubound, ny-1, nx-1)
		  Redim spData3(headers.ubound, ny-1, nx-1)
		  Redim spData4(headers.ubound, ny-1, nx-1)
		  
		  //Replace old values with 'noData'
		  for k=0 to headers.Ubound
		    for j=0 to ny-1
		      for i=0 to nx-1
		        dataAvail(k,j,i)=false
		      next
		    next
		  next
		  
		  
		  // Get data =================================================
		  
		  dim ix, iy, idixiy, idx as integer
		  
		  for k=0 to headers.Ubound
		    
		    rs=db.GetMeasurementDataSpace(CLong(headers(k)), dataType)
		    if rs=nil then
		      lastMessage=lastMessage+EndOfLine +"Problem while querying for data on header " + headers(k) + " with data type " + CStr(dataType)
		      return false
		    end
		    if rs.EOF then continue
		    
		    idx=0
		    while not(rs.EOF)
		      
		      idixiy=rs.Field("id_tb_ixiy").IntegerValue
		      idx=id_ixiys.IndexOf(idixiy,idx)
		      
		      //Minus 1, since they are array indexes...
		      ix=ixs(idx)-1
		      iy=iys(idx)-1
		      
		      dataAvail(k,iy,ix)=true
		      if dataType=0 then
		        spData1(k,iy,ix)=rs.Field("velu").DoubleValue
		        spData2(k,iy,ix)=rs.Field("velv").DoubleValue
		        spData3(k,iy,ix)=rs.Field("accu").DoubleValue
		        spData4(k,iy,ix)=rs.Field("accv").DoubleValue
		      elseif dataType<10 then
		        spData1(k,iy,ix)=rs.Field("vel").DoubleValue
		        spData2(k,iy,ix)=rs.Field("acc").DoubleValue
		        spData3(k,iy,ix)=rs.Field("var").DoubleValue
		        spData4(k,iy,ix)=rs.Field("pow").DoubleValue
		      elseif dataType=10 then
		        spData1(k,iy,ix)=rs.Field("hwave").DoubleValue
		        spData2(k,iy,ix)=rs.Field("dir").DoubleValue
		      elseif dataType<20 then
		        spData1(k,iy,ix)=rs.Field("hwave").DoubleValue
		      elseif dataType=20 then
		        spData1(k,iy,ix)=rs.Field("dir").DoubleValue
		        spData2(k,iy,ix)=rs.Field("speed").DoubleValue
		      end
		      
		      rs.MoveNext
		      
		    wend
		    
		  next
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded data"
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadTemporalSpatialData(headers() as string, dataType as integer, dataAvail(,,) as Boolean, spData1(,,) as single, spData2(,,) as single, spData3(,,) as single, spData4(,,) as single, qual(,,) as Integer) As Boolean
		  // It loads measured data from the database that corresponds to the dataType and that correspond to the headers given in <headers()>.
		  // Since xojo will read all array inputs in a method as pointers (ByRef, not ByVal), the data is loaded into the arrays given as input.
		  // The dataAvail boolean array indicates on which grid points there is data available.
		  // The type of data loaded depends of course on the datatype and on the number of 3D arrays given as input.
		  //
		  // Different cases for 3D arrays given as input for this function are:
		  //
		  // dataAvail (Boolean) + one spatial data (Single)
		  // dataAvail (Boolean) + one spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + two spatial data(Single)
		  // dataAvail (Boolean) + two spatial data (Single) + qual (int)
		  // dataAvail (Boolean) + four spatial data (Single)
		  // dataAvail (Boolean) + four spatial data (Single) + qual (int)
		  //
		  // The order of dimensions is (time, latitude, longitude) 
		  //
		  // The method returns true when success, false otherwise. lastMessage property is used for error handling.
		  
		  dim i, j, k as integer
		  dim rs as RecordSet
		  lastMessage=""
		  
		  if grid_ID=0 then
		    lastMessage="No grid is loaded!"
		    return false
		  end
		  
		  // Check for invalid datatype...
		  if dataType=0 then
		  elseif dataType<10 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=10 then
		  elseif dataType<20 then
		    if not(BasicStInfoLoaded) then
		      if not(LoadBasicStInfo) then return false
		    end
		  elseif dataType=20 then
		  else
		    lastMessage="Invalid data type!"
		    return false
		  end
		  
		  
		  
		  // Redim variables...
		  Redim dataAvail(headers.ubound, ny-1, nx-1)
		  Redim spData1(headers.ubound, ny-1, nx-1)
		  Redim spData2(headers.ubound, ny-1, nx-1)
		  Redim spData3(headers.ubound, ny-1, nx-1)
		  Redim spData4(headers.ubound, ny-1, nx-1)
		  Redim qual(headers.ubound, ny-1, nx-1)
		  
		  //Replace old values with 'noData'
		  for k=0 to headers.Ubound
		    for j=0 to ny-1
		      for i=0 to nx-1
		        dataAvail(k,j,i)=false
		      next
		    next
		  next
		  
		  
		  // Get data =================================================
		  
		  dim ix, iy, idixiy, idx as integer
		  
		  for k=0 to headers.Ubound
		    
		    rs=db.GetMeasurementDataSpace(CLong(headers(k)), dataType)
		    if rs=nil then
		      lastMessage=lastMessage+EndOfLine +"Problem while querying for data on header " + headers(k) + " with data type " + CStr(dataType)
		      return false
		    end
		    if rs.EOF then continue
		    
		    idx=0
		    while not(rs.EOF)
		      
		      idixiy=rs.Field("id_tb_ixiy").IntegerValue
		      idx=id_ixiys.IndexOf(idixiy,idx)
		      
		      //Minus 1, since they are array indexes...
		      ix=ixs(idx)-1
		      iy=iys(idx)-1
		      
		      dataAvail(k,iy,ix)=true
		      if dataType=0 then
		        spData1(k,iy,ix)=rs.Field("velu").DoubleValue
		        spData2(k,iy,ix)=rs.Field("velv").DoubleValue
		        spData3(k,iy,ix)=rs.Field("accu").DoubleValue
		        spData4(k,iy,ix)=rs.Field("accv").DoubleValue
		        qual(k,iy,ix)=rs.Field("kl").IntegerValue
		      elseif dataType<10 then
		        spData1(k,iy,ix)=rs.Field("vel").DoubleValue
		        spData2(k,iy,ix)=rs.Field("acc").DoubleValue
		        spData3(k,iy,ix)=rs.Field("var").DoubleValue
		        spData4(k,iy,ix)=rs.Field("pow").DoubleValue
		        qual(k,iy,ix)=rs.Field("qual").IntegerValue
		      elseif dataType=10 then
		        spData1(k,iy,ix)=rs.Field("hwave").DoubleValue
		        spData2(k,iy,ix)=rs.Field("dir").DoubleValue
		        qual(k,iy,ix)=rs.Field("qual").IntegerValue
		      elseif dataType<20 then
		        spData1(k,iy,ix)=rs.Field("hwave").DoubleValue
		        qual(k,iy,ix)=rs.Field("qual").IntegerValue
		      elseif dataType=20 then
		        spData1(k,iy,ix)=rs.Field("dir").DoubleValue
		        spData2(k,iy,ix)=rs.Field("speed").DoubleValue
		        qual(k,iy,ix)=rs.Field("qual").IntegerValue
		      end
		      
		      rs.MoveNext
		      
		    wend
		    
		  next
		  
		  //===========================
		  
		  lastMessage=lastMessage+EndOfLine +"Succesfully loaded data"
		  
		  return true
		End Function
	#tag EndMethod


	#tag Note, Name = Updates
		
		
		
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited the module. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new version in the next line, not at the end!!! latest version info should ALWAYS be displayed first!!!
		
		
		2014.08.13
		RG
		-Added LoadDatesAndHeadersWithinRange methods.
		
		2014.08.08
		RG
		-First version
		
		
		IMPORTANT!!! Insert notes of new version at the beginning, not at the end!!! Latest version info should ALWAYS be displayed first!!!
		
	#tag EndNote


	#tag Property, Flags = &h0
		BasicGridInfoLoaded As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		BasicStInfoLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		codeSt(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private grid_ID As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		id_ixiy(-1,-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private id_ixiys(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private ixs(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private iys(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		lastMessage As String
	#tag EndProperty

	#tag Property, Flags = &h0
		lat(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		latSt(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		lon(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		longNameSt(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		lonSt(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		nameSt(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		nx As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		ny As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		trueNorthSt(-1) As Double
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="BasicGridInfoLoaded"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BasicStInfoLoaded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lastMessage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nx"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ny"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
