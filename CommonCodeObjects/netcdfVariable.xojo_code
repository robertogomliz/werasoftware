#tag Class
Protected Class netcdfVariable
	#tag Method, Flags = &h0
		Function AddAttribute(AttribName as String, AttribContent() as double) As Boolean
		   
		  dim i as integer
		  dim auxStr as string
		  
		  if AttribName ="" then
		    returnMsg="Name of variable attribute is empty."
		    return(false)
		  elseif AttribContent.Ubound=-1 then
		    returnMsg="Attribute content is empty."
		    return(false)
		  end if
		  
		  attributeName.Append(AttribName )
		  
		  
		  if dataType=4 then
		    auxStr=replace(Format(AttribContent(0),"-#.######e"),",",".")+"f"
		  else
		    auxStr=replace(Format(AttribContent(0),"-#.##############e"),",",".")
		  end if
		  
		  for i=1 to AttribContent.Ubound
		    
		    if dataType=4 then
		      auxStr=auxStr+", "+replace(Format(AttribContent(i),"-#.######e"),",",".")+"f"
		    else
		      auxStr=auxStr+", "+replace(Format(AttribContent(i),"-#.##############e"),",",".")
		    end if
		    
		  next
		  
		  
		  attributeContent.Append(auxStr)
		  nAttributes=nAttributes+1
		  
		  return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddAttribute(AttribName as String, AttribContent as double) As Boolean
		   
		  
		  if AttribName ="" then
		    returnMsg="Name of variable attribute is empty."
		    return(false)
		  end if
		  
		  attributeName.Append(AttribName )
		  if dataType=4 then
		    attributeContent.Append(replace(Format(AttribContent,"-#.######e"),",","."))+"f"
		  else
		    attributeContent.Append(replace(Format(AttribContent,"-#.##############e"),",","."))
		  end if
		  nAttributes=nAttributes+1
		  
		  return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddAttribute(AttribName as String, AttribContent() as Integer) As Boolean
		   
		  dim i as integer
		  dim auxStr as string
		  
		  if AttribName ="" then
		    returnMsg="Name of variable attribute is empty."
		    return(false)
		  elseif AttribContent.Ubound=-1 then
		    returnMsg="Attribute content is empty."
		    return(false)
		  end if
		  
		  attributeName.Append(AttribName )
		  
		  if dataType=1 then
		    auxStr=CStr(AttribContent(0))+"b"
		  elseif dataType=3 then
		    auxStr=CStr(AttribContent(0))+"L"
		  else
		    auxStr=CStr(AttribContent(0))
		  end if
		  
		  
		  for i=1 to AttribContent.Ubound
		    if dataType=1 then
		      auxStr=auxStr+", "+CStr(AttribContent(i))+"b"
		    elseif dataType=3 then
		      auxStr=auxStr+", "+CStr(AttribContent(i))+"L"
		    else
		      auxStr=auxStr+", "+CStr(AttribContent(i))
		    end if
		  next
		  attributeContent.Append(auxStr)
		  nAttributes=nAttributes+1
		  
		  return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddAttribute(AttribName as String, AttribContent as Integer) As Boolean
		   
		  
		  if AttribName ="" then
		    returnMsg="Name of variable attribute is empty."
		    return(false)
		  end if
		  
		  attributeName.Append(AttribName )
		  if dataType=1 then
		    attributeContent.Append(CStr(AttribContent)+"b")
		  elseif dataType=3 then
		    attributeContent.Append(CStr(AttribContent)+"L")
		  else
		    attributeContent.Append(CStr(AttribContent))
		  end if
		  nAttributes=nAttributes+1
		  
		  return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddAttribute(AttribName as String, AttribContent() as string) As Boolean
		   
		  dim quotes as string=Chr(34)
		  dim auxStr as string
		  dim i as integer
		  
		  if AttribName ="" then
		    returnMsg="Name of variable attribute is empty."
		    return(false)
		  elseif AttribContent.Ubound =0 then
		    returnMsg="Variable attribute content is empty."
		    return(false)
		  else
		    for i=0 to AttribContent.Ubound
		      if AttribContent(0)="" then
		        returnMsg="Variable attribute content is empty."
		        return(false)
		      end if
		    next
		  end if
		  
		  
		  attributeName.Append(AttribName )
		  
		  //Check for CDL notation
		  if IsNumeric(AttribContent(0)) then
		    
		  elseif   (AttribContent(0).Right(1)="b" OR AttribContent(0).Right(1)="B" OR AttribContent(0).Right(1)="f" OR AttribContent(0).Right(1)="F" OR AttribContent(0).Right(1)="L" ) AND IsNumeric(AttribContent(0).Left(AttribContent(0).len -1))   then
		    
		  else
		    
		    for i=0 to AttribContent.Ubound
		      if Left(AttribContent(i),1)<>quotes then AttribContent(i)=quotes+AttribContent(i)
		      if Right(AttribContent(i),1)<>quotes then AttribContent(i)=AttribContent(i)+quotes
		    next
		    
		  end if
		  
		  auxStr=CStr(AttribContent(0))
		  for i=1 to AttribContent.Ubound
		    auxStr=auxStr+", "+CStr(AttribContent(i))
		  next
		  
		  
		  attributeContent.Append(auxStr)
		  nAttributes=nAttributes+1
		  
		  return(true)
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddAttribute(AttribName as String, AttribContent as string) As Boolean
		   
		  dim quotes as string=Chr(34)
		  
		  if AttribName ="" then
		    returnMsg="Name of variable attribute is empty."
		    return(false)
		  elseif AttribContent ="" then
		    returnMsg="Variable attribute content is empty."
		    return(false)
		  end if
		  
		  attributeName.Append(AttribName )
		  
		  //Check for CDL notation
		  if IsNumeric(AttribContent) then
		    
		  elseif   (AttribContent.Right(1)="b" OR AttribContent.Right(1)="B" OR AttribContent.Right(1)="f" OR AttribContent.Right(1)="F" OR AttribContent.Right(1)="L" ) AND IsNumeric(AttribContent.Left(AttribContent.len -1))   then
		    
		  else
		    if Left(AttribContent,1)<>quotes then AttribContent=quotes+AttribContent
		    if Right(AttribContent,1)<>quotes then AttribContent=AttribContent+quotes
		  end if
		  
		  attributeContent.Append(AttribContent)
		  nAttributes=nAttributes+1
		  
		  return(true)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddAttributes(AttribNames() as String, AttribContents() as string) As Boolean
		  // This method is just an interface to AddAttribute method to have less code when adding a lot of attributes.
		  
		  dim i as integer
		  
		  if AttribNames.Ubound<0 OR AttribNames.Ubound<>AttribContents.Ubound then
		    returnMsg="List attribute names and list of attribute contents have different size."
		    return(false)
		  end
		  
		  for i=0 to AttribNames.Ubound
		    if not( AddAttribute(AttribNames(i),AttribContents(i)) ) then
		      Return false
		    end
		  next
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetAttribute(attName as string) As string
		  
		  dim i as integer
		  
		  for i=0 to attributeName.Ubound
		    if attributeName(i)=attName  then return attributeContent(i)
		  next
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetData(dataInput(,,,) as Variant)
		  dataArray=dataInput
		End Sub
	#tag EndMethod


	#tag Note, Name = Updates
		
		
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited the module. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new version in the next line, not at the end!!! latest version info should ALWAYS be displayed first!!!
		
		
		2014.07.28
		RG
		-Added GetAttribute method.
		
		2014.07.25
		RG
		-First version
		
		
		IMPORTANT!!! Insert notes of new version at the beginning, not at the end!!! Latest version info should ALWAYS be displayed first!!!
		
	#tag EndNote


	#tag Property, Flags = &h0
		attributeContent(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		attributeName(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		dataArray(-1,-1,-1,-1) As Variant
	#tag EndProperty

	#tag Property, Flags = &h0
		dataType As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		FillValue As variant
	#tag EndProperty

	#tag Property, Flags = &h0
		info As String
	#tag EndProperty

	#tag Property, Flags = &h0
		name As String
	#tag EndProperty

	#tag Property, Flags = &h0
		nAttributes As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		nDimensions As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		returnMsg As String
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			Since this is an array, index of items is 0-based. That is, the first item of the array has an index of 0
			Content, however, is 1-based... this means that the content of the array can never be 0 since dimension 0 does not exists.
		#tag EndNote
		usedDimensions(-1) As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="dataType"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="info"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nAttributes"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nDimensions"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="returnMsg"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
