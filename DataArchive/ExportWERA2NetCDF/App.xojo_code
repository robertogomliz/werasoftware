#tag Class
Protected Class App
Inherits ConsoleApplication
	#tag Event
		Function Run(args() as String) As Integer
		  //test comment from leif PC
		  
		  if DebugBuild then
		    ct.MsgLog(false," ",True)
		    ct.MsgLog(false,"AAHH... ICH LAUFE GERADE IN DEBUG MODE. VERGISS NICHT DIE QUELLCODE ÄNDERUNGEN ZU DOKUMENTIEREN!!! (mach es in App.Version_Notes).",True)
		    execDir="/home/wera/ApplicationSoftware_WOGBUE/DataArchivePrograms/"
		    break
		  else
		    execDir=App.ExecutableFile.Parent.AbsolutePath
		  end
		  dim auxDbl as double
		  dim datenow as date
		  datenow=new date
		  auxDbl=datenow.TotalSeconds
		  
		  
		  DBConfigFile=execDir+"../DataArchivePrograms/DataArchive_setup.txt"
		  
		  if args.Ubound>=1 then
		    
		    if args(1)="A" then
		      MODE=1
		      ct.Mode=1
		      if AssignInput("t",args(2)) then Quit
		      if AssignInput("d",args(3)) then Quit
		      if AssignInput("o",args(4)) then Quit
		      if AssignInput("q",args(5)) then Quit
		      if AssignInput("f",args(6)) then Quit
		      if AssignInput("g",args(7)) then Quit
		      
		      
		      kLogFile=args(8)
		      
		    else
		      MODE=0
		      
		      ct.ConsoleOutput=Join(args," ") + EndOfLine
		      //================ Check input arguments ============
		      if ct.CheckInputs(args) then
		        ct.MsgLog(false," ",True)
		        Quit
		      end
		      
		    end
		  end
		  
		  
		  
		  //============Licence check================
		  if key.LoadLicenceFile(App.execDir + "../") then
		    if not(std.ActivateMBSComplete) then  ct.MsgLog(false,"MBS Plugin serial not valid?",True)
		    if key.LicenceIsValidOnThisPC then
		      if not( key.PackageIsValid(key.Packages.DataArchive))  then
		        ct.MsgLog(true,"Licence does not include WERA DataArchive tools! Application terminated.",true)
		        Quit
		      end
		    else
		      ct.MsgLog(true,"Could not validate licence! Application terminated.",true)
		      ct.MsgLog(false, key.lastMessage_key,true)
		      Quit
		    end
		  else
		    ct.MsgLog(true, "Could not load licence file! Application terminated.", true)
		    ct.MsgLog(false, key.lastMessage_key,true)
		    Quit
		  end
		  
		  
		  
		  //================ Check database connection ============
		  db.archive=new MySQLDatabase
		  if MODE=1 then
		    db.archive.DatabaseName = args(9)
		    db.archive.Host = args(10)
		    db.archive.Username = args(11)
		    db.archive.Password = args(12)
		  else
		    if std.CheckForFolderitem(false,DBConfigFile)=0 then
		      if not(db.ReadDBSetupFile(DBConfigFile)) then
		        ct.MsgLog(true,db.lastMessage_db,True)
		        'Quit
		      end
		    else
		      ct.MsgLog(true, " DB config file error :" +std.lastMessage_std+" !!",True)
		      Quit
		    end
		  end
		  if db.CheckDatabaseConnection then
		    ct.MsgLog(false,db.lastMessage_db,True)
		  else
		    ct.MsgLog(true,db.lastMessage_db + " Application terminated.",True)
		    Quit
		  end
		  
		  
		  DA=new WERA_DB_DataLoader
		  NC=new netcdfFile
		  
		  //================ Other checks required ============
		  if CheckEverything() then
		    ct.MsgLog(false," ",True)
		    Quit
		  end
		  
		  //============Setting grid automatically in case not indicated...============
		  if db.grid="" OR db.grid="0" then
		    if not(db.SetGridAutomatically(dataType, timePoint.SQLDateTime, timePointEnd.SQLDateTime, true)) then
		      ct.MsgLog(true,"Could not set grid automatically! Application terminated.",True)
		      Quit
		    end if
		  end
		  if not( DA.LoadBasicGrid(CLong(db.grid)) ) then
		    ct.MsgLog(true, DA.lastMessage +" Application terminated.",True)
		    Quit
		  end if
		  
		  //========Get headers and dates within time range========
		  if not(GetHeadersAndDates()) then
		    ct.MsgLog(false," ",True)
		    Quit
		  end
		  
		  //===========Set basic dimensions and basic grid info =============
		  if not(SetAndLoadBasicDimensions()) then
		    ct.MsgLog(false," ",True)
		    Quit
		  end
		  
		  //======== Loading data for all time instants============
		  if dataType = 0 then
		    
		    if not(SetAndLoadVectorCurrent()) then
		      ct.MsgLog(false," ",True)
		      Quit
		    end
		    
		    if specialFormat=1 then
		      if not(IncludeRadialDataToo()) then
		        ct.MsgLog(false," ",True)
		        Quit
		      end
		    else
		      measTimeDBs=Nil 'Releasing some memory
		    end if
		    
		    
		  elseif dataType<10 then
		    
		    if not(SetAndLoadRadialCurrent()) then
		      ct.MsgLog(false," ",True)
		      Quit
		    end
		    
		  elseif dataType=10 then
		    
		    if not(SetAndLoadWave()) then
		      ct.MsgLog(false," ",True)
		      Quit
		    end
		    
		  elseif dataType<20 then
		    
		    if not(SetAndLoadRadialCurrent()) then
		      ct.MsgLog(false," ",True)
		      Quit
		    end
		    
		  else
		    
		    if not(SetAndLoadWind()) then
		      ct.MsgLog(false," ",True)
		      Quit
		    end
		    
		  end
		  
		  db.archive.Close
		  
		  
		  //Create CDL===================================================
		  ct.MsgLog(false,"Creating CDL file (may take a few minutes)...",false)
		  dim auxStr as string
		  
		  if outputFile.Directory then
		    if timePoint.TotalSeconds=timePointEnd.TotalSeconds then
		      
		      if specialFormat=2 then
		        auxStr=std.Date2yyyymmddhhmmss(timePoint,withSeconds)
		      else
		        auxStr=std.Date2Gurgeltime(timePoint,withSeconds)
		      end
		    else
		      if specialFormat=2 then
		        auxStr=std.Date2yyyymmddhhmmss(timePoint,withSeconds)+"_"+std.Date2yyyymmddhhmmss(timePointEnd,withSeconds)
		      else
		        auxStr=std.Date2Gurgeltime(timePoint,withSeconds) + "_" + std.Date2Gurgeltime(timePointEnd,withSeconds)
		      end
		    end if
		    
		    if dataType=0 then
		      auxStr=auxStr+"_CurrentVectors"
		    elseif dataType<10 then
		      auxStr=auxStr+"_" +DA.codeSt(dataType-1) + "_RadialCurrents"
		    elseif dataType=10 then
		      auxStr=auxStr+"_WaveField"
		    elseif dataType<20 then
		      auxStr=auxStr+"_" +DA.codeSt(dataType-11) + "_RadialCurrents"
		    else
		      auxStr=auxStr+"_WindField"
		    end if
		    
		  end if
		  
		  DA=Nil  //Hopefully this would release a little memory usage...
		  
		  if not( NC.CreateOnlyCDL(outputFile.AbsolutePath + auxStr  ) ) then
		    ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		    Quit
		  end if
		  ct.MsgLog(false,"Done!",true)
		  
		  
		  
		  
		  
		  
		  //Create NetCDF===================================================
		  ct.MsgLog(false,"Creating NetCDF file (may take a few minutes)...",false)
		  dim strAux as string
		  strAux=NC.cdlFile.NativePath
		  auxStr=strAux
		  
		  NC=Nil //This should realease a lot of memory usage...
		  
		  if strAux.Right(4)= ".cdl" then
		    strAux=Left(strAux, strAux.Len - 4) + ".nc"
		  elseif strAux.Right(3)= ".nc" then
		    
		  else
		    strAux=strAux + ".nc"
		  end
		  
		  if not(std.ShellExec("ncgen -o " + strAux + " " +auxStr )) then
		    ct.MsgLog(true,"Problem using ncgen to convert from CDL to netCDF: " + std.lastMessage_std + " Application terminated.",True)
		    Quit
		  end if
		  
		  if std.CheckForFolderitem(false, strAux)<>0 then
		    ct.MsgLog(true,"Couldn't find netcdf file (" + strAux + ") after being created! Application terminated.",True)
		    Quit
		  end if
		  
		  ct.MsgLog(false,"Done!",true)
		  
		  if Not(DebugBuild) then
		    dim fi as folderitem
		    fi=GetFolderItem(auxStr)
		    if fi<>Nil then fi.Delete
		  end
		  
		  ct.MsgLog(false,"Finished!",True)
		  ct.MsgLog(false," ",True)
		  
		  
		  if DebugBuild then
		    datenow=new date
		    ct.MsgLog(false,CStr(datenow.TotalSeconds-auxDbl) + " seconds of execution time",True)
		  end
		  if Mode=1 then Print "TRUE"
		End Function
	#tag EndEvent

	#tag Event
		Function UnhandledException(error As RuntimeException) As Boolean
		  dim errorString, stackArray() as String
		  dim i as integer
		  
		  errorString=EndOfLine + EndOfLine + "There was an unhandled exception!"
		  errorString=errorString + EndOfLine + EndOfLine + "Error number " + CStr(error.ErrorNumber) + ": " + error.Message + EndOfLine + EndOfLine
		  errorString=errorString + "Error stack:" + EndOfLine
		  
		  stackArray=error.Stack
		  for i=stackArray.Ubound DownTo 0
		    errorString=errorString + stackArray(i) + EndOfLine
		  next
		  
		  ct.MsgLog(false," ",True)
		  ct.MsgLog(false,"Unhandled Exception! Application terminated.",True)
		  
		  ct.MsgLog(false, errorString,True)
		  
		  log.Write(false, ct.ConsoleOutput)
		  
		  ct.SendOutputReport("3","1")
		End Function
	#tag EndEvent


	#tag Method, Flags = &h0
		Function AssignInput(char as string, content as String) As boolean
		  
		  Dim auxInt as Integer
		  
		  if StrComp(char,"t",0)=0 then
		    timePoint=new date
		    auxInt=len(content)
		    if auxInt=11 or auxInt=13 then
		      if IsNumeric(content) then
		        timePoint.SQLDateTime=std.Gurgeltime2DBstring(content)
		        if auxInt=13 then withSeconds=true   //This is to generate output files with seconds too! e.g. from DataManager program.
		        return false
		      end if
		    elseif auxInt=19 then
		      try
		        timePoint.SQLDateTime=content.Left(10) + " " + content.Right(8)
		      catch err as UnsupportedFormatException
		        ct.MsgLog(true,"Date label not understood! Application terminated.",True)
		        ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		        Return true
		      end
		      return false
		    end if
		    
		    ct.MsgLog(true,"Date label not understood! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		    
		  elseif StrComp(char,"T",0)=0 then
		    timePointEnd=new date
		    auxInt=len(content)
		    if auxInt=11 or auxInt=13 then
		      if IsNumeric(content) then
		        timePointEnd.SQLDateTime=std.Gurgeltime2DBstring(content)
		        return false
		      end if
		    elseif auxInt=19 then
		      try
		        timePointEnd.SQLDateTime=content.Left(10) + " " + content.Right(8)
		      catch err as UnsupportedFormatException
		        ct.MsgLog(true,"Date label not understood! Application terminated.",True)
		        ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		        Return true
		      end
		      return false
		    end if
		    
		    ct.MsgLog(true,"End date label not understood! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		    
		    
		  elseif StrComp(char,"d",0)=0 then
		    
		    if content.Len>0 AND IsNumeric(content) then
		      dataType=CLong(Content)
		      if dataType>=0 and dataType<=20 then return false
		    end if
		    
		    ct.MsgLog(true,"Data type input has to be numeric! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		    
		  elseif StrComp(char,"x",0)=0 then
		    
		    if content.Len>0 AND IsNumeric(content) then
		      xStr=content
		      return false
		    end if
		    
		    ct.MsgLog(true,"Longitudinal reference input has to be numeric! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		    
		  elseif StrComp(char,"y",0)=0 then
		    
		    if content.Len>0 AND IsNumeric(content) then
		      yStr=content
		      return false
		    end if
		    
		    ct.MsgLog(true,"Latitudinal reference input has to be numeric! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		    
		  elseif StrComp(char,"q",0)=0 then
		    
		    if content.Len>0 AND IsNumeric(content) then
		      qualFilt=CLong(Content)
		      if qualFilt>=0 and qualFilt<=6 then return false
		      ct.MsgLog(true,"Quality filter is not a valid number! Application terminated.",True)
		      ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		      Return true
		    end if
		    
		    ct.MsgLog(true,"Quality filter input has to be numeric! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		    
		  elseif StrComp(char,"f",0)=0 then
		    
		    if content.Len>0 AND IsNumeric(content) then
		      specialFormat=CLong(Content)
		      ct.MsgLog(true,"Special format "+cstr(specialFormat),True)
		      if specialFormat>=0 and specialFormat<=3 then return false
		      ct.MsgLog(true,"Special format input paramter is not a valid number! Application terminated.",True)
		      ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		      Return true
		    end if
		    
		    ct.MsgLog(true,"Special format input has to be numeric! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		    
		  elseif StrComp(char,"g",0)=0 then
		    
		    if content.Len>0 AND IsNumeric(content) then
		      db.grid=CStr(CLong(Content))   // I think this would throw an error... db is NIL at this point...
		      return false
		    end if
		    
		    ct.MsgLog(true,"Grid ID input has to be numeric! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		    
		  elseif StrComp(char,"o",0)=0 then
		    
		    if content.Len=0 then
		      ct.MsgLog(true,"No output path supplied! Application terminated.",True)
		      ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		      return true
		    end if
		    
		    outputFile=GetFolderItem(content)
		    if outputFile=Nil then
		      ct.MsgLog(true,"Invalid output path! Application terminated.",True)
		      ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		      Return true
		    end
		  elseif StrComp(char,"c",0)=0 then
		    DBconfigFile=content
		  else
		    return true
		  end
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function CheckEverything() As boolean
		  
		  if xStr<>"" AND yStr<>"" then   ' a time series needs to be exported.
		    
		    ct.MsgLog(true,"Time-series export not yet implemented! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		    
		    timeseries=true
		  else
		    
		    if IsNull(timePointEnd) then
		      
		      timePointEnd=new date
		      timePointEnd=timePoint
		      
		    else
		      
		      if timePointEnd.TotalSeconds<timePoint.TotalSeconds then
		        ct.MsgLog(true,"End is before start date! Application terminated.",True)
		        ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		        Return true
		      end
		      
		    end if
		    
		  end if
		  
		  if (dataType mod 10)>0 then
		    
		    if not(DA.LoadBasicStInfo) then
		      ct.MsgLog(true, DA.lastMessage +" Application terminated.",True)
		      Return true
		    end if
		    
		    if db.sites<dataType then
		      ct.MsgLog( true, "Station "+ CStr(dataType) + " does not exists! Application terminated.",True)
		      Return true
		    end if
		    
		  elseif not(dataType=0 OR dataType=10 OR dataType=20) then
		    ct.MsgLog(true,"Invalid data type! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		  end if
		  
		  if outputFile=nil then
		    ct.MsgLog(true,"Output path is not given! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		  end
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function GetHeadersAndDates() As Boolean
		  
		  
		  
		  
		  if (dataType=0 OR dataType=10) AND specialFormat=1 then   'We need to load the radial headers too!
		    if not(  DA.LoadDatesAndHeadersWithinRange( measTimeDBs, id_headers, dataType, timePoint, timePointEnd, id_rad_headers )  ) then
		      ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		      Return false
		    end if
		  else
		    if not(  DA.LoadDatesAndHeadersWithinRange( measTimeDBs, id_headers, dataType, timePoint, timePointEnd )  ) then
		      ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		      Return false
		    end if
		  end if
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function IncludeRadialDataToo() As Boolean
		  dim auxVar1(-1) as Variant
		  dim auxVar2(-1,-1) as Variant
		  dim auxSgl31(-1,-1,-1), auxSgl32(-1,-1,-1), auxSgl33(-1,-1,-1), auxSgl34(-1,-1,-1) as single
		  dim auxInt3q(-1,-1,-1) as integer
		  dim auxBool3(-1,-1,-1) as Boolean
		  dim i, j, k, l as integer
		  dim rs as RecordSet
		  
		  dim auxSglFillValue, auxSgl as Single
		  auxSglFillValue=CDbl("9.96921e+36")
		  
		  ct.MsgLog(false,"---Also including radial data contribution to each combined measurement!---",true)
		  
		  if timeseries then
		    
		    //Not yet implemented...
		    
		  else
		    
		    
		    if not(DA.LoadBasicStInfo) then
		      ct.MsgLog(true, DA.lastMessage +" Application terminated.",True)
		      Return true
		    end if
		    
		    if db.sites<1 then
		      ct.MsgLog( true, "Could not find any station! Application terminated.",True)
		      Return true
		    end if
		    
		    
		    //Adding data of additionally required dimensions
		    ct.MsgLog(false,"Adding station dimensions and their data...",false)
		    if not(NC.AddDimension("station",db.sites)) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not(NC.AddDimension("stringLength",11)) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      return false
		    end
		    
		    
		    //Adding the station names
		    
		    'station
		    Redim auxVar1(db.sites-1)
		    for i=1 to db.sites
		      auxVar1(i-1)=i
		    next
		    if not( NC.AddVariable( "station", Array(4), auxVar1  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      return false
		    end
		    'station_names
		    for i=1 to db.sites
		      auxVar1(i-1)=DA.nameSt(i-1)
		    next
		    if not( NC.AddVariable( "station_name", Array(4,5), auxVar1  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      return false
		    end
		    
		    'Attributes
		    if not( NC.GetVariable("station").AddAttribute("axis","S") ) then
		      ct.MsgLog(true,NC.GetVariable("station").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("station").AddAttribute("_FillValue","-2147483647") ) then
		      ct.MsgLog(true,NC.GetVariable("station").returnMsg + " Application terminated.",True)
		      return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    
		    
		    
		    
		    
		    
		    //Adding the station measurement times
		    ct.MsgLog(false,"Adding measurement times of the stations...",false)
		    Redim auxVar2(measTimeDBs.Ubound, db.sites-1)
		    for i=0 to measTimeDBs.Ubound
		      for j=0 to db.sites-1
		        rs=db.GetHeaderDataFromHeaderID(j+1,id_rad_headers(i,j))
		        if rs.EOF then
		          ct.MsgLog(true,"Could not find measurement date from radial header " + id_rad_headers(i,j) + "! Application terminated.",True)
		          return false
		        end
		        auxVar2(i,j)=NC.DateString2NetCDFTime(rs.Field("measTimeDB").DateValue.SQLDateTime)
		      next
		    next
		    
		    if not( NC.AddVariable( "rad_meas_times", Array(3,4), auxVar2  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      return false
		    end
		    
		    'Attributes
		    if not( NC.GetVariable("rad_meas_times").AddAttribute("units","days since 1900-01-01T00:00:00 UTC") ) then
		      ct.MsgLog(true,NC.GetVariable("rad_meas_times").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("rad_meas_times").AddAttribute("standard_name","rad_meas_times") ) then
		      ct.MsgLog(true,NC.GetVariable("rad_meas_times").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("rad_meas_times").AddAttribute("long_name","Julian day (UT) of each radial measurement") ) then
		      ct.MsgLog(true,NC.GetVariable("rad_meas_times").returnMsg + " Application terminated.",True)
		      return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    
		    
		    ct.MsgLog(false,"Loading and adding station coordinates...",false)
		    
		    for j=0 to db.sites-1
		      'longitude_s
		      auxSgl=DA.lonSt(j)
		      auxVar1(j)=auxSgl
		    next
		    
		    if not( NC.AddVariable( "longitude_s", Array(4), auxVar1  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    for j=0 to db.sites-1
		      'latitude_s
		      auxSgl=DA.latSt(j)
		      auxVar1(j)=auxSgl
		    next
		    
		    if not( NC.AddVariable( "latitude_s", Array(4), auxVar1  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    ct.MsgLog(false,"Done!",true)
		    
		    
		    
		    
		    
		    //Loading and setting radial angle
		    ct.MsgLog(false,"Loading radial angles...",false)
		    dim radData(-1, -1) as single
		    Redim auxSgl31( db.sites-1, DA.ny-1, DA.nx-1 )
		    
		    for k=0 to db.sites-1
		      
		      if not( DA.LoadRadAngle(k+1, radData ) ) then
		        ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		        Return false
		      end if
		      
		      for j=0 to UBound(radData,2)
		        for i=0 to UBound(radData,1)
		          auxSgl31(k,j,i)=radData(i,j)
		        next
		      next
		    next
		    
		    ct.MsgLog(false,"Adding data to netcdf virtual object...",false)
		    if not( NC.AddVariable( "hcdt", Array(4,2,1), auxSgl31  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    
		    
		    
		    //Loading measured data
		    dim auxVar41(-1,-1,-1,-1), auxVar42(-1,-1,-1,-1), auxVar43(-1,-1,-1,-1), auxVar44(-1,-1,-1,-1), auxVar4q(-1,-1,-1,-1) as Variant
		    Redim auxVar41(measTimeDBs.Ubound, db.sites-1, UBound(radData,2), UBound(radData,1))
		    Redim auxVar42(measTimeDBs.Ubound, db.sites-1, UBound(radData,2), UBound(radData,1))
		    Redim auxVar43(measTimeDBs.Ubound, db.sites-1, UBound(radData,2), UBound(radData,1))
		    Redim auxVar44(measTimeDBs.Ubound, db.sites-1, UBound(radData,2), UBound(radData,1))
		    Redim auxVar4q(measTimeDBs.Ubound, db.sites-1, UBound(radData,2), UBound(radData,1))
		    
		    for l=0 to db.sites-1
		      
		      ct.MsgLog(false,"Loading radial data contribution from station "  + CStr(l+1) + " (may take several seconds)...",false)
		      
		      for i=0 to id_headers.Ubound
		        id_headers(i)=id_rad_headers(i,l)
		      next
		      
		      if not( DA.LoadTemporalSpatialData( id_headers, l+1, auxBool3, auxSgl31, auxSgl32, auxSgl33, auxSgl34, auxInt3q ) ) then
		        ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		        Return false
		      end if
		      ct.MsgLog(false,"Done!",true)
		      
		      ct.MsgLog(false,"Assign fill values where required...",false)
		      for k=0 to measTimeDBs.Ubound
		        for j=0 to UBound(radData,2)
		          for i=0 to UBound(radData,1)
		            if auxBool3(k,j,i) then
		              auxVar41(k,l,j,i)=auxSgl31(k,j,i)
		              auxVar42(k,l,j,i)=auxSgl32(k,j,i)
		              auxVar43(k,l,j,i)=auxSgl33(k,j,i)
		              auxVar44(k,l,j,i)=auxSgl34(k,j,i)
		              auxVar4q(k,l,j,i)=auxInt3q(k,j,i)
		            else
		              auxVar41(k,l,j,i)=auxSglFillValue
		              auxVar42(k,l,j,i)=auxSglFillValue
		              auxVar43(k,l,j,i)=auxSglFillValue
		              auxVar44(k,l,j,i)=auxSglFillValue
		              auxVar4q(k,l,j,i)=-128
		            end if
		          next
		        next
		      next
		      ct.MsgLog(false,"Done!",true)
		    next
		    auxSgl31=Nil  //Releasing allocated memory (hopefully)
		    auxSgl32=Nil
		    auxSgl33=Nil
		    auxSgl34=Nil
		    auxInt3q=Nil
		    measTimeDBs=Nil
		    
		    
		    ct.MsgLog(false,"Adding radial current velocity to netcdf virtual object...",false)
		    if not( NC.AddVariable( "hcsp", Array(3,4,2,1), auxVar41  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxVar41=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding radial current velocity accuracy to netcdf virtual object...",false)
		    if not( NC.AddVariable( "hcsp_error", Array(3,4,2,1), auxVar42  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxVar42=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding radial current velocity variance to netcdf virtual object...",false)
		    if not( NC.AddVariable( "variance", Array(3,4,2,1), auxVar43  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxVar43=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding power to netcdf virtual object...",false)
		    if not( NC.AddVariable( "power", Array(3,4,2,1), auxVar44  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxVar44=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding radial current velocity quality to netcdf virtual object...",false)
		    if not( NC.AddVariable( "qual", Array(3,4,2,1), auxVar4q  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxVar4q=Nil  //Releasing allocated memory (hopefully)
		    
		    
		    
		    
		    
		    ct.MsgLog(false,"Adding attributes to variables in netcdf virtual object...",false)
		    dim auxStrArr1(-1), auxStrArr2(-1) as string
		    auxStrArr1=Array("long_name","standard_name","units","_FillValue","valid_min","valid_max")
		    
		    auxStrArr2=Array("longitude_s","longitude_s","degree_east","9.96921e+36f","-180.f","180.f")
		    if not( NC.GetVariable("longitude_s").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("longitude_s").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("latitude_s","latitude_s","degree_north","9.96921e+36f","-90.f","90.f")
		    if not( NC.GetVariable("latitude_s").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("latitude_s").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Radial current direction","direction_of_sea_water_velocity","degree","9.96921e+36f","0.f","360.f")
		    if not( NC.GetVariable("hcdt").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("hcdt").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Radial current velocity","sea_water_speed","m.s-1","9.96921e+36f","-100.f","100.f")
		    if not( NC.GetVariable("hcsp").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("hcsp").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Accuracy of radial current velocity","error_on_sea_water_speed","m.s-1","9.96921e+36f","-100.f","100.f")
		    if not( NC.GetVariable("hcsp_error").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("hcsp_error").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr1=Array("long_name","standard_name","units","_FillValue")
		    auxStrArr2=Array("Variance of measured radial current velocity","variance_on_sea_water_speed","m.s-1","9.96921e+36f")
		    if not( NC.GetVariable("variance").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("variance").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr1=Array("long_name","standard_name","units","_FillValue")
		    auxStrArr2=Array("Power associated to measured radial current velocity","power_on_sea_water_speed","dB","9.96921e+36f")
		    if not( NC.GetVariable("power").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("power").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr1=Array("long_name","standard_name","_FillValue")
		    auxStrArr2=Array("WERA radial current quality","quality_of_radial_sea_water_velocity","-128")
		    if not( NC.GetVariable("qual").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("qual").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    ct.MsgLog(false,"Done!",true)
		    
		    
		  end if
		  
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SetAndLoadBasicDimensions() As Boolean
		  
		  dim auxVar1(-1) as Variant
		  dim i as integer
		  
		  if timeseries then
		    
		    //Not yet implemented...
		    
		  else
		    
		    
		    if not(DA.LoadBasicGrid(CLong(db.grid))) then
		      ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		      Return false
		    end if
		    
		    if not(NC.AddDimension("longitude",DA.nx)) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    if not(NC.AddDimension("latitude",DA.ny)) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    if not(NC.AddDimension("time",0)) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    
		    // Add data from dimensions as variables...
		    
		    'longitude
		    Redim auxVar1(DA.nx-1)
		    for i=0 to auxVar1.Ubound
		      auxVar1(i)=DA.lon(i)
		    next
		    if not( NC.AddVariable( "longitude", Array(1), auxVar1  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    'latitude
		    Redim auxVar1(DA.ny-1)
		    for i=0 to auxVar1.Ubound
		      auxVar1(i)=DA.lat(i)
		    next
		    if not( NC.AddVariable( "latitude", Array(2), auxVar1  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    'time
		    if specialFormat=2 then
		      Redim auxVar1(measTimeDBs.Ubound)
		      for i=0 to measTimeDBs.Ubound
		        auxVar1(i)=cdbl(std.Date2yyyymmddhhmmss(measTimeDBs(i),withSeconds))
		      next
		      if not( NC.AddVariable( "time", Array(3), auxVar1  ) ) then
		        ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		        Return false
		      end
		    else
		      Redim auxVar1(measTimeDBs.Ubound)
		      if not(NC.setNetCDFTimeReference("days since 1900-01-01T00:00:00 UTC")) then
		        ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		        Return false
		      end
		      Redim auxVar1(measTimeDBs.Ubound)
		      for i=0 to measTimeDBs.Ubound
		        auxVar1(i)=NC.DateString2NetCDFTime(measTimeDBs(i).SQLDateTime)
		      next
		      if not( NC.AddVariable( "time", Array(3), auxVar1  ) ) then
		        ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		        Return false
		      end
		    end
		    
		    
		    //Add attributes
		    'longitude
		    if not( NC.GetVariable("longitude").AddAttribute("axis","X") ) then
		      ct.MsgLog(true,NC.GetVariable("longitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("longitude").AddAttribute("units","degree_east") ) then
		      ct.MsgLog(true,NC.GetVariable("longitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("longitude").AddAttribute("standard_name","longitude") ) then
		      ct.MsgLog(true,NC.GetVariable("longitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("longitude").AddAttribute("long_name","longitude") ) then
		      ct.MsgLog(true,NC.GetVariable("longitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("longitude").AddAttribute("_FillValue","9.96921e+36f") ) then
		      ct.MsgLog(true,NC.GetVariable("longitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("longitude").AddAttribute("valid_min","-180.f") ) then
		      ct.MsgLog(true,NC.GetVariable("longitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("longitude").AddAttribute("valid_max","180.f") ) then
		      ct.MsgLog(true,NC.GetVariable("longitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    
		    'latitude
		    if not( NC.GetVariable("latitude").AddAttribute("axis","Y") ) then
		      ct.MsgLog(true,NC.GetVariable("latitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("latitude").AddAttribute("units","degree_north") ) then
		      ct.MsgLog(true,NC.GetVariable("latitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("latitude").AddAttribute("standard_name","latitude") ) then
		      ct.MsgLog(true,NC.GetVariable("latitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("latitude").AddAttribute("long_name","latitude") ) then
		      ct.MsgLog(true,NC.GetVariable("latitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("latitude").AddAttribute("_FillValue","9.96921e+36f") ) then
		      ct.MsgLog(true,NC.GetVariable("latitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("latitude").AddAttribute("valid_min","-90.f") ) then
		      ct.MsgLog(true,NC.GetVariable("latitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("latitude").AddAttribute("valid_max","90.f") ) then
		      ct.MsgLog(true,NC.GetVariable("latitude").returnMsg + " Application terminated.",True)
		      return false
		    end
		    
		    'time
		    if specialFormat=2 then
		      'time
		      if not( NC.GetVariable("time").AddAttribute("axis","T") ) then
		        ct.MsgLog(true,NC.GetVariable("time").returnMsg + " Application terminated.",True)
		        return false
		      end
		      if not( NC.GetVariable("time").AddAttribute("units","YYYYMMDDHHMM[SS]") ) then
		        ct.MsgLog(true,NC.GetVariable("time").returnMsg + " Application terminated.",True)
		        return false
		      end
		      if not( NC.GetVariable("time").AddAttribute("standard_name","time") ) then
		        ct.MsgLog(true,NC.GetVariable("time").returnMsg + " Application terminated.",True)
		        return false
		      end
		      if not( NC.GetVariable("time").AddAttribute("long_name","Date of  measurement") ) then
		        ct.MsgLog(true,NC.GetVariable("time").returnMsg + " Application terminated.",True)
		        return false
		      end
		      
		      
		    else
		      if not( NC.GetVariable("time").AddAttribute("axis","T") ) then
		        ct.MsgLog(true,NC.GetVariable("time").returnMsg + " Application terminated.",True)
		        return false
		      end
		      if not( NC.GetVariable("time").AddAttribute("units","days since 1900-01-01T00:00:00 UTC") ) then
		        ct.MsgLog(true,NC.GetVariable("time").returnMsg + " Application terminated.",True)
		        return false
		      end
		      if not( NC.GetVariable("time").AddAttribute("standard_name","time") ) then
		        ct.MsgLog(true,NC.GetVariable("time").returnMsg + " Application terminated.",True)
		        return false
		      end
		      if not( NC.GetVariable("time").AddAttribute("long_name","Julian day (UT) of each measurement") ) then
		        ct.MsgLog(true,NC.GetVariable("time").returnMsg + " Application terminated.",True)
		        return false
		      end
		      
		    end
		    
		  end if
		  
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SetAndLoadRadialCurrent() As Boolean
		  dim auxVar1(-1) as Variant
		  dim auxSgl31(-1,-1,-1), auxSgl32(-1,-1,-1), auxSgl33(-1,-1,-1), auxSgl34(-1,-1,-1) as single
		  dim auxInt3q(-1,-1,-1) as integer
		  dim auxBool3(-1,-1,-1) as Boolean
		  dim i, j, k as integer
		  
		  dim auxSglFillValue, auxSgl as Single
		  auxSglFillValue=CDbl("9.96921e+36")
		  
		  if timeseries then
		    
		    //Not yet implemented...
		    
		  else
		    
		    //Adding data of additionally required dimensions
		    if not(NC.AddDimension("station",1)) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not(NC.AddDimension("stringLength",11)) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      return false
		    end
		    
		    'adding the station name
		    
		    'station
		    Redim auxVar1(0)
		    auxVar1(0)=1
		    if not( NC.AddVariable( "station", Array(4), auxVar1  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      return false
		    end
		    'station_name
		    auxVar1(0)=DA.nameSt((dataType mod 10)-1)
		    if not( NC.AddVariable( "station_name", Array(4,5), auxVar1  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      return false
		    end
		    
		    'Attributes
		    if not( NC.GetVariable("station").AddAttribute("axis","S") ) then
		      ct.MsgLog(true,NC.GetVariable("station").returnMsg + " Application terminated.",True)
		      return false
		    end
		    if not( NC.GetVariable("station").AddAttribute("_FillValue","-2147483647") ) then
		      ct.MsgLog(true,NC.GetVariable("station").returnMsg + " Application terminated.",True)
		      return false
		    end
		    
		    
		    
		    
		    
		    //Loading and setting radial angle
		    ct.MsgLog(false,"Loading radial angles...",false)
		    dim radData(-1, -1) as single
		    if not( DA.LoadRadAngle(dataType mod 10, radData ) ) then
		      ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		      Return false
		    end if
		    ct.MsgLog(false,"Adding data to netcdf virtual object...",false)
		    Redim auxSgl31( measTimeDBs.Ubound, UBound(radData,2), UBound(radData,1) )
		    for k=0 to measTimeDBs.Ubound
		      for j=0 to UBound(radData,2)
		        for i=0 to UBound(radData,1)
		          auxSgl31(k,j,i)=radData(i,j)
		        next
		      next
		    next
		    if not( NC.AddVariable( "hcdt", Array(3,2,1), auxSgl31  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    
		    
		    ct.MsgLog(false,"Loading and adding station coordinates...",false)
		    'longitude_s
		    auxSgl=DA.lonSt((dataType mod 10)-1)
		    auxVar1(0)=auxSgl
		    if not( NC.AddVariable( "longitude_s", Array(4), auxVar1  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    'latitude_s
		    auxSgl=DA.latSt((dataType mod 10)-1)
		    auxVar1(0)=auxSgl
		    if not( NC.AddVariable( "latitude_s", Array(4), auxVar1  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    
		    
		    
		    //Loading measured data
		    ct.MsgLog(false,"Loading measured data (may take several seconds)...",false)
		    if not( DA.LoadTemporalSpatialData( id_headers, dataType, auxBool3, auxSgl31, auxSgl32, auxSgl33, auxSgl34, auxInt3q ) ) then
		      ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		      Return false
		    end if
		    ct.MsgLog(false,"Done!",true)
		    
		    ct.MsgLog(false,"Assign fill values where required...",false)
		    for k=0 to measTimeDBs.Ubound
		      for j=0 to UBound(radData,2)
		        for i=0 to UBound(radData,1)
		          if not(auxBool3(k,j,i)) then
		            auxSgl31(k,j,i)=auxSglFillValue
		            auxSgl32(k,j,i)=auxSglFillValue
		            auxSgl33(k,j,i)=auxSglFillValue
		            auxSgl34(k,j,i)=auxSglFillValue
		            auxInt3q(k,j,i)=-128
		          end if
		        next
		      next
		    next
		    ct.MsgLog(false,"Done!",true)
		    measTimeDBs=Nil
		    
		    ct.MsgLog(false,"Adding radial current velocity to netcdf virtual object...",false)
		    if not( NC.AddVariable( "hcsp", Array(3,2,1), auxSgl31  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxSgl31=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding radial current velocity accuracy to netcdf virtual object...",false)
		    if not( NC.AddVariable( "hcsp_error", Array(3,2,1), auxSgl32  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxSgl32=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding radial current velocity variance to netcdf virtual object...",false)
		    if not( NC.AddVariable( "variance", Array(3,2,1), auxSgl33  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxSgl33=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding power to netcdf virtual object...",false)
		    if not( NC.AddVariable( "power", Array(3,2,1), auxSgl34  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxSgl34=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding radial current velocity quality to netcdf virtual object...",false)
		    if not( NC.AddVariable( "qual", Array(3,2,1), auxInt3q  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxInt3q=Nil  //Releasing allocated memory (hopefully)
		    
		    
		    
		    
		    
		    ct.MsgLog(false,"Adding attributes to variables in netcdf virtual object...",false)
		    dim auxStrArr1(-1), auxStrArr2(-1) as string
		    auxStrArr1=Array("long_name","standard_name","units","_FillValue","valid_min","valid_max")
		    
		    auxStrArr2=Array("longitude_s","longitude_s","degree_east","9.96921e+36f","-180.f","180.f")
		    if not( NC.GetVariable("longitude_s").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("longitude_s").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("latitude_s","latitude_s","degree_north","9.96921e+36f","-90.f","90.f")
		    if not( NC.GetVariable("latitude_s").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("latitude_s").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Radial current direction","direction_of_sea_water_velocity","degree","9.96921e+36f","0.f","360.f")
		    if not( NC.GetVariable("hcdt").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("hcdt").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Radial current velocity","sea_water_speed","m.s-1","9.96921e+36f","-100.f","100.f")
		    if not( NC.GetVariable("hcsp").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("hcsp").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Accuracy of radial current velocity","error_on_sea_water_speed","m.s-1","9.96921e+36f","-100.f","100.f")
		    if not( NC.GetVariable("hcsp_error").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("hcsp_error").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Variance of measured radial current velocity","variance_on_sea_water_speed","m.s-1","9.96921e+36f","0.f","9.96921e+36f")
		    if not( NC.GetVariable("variance").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("variance").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Power associated to measured radial current velocity","power_on_sea_water_speed","dB","9.96921e+36f","0.f","9.96921e+36f")
		    if not( NC.GetVariable("power").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("power").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("WERA radial current quality","quality_of_radial_sea_water_velocity","NoUnits","-128","0","127")
		    if not( NC.GetVariable("qual").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("qual").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    ct.MsgLog(false,"Done!",true)
		    
		    
		    
		    
		  end if
		  
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SetAndLoadRadialWave() As Boolean
		  //Not yet implemented
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SetAndLoadVectorCurrent() As Boolean
		  dim auxVar21(-1,-1), auxVar22(-1,-1) as Variant
		  dim auxSgl31(-1,-1,-1), auxSgl32(-1,-1,-1), auxSgl33(-1,-1,-1), auxSgl34(-1,-1,-1) as single
		  dim auxInt3q(-1,-1,-1) as integer
		  dim auxBool3(-1,-1,-1) as Boolean
		  dim i, j, k as integer
		  
		  dim auxSglFillValue as Single
		  auxSglFillValue=CDbl("9.96921e+36")
		  
		  if timeseries then
		    
		    //Not yet implemented...
		    
		  else
		    
		    
		    
		    
		    //Loading and setting gdops
		    ct.MsgLog(false,"Loading GDOP...",false)
		    dim gdopxData(-1, -1) as single
		    dim gdopyData(-1, -1) as single
		    if not( DA.LoadGDOP(gdopxData, gdopyData ) ) then
		      ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		      Return false
		    end if
		    ct.MsgLog(false,"Adding data to netcdf virtual object...",false)
		    Redim auxVar21(UBound(gdopxData,2), UBound(gdopxData,1) )
		    Redim auxVar22(UBound(gdopxData,2), UBound(gdopxData,1) )
		    for j=0 to UBound(gdopxData,2)
		      for i=0 to UBound(gdopxData,1)
		        auxVar21(j,i)=gdopxData(i,j)
		        auxVar22(j,i)=gdopyData(i,j)
		      next
		    next
		    if not( NC.AddVariable( "gdopx", Array(2,1), auxVar21  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    if not( NC.AddVariable( "gdopy", Array(2,1), auxVar22  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    
		    
		    //Loading measured data
		    ct.MsgLog(false,"Loading measured data (may take several seconds)...",false)
		    if not( DA.LoadTemporalSpatialData( id_headers, dataType, auxBool3, auxSgl31, auxSgl32, auxSgl33, auxSgl34, auxInt3q ) ) then
		      ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		      Return false
		    end if
		    ct.MsgLog(false,"Done!",true)
		    
		    ct.MsgLog(false,"Assign fill values where required...",false)
		    for k=0 to measTimeDBs.Ubound
		      for j=0 to UBound(gdopxData,2)
		        for i=0 to UBound(gdopxData,1)
		          if not(auxBool3(k,j,i)) then
		            auxSgl31(k,j,i)=auxSglFillValue
		            auxSgl32(k,j,i)=auxSglFillValue
		            auxSgl33(k,j,i)=auxSglFillValue
		            auxSgl34(k,j,i)=auxSglFillValue
		            auxInt3q(k,j,i)=-128
		          end if
		        next
		      next
		    next
		    ct.MsgLog(false,"Done!",true)
		    
		    ct.MsgLog(false,"Adding U component of current velocity to netcdf virtual object...",false)
		    if not( NC.AddVariable( "ewct", Array(3,2,1), auxSgl31  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxSgl31=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding V component of current velocity to netcdf virtual object...",false)
		    if not( NC.AddVariable( "nsct", Array(3,2,1), auxSgl32  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxSgl32=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding accuracy of U component of current velocity to netcdf virtual object...",false)
		    if not( NC.AddVariable( "ewct_error", Array(3,2,1), auxSgl33  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxSgl33=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding accuracy of V component of current velocity to netcdf virtual object...",false)
		    if not( NC.AddVariable( "nsct_error", Array(3,2,1), auxSgl34  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxSgl34=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding quality values to netcdf virtual object...",false)
		    if not( NC.AddVariable( "kl_qual", Array(3,2,1), auxInt3q  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxInt3q=Nil  //Releasing allocated memory (hopefully)
		    
		    
		    
		    
		    ct.MsgLog(false,"Adding attributes to variables in netcdf virtual object...",false)
		    dim auxStrArr1(-1), auxStrArr2(-1) as string
		    auxStrArr1=Array("long_name","standard_name","units","_FillValue","valid_min","valid_max")
		    
		    auxStrArr2=Array("U component of current velocity","eastward_sea_water_velocity","m.s-1","9.96921e+36f","-100.f","100.f")
		    if not( NC.GetVariable("ewct").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("ewct").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("V component of current velocity","northward_sea_water_velocity","m.s-1","9.96921e+36f","-100.f","100.f")
		    if not( NC.GetVariable("nsct").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("nsct").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Accuracy of U component of current velocity","error_on_eastward_sea_water_velocity","m.s-1","9.96921e+36f","-100.f","100.f")
		    if not( NC.GetVariable("ewct_error").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("ewct_error").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Accuracy of V component of current velocity","error_on_northward_sea_water_velocity","m.s-1","9.96921e+36f","-100.f","100.f")
		    if not( NC.GetVariable("nsct_error").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("nsct_error").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("WERA current vector quality","quality_of_sea_water_velocity","NoUnits","-128","0","127")
		    if not( NC.GetVariable("kl_qual").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("kl_qual").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Longitudinal dilution of precision","gdopx","NoUnits","9.96921e+36f","0.f","9.96921e+36f")
		    if not( NC.GetVariable("gdopx").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("gdopx").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Latitudinal dilution of precision","gdopy","NoUnits","9.96921e+36f","0.f","9.96921e+36f")
		    if not( NC.GetVariable("gdopy").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("gdopy").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    ct.MsgLog(false,"Done!",true)
		    
		    
		    
		    
		  end if
		  
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SetAndLoadWave() As Boolean
		  dim auxVar21(-1,-1), auxVar22(-1,-1) as Variant
		  dim auxSgl31(-1,-1,-1), auxSgl32(-1,-1,-1) as single
		  dim auxInt3q(-1,-1,-1) as integer
		  dim auxBool3(-1,-1,-1) as Boolean
		  dim i, j, k as integer
		  
		  
		  if timeseries then
		    
		    //Not yet implemented...
		    
		  else
		    
		    
		    //Loading and setting gdops
		    ct.MsgLog(false,"Loading GDOP...",false)
		    dim gdopxData(-1, -1) as single
		    dim gdopyData(-1, -1) as single
		    if not( DA.LoadGDOP(gdopxData, gdopyData ) ) then
		      ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		      Return false
		    end if
		    ct.MsgLog(false,"Adding data to netcdf virtual object...",false)
		    Redim auxVar21(UBound(gdopxData,2), UBound(gdopxData,1) )
		    Redim auxVar22(UBound(gdopxData,2), UBound(gdopxData,1) )
		    for j=0 to UBound(gdopxData,2)
		      for i=0 to UBound(gdopxData,1)
		        auxVar21(j,i)=gdopxData(i,j)
		        auxVar22(j,i)=gdopyData(i,j)
		      next
		    next
		    if not( NC.AddVariable( "gdopx", Array(2,1), auxVar21  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    if not( NC.AddVariable( "gdopy", Array(2,1), auxVar22  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    
		    
		    //Loading measured data
		    ct.MsgLog(false,"Loading measured data (may take several seconds)...",false)
		    if not( DA.LoadTemporalSpatialData( id_headers, dataType, auxBool3, auxSgl31, auxSgl32, auxInt3q ) ) then
		      ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		      Return false
		    end if
		    ct.MsgLog(false,"Done!",true)
		    
		    ct.MsgLog(false,"Assign fill values where required...",false)
		    for k=0 to measTimeDBs.Ubound
		      for j=0 to UBound(gdopxData,2)
		        for i=0 to UBound(gdopxData,1)
		          if not(auxBool3(k,j,i)) then
		            auxSgl31(k,j,i)=-999
		            auxSgl32(k,j,i)=-999
		            auxInt3q(k,j,i)=-128
		          end if
		        next
		      next
		    next
		    ct.MsgLog(false,"Done!",true)
		    measTimeDBs=Nil
		    
		    ct.MsgLog(false,"Adding significant wave height to netcdf virtual object...",false)
		    if not( NC.AddVariable( "Hs", Array(3,2,1), auxSgl31  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxSgl31=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding mean wave direction to netcdf virtual object...",false)
		    if not( NC.AddVariable( "Wdir", Array(3,2,1), auxSgl32  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxSgl32=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding quality values to netcdf virtual object...",false)
		    if not( NC.AddVariable( "qual", Array(3,2,1), auxInt3q  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxInt3q=Nil  //Releasing allocated memory (hopefully)
		    
		    
		    
		    
		    
		    ct.MsgLog(false,"Adding attributes to variables in netcdf virtual object...",false)
		    dim auxStrArr1(-1), auxStrArr2(-1) as string
		    auxStrArr1=Array("long_name","standard_name","units","_FillValue","valid_min","valid_max")
		    
		    auxStrArr2=Array("Significant wave height","sea_surface_wave_significant_height","meter","-999.f","0.f","100.f")
		    if not( NC.GetVariable("Hs").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("Hs").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Mean wave direction","sea_surface_wave_to_direction","degrees","-999.f","0.f","360.f")
		    if not( NC.GetVariable("Wdir").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("Wdir").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("WERA quality number","quality_of_wave_parameters","NoUnits","-128","0","127")
		    if not( NC.GetVariable("qual").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("qual").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    
		    auxStrArr2=Array("Longitudinal dilution of precision","gdopx","NoUnits","9.96921e+36f","0.f","9.96921e+36f")
		    if not( NC.GetVariable("gdopx").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("gdopx").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Latitudinal dilution of precision","gdopy","NoUnits","9.96921e+36f","0.f","9.96921e+36f")
		    if not( NC.GetVariable("gdopy").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("gdopy").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    ct.MsgLog(false,"Done!",true)
		    
		    
		  end if
		  
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SetAndLoadWind() As Boolean
		  dim auxVar21(-1,-1), auxVar22(-1,-1) as Variant
		  dim auxSgl31(-1,-1,-1), auxSgl32(-1,-1,-1) as single
		  dim auxInt3q(-1,-1,-1) as integer
		  dim auxBool3(-1,-1,-1) as Boolean
		  dim i, j, k as integer
		  
		  
		  if timeseries then
		    
		    //Not yet implemented...
		    
		  else
		    
		    
		    //Loading and setting gdops
		    ct.MsgLog(false,"Loading GDOP...",false)
		    dim gdopxData(-1, -1) as single
		    dim gdopyData(-1, -1) as single
		    if not( DA.LoadGDOP(gdopxData, gdopyData ) ) then
		      ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		      Return false
		    end if
		    ct.MsgLog(false,"Adding data to netcdf virtual object...",false)
		    Redim auxVar21(UBound(gdopxData,2), UBound(gdopxData,1) )
		    Redim auxVar22(UBound(gdopxData,2), UBound(gdopxData,1) )
		    for j=0 to UBound(gdopxData,2)
		      for i=0 to UBound(gdopxData,1)
		        auxVar21(j,i)=gdopxData(i,j)
		        auxVar22(j,i)=gdopyData(i,j)
		      next
		    next
		    if not( NC.AddVariable( "gdopx", Array(2,1), auxVar21  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    if not( NC.AddVariable( "gdopy", Array(2,1), auxVar22  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    
		    
		    //Loading measured data
		    ct.MsgLog(false,"Loading measured data (may take several seconds)...",false)
		    if not( DA.LoadTemporalSpatialData( id_headers, dataType, auxBool3, auxSgl31, auxSgl32, auxInt3q ) ) then
		      ct.MsgLog(true,DA.lastMessage + " Application terminated.",True)
		      Return false
		    end if
		    ct.MsgLog(false,"Done!",true)
		    
		    ct.MsgLog(false,"Assign fill values where required...",false)
		    for k=0 to measTimeDBs.Ubound
		      for j=0 to UBound(gdopxData,2)
		        for i=0 to UBound(gdopxData,1)
		          if not(auxBool3(k,j,i)) then
		            auxSgl31(k,j,i)=-999
		            auxSgl32(k,j,i)=-999
		            auxInt3q(k,j,i)=-128
		          end if
		        next
		      next
		    next
		    ct.MsgLog(false,"Done!",true)
		    measTimeDBs=Nil
		    
		    ct.MsgLog(false,"Adding wind direction to netcdf virtual object...",false)
		    if not( NC.AddVariable( "Udir", Array(3,2,1), auxSgl31  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxSgl31=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding wind speed to netcdf virtual object...",false)
		    if not( NC.AddVariable( "U10", Array(3,2,1), auxSgl32  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxSgl32=Nil  //Releasing allocated memory (hopefully)
		    
		    ct.MsgLog(false,"Adding quality values to netcdf virtual object...",false)
		    if not( NC.AddVariable( "qual", Array(3,2,1), auxInt3q  ) ) then
		      ct.MsgLog(true,NC.returnMsg + " Application terminated.",True)
		      Return false
		    end
		    ct.MsgLog(false,"Done!",true)
		    auxInt3q=Nil  //Releasing allocated memory (hopefully)
		    
		    
		    
		    
		    ct.MsgLog(false,"Adding attributes to variables in netcdf virtual object...",false)
		    dim auxStrArr1(-1), auxStrArr2(-1) as string
		    auxStrArr1=Array("long_name","standard_name","units","_FillValue","valid_min","valid_max")
		    
		    auxStrArr2=Array("Wind direction","wind_to_direction","degrees","-999.f","0.f","360.f")
		    if not( NC.GetVariable("Udir").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("Udir").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Wind speed","wind_speed","m.s-1","-999.f","0.f","100.f")
		    if not( NC.GetVariable("U10").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("U10").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    //auxStrArr1=Array("long_name","standard_name","_FillValue")
		    auxStrArr2=Array("WERA quality number","quality_of_wind_parameters","NoUnits","-128","0","127")
		    if not( NC.GetVariable("qual").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("qual").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    //auxStrArr1=Array("long_name","standard_name","_FillValue","valid_min")
		    
		    auxStrArr2=Array("Longitudinal dilution of precision","gdopx","NoUnits","9.96921e+36f","0.f","9.96921e+36f")
		    if not( NC.GetVariable("gdopx").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("gdopx").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    auxStrArr2=Array("Latitudinal dilution of precision","gdopy","NoUnits","9.96921e+36f","0.f","9.96921e+36f")
		    if not( NC.GetVariable("gdopy").AddAttributes(auxStrArr1,auxStrArr2) ) then
		      ct.MsgLog(true,NC.GetVariable("gdopy").returnMsg + " Application terminated.",True)
		      Return false
		    end
		    
		    ct.MsgLog(false,"Done!",true)
		    
		    
		  end if
		  
		  
		  return true
		End Function
	#tag EndMethod


	#tag Note, Name = Version_Notes
		HOW TO FILL THIS NOTES...
		
		=Filename= (Notes... if applies. e.g. Version compiled for MELCO)
		A date to have a time reference (yyyy.mm.dd), 
		App version (Built using App.MajorVersion and App.NonReleaseVersion. CHECK THE VALUE OF App.NonRealeaseVersion AFTER COMPILING... COMPILING INCREASES THE VALUE AUTOMATICALLY!), 
		Person who edited and compiled this new version. 
		Real Studio version when compiled (check it on Hilfe>Über RealStudio) / Operative system (compiled with linux or windows)
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		IMPORTANT!!! Insert notes of new version in the next line, not at the end!!! last version info should ALWAYS be displayed first!!!
		
		//Here a comment to test parallel modifications...
		
		=ExportWERA2NetCDF_V3_160301.xojo_binary_project=
		2016.03.01
		Marek Swirgon
		Version 4.0 BETA (do not think this number is correct!)
		Xojo 2015 R4 / Windows 7
		-Add automatic Mode for Radial Processor "A"
		- Add optional param -c configDBfile
		
		=ExportWERA2NetCDF_V2_151207.xojo_binary_project=
		2015.12.07
		Marek Swirgon
		Version 2.18 BETA
		Xojo 2015 R3.1 / Windows 7
		-Add option specialFormat =2 for file format yyymmddhhmmss...
		
		=ExportWERA2NetCDF_V2_150701.xojo_binary_project=
		2015.07.01
		Marek Swirgon
		Version 2.6 BETA
		Xojo 2015 R1 / Windows 7
		-Change Licence Validation (for CPU names without available speed, and reading numbers of processors from /proc/cpuinfo)
		
		=ExportWERA2NetCDF_V2_150211.xojo_binary_project=
		2015.02.11
		Roberto Gomez
		Version 2.5 BETA
		Xojo 2014 3.2 / Windows7
		-Now adding seconds to the output file when -t input is given in Gurgel's format and including seconds (13 chars long).
		
		=ExportWERA2NetCDF_V2_150206.xojo_binary_project=
		2015.02.06
		Roberto Gomez
		Version 2.4 BETA
		Xojo 2014 3.2 / Windows7
		-Improved handling of error messages when using method netcdfFile.variables(n).AddAttribute(s)... now logging returnMsg from netcdfVariable, not netcdfFile (which is empty).
		-The netcdffile (NC object) is declared to NIL
		-Corrected error when adding attributes on data variables when exporting wind files. 
		
		=ExportWERA2NetCDF_V2_150205.xojo_binary_project=
		2015.02.05
		Roberto Gomez
		Version 2.3 BETA
		Xojo 2014 3.2 / Windows7
		-Compiled with Xojo
		-Now correctly exporting U and V accuracy instead of repeating U and V current values.
		-Setting local auxiliar arrays to Nil (e.g. auxSgl31, auxSgl32, auxInt3q, auxVar41 and so on) after they are not needed anymore in SetAndLoad methods for current, wave and wind.
		  This is to free memory and avoid crash of software when grid is very big. 
		-Also setting DA and NC.variables to Nil after they are not required anymore... to release additional memory.
		
		=ExportWERA2NetCDF_V2_141205.rbp=
		2014.12.05
		Roberto Gomez
		Version 2.2 BETA
		RS 2012 2.1 / Windows7
		-Activating MBS plugin serial before checking if WERA licence is valid, to avoid warning on MBS licence.
		
		=ExportWERA2NetCDF_V2_141117.rbp=
		2014.11.17
		Roberto Gomez
		Version 2.1 BETA
		RS 2012 2.1 / Windows7
		-Increased major version to 2 (mainly due to licence check implementation... should have done that before).
		
		=ExportWERA2NetCDF_V1_141107.rbp=
		2014.11.07
		Version 1.4 BETA
		Roberto Gomez
		RS 2012 2.1 / Windows
		-Implemented licence check
		
		=ExportWERA2NetCDF_V1_140822.rbp=
		2014.08.22
		Roberto Gomez
		Not yet compiled...
		RS 2012 2.1 / Windows 7
		-Added variable attributes "units", "valid_min" and "valid_max" on variables who did not had it before. This is a patch to avoid an error with ARRET software. However, some variables do not really have units or valid ranges so Actimar must modify this for future versions.
		
		=ExportWERA2NetCDF_V1_140813.rbp=
		2014.08.13
		Roberto Gomez
		Version 1.3
		RS 2012 2.1 / Windows 7
		-Changed "ten" dimension for "stringLength" dimension.
		-Possibility to also include radial data when exporting current vector data using f=1 as input. For this purpose, the method 'IncludeRadialDataToo' was added.
		
		=ExportWERA2NetCDF_V1_140811.rbp=
		2014.08.11
		Roberto Gomez
		Version 1.2
		RS 2012 2.1 / Windows 7
		-Removed space at the beginning of App.kAppName.
		-Included possibility for exporting 2D wave and wind data.
		
		=ExportWERA2NetCDF_V1_140808.rbp=
		2014.08.08
		Roberto Gomez
		Version 1.1
		RS 2012 2.1 / Windows 7
		-First version (only for radial currents and combined currents, with actimar format plus accuracies, qualities and gdops for combined data)
		
		
		IMPORTANT!!! Insert notes of new version at the beginning, not at the end!!! last version info should ALWAYS be displayed first!!!
	#tag EndNote


	#tag Property, Flags = &h1
		Protected DA As WERA_DB_DataLoader
	#tag EndProperty

	#tag Property, Flags = &h0
		dataType As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		DBconfigFile As string
	#tag EndProperty

	#tag Property, Flags = &h0
		execDir As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected id_headers(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected id_ixiy As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected id_rad_headers(-1,-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ix As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		iy As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		kLogFile As string = "ExportWERA2NetCDF"
	#tag EndProperty

	#tag Property, Flags = &h0
		measTimeDBs(-1) As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		Mode As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected NC As netcdfFile
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected nx As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ny As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		outputFile As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		qualFilt As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		specialFormat As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		timePoint As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		timePointEnd As Date
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected timeseries As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		Untitled As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		withSeconds As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		xStr As String
	#tag EndProperty

	#tag Property, Flags = &h0
		yStr As String
	#tag EndProperty


	#tag Constant, Name = kAppName, Type = String, Dynamic = False, Default = \"ExportWERA2NetCDF", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kDescription, Type = String, Dynamic = False, Default = \"It is used to export a single map of data. It loads from the database the data type indicated by <dataType> in the point in time specified in <timeInstantLabel>. The data is then exported into a NetCDF file. *_* The meaning of <dataType> is: *_*   0       Vector current velocity data  (UV components). *_*   n       Radial current velocity from station n. *_*   10      Combined significant wave height and direction. *_*   10+n    Empirical estimate of significant wave height from station n. *_*   20      Wind directions (and speed if available). *_* The netcdf file is created with the filename or in the folder indicated by <outputFile> *_* Optionally\x2C it is possible to filter the data using <qualityFilter>. The valid values for this input are: *_*   0       No filter (default value). Include quality as extra variable. *_*   1       Only best quality. *_*   2       Include quality levels 1 and 2. *_*   3       Include quality levels 1\x2C 2 and 3 (artefacts removed). *_*   4       Include quality levels 1\x2C 2\x2C 3 and 4. *_* Optionally\x2C if there are more than 1 grid defined in the database\x2C it is required to indicate which grid to use in <gridToUse>. If not indicated\x2C it will use the grid available in the time given;;(NOT YET IMPLEMENTED) It is used to export a time-series of data. The starting and end times of the time-series are given in <timeStart> and <timeEnd> respectively. The point from which the time-series should be created are indicated via the <longitudinalReference> and <latitudinalReference> inputs. These may be given as grid coordinate using an integer value\x2C or as longitude and latitude coordinates using a floating number that includes a decimal point.;;It is used to export the whole data in between a range of time. Execution may take several minutes.", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kInputTypes, Type = String, Dynamic = False, Default = \"t;T;d;x;y;q;f;g;o;c", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kNotes, Type = String, Dynamic = False, Default = \"<TimeLabel> is specified as Year-DayOfYear-Hour-Minutes and Optionally Seconds (yyyydddhhmm[ss]). It can also be expressed in SQL datetime format\x2C however it would need to be quoted (-t\x3D\"yyyy-MM-dd hh-mm-ss\").", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kSyntax, Type = String, Dynamic = False, Default = \"-t\x3D<timeInstantLabel> -d\x3D<dataType> -o\x3D<outputFile> [-q\x3D<qualityFilter>] [-f\x3D<specialFormatFlag>] [-g\x3D<gridToUse>];;-t\x3D<timeStart> -T\x3D<timeEnd> -d\x3D<dataType> -x\x3D<longitudinalReference> -y\x3D<latitudinalReference> -o\x3D<outputFile> [-q\x3D<qualityFilter>] [-f\x3D<specialFormatFlag>] [-g\x3D<gridToUse>];;-t\x3D<timeStart> -T\x3D<timeEnd> -d\x3D<dataType> -o\x3D<outputFile> [-q\x3D<qualityFilter>] [-f\x3D<specialFormatFlag>] [-g\x3D<gridToUse>]", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kUse, Type = String, Dynamic = False, Default = \"This console application is used to create a NetCDF file out of data from the WERA Data-Archive. All the type of data stored in the WERA Data-Archive may be exported into a NetCDF file with different formats/conventions. Filtering different quality control levels is also possible.", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="dataType"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DBconfigFile"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="execDir"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ix"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="iy"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="kLogFile"
			Group="Behavior"
			InitialValue="ExportWERA2NetCDF"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Mode"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="qualFilt"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="specialFormat"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Untitled"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="withSeconds"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="xStr"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="yStr"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
