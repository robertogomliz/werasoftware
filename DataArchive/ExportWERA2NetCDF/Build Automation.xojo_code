#tag BuildAutomation
			Begin BuildStepList Linux
				Begin BuildProjectStep Build
				End
				Begin IDEScriptBuildStep SaveProjectAfterCompile , AppliesTo = 2
					//Executing this internal IDE xojo script will save the project after being build.
					//Note that this will be executed only when built for a release, and not when built for debugging mode.
					//And advantage of using this script is that the project will be automatically saved after compiling, making sure that the version numbering stays updated.
					
					DoCommand("SaveFile")
				End
			End
			Begin BuildStepList Mac OS X
				Begin BuildProjectStep Build
				End
			End
			Begin BuildStepList Windows
				Begin BuildProjectStep Build
				End
			End
#tag EndBuildAutomation
