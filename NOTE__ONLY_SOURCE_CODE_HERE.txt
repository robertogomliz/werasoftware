Note for xojo developers in HZM:
RG 16.08.04

The idea of this folder is to contain only the latest source code objects organized into subfolders and different software packages.

The motivation behind is to facilitate syncronization of the latest software state when several engineers are working on the same code from different places (e.g. Roberto from Mexico).

That is, it should be possible to copy these folders somewhere and backup the latest version of source code, instead of individually copying the latest files and required modules/classes and re-organizing them again into their original structure in another local computer (outside HZM network) for them to work properly and to be able to modify the code and test it.

Please note that:

-Only Xojo Text Format should be used (DO NOT save your project as *.binary_xojo_project but as *.xojo_project!). It makes much easier to compare and see differences between two files which are suspected to be different.
-All required objects like images or additional items (external classes or modules) inside the xojo projects should also be contained within these folders (when the project is opened in Xojo, it should not ask for a missing object which was located somewhere outside these folders).
-No old versions of code should be maintained within these folders, only latest one. The subfolder which contains each of the xojo projects should include a direct access (or link) to the place where the old versions of code are stored.
-No builds (executable binaries and corresponding libraries) should be maintained within these folders. The subfolder which contains each of the xojo projects should include a direct access (or link) to the place where the builds are stored.


