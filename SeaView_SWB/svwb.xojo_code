#tag Module
Protected Module svwb
	#tag Method, Flags = &h0
		Function AssignParameterValue(lineOfText as string) As Boolean
		  // Assigns the value of a parameter from a line of text from the configuration file
		  
		  Dim auxStringArray(-1), auxString as String
		  Dim i as Integer
		  
		  auxStringArray=split(lineOfText,"=")
		  
		  if Ubound(auxStringArray)<=0 then
		    return false
		  else
		    auxString=Trim(auxStringArray(0)) 'Isolate variable name
		    cfgVarsName_svwb.Append(auxString) 'Add it to the list
		    
		    auxStringArray=split(auxStringArray(1),"//")  // Remove comment
		    auxStringArray=split(Trim(auxStringArray(0)),Chr(9))  // Isolate the parameter value by removing the tabs after the value. For the case of tab separated multiple values, this variable contains all values.
		    auxString=Trim(auxStringArray(0))  // The variable value should be in the first cell... we only remove spaces before and after
		    
		    Select Case cfgVarsName_svwb(cfgVarsName_svwb.Ubound)
		    case "gridUsed"
		      gridUsed=CLong(auxString)
		      
		    case "nPts"
		      nPts=CLong(auxString)
		      Redim ptName_pt(nPts-1)
		      Redim lat_pt(nPts-1)
		      Redim lon_pt(nPts-1)
		      Redim nRings_pt(nPts-1)
		      Redim wavAvail_pt(nPts-1)
		      Redim specAvail_pt(nPts-1)
		      Redim fullDirSpecAvail_pt(nPts-1)
		      Redim winAvail_pt(nPts-1)
		      Redim specPrefix_pt(nPts-1)
		      Redim sdtFile_pt(nPts-1)
		      Redim secondConf_pt(nPts-1)
		      
		    case "ptName_pt"
		      for i=0 to nPts-1
		        if auxStringArray.Ubound<i then
		          ptName_pt(i)="location"+CStr(i+1)
		        else
		          ptName_pt(i)=auxStringArray(i)
		        end
		      next
		      
		    case "lat_pt"
		      for i=0 to nPts-1
		        if auxStringArray.Ubound<i then
		          lat_pt(i)=0
		        else
		          lat_pt(i)=CDbl(auxStringArray(i))
		        end
		      next
		      
		    case "lon_pt"
		      for i=0 to nPts-1
		        if auxStringArray.Ubound<i then
		          lon_pt(i)=0
		        else
		          lon_pt(i)=CDbl(auxStringArray(i))
		        end
		      next
		      
		    case "nRings_pt"
		      for i=0 to nPts-1
		        if auxStringArray.Ubound<i then
		          nRings_pt(i)=0
		        else
		          nRings_pt(i)=CLong(auxStringArray(i))
		        end
		      next
		      
		    case "wavAvail_pt"
		      for i=0 to nPts-1
		        if auxStringArray.Ubound<i then
		          wavAvail_pt(i)=false
		        else
		          if auxStringArray(i)="true" or auxStringArray(i)="True" or auxStringArray(i)="TRUE" or auxStringArray(i)=".TRUE" or auxStringArray(i)=".TRUE." or auxStringArray(i)="1" then
		            wavAvail_pt(i)=true
		          else
		            wavAvail_pt(i)=false
		          end
		        end
		      next
		      
		    case "specAvail_pt"
		      for i=0 to nPts-1
		        if auxStringArray.Ubound<i then
		          specAvail_pt(i)=false
		        else
		          if auxStringArray(i)="true" or auxStringArray(i)="True" or auxStringArray(i)="TRUE" or auxStringArray(i)=".TRUE" or auxStringArray(i)=".TRUE." or auxStringArray(i)="1" then
		            specAvail_pt(i)=true
		          else
		            specAvail_pt(i)=false
		          end
		        end
		      next
		      
		    case "fullDirSpecAvail_pt"
		      for i=0 to nPts-1
		        if auxStringArray.Ubound<i then
		          fullDirSpecAvail_pt(i)=false
		        else
		          if auxStringArray(i)="true" or auxStringArray(i)="True" or auxStringArray(i)="TRUE" or auxStringArray(i)=".TRUE" or auxStringArray(i)=".TRUE." or auxStringArray(i)="1" then
		            fullDirSpecAvail_pt(i)=true
		          else
		            fullDirSpecAvail_pt(i)=false
		          end
		        end
		      next
		      
		    case "winAvail_pt"
		      for i=0 to nPts-1
		        if auxStringArray.Ubound<i then
		          winAvail_pt(i)=false
		        else
		          if auxStringArray(i)="true" or auxStringArray(i)="True" or auxStringArray(i)="TRUE" or auxStringArray(i)=".TRUE" or auxStringArray(i)=".TRUE." or auxStringArray(i)="1" then
		            winAvail_pt(i)=true
		          else
		            winAvail_pt(i)=false
		          end
		        end
		      next
		      
		    case "specPrefix_pt"
		      for i=0 to nPts-1
		        if auxStringArray.Ubound<i then
		          specPrefix_pt(i)="SV"+CStr(i+1)
		        else
		          specPrefix_pt(i)=auxStringArray(i)
		        end
		      next
		      
		    case "sdtFile_pt"
		      for i=0 to nPts-1
		        if auxStringArray.Ubound<i then
		          sdtFile_pt(i)="SV"+CStr(i+1) + ".sdt"
		        else
		          sdtFile_pt(i)=auxStringArray(i)
		        end
		      next
		      
		    case "secondConf_pt"
		      for i=0 to nPts-1
		        if auxStringArray.Ubound<i then
		          secondConf_pt(i)="none"
		        else
		          secondConf_pt(i)=auxStringArray(i)
		        end
		      next
		      
		    case "maxWiSpd"
		      maxWiSpd=CDbl(auxString)
		      
		    case "forceCombinedEmpiricalHs"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        forceCombinedEmpiricalHs=true
		      else
		        forceCombinedEmpiricalHs=false
		      end
		      
		    case "forceInvertedHs"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        forceInvertedHs=true
		      else
		        forceInvertedHs=false
		      end
		      
		    case "winAscCode"
		      winAscCode=auxString
		      
		    case "winAscPath"
		      winAscPath=auxString
		      
		    case "winAscParams"
		      winAscParams=auxString
		      
		    case "winAscSeconds"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        winAscSeconds=true
		      else
		        winAscSeconds=false
		      end
		      
		    case "winAscOrganized"
		      if auxString="true" or auxString="True" or auxString="TRUE" or auxString=".TRUE" or auxString=".TRUE." or auxString="1" then
		        winAscOrganized=true
		      else
		        winAscOrganized=false
		      end
		      
		    else
		      //Warning log... parameter not recognized!
		      log.Write(false,"WARNING... parameter in SeaView's wave-buoy configuration file not recognized > " + cfgVarsName_svwb(cfgVarsName_svwb.Ubound))
		      cfgVarsName_svwb.Remove(cfgVarsName_svwb.Ubound)
		      return false
		    end
		    return true
		  end
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DecodeQualVal(ByRef qual as Integer, ByRef wQual as Boolean, ByRef invData as Boolean , ByRef dirQual as Boolean) As integer
		  
		  
		  'Wind speed quality encoded as a value of 40. That is, if QualVal>=40, then wind speed has a good quality
		  if qual>=40 then
		    wQual=true
		    qual=qual-40
		  else
		    wQual=false
		  end
		  
		  'Data from inversion exists (direction and period values). Value encoded as 20. That is, if QualVal>=40 then QualVal=QualVal-40 (removed wind quality flag), if now QualVal>=20 then direction and period parameters exist.
		  if qual>=20 then
		    invData=true
		    qual=qual-20
		  else
		    invData=false
		  end
		  
		  'Direction and period quality encoded as a value of 10. That is, if QualVal>=40 then QualVal=QualVal-40 (removed wind quality flag), if now QualVal>=20 then QualVal=QualVal-20 (removed existence of period/direction data flag), if now QualVal>=10 then direction and period parameters have a good quality.
		  if qual>=10 then
		    dirQual=true
		    qual=qual-10
		  else
		    dirQual=false
		  end
		  
		  
		  'At the end, the quality levels of Hs are stored as follows:
		  '1=Hs from inverted data
		  '2=Hs from combined empirical values
		  '3=Hs from empirical value of the only site which passed the quality test
		  '4=Nothing passed the quality test, but anyway Hs from combined empirical values is stored
		  return (qual)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function EncodeQualVal(qual as Integer, wQual as Boolean, invData as Boolean, dirQual as Boolean) As integer
		  
		  // Encodes all the quality values of a .svdat file into a single integer, to save storage size on database.
		  
		  dim result as integer=0
		  
		  if wQual then result=result+40   'Wind speed quality encoded as a value of 40. That is, if QualVal>=40, then wind speed has a good quality
		  if invData then result=result + 20  'Data from inversion exists (direction and period values). Value encoded as 20. That is, if QualVal>=40 then QualVal=QualVal-40 (removed wind quality flag), if now QualVal>=20 then direction and period parameters exist.
		  if dirQual then result=result + 10  'Direction and period quality encoded as a value of 10. That is, if QualVal>=40 then QualVal=QualVal-40 (removed wind quality flag), if now QualVal>=20 then QualVal=QualVal-20 (removed existence of period/direction data flag), if now QualVal>=10 then direction and period parameters have a good quality.
		  
		  
		  return (result + qual) 
		  'At the end, the quality levels of Hs are stored as follows: 
		  '1=Hs from inverted data
		  '2=Hs from combined empirical values
		  '3=Hs from empirical value of the only site which passed the quality test
		  '4=Nothing passed the quality test, but anyway Hs from combined empirical values is stored
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function InitializeConfig(path as string) As boolean
		  // Initializes all the parameters of the Synthetic Wave Buoy from SeaView Sensing.
		  
		  
		  // Opening config.cfg file =======================================================
		  Dim f As New FolderItem
		  
		  if path.Len>3 then
		    f=GetFolderItem(path)
		  else //empty string
		    f = GetFolderItem(App.execDir + "config.cfg")
		  end
		  
		  
		  Dim configFile As TextInputStream
		  
		  If f <> Nil Then
		    If f.Exists Then
		      // Be aware that TextInputStream.Open coud raise an exception
		      Try
		        configFile = TextInputStream.Open(f)
		        configFile.Encoding = Encodings.ASCII
		      Catch e As IOException
		        configFile.Close
		        log.Write(true,"Error accessing wave buoy configuration file.")
		        return false
		      End Try
		    Else
		      log.Write(true,"Wave buoy configuration file does not exists.")
		      return false
		    End If
		  Else
		    log.Write(true,"Wave buoy configuration file is 'Nil'. Make sure folder exists.")
		    return false
		  End If
		  
		  
		  // Reading config.cfg file =======================================================
		  
		  Dim auxString As String
		  Dim cnt, i as Integer
		  
		  linesOfTxtFile_svwb=Split(configFile.ReadAll,EndOfLine)  // Split the text content into lines
		  
		  cnt=0
		  ReDim lineIdxOfVars_svwb(-1)
		  ReDim cfgVarsName_svwb(-1)
		  for i=0 to linesOfTxtFile_svwb.Ubound
		    
		    auxString=left(Trim(linesOfTxtFile_svwb(i)),1)  // Getting the first written character to see if it is only a comment or a blank line
		    
		    if auxString="/" or auxString="!" or auxString="%" or auxString="" then
		      continue
		    else
		      if svwb.AssignParameterValue(linesOfTxtFile_svwb(i)) then
		        lineIdxOfVars_svwb.Append(i)
		        cnt=cnt+1
		      else
		        // If Assign... did not suceed then it is already logged to file as a warning
		      end
		    end
		  next
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ReadSpectrum(path as string) As Boolean
		  
		  Dim t As TextInputStream
		  Dim f as FolderItem
		  Dim anArray (3) as String
		  
		  f=GetFolderItem(path)
		  If f <> Nil then
		    if f.Exists then
		      t=TextInputStream.Open(GetFolderItem(path))
		      t.Encoding=Encodings.ASCII //specify encoding of input stream
		      
		      //Lines that are not required for spectrum data
		      anArray(0)=t.readline 'Header
		      anArray(0)=t.readline 'Wind data
		      anArray(0)=t.readline 'empirical Hs from each station
		      anArray(0)=t.readline 'combined empirical Hs
		      if t.EOF then
		        log.Write(true, "Wave buoy data file does not contain spectrum data " + path)
		        return false
		      else
		        anArray(0)=t.readline 'wave parameters from inversion
		      end if
		      
		      Redim freqs(-1)
		      Redim energy(-1)
		      Redim dir(-1)
		      
		      while not(t.EOF) 'Number of spectrum lines is not always the same!
		        anArray = t.readline.split("   ")
		        if anArray.Ubound=2 then
		          freqs.Append(CDbl(anArray(0)))
		          energy.Append(CDbl(anArray(1)))
		          dir.Append(CDbl(anArray(2)))
		          if dir(dir.Ubound)<0 then dir(dir.Ubound)=dir(dir.Ubound)+360
		        end
		      wend
		      t.Close
		      return true
		      
		    else
		      log.Write(true, "Wave buoy data file does not exists " + path)
		      return false
		    end
		  else
		    log.Write(true, "Wave buoy data file returns nil " + path)
		    return false
		  End if
		End Function
	#tag EndMethod


	#tag Note, Name = Updates
		
		
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new updates in the next line, not at the end!!! latest update info should ALWAYS be displayed first!!!
		
		2016.03.21
		Roberto Gomez
		-Added property winAscPath to include the path where the win_asc files should be created while redoing the maps in SVWaBuWind2WERA program.
		-Added property winAscSeconds to indicate if filename should include seconds while redoing the maps in SVWaBuWind2WERA program.
		-Added property winAscOrganized to indicate if files should be organized under year and day of year while redoing the maps in SVWaBuWind2WERA program.
		-Added property winAscParams to indicate the correct params.cfg file to use for Plott_UV while redoing the maps in SVWaBuWind2WERA program.
		
		2016.03.20
		Roberto Gomez
		-Added property nRings_pt to provide the size of the SWB (3x3 or 5x5) and to be used for wind speed interpolation.
		-Added property winAscCode to include the CODE parameter from params.cfg to redo the maps in SVWaBuWind2WERA program.
		
		2014.02.12
		Roberto Gomez
		-Added property maxWiSpd to mark as bad quality all those wind speed values higher as the given value.
		
		2014.02.12
		Roberto Gomez
		-Added property forceInvertedHs to force using only the Hs values from inversion always that it is available.
		
		2014.02.11
		Roberto Gomez
		-Added method EncodeQualVal to create the quality value that includes, quality of Hs, of wind speed and period/direction parameters altogether. It also includes if period/direction parameters exists at all.
		-Added method DecodeQualVal to obtain the quality value of Hs, of wind speed and period/direction parameters, from a single integer with quality data encoded. Includes decoding if period/direction parameters exists at all.
		-Added property forceCombinedEmpiricalHs to force using only the combined empirical Hs values and not the inverted values (the Hs value from inversion seems to be not reliable sometimes).
		
		2013.10.15
		Roberto Gomez
		-Added parameters specPrefix_pt, sdtFile_pt and secondConf_pt, to set the different configuration parameters for each of the wave buoys.
		
		2013.10.08
		Roberto Gomez
		-Modified ReadSpectrum method to deal with different number of spectral lines.
		
		2013.09.23
		Roberto Gomez
		-Added properties gridX and gridY, required in data viewer time series page to work properly.
		
		2013.09.16
		Roberto Gomez
		-Added properties pixelX, pixelY and id_ixiys, required in data viewer ocean maps page to work properly.
		
		2013.09.13
		Roberto Gomez
		-Added parameter gridUsed. This parameter should tell the console tools that add the data to the database, to which grid should be added.
		-Included an extra trim in AssignParameterValue method when doing split with tabs for parameters for each points. Avoids having an space at the beginning of parameters for the first point.
		
		
		IMPORTANT!!! Insert notes of new update at the beginning, not at the end!!! Latest update info should ALWAYS be displayed first!!!
		
	#tag EndNote


	#tag Property, Flags = &h0
		cfgVarsName_svwb(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		dir(34) As double
	#tag EndProperty

	#tag Property, Flags = &h0
		energy(34) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		forceCombinedEmpiricalHs As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		forceInvertedHs As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		freqs(34) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		fullDirSpecAvail_pt(-1) As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		gridUsed As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		gridX(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		gridY(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		id_ixiys(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		lat_pt(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		lineIdxOfVars_svwb(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		linesOfTxtFile_svwb(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		lon_pt(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		maxWiSpd As Double = 20
	#tag EndProperty

	#tag Property, Flags = &h0
		nPts As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		nRings_pt(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		pixelX(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		pixelY(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		ptName_pt(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sdtFile_pt(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		secondConf_pt(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		specAvail_pt(-1) As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		specPrefix_pt(-1) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		wavAvail_pt(-1) As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		winAscCode As string
	#tag EndProperty

	#tag Property, Flags = &h0
		winAscOrganized As Boolean = true
	#tag EndProperty

	#tag Property, Flags = &h0
		winAscParams As string = "params.cfg"
	#tag EndProperty

	#tag Property, Flags = &h0
		winAscPath As string = "/home/wera/data/wav_asc/"
	#tag EndProperty

	#tag Property, Flags = &h0
		winAscSeconds As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		winAvail_pt(-1) As Boolean
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="forceCombinedEmpiricalHs"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="forceInvertedHs"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="gridUsed"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="maxWiSpd"
			Group="Behavior"
			InitialValue="20"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nPts"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="winAscCode"
			Group="Behavior"
			Type="string"
		#tag EndViewProperty
		#tag ViewProperty
			Name="winAscOrganized"
			Group="Behavior"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="winAscParams"
			Group="Behavior"
			InitialValue="params.cfg"
			Type="string"
		#tag EndViewProperty
		#tag ViewProperty
			Name="winAscPath"
			Group="Behavior"
			InitialValue="/home/wera/data/wav_asc/"
			Type="string"
		#tag EndViewProperty
		#tag ViewProperty
			Name="winAscSeconds"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
