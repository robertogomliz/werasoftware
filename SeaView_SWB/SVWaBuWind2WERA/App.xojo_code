#tag Class
Protected Class App
Inherits ConsoleApplication
	#tag Event
		Function Run(args() as String) As Integer
		  if DebugBuild then
		    ct.MsgLog(false," ",True)
		    ct.MsgLog(false,"AAHH... ICH LAUFE GERADE IN DEBUG MODE. VERGISS NICHT DIE QUELLCODE ÄNDERUNGEN ZU DOKUMENTIEREN!!! (mach es in App.Version_Notes).",True)
		    execDir="/home/wera/ApplicationSoftware/SeaViewWaveBuoy/"
		    //break
		  else
		    execDir=App.ExecutableFile.Parent.AbsolutePath
		  end
		  
		  ct.ConsoleOutput=Join(args," ") + EndOfLine
		  
		  //================ Check input arguments ============
		  if ct.CheckInputs(args) then
		    ct.MsgLog(false," ",True)
		    Quit
		  end
		  
		  
		  //============Licence check================
		  if key.LoadLicenceFile(App.execDir + "../") then
		    if not(std.ActivateMBSComplete) then  ct.MsgLog(false,"MBS Plugin serial not valid?",True)
		    if key.LicenceIsValidOnThisPC then
		      if not( key.PackageIsValid(key.Packages.SV_SWB))  then
		        ct.MsgLog(true,"Licence does not include WERA tools for SeaView's Synthetic Wave Buoy! Application terminated.",true)
		        Quit
		      end
		    else
		      ct.MsgLog(true,"Could not validate licence! Application terminated.",true)
		      ct.MsgLog(false, key.lastMessage_key,true)
		      Quit
		    end
		  else
		    ct.MsgLog(true, "Could not load licence file! Application terminated.", true)
		    ct.MsgLog(false, key.lastMessage_key,true)
		    Quit
		  end
		  
		  
		  //================ Check database connection ============
		  db.archive=new MySQLDatabase
		  if not(db.ReadDBSetupFile(execDir+"../DataArchivePrograms/DataArchive_setup.txt")) then
		    ct.MsgLog(true,db.lastMessage_db,True)
		    'Quit
		  end
		  if db.CheckDatabaseConnection then
		    ct.MsgLog(false,db.lastMessage_db,True)
		  else
		    ct.MsgLog(true,db.lastMessage_db + " Application terminated.",True)
		    Quit
		  end
		  
		  
		  
		  //================ Loading SWB parameters ============
		  ct.MsgLog(false,"Loading SWB parameters....",false)
		  if not(svwb.InitializeConfig(" ")) then
		    ct.MsgLog(true,"Problem loading SWB parameters! Application terminated.", True)
		    Quit
		  end
		  ct.MsgLog(false,"Done!",true)
		  
		  if svwb.nPts<1 then 
		    ct.MsgLog(true,"No SWB is defined in parameters file! Application terminated.",true)
		    Quit
		  end
		  db.grid=CStr(svwb.gridUsed)
		  
		  if CalcHalfLatLon then
		    ct.MsgLog(false," ", true)
		    Quit
		  end
		  
		  
		  
		  
		  
		  redim svwb.id_ixiys(nPts-1)
		  redim svwb.gridX(nPts-1)
		  redim svwb.gridY(nPts-1)
		  redim wSpeedSWB(nPts-1)
		  redim wDirSWB(nPts-1)
		  redim wSpeedGoodQual(nPts-1)
		  redim idxInVectOfSWB(nPts-1)
		  
		  
		  GatherData()
		  
		  AddVectorData()
		  
		  if goodQualCnt=2 then
		    DoInterpolation()
		  elseif goodQualCnt=1 then
		    AdjustSpeedWithDirectionDiff()
		  elseif goodQualCnt>2 then
		    ct.MsgLog(true, "Program is not yet capable of interpolation of more than 2 SWB! No interpolation will be performed.", true)
		  end
		  
		  StoreToDB()
		  
		  ReDoMaps()
		  
		  
		  db.archive.Close
		  ct.MsgLog(false,"Finished!",True)
		  ct.MsgLog(false," ",True)
		End Function
	#tag EndEvent

	#tag Event
		Function UnhandledException(error As RuntimeException) As Boolean
		  dim errorString, stackArray() as String
		  dim i as integer
		  
		  errorString=EndOfLine + EndOfLine + "There was an unhandled exception!"
		  errorString=errorString + EndOfLine + EndOfLine + "Error number " + CStr(error.ErrorNumber) + ": " + error.Message + EndOfLine + EndOfLine
		  errorString=errorString + "Error stack:" + EndOfLine
		  
		  stackArray=error.Stack
		  for i=stackArray.Ubound DownTo 0
		    errorString=errorString + stackArray(i) + EndOfLine
		  next
		  
		  ct.MsgLog(false," ",True)
		  ct.MsgLog(false,"Unhandled Exception! Application terminated.",True)
		  
		  ct.MsgLog(false, errorString,True)
		  
		  log.Write(false, ct.ConsoleOutput)
		  
		  ct.SendOutputReport("3","1")
		End Function
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddVectorData()
		  dim i, j, k, auxInt as integer
		  
		  // Create vectors
		  for i=0 to nPts-1
		    
		    ct.MsgLog(false," ",true)
		    ct.MsgLog(false,"Adding data from SWB  '"+ svwb.ptName_pt(i) + "' to vectors....",false)
		    
		    if wSpeedGoodQual(i) then 'if wind speed at SWB is good quality then the value should be spread to contiguos cells...
		      
		      
		      
		      //Spread wind speed value from SWB to contiguous cells in WERA data table
		      for j=svwb.gridX(i)-svwb.nRings_pt(i) to svwb.gridX(i)+svwb.nRings_pt(i)
		        for k=svwb.gridY(i)-svwb.nRings_pt(i) to svwb.gridY(i)+svwb.nRings_pt(i)
		          
		          //First get idixiy
		          rs=db.GetPtDataFromIxIy(j,k)
		          if db.archive.Error then
		            ct.MsgLog(false, "Application terminated.",true)
		            ct.MsgLog(true, db.lastMessage_db,true)
		            quit
		          end if
		          if rs.BOF then
		            ct.MsgLog(true,"Could not retrieve point information from grid coordinates (" + Cstr(j) + "," + CStr(k) + ")! Application terminated.",true)
		            quit
		          end
		          
		          auxInt=rs.Field("id_tb_ixiy").IntegerValue
		          idixiyVect.Append(auxInt)
		          xGridVect.Append(j)
		          yGridVect.Append(k)
		          wSpeedVect.Append(wSpeedSWB(i))
		          
		          if j=svwb.gridX(i) and k=svwb.gridY(I) then idxInVectOfSWB(i)=xGridVect.Ubound 'This is the location of the SWB... store current index
		          
		          // Now get wind direction value from WERA to reduce wind speed value if direction is very different from SWB
		          rs=db.GetMeasDataFromPtIdHeadID(20,headerWindWERA,CStr(auxInt),0)
		          if db.archive.Error then
		            ct.MsgLog(false, "Application terminated.",true)
		            ct.MsgLog(true, db.lastMessage_db,true)
		            quit
		          end if
		          if rs.BOF then
		            wDirVect.Append(-999)
		          else
		            auxInt=rs.Field("dir").IntegerValue
		            wDirVect.Append(auxInt)
		          end
		          
		        next
		      next
		      
		      ct.MsgLog(false,"Done!",true)
		      
		    else
		      
		      ct.MsgLog(false, "Wind speed quality is not good... skiping...", true)
		      continue
		      
		    end
		    
		    
		  next
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AdjustSpeedWithDirectionDiff()
		  //This method will be used to adjust the wind speed acoording to the difference between the wind direction measured by the SWB
		  //and the wind direction in the WERA map. It is executed when only one SWB provided valid data... when two SWB provided valid
		  //data, this process is done already at the DoInterpolation method.
		   
		  dim auxDbl as Double
		  dim auxInt, i as Integer
		  
		  
		  //Find which SWB will be adjusted...
		  if wSpeedGoodQual(0) then auxInt=0 else auxInt=1
		  
		  for i=0 to wSpeedVect.Ubound
		    
		    'The variable auxDbl will be used to store the difference between the wind dir reference and
		    'the measured in the WERA map
		    auxDbl=ShortestAngleDistance(wDirVect(i),wDirSWB(auxInt))
		    
		    if abs(auxDbl)>90 then
		      wSpeedVect(i)=-999 'means "no data available"
		    else
		      wSpeedVect(i)=wSpeedVect(i)*Cos(3.1416*auxDbl/180)
		      if wSpeedVect(i)<3 then wSpeedVect(i)=-999 'Lower sensitivity is 3 meters per second... if lower than that then make it a gap
		    end
		  next
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AssignInput(char as string, content as String) As boolean
		  
		  Dim auxInt as Integer
		  
		  select case char
		  case "t"
		    timePoint= new date
		    auxInt=len(content)
		    if auxInt=11 or auxInt=13 then
		      if IsNumeric(content) then
		        timePoint.SQLDateTime=std.Gurgeltime2DBstring(content)
		        return false
		      end if
		    elseif auxInt=19 then
		      try
		        timePoint.SQLDateTime=content.Left(10) + " " + content.Right(8)
		      catch err as UnsupportedFormatException
		        ct.MsgLog(true,"Date label not understood! Application terminated.",True)
		        ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		        Return true
		      end
		      return false
		    end if
		    
		    ct.MsgLog(true,"Date label not understood! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		    
		    
		  else
		    return true
		  end
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CalcHalfLatLon() As Boolean
		  
		  dim sqlRecordSet as RecordSet
		  
		  // Get grid size
		  sqlRecordSet=db.GetGridData()
		  if db.archive.Error then
		    ct.MsgLog(false, "Application terminated.",true)
		    ct.MsgLog(true, db.lastMessage_db,true)
		    return true
		  else
		    if sqlRecordSet.BOF then
		      ct.MsgLog(true,"No grid found for grid id = " + db.grid + ". Application terminated.",true)
		      return true
		    end
		    
		    dim grdminlon, grdminlat, grdmaxlon, grdmaxlat, grdlonstep, grdlatstep as Double
		    dim grdnx, grdny as integer
		    
		    //Getting the grid size
		    grdnx=sqlRecordSet.Field("nx").IntegerValue
		    grdny=sqlRecordSet.Field("ny").IntegerValue
		    
		    sqlRecordSet=db.GetPtDataFromIxIy(1,1)
		    grdminlon=sqlRecordSet.Field("lon_grd").DoubleValue
		    grdminlat=sqlRecordSet.Field("lat_grd").DoubleValue
		    
		    sqlRecordSet=db.GetPtDataFromIxIy(grdnx,grdny)
		    grdmaxlon=sqlRecordSet.Field("lon_grd").DoubleValue
		    grdmaxlat=sqlRecordSet.Field("lat_grd").DoubleValue
		    
		    grdlonstep=(grdmaxlon-grdminlon)/(grdnx-1)
		    grdlatstep=(grdmaxlat-grdminlat)/(grdny-1)
		    
		    // calculation of the half of the step distance between one index and the next (in the WERA grid)
		    halflonstep=abs(Ceil(grdlonstep*100000)/100000)/2  //abs(Ceil(lonstep*100000)/100000)/2
		    halflatstep=abs(Ceil(grdlatstep*100000)/100000)/2 //abs(Ceil(latstep*100000)/100000)/2
		    
		  end
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DoInterpolation()
		  
		  dim xGridVectAux(-1), yGridVectAux(-1) as double
		  dim i as integer
		  dim auxDbl,plusCosTheta, minusSinTheta as Double
		  
		  i=xGridVect.Ubound
		  redim xGridVectAux(i)
		  redim yGridVectAux(i)
		  
		  
		  ct.MsgLog(false," ",true)
		  ct.MsgLog(false,"Doing interpolation....",false)
		  
		  // Changing to auxiliar vectors of grid position. Original grid coordinates remain unchanged.
		  for i=0 to xGridVectAux.Ubound
		    xGridVectAux(i)=xGridVect(i)
		    yGridVectAux(i)=yGridVect(i)*(-1)  'Y axis is being inverted here to make it incremental upwards (to the north)
		  next
		  
		  // Translation to make swb1 to be (0,0)
		  for i=0 to xGridVectAux.Ubound
		    xGridVectAux(i)=xGridVectAux(i)-svwb.gridX(0)
		    yGridVectAux(i)=yGridVectAux(i)+svwb.gridY(0) 'The y axis is being added instead of substracted because of the inversion
		  next
		  
		  //Rotate whatever is required to make swb2 be aligned to the x axis
		  'Angle theta form from swb1 to swb2 will be calculated and stored in auxDbl
		  auxDbl=ATan2(yGridVectAux(idxInVectOfSWB(1))-yGridVectAux(idxInVectOfSWB(0)),xGridVectAux(idxInVectOfSWB(1))-xGridVectAux(idxInVectOfSWB(0)))
		  'Now we create a rotation matrix with the negative of the angle calculated...
		  auxDbl=-auxDbl
		  plusCosTheta=Cos(auxDbl)
		  minusSinTheta=Sin(auxDbl)*(-1)
		  for i=0 to xGridVectAux.Ubound
		    'Do rotation only on x values... y values will not be required anymore, except for the value calculated before the cycle
		    xGridVectAux(i)=plusCosTheta*xGridVectAux(i)+minusSinTheta*yGridVectAux(i) 
		  next
		  
		  //Scale x axis so that distance between swb1 and swb2 is unit
		  auxDbl=xGridVectAux(idxInVectOfSWB(1))
		  for i=0 to xGridVectAux.Ubound
		    xGridVectAux(i)=xGridVectAux(i)/auxDbl
		  next
		  
		  //Use x values and the difference between speed of swb1 and swb2 to interpolate wind speed
		  auxDbl=wSpeedSWB(1)-wSpeedSWB(0)
		  for i=0 to xGridVectAux.Ubound
		    if xGridVectAux(i)<0 OR xGridVectAux(i)>1 then continue  'To avoid extrapolation
		    wSpeedVect(i)=wSpeedSWB(0) + xGridVectAux(i)*auxDbl
		  next
		  
		  //Use x values and the difference between wind direction of swb1 and swb2 to 
		  //interpolate wind direction and adjust windspeed if difference with regards wind direction from WERA is too big...
		  auxDbl=ShortestAngleDistance(wDirSWB(0),wDirSWB(1))
		  for i=0 to xGridVectAux.Ubound
		    
		    'The variable plusCosTheta will be used to store the wind direction 'reference' to be compared to WERA wind direction
		    if xGridVectAux(i)<0 then
		      plusCosTheta=wDirSWB(0) 'wind dir reference is exactly the wind direction of swb1
		    elseif xGridVectAux(i)>1 then
		      plusCosTheta=wDirSWB(1) 'wind dir reference is exactly the wind direction of swb2
		    else
		      plusCosTheta=wDirSWB(0) + xGridVectAux(i)*auxDbl 'wind dir reference is something in between swb1 and swb
		    end
		    
		    'The variable minusSinTheta will be used to store the difference between the wind dir reference and 
		    'the measured in the WERA map
		    minusSinTheta=ShortestAngleDistance(wDirVect(i),plusCosTheta)
		    
		    if abs(minusSinTheta)>90 then
		      wSpeedVect(i)=-999 'means "no data available"
		    else
		      wSpeedVect(i)=wSpeedVect(i)*Cos(3.1416*minusSinTheta/180)
		      if wSpeedVect(i)<3 then wSpeedVect(i)=-999 'Lower sensitivity is 3 meters per second... if lower than that then make it a gap
		    end
		  next
		  
		  ct.MsgLog(false,"Done!",true)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function FindInterruptionReason(consoleText as String) As string
		  // Tries to find the reason for program interruption from the console displayed text and shows it.
		  
		  dim position, i, dotsCounter, eolCounter as integer
		  dim reasonString as string
		  dim stillSearchingFlag as Boolean
		  
		  i=1
		  dotsCounter=0
		  eolCounter=0
		  stillSearchingFlag=true
		  position=InStr(consoleText,"Application terminated")
		  
		  if (position > 0) then
		    consoleText=left(consoleText,position-1)
		    
		    for i=1 to len(consoleText)
		      
		      reasonString=Right(consoleText,i)
		      
		      if InStr(reasonString,EndOfLine)>eolCounter then
		        
		        if len(reasonString)<10 then
		          eolCounter=eolCounter + 1
		        else
		          return Right(consoleText,i-3)
		        end
		        
		      elseif InStr(reasonString,"...")>dotsCounter then
		        
		        if len(reasonString)<10 then
		          dotsCounter=dotsCounter + 1
		        else
		          return Right(consoleText,i-3)
		        end
		        
		      end
		      
		    next
		    
		  else
		    return " Execution interrupted! Unknown reason."
		  end
		  
		  return reasonString
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GatherData()
		  
		  dim i,auxInt as integer
		  dim auxBool1, auxBool2 as Boolean
		  
		  
		  for i=0 to nPts-1 'on each SWB location...
		    
		    ct.MsgLog(false," ",true)
		    ct.MsgLog(false,"----" + svwb.ptName_pt(i) + "----", true)
		    
		    //get grid cell info
		    ct.MsgLog(false,"Getting grid cell info at coordinates of SWB '"+ svwb.ptName_pt(i) + "'....",false)
		    rs=db.GetPtInfoFromApproxCoords(svwb.lat_pt(i)-halflatstep, svwb.lat_pt(i)+halflatstep, svwb.lon_pt(i)-halflonstep, svwb.lon_pt(i)+halflonstep)
		    if db.archive.Error then
		      ct.MsgLog(false, "Application terminated.",true)
		      ct.MsgLog(true, db.lastMessage_db,true)
		      quit
		    end if
		    if rs.BOF then
		      ct.MsgLog(true,"Could not retrieve point information from SWB coordinates! Application terminated.",true)
		      quit
		    end
		    
		    svwb.id_ixiys(i)=rs.Field("id_tb_ixiy").IntegerValue
		    svwb.gridX(i)=rs.Field("ix").IntegerValue
		    svwb.gridY(i)=rs.Field("iy").IntegerValue
		    
		    ct.MsgLog(false,"Done!",true)
		    
		    
		    //check if data is already in SWB table
		    ct.MsgLog(false,"Checking in database if there are SWB data at " + timePoint.SQLDateTime + " for this location...", false)
		    rs=db.GetSVWaBuData(timePoint.SQLDateTime,CStr(svwb.id_ixiys(i)))
		    
		    if db.archive.Error then
		      ct.MsgLog(false, "Application terminated!", true)
		      ct.MsgLog(true, db.lastMessage_db, true)
		      quit
		    end
		    
		    if rs.BOF then
		      ct.MsgLog(false, "No data found! No need to continue execution." + EndOfLine + "Finished!", true)
		      Quit
		    end
		    
		    wSpeedSWB(i)=rs.Field("wi_spd").DoubleValue
		    wDirSWB(i)=rs.Field("wi_dir").DoubleValue
		    auxInt=rs.Field("qual").IntegerValue
		    auxInt=svwb.DecodeQualVal(auxInt,auxBool1,auxBool2,auxBool2)
		    wSpeedGoodQual(i)=auxBool1
		    if auxBool1 then goodQualCnt=goodQualCnt+1
		    
		    ct.MsgLog(false, "Data found and stored!", true)
		    
		    
		  next
		  
		  if goodQualCnt=0 then
		    ct.MsgLog(false, "Wind speed data from SWB is bad quality! No need to continue execution." + EndOfLine + "Finished!", true)
		    quit
		  end
		  
		  
		  //Check if there is a record of wind measurements from WERA SW
		  ct.MsgLog(false," ",true)
		  ct.MsgLog(false,"Checking in database if there is a record of wind measurements from WERA...", false)
		  rs=db.GetHeaderData(20,timePoint.SQLDateTime,timePoint.SQLDateTime,false)
		  
		  if db.archive.Error then
		    ct.MsgLog(false, "Application terminated!", true)
		    ct.MsgLog(true, db.lastMessage_db, true)
		    quit
		  end
		  
		  if rs.BOF then
		    ct.MsgLog(false, "No WERA wind record yet! No need to continue execution." + EndOfLine + "Finished!", true)
		    quit
		  end
		  
		  headerWindWERA=rs.Field("id_tb_wuv_headers").IntegerValue
		  ct.MsgLog(false, "Found it.", true)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReDoMaps()
		  
		  
		  
		  dim shellBefehl,auxStr as string
		  dim s as shell
		  s=new shell
		  
		  ct.MsgLog(false," ",true)
		  ct.MsgLog(false,"Recreate maps of wind measured data:",true)
		  
		  //Re-Create win_asc file
		  ct.MsgLog(false,"Recreate win_asc file...",false)
		  shellBefehl=App.execDir + "../DataArchivePrograms/MakeWiuvFromDB -t="+std.Date2Gurgeltime(timePoint, svwb.winAscSeconds) + " -c=" + svwb.winAscCode + " -o=" + svwb.winAscPath
		  s.Execute(shellBefehl)
		  if (InStr(s.Result,"Application terminated") > 0) OR (InStr(s.Result,"Finished!") = 0) then  'Program was interrupted... something was not right!
		    ct.MsgLog(true,FindInterruptionReason(s.Result) + " Application terminated.",true)
		    log.Write(false,s.Result)
		    quit
		  else
		    ct.MsgLog(false,"Done!",true)
		  end
		  
		  //Generate map
		  ct.MsgLog(false,"Generating wind map...",false)
		  auxStr=svwb.winAscPath + std.Date2Gurgeltime(timePoint, svwb.winAscSeconds) +"_"+svwb.winAscCode + ".win_asc"
		  shellBefehl = "Plott_UV -f=" + auxStr + " -c=" + svwb.winAscParams + " -d=10"
		  dim InSh as InterShell
		  InSh=new InterShell
		  InSh.mode=2
		  InSh.Execute(shellBefehl)
		  while(InSh.IsRunning)
		    InSh.Poll()
		  wend
		  if InSh.ErrorCode<>0 then
		    ct.MsgLog(true,"Problem creating map! Application terminated.",true)
		    log.Write(false,InSh.Result)
		    quit
		  end
		  
		  ct.MsgLog(false,"Done!",true)
		  
		  //Modify output map to correct resolution
		  ct.MsgLog(false,"Modifying resolution...",false)
		  auxStr=auxStr + ".gif"
		  shellBefehl="convert -scale 720x717 " + auxStr + " " + auxStr
		  if ct.ShellExecC(shellBefehl,"Problem changing map resolution!") then quit
		  ct.MsgLog(false,"Done!",true)
		  
		  //Copy map to also have map for QC 3 (artefacts removed)
		  ct.MsgLog(false,"Copying map for QC level 3...",false)
		  shellBefehl = "cp " + auxStr + " " + replace(auxStr,".win_asc.gif","_ar.win_asc.gif")
		  if ct.ShellExecC(shellBefehl,"Problem copying map file!") then quit
		  ct.MsgLog(false,"Done!",true)
		  
		  
		  if svwb.winAscOrganized then
		    ct.MsgLog(false,"Moving all files to subfolder...",false)
		    auxStr=std.Date2Gurgeltime(timePoint, svwb.winAscSeconds)
		    shellBefehl = "mv " + svwb.winAscPath + auxStr +"_"+svwb.winAscCode + "* " + svwb.winAscPath + left(auxStr,4) + "/" + Mid(auxStr,5,3) + "/"
		    if ct.ShellExecC(shellBefehl,"Problem moving files!") then quit
		    ct.MsgLog(false,"Done!",true)
		  end if
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ShortestAngleDistance(angFrom as double, angTo as double) As double
		  //returns the shortest difference in degrees between two angles given also in degrees.
		  //e.g. shortest difference between 10° and 350° is not 340°, but -20°.
		  
		  dim auxDbl as double
		  
		  auxDbl=angTo-angFrom
		  if abs(auxDbl)>180 then 'Difference should go through 0 degrees!
		    if angTo>angFrom then
		      angTo=angTo-360
		    else
		      angFrom=angFrom-360
		    end
		    auxDbl=angTo-angFrom
		  end
		  
		  return auxDbl
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub StoreToDB()
		  
		  dim i as integer
		  dim sqlString as string
		  
		  ct.MsgLog(false," ",true)
		  ct.MsgLog(false,"Storing wind speed in WERA wind measurements....",false)
		  
		  for i=0 to xGridVect.Ubound
		    
		    sqlString="UPDATE tb_wiuv_data SET speed=" + CStr(wSpeedVect(i)) + " WHERE id_tb_wuv_headers=" + CStr(headerWindWERA) + " AND id_tb_ixiy=" + CStr(idixiyVect(i))
		    
		    db.archive.SQLExecute(sqlString)
		    if db.HandleDatabaseError("Problem updating wind speed values on WERA wind measurements! Application terminated.") then
		      ct.MsgLog(true,db.lastMessage_db,true)
		      Quit
		    end
		    
		  next
		  
		  ct.MsgLog(false,"Done!",true)
		End Sub
	#tag EndMethod


	#tag Note, Name = Version_Notes
		HOW TO FILL THIS NOTES...
		
		=Filename= (Notes... if applies. e.g. Version compiled for MELCO)
		A date to have a time reference (yyyy.mm.dd), 
		App version (Built using App.MajorVersion and App.NonReleaseVersion. CHECK THE VALUE OF App.NonRealeaseVersion AFTER COMPILING... COMPILING INCREASES THE VALUE AUTOMATICALLY!), 
		Person who edited and compiled this new version. 
		Real Studio version when compiled (check it on Hilfe>Über RealStudio) / Operative system (compiled with linux or windows)
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		IMPORTANT!!! Insert notes of new version in the next line, not at the end!!! last version info should ALWAYS be displayed first!!!
		
		=SVWaBuWind2WERA_V2_160323.xojo_binary_project=
		2016.03.23
		Roberto Gomez
		Version 3.5 BETA
		Xojo 2015 R1 / Windows 7
		-Changed program so it additionally interpolates the wind direction from SWB, and uses this as reference. If the wind direction of WERA is
		  the same as the reference from SWB, then wind speed remains unmodified. If different, it would be adjusted by a factor equal to cos(diff).
		  If the difference is more than 90 degrees, then the wind speed is marked as gap (-999).
		
		=SVWaBuWind2WERA_V2_160322.xojo_binary_project=
		2016.03.22
		Roberto Gomez
		Version 3.4 BETA
		Xojo 2015 R1 / Windows 7
		-Changed program so it does not spread the wind speed measurement on all the WERA wind data anymore.
		  Now it spreads the value measured at the SWB to contiguos cells and performs interpolation (up to 2 SWB at the moment without filling blanks).
		
		=SVWaBuWind2WERA_V2_150701.xojo_binary_project=
		2015.07.01
		Marek Swirgon
		Version 3.3 BETA
		Xojo 2015 R1 / Windows 7
		-Change Licence Validation (for CPU names without available speed, and reading numbers of processors from /proc/cpuinfo)
		-Corrected the missing '_db' , '_key'at the end of '..lastMessage'
		
		=SVWaBuWind2WERA_V2_141205.rbp=
		2014.12.05
		Roberto Gomez
		Version 2.2 BETA
		RS 2012 2.1 / Windows7
		-Activating MBS plugin serial before checking if WERA licence is valid, to avoid warning on MBS licence.
		
		=SVWaBuWind2WERA_V2_141114.rbp=
		2014.11.14
		Version 2.1 BETA
		Roberto Gomez
		RS 2012 2.1 / Windows
		-Increased major version to 2
		-Implemented licence check
		-Using standard code for unhandled exception event.
		-Updated ct.MsgLog method to 3 inputs.
		-Replaced usage of log.Write with ct.MsgLog
		
		=SVWaBuWind2WERA_V1_140312.rbp=
		2014.03.14
		Roberto Gomez
		Version 1.3
		RS 2012 2.1 / Windows 7
		-If Wind Speed value from SeaView SWB failed QC, then do not use it.
		
		=SVWaBuWind2WERA_V1_131216.rbp=
		2013.12.16
		Roberto Gomez
		Version 1.2
		RS 2012 2.1 / Linux
		-Corrected instances of rs=nil, which should be rs.BOF
		
		=SVWaBuWind2WERA_V1_131211.rbp=
		2013.12.11
		Roberto Gomez
		Version 1.1
		RS 2012 2.1 / Linux
		-First version
		
		
		IMPORTANT!!! Insert notes of new version at the beginning, not at the end!!! last version info should ALWAYS be displayed first!!!
	#tag EndNote


	#tag Property, Flags = &h0
		execDir As String
	#tag EndProperty

	#tag Property, Flags = &h0
		goodQualCnt As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		halflatstep As double
	#tag EndProperty

	#tag Property, Flags = &h0
		halflonstep As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		headerWindWERA As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		idixiyVect(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		idxInVectOfSWB(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		rs As RecordSet
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected timePoint As date
	#tag EndProperty

	#tag Property, Flags = &h0
		wDirSWB(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		wDirVect(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		wSpeedGoodQual(-1) As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		wSpeedSWB(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		wSpeedVect(-1) As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		xGridVect(-1) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		yGridVect(-1) As Integer
	#tag EndProperty


	#tag Constant, Name = kAppName, Type = String, Dynamic = False, Default = \"SVWaBuWind2WERA", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kDescription, Type = String, Dynamic = False, Default = \"Loads from the database the wind data measured at the location of Synthetic Wave Buoys corresponding to the point in time specified in <TimeLabel>. The obtained values are used to estimate the wind speed over wider areas in the WERA grid and are stored on the wind data table of the WERA Data Archive. WERA wind maps are recreated to include wind speed values.", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kInputTypes, Type = String, Dynamic = False, Default = \"t", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kLogFile, Type = String, Dynamic = False, Default = \"SVWaBuWind2WERA", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kNotes, Type = String, Dynamic = False, Default = \"<TimeLabel> is specified as Year-DayOfYear-Hour-Minutes and Optionally Seconds (yyyydddhhmm[ss]).", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kSyntax, Type = String, Dynamic = False, Default = \"-t\x3D<TimeLabel>", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kUse, Type = String, Dynamic = False, Default = \"This console application is used to take advantage of the wind data available at the location of Synthetic Wave Buoys to estimate the wind speed values on a wider area in the WERA measurement grid.", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="execDir"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="goodQualCnt"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="halflatstep"
			Group="Behavior"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="halflonstep"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="headerWindWERA"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
