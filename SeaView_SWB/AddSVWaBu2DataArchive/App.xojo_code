#tag Class
Protected Class App
Inherits ConsoleApplication
	#tag Event
		Function Run(args() as String) As Integer
		  if DebugBuild then
		    ct.MsgLog(false," ",True)
		    ct.MsgLog(false,"AAHH... ICH LAUFE GERADE IN DEBUG MODE. VERGISS NICHT DIE QUELLCODE ÄNDERUNGEN ZU DOKUMENTIEREN!!! (mach es in App.Version_Notes).",True)
		    execDir="/home/wera/ApplicationSoftware/DataArchivePrograms/"
		    break
		  else
		    execDir=App.ExecutableFile.Parent.AbsolutePath
		  end
		  
		  ct.ConsoleOutput=Join(args," ") + EndOfLine
		  
		  //================ Check input arguments ============
		  if ct.CheckInputs(args) then
		    ct.MsgLog(false," ",True)
		    Quit
		  end
		  
		  
		  //============Licence check================
		  if key.LoadLicenceFile(App.execDir + "../") then
		    if not(std.ActivateMBSComplete) then  ct.MsgLog(false,"MBS Plugin serial not valid?",True)
		    if key.LicenceIsValidOnThisPC then
		      if not( key.PackageIsValid(key.Packages.SV_SWB))  then
		        ct.MsgLog(true,"Licence does not include WERA tools for SeaView's Synthetic Wave Buoy! Application terminated.",true)
		        Quit
		      end
		    else
		      ct.MsgLog(true,"Could not validate licence! Application terminated.",true)
		      ct.MsgLog(false, key.lastMessage_key,true)
		      Quit
		    end
		  else
		    ct.MsgLog(true, "Could not load licence file! Application terminated.", true)
		    ct.MsgLog(false, key.lastMessage_key,true)
		    Quit
		  end
		  
		  
		  //================ Other checks required ============
		  if CheckEverything() then
		    ct.MsgLog(false," ",True)
		    Quit
		  end
		  
		  
		  //================ Check database connection ============
		  db.archive=new MySQLDatabase
		  if not(db.ReadDBSetupFile(execDir+"../DataArchivePrograms/DataArchive_setup.txt")) then
		    ct.MsgLog(true,db.lastMessage_db,True)
		    'Quit
		  end
		  if db.CheckDatabaseConnection then
		    ct.MsgLog(false,db.lastMessage_db,True)
		  else
		    ct.MsgLog(true,db.lastMessage_db + " Application terminated.",True)
		    Quit
		  end
		  
		  
		  //=============== Get Synthetic wave buoy parameters============
		  ct.MsgLog(false,"Loading SWB parameters....",false)
		  if not(svwb.InitializeConfig(" ")) then
		    ct.MsgLog(true,"Problem loading SWB parameters! Application terminated.", True)
		    Quit
		  end
		  ct.MsgLog(false,"Done!",true)
		  
		  db.grid=CStr(svwb.gridUsed)
		  
		  // ==============Calculate half step for latitude and longitude=======
		  if CalcHalfLatLon() then
		    ct.MsgLog(false," ",true)
		    Quit
		  end
		  
		  //=============== Retrieving Data from File===============
		  if GetDataFromWbdatFile() then
		    ct.MsgLog(false," ",True)
		    Quit
		  end
		  
		  //=============== Insert data to respective table ===================
		  if WriteMeasurementsToDataArchive() then
		    ct.MsgLog(false," ",True)
		    Quit
		  end
		  
		  db.archive.Close
		  ct.MsgLog(false,"Finished!",True)
		  ct.MsgLog(false," ",True)
		End Function
	#tag EndEvent

	#tag Event
		Function UnhandledException(error As RuntimeException) As Boolean
		  dim errorString, stackArray() as String
		  dim i as integer
		  
		  errorString=EndOfLine + EndOfLine + "There was an unhandled exception!"
		  errorString=errorString + EndOfLine + EndOfLine + "Error number " + CStr(error.ErrorNumber) + ": " + error.Message + EndOfLine + EndOfLine
		  errorString=errorString + "Error stack:" + EndOfLine
		  
		  stackArray=error.Stack
		  for i=stackArray.Ubound DownTo 0
		    errorString=errorString + stackArray(i) + EndOfLine
		  next
		  
		  ct.MsgLog(false," ",True)
		  ct.MsgLog(false,"Unhandled Exception! Application terminated.",True)
		  
		  ct.MsgLog(false, errorString,True)
		  
		  log.Write(false, ct.ConsoleOutput)
		  
		  ct.SendOutputReport("3","1")
		End Function
	#tag EndEvent


	#tag Method, Flags = &h0
		Function AssignInput(char as String, content as string) As Boolean
		  
		  select case char
		  case "p"
		    pAsc=content
		  else
		    return true
		  end
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function CalcHalfLatLon() As Boolean
		  dim sqlRecordSet as RecordSet
		  
		  // Get grid size
		  sqlRecordSet=db.GetGridData()
		  if db.archive.Error then
		    ct.MsgLog(false, "Application terminated.",true)
		    ct.MsgLog(true, db.lastMessage_db,true)
		    return true
		  else
		    if sqlRecordSet.BOF then
		      ct.MsgLog(true,"No grid found for grid id = " + db.grid + ". Application terminated.",true)
		      return true
		    end
		    
		    dim grdnx, grdny as Integer
		    dim grdminlon, grdminlat, grdmaxlon, grdmaxlat, grdlonstep, grdlatstep as Double
		    
		    //Getting the grid size
		    grdnx=sqlRecordSet.Field("nx").IntegerValue
		    grdny=sqlRecordSet.Field("ny").IntegerValue
		    
		    sqlRecordSet=db.GetPtDataFromIxIy(1,1)
		    grdminlon=sqlRecordSet.Field("lon_grd").DoubleValue
		    grdminlat=sqlRecordSet.Field("lat_grd").DoubleValue
		    
		    sqlRecordSet=db.GetPtDataFromIxIy(grdnx,grdny)
		    grdmaxlon=sqlRecordSet.Field("lon_grd").DoubleValue
		    grdmaxlat=sqlRecordSet.Field("lat_grd").DoubleValue
		    
		    grdlonstep=(grdmaxlon-grdminlon)/(grdnx-1)
		    grdlatstep=(grdmaxlat-grdminlat)/(grdny-1)
		    
		    // calculation of the half of the step distance between one index and the next (in the WERA grid)
		    halflonstep=abs(Ceil(grdlonstep*100000)/100000)/2  //abs(Ceil(lonstep*100000)/100000)/2
		    halflatstep=abs(Ceil(grdlatstep*100000)/100000)/2 //abs(Ceil(latstep*100000)/100000)/2
		    
		  end
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function CheckEverything() As Boolean
		  
		  //=================== Validating paths =========================
		  
		  if len(pAsc)=0 then
		    ct.MsgLog(true,"File path is empty! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		  end
		  
		  psAsc=GetFolderItem(pAsc)
		  if psAsc = Nil then
		    ct.MsgLog(true,"Invalid pathname for file! Application terminated.",True)
		    Return true
		  elseif psAsc.Directory then
		    ct.MsgLog(true,"Path seems to be a folder, not a file. Application Terminated.",True)
		    Return true
		  elseif not(psAsc.Exists) then
		    ct.MsgLog(true,"File does not exists! Application terminated.",True)
		    Return true
		  else
		    SourceStream=TextInputStream.Open(psAsc)
		    if SourceStream = Nil then
		      ct.MsgLog(true,"Error opening file! Application terminated.",True)
		      Return true
		    end
		    SourceStream.Close
		  end
		  if right(psAsc.AbsolutePath,6)<>".wbdat" then
		    ct.MsgLog(true,"File extension is incorrect! Application terminated.",True)
		    ct.MsgLog(false,"Use '-h' as input parameter to see the syntax of input arguments.",True)
		    Return true
		  end
		  
		  ct.MsgLog(false,"    Path is valid... .wbdat text file was found!",True)
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function GetDataFromWbdatFile() As Boolean
		  Dim auxStringArray(-1), qual, level, wSpeed, wDir, Hs as String
		  Dim auxDbl as Double
		  dim wQual, dirQual, invData as Boolean
		  
		  ct.MsgLog(false,"Reading data from .wbdat file...",false)
		  
		  //Opening ASCII file as textfile
		  SourceStream=TextInputStream.Open(psAsc)
		  SourceStream.Encoding=Encodings.ASCII
		  
		  //First line is measurement header
		  auxStringArray=Split(SourceStream.ReadLine,"   ")
		  measTimeDB= new Date(CLong(auxStringArray(0).Mid(13,4)),CLong(auxStringArray(0).Mid(10,2)),CLong(auxStringArray(0).Mid(7,2)),CLong(auxStringArray(0).Mid(1,2)),CLong(auxStringArray(0).Mid(4,2)))
		  lat=CDbl(auxStringArray(1))
		  lon=CDbl(auxStringArray(2))
		  level=auxStringArray(3)
		  
		  //Second line contains ALWAYS wind parameters
		  auxStringArray=Split(SourceStream.ReadLine,"   ")
		  wSpeed = auxStringArray(0)
		  if wSpeed="-9999" then wSpeed="-999" //sometimes wind speed and direction provide -9999... lets change it to -999 since -999 is marked as gap.
		  auxDbl=CDbl(auxStringArray(1))
		  if auxDbl=-9999 then
		    auxDbl=-999
		  else
		    if auxDbl<0 then wDir=CStr(auxDbl+360) else wDir = auxStringArray(1)
		  end
		  if auxStringArray(2)="1" then wQual=true
		  
		  //'Third line contains empirical significant wave heights from each station
		  auxStringArray=Split(SourceStream.ReadLine,"   ")
		  
		  select case level
		  case "-1" 'Then it is an artifact... use combined empirical value... 'Third line is not needed
		    qual="4"
		    auxStringArray=Split(SourceStream.ReadLine,"   ") 'Fourth line contains combined empirical significant wave heights
		    Hs=auxStringArray(0)
		    if auxStringArray(1)="1" then wQual=true else wQual=false 'Use combined empirical quality for the wind speed!
		    auxStringArray=Split(SourceStream.ReadLine,"   ") 'Fifth line contains wave parameters
		    
		  case "0" 'Then store the empirical significant wave height value whose quality value is 1
		    qual="3"
		    if auxStringArray(1) = "1" then
		      Hs=auxStringArray(0)
		    else
		      Hs=auxStringArray(2)
		    end
		    auxStringArray=Split(SourceStream.ReadLine,"   ") 'Fourth line contains combined empirical significant wave heights
		    if svwb.forceCombinedEmpiricalHs then Hs=auxStringArray(0) 'Replacing the value with the combined value...
		    if auxStringArray(1)="1" then wQual=true else wQual=false 'Use combined empirical quality for the wind speed!
		    auxStringArray=Split(SourceStream.ReadLine,"   ") 'Fifth line contains wave parameters
		    
		  case "1" 'Then store the combined empirical value... Third line is not needed
		    qual="2"
		    auxStringArray=Split(SourceStream.ReadLine,"   ")'Fourth line contains combined empirical significant wave heights
		    Hs=auxStringArray(0)
		    if auxStringArray(1)="1" then wQual=true else wQual=false 'Use combined empirical quality for the wind speed!
		    auxStringArray=Split(SourceStream.ReadLine,"   ") 'Fifth line contains wave parameters
		    
		  case "2" 'Then store the Hs value from inversion... Third line is not needed
		    qual="1"
		    auxStringArray=Split(SourceStream.ReadLine,"   ") 'Fourth line is not needed
		    if svwb.forceCombinedEmpiricalHs then Hs=auxStringArray(0) 'Replacing the value with the combined value... It was actually needed
		    if auxStringArray(1)="1" then wQual=true else wQual=false 'Use combined empirical quality for the wind speed!
		    auxStringArray=Split(SourceStream.ReadLine,"   ") 'Fifth line contains wave parameters from inversion
		    if not(svwb.forceCombinedEmpiricalHs) then Hs=auxStringArray(0)
		  else
		    ct.MsgLog(true, "Level value in header not identified: " + level + ". Application terminated.",true)
		    return true
		    
		  end
		  
		  if CDbl(wSpeed)>svwb.maxWiSpd then wQual=false  'Overwriting to bad quality in case the value is bigger than limit
		  
		  sqlString="INSERT INTO tb_svwabu (measTimeDB, qual, wi_spd, wi_dir, wa_height"
		  if auxStringArray.Ubound>=6 then  'If the fifth line is not empty, then data from inversion is available... store this data too.
		    invData=true
		    if svwb.forceInvertedHs then Hs=auxStringArray(0) 'Replacing the value with the inverted value
		    if auxStringArray(6)="1" then dirQual=true 'In case direction and period data is not reliable
		    
		    sqlString=sqlString + ", wa_mean_T, wa_peak_T, wa_mean_dir, wa_peak_dir"
		    
		    sqlString=sqlString + ", id_tb_ixiy) VALUES ('" + measTimeDB.SQLDateTime + "'," + CStr(svwb.EncodeQualVal(CLong(qual), wQual, invData, dirQual)) + "," + wSpeed + "," + wDir + "," + Hs +  ","
		    
		    sqlString=sqlString + auxStringArray(1) + "," + auxStringArray(2) + ","
		    auxDbl=CDbl(auxStringArray(3))
		    if auxDbl<0 then auxDbl=auxDbl+360
		    sqlString=sqlString + CStr(auxDbl) + ","
		    auxDbl=CDbl(auxStringArray(4))
		    if auxDbl<0 then auxDbl=auxDbl+360
		    sqlString=sqlString + CStr(auxDbl) + ","
		    
		  else
		    sqlString=sqlString + ", id_tb_ixiy) VALUES ('" + measTimeDB.SQLDateTime + "'," + CStr(svwb.EncodeQualVal(CLong(qual), wQual, invData, dirQual)) + "," + wSpeed + "," + wDir + "," + Hs +  ","
		  end
		  SourceStream.Close
		  
		  
		  ct.MsgLog(false,"Done!",true)
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function WriteMeasurementsToDataArchive() As Boolean
		  dim rs as RecordSet
		  dim id_tb_ixiy as string
		  
		  // Get point information
		  ct.MsgLog(false,"Getting point ID from coordinates...",false)
		  rs=db.GetPtInfoFromApproxCoords(lat-halflatstep,lat+halflatstep,lon-halflonstep,lon+halflonstep)
		  if db.archive.Error then
		    ct.MsgLog(false, "Application terminated.",true)
		    ct.MsgLog(true, db.lastMessage_db,true)
		    return true
		  end if
		  if rs.BOF then
		    ct.MsgLog(true, "No point information  found for coordinates " + CStr(lat) + ", " + CStr(lon) + ". Application terminated.",true)
		    return true
		  end
		  ct.MsgLog(false,"Got it!",true)
		  id_tb_ixiy= rs.Field("id_tb_ixiy").StringValue
		  sqlString=sqlString + id_tb_ixiy + ");"
		  
		  // Checking if measurement exists already
		  ct.MsgLog(false,"Checking if measurement exists already...",false)
		  rs=db.GetSVWaBuData(measTimeDB.SQLDateTime, id_tb_ixiy)
		  if db.archive.Error then
		    ct.MsgLog(false, "Application terminated.",true)
		    ct.MsgLog(true, db.lastMessage_db,true)
		    return true
		  end if
		  if not(rs.BOF) then
		    ct.MsgLog(true, "Measurement exists already! Application terminated.",true)
		    return true
		  end
		  ct.MsgLog(false,"It does not!",true)
		  
		  // Inserting record to database
		  ct.MsgLog(false,"Inserting measurement data to database...",false)
		  db.archive.SQLExecute(sqlString)
		  if db.HandleDatabaseError("Error inserting records! Application terminated.") then
		    ct.MsgLog(true,db.lastMessage_db,true)
		    return true
		  end
		  ct.MsgLog(false,"Done!",true)
		  
		  
		  return false
		End Function
	#tag EndMethod


	#tag Note, Name = Version_Notes
		
		HOW TO FILL THIS NOTES...
		
		=Filename= (Notes... if applies. e.g. Version compiled for MELCO)
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		App version (Built using App.MajorVersion and App.NonReleaseVersion. CHECK THE VALUE OF App.NonRealeaseVersion AFTER COMPILING... COMPILING INCREASES THE VALUE AUTOMATICALLY!), 
		Real Studio version when compiled (check it on Hilfe>Über RealStudio) / Operative system (compiled with linux or windows)
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		IMPORTANT!!! Insert notes of new version in the next line, not at the end!!! last version info should ALWAYS be displayed first!!!
		
		=AddSVWaBu2DataArchive_V3_160323.xojo_binary_project=
		2016.03.23
		Roberto Gomez
		Version 3.6 BETA
		Xojo 2015 R1 / Windows 7
		-Sometimes the SWB software returns -9999 for both wind speed and wind direction. Now it is checked if this is the value and it is changed to -999, in order for the DataViewer to understand this as gap.
		
		=AddSVWaBu2DataArchive_V3_160322.xojo_binary_project=
		2016.03.22
		Roberto Gomez
		Version 3.5 BETA
		Xojo 2015 R1 / Windows 7
		-Verifying that the wind speed is bigger than threshold is done now much later in the code, to be able to toss out bad measurements even when the combined empirical wave quality says is good.
		
		=AddSVWaBu2DataArchive_V3_160321.xojo_binary_project=
		2016.03.21
		Roberto Gomez
		Version 3.4 BETA
		Xojo 2015 R1 / Windows 7
		-Now using the quality indicator of combined empirical wave for wind as well.
		
		=AddSVWaBu2DataArchive_V3_150701.xojo_binary_project=
		2015.07.01
		Marek Swirgon
		Version 3.3 BETA
		Xojo 2015 R1 / Windows 7
		-Change Licence Validation (for CPU names without available speed, and reading numbers of processors from /proc/cpuinfo)
		-Corrected the missing '_db' , '_key'at the end of '..lastMessage'
		
		=AddSVWaBu2DataArchive_V3_141205.rbp=
		2014.12.05
		Roberto Gomez
		Version 3.2 BETA
		RS 2012 2.1 / Windows7
		-Activating MBS plugin serial before checking if WERA licence is valid, to avoid warning on MBS licence.
		
		=AddSVWaBu2DataArchive_V3_141114.rbp=
		2014.11.14
		Version 3.1 BETA
		Roberto Gomez
		RS 2012 2.1 / Windows
		-Increased major version to 2
		-Implemented licence check
		-Using standard code for unhandled exception event.
		-Updated ct.MsgLog method to 3 inputs.
		-Replaced usage of log.Write with ct.MsgLog
		
		=AddSVWaBu2DataArchive_V1_140311.rbp=
		2014.03.11
		Roberto Gomez
		Version 2.4 BETA
		RS 2012 2.1 / Windows 7
		-Implemented usage of parameter svwb.maxWiSpd to mark as bad quality all strangely high values of wind speed.
		
		=AddSVWaBu2DataArchive_V1_140212.rbp=
		2014.02.11
		Roberto Gomez
		Version 2.2 BETA
		RS 2012 2.1 / Windows 7
		-Corrected programming error to make svwb.forceCombinedEmpiricalHs to work as it should when inversion data should be stored. 
		-Using also svwb.forceInvertedHs to force using the inverted Hs when it is available.
		
		=AddSVWaBu2DataArchive_V1_140211.rbp=
		2014.02.11
		Roberto Gomez
		Version 2.1 BETA
		RS 2012 2.1 / Windows 7
		-Now taking into account the wind quality value and direction quality value separated. These are encoded into the qual value in the database. If inversion data is available, this will also be encoded into the database qual value.
		-Included build automation scripts to save before debugging and after compiling.
		-Implemented the parameter svwb.forceCombinedEmpiricalHs to indicate only values from combined empirical hs should be taken and not store the Hs value from inversion (which seems to be unreliable).
		
		=AddSVWaBu2DataArchive_V1_140123.rbp=
		2014.01.23
		Roberto Gomez
		Version 1.6
		RS 2012 2.1 / Windows 7
		-Converting angle values from (-180,180) to (0,360) before storing in the database.
		-Fixed bug that stores Hs value as 1 when the empirical Hs value from antenna 2 should be stored.
		
		=AddSVWaBu2DataArchive_V1_131202.rbp=
		2013.12.02
		Roberto Gomez
		Version 1.5
		RS 2012 2.1 / Windows 7
		-Now taking into account the second quality flag on the line with inversion results. If second quality flag is bad then reduce quality to 2 and do not store inversion results, only Hs.  
		
		=AddSVWaBu2DataArchive_V1_131015.rbp=
		2013.10.15
		Roberto Gomez
		Version 1.4
		RS 2012 2.1 / Windows 7
		-Recompiled to consider new parameters in svwb configuration file.
		
		=AddSVWaBu2DataArchive_V1_131008.rbp=
		2013.10.08
		Roberto Gomez
		Version 1.3
		RS 2012 2.1 / Windows 7
		-Modified to comply with modifications on SeaView's wavebuoy software. Now handling empirical values of significant wave height.
		
		=AddSVWaBu2DataArchive_V1_130923.rbp=
		2013.09.23
		Roberto Gomez
		Version 1.2
		RS 2012 2.1 / Windows 7
		-Corrected order of data insertion.
		
		=AddSVWaBu2DataArchive_V1_130916.rbp=
		2013.09.16
		Roberto Gomez
		Version 1.1
		RS 2012 2.1 / Windows 7
		-First version
		
		
		IMPORTANT!!! Insert notes of new version at the beginning, not at the end!!! last version info should ALWAYS be displayed first!!!
	#tag EndNote


	#tag Property, Flags = &h0
		execDir As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected halflatstep As Double
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected halflonstep As Double
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected lat As Double
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected lon As Double
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected measTimeDB As date
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected pAsc As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected psAsc As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected SourceStream As TextInputStream
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected sqlString As string
	#tag EndProperty


	#tag Constant, Name = kAppName, Type = String, Dynamic = False, Default = \"AddSVWaBu2DataArchive", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kDescription, Type = String, Dynamic = False, Default = \"Adds wave/wind parameters from the SeaView\'s synthetic wave buoy data file indicated by <PathToWbdatFile>.", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kInputTypes, Type = String, Dynamic = False, Default = \"p", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kLogFile, Type = String, Dynamic = False, Default = \"AddSVWaBu2DataArchive", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kNotes, Type = String, Dynamic = False, Default = \"The \'\x3D\' character must be used. Spaces are not allowed within each argument\x2C before and after the \'\x3D\' character (e.g. using \'-p \x3D /home/.../filename.wbdat\' will throw an error).", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kSyntax, Type = String, Dynamic = False, Default = \"-p\x3D<PathToWbdatFile>", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kUse, Type = String, Dynamic = False, Default = \"This console application is used to add the measurements of a SeaView\'s synthetic wave bouy file (.wbdat) to the data archive of WERA. The console application will only add the data corresponding to wave and wind parameters\x2C without including the wave energy/direction spectrum information.", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="execDir"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
