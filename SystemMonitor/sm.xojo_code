#tag Module
Protected Module sm
	#tag Method, Flags = &h1
		Protected Function UpdateSystemStatus() As boolean
		  
		  // Reads the system warnings contained in any text file (*.status) located in /home/wera/SystemMonitor/status/.
		  dim dir as new FolderItem
		  dim f as new FolderItem
		  dim SourceStream as TextInputStream
		  dim i, j as integer
		  dim fileLines(-1), fileColumns(-1), detailsStringTemp(1), extraWarningsTemp as string
		  dim warningAvailTemp(1) as Boolean
		  
		  extraWarningsTemp=EndOfLine
		  for i=0 to 1
		    warningAvailTemp(i)=false
		    detailsStringTemp(i)=""
		  next
		  
		  dir=GetFolderItem(App.execDir + "../SystemMonitor/status/")
		  
		  if dir = Nil then
		    log.Write(false,"Invalid folder for system monitor!")
		    return false
		  elseif not(dir.Directory) then
		    log.Write(false,"System status path is not a folder.")
		    return false
		  else
		    
		    if dir.Count=0 then
		      log.Write(false,"No system status files!")
		      return false
		    end
		    
		    for i=1 to dir.Count
		      f=dir.Item(i)
		      if Right(f.AbsolutePath,7)=".status" then //only if it is a .status file
		        
		        
		        SourceStream=TextInputStream.Open(f)
		        if SourceStream = Nil then
		          log.Write(false,"Error opening system status file! " + f.AbsolutePath )
		          return false
		        else
		          fileLines=Split(SourceStream.ReadAll, EndOfLine)
		          
		          for j=0 to fileLines.Ubound
		            if trim(fileLines(j))<>"" then  //empty line?
		              fileColumns=Split(fileLines(j),Chr(9))
		              
		              if CLong(fileColumns(4))>=8 then  //If equal or bigger than 8 than it should be displayed on the DataViewer.
		                
		                if cfgDV.nStations=1 then
		                  
		                  if fileColumns(1) = siteInfo.longname(0) OR fileColumns(1) = siteInfo.acronym(0) OR fileColumns(1) = siteInfo.sname(0) then
		                    warningAvailTemp(0)=true
		                    detailsStringTemp(0)=detailsStringTemp(0) + fileColumns(0) + "    " + fileColumns(2) + ": " + fileColumns(5) + EndOfLine
		                  else
		                    extraWarningsTemp=extraWarningsTemp + EndOfLine + "From " + fileColumns(1) + EndOfLine + fileColumns(0) + "    " + fileColumns(2) + ": " + fileColumns(5) + EndOfLine
		                  end
		                  
		                else
		                  
		                  if fileColumns(1) = siteInfo.longname(0) OR fileColumns(1) = siteInfo.acronym(0) OR fileColumns(1) = siteInfo.sname(0) then
		                    warningAvailTemp(0)=true
		                    detailsStringTemp(0)=detailsStringTemp(0) + fileColumns(0) + "    " + fileColumns(2) + ": " + fileColumns(5) + EndOfLine
		                  elseif fileColumns(1) = siteInfo.longname(1) OR fileColumns(1) = siteInfo.acronym(1) OR fileColumns(1) = siteInfo.sname(1) then
		                    warningAvailTemp(1)=true
		                    detailsStringTemp(1)=detailsStringTemp(1) + fileColumns(0) + "    " + fileColumns(2) + ": " + fileColumns(5) + EndOfLine
		                  else
		                    extraWarningsTemp=extraWarningsTemp + EndOfLine + "From " + fileColumns(1) + EndOfLine + fileColumns(0) + "    " + fileColumns(2) + ": " + fileColumns(5) + EndOfLine
		                  end
		                  
		                end if
		              end
		              
		            end
		          next
		          SourceStream.Close
		        end
		        
		        
		        
		      end
		      
		    next
		    
		  end
		  
		  //Updating variables
		  extraWarnings=extraWarningsTemp
		  for i=0 to 1
		    warningAvail(i)=warningAvailTemp(i)
		    detailsString(i)=detailsStringTemp(i)
		  next
		  
		  return true
		End Function
	#tag EndMethod


	#tag Note, Name = Updates
		
		
		HOW TO FILL THIS NOTES...
		
		A date to have a time reference (yyyy.mm.dd), 
		Person who edited and compiled this new version. 
		-Change 1
		-Change 2
		...
		-Change N
		
		=================================================================================================
		
		
		IMPORTANT!!! Insert notes of new updates in the next line, not at the end!!! latest update info should ALWAYS be displayed first!!!
		
		2014.10.10
		RG   
		-Added statement if cfgDV.nStations=1 in UpdateSystemStatus method to avoid unhandled exception while updating warnings when cfgDV.nStations=1
		
		2013.10.11   
		-Now looking for status files at folder ../SystemMonitor/status/
		
		2013.09.09   (Used for demo in Turkei project)
		-SystemMonitor folder may be found with relative path.
		-Using now .status extension for status files.
		
		2013.09.04   (Used for demo in Turkei project)
		-First version created.
		
		
		IMPORTANT!!! Insert notes of new update at the beginning, not at the end!!! Latest update info should ALWAYS be displayed first!!!
		
	#tag EndNote


	#tag Property, Flags = &h1
		Protected detailsString(1) As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected extraWarnings As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected warningAvail(1) As boolean
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
